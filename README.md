# Installation Composer 

Installer composer via le tutoriel suivant :

https://getcomposer.org/download/

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === 'aa96f26c2b67226a324c27919f1eb05f21c248b987e6195cad9690d5c1ff713d53020a02ac8c217dbf90a7eacc9d141d') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/bin --filename=composer
rm composer-setup.php
```

puis lancer l'installation du dossier vendor
```
composer install
```

# Installation PHP5.6 minimum sur Mac OS X
**PHP5.6 minimum est requis pour installer composer !**

Les utilisateurs Mac OS X doivent donc upgrader PHP : 

https://php-osx.liip.ch/

# Tests

pour lancer les tests unitaires, inutile d'installer phpunit, il à été installé avec composer :)
```
php ./vendor/bin/phpunit -c tests/phpunit.xml
```
