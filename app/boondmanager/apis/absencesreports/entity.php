<?php
/**
 * absencesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\AbsencesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Services\BM;
use BoondManager\APIs\AbsencesReports\Specifications\HaveCreateAccess;
use BoondManager\APIs\AbsencesReports\Specifications\HaveDeleteAccess;
use BoondManager\APIs\AbsencesReports\Specifications\HaveReadAccess;
use BoondManager\APIs\AbsencesReports\Specifications\HaveWriteAccess;

class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'title',
		'informationComments',
		'closed',
		'validationWorkflow' => [
			'id',
			'lastName',
			'firstName'
		],
		'absencesPeriods' => [
			'id',
			'startDate',
			'endDate',
			'duration',
			'title',
			'workUnitType' => [
				'reference',
				'name',
				'activityType'
			]
		],
		'absencesAccounts' => [
			'period',
			'year',
			'amountAcquired',
			'amountBeingAcquired',
			'amountAcquiredAsked',
			'amountBeingAcquiredAsked',
			'deltaAcquiredAsked',
			'deltaBeingAcquiredAsked',
			'workUnitType' => [
				'reference',
				'name',
				'activityType'
			],
			'agency' => [
				'id',
				'name'
			]
		],
		'resource' => [
			'id',
			'lastName',
			'firstName',
			'account' => [
				'id',
				'workUnitTypesAllowedForAbsences',
				'workUnitRate'
			]
		],
		'agency' => [
			'id',
			'name',
			'workUnitRate',
			'calendar',
			'workUnitTypes' => [
				'reference',
				'activityType',
				'name'
			]
		],
		'files' => [
			'id',
			'name'
		],
		'validations' => [
			'id',
			'date',
			'state',
			'reason',
			'realValidator' => [
				'id',
				'lastName',
				'firstName'
			],
			'expectedValidator' => [
				'id',
				'lastName',
				'firstName'
			]
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// gets existing
			$entity = Services\AbsencesReports::get($id);
			// if not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(), $entity );
		} else {
			// retrieve filtered params
			$filter = new Filters\GetDefault();
			$filter->setData( $this->requestAccess->getParams() );

			// check params
			$this->checkFilter($filter);

			$this->checkAccessWithSpec( new HaveCreateAccess ); // TODO

			$entity = Services\AbsencesReports::getNew($filter);
		}

		list($periods, $quotas) = Services\Absences::returnPeriodesDecomptesAvailableAndSetQuotasList($entity, $entity->creationDate, true);
		$absencesTaken = Services\Absences::getListeAbsencesPrises($entity->resource->id, $periods);
		$absencesAsked = Services\Absences::getListeAbsencesPoses($entity->resource->id, $periods);

		$entity->absencesAccounts = Services\Absences::getAllAccounts($entity->resource, $absencesAsked, $absencesTaken, $quotas);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS),
		];

		if($warnings = Services\Absences::getAbsencesWarnings($entity)){
			$tabData['warnings'] = $warnings;
		}

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$entity = Services\AbsencesReports::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $entity);

		$filter = new Filters\SaveEntity($entity);
		$filter->setData( $this->requestAccess->get('data') );

		$this->checkFilter($filter);

		$entity = Services\AbsencesReports::buildFromFilter($filter, $entity);

		if( Services\AbsencesReports::update($entity) ) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}

	public function api_delete() {
		$entity = Services\AbsencesReports::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $entity );

		if( Services\AbsencesReports::delete($entity) ) {
			$this->sendJSONResponse([
				'data' => [
					'success' => true
				]
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
		}
	}
}
