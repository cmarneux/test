<?php
/**
 * getdefault.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports\Filters;

use BoondManager\Lib\Filters\Inputs\InputMonth;
use BoondManager\Models\TimesReport;
use BoondManager\Services\Agencies;
use BoondManager\Services\Employees;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;

/**
 * Class GetDefault
 * @package BoondManager\APIs\TimesReports\Filters
 * @property InputMonth term
 * @property InputInt resource
 * @property InputInt agency
 */
class GetDefault extends AbstractFilters {

	/**
	 * @var TimesReport|null
	 */
	private $_timeReport;

	/**
	 * Profil constructor.
	 * @param TimesReport $timeReport
	 */
	public function __construct($timeReport = null){

		$this->_timeReport = $timeReport;

		parent::__construct();

		$employee = new InputInt('resource');
		$employee->setRequired(true);
		$employee->addFilterCallback(function($value) {
			if(Employees::find($value)) return $value;
			else return false;
		});
		$this->addInput( $employee );

		$agency = new InputInt('agency');
		$agency->addFilterCallback(function($value) {
			if(Agencies::find($value)) return $value;
			else return false;
		});
		$this->addInput( $agency );
	}
}
