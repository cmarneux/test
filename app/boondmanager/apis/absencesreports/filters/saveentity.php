<?php
/**
 * saveentity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports\Filters;

use BoondManager\Lib\Filters\Absence;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\Employee;
use BoondManager\Models\AbsencesReport;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;

/**
 * Class SaveEntity
 * @package BoondManager\Models\Filters\Profiles\AbsencesReports
 * @property InputDate creationDate
 * @property InputString title
 * @property InputString informationComments
 * @property InputMultiObjects absencesPeriods
 * @property Employee resource
 * @property Agency agency
 */
class SaveEntity extends AbstractJsonAPI{

	/**
	 * @var string
	 */
	protected $_objectClass = AbsencesReport::class;
	/**
	 * @var AbsencesReport
	 */
	protected $_absenceReport;

	/**
	 * SaveEntity constructor.
	 * @param AbsencesReport|null $absence
	 */
	public function __construct($absence = null)
	{
		$this->_absenceReport = $absence;
		parent::__construct();

		$this->addInput( new InputDate('creationDate') );

		$title = new InputString('title');
		$title->setMaxLength(250);
		$this->addInput( $title );

		$this->addInput( new InputString('informationComments') );

		$this->addInput( new InputMultiObjects('absencesPeriods', new Absence()));

		$this->addInput( new Employee() );
		$this->addInput( new Agency() );

		$this->isCreation() ? $this->adaptForCreation() : $this->adaptForEdition();
	}

	private function adaptForCreation() {
		/** @var Absence $model */
		$model = $this->absencesPeriods->getModel();
		$model->id->setDisabled(true);

		$this->resource->setRequired(true);
		$this->agency->setRequired(true);
		$this->creationDate->setRequired(true);
	}

	private function adaptForEdition() {
		$this->creationDate->setDisabled(true);
		$this->resource->setDisabled(true);
		$this->agency->setDisabled(true);
	}

	private function isCreation() {
		return !$this->_absenceReport || !$this->_absenceReport->id;
	}

	protected function postValidation() {

	}
}
