<?php
/**
 * absences.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports;

use BoondManager\APIs\AbsencesReports\Filters\SaveEntity;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\AbsencesReport;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\AbsencesReports\Specifications\HaveCreateAccess;
use BoondManager\APIs\AbsencesReports\Specifications\HaveSearchAccess;
use BoondManager\OldModels\Specifications\RequestAccess\HaveRight;

class Index extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'title',
		'state',
		'absencesPeriods' => [
			'id',
			'startDate',
			'endDate',
			'duration',
			'title',
			'workUnitType'
		],
		'agency' => [
			'id'
		],
		'resource' => [
			'id',
			'lastName',
			'firstName'
		],
		'validations' => [
			'id',
			'state',
			'date',
			'expectedValidator' => [
				'id',
				'lastName',
				'firstName'
			]
		]
	];

	public function api_get() {

		$searchAccess = new HaveSearchAccess;
		$extractAccess = $searchAccess
			->and_( new HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_ACTIVITIES_EXPENSES) )
			->isSatisfiedBy($this->requestAccess);

		$this->checkAccessWithSpec( $searchAccess );

		$filter = new Filters\SearchAbsencesReports();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		if( $extractAccess && false){
			throw new \Exception('TODO'); //TODO
			$extractRequest = new Filters\Extract\AbsencesReports();
			$extractRequest->setAndFilterData($this->requestAccess->getParams());
			if($extractRequest->isValid() && $this->requestAccess->get('format') == 'csv'){
				$service = new Services\Extraction\AbsencesReports(
					'absences-reports.csv',
					$extractRequest->encoding->getValue(),
					$extractRequest->fullExtract->getValue()
				);
				$service->extract($filter);
				return;
			}
		}

		$result = Services\AbsencesReports::search($filter);

		foreach($result->rows as $entity){
			/** @var AbsencesReport $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post(){
		$this->checkAccessWithSpec(new HaveCreateAccess());

		$filter = new Filters\SaveEntity();
		$filter->setData( $this->requestAccess->get('data') );

		$this->checkFilter($filter);

		$entity = Services\AbsencesReports::buildFromFilter($filter);

		if(Services\AbsencesReports::create($entity)) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
