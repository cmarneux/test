<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\AbsencesReports;

class Rights extends AbstractController{
	public function api_get(){
		$timesReport = AbsencesReports::get($this->requestAccess->id);
		if(!$timesReport) $this->error(404);

		$this->sendJSONResponse([
			'data' => AbsencesReports::getRights($timesReport)
		]);
	}
}
