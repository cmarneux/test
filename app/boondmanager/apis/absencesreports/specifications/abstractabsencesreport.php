<?php
/**
 * abstractabsencesreport.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\AbsencesReports\Specifications;

use BoondManager\Lib\Specifications\AbstractActivity;
use BoondManager\Models\AbsencesReport;
use BoondManager\Lib\RequestAccess;

abstract class AbstractAbsencesReport extends AbstractActivity {
	/**
	 * get the absencesreport from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\AbsencesReport|null
	 */
	public function getAbsencesReport($request){
		if($request->data instanceof AbsencesReport) return $request->data;
		else return null;
	}
}
