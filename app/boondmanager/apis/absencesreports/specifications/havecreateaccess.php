<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Indicate if the user have the right to create an absenceReport
 * @package BoondManager\Models\Specifications\User
 */
class HaveCreateAccess extends AbstractAbsencesReport{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 * //TODO : rediger les vrais tests (pour le moment une copie de expensereports mais ne semble pas suffisants)
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->user;

		if($user->isGod()) return true;

		// TODO: Implement isSatisfiedBy() method. from Loic's spec
		if(!$user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES)) return false;

		return true;
	}
}
