<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class CanWriteExpensesReport
 *
 * Indicate if the user have the right to write into absenceReport
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractAbsencesReport
{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the positioning is a wrong type
	 * @return bool
	 * //TODO : rediger les vrais tests (pour le moment une copie de expensereports mais ne semble pas suffisants)
	 */
	public function isSatisfiedBy($request)
	{
		$user    = $request->user;
		$entity = $this->getAbsencesReport($request);
		if (!$entity) return false;

		if ($user->isGod()) return true;
		if(!$user->isManager()) return false;

		//~ TODO : Tin : Adapter les tests supplémentaires pour chaque onglets suivant les specs de Loïc
		return $user->hasRight(BM::RIGHT_DELETION, BM::MODULE_ACTIVITIES_EXPENSES);
	}
}
