<?php
/**
 * UserHaveWriteAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\AbsencesReports\Specifications;

use BoondManager\Lib\RequestAccess;

/**
 * Class CanWriteExpensesReport
 *
 * Indicate if the user have the right to write into absenceReport
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveWriteAccess extends AbstractAbsencesReport{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 * //TODO : rediger les vrais tests (pour le moment une copie de expensereports mais ne semble pas suffisants)
	 */
	public function isSatisfiedBy($request){

		$user = $request->getUser();
		$entity = $this->getAbsencesReport($request);
		if(!$entity) return false;

		if($user->isGod()) return true;
		if (! (new HaveReadAccess())->isSatisfiedBy($request)) return false;

		return $this->canSaveActivity($entity, $user);
	}
}
