<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\AbsencesReports\Specifications;

use BoondManager\Lib\RequestAccess;

class IsActionAllowed extends AbstractAbsencesReport {

	const RIGHTS_ACTIONS = ['share', 'validate', 'reject', 'unvalidate', 'downloadInternalPDF'];

	private $action;

	public function __construct($action)
	{
		if(!in_array($action, self::RIGHTS_ACTIONS)) throw new \Exception("unknow action '$action'");
		$this->action = $action;
	}

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->getUser();
		$entity = $this->getAbsencesReport($request);
		if(!$entity) return false;

		if($user->isGod()) return true;

		// nécéssite à minima un accès lecture
		if ( !(new HaveReadAccess())->isSatisfiedBy($request) ) return false;

		switch ($this->action) {
			case 'share' :
				return !$user->isEmployee();
			case 'validate' :
				return $this->canValidate($entity, $user);
			case 'reject' :
				return $this->canReject($entity, $user);
			case 'unvalidate' :
				return $this->canDevalidate($entity, $user);
			case 'downloadInternalPDF' :
				return $this->canAllowPDFPrj($entity, $user);
			default :
				return false;
		}
	}
}
