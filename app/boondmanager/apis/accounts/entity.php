<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts;

use BoondManager\APIs\Accounts\Specifications\HaveCreateAccess;
use BoondManager\APIs\Accounts\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Accounts\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Accounts;
use BoondManager\Services\Devices;

/**
 * Class Entity
 * @package BoondManager\APIs\Accounts
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'firstName',
		'lastName',
		'email1',
		'typeOf',
		'login',
		'dashboardGraphsAvailable',
		'logInOnlyFromThirdParty',
		'warningForNewDevice',
		'manageAllowedDevices',
		'modules',
		'rights',
		'apps' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		],
		'role' => [
			'id',
			'name'
		],
		'otherAgencies' => [
			'id',
			'name'
		],
		'otherPoles' => [
			'id',
			'name'
		],
		'devices' => [
			'id',
			'isManaged',
			'isSessionExists',
			'name',
			'browser',
			'authorizationDate',
			'lastLogInDate',
			'internetProtocol'
		],
		'advancedAppCertification' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		],
		'advancedAppCalendar' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		],
		'advancedAppMail' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		],
		'advancedAppViewer' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		],
		'advancedAppEmailing' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		],
		'advancedAppTemplates' => [
			'id',
			'name',
			'title',
			'description',
			'category',
			'visibility'
		]
	];

	const ALLOWED_FIELDS_DEFAULT = [
		'id',
		'country',
		'currency',
		'exchangeRate',
		'calendar'
	];

	/**
	 * Get account's basic data
	 */
	public function api_get() {
		if($this->requestAccess->id) {
			$account = Accounts::get($this->requestAccess->id);
			if(!$account) $this->error(404);

			$account->devices = Devices::getAllDevicesFromUser($account->userId);

			$this->checkAccessWithSpec(new HaveReadAccess(), $account);

			$ALLOWED_FIELDS = self::ALLOWED_FIELDS;
		} else {
			//api/agency/default
			$this->checkAccessWithSpec( new HaveCreateAccess );

			$account = Accounts::getNew();
			$ALLOWED_FIELDS = self::ALLOWED_FIELDS_DEFAULT;
		}

		$this->sendJSONResponse([
			'data' => $account->filterFields($ALLOWED_FIELDS)
		]);
	}

	/**
	 * Delete an account
	 */
	public function api_delete() {
		$account = Accounts::get($this->requestAccess->id);
		if(!$account) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $account);

		if(Accounts::delete($account->getID()))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}

	/**
	 * Update account's information data
	 */
	public function api_put() {
		$account = Accounts::get($this->requestAccess->id);
		if(!$account) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $account);

		//Build filters
		$filter = new Filters\Entity($account);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build agency
		Accounts::buildFromFilter($filter, $account);

		if(Accounts::update($account))

			$this->sendJSONResponse([
				'data' => $this->getJSONData($account)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
