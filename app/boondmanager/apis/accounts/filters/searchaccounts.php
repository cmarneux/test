<?php
/**
 * projects.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;

/**
 * Class SearchAccounts
 * @property InputMultiDict resourceTypes
 * @property InputMultiDict resourceStates
 * @property InputMultiEnum userTypes
 * @property InputMultiEnum userSubscriptions
 * @property InputMultiEnum sort
 * @package BoondManager\APIs\Accounts\Filters
 */
class SearchAccounts extends AbstractSearch {
	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_AGENCY_NAME = 'agency.name',
		ORDERBY_LOGIN = 'login',
		ORDERBY_STATE = 'state',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName',
		ORDERBY_RESOURCE_TYPEOF = 'resource.typeOf',
		ORDERBY_ROLE_NAME = 'role.name';
	/**#@-*/

	public function __construct()
	{
		parent::__construct();

		$input = new InputMultiDict('resourceTypes', 'specific.setting.typeOf.resource');
		$this->addInput($input);

		$input = new InputMultiDict('resourceStates', 'specific.setting.state.resource');
		$this->addInput($input);

		$input = new InputMultiEnum('userTypes');
		$input->setAllowedValues([
			BM::USER_TYPE_RESOURCE, BM::USER_TYPE_MANAGER
		]);
		$this->addInput($input);

		$input = new InputMultiEnum('userSubscriptions');
		$input->setAllowedValues([
			BM::USER_ACCESS_ACTIVE,
			BM::USER_ACCESS_INACTIVE
		]);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_AGENCY_NAME,
			self::ORDERBY_LOGIN,
			self::ORDERBY_STATE,
			self::ORDERBY_MAINMANAGER_LASTNAME,
			self::ORDERBY_RESOURCE_TYPEOF,
			self::ORDERBY_ROLE_NAME
		]);
		$this->addInput($input);
	}
}
