<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts;

use BoondManager\APIs\Accounts\Specifications\HaveCreateAccess;
use BoondManager\APIs\Accounts\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Accounts;
use BoondManager\APIs\Accounts\Filters;
use Wish\Models\Model;

/**
 * Class Index
 * @package BoondManager\APIs\Accounts
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'firstName',
		'lastName',
		'typeOf',
		'level',
		'subscription',
		'login',
		'mainManager' => [
			'id',
			'firstName',
			'lastName'
		],
		'agency' => [
			'id',
			'name'
		],
		'role' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		]
	];

	/**
	 * Search accounts
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$filter = new Filters\SearchAccounts();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = Accounts::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create an account
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default agency
		$account = Accounts::buildFromFilter($filter);
		if(!$account) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $account);

		if(Accounts::create($account)) {
			$apiAccount = new Entity();
			$this->sendJSONResponse([
				'data' => $apiAccount->getJSONData($account)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
