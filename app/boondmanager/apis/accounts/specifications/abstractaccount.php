<?php
/**
 * abstractaccount.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts\Specifications;

use BoondManager\Models\Account;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;

abstract class AbstractAccount extends AbstractSpecificationItem {
	/**
	 * Get the user from the request
	 * @param RequestAccess $request
	 * @return Account|null
	 */
	public function getAccount($request) {
		if($request->data instanceof Account) return $request->data;
		else return null;
	}
}
