<?php
/**
 * HaveDeleteAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveDeleteAccess
 * @package BoondManager\APIs\Accounts\Specifications
 */
class HaveDeleteAccess extends AbstractAccount {
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		$account = $this->getAccount($request);

		//Check this API authorization & access
		return $user->isAdministrator() && $account->isManager();
	}
}
