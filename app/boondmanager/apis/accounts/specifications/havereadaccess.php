<?php
/**
 * HaveReadAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveReadAccess
 * @package BoondManager\APIs\Accounts\Specifications
 */
class HaveReadAccess extends AbstractAccount {

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		$account = $this->getAccount($request);

		//Check this API authorization & access
		return $user->isAdministrator() && $account->isManager();
	}
}
