<?php
/**
 * HaveSearchAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Accounts\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;

/**
 * Class HaveSearchAccess
 * @package BoondManager\APIs\Accounts\Specifications
 */
class HaveSearchAccess extends AbstractSpecificationItem{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		//Check this API authorization & access
		return $user->isAdministrator();
	}
}
