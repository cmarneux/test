<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Actions;

use BoondManager\APIs\Actions\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use Wish\Models\Model;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$project = null;

		$project = Services\Actions::get($this->requestAccess->id);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(), $project);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($project);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
