<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Actions;

use BoondManager\APIs\Actions\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Contact;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\Models\Candidate;
use BoondManager\OldModels\Filters;

class Entity extends AbstractController {

	const ALLOWED_FIELDS = [
		'creationDate',
		'startDate',
		'endDate',
		'typeOf',
		'title',
		'text',
		'priority',
		'state',
		'place',
		'guests',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'files' => [
			'id',
			'name'
		],
		'dependsOn' => [
			'id',
			// candidat & resources & contact
			'lastName',
			'firstName',
			'phone1',
			// project, opportunity, order, bill, purchase, contact
			'company' => [
				'id',
				'name'
			],
			// project
			'contact' => [
				'id',
				'lastName',
				'firstName',
				'phone1'
			],
			// product
			'name',
			'reference',
			'title',
		]
	];

	public function api_get() {
		// récupération d'une action 0 vierge pour la création
		if(!$this->requestAccess->id) {
			$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);
			/*
			$parentFilter = new Filters\ParentEntity([
				BM::CATEGORY_CANDIDATE, BM::CATEGORY_RESOURCE, BM::CATEGORY_CRM_CONTACT, BM::CATEGORY_CRM_COMPANY
			]) ;
			$parentFilter->setData($this->requestAccess->getParams());
			$this->checkFilter($parentFilter);

			$parent = $parentFilter->parent;
			$category = $parentFilter->category;

			$action = Services\Actions::buildAction($category);
			$action->ID_PARENT = $parent;
			*/
		} else {
			$action = Services\Actions::get($this->requestAccess->id);
			if(!$action) $this->error(404);
		}

		$this->checkAccessWithSpec(new HaveReadAccess(), $action);

		$this->sendJSONResponse([
			'data' => $action->filterFields(self::ALLOWED_FIELDS)
		]);

		return;
	}

	public function api_put() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);

		$action = Services\Actions::get($this->requestAccess->id);
		if(!$action) $this->error(404);

		$parent   = $action->ID_PARENT;

		// vérification des droits
		switch($action->category) {
			case BM::CATEGORY_CANDIDATE:
				$entity = Services\Candidates::get($parent, Candidate::TAB_DEFAULT);
				$writeSpec = new Specifications\HaveWriteAccess(Candidate::TAB_ACTIONS);
				break;
			case BM::CATEGORY_RESOURCE:
				$entity = Services\Employees::get($parent, Employee::TAB_DEFAULT);
				$writeSpec = new Specifications\HaveWriteAccess(Employee::TAB_ACTIONS);
				break;
			case BM::CATEGORY_CRM_CONTACT:
				$entity = Services\Employees::get($parent, Contact::TAB_DEFAULT);
				$writeSpec = new Specifications\HaveWriteAccess(Contact::TAB_ACTIONS);
				break;
			case BM::CATEGORY_OPPORTUNITY:
				$entity = Services\Opportunities::get($parent, Opportunity::TAB_DEFAULT);
				$writeSpec = new \BoondManager\APIs\Opportunities\Specifications\HaveWriteAccess(Opportunity::TAB_ACTIONS);
				break;
			default:
				$this->error(409, 'unable to find action category ('.$action->category.')');
				break;
		}
		if(!$entity) {
			$this->error(409, "Unable to find parent (parent:{$parent}");
		} else {
			$this->checkAccessWithSpec($writeSpec, $entity);
		}

		// filter + check + update
		$filter = new Filters\Profiles\Actions\Save($action->category);
		$filter->setDefaultValue($action->encode());
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		Services\Actions::updateAction($action, $filter);

		$answer = [
			'data' => $action
		];

		$this->sendJSONResponse($answer);
	}
}
