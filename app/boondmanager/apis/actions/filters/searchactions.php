<?php
/**
 * actions.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Actions\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Lib\Filters\Inputs\ActionStates;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;

use BoondManager\Databases\Local\Action as DBActions;
use BoondManager\Services;
use BoondManager\Services\CurrentUser;

/**
 * Class Resources
 * @property-read InputMultiValues $actionTypes filter for ACTION_TYPE
 * @property-read ActionStates $states filter for COMP.PROFIL_ETAT
 * @property-read \Wish\Filters\Inputs\InputMultiValues $origins
 * @property-read \Wish\Filters\Inputs\InputValue $sort
 * @property-read \Wish\Filters\Inputs\InputBoolean $visibleProfile
 * @property-read \Wish\Filters\Inputs\InputValue $period
 * @property-read InputDate $startDate
 * @property-read InputDate $endDate
 * @property-read InputBoolean $myApps
 * @property-read \Wish\Filters\Inputs\InputValue $user
 * @property-read InputValue $candidate
 * @package BoondManager\Models\Filters\Search
 */
class SearchActions extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_ACTIONS;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CUSTOM = 0,
		PERIOD_TODAY = 1,
		PERIOD_WEEK = 2,
		PERIOD_MONTH = 3;
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_DATE = 'ACTION_DATE',
		ORDERBY_TYPE = 'ACTION_TYPE',
		ORDERBY_REFERENCE = 'RAPPEL_NOM',
		ORDERBY_EMAIL = 'RAPPEL_EMAIL',
		ORDERBY_ACTION = 'ACTION_TEXTE',
		ORDERBY_RESP = 'RESP_NOM';
	/**#@-*/

	/**
	 * the categories selected
	 * @var array
	 */
	private $selectedCategories = [];

	/**
	 * the categories available
	 * @var array
	 */
	private $availableCategories = [];

	/**
	* Resources constructor.
	* @param array $categories all categoriesID accessible for the user
	*/
	public function __construct($categories = null)
	{
		parent::__construct();

		if(is_null($categories))
			$categories = Services\Actions::getUserCategories();

		$this->availableCategories = $categories;

		//profile visibility
		$visibleProfile = new InputBoolean('visibleProfile');
		$visibleProfile->setModeDefaultValue(true);
		$this->addInput($visibleProfile);

		$type = new InputMultiValues('actionTypes');
		$typesIds = Services\Actions::getTypesIds($categories);
		$type->setModeDefaultValue($typesIds);
		//$type->addFilterInArray($typesIds);
		$type->setAllowEmptyValue(false);
		$this->addInput($type);
		$this->resetActionTypesFilter($categories);

		$states = new ActionStates('states', $categories);

		$from = new InputDict('origins', 'specific.setting.origin');

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_ACTION, self::ORDERBY_DATE, self::ORDERBY_EMAIL, self::ORDERBY_REFERENCE, self::ORDERBY_RESP, self::ORDERBY_TYPE
		]);

		$start = new InputDate('startDate');
		$end = new InputDate('endDate');

		$period = new InputEnum('period');
		$period->setModeDefaultValue(BM::INDIFFERENT);
		$period->setAllowedValues([
			BM::INDIFFERENT, self::PERIOD_CUSTOM, self::PERIOD_TODAY, self::PERIOD_WEEK, self::PERIOD_MONTH
		]);

		$myApps = new InputBoolean('myApps');

		$user = new InputValue('user');
		$user->addFilter(FILTER_VALIDATE_INT);

		$candidate = new InputValue('candidate');
		$candidate->addFilter(FILTER_VALIDATE_INT);

		// $category doit etre valide avant $type et $state
		$this->addInput([$states, $period, $from, $start, $end, $sort, $myApps, $user, $candidate]);
	}

	protected function postValidation(){
		parent::postValidation();

		if(!CurrentUser::instance()->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE))
			$this->visibleProfile->setDisabled(true);

		switch ($this->period->getValue()){
			case self::PERIOD_TODAY:
				$this->startDate->setValue(date('Y-m-d'))->filter();
				$this->endDate->setValue(date('Y-m-d'))->filter();
				break;
			case self::PERIOD_WEEK:
				$this->startDate->setValue(date('Y-m-d', mktime(0,0,0,date('n'),date('j')-date('w')+1,date('Y'))))->filter();
				$this->endDate->setValue(date('Y-m-d', mktime(0,0,0,date('n'),date('j')-date('w')+7,date('Y'))))->filter();
				break;
			case self::PERIOD_MONTH:
				$this->startDate->setValue(date('Y-m-d', mktime(0,0,0,date('n'),1,date('Y'))))->filter();
				$this->endDate->setValue(date('Y-m-d', mktime(0,0,0,date('n')+1,0,date('Y'))))->filter();
				break;
		}
	}

	private function resetActionTypesFilter($categories)
	{
		$dictTypes = Services\Actions::getTypes( $categories );
		$this->actionTypes->resetFilters();
		$this->actionTypes->addFilterInDict($dictTypes);
	}

	/**
	* get a list of categories id selected in the type
	* @return array
	*/
	public function getSelectedCategories(){
		if(!$this->selectedCategories && $this->actionTypes->isDefined()){
			foreach($this->actionTypes->getValue() as $value){
				$this->addSelectedCategory( Services\Actions::getCategoryFromType($value) );
			}
		}
		if($this->selectedCategories) return $this->selectedCategories;
		else return $this->availableCategories;
	}

	/**
	* map a category ID with a note type (DB Value)
	* @param $categoriesID
	* @return mixed
	*/
	private static function mapNoteValueForCategories($categoriesID){
		return Tools::mapData($categoriesID, [
			BM::CATEGORY_OPPORTUNITY => DBActions::ACTION_TYPE_OPPORTUNITY_NOTE,
			BM::CATEGORY_CRM_CONTACT => DBActions::ACTION_TYPE_CRM_CONTACT_NOTE,
			BM::CATEGORY_CANDIDATE => DBActions::ACTION_TYPE_CANDIDATE_NOTE,
			BM::CATEGORY_PROJECT => DBActions::ACTION_TYPE_PROJECT_NOTE,
			BM::CATEGORY_BILLING => DBActions::ACTION_TYPE_BILLING_NOTE,
		]);
	}

	/**
	* add a selected category
	* @param string $topKey
	*/
	private function addSelectedCategory($topKey){
		if(!in_array($topKey, $this->selectedCategories)) $this->selectedCategories[] = $topKey;
	}

	/**
	 * reset categories
	 * @param array $categories
	 * @return $this
	 */
	public function setAvailableCategories(array $categories){
		$this->resetActionTypesFilter($categories);
		$this->availableCategories = $categories;
		return $this;
	}

	/**
	 * get categories ID used to build the filter
	 * @return array
	 */
	public function getAvailableCategories(){
		return $this->availableCategories;
	}

	/**
	 * generate a filter adapted for the user
	 * @return SearchActions
	 * @throws \Exception
	 * @deprecated
	 */
	public static function getUserFilter()
	{

		$filter = new self();
		$user = CurrentUser::instance();

		$filter->visibleProfile->setDefaultValue(!$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE))
		                       ->addFilterInArray($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) ? [0,1]:[1]);

		return $filter;
	}

	/**
	 * @return $this
	 * @deprecated
	 */
	public function setDefaultTypesToReminder(){
		$this->actionTypes->setDefaultValue([3, 1, 5, 51, 31, 21, 71]);
		return $this;
	}

	/**
	 * @return $this
	 * @deprecated
	 */
	public function setDefaultTypesToAllExceptNotifications(){
		$default = [];
		foreach($this->availableCategories as $cat){
			$default = array_merge($default, Services\Actions::getCustomTypes($cat));
		}
		$this->actionTypes->setDefaultValue( array_column($default, 'id'));
		return $this;
	}

	/**
	 * @return $this
	 * @deprecated
	 */
	public function setDefaultValuesToAllNotifications(){
		$default = [];
		foreach($this->availableCategories as $cat)
			$default[] = Services\Actions::getNotificationTypeForCategory($cat);

		$this->actionTypes->setDefaultValue( $default );
		return $this;
	}

	public function getActionTypesForCat($cat){
		$types = [];
		$vals = $this->actionTypes->getValue();
		foreach($vals as $v){
			if(Services\Actions::getCategoryFromType($v) == $cat)
				$types[] = $v;
		}
		return $types;
	}
}

