<?php
/**
 * actions.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\APIs\Actions;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Action;
use BoondManager\Services\BM;
use BoondManager\Models\Candidate;
use BoondManager\Models\Contact;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Actions\Specifications\HaveSearchAccess;
use BoondManager\APIs\Candidates;
use BoondManager\APIs\Employees;
use BoondManager\APIs\Contacts;
use BoondManager\OldModels\Specifications\RequestAccess\Opportunities;

/**
 * Actions list controller.
 * @package Actions
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		// attention, saleté :p
		'dependsOn' => [
			'id',
			// type profil
			'lastName',
			'firstName',
			'email1',
			'phone1',
			'phone2',
			// pour les contacts
			'company' => [
				'id',
				'name',
				'phone1'
			],
			// type produit,
			'name',
			'reference',
			// besoin
			'title',
			// pour les relations vers un autre contacts
			'contact' => [
				'id',
				'lastName',
				'firstName',
				'email1',
				'phone1',
				'phone2',
				'company' => [
					'id',
					'name',
					'phone1'
				]
			],
			// bdc
			'number',
			'project' => [
				'id',
			],
			'order' => [
				'id',
				'number'
			]
		]
	];

	public function api_get() {

		$this->checkAccessWithSpec( new HaveSearchAccess() );

		$filter = new Filters\SearchActions();
		$filter->setData($this->requestAccess->getParams());
		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		foreach($result->rows as $entity){
			/** @var Action $entity */
			Services\Actions::attachRights($entity);
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				//'categories' => Services\Actions::getUserCategories(),
				'totals' => [
					'rows' => $result->total,
				],
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {

		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);

		$filter = new Filters\Profiles\Actions\Save();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		$parent   = $filter->dependsOn->getValue();

		// vérification des droits
		switch($filter->getCategory()) {
			case BM::CATEGORY_CANDIDATE:
				$entity = Services\Candidates::get($parent, Candidate::TAB_DEFAULT);
				$writeSpec = new Candidates\Specifications\HaveWriteAccess(Candidate::TAB_ACTIONS);
				break;
			case BM::CATEGORY_RESOURCE:
				$entity = Services\Employees::get($parent, Employee::TAB_DEFAULT);
				$writeSpec = new Employees\Specifications\HaveWriteAccess(Employee::TAB_ACTIONS);
				break;
			case BM::CATEGORY_CRM_CONTACT:
				$entity = Services\Employees::get($parent, Contact::TAB_DEFAULT);
				$writeSpec = new Contacts\Specifications\HaveWriteAccess(Contact::TAB_ACTIONS);
				break;
			case BM::CATEGORY_OPPORTUNITY:
				$entity = Services\Opportunities::get($parent, Opportunity::TAB_DEFAULT);
				$writeSpec = new \BoondManager\APIs\Opportunities\Specifications\HaveWriteAccess(Opportunity::TAB_ACTIONS);
				break;
			default:
				$this->error(404);
				break;
		}

		if(!$entity) {
			$this->error(409, "Unable to find parent (parent:$parent)");
		} else {
			$this->checkAccessWithSpec($writeSpec, $entity);
		}

		$action = Services\Actions::createAction($filter);

		$answer = [
			'data' => $action
		];

		$this->sendJSONResponse($answer);
	}
}
