<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Actions;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Actions;

/**
 * Class Rights
 * @package BoondManager\APIs\Inactivities
 */
class Rights extends AbstractController {
	/**
	 * Get action's rights
	 */
	public function api_get() {
		$action = Actions::get($this->requestAccess->id);
		if(!$action) $this->error(404);

		$this->sendJSONResponse([
			'data' => Actions::getRights($action)
		]);
	}
}
