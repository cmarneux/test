<?php
/**
 * abstractaction.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Actions\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Action;
use Wish\Specifications\AbstractSpecificationItem;

abstract class AbstractAction extends AbstractSpecificationItem {
	/**
	 * @param RequestAccess $request
	 * @return Action
	 * @throws \Exception
	 */
	protected function getAction($request){
		/** @var Action $data */
		$data = $request->data;
		if(!$data instanceof Action) throw new \Exception('request access doesn\'t contains an action');
		return $data;
	}
}
