<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Actions\Specifications;

use BoondManager\APIs;
use BoondManager\Models\Action;
use BoondManager\Models\Candidate;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Contact;
use BoondManager\Models\Employee;
use BoondManager\Models\Invoice;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Order;
use BoondManager\Models\Project;
use BoondManager\Services\BM;
use Wish\Tools;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on actions
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Actions
 */
class HaveReadAccess extends AbstractAction {

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;

		if($user->isGod()) return true;

		/** @var Action $action */
		$action = $this->getAction($request);

		if($action->isNotification) return false;

		switch (get_class($action->dependsOn)) {
			case Candidate::class: return $this->canReadCandidate($request); break;
			case Employee::class: return $this->canReadEmployee($request); break;
			case Contact::class: return $this->canReadCRMContact($request); break;
			case Project::class: return $this->canReadProject($request); break;
			case Order::class: return $this->canReadOrder($request); break;
			case Invoice::class: return $this->canReadInvoice($request); break;
			case Opportunity::class: return $this->canReadOpportunity($request); break;
			default : throw new \Exception('class '.get_class($action->dependsOn).' not handled in action\'s read spec');
		}

		return false;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadCandidate($request) {
		$user = $request->user;

		/** @var Candidate $candidate */
		$candidate = $this->getAction($request)->dependsOn;

		if($candidate->isVisible() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)) {
			$hierarchyAccess = $user->checkHierarchyAccess($candidate, BM::MODULE_CANDIDATES);
			return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS)
				|| in_array($hierarchyAccess, [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY])
				|| BM::PROFIL_ACCESS_READ_ONLY && $user->hasAccess(BM::MODULE_CANDIDATES, BM::MODULE_CANDIDATES_ACTIONS);
		}else return false;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadCRMContact($request) {
		$user = $request->user;

		/** @var Contact $contact */
		$contact = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($contact, BM::MODULE_CONTACTS, '', [], Tools::getFieldsToArray($contact->influencers, 'id'));

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || in_array($hierarchyAccess, [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY]);
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadEmployee($request) {
		$user = $request->user;

		/** @var Employee $employee */
		$employee = $this->getAction($request)->dependsOn;

		if($employee->isVisible() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)) {
			$hierarchyAccess = $user->checkHierarchyAccess($employee, BM::MODULE_RESOURCES);
			$isUserProfile = $user->getEmployeeId() == $employee->id;
			return $hierarchyAccess == BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY
				|| ( $hierarchyAccess == BM::PROFIL_ACCESS_READ_WRITE || $user->hasAccess(BM::MODULE_RESOURCES, BM::MODULE_RESOURCES_ACTIONS) )
				&& (
					$isUserProfile && $employee->isTopManager()
					|| !$isUserProfile
					&& ($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
						|| !$employee->isManager()
						|| $user->isMyManager($employee->id)
					)
				);
		}else return false;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadProject($request) {

		$action = $this->getAction($request);
		$user = $request->user;

		/** @var Project $project */
		$project = $action->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($project, BM::MODULE_PROJECTS);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_PROJECTS)
			|| in_array($hierarchyAccess, [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY])
			|| $action->isCollaborative() && $user->getEmployeeId() == $action->mainManager->id; // cas de la gestion de projet
	}


	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadOpportunity($request) {
		$user = $request->user;

		/** @var Opportunity $opportunity */
		$opportunity = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($opportunity, BM::MODULE_OPPORTUNITIES);

		if($opportunity->isVisible() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)) {
			$user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_OPPORTUNITIES) || in_array($hierarchyAccess, [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY]);
		}else
			return false;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadOrder($request) {
		$user = $request->user;

		/** @var Project $order */
		$order = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($order, BM::MODULE_BILLING);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_BILLING) || in_array($hierarchyAccess, [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY]);
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canReadInvoice($request) {
		$user = $request->user;

		/** @var Project $invoice */
		$invoice = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($invoice, BM::MODULE_BILLING);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_BILLING) || in_array($hierarchyAccess, [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY]);
	}
}
