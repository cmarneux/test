<?php
/**
 * UserHaveSearchAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Actions\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on actions
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Actions
 */
class HaveSearchAccess extends AbstractSpecificationItem{

	/**
	* check if the object match the specification
	* @param RequestAccess $request
	* @return bool
	*/
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;

		return $user->isManager() && $user->hasAccess(BM::MODULE_ACTIONS);
	}
}
