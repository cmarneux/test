<?php
/**
 * havewriteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Actions\Specifications;

use BoondManager\APIs;
use BoondManager\Models\Action;
use BoondManager\Models\Candidate;
use BoondManager\Models\Contact;
use BoondManager\Models\Employee;
use BoondManager\Models\Invoice;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Order;
use BoondManager\Models\Project;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use Wish\Tools;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on actions
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Actions
 */
class HaveWriteAccess extends AbstractAction {

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;

		if($user->isGod()) return true;

		/** @var Action $action */
		$action = $this->getAction($request);

		if($action->isNotification) return false;

		// check read access first
		if(!(new HaveReadAccess())->isSatisfiedBy($request)) return false;

		switch (get_class($action->dependsOn)) {
			case Candidate::class: return $this->canWriteCandidate($request); break;
			case Employee::class: return $this->canWriteEmployee($request); break;
			case Contact::class: return $this->canWriteCRMContact($request); break;
			case Project::class: return $this->canWriteProject($request); break;
			case Order::class: return $this->canWriteOrder($request); break;
			case Invoice::class: return $this->canWriteInvoice($request); break;
			case Opportunity::class: return $this->canWriteOpportunity($request); break;
			default : throw new \Exception('class '.get_class($action->dependsOn).' not handled in action\'s read spec');
		}

		return false;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteCandidate($request) {
		$user = $request->user;

		/** @var Candidate $candidate */
		$candidate = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($candidate, BM::MODULE_CANDIDATES);
		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess == BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteCRMContact($request) {
		$user = $request->user;

		/** @var Contact $contact */
		$contact = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($contact, BM::MODULE_CONTACTS, '', [], Tools::getFieldsToArray($contact->influencers, 'id'));

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess==BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteEmployee($request) {
		$user = $request->user;

		/** @var Employee $employee */
		$employee = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($employee, BM::MODULE_RESOURCES);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess==BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteProject($request) {

		$action = $this->getAction($request);
		$user = $request->user;

		/** @var Project $project */
		$project = $action->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($project, BM::MODULE_PROJECTS);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess==BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY || $action->isCollaborative() && $action->mainManager->id == $user->getEmployeeId();
	}


	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteOpportunity($request) {
		$user = $request->user;

		/** @var Opportunity $opportunity */
		$opportunity = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($opportunity, BM::MODULE_OPPORTUNITIES);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess==BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY ;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteOrder($request) {
		$user = $request->user;

		/** @var Project $order */
		$order = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($order, BM::MODULE_BILLING);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess==BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY ;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	private function canWriteInvoice($request) {
		$user = $request->user;

		/** @var Project $invoice */
		$invoice = $this->getAction($request)->dependsOn;

		$hierarchyAccess = $user->checkHierarchyAccess($invoice, BM::MODULE_BILLING);

		return $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_ACTIONS) || $hierarchyAccess==BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY ;
	}
}
