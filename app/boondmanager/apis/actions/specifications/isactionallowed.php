<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Actions\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;


/**
 * Class IsActionAllowed
 * @package BoondManager\APIs\Inactivities\Specifications
 */
class IsActionAllowed extends AbstractAction
{
	const AVAILABLE_ACTIONS = ['share'];

	private $action;

	/**
	 * IsActionAllowed constructor.
	 * @param string $action
	 */
	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		$action = $this->getAction($request);
		if(!$action) return false;

		$allow = false;
		switch($this->action) {
			case 'share':
				$allow = $user->isManager() && (new HaveReadAccess())->isSatisfiedBy($request);
				break;
		}
		return $allow;
	}
}
