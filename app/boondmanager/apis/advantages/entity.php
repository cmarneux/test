<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Advantages;

use BoondManager\Services\BM;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;
use Wish\Exceptions;

class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'date',
		'returnDate',
		'advantageType' => [
			'reference',
			'frequency',
			'category',
			'participationQuota',
			'agencyQuota',
			'employeeQuota'
		],
		'quantity',
		'participationAmount',
		'agencyAmount',
		'employeeAmount',
		'costPaid',
		'costCharged',
		'informationComments',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'resource' => [
			'id',
			'lastName',
			'firstName',
			'typeOf',
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			],
			'hrManager' => [
				'id',
				'lastName',
				'firstName'
			]
		],
		'contract' => [
			'id'
		],
		'project' => [
			'id',
			'reference'
		],
		'delivery' => [
			'id',
			'name'
		],
		'agency' => [
			'id',
			'name',
			'advantageTypes' => [
				'reference',
				'name',
				'frequency',
				'category',
				'participationQuota',
				'agencyQuota',
				'employeeQuota'
			]
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			$advantage = Services\Advantages::get($id);
			if(!$advantage) $this->error(404);
		} else {
			try {
				$advantage = Services\Advantages::newAdvantage(
					$this->requestAccess->get('resource'),
					$this->requestAccess->get('contract'),
					$this->requestAccess->get('project'),
					$this->requestAccess->get('delivery')
				);
			} catch(Exceptions\Custom $e) {
				$this->error( $e->getCode(), $e->getMessage() );
			}
		}

			// checking read access (if none found, throw an error)
		$this->checkAccessWithSpec(new HaveReadAccess(Employee::TAB_ADMINISTRATIVE), $advantage->resource);

		$this->sendJSONResponse([
			'data' => $advantage->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	public function api_put() {
		$advantage = Services\Advantages::get($this->requestAccess->id);
		if (!$advantage || !$advantage->resource) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_ADMINISTRATIVE), $advantage->resource);

		$filter = new Filters\SaveEntity($advantage);
		$filter->setValue($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		$advantage = Services\Advantages::buildFromFilter($filter, $advantage);

		if( Services\Advantages::update($advantage)){
			$this->sendJSONResponse([
				'data' => $advantage->filterFields(self::ALLOWED_FIELDS)
			]);
		} else {
			$this->error( BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY );
		}
	}

	public function api_delete() {
		$advantage = Services\Advantages::get($this->requestAccess->id);
		if (!$advantage) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_ADMINISTRATIVE), $advantage->resource);

		if ( Services\Advantages::delete($advantage) ){
			$this->sendJSONResponse([
				'success' => true
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
		}
	}
}
