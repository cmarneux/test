<?php
/**
 * advantage.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Advantages\Filters;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputRelationship;


/**
 * Class FindRelationships
 * @package BoondManager\Models\Filters\Profiles\Contracts
 * @property InputRelationship resource
 * @property InputRelationship contract
 * @property InputRelationship project
 * @property InputRelationship delivery
 */
class FindRelationships extends AbstractJsonAPI{

	public function __construct()
	{
		parent::__construct();

		$this->addInput( (new InputRelationship('resource'))->setRequired(true) );
		$this->addInput( (new InputRelationship('contract')) );
		$this->addInput( (new InputRelationship('project')) );
		$this->addInput( (new InputRelationship('delivery')) );
	}

	public function getResourceID(){
		return $this->resource->getValue();
	}

	public function getProjectID(){
		return $this->project->getValue();
	}

	public function getContractID(){
		return $this->contract->getValue();
	}

	public function getDeliveryID() {
		return $this->delivery->getValue();
	}
}

