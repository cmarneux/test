<?php
/**
 * Advantage.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Advantages\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\AdvantageContract;
use BoondManager\Lib\Filters\Inputs\Attributes\Currency;
use BoondManager\Lib\Filters\Inputs\Attributes\ExchangeRate;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\Contract;
use BoondManager\Lib\Filters\Inputs\Relationships\Delivery;
use BoondManager\Lib\Filters\Inputs\Relationships\Employee;
use BoondManager\Lib\Filters\Inputs\Relationships\Project;
use BoondManager\Models;
use BoondManager\Services;
use BoondManager\Services\CurrentUser;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\MySQL\Where;
use Wish\Tools;

/**
 * Class Advantage
 * @package BoondManager\Models\Filters\Profiles
 * @property InputDate date
 * @property AdvantageContract $advantageType
 * @property InputFloat quantity
 * @property InputString informationComments
 * @property InputValue currency
 * @property InputValue currencyAgency
 * @property InputFloat exchangeRate
 * @property InputFloat exchangeRateAgency
 * @property InputFloat participationAmount
 * @property InputFloat agencyAmount
 * @property InputFloat employeeAmount
 * @property InputFloat costCharged
 * @property InputRelationship resource
 * @property InputRelationship agency
 * @property InputRelationship contract
 * @property InputRelationship project
 * @property InputRelationship delivery
 */
class SaveEntity extends AbstractJsonAPI
{
	protected $_objectClass = Models\Advantage::class;

	/**
	 * @var Models\Advantage
	 */
	private $_advantage;

	public function __construct(Models\Advantage $advantage)
	{
		parent::__construct();

		$this->_advantage = $advantage;

		$this->addInput( new InputDate('date') );
		$this->addInput( new InputDate('returnDate') );

		$typeOf = new AdvantageContract('advantageType') ;
		$typeOf->setAllowedAdvantageTypes( $this->getAdvantagesTypes() );
		$this->addInput( $typeOf );

		$quantity = new InputFloat('quantity');
		$quantity->setMin(0);
		$this->addInput($quantity);

		$this->addInput( new InputString('informationComments') );

		$this->addInput( new Currency() );
		$this->addInput( new Currency('currencyAgency') );
		$this->addInput( new ExchangeRate() );
		$this->addInput( new ExchangeRate('exchangeRateAgency') );

		$this->addInput( new InputFloat('participationAmount') );
		$this->addInput( new InputFloat('agencyAmount') );
		$this->addInput( new InputFloat('employeeAmount') );

		$this->addInput( new Employee() );
		$this->addInput( new Contract() );
		$this->addInput( new Project() );

		$delivery = new Delivery();
		$delivery->addFilterCallback(function($value) {
			// loading contract information
			if($value->contract) $value->contract = Services\Contracts::get($value->contract->id);
			return $value;
		});
		$this->addInput( $delivery );
		$this->addInput( new Agency() );

		$this->isCreation() ? $this->adaptForCreation() : $this->adaptForEdition();
	}

	private function isCreation(){
		return !$this->_advantage->id;
	}

	private function adaptForCreation() {
		$this->agency->setRequired(true);
		$this->resource->setRequired(true);
		$this->advantageType->setRequired(true);
		$this->date->setRequired(true);

		$user = CurrentUser::instance()->getAccount();

		$this->quantity->setModeDefaultValue(1, $alwaysDefined = true);
		$this->currency->setModeDefaultValue( $user->currency , $alwaysDefined = true);
		$this->currencyAgency->setModeDefaultValue( $user->currency, $alwaysDefined = true);
		$this->exchangeRate->setModeDefaultValue(1, $alwaysDefined = true);
		$this->exchangeRateAgency->setModeDefaultValue(1, $alwaysDefined = true);
	}

	private function adaptForEdition() {
		$this->resource->setDisabled(true);
		$this->agency->setDisabled(true);
		$this->contract->setDisabled(true);
		$this->project->setDisabled(true);
		$this->delivery->setDisabled(true);
		$this->advantageType->setDisabled(true);
	}

	private function getProject() {
		return $this->project->isDefined() ? $this->project->getValue() : $this->_advantage->project;
	}

	private function getContract() {
		$contractItem = $this->contract->isDefined() ? $this->contract->getValue() : $this->_advantage->contract;

		if(!$contractItem) {
			$contractItem = $this->delivery->isDefined() ? $this->delivery->getValue()->contract : $this->_advantage->delivery->contract;
		}

		return $contractItem;
	}

	private function getResource() {
		return $this->resource->isDefined() ? $this->resource->getValue() : $this->_advantage->resource;
	}

	private function getAdvantageType() {
		return $this->advantageType->isDefined() ? $this->advantageType->getValue() : $this->_advantage->advantageType;
	}

	protected function postValidation()
	{
		$advantageType = $this->getAdvantageType();
		if($this->isCreation()){
			if( ($this->getProject() || $this->getContract()) && $advantageType->isContractual()){
				switch ($advantageType->category){
					case Models\AdvantageType::CATEGORY_FIXEDAMOUNT:
						$this->employeeAmount->setModeDefaultValue( $advantageType->employeeQuota, $markAsDefined = true );
						//$this->agencyAmount->setValue( $advantageType->employeeQuota * ($advantageType->agencyQuota - 1));
						break;
					case Models\AdvantageType::CATEGORY_VARIABLESALARYBASIS:
						if($this->getResource()->isExternalConsultant()) {
							$this->employeeAmount->setValue(0);
							$this->agencyAmount->setValue(0);
							$this->participationAmount->setValue(0);
						}else{
							$contract      = $this->getContract();
							$monthlySalary = $contract ? $contract->monthlySalary : 0;
							$this->employeeAmount->setValue($advantageType->employeeQuota * $monthlySalary / 100);
							$this->agencyAmount->setValue($advantageType->agencyQuota * $monthlySalary / 100);
							$this->participationAmount->setValue($advantageType->participationQuota * $monthlySalary / 100);
						}
						break;
					case Models\AdvantageType::CATEGORY_PACKAGE:
						$this->employeeAmount->setModeDefaultValue( $advantageType->employeeQuota ,  $markAsDefined = true );
						$this->agencyAmount->setModeDefaultValue( $advantageType->agencyQuota ,  $markAsDefined = true );
						$this->participationAmount->setModeDefaultValue( $advantageType->participationQuota ,  $markAsDefined = true );
						break;
				}
			}
		}

		switch($this->getAdvantageType()->category ){
			case Models\AdvantageType::CATEGORY_FIXEDAMOUNT:
				// overwriting agency amount
				$this->agencyAmount->setValue( $this->getEmployeeAmount() * ($this->advantageType->agencyQuota - 1));
				$this->participationAmount->setDisabled(true);
				break;
			case Models\AdvantageType::CATEGORY_PACKAGE: //Forfait
				// pas de traitement, on prend tout
				break;
			case Models\AdvantageType::CATEGORY_VARIABLESALARYBASIS:
				if  (!$this->isCreation() || !(($this->getProject() || $this->getContract()) && $this->getAdvantageType()->isContractual())) {
					$this->agencyAmount->setDisabled(true);
					$this->employeeAmount->setDisabled(true);
					$this->participationAmount->setDisabled(true);
				}
				break;
			default:
				$this->agencyAmount->setDisabled(true);
				$this->employeeAmount->setDisabled(true);
				$this->participationAmount->setDisabled(true);
		}
	}

	private function getEmployeeAmount() {
		return $this->employeeAmount->isDefined() ? $this->employeeAmount->getValue() : ($this->isCreation() ? 0 : $this->_advantage->employeeAmount);
	}

	/**
	 * @return Models\AdvantageType[]
	 */
	public function getAdvantagesTypes()
	{
		if ($this->_advantage->contract) {
			$advantages = Tools::useColumnAsKey('reference', $this->_advantage->contract->advantages, $castString = true);
			$advantages = array_filter($advantages, function ($value) {
				/** @var Models\AdvantageType $value */
				return $value->isContractual();
			});
		} else {
			$advantages = Tools::useColumnAsKey('reference', $this->_advantage->agency->advantageTypes, $castString = true);
			$advantages = array_filter($advantages, function ($value) {
				/** @var Models\AdvantageType $value */
				return $value->isPunctual();
			});
		}

		return $advantages;
	}

	/**
	 * @return FindRelationships
	 */
	public static function getFilterForRelationships()
	{
		return new FindRelationships();
	}
}
