<?php
/**
 * advantages.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Advantages\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\MySQL\Where;

use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiDB;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Resources
 * @property-read InputValue $user
 * @property-read \Wish\Filters\Inputs\InputMultiDB $advantageTypes
 * @property-read \Wish\Filters\Inputs\InputMultiValues resourceTypes
 * @property-read \Wish\Filters\Inputs\InputMultiValues $resourceStates
 * @property-read InputValue $sort
 * @property-read InputValue $period
 * @property-read InputDate $startDate
 * @property-read InputDate $endDate
 * @package BoondManager\Models\Filters\Search
 */
class SearchAdvantages extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_RESOURCES;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_RESOURCE_LASTNAME = 'resource.lastName',
		ORDERBY_TYPEREFERENCE = 'typeReference',
		ORDERBY_RESOURCE_FUNCTION = 'resource.function';
	/**#@-*/

	/**
	 * Advantages constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->flags->setDisabled(true);

		$advantageTypes = new InputMultiDB('advantageTypes');
		$advantageTypes->addFilterExistsInDB('TAB_TYPEAVANTAGE', new Where('ID_TYPEAVANTAGE IN (?)')); // <- uses 'column IN (?)' (notice the IN) because it will perform only one query

		$resourceStates = new InputMultiDict('resourceStates', 'specific.setting.state.resource');
		$resourceTypes = new InputMultiDict('resourceTypes', 'specific.setting.typeOf.resource');

		$period = new InputEnum('period');
		$period->setModeDefaultValue(BM::INDIFFERENT);
		$period->setAllowedValues([BM::INDIFFERENT, self::PERIOD_CREATED]);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_CREATIONDATE, self::ORDERBY_TYPEREFERENCE, self::ORDERBY_RESOURCE_LASTNAME,
			self::ORDERBY_RESOURCE_FUNCTION
		]);

		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');

		$this->addInput([$startDate, $endDate, $advantageTypes, $resourceTypes, $resourceStates, $period, $sort]);
	}
}
