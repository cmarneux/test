<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Advantages;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Advantages\Specifications\HaveSearchAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;
use Wish\Exceptions;

class Index extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'advantageType' => [
			'reference',
			'name'
		],
		'quantity',
		'costPaid',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'agency' => [
			'id',
			'name'
		],
		'resource' => [
			'id',
			'lastName',
			'firstName'
		],
	];

	public function api_get() {

		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchAdvantages();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = Services\Advantages::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post(){

		$filter = Filters\SaveEntity::getFilterForRelationships();
		$filter->setAndFilterData($this->requestAccess->get('data'));

		try {
			$advantage = Services\Advantages::newAdvantage(
				$filter->getResourceID(),
				$filter->getContractID(),
				$filter->getProjectID(),
				$filter->getDeliveryID()
			);
		}catch(Exceptions\Custom $e){
			$this->error( $e->getCode(), $e->getMessage() );
		}

		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_ADMINISTRATIVE), $advantage->resource);

		$filter = new Filters\SaveEntity($advantage);
		$filter->setValue($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		$advantage = Services\Advantages::buildFromFilter($filter, $advantage);

		if( Services\Advantages::create($advantage) ) {
			$this->sendJSONResponse([
				'data' => $advantage->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(Services\BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
