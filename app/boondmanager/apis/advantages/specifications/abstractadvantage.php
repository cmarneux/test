<?php
/**
 * abstractadvantage.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Advantages\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Advantage;
use BoondManager\Lib\RequestAccess;

abstract class AbstractAdvantage extends AbstractSpecificationItem{
    /**
     * get the advantage from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Advantage|null
     */
    public function getAdvantage($request){
        if($request->data instanceof Advantage) return $request->data;
        else return null;
    }
}
