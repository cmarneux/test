<?php
/**
 * UserHaveSearchAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Advantages\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on advantages
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Advantages
 */
class HaveSearchAccess extends AbstractAdvantage{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		//~ On autorise la recherche uniquement pour les administrateurs... (sinon ce serait très compliqué de vérifier les droits pour chaque contrat retourné)
		return $user->isGod();
		//~ return true;
	}
}
