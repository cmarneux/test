<?php
/**
 * activityexpenses.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;
use BoondManager\APIs\Agencies\Filters;
use BoondManager\APIs\Agencies\Specifications\HaveReadAccess;
use BoondManager\Models\Agency;

/**
 * Class ActivityExpenses
 * @package BoondManager\APIs\Agencies
 */
class ActivityExpenses extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'activityExpensesLogo',
		'timesLegals',
		'expensesLegals',
		'absencesLegals',
		'timesCreationAndMailingDate',
		'expensesCreationAndMailingDate',
		'contractualExpensesAutomaticFilling',
		'workUnitTypes' => [
			"id",
			"reference",
			"activityType",
			"name",
			"state",
            "warning"
		],
		'allowResourcesToViewAbsencesAccounts',
		'maskExceedingWarningsWithQuotas',
		'defaultAbsencesAccountsPeriod',
		'absencesCalculationMethod',
		'absencesQuotas' => [
			'id',
			'quotaAcquired',
			'period',
			'useBeingAcquired',
			'monthlyIncrement',
			'autoDeferral',
			'workUnitType' => [
				'reference'
			]
		],
		'expenseTypes' => [
			"id",
			"reference",
			"taxRate",
			"name",
			"state"
		],
		'ratePerKilometerTypes' => [
			"id",
			"reference",
			"amount",
			"name",
			"state"
		],
		'timesReportsWorkflow',
		'expensesReportsWorkflow',
		'maskExceedingWarningsWithQuotas',
		'maskExceedingWarningsWithQuotas',
		'absencesReportsWorkflow'
	];

	/**
	 * Get agency's information data
	 */
	public function api_get() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_ACTIVITYEXPENSES);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Agency::TAB_ACTIVITYEXPENSES), $agency);

		$this->sendJSONResponse([
			'data' => $agency->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	/**
	 * Update agency's information data
	 */
	public function api_put() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_ACTIVITYEXPENSES);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Agency::TAB_ACTIVITYEXPENSES), $agency);

		//Build filters
		$filter = new Filters\ActivityExpenses($agency);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build agency
		Agencies::buildFromFilter($filter, $agency);

		if(Agencies::update($agency, Agency::TAB_ACTIVITYEXPENSES))
			$this->sendJSONResponse([
				'data' => $agency->filterFields(self::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
