<?php
/**
 * billing.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;
use BoondManager\APIs\Agencies\Filters;
use BoondManager\APIs\Agencies\Specifications\HaveReadAccess;
use BoondManager\Models\Agency;

/**
 * Class Billing
 * @package BoondManager\APIs\Agencies
 */
class Billing extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'invoicesLogo',
		'invoiceRecordTypes' => [
		    'id',
            'reference',
            'name',
            'workUnitTypes' => [
                'reference'
            ],
            'expenseTypes' => [
                'reference'
            ],
            'schedule',
            'reinvoicedPurchase',
            'product'

        ],
		'banksDetails' => [
            'id',
            'description',
            'iban',
            'bic',
			'isDefault'
        ],
		'invoicesLegals',
		'invoicesLockingStates',
		'invoicesAutomaticCreation',
		'invoicesDeltaCreationInDays',
		'invoicesStateForSendingMailToClient',
		'invoicesStateToSetAfterSendingMailToClient',
		'invoicesMailSenderType',
		'invoicesMailSenderCustomized',
		'invoicesReferenceMask',
		'invoicesStartingNumber',
		'ordersPaymentTerm',
		'ordersPaymentMethod',
		'ordersTaxRate',
		'ordersCopyCommentsOnNewInvoice',
		'ordersShowCommentsOnPDF',
		'ordersShowFactorOnPDF',
		'ordersShowCompanyVATNumberOnPDF',
		'ordersShowCompanyNumberOnPDF',
		'ordersShowBankDetailsOnPDF',
		'ordersShowProjectReferenceOnPDF',
		'ordersShowResourcesNameOnPDF',
		'ordersShowAverageDailyPriceOnPDF',
		'ordersShowNumberOfWorkignDaysOnPDF',
		'ordersShowFooterOnPDF',
		'ordersSeparateActivityExpensesAndPurchases',
		'ordersGroupDeliveries',
		'factor' => [
			'id',
			'name'
		]
	];

	/**
	 * Get agency's information data
	 */
	public function api_get() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_BILLING);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Agency::TAB_BILLING), $agency);

		$this->sendJSONResponse([
			'data' => $agency->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	/**
	 * Update agency's information data
	 */
	public function api_put() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_BILLING);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Agency::TAB_BILLING), $agency);

		//Build filters
		$filter = new Filters\Billing($agency);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build agency
		Agencies::buildFromFilter($filter, $agency);

		if(Agencies::update($agency, Agency::TAB_BILLING))
			$this->sendJSONResponse([
				'data' => $agency->filterFields(self::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
