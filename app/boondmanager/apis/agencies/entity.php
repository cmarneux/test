<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveCreateAccess;
use BoondManager\APIs\Agencies\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Agencies\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;

/**
 * Class Entity
 * @package BoondManager\APIs\Agencies
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'name'
	];

    const ALLOWED_FIELDS_DEFAULT = [
        'id',
        'country',
        'currency',
        'exchangeRate',
        'calendar'
    ];

	/**
	 * Get agency's basic data
	 */
	public function api_get() {
		if($this->requestAccess->id) {
			$agency = Agencies::get($this->requestAccess->id, BM::TAB_DEFAULT);
			if(!$agency) $this->error(404);

			$this->checkAccessWithSpec(new HaveReadAccess(BM::TAB_DEFAULT), $agency);

			$ALLOWED_FIELDS = self::ALLOWED_FIELDS;
		} else {
			//api/agency/default
            $this->checkAccessWithSpec( new HaveCreateAccess );

            $agency = Agencies::getNew();
            $ALLOWED_FIELDS = self::ALLOWED_FIELDS_DEFAULT;
		}

        $this->sendJSONResponse([
            'data' => $agency->filterFields($ALLOWED_FIELDS)
        ]);
	}

	/**
	 * Delete an agency
	 */
	public function api_delete() {
		$agency = Agencies::get($this->requestAccess->id);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $agency);

		if(Agencies::delete($agency->getID()))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}
}
