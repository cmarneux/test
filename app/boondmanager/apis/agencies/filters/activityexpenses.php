<?php
/**
 * activityexpenses.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use BoondManager\Services\BM;
use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiObjects;

/**
 * Class ActivityExpenses
 * @property Attributes\Legals timesLegals
 * @property Attributes\Legals expensesLegals
 * @property Attributes\Legals absencesLegals
 * @property Attributes\DayOfMonth timesCreationAndMailingDate
 * @property Attributes\DayOfMonth expensesCreationAndMailingDate
 * @property InputBoolean contractualExpensesAutomaticFilling
 * @property InputBoolean allowResourcesToViewAbsencesAccounts
 * @property InputBoolean maskExceedingWarningsWithQuotas
 * @property InputEnum defaultAbsencesAccountsPeriod
 * @property InputEnum absencesCalculationMethod
 * @property Attributes\Workflow timesReportsWorkflow
 * @property Attributes\Workflow expensesReportsWorkflow
 * @property Attributes\Workflow absencesReportsWorkflow
 * @property InputMultiObjects workUnitTypes
 * @property InputMultiObjects absencesQuotas
 * @property InputMultiObjects expenseTypes
 * @property InputMultiObjects ratePerKilometerTypes
 * @property InputBoolean copyDataOnOtherAgencies
 * @package BoondManager\APIs\Agencies\Filters
 */
class ActivityExpenses extends AbstractJsonAPI {
    protected $_objectClass = Models\Agency::class;

	/**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Simulation constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency) {
		parent::__construct();

		$this->agency = $agency;

		$this->addInput(new Attributes\Legals('timesLegals'));
		$this->addInput(new Attributes\Legals('expensesLegals'));
		$this->addInput(new Attributes\Legals('absencesLegals'));

		$input = new Attributes\DayOfMonth('timesCreationAndMailingDate');
		$input->addAllowedValue('inactive');
		$this->addInput($input);

		$input = new Attributes\DayOfMonth('expensesCreationAndMailingDate');
		$input->addAllowedValue('inactive');
		$this->addInput($input);

		$this->addInput(new InputBoolean('contractualExpensesAutomaticFilling'));
        $this->addInput(new InputBoolean('allowResourcesToViewAbsencesAccounts'));
        $this->addInput(new InputBoolean('maskExceedingWarningsWithQuotas'));

        $input = new InputEnum('defaultAbsencesAccountsPeriod');
        $input->setAllowedValues(Models\AbsencesQuota::ACCOUNTSPERIOD);
        $this->addInput($input);

        $input = new InputEnum('absencesCalculationMethod');
        $input->setAllowedValues(Models\AbsencesQuota::CALCULATIONMETHOD);
        $this->addInput($input);

        $input = new Attributes\Workflow('timesReportsWorkflow');
        $input->addUnicityFilter();
        $this->addInput($input);

        $input = new Attributes\Workflow('expensesReportsWorkflow');
        $input->addUnicityFilter();
        $this->addInput($input);

        $input = new Attributes\Workflow('absencesReportsWorkflow');
        $input->addUnicityFilter();
        $this->addInput($input);

        $template = new Attributes\WorkUnitType();
        $template->setAllowedWorkUnitTypes($agency->workUnitTypes);
        $input = new InputMultiObjects('workUnitTypes', $template);
        $input->addGroupFilterOnAttribute('id');
        $input->addGroupFilterOnAttribute('reference');
        $this->addInput($input);

        $template = new Attributes\AbsencesQuota();
        $template->setAllowedAbsencesQuotas($agency->absencesQuotas);
        $input = new InputMultiObjects('absencesQuotas', $template);
        $input->addGroupFilterOnAttribute('id');
        $this->addInput($input);

        $template = new Attributes\ExpenseType();
        $template->setAllowedExpenseTypes($agency->expenseTypes);
        $input = new InputMultiObjects('expenseTypes', $template);
        $input->addGroupFilterOnAttribute('id');
        $input->addGroupFilterOnAttribute('reference');
        $this->addInput($input);

        $template = new Attributes\RatePerKilometerType();
        $template->setAllowedRatePerKilometerTypes($agency->ratePerKilometerTypes);
        $input = new InputMultiObjects('ratePerKilometerTypes', $template);
        $input->addGroupFilterOnAttribute('id');
        $input->addGroupFilterOnAttribute('reference');
        $this->addInput($input);

        $this->addInput(new InputBoolean('copyDataOnOtherAgencies'));
	}

	protected function postValidation() {
        $workUnitTypes = $this->workUnitTypes->isDefined() ? $this->workUnitTypes->toObject() : $this->agency->workUnitTypes;
        /**
         * @var Attributes\AbsencesQuota $aq
         */
        foreach($this->absencesQuotas->getItems() as $aq) {
            $aq->setAllowedWorkUnitTypes($workUnitTypes);
            if(!$aq->isValid()) $this->absencesQuotas->invalidate();
        }
	}
}


