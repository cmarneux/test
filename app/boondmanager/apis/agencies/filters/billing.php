<?php
/**
 * billing.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use BoondManager\Services\Dictionary;
use BoondManager\Services\Agencies;
use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;
use BoondManager\Lib\Filters\Inputs\Relationships;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiObjects;

/**
 * Class Billing
 * @property Attributes\Legals invoicesLegals
 * @property Attributes\StartingNumber invoicesStartingNumber
 * @property Attributes\ReferenceMask invoicesReferenceMask
 * @property InputMultiDict invoicesLockingStates
 * @property InputDict invoicesStateForSendingMailToClient
 * @property InputDict invoicesStateToSetAfterSendingMailToClient
 * @property InputDict ordersPaymentTerm
 * @property InputDict ordersPaymentMethod
 * @property InputEnum invoicesMailSenderType
 * @property Attributes\Email invoicesMailSenderCustomized
 * @property Attributes\TaxRate ordersTaxRate
 * @property InputInt invoicesDeltaCreationInDays
 * @property InputMultiObjects invoiceRecordTypes
 * @property InputMultiObjects banksDetails
 * @property Relationships\Company factor
 * @property Relationships\BankDetails bankDetails
 * @property InputBoolean invoicesAutomaticCreation
 * @property InputBoolean ordersCopyCommentsOnNewInvoice
 * @property InputBoolean ordersShowCommentsOnPDF
 * @property InputBoolean ordersShowFactorOnPDF
 * @property InputBoolean ordersShowCompanyVATNumberOnPDF
 * @property InputBoolean ordersShowCompanyNumberOnPDF
 * @property InputBoolean ordersShowBankDetailsOnPDF
 * @property InputBoolean ordersShowProjectReferenceOnPDF
 * @property InputBoolean ordersShowResourcesNameOnPDF
 * @property InputBoolean ordersShowAverageDailyPriceOnPDF
 * @property InputBoolean ordersShowNumberOfWorkignDaysOnPDF
 * @property InputBoolean ordersShowFooterOnPDF
 * @property InputBoolean ordersSeparateActivityExpensesAndPurchases
 * @property InputBoolean ordersGroupDeliveries
 * @property InputBoolean copyDataOnOtherAgencies
 * @package BoondManager\APIs\Agencies\Filters
 */
class Billing extends AbstractJsonAPI {
    protected $_objectClass = Models\Agency::class;

    /**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Simulation constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency) {
		parent::__construct();

		$this->agency = $agency;

        $this->addInput(new Attributes\Legals('invoicesLegals'));

        $this->addInput(new Attributes\StartingNumber('invoicesStartingNumber'));

        $input = new Attributes\ReferenceMask('invoicesReferenceMask');
        $input->setFilterInvoiceOrQuotationReferenceMask();
        $this->addInput($input);

        $input = new InputMultiDict('invoicesLockingStates', 'specific.setting.state.invoice');
        $input->addUnicityFilter();
        $this->addInput($input);

        $input = new InputDict('invoicesStateForSendingMailToClient', 'specific.setting.state.invoice');
        $input->setAllowEmptyValue(true);
        $this->addInput($input);

        $input = new InputDict('invoicesStateToSetAfterSendingMailToClient', 'specific.setting.state.invoice');
        $input->setAllowEmptyValue(true);
        $this->addInput($input);

        $input = new InputDict('ordersPaymentTerm', 'specific.setting.paymentTerm');
        $this->addInput($input);

        $input = new InputDict('ordersPaymentMethod');
        $input->setDict(Dictionary::getDict('specific.setting.paymentMethod'), [-1]);
        $this->addInput($input);

        $input = new InputEnum('invoicesMailSenderType');
        $input->setAllowedValues([Models\Agency::MAILSENDERTYPE_MAINMANAGER, Models\Agency::MAILSENDERTYPE_CONTACT, Models\Agency::MAILSENDERTYPE_CUSTOMIZED]);
        $this->addInput($input);

        $this->addInput(new Attributes\Email('invoicesMailSenderCustomized'));

        $this->addInput(new Attributes\TaxRate('ordersTaxRate'));

        $this->addInput(new InputInt('invoicesDeltaCreationInDays'));

        $this->addInput(new InputBoolean('invoicesAutomaticCreation'));

        $this->addInput(new InputBoolean('ordersCopyCommentsOnNewInvoice'));

        $this->addInput(new InputBoolean('ordersShowCommentsOnPDF'));

        $this->addInput(new InputBoolean('ordersShowFactorOnPDF'));

        $this->addInput(new InputBoolean('ordersShowCompanyVATNumberOnPDF'));

        $this->addInput(new InputBoolean('ordersShowCompanyNumberOnPDF'));

        $this->addInput(new InputBoolean('ordersShowBankDetailsOnPDF'));

        $this->addInput(new InputBoolean('ordersShowProjectReferenceOnPDF'));

        $this->addInput(new InputBoolean('ordersShowResourcesNameOnPDF'));

        $this->addInput(new InputBoolean('ordersShowAverageDailyPriceOnPDF'));

        $this->addInput(new InputBoolean('ordersShowNumberOfWorkignDaysOnPDF'));

        $this->addInput(new InputBoolean('ordersShowFooterOnPDF'));

        $this->addInput(new InputBoolean('ordersSeparateActivityExpensesAndPurchases'));

        $this->addInput(new InputBoolean('ordersGroupDeliveries'));

        $template = new Attributes\InvoiceRecordType();
        $template->setAllowedInvoiceRecordTypes($agency->invoiceRecordTypes);
        $input = new InputMultiObjects('invoiceRecordTypes', $template);
        $input->addGroupFilterOnAttribute('id');
        $input->addGroupFilterOnAttribute('reference');
        $this->addInput($input);

        $template = new Attributes\BankDetails();
        $template->setAllowedBanksDetails($agency->banksDetails);
        $input = new InputMultiObjects('banksDetails', $template);
        $input->addGroupFilterOnAttribute('id');
        $this->addInput($input);

        $this->addInput(new Relationships\Company('factor'));

        $this->addInput(new Relationships\BankDetails());

        $this->addInput(new InputBoolean('copyDataOnOtherAgencies'));
	}

	protected function postValidation() {
        if($this->invoicesMailSenderCustomized->isDefined() && (!$this->invoicesMailSenderType->isDefined() || $this->invoicesMailSenderType->getValue() != Models\Agency::MAILSENDERTYPE_CUSTOMIZED)) {
            $this->invoicesMailSenderCustomized->invalidateIfDebug(Models\Agency::ERROR_AGENCY_MAILSENDERCUSTOMIZED_WRONG_TYPE);
            $this->invoicesMailSenderType->invalidateIfDebug(Models\Agency::ERROR_AGENCY_MAILSENDERCUSTOMIZED_WRONG_TYPE);
        }

        if($this->invoicesMailSenderType->isDefined() && $this->invoicesMailSenderType->getValue() == Models\Agency::MAILSENDERTYPE_CUSTOMIZED && $this->invoicesMailSenderCustomized->isDefined() && $this->invoicesMailSenderCustomized->isValid()) {
            $this->invoicesMailSenderCustomized->invalidateIfDebug(Models\Agency::ERROR_AGENCY_MAILSENDERCUSTOMIZED_WRONG_EMAIL);
            $this->invoicesMailSenderType->invalidateIfDebug(Models\Agency::ERROR_AGENCY_MAILSENDERCUSTOMIZED_WRONG_EMAIL);
            $this->invoicesMailSenderCustomized->setDisabled(true);
            $this->invoicesMailSenderType->setDisabled(true);
        }

        //We get agencies activity & expenses data
        $agencyActivityExpenses = Agencies::get($this->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);
        /**
         * @var Attributes\InvoiceRecordType $irt
         */
        foreach($this->invoiceRecordTypes->getItems() as $irt) {
            $irt->setAllowedWorkUnitTypes($agencyActivityExpenses->workUnitTypes);
            $irt->setAllowedExpenseTypes($agencyActivityExpenses->expenseTypes);
        }

        $banksDetails = $this->banksDetails->isDefined() ? $this->banksDetails->getValue() : $this->agency->banksDetails;
        $this->bankDetails->setAllowedBanksDetails($banksDetails);
	}
}


