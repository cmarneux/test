<?php
/**
 * information.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;
use BoondManager\Lib\Filters\Inputs\Attributes;

/**
 * Class Information
 * @property InputString name
 * @property InputInt staff
 * @property Attributes\Email email1
 * @property Attributes\Phone phone1
 * @property Attributes\Address address
 * @property Attributes\PostCode postcode
 * @property Attributes\Town town
 * @property Attributes\Country country
 * @property Attributes\VatNumber vatNumber
 * @property Attributes\RegistrationNumber registrationNumber
 * @property Attributes\LegalStatus legalStatus
 * @property Attributes\RegisteredOffice registeredOffice
 * @property Attributes\ApeCode apeCode
 * @property Attributes\NumberOfWorkingDays numberOfWorkingDays
 * @property Attributes\ChargeFactor chargeFactor
 * @property InputFloat exchangeRate
 * @property Attributes\Currency currency
 * @property InputFloat workUnitRate
 * @property Attributes\Calendar calendar
 * @property InputBoolean state
 * @package BoondManager\APIs\Agencies\Filters
 */
class Information extends AbstractJsonAPI {
    protected $_objectClass = Models\Agency::class;

    /**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Information constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency = null) {
		parent::__construct();

		$this->agency = $agency;

		$input = new InputString('name');
		$input->setMinLength(1);
		$input->setMaxLength(100);
		$this->addInput($input);

		$input = new InputInt('staff');
		$input->setMin(0);
		$this->addInput($input);

		$this->addInput(new Attributes\Email('email1'));
		$this->addInput(new Attributes\Phone('phone1'));
		$this->addInput(new Attributes\Address());
		$this->addInput(new Attributes\PostCode());
		$this->addInput(new Attributes\Town());
		$this->addInput(new Attributes\Country());
		$this->addInput(new Attributes\Calendar());
		$this->addInput(new Attributes\NumberOfWorkingDays());
		$this->addInput(new Attributes\ChargeFactor());
		$this->addInput(new Attributes\VatNumber());
		$this->addInput(new Attributes\RegistrationNumber());
		$this->addInput(new Attributes\LegalStatus());
		$this->addInput(new Attributes\RegisteredOffice());
		$this->addInput(new Attributes\ApeCode());

		$this->addInput(new Attributes\Currency());
		$this->addInput(new InputFloat('exchangeRate'));

		$this->addInput(new InputFloat('workUnitRate'));

		$this->addInput(new InputBoolean('state'));

		if($this->isCreation())
			$this->adaptForCreation();
		else
			$this->adaptForEdition();
	}

	/**
	 *
	 */
	protected function postValidation() {

	}

	/**
	 * Test if the agency is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->agency && $this->agency->id ? false : true;
	}

	/**
	 * Set inputs for an update
	 */
	private function adaptForEdition() {
	}

	/**
	 * Set inputs for a creation
	 */
	private function adaptForCreation() {
		$this->name->setRequired(true);
	}
}


