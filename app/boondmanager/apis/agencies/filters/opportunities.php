<?php
/**
 * opportunities.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use BoondManager\Services\Dictionary;
use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;

/**
 * Class Opportunities
 * @property InputString opportunitiesTechnicalAssistanceReferenceMask
 * @property InputString opportunitiesPackageReferenceMask
 * @property InputString opportunitiesRecruitmentReferenceMask
 * @property InputString opportunitiesProductReferenceMask
 * @property InputInt quotationsValidityInDays
 * @property InputString quotationsLegals
 * @property Attributes\StartingNumber quotationsStartingNumber
 * @property Attributes\ReferenceMask quotationsReferenceMask
 * @property InputDict quotationsPaymentTerm
 * @property Attributes\TaxRate quotationsTaxRate
 * @property InputBoolean quotationsShowCompanyVATNumberOnPDF
 * @property InputBoolean quotationsShowCompanyNumberOnPDF
 * @property InputBoolean quotationsShowOpportunityReferenceOnPDF
 * @property InputBoolean quotationsShowFooterOnPDF
 * @package BoondManager\APIs\Agencies\Filters
 */
class Opportunities extends AbstractJsonAPI {
    protected $_objectClass = Models\Agency::class;

    /**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Simulation constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency) {
		parent::__construct();

		$this->agency = $agency;

        $input = new Attributes\ReferenceMask('opportunitiesTechnicalAssistanceReferenceMask');
        $input->setFilterDefaultReferenceMask();
        $this->addInput($input);

        $input = new Attributes\ReferenceMask('opportunitiesPackageReferenceMask');
        $input->setFilterDefaultReferenceMask();
        $this->addInput($input);

        $input = new Attributes\ReferenceMask('opportunitiesRecruitmentReferenceMask');
        $input->setFilterDefaultReferenceMask();
        $this->addInput($input);

        $input = new Attributes\ReferenceMask('opportunitiesProductReferenceMask');
        $input->setFilterDefaultReferenceMask();
        $this->addInput($input);

        $input = new InputInt('quotationsValidityInDays');
        $input->setMin(0);
        $this->addInput($input);

        $input = new InputString('quotationsLegals');
        $input->setMaxLength(5000);
        $this->addInput($input);

        $this->addInput(new Attributes\StartingNumber('quotationsStartingNumber'));

        $input = new Attributes\ReferenceMask('quotationsReferenceMask');
        $input->setFilterInvoiceOrQuotationReferenceMask();
        $this->addInput($input);

        $this->addInput(new InputDict('quotationsPaymentTerm', 'specific.setting.paymentTerm'));

        $this->addInput(new Attributes\TaxRate('quotationsTaxRate'));

        $this->addInput(new InputBoolean('quotationsShowCompanyVATNumberOnPDF'));

        $this->addInput(new InputBoolean('quotationsShowCompanyNumberOnPDF'));

        $this->addInput(new InputBoolean('quotationsShowOpportunityReferenceOnPDF'));

        $this->addInput(new InputBoolean('quotationsShowFooterOnPDF'));
	}

	protected function postValidation() {
	}
}


