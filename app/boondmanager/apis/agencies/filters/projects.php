<?php
/**
 * projects.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;

/**
 * Class Projects
 * @property InputString projectsTechnicalAssistanceReferenceMask
 * @property InputString projectsPackageReferenceMask
 * @property InputString projectsRecruitmentReferenceMask
 * @property InputString projectsProductReferenceMask
 * @property InputFloat projectsRebillingRate
 * @property InputBoolean allowAlonesMarkers
 * @property InputBoolean allowAdvantagesOnProjects
 * @property InputBoolean allowExceptionalScalesOnProjects
 * @property Attributes\DayOfMonth jDay
 * @package BoondManager\APIs\Agencies\Filters
 */
class Projects extends AbstractJsonAPI {
    protected $_objectClass = Models\Agency::class;

    /**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Simulation constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency) {
		parent::__construct();

		$this->agency = $agency;

        $input = new InputString('projectsTechnicalAssistanceReferenceMask');
        $input->setMaxLength(50);
        $this->addInput($input);

        $input = new InputString('projectsPackageReferenceMask');
        $input->setMaxLength(50);
        $this->addInput($input);

        $input = new InputString('projectsRecruitmentReferenceMask');
        $input->setMaxLength(50);
        $this->addInput($input);

        $input = new InputString('projectsProductReferenceMask');
        $input->setMaxLength(50);
        $this->addInput($input);

        $input = new InputFloat('projectsRebillingRate');
        $input->setMin(0);
        $input->setMax(100);
        $this->addInput($input);

        $this->addInput(new InputBoolean('allowAlonesMarkers'));

        $this->addInput(new InputBoolean('allowAdvantagesOnProjects'));

        $this->addInput(new InputBoolean('allowExceptionalScalesOnProjects'));

        $input = new Attributes\DayOfMonth('jDay');
        $input->addAllowedValue('actualDay');
        $this->addInput($input);
	}

	protected function postValidation() {
	}
}


