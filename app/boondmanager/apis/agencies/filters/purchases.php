<?php
/**
 * purchases.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use BoondManager\Services\Dictionary;
use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputString;

/**
 * Class Purchases
 * @property InputString purchasesReferenceMask
 * @property InputDict purchasesPaymentTerm
 * @property InputDict purchasesPaymentMethod
 * @property Attributes\TaxRate purchasesTaxRate
 * @package BoondManager\APIs\Agencies\Filters
 */
class Purchases extends AbstractJsonAPI {
    protected $_objectClass = Models\Agency::class;

	/**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Simulation constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency) {
		parent::__construct();

		$this->agency = $agency;

        $input = new InputString('purchasesReferenceMask');
        $input->setMaxLength(250);
        $this->addInput($input);

        $this->addInput(new InputDict('purchasesPaymentTerm', 'specific.setting.paymentTerm'));

        $input = new InputDict('purchasesPaymentMethod');
        $input->setDict(Dictionary::getDict('specific.setting.paymentMethod'), [-1]);
        $this->addInput($input);

        $this->addInput(new Attributes\TaxRate('purchasesTaxRate'));
	}

	protected function postValidation() {
	}
}


