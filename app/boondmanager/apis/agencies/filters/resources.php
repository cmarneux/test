<?php
/**
 * resources.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Filters;

use BoondManager\Services\Agencies;
use BoondManager\Services\BM;
use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Tools;

/**
 * Class Resources
 * @property InputFloat contractsNumberOfHoursPerWeek
 * @property InputBoolean allowExceptionalScalesOnContracts
 * @property InputMultiObjects advantageTypes
 * @property InputMultiObjects advantagePaidExceptionalTypes
 * @property InputMultiObjects exceptionalScaleTypes
 * @package BoondManager\APIs\Agencies\Filters
 */
class Resources extends AbstractJsonAPI {
	protected $_objectClass = Models\Agency::class;

	/**
	 * @var Models\Agency
	 */
	protected $agency;

	/**
	 * Simulation constructor.
	 * @param Models\Agency|null $agency
	 */
	public function __construct(Models\Agency $agency) {
		parent::__construct();

		$this->agency = $agency;

		$input = new InputFloat('contractsNumberOfHoursPerWeek');
		$input->setMin(0);
		$input->setMax(168);
		$this->addInput($input);

		$this->addInput(new InputBoolean('allowExceptionalScalesOnContracts'));

		$template = new Attributes\AdvantageType();
		$template->setAllowedAdvantageTypes($agency->advantageTypes);
		$input = new InputMultiObjects('advantageTypes', $template);
		$input->addGroupFilterOnAttribute('id');
		$input->addGroupFilterOnAttribute('reference');
		$this->addInput($input);

		$template = new Attributes\AdvantagePaidExceptionalType();
		$input = new InputMultiObjects('advantagePaidExceptionalTypes', $template);
		$input->addGroupFilterCallback(function($array) {
			$newArray = [];
			foreach($array as $entry) {
				$key = $entry->workUnitType->reference->getValue();
				if(isset($newArray[$key]))
					return false;
				else $newArray[$key] = $key;
			}
			return $array;
		}, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
		$this->addInput($input);

		$template = new Attributes\ExceptionalScaleType();
		$template->setAllowedExceptionalScaleTypes($agency->exceptionalScaleTypes);
		$input = new InputMultiObjects('exceptionalScaleTypes', $template);
		$input->addGroupFilterOnAttribute('reference');
		$this->addInput($input);
	}

	protected function postValidation() {
		//We get agencies activity & expenses data
		$agencyActivityExpenses = Agencies::get($this->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);
		$advantageTypes = $this->advantageTypes->isDefined() ? $this->advantageTypes->toObject() : $this->agency->advantageTypes;
		$exceptionalScaleTypes = $this->exceptionalScaleTypes->isDefined() ? $this->exceptionalScaleTypes->toObject() : $this->agency->exceptionalScaleTypes;
		/**
		 * @var Attributes\AdvantagePaidExceptionalType $apet
		 */
		foreach($this->advantagePaidExceptionalTypes->getItems() as $apet) {
			$apet->setAllowedWorkUnitTypes($agencyActivityExpenses->workUnitTypes);
			$apet->setAllowedAdvantageTypes($advantageTypes);
		}

		/**
		 * @var Attributes\ExceptionalScaleType $est
		 */
		foreach($this->exceptionalScaleTypes->getItems() as $est) {
			$est->setAllowedWorkUnitTypes($agencyActivityExpenses->workUnitTypes);
			/**
			 * @var Attributes\ExceptionalScaleType $pEst
			 */
			foreach($exceptionalScaleTypes as $pEst) {
				if ($pEst->reference == $est->reference)
					$est->setAllowedExceptionalRuleTypes($pEst->exceptionalRuleTypes);
			}
		}
	}
}


