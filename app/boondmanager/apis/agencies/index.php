<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveCreateAccess;
use BoondManager\APIs\Agencies\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;
use BoondManager\APIs\Agencies\Filters;
use Wish\Cache;

/**
 * Class Index
 * @package BoondManager\APIs\Agencies
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'name',
		'calendar',
		'currency',
		'workUnitRate',
		'staff',
		'numberOfWorkingDays',
		'chargeFactor',
		'vatNumber',
		'address',
		'postcode',
		'town',
		'country',
		'state'
	];

    /**
     * Search agencies
     */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$cache = Cache::instance();
		$cache->setEnabled(false);
		$result = Agencies::getAllAgencies($full = true);
		$cache->setEnabled(true);

		foreach($result as $agency)
			$agency->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => sizeof($result),
				]
			],
			'data' => $result
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create an agency
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Information();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default agency
		$agency = Agencies::buildFromFilter($filter);
		if(!$agency) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $agency);

		if(Agencies::create($agency)) {
			$apiAgency = new Information();
			$this->sendJSONResponse([
				'data' => $apiAgency->getJSONData($agency)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
