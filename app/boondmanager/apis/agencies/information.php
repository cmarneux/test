<?php
/**
 * information.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;
use BoondManager\APIs\Agencies\Filters;
use BoondManager\APIs\Agencies\Specifications\HaveReadAccess;
use BoondManager\Models\Agency;

/**
 * Class Information
 * @package BoondManager\APIs\Agencies
 */
class Information extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'name',
		'email1',
		'phone1',
		'address',
		'postcode',
		'town',
		'country',
		'vatNumber',
		'registrationNumber',
		'legalStatus',
		'registeredOffice',
		'apeCode',
		'numberOfWorkingDays',
		'chargeFactor',
		'currency',
		'exchangeRate',
		'workUnitRate',
		'calendar',
		'state'
	];

	/**
	 * @param Agency $agency
	 * @return Agency
	 */
	public function getJSONData(Agency $agency) {
		return $agency->filterFields(self::ALLOWED_FIELDS);
	}

	/**
	 * Get agency's information data
	 */
	public function api_get() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_INFORMATION);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Agency::TAB_INFORMATION), $agency);

		$this->sendJSONResponse([
			'data' => $this->getJSONData($agency)
		]);
	}

	/**
	 * Update agency's information data
	 */
	public function api_put() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_INFORMATION);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Agency::TAB_INFORMATION), $agency);

		//Build filters
		$filter = new Filters\Information($agency);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build agency
		Agencies::buildFromFilter($filter, $agency);

		if(Agencies::update($agency, Agency::TAB_INFORMATION))

			$this->sendJSONResponse([
				'data' => $this->getJSONData($agency)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
