<?php
/**
 * opportunities.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;
use BoondManager\APIs\Agencies\Filters;
use BoondManager\APIs\Agencies\Specifications\HaveReadAccess;
use BoondManager\Models\Agency;

/**
 * Class Opportunities
 * @package BoondManager\APIs\Agencies
 */
class Opportunities extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'opportunitiesTechnicalAssistanceReferenceMask',
		'opportunitiesPackageReferenceMask',
		'opportunitiesRecruitmentReferenceMask',
		'opportunitiesProductReferenceMask',
		'quotationsLegals',
		'quotationsReferenceMask',
		'quotationsStartingNumber',
		'quotationsValidityInDays',
		'quotationsPaymentTerm',
		'quotationsTaxRate',
		'quotationsShowCompanyVATNumberOnPDF',
		'quotationsShowCompanyNumberOnPDF',
		'quotationsShowOpportunityReferenceOnPDF',
		'quotationsShowFooterOnPDF'
	];

	/**
	 * Get agency's information data
	 */
	public function api_get() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_OPPORTUNITIES);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Agency::TAB_OPPORTUNITIES), $agency);

		$this->sendJSONResponse([
			'data' => $agency->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	/**
	 * Update agency's information data
	 */
	public function api_put() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_OPPORTUNITIES);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Agency::TAB_OPPORTUNITIES), $agency);

		//Build filters
		$filter = new Filters\Opportunities($agency);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build agency
		Agencies::buildFromFilter($filter, $agency);

		if(Agencies::update($agency, Agency::TAB_OPPORTUNITIES))
			$this->sendJSONResponse([
				'data' => $agency->filterFields(self::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
