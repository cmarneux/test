<?php
/**
 * resources.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies;

use BoondManager\APIs\Agencies\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Agencies;
use BoondManager\APIs\Agencies\Filters;
use BoondManager\APIs\Agencies\Specifications\HaveReadAccess;
use BoondManager\Models\Agency;

/**
 * Class Resources
 * @package BoondManager\APIs\Agencies
 */
class Resources extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'contractsNumberOfHoursPerWeek',
        'allowExceptionalScalesOnContracts',
        'advantageTypes' => [
            'id',
            'reference',
            'name',
            'frequency',
            'category',
            'participationQuota',
            'employeeQuota',
            'agencyQuota',
            'default',
            'state'
        ],
        'advantagePaidExceptionalTypes' => [
            'workUnitType' => [
                'reference'
            ],
            'advantageType' => [
                'reference'
            ]
        ],
        'exceptionalScaleTypes' => [
            'reference',
            'name',
            'default',
            'workUnitTypes' => [
                'reference'
            ],
            'exceptionalRuleTypes' => [
                'id',
                'name',
                'reference',
                'priceExcludingTaxOrPriceRate',
                'grossCostOrSalaryRate',
                'state'
            ]
        ]
	];

	/**
	 * Get agency's information data
	 */
	public function api_get() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_RESOURCES);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Agency::TAB_RESOURCES), $agency);

		$this->sendJSONResponse([
			'data' => $agency->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	/**
	 * Update agency's information data
	 */
	public function api_put() {
		$agency = Agencies::get($this->requestAccess->id, Agency::TAB_RESOURCES);
		if(!$agency) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Agency::TAB_RESOURCES), $agency);

		//Build filters
		$filter = new Filters\Resources($agency);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build agency
		Agencies::buildFromFilter($filter, $agency);

		if(Agencies::update($agency, Agency::TAB_RESOURCES))

			$this->sendJSONResponse([
				'data' => $agency->filterFields(self::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
