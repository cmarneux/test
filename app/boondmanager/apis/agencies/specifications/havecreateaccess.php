<?php
/**
 * HaveCreateAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;

/**
 * Class HaveCreateAccess
 * @package BoondManager\APIs\Agencies\Specifications
 */
class HaveCreateAccess extends AbstractSpecificationItem {
    /**
     * @param RequestAccess $request
     * @return bool
     */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		//Check this API authorization & access
		return $user->isAdministrator();
	}
}
