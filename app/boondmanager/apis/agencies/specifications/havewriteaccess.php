<?php
/**
 * HaveWriteAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Agencies\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;

/**
 * Class HaveWriteAccess
 * @package BoondManager\APIs\Agencies\Specifications
 */
class HaveWriteAccess extends AbstractSpecificationItem {
    use TabBehavior;

    /**
     * @param RequestAccess $request
     * @return bool
     */
    public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
        $user = $request->user;

		//Check this API authorization & access
		return $user->isAdministrator();
    }
}
