<?php
/**
 * assignments.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\AssignmentList;
use BoondManager\OldModels\Specifications\RequestAccess\MustBeAuthenticated;
use BoondManager\Services\CurrentUser;

class Assignments extends AbstractController{
	public function api_get(){
		$this->checkAccessWithSpec( new MustBeAuthenticated );

		$assignments = new AssignmentList(CurrentUser::instance());

		foreach($assignments->getHRManager() as $m)
			$m->filterFields(['id', 'lastName', 'firstName']);

		foreach($assignments->getManagers() as $m) {
			$m->filterFields(['id', 'lastName', 'firstName']);
		}

		$this->sendJSONResponse([
			'data' => $assignments
		]);
	}
}
