<?php
/**
 * batch.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\Application\Filters\BatchEntry;
use BoondManager\OldModels\Specifications\RequestAccess\MustBeAuthenticated;

class Batch extends AbstractController{
	public function api_get(\Base $f3){
		$this->checkAccessWithSpec( new MustBeAuthenticated() );

		$filter = new Filters\Batch();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$data = [];
		$headerCopy = $f3->HEADERS;
		BM::setMockingErrorHandler();
		$f3->QUIET = true;
		foreach($filter->data->getItems() as /** @var BatchEntry $filter */ $fRequest){
			$method = $fRequest->method->getValue();
			$api = $fRequest->api->getValue();
			$headers = $headerCopy;
			$params = $headers = [];
			$body = null;

			if($fRequest->body->isDefined()){
				$headers['Content-Type'] = 'application/json';
				$body = json_encode($fRequest->body->getValue());
			}

			try {
				$f3->mock("$method /api$api [ajax]", $params, $headers, $body);
			}catch(\Exception $e){}

			$resp = json_decode($f3->RESPONSE, true);

			if( $fRequest->id->isDefined() ) $resp = ['id'=>$fRequest->id->getValue()] + $resp;

			$f3->clear('RESPONSE');

			$data[] = $resp;
		}
		$f3->QUIET = false;
		BM::setPageErrorHandler();

		$this->sendJSONResponse(['data' =>$data]);
	}
	public function api_put($f3){
		return $this->api_get($f3);
	}
	public function api_post($f3){
		return $this->api_get($f3);
	}
}
