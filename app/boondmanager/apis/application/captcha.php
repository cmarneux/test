<?php
/**
 * captcha.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;

/**
 * Class Captcha
 * @package BoondManager\Controllers\Main\Application
 */
class Captcha extends AbstractController {

	protected $requireClient = false;

	public function api_get() {

		if($code = $this->requestAccess->get('code')) {
			$this->sendJSONResponse(['data' => ['isValid' =>BM::validateCaptcha($code, $clear = false)]]);
		} else {
			try {
				$img = BM::generateCaptcha();
				$img->render();
			} catch (\Exception $e) {
				$this->error(BM::ERROR_APPLICATION_FAILED_TO_CREATE_CAPTCHA);
			}
		}
	}
}
