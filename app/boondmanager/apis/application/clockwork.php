<?php
/**
 * clockwork.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use Wish\Tools;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;

class ClockWork extends AbstractController {
    public function api_get($f3) {
        if(!BM::isDevelopmentMode()) $this->error(403);

        $storage = Tools::getClockwork()->getStorage();
        $request = $storage->retrieve($f3->get('PARAMS.id'));
        if($request) echo $request->toJson();
        else $this->error(404);
    }
}
