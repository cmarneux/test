<?php
/**
 * connectnewdevice.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Services\Devices;
use Wish\Email;
use Wish\Tools;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Services\Login;
use BoondManager\Databases\Local;

/**
 * Class ConnectNewDevice
 * @package BoondManager\Controllers\Main\Application
 */
class ConnectNewDevice extends AbstractController {

	protected $requireClient = false;

	/**
	 * Log in the user from a new device.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a POST request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_post($f3) {

		if(CurrentUser::instance()->isLogged()) $this->error(BM::ERROR_APPLICATION_ALREADY_LOGGED);	//User's ever logged
		if(!$f3->exists('SESSION.device')) $this->error(BM::ERROR_APPLICATION_NO_LOGIN_ATTEMPT_FOUND_WITH_THIS_DEVICE);	//No attempt to login before with post API

		$tabData = [];
		$isAccessible = false;
		//Check if the authorization's code of new device is correct OR save the new device & log in
		if($f3->get('REQUEST.code') == $f3->get('SESSION.device.code') && $f3->exists('REQUEST.nom')) {
			$tabDevice = $f3->get('SESSION.device.new');

            if($f3->exists('SESSION.CLIENT')) $f3->clear('SESSION.CLIENT');
            $f3->config(BM::getCustomerInterfaceFilePath($tabDevice[0]));//Load customer's configuration

			if(Login::connectionFromLoginPassword($tabDevice[1], $tabDevice[2], 1)) {
				$isAccessible = true;

				$tabDevices = json_decode($f3->get('REQUEST.devices'), true);
				$deviceBDD = new Local\Device();
				if($device = $deviceBDD->getDeviceFromUserAndCookie($f3->get('SESSION.user.id'), $tabDevices))
					$device = $deviceBDD->updateDevice(['DALL_NOM' => $f3->get('REQUEST.nom')], $device->ID_DEVICEALLOWED);
				else {
					$device = Devices::createDevice($this->requestAccess->get('nom'));
				}

				//Return unid identifier's evice that will be save into session
				$tabDevices[$tabDevice[0].$f3->get('SESSION.user.id')] = $device->DALL_SHA; // bug
				$tabData['meta']['devices'] = json_encode($tabDevices);

				Login::buildComplementaryUserData(); //Save session's parameters
				BM::autoSetModeDemo();

				//Check if customer's logo exists
				$img_file = Tools::whichImageWithExtensionExist($f3->get('MAIN_ROOTPATH').'/www/img/_clients/logo/'.$tabDevice[0]);
				$tabData['meta']['company']['logo'] = ($img_file !== false)?'_clients/logo'.$img_file:'';

				if($tabDevice[4] == 1) {//Warn the connection of this new device
					$email = new Email();
					$email->Send($tabDevice[3], Dictionary::getDict('main.application.mail.nouveau.objet'), Dictionary::getDict('main.application.mail.nouveau.message', $f3->get('BOONDMANAGER.APP_URL') . (($tabDevice[0] != '') ? '/' . $tabDevice[0] : '')), $f3->get('EMAIL.NOREPLY'));
				}
			}
		}
		if(!$isAccessible) $this->error(BM::ERROR_APPLICATION_WRONG_CODE_TO_AUTHENTICATE_THIS_DEVICE);	//Wrong code
		$tabData = array_merge_recursive($tabData, Login::getUserData());//We make the data array
		$this->sendJSONResponse($tabData);
	}
}
