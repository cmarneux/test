<?php
/**
 * currentuser.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;

/**
 * Class CurrentUser
 * @package BoondManager\Controllers\Main\Application
 */
class CurrentUser extends AbstractController{

	protected $requireClient = false;

	const ALLOWED_FIELDS = [
		'id', 'login', 'navigationBar', 'advancedApps', 'rights', 'homePage',
		'firstName', 'lastName', 'email1', 'phone1', 'level', 'lastLogInDate',
		'agency' => [
			'id', 'name', 'group', 'logo', 'code', 'currency', 'exchangeRate'
		],
		'modules' => [
			'resources', 'projects', 'opportunities', 'candidates', 'products', 'crm', 'purchases', 'reporting',
			'activityExpenses', 'billing', 'subscription', 'actions', 'dashboard', 'flags', 'actions'
		],
		'apps' => [
			'id', 'name', 'description', 'category', 'suffixWeb'
		]
	];

	/**
	 * Get user's main data
	 */
	public function api_get() {

		$currentUser = \BoondManager\Services\CurrentUser::instance();
		if($currentUser->isLogged()) {
			$data = [
				'data' => $currentUser->getAccount()->filterFields(self::ALLOWED_FIELDS)
			];
		} else
			$data = [];

		$this->sendJSONResponse($data, true, false);
	}
}
