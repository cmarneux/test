<?php
/**
 * dashboard.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;
use BoondManager\Lib\AbstractController;
use BoondManager\Databases\Local\Apps;
use BoondManager\OldModels\Specifications\RequestAccess\MustBeAuthenticated;

/**
 * Dashboard controller.
 * @namespace \BoondManager\Controllers\Main
 */
class Dashboard extends AbstractController  {
	/**
	 * Get dashboard data.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a GET request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 *
	 * @note Transmet une réponse JSON
	 *
	 * @api {get} /dashboard Get dashboard data
	 * @apiName get
	 * @apiGroup Main/Dashboard
	 * @apiVersion 1.0.0
	 *
	 * @apiUse applicationGlobalAnswer
	 *
	 * @apiSuccessExample JSON answer
	 *     {
	 *       "version": "7.0",
	 *       "isLogged": true,
	 *       "csrf": "xHIASFTRgJsS9i6jQZvk7WkDEQwsd7zRod39NKVJE1c=",
	 *     }
	 */
	public function api_get($f3) {
		$this->checkAccessWithSpec( new MustBeAuthenticated );
		$this->sendJSONResponse();
	}
}
