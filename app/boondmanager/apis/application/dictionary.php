<?php
/**
 * dictionary.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\OldModels\Specifications\RequestAccess\MustBeAuthenticated;

class Dictionary extends AbstractController{
	public function api_get(){

		$this->checkAccessWithSpec(new MustBeAuthenticated());

		$this->sendJSONResponse([
			'data' => BM::getDictSpecific(true, $this->requestAccess->get('language'))
		]);
	}
}
