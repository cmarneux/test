<?php
/**
 * entry.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Dictionary;
use BoondManager\Services\BM;

class DictionaryEntry extends AbstractController {
	public function api_get($f3) {
		$id = $f3->get('PARAMS.id');
		$key = 'specific.'.$f3->get('PARAMS.key');
		if(!is_null($id)) $data = Dictionary::lookupEntry($key, $id);
		else $data = Dictionary::getDict($key);

		if(!$data) $this->error(404);

		$data = [
			'data' => $data
		];

		$this->sendJSONResponse($data);
	}
}
