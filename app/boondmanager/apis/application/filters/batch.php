<?php
/**
 * batch.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputMultiObjects;

/**
 * Class Batch
 * @package BoondManager\Models\Filters
 * @property InputMultiObjects data
 */
class Batch extends AbstractFilters
{
	public function __construct()
	{
		parent::__construct();

		$requests = new InputMultiObjects('data', new BatchEntry());
		$this->addInput($requests);
	}
}
