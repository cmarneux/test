<?php
/**
 * BatchEntry.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application\Filters;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class BatchEntry
 * @package BoondManager\Models\Filters
 * @property InputInt id
 * @property \Wish\Filters\Inputs\InputValue method
 * @property \Wish\Filters\Inputs\InputString api
 * @property InputValue body
 */
class BatchEntry extends AbstractFilters
{
	public function __construct()
	{
		parent::__construct();
		$id = new InputInt('id');

		$method = new InputEnum('method');
		$method->setRequired(true);
		$method->setAllowedValues(['GET', 'PUT', 'POST', 'DELETE']);

		$api = new InputString('api');
		$api->setRequired(true);

		$body = new InputValue('body');

		$this->addInput([$id, $method, $api, $body]);
	}
}
