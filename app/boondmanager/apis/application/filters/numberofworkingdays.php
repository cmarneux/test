<?php
/**
 * weekendandbankholidays.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Calendar;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;

/**
 * Class WeekendAndBankHolidays
 * @property \Wish\Filters\Inputs\InputValue calendar
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputValue productTypes
 * @package BoondManager\Models\Filters
 * @FIXME verifier à quoi sert le champ productTypes
 */
class NumberOfWorkingDays extends AbstractFilters
{
	public function __construct()
	{
		parent::__construct();

		$calendar = new Calendar();
		$calendar->setModeDefaultValue(CurrentUser::instance()->getCalendar());

		$startDate = new InputDate('startDate');
		$startDate->setRequired(true);

		$endDate = new InputDate('endDate');
		$endDate->setRequired(true);

		$productTypes = new InputDict('productTypes', 'specific.setting.typeOf.subscription');

		$this->addInput([$calendar, $startDate, $endDate, $productTypes]);
	}
}
