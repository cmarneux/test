<?php
/**
 * perimeter.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Services\BM;

/**
 * Class Perimeter
 * @package BoondManager\Models\Filters
 * @property InputValue module
 */
class Perimeter extends AbstractFilters
{
	public function __construct()
	{
		parent::__construct();

		$field = new InputEnum('module');
		$field->setAllowedValues( BM::getAllModules() );
		$field->setRequired(true);
		$this->addInput($field);
	}
}
