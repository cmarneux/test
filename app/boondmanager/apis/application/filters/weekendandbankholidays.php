<?php
/**
 * weekendandbankholidays.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Calendar;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use BoondManager\Services\CurrentUser;

/**
 * Class WeekendAndBankHolidays
 * @property \Wish\Filters\Inputs\InputValue calendar
 * @property \Wish\Filters\Inputs\InputDate startDate
 * @property InputDate endDate
 * @package BoondManager\Models\Filters
 */
class WeekendAndBankHolidays extends AbstractFilters
{
	public function __construct()
	{
		parent::__construct();

		$calendar = new Calendar();
		$calendar->setModeDefaultValue(CurrentUser::instance()->getCalendar());

		$startDate = new InputDate('startDate');
		$startDate->setRequired(true);

		$endDate = new InputDate('endDate');
		$endDate->setRequired(true);

		$this->addInput([$calendar, $startDate, $endDate]);
	}
}
