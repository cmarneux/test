<?php
/**
 * perimeters.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\CurrentUser;

class Flags extends AbstractController{

	public function api_get(){
		$flags = CurrentUser::instance()->getFlags();

		foreach($flags as $flag) {
			$flag->mainManager->filterFields(['id', 'firstName', 'lastName']);
			$flag->filterFields(['id', 'name', 'mainManager']);
		}

		$this->sendJSONResponse([
			'data' => $flags
		]);
	}
}
