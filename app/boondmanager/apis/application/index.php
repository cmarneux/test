<?php
/**
 * application.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Services\Devices;
use BoondManager\Services\Dictionary;
use BoondManager\Lib\AbstractController;
use Wish\Email;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use Wish\Tools;
use BoondManager\Services\Login;
use BoondManager\Databases\BoondManager;
use BoondManager\Databases\Local;

/**
 * BoondManager application main controller.
 * @namespace \BoondManager\Controllers\Main
 */
class Index extends AbstractController {

	protected $requireClient = false;

	/**
	 * Controller init.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base).
	 */
	public function beforeRoute($f3) {
		BM::enableNavigatorCheck( false );
		parent::beforeRoute($f3);
	}

	/**
	 * Get user connection status.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a GET request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_get($f3) {
		$tabData = [];
		if($this->requestAccess->get('jwt')) {
			if(Login::isLoggedToZenDesk()) $tabData['meta']['landingPage'] = Login::getLandingPage();
			else Login::prepareLoginZenDesk( $this->requestAccess->get('return_to') );
		} else if(CurrentUser::instance()->isLogged()) {
			$tabData['meta']['landingPage'] = Login::getLandingPage();
		} else if($this->requestAccess->get('company') && is_file($f3->get('MAIN_APPPATH').'/boondmanager/configs/_clients/'.$this->requestAccess->get('company').'.ini')) {
			$dataBDD = new BoondManager\Customer();//Force usage of BoondManager BDD
			if($data = $dataBDD->getCustomerFromCode($this->requestAccess->get('company'))) {//Get customer from this uniq URL
				$tabData['meta']['user']['customer']['code'] = $data->CLIENT_WEB;
				//Check if customer's logo exists
				$img_file = Tools::whichImageWithExtensionExist($f3->get('MAIN_ROOTPATH').'/www/img/_clients/logo/'.$data->CLIENT_WEB);
				$tabData['meta']['user']['customer']['logo'] = ($img_file !== false)?'_clients/logo/'.$img_file:'';
			}
		}
		$this->sendJSONResponse($tabData);
	}

	/**
	 * Log in the user from a known device.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a POST request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_post($f3) {
		Login::deleteLoginData(false);

		$tabData = [];
		// connect using login/password or a saved hash (rememberMe)
		if(($this->requestAccess->get('login') && $this->requestAccess->get('password')) || $this->requestAccess->get('hash')) {
			if($this->requestAccess->get('hash')) {
				$account = Login::connectionFromHash($this->requestAccess->get('hash'));
			} else {
				$login = $this->requestAccess->get('login') ;
				$password = $this->requestAccess->get('password') ;
				$company = BM::loadCustomerInterfaceFromLogin($login, $this->requestAccess->get('company'));
				$pwdEncrypt = Login::saltPassword($password, $login);
				$account = Login::connectionFromLoginPassword($login, $pwdEncrypt, $company, $thirdParty = true);

				// Attention, le front envoi les chaines correspondantes aux booleans !
				if($this->requestAccess->get('rememberMe') === "true") {
					$hash = Login::generateHash(CurrentUser::instance(), $pwdEncrypt);
				}
			}

			if(!$account) {
				$this->error(BM::ERROR_APPLICATION_WRONGIDS);
			}

			$currentUser = CurrentUser::instance();

			$company = BM::getCustomerCode();

			$connectedEmail = Login::getValidEmailFromUserConnection($account);

			// TODO: deplacer la verification du device dans le service Login
			$newDevice = false;

			//Check if this device already exists
			$inputDeviceList = json_decode($this->requestAccess->get('devices'), true);
			$device = Devices::getFromUserAndCookie($currentUser->getUserId(), $inputDeviceList, true);

			// if this device is new or has no name
			if(!$device || $device->name == '') {
				if($account->securityCookie == 1) {
					$newDevice = true;//Ask for activated this device with an authorization code
				} else if($account->securityAlert == 1) {//Ask for sending an email
					Login::sendSecurityAlert($connectedEmail, BM::getBoondManagerUrl($company));
				}
				//If this device is new, and no authorization code is needed, we add it
				if(!$account->securityCookie && !$device) {
					$device = Devices::createDevice(Dictionary::getDict('main.application.device.title'));
					$inputDeviceList[] = [
						'id' => $company.$currentUser->getUserId(),
						'value' => $device->sha
					];
				}
			}

			if($newDevice) { //Management of user's connection from a new device
				//Log out completely the user
				Login::deleteLoginData(true);
				// send email to register the new device
				if(!$f3->exists('REQUEST.hash')) {
					$f3->set('SESSION.device.new', [$account->client, $account->login, $account->password, $connectedEmail, $account->securityAlert]);
					$f3->set('SESSION.device.code', uniqid());

					$email = new Email();
					$email->Send($connectedEmail,
						Dictionary::getDict('main.application.mail.code.objet'),
						Dictionary::getDict('main.application.mail.code.message', $f3->get('SESSION.device.code')),
						$f3->get('BMEMAIL.NOREPLY')
					);
				}
			} else {
				Login::buildComplementaryUserData(); //Save session's parameters
				BM::autoSetModeDemo();
			}

			$tabData['data']['device'] = ['isNew'  => $newDevice];

			if(isset($tabDevices)) {
				$tabData['data']['device']['users'] = $tabDevices;
			}

			if($newDevice && $company && $company == $f3->get('REQUEST.company')) {//We add agency's data if it was defined
				//$tabData['meta']['user']['login'] = $login;

				//Check if customer's logo exists
				//$img_file = Tools::whichImageWithExtensionExist($f3->get('MAIN_ROOTPATH').'/www/img/_clients/logo/'.$company);
				//$tabData['meta']['user']['company']['web'] = $company;
				//$tabData['meta']['user']['company']['logo'] = ($img_file !== false)?'_clients/logo'.$img_file:'';
			}
		} else {
			$this->error(BM::ERROR_APPLICATION_WRONGIDS);
		}
		$account = CurrentUser::instance()->getAccount();

		$tabData = array_merge_recursive($tabData, [
			'data' => [
				'login' => strval($account->login),
				'resource' => [
					'email1' => strval($account->email1)
				]
			]
		]);//We make the data array

		$tabData['meta']['landingPage'] = Login::getLandingPage();

		if(isset($hash)) {
			$tabData['data']['hash'] = strval($hash);
		}

		if($account->agency){
			$tabData['data']['customer'] = [
				'code' => strval($account->agency->code),
				'logo' => $account->agency->getLogo()
			];
		}

		$this->sendJSONResponse($tabData, true, true);
	}

	/**
	 * Log out the user.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a DELETE request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_delete() {
		//TODO : Manage Specifications

		$web = CurrentUser::instance()->isLogged() ? CurrentUser::instance()->getAccount()->agency->code : '';
		if(CurrentUser::instance()->isLogged()) {
			//Clean all data on device's session
			$deviceBDD = Local\Device::instance();
			if($device = Devices::getFromUserAndCookie(CurrentUser::instance()->getUserId(), json_decode($this->requestAccess->get('device'), true)))
				$deviceBDD->updateDevice(['DALL_SERVER' => 0, 'DALL_SESSION' => ''], $device->id);
		}
		Login::deleteLoginData(true);
		$this->sendJSONResponse([
			'meta' => [
				'user' => [
					'company' => ['web' => $web]
				]
			]
		]);
	}
}
