<?php
/**
 * language.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;

/**
 * Class Language
 * @package BoondManager\Controllers\Main\Application
 */
class Language extends AbstractController {

	protected $requireClient = false;

	/**
	 * Change application's language.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a POST request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_post($f3) {
		if(BM::isValidLanguage($f3->get('REQUEST.language'))) {
			BM::setLanguage($f3->get('REQUEST.language'));
			$tabData = [];
			$tabData['meta']['language'] = BM::getLanguage();
			$this->sendJSONResponse($tabData);
		} else
			$this->error(BM::ERROR_APPLICATION_WRONG_LANGUAGE);
	}
}
