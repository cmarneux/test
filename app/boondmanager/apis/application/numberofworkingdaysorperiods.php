<?php
/**
 * weekendandbankholidays.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use Wish\Tools;
use BoondManager\Lib\AbstractController;

class NumberOfWorkingDaysOrPeriods extends AbstractController{
	public function api_get(){

		$filter = new  Filters\NumberOfWorkingDays();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		$days = Tools::getNumberOfWorkingDays($filter->startDate->getValue(), $filter->endDate->getValue(), $filter->calendar->getValue());

		$this->sendJSONResponse([
			'data' => $days
		]);
	}
}
