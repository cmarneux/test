<?php
/**
 * perimeters.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\APIs\Application\Filters\Perimeter;
use BoondManager\Services\CurrentUser;

class Perimeters extends AbstractController{

	public function api_get(){

		$filter = new Perimeter();
		$filter->setData($this->requestAccess->getParams());
		$this->checkFilter($filter);

		$perimetre = CurrentUser::instance()->getSearchPerimeter($filter->module->getValue());

		foreach($perimetre->getManagers() as $manager)
			$manager->filterFields(['id', 'firstName', 'lastName']);

		foreach($perimetre->getPoles() as $pole)
			$pole->filterFields(['id', 'name']);

		foreach($perimetre->getBUs() as $bu)
			$bu->filterFields(['id', 'name']);

		foreach($perimetre->getAgencies() as $agency)
			$agency->filterFields(['id', 'name']);

		$this->sendJSONResponse([
			'data' => $perimetre
		]);
	}
}
