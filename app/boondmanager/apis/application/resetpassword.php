<?php
/**
 * resetpassword.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Login;
use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;

/**
 * Class ResetPassword
 * @package BoondManager\Controllers\Main\Application
 */
class ResetPassword extends AbstractController {

	protected $requireClient = false;
	/**
	 * Reset password.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a POST request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_post($f3) {

		//Check that reset's request is valid OR reset the password
		$isAccessible = false;
		$tabData = [];

		if($f3->exists('REQUEST.chki') && $f3->exists('REQUEST.chkc') && $f3->get('REQUEST.chki') != '' && $f3->get('REQUEST.chkc') != '') {
			//Back up the old config
			$oldCustomerCode = BM::getCustomerCode();

			//Load the request config
            $configFile = BM::loadCustomerInterfaceFromLogin($f3->get('REQUEST.login'), $f3->get('REQUEST.company'));

			$userBDD = $configFile ? new Local\Account() : new BoondManager\Account();
			if($user = $userBDD->getAccount($f3->get('REQUEST.chki'))) {
				if(Login::getResetPasswordHash($configFile, $user) == $f3->get('REQUEST.chkc')) {
					$isAccessible = true;
					$tabData['meta']['login'] = $user->login;
					if ($f3->exists('REQUEST.pwd') && $f3->exists('REQUEST.cpwd') && $f3->get('REQUEST.pwd') != '' && $f3->get('REQUEST.cpwd') != '' && strlen($f3->get('REQUEST.pwd')) >= 5) {
						$tabData['meta']['id'] = $userBDD->updateAccount([
							'USER' => [
								'USER_PWD' => Login::saltPassword($f3->get('REQUEST.pwd'), $user->login)
							]
						], $user->id);//Update password
					}
				}
			}

			//Restaure the old config
			BM::loadCustomerInterface($oldCustomerCode);
        }
        if(!$isAccessible) $this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);	//Wrong parameters to reset password
        $this->sendJSONResponse($tabData);
    }
}
