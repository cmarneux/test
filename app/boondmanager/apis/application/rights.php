<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use Wish\Exceptions;

class Rights extends AbstractController {
	public function api_get() {
		$filter = new Filters\Rights();
		$filter->setData($this->requestAccess->getParams());
		$this->checkFilter($filter);

		if($filter->id->getValue()) {
			try {
				$data = Services\Rights::getEntityRights($filter->module->getValue(), $filter->id->getValue());
			} catch(Exceptions\NotFound $e) {
				$this->error(404);
			}
		} else
			$data = Services\Rights::getModulesRights( $filter->module->getValue());

		$this->sendJSONResponse([
			'data' => $data
		]);
	}
}
