<?php
/**
 * sendmail.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use Wish\Email;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Services\Login;
use BoondManager\Databases\Local;

/**
 * Class SendMail
 * @package BoondManager\APIs\Application
 */
class SendMail extends AbstractController {

	protected $requireClient = false;

	/**
	 * @param \Base $f3
	 */
	public function api_post($f3) {
		$tabData = [];
		$errCode = null;
		if($f3->exists('REQUEST.login') && $f3->get('REQUEST.login') != '') {//If data are correct, we send instructions email
			//Back up the old config
			$oldCustomerCode = BM::getCustomerCode();

			//Load the request config
			$configFile = BM::loadCustomerInterfaceFromLogin($f3->get('REQUEST.login'), $f3->get('REQUEST.company'));

			if($configFile) {
				$userBDD = new Local\Account();
				if($user = $userBDD->getAccountFromLogin($f3->get('REQUEST.login'))) {
					if(BM::validateCaptcha($f3->get('REQUEST.captcha'))) {
						$tabData['meta']['email'] = Login::getValidEmailFromUserConnection($user);
						if($tabData['meta']['email'] != '') {
							$tabData['meta']['company'] = $configFile;
							$email                      = new Email();
							$token                      = Login::getResetPasswordHash($configFile, $user);

							//The reset key is product of URL sub-domain, login, string "resetpwd", user's identifier & last connection date (Thus, this key is uniq)
							$tabData['meta']['mailSent'] = $email->Send($tabData['meta']['email'], Dictionary::getDict('main.application.mail.reset.objet'), Dictionary::getDict('main.application.mail.reset.message', $f3->get('BOONDMANAGER.APP_URL') . "/resetpassword?company=$configFile&chki={$user->id}&chkc=$token"), $f3->get('BMEMAIL.NOREPLY')); //FIXME for debug only;
						} else $errCode = BM::ERROR_GLOBAL_NO_VALID_EMAIL;
					} else $errCode = BM::ERROR_APPLICATION_WRONG_CAPTCHA;
				} else $errCode = BM::ERROR_APPLICATION_LOGIN_DOES_NOT_EXIST;
			} else $errCode = BM::ERROR_APPLICATION_LOGIN_DOES_NOT_EXIST;

			//Restaure the old config
			BM::loadCustomerInterface($oldCustomerCode);

		} else $errCode = BM::ERROR_APPLICATION_LOGIN_DOES_NOT_EXIST;

		if($errCode)
			$this->error($errCode);
		else
			$this->sendJSONResponse($tabData);
	}
}
