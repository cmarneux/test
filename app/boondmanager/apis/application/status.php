<?php
/**
 * status.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;

/**
 * Class Status
 * @package BoondManager\Controllers\Main\Application
 */
class Status extends AbstractController{

	protected $requireClient = false;

	public function api_get() {
		$this->sendJSONResponse([], true, false);
	}
}
