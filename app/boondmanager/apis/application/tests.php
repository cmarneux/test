<?php
/**
 * test.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Application;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;

/**
 * BoondManager Tests API controller.
 * @namespace \BoondManager\Controllers\Main
 */
class Tests extends AbstractController {
	/**
	 * Controller init.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base).
	 */
	public function beforeRoute($f3) {
		if(!BM::isDevelopmentMode()) $this->error(404);	//This URL is only accessible in Test Mode
		BM::enableNavigatorCheck( false );
		$f3->clear('SESSION');					//These API are Session State Less

		$tabRequest = explode('/', $f3->get('PARAMS.1'));

		list($category, $controller, $action) = $tabRequest;
		if($category == 'UnitTesting'){
			$category = array_shift($tabRequest);
			$action = array_pop($tabRequest);
			$controller = implode('\\', $tabRequest);
		}

		$f3->set('PARAMS.category', $category);
		$f3->set('PARAMS.controller', $controller);
		$f3->set('PARAMS.action', $action);

		$f3->set('REQUEST.login', 'vhugo');
		$f3->set('REQUEST.password', 'vhugo');

		parent::beforeRoute($f3);
	}
	/**
	 * Manage this API call.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a GET|POST|PUT|DELETE request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_call($f3) {
		$test = new \Test;
		$f3->set('QUIET',TRUE);  //Do not show output of the active route

		$action = $f3->get('PARAMS.action')?$f3->get('PARAMS.action'):'get';
		$category = $f3->get('PARAMS.category');
		$controller = $f3->get('PARAMS.controller');

		$class = "\\BoondManager\\Tests\\$category\\$controller";
		if(!class_exists($class)) $this->error(404);
		$f3->call('\\BoondManager\\Tests\\'.$category.'\\'.$controller.'->api_'.$action, array($f3, $test)); //Execute the controller

		$f3->set('QUIET',FALSE); //Allow test results to be shown later
		$f3->clear('ERROR');  //Clear any errors

		header('Content-Type: text/html');
		$testName = "$category/$controller/$action";
		echo '<h3 class="testName">GET /'.$testName.'</h3>';
		echo '<table>';
		foreach ($test->results() as $result) {
			echo '<tr><td class="test">'.$result['text'].'</td><td width="100px" style="text-align:center">';
			if($result['status'])
				echo '<span style="color:#00FF00" class="result successful"><b>OK</b></span>';
			else
				echo '<span style="color:#FF0000" class="result failed"><b>KO</b></span></td><td><span style="font-size: 13px"><i class="source">'.$result['source'].'</i></span>';
			echo '</td></tr>';
		}
		echo '</table>';
		$f3->clear('SESSION');	//These API are Session State Less
	}

	/**
	 * get the list of all class' tests to run
	 * @param string $pathSeparator
	 * @return array
	 */
	private function getOrdonnanceur($pathSeparator ='\\') {

		//Ordonnanceur
		$plan = [
			'UnitTesting'=>[
				'Lib\Tools',
				'Models\Dictionary',
				'Models\Services\Actions',
			]
		];

		$return = [];
		foreach($plan as $category=>$controllers)
			foreach($controllers as $controller)
				$return[] = implode($pathSeparator, [$category, $controller]);

		return $return;
	}


	/**
	 * run all the tests (if a "noajax" parameter is given, all tests will be run before a render)
	 * @param \Base $f3
	 */
	public function api_all($f3) {
		if(!$f3->exists('REQUEST.noajax')) {
			$this->renderAjax();
			return;
		}

		// this can take a very long time ...
		set_time_limit(0);

		$plan = $this->getOrdonnanceur();

		$test = new \Test;
		$f3->set('QUIET', TRUE);  //Do not show output of the active route
		foreach($plan as $class) {
			$fullClassName = '\BoondManager\Tests\\'.$class;
			$methods = get_class_methods($fullClassName);
			//var_dump($fullClassName, $methods);
			foreach($methods as $method) {
				$f3->clear('SESSION');
				$f3->call($fullClassName.'->api_'.$method, array($f3, $test)); //Execute the controller
				$f3->clear('ERROR');  //Clear any errors
			}
		}
		$f3->set('QUIET', FALSE); //Allow test results to be shown later

		header('Content-Type: text/html');
		echo '<h3>All Tests</h3>';
		echo '<table>';
		foreach ($test->results() as $result) {
			echo '<tr><td class="test">'.$result['text'].'</td><td width="100px" style="text-align:center">';
			if($result['status'])
				echo '<span style="color:#00FF00" class="result successful"><b>OK</b></span>';
			else
				echo '<span style="color:#FF0000" class="result failed"><b>KO</b></span></td><td><span style="font-size: 13px"><i class="source">'.$result['source'].'</i></span>';
			echo '</td></tr>';
		}
		echo '</table>';

	}

	/**
	 * execute all test using ajax
	 */
	private function renderAjax() {
		$plan = $this->getOrdonnanceur();

		$paths = [];
		foreach($plan as $class) {
			$fullClassName = '\BoondManager\Tests\\'.$class;
			$methods = get_class_methods($fullClassName);
			foreach($methods as $m) {
				$paths[] = str_replace('\\','/', $class.'\\'.$m);
			}
		}

		$view = \View::instance();
		echo $view->render('../F3/boondmanager/views/tests.ajax.phtml', null , ['paths'=>$paths]);
	}
}
