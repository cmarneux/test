<?php
/**
 * weekendandbankholidays.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Application;

use Wish\Tools;
use BoondManager\Lib\AbstractController;

class WeekendAndBankHolidays extends AbstractController{
	public function api_get(){

		$filter = new  Filters\WeekendAndBankHolidays();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		$dates = [];
		$startYear = $filter->startDate->getYear();
		$endYear = $filter->endDate->getYear();
		for($y = $startYear; $y <= $endYear; $y++){
			$holidays = Tools::getBankHolidays($filter->calendar->getValue(), $y);

			$endDate = ($y === $endYear) ? $filter->endDate->getDateTime() : new \DateTime("$y-12-31");
			$date = ($y === $startYear) ? $filter->startDate->getDateTime() : new \DateTime("$y-1-1");

			$days = $endDate->diff($date)->days;

			for($d = 0; $d <= $days; $d++){
				if(in_array($date->format('j_n_Y'), $holidays)) $type = 'bankHoliday';
				else if(in_array($date->format('D'), ['Sat','Sun'])) $type = 'weekend';
				else $type = 'normal';

				$dates[] = [
					'date' => $date->format('Y-m-d'),
					'typeOf' => $type
				];

				$date->add( new \DateInterval("P1D") );
			}
		}

		$this->sendJSONResponse([
			'data' => $dates
		]);
	}
}
