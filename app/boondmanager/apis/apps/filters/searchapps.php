<?php
/**
 * apps.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Apps\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;

/**
 * Class SearchApps
 *
 * @property InputMultiValues typeOf
 * @property InputBoolean validation
 * @property InputBoolean visibility
 * @property InputBoolean myApps
 * @property InputMultiValues sort
 * @package BoondManager\APIs\Apps\Filters
 */
class SearchApps extends AbstractSearch {
	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_NAME = 'name',
		ORDERBY_VALIDATION = 'validation',
		ORDERBY_VENDOR = 'vendor.name',
		ORDERBY_NOTE = 'note';
	/**#@-*/

	/**
	 * Apps constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		throw new \Exception('Gérer les droits sur les filtres');

		$input = new InputMultiDict('typeOf', 'specific.setting.typeOf.app');
		$this->addInput($input);

		$input = new InputBoolean('validation');
		$input->setModeDefaultValue(true);
		$this->addInput($input);

		$input = new InputBoolean('visibility');
		$input->setModeDefaultValue(true);
		$this->addInput($input);

		$input = new InputBoolean('myApps');
		$input->setModeDefaultValue(false);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_NAME, self::ORDERBY_VALIDATION, self::ORDERBY_VENDOR, self::ORDERBY_NOTE
		]);
		$this->addInput($input);
	}
}
