<?php
/**
 * apps.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Apps;

use BoondManager\APIs\Projects\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use Wish\Models\Model;
use BoondManager\Services\Apps;
use BoondManager\APIs\Apps\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\Apps
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'name'
	];

	/**
	 * Search apps
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$filter = new Filters\SearchApps();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = Apps::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
