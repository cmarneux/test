<?php
/**
 * abstractappr.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Apps\Specifications;

use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\App;
use BoondManager\Lib\RequestAccess;

abstract class AbstractApp extends AbstractSpecificationItem {
	/**
	 * Get the customer from the request
	 * @param RequestAccess $request
	 * @return App|null
	 */
	public function getApp($request) {
		if($request->data instanceof App) return $request->data;
		else return null;
	}

	/**
	 * Get read & write access
	 * @param CurrentUser $user
	 * @param App $app
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($user, $app) {
		list($read, $write) = [true, true];
		return [$read, $write];
	}
}
