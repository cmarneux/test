<?php
/**
 * havesearchaccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Apps\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveSearchAccess
 * @package BoondManager\APIs\Projects\Specifications
 */
class HaveSearchAccess extends AbstractApp{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		throw new \Exception('Gérer les droits sur les spécifications'); //TODO
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		return true;
	}
}
