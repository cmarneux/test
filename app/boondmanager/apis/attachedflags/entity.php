<?php
/**
 * entity.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\AttachedFlags;

use BoondManager\APIs\AttachedFlags\Specifications\HaveCreateAccess;
use BoondManager\APIs\AttachedFlags\Specifications\HaveDeleteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\AttachedFlags;
use BoondManager\APIs\AttachedFlags\Filters;

/**
 * Class Entity
 * @package BoondManager\APIs\AttachedFlags
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'dependsOn' => [
			'id'
		],
		'flag' => [
			'id'
		]
	];

	/**
	 * Delete an attached flag
	 */
	public function api_delete() {

		$attachedFlag = AttachedFlags::get($this->requestAccess->id);
		if(!$attachedFlag) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $attachedFlag);

		if(AttachedFlags::delete($attachedFlag))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}
}
