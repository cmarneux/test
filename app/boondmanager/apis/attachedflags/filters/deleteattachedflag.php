<?php
/**
 * deleteattachedflag.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\AttachedFlags\Filters;

use BoondManager\Services\Actions;
use BoondManager\Services\BM;
use BoondManager\Services\Candidates;
use BoondManager\Services\Companies;
use BoondManager\Services\Contacts;
use BoondManager\Services\Employees;
use BoondManager\Services\Flags;
use BoondManager\Services\Invoices;
use BoondManager\Services\Opportunities;
use BoondManager\Services\Orders;
use BoondManager\Services\Positionings;
use BoondManager\Services\Products;
use BoondManager\Services\Projects;
use BoondManager\Services\Purchases;
use Wish\Filters\AbstractFilters;
use BoondManager\Models;
use Wish\Filters\Inputs\InputValue;

/**
 * Class DeleteAttachedFlag
 * @property InputValue company
 * @property InputValue opportunity
 * @property InputValue project
 * @property InputValue order
 * @property InputValue product
 * @property InputValue purchase
 * @property InputValue action
 * @property InputValue employee
 * @property InputValue candidate
 * @property InputValue positioning
 * @property InputValue contact
 * @property InputValue invoice
 * @property InputValue flag
 * @package BoondManager\APIs\AttachedFlags\Filters
 */
class DeleteAttachedFlag extends AbstractFilters {
	/**
	 * @var Models\Flag
	 */
	protected $attachedFlag;

	/**
	 * Information constructor.
	 * @param Models\AttachedFlag|null $attachedFlag
	 */
	public function __construct(Models\AttachedFlag $attachedFlag = null) {
		parent::__construct();

		$this->attachedFlag = $attachedFlag;

		$input = new InputValue('company');
		$input->addFilterCallback(function ($value) {
			return Companies::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('opportunity');
		$input->addFilterCallback(function ($value) {
			return Opportunities::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('project');
		$input->addFilterCallback(function ($value) {
			return Projects::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('order');
		$input->addFilterCallback(function ($value) {
			return Orders::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('product');
		$input->addFilterCallback(function ($value) {
			return Products::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('purchase');
		$input->addFilterCallback(function ($value) {
			return Purchases::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('action');
		$input->addFilterCallback(function ($value) {
			return Actions::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('employee');
		$input->addFilterCallback(function ($value) {
			return Employees::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('candidate');
		$input->addFilterCallback(function ($value) {
			return Candidates::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('positioning');
		$input->addFilterCallback(function ($value) {
			return Positionings::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('contact');
		$input->addFilterCallback(function ($value) {
			return Contacts::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('invoice');
		$input->addFilterCallback(function ($value) {
			return Invoices::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new InputValue('flag');
		$input->addFilterCallback(function ($value) {
			return Flags::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 *
	 */
	protected function postValidation() {
		$nbObjectDefined = 0;

		if($this->company->isDefined()) $nbObjectDefined++;
		if($this->opportunity->isDefined()) $nbObjectDefined++;
		if($this->project->isDefined()) $nbObjectDefined++;
		if($this->order->isDefined()) $nbObjectDefined++;
		if($this->product->isDefined()) $nbObjectDefined++;
		if($this->purchase->isDefined()) $nbObjectDefined++;
		if($this->action->isDefined()) $nbObjectDefined++;
		if($this->employee->isDefined()) $nbObjectDefined++;
		if($this->candidate->isDefined()) $nbObjectDefined++;
		if($this->positioning->isDefined()) $nbObjectDefined++;
		if($this->contact->isDefined()) $nbObjectDefined++;
		if($this->invoice->isDefined()) $nbObjectDefined++;

		if($nbObjectDefined != 1)
			$this->invalidate(Models\AttachedFlag::ERROR_ATTACHEDFLAGS_WRONG_QUERY_PARAMETERS);
	}
}


