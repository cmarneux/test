<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\AttachedFlags\Filters;

use BoondManager\Services\Actions;
use BoondManager\Services\BM;
use BoondManager\Services\Candidates;
use BoondManager\Services\Companies;
use BoondManager\Services\Contacts;
use BoondManager\Services\Employees;
use BoondManager\Services\Invoices;
use BoondManager\Services\Opportunities;
use BoondManager\Services\Orders;
use BoondManager\Services\Positionings;
use BoondManager\Services\Products;
use BoondManager\Services\Projects;
use BoondManager\Services\Purchases;
use Wish\Filters\AbstractJsonAPI;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Models;
use Wish\Filters\Inputs\InputRelationship;

/**
 * Class Entity
 * @property InputRelationship dependsOn
 * @property Relationships\Flag flag
 * @package BoondManager\APIs\AttachedFlags\Filters
 */
class Entity extends AbstractJsonAPI {
	/**
	 * @var Models\Flag
	 */
	protected $attachedFlag;

	/**
	 * Information constructor.
	 * @param Models\AttachedFlag|null $attachedFlag
	 */
	public function __construct(Models\AttachedFlag $attachedFlag = null) {
		parent::__construct();

		$this->attachedFlag = $attachedFlag;

		$input = new InputRelationship('dependsOn');
		$input->addFilterCallback(function ($value) {
			/**
			 * @var InputRelationship $this
			 */
			//TODO : Instancier en constante $_jsonType afin de pouvoir la traiter dans ce type de switch => Tanguy Lambert
			switch($this->getType()) {
				case 'company': return Companies::find($value);break;
				case 'opportunity': return Opportunities::find($value);break;
				case 'project': return Projects::find($value);break;
				case 'order': return Orders::find($value);break;
				case 'product': return Products::find($value);break;
				case 'purchase': return Purchases::find($value);break;
				case 'action': return Actions::find($value);break;
				case 'resource': return Employees::find($value);break;
				case 'candidate': return Candidates::find($value);break;
				case 'positioning': return Positionings::find($value);break;
				case 'contact': return Contacts::find($value);break;
				case 'invoice': return Invoices::find($value);break;
			}
			return false;
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$this->addInput($input);

		$input = new Relationships\Flag();
		$input->setRequired(true);
		$this->addInput($input);

		if($this->isCreation())
			$this->adaptForCreation();
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->attachedFlag->id ? false : true;
	}

	/**
	 * Set inputs for a creation
	 */
	private function adaptForCreation() {
		$this->dependsOn->setRequired(true);
	}
}


