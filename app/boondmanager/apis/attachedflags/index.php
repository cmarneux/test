<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\AttachedFlags;

use BoondManager\APIs\AttachedFlags\Specifications\HaveCreateAccess;
use BoondManager\APIs\AttachedFlags\Specifications\HaveDeleteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\AttachedFlags;
use BoondManager\APIs\AttachedFlags\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\AttachedFlags
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'dependsOn' => [
			'id'
		],
		'flag' => [
			'id'
		]
	];

	/**
	 * Create an attached flag
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default project
		$attachedFlag = AttachedFlags::buildFromFilter($filter);

		if(!$attachedFlag) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $attachedFlag);

		if(AttachedFlags::create($attachedFlag)) {
			$this->sendJSONResponse([
				'data' => $attachedFlag->filterFields(self::ALLOWED_FIELDS)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}

	/**
	 * Delete an attached flag
	 */
	public function api_delete() {
		$filter = new Filters\DeleteAttachedFlag();
		$filter->setData($this->requestAccess->getParams());
		$this->checkFilter($filter);

		$attachedFlag = AttachedFlags::getFromEntity($filter);
		if(!$attachedFlag) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $attachedFlag);

		if(AttachedFlags::delete($attachedFlag))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}
}
