<?php
/**
 * abstractattachedflag.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\AttachedFlags\Specifications;

use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Models\AttachedFlag;
use BoondManager\Lib\RequestAccess;

abstract class AbstractAttachedFlag extends AbstractSpecificationItem {
	/**
	 * Get the flag from the request
	 * @param RequestAccess $request
	 * @return AttachedFlag|null
	 */
	public function getAttachedFlag($request) {
		if($request->data instanceof AttachedFlag) return $request->data;
		else return null;
	}

	/**
	 * Get read & write access
	 * @param CurrentUser $user
	 * @param AttachedFlag $attachedFlag
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($user, $attachedFlag) {
		list($read, $write) = [false, false, false];

		$hierarchyAccess = $user->checkHierarchyAccess(
			[0, 0, $attachedFlag->flag->getManagerID()],
			BM::MODULE_FLAGS
		);

		switch ($hierarchyAccess) {
			case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY:case BM::PROFIL_ACCESS_READ_WRITE:
			$read = true;
			$write = true;
			break;
		}

		return [$read, $write];
	}
}
