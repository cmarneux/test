<?php
/**
 * HaveWriteAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\AttachedFlags\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveWriteAccess
 * @package BoondManager\APIs\AttachedFlags\Specifications
 */
class HaveWriteAccess extends AbstractAttachedFlag{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		//Check this API authorization & access
		if(!$user->isManager() || !$user->hasAccess(BM::MODULE_FLAGS)) return false;

		//Check this API access (cf. V6 => listeflags.php => isAccessible & majView)
		$attachedFlag = $this->getAttachedFlag($request);
		if(!$attachedFlag) return false;

		list($read, $write) = $this->getReadWriteAccess($user, $attachedFlag);
		return $write;
	}
}
