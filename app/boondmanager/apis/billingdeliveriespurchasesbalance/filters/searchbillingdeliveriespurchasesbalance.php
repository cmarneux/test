<?php
/**
 * billingdeliveriespurchasesbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingDeliveriesPurchasesBalance\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class BillingDeliveriesPurchasesBalance
 * build a filter for lists/billingdeliveriespurchasesbalance (search & sort)
 */
class SearchBillingDeliveriesPurchasesBalance extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_BILLING;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_INPROGRESS = 'inProgress',
		PERIOD_ADDITIONALDATAINPROGRESS = 'additionalDataInProgress';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_ENDDATE = 'endDate',
		ORDERBY_ID = 'id',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_ORDER_REFERENCE = 'order.reference',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingDeliveriesPurchasesBalance constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addInput(new InputMultiDict('projectStates', 'specific.setting.state.project'));
		$this->addInput(new InputMultiDict('projectTypes', 'specific.setting.typeOf.project'));

		$period = new InputEnum('period');
		$period->setModeDefaultValue( BM::INDIFFERENT );
		$period->setAllowedValues([self::PERIOD_INPROGRESS, self::PERIOD_ADDITIONALDATAINPROGRESS]);
		$this->addInput($period);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_STARTDATE, self::ORDERBY_ENDDATE, self::ORDERBY_ID, self::ORDERBY_MAINMANAGER_LASTNAME,
			self::ORDERBY_PROJECT_COMPANY_NAME, self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_PROJECT_REFERENCE,
			self::ORDERBY_ORDER_REFERENCE
		]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}
}
