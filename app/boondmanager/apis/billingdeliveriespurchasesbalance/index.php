<?php
/**
 * billingdeliveriespurchasesbalance.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingDeliveriesPurchasesBalance;
use BoondManager\Lib\AbstractController;
use BoondManager\APIs\BillingDeliveriesPurchasesBalance\Specifications\HaveSearchAccess;
use BoondManager\Services\BillingDeliveriesPurchasesBalance;
use BoondManager\APIs\BillingDeliveriesPurchasesBalance\Filters;
use Wish\Models\Model;

/**
 * BillingDeliveriesPurchasesBalance list controller.
 * @package BillingDeliveriesPurchasesBalance
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'title',
		'numberOfOrders',
		'numberOfCorrelatedOrders',
		'dependsOn' => [
			'id',
			// product
			'name',
			// employee
			'lastName',
			'firstName',
		],
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'mainManager' => [
				'id',
				'firstName',
				'lastName',
			],
			'opportunity' => [
				'id',
				'title',
			],
			'contact' => [
				'id',
				'firstName',
				'lastName',
			],
			'company' => [
				'id',
				'name',
			],
		],
		'order' => [
			'number',
			'reference'
		],
	];

	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchBillingDeliveriesPurchasesBalance();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = BillingDeliveriesPurchasesBalance::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}
}
