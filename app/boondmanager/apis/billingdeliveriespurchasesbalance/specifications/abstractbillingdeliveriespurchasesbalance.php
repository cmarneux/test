<?php
/**
 * abstractbillingdeliveriespurchasesbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingDeliveriesPurchasesBalance\Specifications;

use BoondManager\Models\Delivery;
use BoondManager\Models\Purchase;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;

abstract class AbstractBillingDeliveriesPurchasesBalance extends AbstractSpecificationItem{
	/**
	 * get the billingdeliveriespurchasesbalance from the request
	 * @param RequestAccess $request
	 */
	public function getBillingDeliveriesPurchasesBalance($request){
		if($request->data instanceof Delivery || $request->data instanceof Purchase) return $request->data;
		else return null;
	}
}
