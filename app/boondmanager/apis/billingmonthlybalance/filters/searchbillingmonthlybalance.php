<?php
/**
 * billingmonthlybalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\BillingMonthlyBalance\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Lib\Filters\Inputs\InputMonth;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;

/**
 * Class BillingMonthlyBalance
 * build a filter for lists/billingmonthlybalance (search & sort)
 */
class SearchBillingMonthlyBalance extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_BILLING;

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_NUMBEROFINVOICES = 'numberOfInvoices',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingMonthlyBalance constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$orderPaymentMethods = new InputMultiDict('orderPaymentMethods', 'specific.setting.paymentMethod');
		$this->addInput($orderPaymentMethods);

		$orderStates = new InputMultiDict('orderStates', 'specific.setting.state.order');
		$this->addInput($orderStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiDict('projectTypes', 'specific.setting.typeOf.project');
		$this->addInput($projectTypes);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_CREATIONDATE, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_PROJECT_COMPANY_NAME,
			self::ORDERBY_PROJECT_REFERENCE, self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_NUMBEROFINVOICES
		]);
		$this->addInput($sort);

		$startMonth = new InputMonth('startMonth');
		$startMonth->setRequired(true);
		$this->addInput($startMonth);
	}
}
