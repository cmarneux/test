<?php
/**
 * billingmonthlybalance.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingMonthlyBalance;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\BillingMonthlyBalance\Specifications\HaveSearchAccess;
use BoondManager\Services\BillingMonthlyBalance;
use BoondManager\APIs\BillingMonthlyBalance\Filters;
use Wish\Models\Model;

/**
 * BillingMonthlyBalance list controller.
 * @package BillingMonthlyBalance
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'number',
		'reference',
		'numberOfInvoices',
		'turnoverProductionExcludingTax',
		'turnoverInvoicedExcludingTax',
		'deltaProductionExcludingTax',
		'mainManager' => [
			'id',
			'firstName',
			'lastName',
		],
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'opportunity' => [
				'id',
				'title',
			],
			'contact' => [
				'id',
				'firstName',
				'lastName',
			],
			'company' => [
				'id',
				'name',
			],
		],
	];

	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchBillingMonthlyBalance();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		$result = BillingMonthlyBalance::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverProductionExcludingTax' => $result->turnoverProductionExcludingTax,
					'turnoverInvoicedExcludingTax' => $result->turnoverInvoicedExcludingTax,
					'deltaProductionExcludingTax' => $result->deltaProductionExcludingTax,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}
}
