<?php
/**
 * UserHaveSearchAccess.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingMonthlyBalance\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on invoices
 *
 * @package BoondManager\Models\Specifications\RequestAccess\BillingMonthlyBalance
 */
class HaveSearchAccess extends AbstractBillingMonthlyBalance{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		//CONFORME v6 !
		return $user->hasAccess(BM::MODULE_BILLING);
	}
}
