<?php
/**
 * billingprojectsbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\BillingProjectsBalance\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;

/**
 * Class BillingProjectsBalance
 * build a filter for lists/billingprojectsbalance (search & sort)
 */
class SearchBillingProjectsBalance extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_BILLING;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_CREATEDORWITHADDITIONALDATAINPROGRESS= 'createdOrWithAdditionalDataInProgress';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_NUMBEROFORDERS = 'numberOfOrders',
		ORDERBY_COMPANY_NAME= 'company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingProjectsBalance constructor.
	 * @param Perimeter|null $perimeter
	 * @param FlagsList|null $flags
	 */
	public function __construct(Perimeter $perimeter = null, FlagsList $flags = null)
	{
		parent::__construct();

		$projectStates = new InputMultiDict('projectStates', 'specific.setting.state.project');
		$this->addInput($projectStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiDict('projectTypes', 'specific.setting.typeOf.project');
		$this->addInput($projectTypes);

		$period = new InputEnum('period', BM::INDIFFERENT);
		$period->setAllowedValues([self::PERIOD_CREATED, self::PERIOD_CREATEDORWITHADDITIONALDATAINPROGRESS]);
		$this->addInput($period);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([self::ORDERBY_CREATIONDATE, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_COMPANY_NAME, self::ORDERBY_REFERENCE,
			self::ORDERBY_STATE, self::ORDERBY_NUMBEROFORDERS]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}
}
