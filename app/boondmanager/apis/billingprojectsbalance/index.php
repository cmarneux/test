<?php
/**
 * billingprojectsbalance.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingProjectsBalance;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\BillingProjectsBalance\Specifications\HaveSearchAccess;
use BoondManager\Services\BillingProjectsBalance;
use BoondManager\APIs\BillingProjectsBalance\Filters;
use Wish\Models\Model;

/**
 * BillingProjectsBalance list controller.
 * @package BillingProjectsBalance
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'typeOf',
		'mode',
		'reference',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'turnoverSignedExcludingTax',
		'turnoverOrderedExcludingTax',
		'deltaOrderedExcludingTax',
		'numberOfOrders',
		'mainManager' => [
			'id',
			'firstName',
			'lastName',
		],
		'opportunity' => [
			'id',
			'title',
		],
		'contact' => [
			'id',
			'firstName',
			'lastName',
		],
		'company' => [
			'id',
			'name',
		],
	];

	/**
	 * Search billingprojectsbalance.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a post request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 *
	 * @api {get} /billingprojectsbalance Search billingprojectsbalance
	 * @apiName billingprojectsbalance/search
	 * @apiGroup BillingProjectsBalance
	 * @apiVersion 1.0.0
	 *
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchBillingProjectsBalance();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = BillingProjectsBalance::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverOrderedExcludingTax' => $result->turnoverOrderedExcludingTax,
					'turnoverSignedExcludingTax' => $result->turnoverSignedExcludingTax,
					'deltaOrderedExcludingTax' => $result->deltaOrderedExcludingTax,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}
}
