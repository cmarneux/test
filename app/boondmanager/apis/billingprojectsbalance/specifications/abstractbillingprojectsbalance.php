<?php
/**
 * abstractbillingprojectsbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingProjectsBalance\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Order;

abstract class AbstractBillingProjectsBalance extends AbstractSpecificationItem{
	/**
	 * get the billingprojectsbalance from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Order|null
	 */
	public function getBillingProjectsBalance($request){
		if($request->data instanceof Order) return $request->data;
		else return null;
	}
}
