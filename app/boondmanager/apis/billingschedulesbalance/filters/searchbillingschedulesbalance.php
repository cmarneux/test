<?php
/**
 * billingschedulesbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\BillingSchedulesBalance\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;

/**
 * Class BillingSchedulesBalance
 * build a filter for lists/billingschedulesbalance (search & sort)
 */
class SearchBillingSchedulesBalance extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_BILLING;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_PAYMENTDATE = 'paymentDate';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_NUMBEROFINVOICES = 'numberOfInvoices',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_TURNOVERINVOICEDINCLUDEDTAX = 'turnoverInvoicedExcludedTax',
		ORDERBY_TURNOVERTERMOFPAYMENTEXCLUDEDTAX = 'turnoverTermOfPaymentExcludedTax',
		ORDERBY_DELTAINVOICEDEXCLUDEDTAX = 'deltaOrderdExcludedTax',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingSchedulesBalance constructor.
	 * @param Perimeter|null $perimeter
	 * @param FlagsList|null $flags
	 */
	public function __construct(Perimeter $perimeter = null, FlagsList $flags = null)
	{
		parent::__construct();

		$orderPaymentMethods = new InputMultiDict('orderPaymentMethods', 'specific.setting.paymentMethod');
		$this->addInput($orderPaymentMethods);

		$orderStates = new InputMultiDict('orderStates', 'specific.setting.state.order');
		$this->addInput($orderStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiDict('projectTypes', 'specific.setting.typeOf.project');
		$this->addInput($projectTypes);

		$period = new InputEnum('period');
		$period->setModeDefaultValue( BM::INDIFFERENT );
		$period->setAllowedValues([self::PERIOD_PAYMENTDATE]);
		$this->addInput($period);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_CREATIONDATE, self::ORDERBY_DELTAINVOICEDEXCLUDEDTAX,
			self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_PROJECT_COMPANY_NAME, self::ORDERBY_PROJECT_REFERENCE,
			self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_NUMBEROFINVOICES, self::ORDERBY_TURNOVERINVOICEDINCLUDEDTAX,
			self::ORDERBY_TURNOVERTERMOFPAYMENTEXCLUDEDTAX
		]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}
}
