<?php
/**
 * billingschedulesbalance.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\BillingSchedulesBalance;
use BoondManager\Lib\AbstractController;
use BoondManager\APIs\BillingSchedulesBalance\Specifications\HaveSearchAccess;
use BoondManager\Services\BillingSchedulesBalance;
use BoondManager\APIs\BillingSchedulesBalance\Filters;
use Wish\Models\Model;

/**
 * BillingSchedulesBalance list controller.
 * @package BillingSchedulesBalance
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'title',
		'turnoverTermOfPaymentExcludingTax',
		'turnoverInvoicedExcludingTax',
		'deltaInvoicedExcludingTax',
		'order' => [
			'id',
			'number',
			'reference',
			'numberOfInvoices',
			'mainManager' => [
				'id',
				'firstName',
				'lastName',
			],
			'project' => [
				'id',
				'reference',
				'typeOf',
				'mode',
				'currency',
				'exchangeRate',
				'currencyAgency',
				'exchangeRateAgency',
				'opportunity' => [
					'id',
					'title',
				],
				'contact' => [
					'id',
					'firstName',
					'lastName',
				],
				'company' => [
					'id',
					'name',
				],
			],
		],
		'invoice',
	];

	/**
	 * Search billingschedulesbalance.
	 *
	 * Called by router, [\Base](http://fatfreeframework.com/base#Routing), with a post request.
	 *
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 *
	 * @api {get} /billingschedulesbalance Search billingschedulesbalance
	 * @apiName billingschedulesbalance/search
	 * @apiGroup BillingSchedulesBalance
	 * @apiVersion 1.0.0
	 *
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchBillingSchedulesBalance();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = BillingSchedulesBalance::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverInvoicedExcludingTax' => $result->turnoverInvoicedExcludingTax,
					'turnoverTermOfPaymentExcludingTax' => $result->turnoverTermOfPaymentExcludingTax,
					'deltaInvoicedExcludingTax' => $result->deltaInvoicedExcludingTax,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}
}
