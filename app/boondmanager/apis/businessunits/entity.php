<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\BusinessUnits;

use BoondManager\APIs\BusinessUnits\Specifications\HaveDeleteAccess;
use BoondManager\APIs\BusinessUnits\Specifications\HaveReadAccess;
use BoondManager\APIs\BusinessUnits\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\BusinessUnit;
use BoondManager\Services\BM;
use BoondManager\Services\BusinessUnits;

/**
 * Class Entity
 * @package BoondManager\APIs\BusinessUnits
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'name',
		'managers' => [
			'id',
			'firstName',
			'lastName'
		],
		'excludedManagersInSearch' => [
			'id',
			'firstName',
			'lastName'
		]
	];

	/**
	 * @param BusinessUnit $bu
	 * @return BusinessUnit
	 */
	public function getJSONData(BusinessUnit $bu) {
		return $bu->filterFields(self::ALLOWED_FIELDS);
	}

	/**
	 * Get business unit's basic data
	 */
	public function api_get() {
		$bu = BusinessUnits::get($this->requestAccess->id);
		if(!$bu) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(), $bu);

		$this->sendJSONResponse([
			'data' => $this->getJSONData($bu)
		]);
	}

	/**
	 * Delete a business unit
	 */
	public function api_delete() {
		$bu = BusinessUnits::get($this->requestAccess->id);
		if(!$bu) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $bu);

		if(BusinessUnits::delete($bu->getID()))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}

	/**
	 * Update business unit's information data
	 */
	public function api_put() {
		$bu = BusinessUnits::get($this->requestAccess->id);
		if(!$bu) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $bu);

		//Build filters
		$filter = new Filters\Entity($bu);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build pole
		BusinessUnits::buildFromFilter($filter, $bu);

		if(BusinessUnits::update($bu)) {
			$this->sendJSONResponse([
				'data' => $this->getJSONData($bu)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
