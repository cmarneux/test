<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\BusinessUnits\Filters;

use BoondManager\Services\BM;
use BoondManager\Services\Managers;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputMultiRelationships;
use Wish\Filters\Inputs\InputString;
use BoondManager\Models;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;

/**
 * Class Entity
 * @property InputString name
 * @property InputMultiRelationships managers
 * @property InputMultiRelationships excludedManagersInSearch
 * @package BoondManager\APIs\BusinessUnits\Filters
 */
class Entity extends AbstractJsonAPI {
	/**
	 * @var Models\BusinessUnit
	 */
	protected $bu;

	/**
	 * Information constructor.
	 * @param Models\BusinessUnit|null $bu
	 */
	public function __construct(Models\BusinessUnit $bu = null) {
		parent::__construct();

		$this->bu = $bu;

		$input = new InputString('name');
		$input->setMinLength(1);
		$input->setMaxLength(250);
		$this->addInput($input);

		//TODO : Add a filter to delete duplicates
		$input = new InputMultiRelationships('managers');
		$input->addFilter(FILTER_CALLBACK, function($value) {
			$managers = Tools::useColumnAsKey('id', Managers::getAllGroupManagers($onlyActive = false));
			if(array_key_exists($value, $managers)) {
				$instance = new Models\Account();
				$instance->fromArray($managers[$value]);
				return $instance;
			} else return false;
		}, null, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$input->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);
		$this->addInput($input);

		//TODO : Add a filter to delete duplicates
		$input = new InputMultiRelationships('excludedManagersInSearch');
		$input->addFilter(FILTER_CALLBACK, function($value) {
			$managers = Tools::useColumnAsKey('id', Managers::getAllGroupManagers($onlyActive = false));
			if(array_key_exists($value, $managers)) {
				$instance = new Models\Account();
				$instance->fromArray($managers[$value]);
				return $instance;
			} else return false;
		}, null, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$input->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);
		$this->addInput($input);

		if($this->isCreation())
			$this->adaptForCreation();
	}

	/**
	 *
	 */
	protected function postValidation() {
		if($this->isCreation())
			$bu = new Models\BusinessUnit();
		else
			$bu = $this->bu;

		$managers = Tools::useColumnAsKey('id', $this->managers->isDefined() ? $this->managers->getValue() : $bu->managers);
		$excludedManagersInSearch = $this->excludedManagersInSearch->isDefined() ? $this->excludedManagersInSearch->getValue() : $bu->excludedManagersInSearch;
		foreach($excludedManagersInSearch as $manager) {
			if(!array_key_exists($manager->id, $managers)) {
				$this->excludedManagersInSearch->invalidate(Models\BusinessUnit::ERROR_BUSINESSUNIT_WRONG_EXCLUDEMANAGERS);
				break;
			}
		}
	}

	/**
	 * Test if the business unit is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->bu->id ? false : true;
	}

	/**
	 * Set inputs for a creation
	 */
	private function adaptForCreation() {
		$this->name->setRequired(true);
	}
}


