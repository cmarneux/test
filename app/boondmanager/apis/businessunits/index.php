<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\BusinessUnits;

use BoondManager\APIs\BusinessUnits\Specifications\HaveCreateAccess;
use BoondManager\APIs\BusinessUnits\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\BusinessUnits;
use BoondManager\APIs\BusinessUnits\Filters;
use Wish\Cache;

/**
 * Class Index
 * @package BoondManager\APIs\BusinessUnits
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'name',
		'managers' => [
			'id',
			'firstName',
			'lastName'
		]
	];

	/**
	 * Search business units
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$cache = Cache::instance();
		$cache->setEnabled(false);
		$result = BusinessUnits::getAllBusinessUnits();
		$cache->setEnabled(true);

		foreach($result as $bu)
			$bu->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => sizeof($result),
				]
			],
			'data' => $result
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a business unit
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default business unit
		$bu = BusinessUnits::buildFromFilter($filter);
		if(!$bu) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $bu);

		if(BusinessUnits::create($bu)) {
			$apiPole = new Entity();
			$this->sendJSONResponse([
				'data' => $apiPole->getJSONData($bu)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
