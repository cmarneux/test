<?php
/**
 * HaveCreateAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\BusinessUnits\Specifications;

use BoondManager\Lib\RequestAccess;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveCreateAccess
 * @package BoondManager\APIs\BusinessUnits\Specifications
 */
class HaveCreateAccess extends AbstractSpecificationItem {
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		//Check this API authorization & access
		return $user->isAdministrator();
	}
}
