<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Action;
use BoondManager\Models\Candidate;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Candidates\Specifications\HaveReadAccess;

class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Proxy method for search/actions->api_get
	 * @return mixed
	 */
	public function api_get() {
		$profil = Services\Candidates::get($this->requestAccess->id);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Candidate::TAB_ACTIONS), $profil);

		$filter = new SearchActions();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchActions::ORDERBY_DATE);
		$filter->order->setDefaultValue(SearchActions::ORDER_DESC);
		$filter->setAvailableCategories([BM::CATEGORY_CANDIDATE]);
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue( $profil->getReference() );

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		foreach($result->rows as $entity){
			/** @var Action $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
