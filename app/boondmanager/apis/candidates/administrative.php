<?php
/**
 * administrative.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Candidate;
use BoondManager\Services;
use BoondManager\Services\BM;
use BoondManager\APIs\Candidates\Specifications\HaveReadAccess;
use BoondManager\APIs\Candidates\Specifications\HaveWriteAccess;

class Administrative extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'dateOfBirth',
		'placeOfBirth',
		'nationality',
		'healthCareNumber',
		'administrativeComments',
		'situation',
		'administrative',
		'actualSalary',
		'desiredSalary' => [
			'min',
			'max'
		],
		'desiredContract',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'resource' => [
			'id',
		],
		'contract' => [
			'id',
			'typeOf',
			'employeeType',
			'startDate',
			'endDate',
			'monthlySalary',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency'
		],
		'providerContact' => [
			'id',
			'lastName',
			'firstName'
		],
		'providerCompany' => [
			'id',
			'name'
		],
		'files' => [
			'id',
			'name'
		]
	];

	public function api_get() {
		$profil = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_ADMINISTRATIVE);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Candidate::TAB_ADMINISTRATIVE), $profil );

		$tabData = [
			'data' => $profil->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		// chargement de la fiche
		$profil = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_ADMINISTRATIVE);
		if(!$profil) $this->error(404); //objet non trouvé

		// vérification des droits
		$this->checkAccessWithSpec( new HaveWriteAccess(Candidate::TAB_ADMINISTRATIVE), $profil );

		$filter = new Filters\SaveAdministrative($profil);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Services\Candidates::buildFromFilter($filter, $profil);

		if(Services\Candidates::update($profil, Candidate::TAB_ADMINISTRATIVE)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

	}
}
