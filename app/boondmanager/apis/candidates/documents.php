<?php
/**
 * files.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\OldModels\Filters;
use BoondManager\Databases\Local\File;
use BoondManager\Models\Candidate;
use BoondManager\Services\Candidates;
use BoondManager\Services\GED;
use BoondManager\APIs\Candidates\Specifications\HaveReadAccess;

class Documents extends AbstractController {
	public function api_get() {
		$filter = Filters\Search\Documents::getUserFilter();
		$filter->category->addFilterInArray([
			File::TYPE_RESOURCE_RESUME, File::TYPE_OTHER
		]);
		$filter->setData($this->requestAccess->getParams());
		$filter->parent->setValue($this->requestAccess->id);
		$filter->type->setValue(BM::CATEGORY_CANDIDATE);
		$this->checkFilter($filter);

		$profil = Candidates::get($this->requestAccess->id);
		if(!$profil) $this->error(404);

		switch($filter->category->getValue()) {
			case File::TYPE_RESOURCE_RESUME:
				$this->checkAccessWithSpec( new HaveReadAccess(Candidate::TAB_INFORMATION), $profil );
				break;
			case File::TYPE_OTHER:
				$this->checkAccessWithSpec( new HaveReadAccess(Candidate::TAB_ADMINISTRATIVE), $profil );
				break;
			default:
				$this->error(403);
				break;
		}

		$ged = new GED();
		$result = $ged->search($filter);

		$data = [
			'meta'=>[
				'totals' => [
					'rows' => $result->total
				]
			],
			'data'=>$result->rows
		];

		$this->sendJSONResponse($data);
	}
}
