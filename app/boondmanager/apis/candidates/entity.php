<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Candidate;
use BoondManager\Services;
use BoondManager\APIs\Candidates\Specifications\HaveCreateAccess;
use BoondManager\APIs\Candidates\Specifications\HaveDeleteAccess;

class Entity extends AbstractController {

	public function api_get() {

		if($id = $this->requestAccess->id) {
			// get an existing profil
			$entity = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_DEFAULT);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			// instead of using one spec, let's check any access to a tab
			$tabs = Services\Candidates::getVisibleTabs($entity);
			if(!$tabs) $this->error(403);

			$ALLOWED_FIELDS = [
				'id',
				'lastName',
				'firstName',
				'civility',
				'creationDate',
				'updateDate',
				'mainManager' => ['id'],
				'hrManager' => ['id'],
				'agency' => ['id'],
				'pole' => ['id']
			];
		} else {
			$this->checkAccessWithSpec( new HaveCreateAccess ); // TODO
			$entity = Services\Candidates::getNewProfil();

			$ALLOWED_FIELDS = [
				'id',
				'civility',
				'state',
				'country',
				'mainManager' => ['id'],
				'hrManager' => ['id'],
				'agency' => ['id'],
				'pole' => ['id']
			];
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$profil = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_TD); //so we can retrieve ID_TD
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $profil );

		$deleted = Services\Candidates::deleteCandidate($profil);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
