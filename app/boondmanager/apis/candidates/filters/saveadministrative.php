<?php
/**
 * saveadministrative.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates\Filters;
use BoondManager\Lib\Filters\Inputs\Relationships\Contact;
use BoondManager\Models\Candidate;
use BoondManager\Lib\Filters\Inputs\Attributes\DesiredSalary;
use BoondManager\Services\CurrentUser;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\MySQL\Where;

/**
 * Class SaveAdministrative
 * @property \Wish\Filters\Inputs\InputString nationality
 * @property \Wish\Filters\Inputs\InputString placeOfBirth
 * @property InputDate dateOfBirth
 * @property \Wish\Filters\Inputs\InputString healthCareNumber
 * @property InputValue situation
 * @property InputValue $currency
 * @property InputFloat $exchangeRate
 * @property InputRelationship contact
 * @property DesiredSalary desiredSalary
 * @property InputFloat $actualSalary
 * @package BoondManager\Models\Filters\Profiles\Resources
 */
class SaveAdministrative extends AbstractJsonAPI
{

	/** @var Candidate */
	private $candidate;

	/**
	 * Profil constructor.
	 * @param Candidate $candidate
	 */
	public function __construct($candidate = null){

		$this->candidate = $candidate;

		parent::__construct();

		$basicStringFields = ['nationality', 'placeOfBirth', 'healthCareNumber'];
		foreach ($basicStringFields as $field)
			$this->addInput(new InputString($field));

		$salary = new InputFloat('actualSalary');

		$situation = new InputDict('situation','specific.setting.situation');

		$devise = new InputDict('currency', 'specific.setting.currency', CurrentUser::instance()->getCurrency());

		$change = new InputFloat('exchangeRate', 1);

		$contract = new InputDict('desiredContract', 'specific.setting.typeOf.contract');

		$date = new InputDate('dateOfBirth');

		$comment = new InputString('administrativeComments');

		$this->addInput(new DesiredSalary());

		$crmContact = new Contact('providerContact');

		$this->addInput([$salary, $situation, $devise, $change, $contract, $date, $crmContact, $comment]);
	}
}
