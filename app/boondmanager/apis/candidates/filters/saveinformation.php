<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Address;
use BoondManager\Lib\Filters\Inputs\Attributes\Country;
use BoondManager\Lib\Filters\Inputs\Attributes\Email;
use BoondManager\Lib\Filters\Inputs\Attributes\Phone;
use BoondManager\Lib\Filters\Inputs\Attributes\PostCode;
use BoondManager\Lib\Filters\Inputs\Attributes\Town;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\HrManager;
use BoondManager\Lib\Filters\Inputs\Relationships\MainManager;
use BoondManager\Lib\Filters\Inputs\Relationships\Pole;
use BoondManager\OldModels\Filters\Profiles\Candidates\Notation;
use BoondManager\Services\Subscriptions;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use BoondManager\Services\BM;
use BoondManager\Lib\Filters\Inputs\Attributes\SourceOrOrigin;
use BoondManager\Lib\Filters\Inputs\Attributes\WebSocial;
use BoondManager\Models\Candidate;
use BoondManager\Lib\RequestAccess;
use BoondManager\APIs\Candidates\Specifications\HaveWriteAccessOnField;
use Wish\Filters\Inputs\InputValue;

/**
 * Class SaveInformation
 * @property InputString $lastName
 * @property InputString $firstName
 * @property Phone $phone1
 * @property Phone $phone2
 * @property Phone $phone3
 * @property Phone $fax
 * @property Address $address
 * @property PostCode $postcode
 * @property Town $town
 * @property Country $country
 * @property InputString $informationComments
 * @property Email $email1
 * @property Email $email2
 * @property Email $email3
 * @property InputDict $civility
 * @property InputBoolean $visibility
 * @property InputDate $dateOfBirth
 * @property SourceOrOrigin $source
 * @property InputDate $creationDate
 * @property InputDate availability
 * @property InputValue availabilityDelay
 * @property InputMultiValues $mobilityAreas
 * @property MainManager $hrManager
 * @property MainManager $mainManager
 * @property Agency $agency
 * @property Pole $pole
 * @property InputDict $state
 * @property InputValue $globalEvaluation
 * @property InputMultiObjects $socialNetworks
 * @property InputMultiObjects $evaluations
 * @property InputBoolean $importResume
 */
class SaveInformation extends AbstractJsonAPI{

	/**
	 * @var Candidate
	 */
	private $_candidate;

	const
		DELAY_UNDEFINED = -1,
		DELAY_IMMEDIATE = 0,
		DELAY_LESS_1_MONTH = 1,
		DELAY_1_3_MONTH = 2,
		DELAY_3_MONTH = 3,
		DELAY_MORE_3_MONTH = 4,
		DELAY_DATED = 9;

	/**
	 * Profil constructor.
	 * @param Candidate $candidate
	 */
	public function __construct($candidate = null){

		$this->_candidate = $candidate;

		parent::__construct();

		$firstName = new InputString('firstName');
		$this->addInput($firstName);

		$firstName = new InputString('lastName');
		$this->addInput($firstName);

		$updateDate = new InputDateTime('updateDate');
		$this->addInput($updateDate);

		$creationDate = new InputDateTime('creationDate');
		$this->addInput($creationDate);

		$civ = new InputDict('civility', 'specific.setting.civility');
		$this->addInput($civ);

		$state = new InputDict('state', 'specific.setting.state.candidate');
		$this->addInput($state);

		for($i=1; $i<=3; $i++){
			$email = new Email('email'.$i);
			$this->addInput($email);
		}

		for($i=1; $i<=3; $i++){
			$phone = new Phone('phone'.$i);
			$this->addInput($phone);
		}

		$input = new Phone('fax');
		$this->addInput($input);

		$input = new Address();
		$this->addInput($input);

		$input = new PostCode();
		$this->addInput($input);

		$input = new Town();
		$this->addInput($input);

		$input = new Country();
		$this->addInput($input);

		$mainManager = new MainManager();
		$this->addInput($mainManager);

		$hrManager = new HrManager('hrManager');
		$this->addInput($hrManager);

		$agency = new Agency();
		$this->addInput($agency);

		$agency = new Pole('pole');
		$this->addInput($agency);

		$source = new SourceOrOrigin('source');
		$this->addInput($source);

		$WebSocial = new WebSocial();
		$input = new InputMultiObjects('socialNetworks', $WebSocial);
		$input->addGroupFilterCallback(function($data) {
			$ids = [];
			foreach ($data as $social) {
				if (in_array($social->network->getValue(), $ids)) return false;
				else $ids[] = $social->network->getValue();
			}
			return $data;
		}, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
		$this->addInput($input);

		$visible = new InputBoolean('visibility');
		$this->addInput($visible);

		$birthDate = new InputDate('dateOfBirth');
		$this->addInput($birthDate);

		$input = new InputString('informationComments');
		$this->addInput($input);

		$mobility = new InputMultiDict('mobilityAreas', 'specific.setting.mobilityArea');
		$this->addInput($mobility);

		$eval = new InputDict('globalEvaluation', 'specific.setting.evaluation');
		$this->addInput($eval);

		$notations = new InputMultiObjects('evaluations', new Notation());
		$this->addInput($notations);

		$delay = new InputEnum('availabilityDelay', self::DELAY_UNDEFINED);
		$delay->setAllowedValues([
			self::DELAY_1_3_MONTH, self::DELAY_3_MONTH, self::DELAY_LESS_1_MONTH, self::DELAY_MORE_3_MONTH, self::DELAY_DATED, self::DELAY_IMMEDIATE
		]);
		$this->addInput($delay);

		$input = new InputDate('availability');
		$this->addInput($input);

		$input = new InputBoolean('importResume');
		$this->addInput($input);

		if($this->isCreation()) {
			$this->adaptForCreation();
		}else{
			$this->adaptForEdition();
		}
	}

	private function adaptForCreation() {
		$this->firstName->setRequired(true);
		$this->lastName->setRequired(true);
		$this->mainManager->setRequired(true);
		$this->agency->setRequired(true);
	}

	private function adaptForEdition() {
		$this->firstName->setAllowEmptyValue(false);
		$this->lastName->setAllowEmptyValue(false);
	}

	protected function postValidation() {
		$request = new RequestAccess();
		$request->setData($this->_candidate);

		foreach(['creationDate', 'visibility', 'mainManager', 'agency'] as $publicField){
			if( $this->$publicField->isDefined() && !(new HaveWriteAccessOnField( $publicField ))->isSatisfiedBy($request) ){
				$this->$publicField->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);
			}
		}

		// if candidate has been hired, its state can not be changed
		if(!$this->isCreation() && $this->_candidate->state == Candidate::STATE_HIRED && $this->state->isDefined() && $this->state->getValue() != $this->_candidate->state) {
			$this->state->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET); // TODO : adapt error message
		}

		if($this->shouldHireCandidate()) {
			if (!Subscriptions::getLicencesLeft()) {
				$this->state->invalidateIfDebug(BM::ERROR_GLOBAL_RESOURCE_QUOTA_MAX);
			}
		}
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->_candidate && $this->_candidate->id ? false : true;
	}

	/**
	 * has the state been changed to hired
	 * @return bool
	 */
	public function shouldHireCandidate() {
		$newState = $this->state->getValue();
		return $newState == Candidate::STATE_HIRED && ($this->isCreation() || $newState != $this->_candidate->state);
	}
}
