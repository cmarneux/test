<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;

/**
 * Class Candidates
 * build a filter for candidates (search & sort)
 * @property-read InputValue $keywordsType (int) types de mot cle _(cf const KEYWORD_TYPE_***)_
 * @property-read InputBoolean $returnHRManager responsable rh
 * @property-read InputValue $sort (string) colonne de tri
 * @property-read InputValue $period (string) _(cf constantes PERIODE_***)_ type de période
 * @property-read InputDate $startDate (date) date de début
 * @property-read InputDate $endDate (date) date de fin
 * @property-read InputMultiValues $candidateStates état du profil
 * @property-read InputMultiValues $activityAreas (array) competences applications
 * @property-read InputMultiValues $expertiseAreas (array) competences intervention
 * @property-read InputMultiValues $tools (array) domaine technique (DT) outils
 * @property-read InputMultiValues $experiences (array) domaine technique (DT) expérience
 * @property-read InputMultiValues $trainings domaine technique (DT) formation
 * @property-read InputMultiValues $availabilityTypes
 * @property-read InputMultiValues $mobilityAreas
 * @property-read InputMultiValues $evaluations
 * @property-read InputMultiValues $contractTypes
 * @property-read InputMultiValues $languages
 * @property-read InputMultiValues $sources
 * @property-read InputValue $perimeterManagersType
 * @property-read InputBoolean $visibleProfile TODO : renommer en onlyVisible
 * @package BoondManager\Models\Filters\Search
 */
class SearchCandidates extends AbstractSearch{
	/**
	 * @var string
	 */
	const PERIMETER_MODULE = BM::MODULE_CANDIDATES;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED= 'created',
		PERIOD_AVAILABLE = 'available',
		PERIOD_UPDATED = 'updated',
		PERIOD_ACTIONS = 'actions';
	/**#@-*/

	/**#@+
	 * column for the order by
	 * @var string ORDERBY
	 */
	const
		ORDERBY_LASTNAME = 'lastName',
		ORDERBY_TITLE = 'title',
		ORDERBY_AVAILABILITY = 'availability',
		ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS = 'numberOfActivePositionings',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName',
		ORDERBY_UPDATEDATE = 'updateDate',
		ORDERBY_STATE = 'state',
		ORDERBY_EXPERIENCE = 'experience',
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_EVALUATION = 'evaluation',
		ORDERBY_HRMANAGER_LASTNAME = 'hrManager.lastName',
		ORDERBY_SOURCE = 'source';
	/**#@-*/

	/**#@+
	 * @var int types of keywords
	 */
	const
		KEYWORD_TYPE_RESUMETD = 'resumeTd',
		KEYWORD_TYPE_LASTNAME = 'lastName',
		KEYWORD_TYPE_FIRSTNAME = 'firstName',
		KEYWORD_TYPE_FULLNAME = 'fullName',
		KEYWORD_TYPE_EMAILS = 'emails',
		KEYWORD_TYPE_TITLE = 'title',
		KEYWORD_TYPE_PHONES = 'phones',
		KEYWORD_TYPE_RESUME = 'resume',
		KEYWORD_TYPE_TD = 'td';
	/**#@- */

	/**#@+
	 * @var int MANAGER_TYPE
	 */
	const
		MANAGER_TYPE_RESP = 'main',
		MANAGER_TYPE_HR = 'hr';
	/**#@-*/

	public function __construct()
	{
		//~ TODO : 12-05-2016 : Tin : manque le filtre languages (migration v6.7.4 > v6......)
		parent::__construct();

		$this->addInput(new InputMultiDict('activityAreas', 'specific.setting.activityArea'));
		$this->addInput(new InputMultiDict('availabilityTypes', 'specific.setting.availability'));
		$this->addInput(new InputMultiDict('candidateStates', 'specific.setting.state.candidate'));
		$this->addInput(new InputMultiDict('contractTypes', 'specific.setting.typeOf.contract'));

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);

		$this->addInput(new InputMultiDict('evaluations', 'specific.setting.evaluation'));
		$this->addInput(new InputMultiDict('experiences', 'specific.setting.experience'));
		$this->addInput(new InputMultiDict('expertiseAreas', 'specific.setting.expertiseArea'));

		//Type de mots clefs
		$keywordsType = new InputEnum('keywordsType');
		$keywordsType->setModeDefaultValue(self::KEYWORD_TYPE_RESUMETD);
		$keywordsType->setAllowedValues([
			self::KEYWORD_TYPE_RESUMETD, self::KEYWORD_TYPE_LASTNAME, self::KEYWORD_TYPE_FIRSTNAME,
			self::KEYWORD_TYPE_FULLNAME, self::KEYWORD_TYPE_EMAILS, self::KEYWORD_TYPE_TITLE,
			self::KEYWORD_TYPE_PHONES, self::KEYWORD_TYPE_RESUME, self::KEYWORD_TYPE_TD
		]);
		$this->addInput($keywordsType);

		$languages = new InputMultiValues('languages');
		$languages->addFilterCallback(function($value) {
			$language_level = explode('|', $value);
			if(count($language_level) != 2 || !in_array($language_level[1], array_column( Dictionary::getDict('specific.setting.languageLevel') , 'id'))) return false;
			return $value;
		}, 'Value of languages is incorrect !');
		$this->addInput($languages);


		$this->addInput(new InputMultiDict('mobilityAreas', 'specific.setting.mobilityArea'));
		$this->addInput(new InputBoolean('returnHRManager'));
		$this->addInput(new InputMultiDict('tools', 'specific.setting.tool'));
		$this->addInput(new InputMultiDict('trainings', 'specific.setting.training'));

		// Colonne de tri
		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_LASTNAME, self::ORDERBY_TITLE, self::ORDERBY_AVAILABILITY,
			self::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS, self::ORDERBY_MAINMANAGER_LASTNAME,
			self::ORDERBY_UPDATEDATE, self::ORDERBY_STATE, self::ORDERBY_EXPERIENCE, self::ORDERBY_CREATIONDATE,
			self::ORDERBY_EVALUATION, self::ORDERBY_HRMANAGER_LASTNAME, self::ORDERBY_SOURCE
		]);
		$this->addInput($sort);

		// periode
		$period = new InputEnum('period');
		$tabPS = [
			self::PERIOD_ACTIONS, self::PERIOD_AVAILABLE, self::PERIOD_CREATED, self::PERIOD_UPDATED
		];
		foreach(Dictionary::getDict('specific.setting.action.candidate') as $item)
			$tabPS[] = self::PERIOD_ACTIONS.'_'.$item['id'];
		$period->setAllowedValues($tabPS);
		$period->setModeDefaultValue(BM::INDIFFERENT);
		$this->addInput($period);

		$this->addInput(new InputMultiDict('sources', 'specific.setting.source'));

		$perimeterManagersType = new InputEnum('perimeterManagersType');
		$perimeterManagersType->setModeDefaultValue(BM::INDIFFERENT);
		$perimeterManagersType->setAllowedValues([
			self::MANAGER_TYPE_HR, self::MANAGER_TYPE_RESP
		]);
		$this->addInput($perimeterManagersType);


		//profile visibility
		$visibleProfile = new InputBoolean('visibleProfile');
		$visibleProfile->setModeDefaultValue(true);
		$this->addInput($visibleProfile);
	}

	protected function postValidation()
	{
		parent::postValidation();

		if(!CurrentUser::instance()->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE))
			$this->visibleProfile->setDisabled(true);
	}

	/*
	 * définition de quelques aides pour manipuler ce filtre
	 */

	/**
	 * @var int the period type
	 */
	private $_periodeType;
	/**
	 * get the period type _(@see self::PERIODS)_
	 * @return string
	 */
	public function getPeriodType(){
		if(is_null($this->_periodeType)) {
			$pr = $this->period->getValue();
			$pr = explode('_', $pr);
			$this->_periodeType = $pr[0];
		}
		return $this->_periodeType;
	}

	/**
	 * get the period action (only for a special period type)
	 * @return int|null
	 */
	public function getPeriodAction(){
		$pr = $this->period->getValue();
		$pr = explode('_', $pr);
		if(count($pr)==2){
			return $pr[1];
		}else
			return null;
	}

	/**
	 * is the choosen period the default one
	 * @return bool
	 */
	public function isDefaultPeriod(){
		return $this->period->getValue() == $this->period->getDefaultValue();
	}

	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
