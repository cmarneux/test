<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\Models\Candidate;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Candidates\Specifications\HaveCreateAccess;
use BoondManager\APIs\Candidates\Specifications\HaveSearchAccess;

class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'updateDate',
		'civility',
		'firstName',
		'lastName',
		'state',
		'visibility',
		'availability',
		'skills',
		'mobilityAreas',
		'title',
		'email1',
		'phone1',
		'phone2',
		'numberOfResumes',
		'numberOfActivePositionings',
		'isEntityUpdating',
		'isEntityDeleting',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'hrManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'resume' => [
			'id'
		],
	];

	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchCandidates();
		$filter->setData( $this->requestAccess->getParams() );

		$this->checkFilter($filter);

		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Services\Extraction\Candidates('candidates.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return;
		}

		$result = Services\Candidates::search($filter);

		foreach($result->rows as $entity) {
			/** @var Candidate $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess );

		// retrieve filtered params
		$filter = new Filters\SaveInformation();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$candidate = Services\Candidates::buildFromFilter($filter);

		if(Services\Candidates::create($candidate)) {

			if( $filter->shouldHireCandidate() ) Services\Candidates::hireCandidate($candidate, $importResume = false);

			$this->sendJSONResponse([
				'data' => $candidate->filterFields(Information::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
