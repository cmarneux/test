<?php
/**
 * profile.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Models\Candidate;
use BoondManager\APIs\Candidates\Specifications\HaveReadAccess;
use BoondManager\APIs\Candidates\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Information extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'updateDate',
		'civility',
		'lastName',
		'firstName',
		'state',
		'email1',
		'email2',
		'email3',
		'phone1',
		'phone2',
		'phone3',
		'fax',
		'postcode',
		'address',
		'town',
		'country',
		'source' => [
			'typeOf',
			'detail'
		],
		'dateOfBirth',
		'mobilityAreas',
		'globalEvaluation',
		'availability',
		'informationComments',
		'evaluations' => [
			'id',
			'notations' => [
				'criteria',
				'evaluation'
			],
			'date',
			'comments',
			'manager',
			'lastName',
			'firstName'
		],
		'socialNetworks' => [
			'network',
			'url'
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'hrManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		],
		'resumes' => [
			'id',
			'name'
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing profil
			$profil = Services\Candidates::get($id, Candidate::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$profil) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Candidate::TAB_INFORMATION), $profil );
		} else {
			$this->error(404);
		}

		$tabData = [
			'data' => $profil->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$profil = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_INFORMATION);
		// if profil not found, throw an error (404 not found)
		if(!$profil) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec( new HaveWriteAccess(Candidate::TAB_INFORMATION), $profil );

		$filter = new Filters\SaveInformation($profil);
		$filter->setData( $this->requestAccess->get('data') );
		$this->checkFilter($filter);

		//Build project
		$profil = Services\Candidates::buildFromFilter($filter, $profil);

		if(Services\Candidates::update($profil, Candidate::TAB_INFORMATION)) {

			if( $filter->shouldHireCandidate() ) Services\Candidates::hireCandidate($profil, $filter->importResume->getValue());

			$this->sendJSONResponse([
				'data' => $profil->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
