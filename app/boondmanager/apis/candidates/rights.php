<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Candidates;

/**
 * Class Rights
 * @package BoondManager\APIs\Projects
 */
class Rights extends AbstractController {
	/**
	 * Get project's rights
	 */
	public function api_get() {
		$project = Candidates::get($this->requestAccess->id);
		if(!$project) $this->error(404);

		$this->sendJSONResponse([
			'data' => Candidates::getRights($project)
		]);
	}
}
