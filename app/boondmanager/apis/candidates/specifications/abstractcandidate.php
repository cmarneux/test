<?php
/**
 * abstractressource.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Candidates\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Candidate;
use BoondManager\Models\Employee;
use BoondManager\Lib\RequestAccess;

abstract class AbstractCandidate extends AbstractSpecificationItem{
    /**
     * get the resource from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Candidate|null
     */
    public function getData($request){
        if($request->data instanceof Candidate) return $request->data;
        else return null;
    }
}
