<?php
/**
 * userhavecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class CanCreateResource
 *
 * Indicate if the user have the right to create a new Resource
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveCreateAccess extends AbstractCandidate
{
	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){
		$user = $request->user;

		return $user->isGod() || $user->isManager() && $user->hasRight(BM::RIGHT_CREATION, BM::MODULE_CANDIDATES);
	}
}
