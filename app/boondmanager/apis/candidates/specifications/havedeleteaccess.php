<?php
/**
 * userhavedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;

/**
 * Class CanCreateResource
 *
 * Indicate if the user have the right to create a new Resource
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractCandidate
{
	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){
		$user = $request->user;

		if( !(new UserTypeIs([BM::USER_TYPE_MANAGER]))->isSatisfiedBy($request))
			return false;

		return $user->isGod() || $user->hasRight(BM::RIGHT_DELETION, BM::MODULE_CANDIDATES);
	}
}
