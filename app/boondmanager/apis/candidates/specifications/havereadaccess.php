<?php
/**
 * userhavereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates\Specifications;

use BoondManager\Services\BM;
use BoondManager\Models\Candidate;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughAgenciesOrBUs;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughHierarchyOrBUs;
use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\Lib\Specifications\HaveAccess;
use BoondManager\OldModels\Specifications\RequestAccess\HaveRight;

class HaveReadAccess extends AbstractCandidate{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;
		$resource = $this->getData($request);
		if(!$resource) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		if(!$user->hasAccess(BM::MODULE_CANDIDATES)) return false;

		switch($this->getTab()) {
			case Candidate::TAB_FLAGS:
			case Candidate::TAB_INFORMATION:
			case Candidate::TAB_TD:
				return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
					|| $resource->isVisible() && ( new CanAccessThroughHierarchyOrBUs)->or_(new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request);

			case Candidate::TAB_ADMINISTRATIVE:
				return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
				|| $resource->isVisible() && (new CanAccessThroughHierarchyOrBUs)->or_(
					(new CanAccessThroughAgenciesOrBUs)->and_(new HaveRight(BM::RIGHT_WRITEALL, BM::MODULE_CANDIDATES))
				)->isSatisfiedBy($request);

			case Candidate::TAB_ACTIONS:
				return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
				|| $resource->isVisible() && (new CanAccessThroughHierarchyOrBUs)->or_(
					(new CanAccessThroughAgenciesOrBUs)->and_(new HaveAccess(BM::MODULE_CANDIDATES, BM::MODULE_CANDIDATES_ACTIONS))
				)->isSatisfiedBy($request);

			case Candidate::TAB_POSITIONINGS:

				if(!$user->hasAccess(BM::MODULE_OPPORTUNITIES)) return false;

				return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
				|| $resource->isVisible() && (new CanAccessThroughHierarchyOrBUs)->or_(
					(new CanAccessThroughAgenciesOrBUs)->and_(new HaveRight(BM::RIGHT_WRITEALL, BM::MODULE_CANDIDATES))
				)->isSatisfiedBy($request);

			default: return false;
		}
	}
}
