<?php
/**
 * UserHaveSearchAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Candidates\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on resources
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Resources
 */
class HaveSearchAccess extends AbstractCandidate{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager());

		// CONFORME v6
		return $user->hasAccess(BM::MODULE_CANDIDATES);
	}
}
