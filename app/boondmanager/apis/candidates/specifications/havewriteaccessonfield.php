<?php
/**
 * userhavewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Candidates\Specifications;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractCandidate{

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * check if the object match the specification
	 * @param mixed $object
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($object)
	{
		/** @var CurrentUser $user*/
		$user = $object->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$candidate = $this->getData($object);

		switch($this->field){
			case 'visibility':
				return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
			case 'creationDate':
				return $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_CANDIDATES);
			case 'mainManager':
			case 'agency':
				return !$candidate || $user->hasRight(BM::RIGHT_ASSIGNMENT, BM::MODULE_CANDIDATES);
			default:
				throw new \Exception('no rules for field "'.$this->field.'"');
		}
	}
}
