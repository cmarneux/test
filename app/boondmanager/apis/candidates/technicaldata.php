<?php
/**
 * technicalData.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Candidates;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Candidate;
use BoondManager\APIs\Candidates\Specifications\HaveReadAccess;
use BoondManager\APIs\Candidates\Specifications\HaveWriteAccess;
use BoondManager\Services;
use BoondManager\Services\BM;

/**
 * Class TD
 * @package BoondManager\Controllers\Profiles\Candidates
 */
class TechnicalData extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'title',
		'skills',
		'experience',
		'training',
		'languages' => [
			'language',
			'level'
		],
		'diplomas',
		'tools' => [
			'tool',
			'level'
		],
		'activityAreas',
		'expertiseAreas',
		'references' => [
			'id',
			'title',
			'description'
		]
	];

	public function api_get() {
		$profil = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_TD);
		if(!$profil) $this->error(404); //objet non trouvé

		$this->checkAccessWithSpec( new HaveReadAccess(Candidate::TAB_TD), $profil );

		$tabData = [
			'data' => $profil->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		// chargement de la fiche
		$profil = Services\Candidates::get($this->requestAccess->id, Candidate::TAB_TD);
		if(!$profil) $this->error(404); //objet non trouvé

		// vérification des droits
		$this->checkAccessWithSpec( new HaveWriteAccess(Candidate::TAB_TD), $profil );

		$filter = new Filters\SaveTD($profil);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Services\Candidates::buildFromFilter($filter, $profil);

		if(Services\Candidates::update($profil, Candidate::TAB_TD)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

	}
}
