<?php
/**
 * Created by PhpStorm.
 * User: vanadar
 * Date: 06/07/16
 * Time: 14:54
 */
namespace BoondManager\APIs\Companies;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Action;
use BoondManager\Models\Company;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Companies\Specifications\HaveReadAccess;

class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		$entity = Services\Companies::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_ACTIONS), $entity);

		$filter = new SearchActions();
		$filter->setIndifferentPerimeter();
		$filter->setAvailableCategories([BM::CATEGORY_CRM_COMPANY, BM::CATEGORY_CRM_CONTACT]);
		$filter->sort->setDefaultValue(SearchActions::ORDERBY_DATE);
		$filter->order->setDefaultValue(SearchActions::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());

		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
