<?php
/**
 * contacts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\APIs\Contacts\Filters\SearchContacts;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Models\Contact;
use BoondManager\Services;
use BoondManager\APIs\Companies\Specifications\HaveReadAccess;

class Contacts extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'civility',
		'firstName',
		'lastName',
		'state',
		'function',
		'department',
		'email1',
		'phone1',
		'phone2',
		'isEntityUpdating',
		'isEntityDeleting',
		'canReadContact',
		'canWriteContact',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {

		//~ TODO: 08-04-2016 - Tin - Gérer la recherche des actions de type notification
		$entity = Services\Companies::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_CONTACTS), $entity);

		$filter = new SearchContacts();
		$filter->setData([
			'keywords' => $entity->getReference()
		]);

		$this->checkFilter($filter);

		$result = Services\Contacts::search($filter);
		Services\Contacts::attachRights($result->rows);

		foreach($result->rows as $row){
			/** @var Contact $row */
			$row->filterFields(self::ALLOWED_FIELDS);
		}

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				],
			],
			'data' => $result->rows
		]);
	}
}
