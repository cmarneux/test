<?php
/**
 * companies.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Services;
use BoondManager\APIs\Companies\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Companies\Specifications\HaveReadAccess;
use BoondManager\APIs\Companies\Specifications\HaveCreateAccess;

class Entity extends AbstractController {
	public function api_get() {
		$ALLOWED_FIELDS = [
			'id',
			'name',
			'creationDate',
			'updateDate',
			'mainManager' =>[
				'id'
			],
			'pole' => [
				'id'
			],
			'agency' => [
				'id'
			]
		];

		if($id = $this->requestAccess->id) {
			// get an existing profil
			$entity = Services\Companies::get($this->requestAccess->id);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_DEFAULT), $entity );
		} else {
			$this->checkAccessWithSpec( new HaveCreateAccess );
			$entity = Services\Companies::getNew();
			$ALLOWED_FIELDS = [
				'id',
				'state',
				'country',
				'mainManager' =>[
					'id'
				],
				'pole' => [
					'id'
				],
				'agency' => [
					'id'
				]
			];
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		// get an existing profil
		$entity = Services\Companies::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess() );

		$deleted = Services\Companies::delete($entity);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
