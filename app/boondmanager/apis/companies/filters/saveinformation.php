<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Filters;

use BoondManager\APIs\Companies\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\Filters\Inputs\Attributes\Address;
use BoondManager\Lib\Filters\Inputs\Attributes\ApeCode;
use BoondManager\Lib\Filters\Inputs\Attributes\Country;
use BoondManager\Lib\Filters\Inputs\Attributes\LegalStatus;
use BoondManager\Lib\Filters\Inputs\Attributes\Phone;
use BoondManager\Lib\Filters\Inputs\Attributes\PostCode;
use BoondManager\Lib\Filters\Inputs\Attributes\RegisteredOffice;
use BoondManager\Lib\Filters\Inputs\Attributes\RegistrationNumber;
use BoondManager\Lib\Filters\Inputs\Attributes\Town;
use BoondManager\Lib\Filters\Inputs\Attributes\VatNumber;
use BoondManager\Lib\Filters\Inputs\Attributes\SourceOrOrigin;
use BoondManager\Lib\Filters\Inputs\Attributes\ContactDetails;
use BoondManager\Lib\Filters\Inputs\Attributes\WebSocial;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\Company;
use BoondManager\Lib\Filters\Inputs\Relationships\Influencers;
use BoondManager\Lib\Filters\Inputs\Relationships\MainManager;
use BoondManager\Lib\Filters\Inputs\Relationships\Pole;
use BoondManager\Lib\RequestAccess;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use BoondManager\Services\BM;
use BoondManager\Models;

/**
 * Class SaveInformation
 * @package BoondManager\Models\Filters\Profiles\Companies
 * @property InputString name
 * @property InputDateTime CreationDate
 * @property \BoondManager\Lib\Filters\Inputs\Attributes\WebSocial[] socialNetworks
 * @property ContactDetails[] $billingDetails
 * @property Influencers influencers
 * @property MainManager mainManager
 * @property Pole pole
 * @property Agency agency
 */
class SaveInformation extends AbstractJsonAPI
{
	/** @var Models\Company|null  */
	private $_company;

	/**
	 * Profil constructor.
	 * @param Models\Company $company
	 */
	public function __construct($company = null)
	{
		$this->_company = $company;

		parent::__construct();

		$this->addInput( new InputString('name') );
		$this->addInput( new InputDateTime('creationDate') );
		$this->addInput( new InputDict('state', 'specific.setting.state.company') );

		$website = new InputString('website');
		$website->setMaxLength(100);
		$this->addInput($website);

		$this->addInput( new Phone('phone1') );
		$this->addInput( new Phone('fax') );

		$this->addInput( new Address() );
		$this->addInput( new PostCode() );
		$this->addInput( new Town() );
		$this->addInput( new Country() );

		$staff = new InputInt('staff');
		$staff->setMin(0);
		$this->addInput($staff);

		$this->addInput( new InputString('informationComments') );

		$input = new InputMultiValues('departments');
		$input->addFilter(FILTER_SANITIZE_STRING);
		$input->addFilter(FILTER_CALLBACK, function($value){
			return trim($value);
		});
		$this->addInput($input);

		$this->addInput( new InputDict('expertiseArea', 'specific.setting.expertiseArea') );

		$this->addInput( new VatNumber() );
		$this->addInput( new RegistrationNumber() );
		$this->addInput( new LegalStatus() );
		$this->addInput( new RegisteredOffice());
		$this->addInput( new ApeCode());

		$WebSocial = new WebSocial();
		$social = new InputMultiObjects('socialNetworks', $WebSocial);
		$this->addInput($social);

		$billingDetails = new ContactDetails();
		$social = new InputMultiObjects('billingDetails', $billingDetails);
		$this->addInput($social);

		$this->addInput( new SourceOrOrigin('origin') );

		$this->addInput( new MainManager() );
		$this->addInput( new Agency() );
		$this->addInput( new Pole() );

		$this->addInput( new Influencers() );

		$this->addInput( new Company('parentCompany') );

		if($this->isCreation()) {
			$this->adaptForCreation();
		}else{
			$this->adaptForEdition();
		}
	}

	/**
	 * @return bool
	 */
	private function isCreation() {
		return !$this->_company || !$this->_company->id;
	}

	private function adaptForCreation() {
		$this->name->setRequired(true);
		$this->mainManager->setRequired(true);
		$this->agency->setRequired(true);
	}

	private function adaptForEdition() {
		$this->name->setAllowEmptyValue(false);
	}

	public function postValidation()
	{
		$request = new RequestAccess();
		$request->setData($this->_company);

		foreach(['creationDate', 'influencers', 'mainManager', 'agency'] as $publicField){
			if( $this->$publicField->isDefined() && !(new HaveWriteAccessOnField( $publicField ))->isSatisfiedBy($request) ){
				$this->$publicField->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);
			}
		}
	}
}
