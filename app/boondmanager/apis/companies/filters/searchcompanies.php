<?php
/**
 * companies.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Companies
 * build a filter for lists/companies (search & sort)
 * @package BoondManager\Models\Filters\Search
 * @property InputMultiValues states
 * @property InputMultiValues expertiseAreas
 * @property InputMultiValues origins
 * @property InputValue period
 * @property InputMultiValues sort
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputValue keywordsType
 */
class SearchCompanies extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_CRM;

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_INFORMATION = 'information',
		ORDERBY_EXPERTISEAREA = 'expertiseArea',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName',
		ORDERBY_UPDATE_DATE = 'updateDate',
		ORDERBY_TYPE = 'type',
		ORDERBY_TOWN = 'town',
		ORDERBY_NAME = 'name';
	/**#@-*/

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_UPDATED = 'updated';
	/**#@-*/

	/**#@+
	 * @var int types of keywords
	 */
	const
		KEYWORD_TYPE_DEFAULT = 'default',
		KEYWORD_TYPE_NAME = 'name',
		KEYWORD_TYPE_PHONES = 'phones';
	/**#@- */

	/**
	 * Companies constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		//Type de mots clefs
		$keywordsType = new InputEnum('keywordsType');
		$keywordsType->setModeDefaultValue(self::KEYWORD_TYPE_DEFAULT);
		$keywordsType->setAllowedValues([
			self::KEYWORD_TYPE_DEFAULT, self::KEYWORD_TYPE_NAME, self::KEYWORD_TYPE_PHONES
		]);
		$this->addInput($keywordsType);

		$this->addInput(new InputMultiDict('states', 'specific.setting.state.company'));
		$this->addInput(new InputMultiDict('origins', 'specific.setting.origin'));
		$this->addInput(new InputMultiDict('expertiseAreas', 'specific.setting.expertiseArea'));

		// period
		$period = new InputEnum('period');
		$period->setModeDefaultValue(BM::INDIFFERENT);
		$period->setAllowedValues([BM::INDIFFERENT, self::PERIOD_CREATED, self::PERIOD_UPDATED]);
		$this->addInput($period);

		// sorting column
		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_INFORMATION, self::ORDERBY_EXPERTISEAREA, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_UPDATE_DATE, self::ORDERBY_TYPE, self::ORDERBY_TOWN,
			self::ORDERBY_NAME
		]);

		$this->addInput($sort);

		// period start & end dates
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);

	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 *
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
