<?php
/**
 * companies.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Companies\Specifications\HaveCreateAccess;
use BoondManager\APIs\Companies\Specifications\HaveSearchAccess;

class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'name',
		'expertiseArea',
		'state',
		'informationComments',
		'website',
		'phone1',
		'town',
		'country',
		'isEntityUpdating',
		'isEntityDeleting',
		'mainManager' => [
			'id',
			'firstName',
			'lastName'
		],
	];

	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchCompanies();
		$filter->setAndFilterData( $this->requestAccess->getParams() );

		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Services\Extraction\Companies('customers.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return;
		}

		$result = Services\Companies::search($filter);

		foreach($result->rows as $entity){
			/** @var Company $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess );

		// retrieve filtered params
		$filter = new Filters\SaveInformation();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$company = Services\Companies::buildFromFilter($filter);

		if(Services\Companies::create($company)) {
			$this->sendJSONResponse([
				'data' => $company->filterFields(Information::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
