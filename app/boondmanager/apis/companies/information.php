<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Services;
use BoondManager\APIs\Companies\Specifications\HaveReadAccess;
use BoondManager\APIs\Companies\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Information extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'updateDate',
		'name',
		'state',
		'website',
		'phone1',
		'fax',
		'postcode',
		'address',
		'town',
		'country',
		'number',
		'origin' => [
			'typeOf',
			'detail'
		],
		'staff',
		'expertiseArea',
		'informationComments',
		'departments',
		'vatNumber',
		'registrationNumber',
		'legalStatus',
		'registeredOffice',
		'apeCode',
		'billingDetails' => [
			'id', 'name', 'contact', 'email1', 'email2', 'email3', 'phone1', 'postcode', 'town', 'country', 'address1', 'address2', 'address3', 'state'
		],
		'socialNetworks' => [
			'network',
			'url'
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		],
		'files' => [
			'id',
			'name'
		],
		'influencers' => [
			'id',
			'lastName',
			'firstName',
		],
		'parentCompany' => [
			'id',
			'name'
		]

	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Companies::get($id, Company::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_INFORMATION), $entity );
		} else $this->error(404);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$entity = Services\Companies::get($this->requestAccess->id, Company::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Company::TAB_INFORMATION), $entity );

		$filter = new Filters\SaveInformation($entity);
		$filter->setData($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		//Build project
		Services\Companies::buildFromFilter($filter, $entity);

		if(Services\Companies::update($entity)) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

	}
}
