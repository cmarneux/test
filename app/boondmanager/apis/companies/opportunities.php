<?php
/**
 * opportunities.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\APIs\Companies\Specifications\HaveReadAccess;
use BoondManager\APIs\Opportunities\Filters\SearchOpportunities;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Models\Opportunity;
use BoondManager\Services;

class Opportunities extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'title',
		'reference',
		'typeOf',
		'mode',
		'state',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'turnoverWeightedExcludingTax',
		'canReadOpportunity',
		'canWriteOpportunity',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		$entity = Services\Companies::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_OPPORTUNITIES), $entity);

		$filter = new SearchOpportunities();
		$filter->sort->setDefaultValue(SearchOpportunities::ORDER_DESC);
		$filter->order->setDefaultValue(SearchOpportunities::ORDERBY_CREATIONDATE);
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Opportunities::search($filter);
		Services\Opportunities::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta'=>[
				'totals'=>[
					'rows' => $result->total,
					'turnoverWeightedExcludingTax' => $result->turnoverWeightedExcludingTax
				]
			],
			'data'=>$result->rows
		]);
	}
}
