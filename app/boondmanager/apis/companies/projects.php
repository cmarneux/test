<?php
/**
 * projects.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\APIs\Companies\Specifications\HaveReadAccess;
use BoondManager\APIs\Projects\Filters\SearchProjects;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Models\Project;
use BoondManager\Services;

class Projects extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'typeOf',
		'mode',
		'reference',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'turnoverSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'canReadProject',
		'canWriteProject',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'opportunity' => [
			'id',
			'title'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		$entity = Services\Companies::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_PROJECTS), $entity);

		$filter = new SearchProjects();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchProjects::ORDERBY_ENDDATE);
		$filter->order->setDefaultValue(SearchProjects::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );
		$filter->sumAdditionalData->setValue(true);

		$this->checkFilter($filter);

		$result = Services\Projects::search($filter);
		Services\Projects::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"turnoverSimulatedExcludingTax" => $result->turnoverSimulatedExcludingTax,
					"costsSimulatedExcludingTax" => $result->costsSimulatedExcludingTax,
					"marginSimulatedExcludingTax" => $result->marginSimulatedExcludingTax,
					"profitabilitySimulated" => $result->profitabilitySimulated
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
