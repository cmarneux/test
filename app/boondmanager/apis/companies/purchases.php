<?php
/**
 * purchases.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Companies;

use BoondManager\APIs\Companies\Specifications\HaveReadAccess;
use BoondManager\APIs\Purchases\Filters\SearchPurchases;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Company;
use BoondManager\Services;

class Purchases extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'title',
		'subscription',
		'typeOf',
		'reference',
		'state',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'amountExcludingTax',
		'deltaExcludingTax',
		'engagedPaymentsAmountExcludingTax',
		'canReadPurchase',
		'canWritePurchase',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'project' => [
			'id',
			'reference'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		$entity = Services\Companies::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_PURCHASES), $entity);

		$filter = new SearchPurchases();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchPurchases::ORDERBY_CREATIONDATE);
		$filter->order->setDefaultValue(SearchPurchases::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Purchases::search($filter);

		Services\Purchases::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"amountExcludingTax" => $result->totalAmountExcludingTax,
					"engagedPaymentsAmountExcludingTax" => $result->engagedPaymentsAmountExcludingTax,
					"deltaExcludingTax" => $result->deltaExcludingTax,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
