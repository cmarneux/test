<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Companies;

/**
 * Class Rights
 * @package BoondManager\APIs\Projects
 */
class Rights extends AbstractController {
	/**
	 * Get project's rights
	 */
	public function api_get() {
		$project = Companies::get($this->requestAccess->id);
		if(!$project) $this->error(404);

		$this->sendJSONResponse([
			'data' => Companies::getRights($project)
		]);
	}
}
