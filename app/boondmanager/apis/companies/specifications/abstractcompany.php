<?php
/**
 * abstractcompany.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Company;
use BoondManager\Lib\RequestAccess;

abstract class AbstractCompany extends AbstractSpecificationItem{
    /**
     * get the company from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Company|null
     */
    public function getData($request){
        if($request->data instanceof Company) return $request->data;
        else return null;
    }
}
