<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class HaveCreateAccess extends AbstractCompany
{
	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		return $user->hasAccess(BM::MODULE_CRM);
	}
}
