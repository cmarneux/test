<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveDeleteAccess extends AbstractCompany{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// TODO: faire la spec basé sur celle de loic
		return true;
	}
}
