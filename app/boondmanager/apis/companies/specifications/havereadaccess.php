<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use BoondManager\Services\BM;
use BoondManager\Models\Company;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadThroughGroupAgenciesBUPoles;
use BoondManager\OldModels\Specifications\RequestAccess\CRM\CanReadWriteThroughHierarchyInfluencersBUPoles;
use BoondManager\Lib\Specifications\TabBehavior;

class HaveReadAccess extends AbstractCompany{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;
		$entity = $this->getData($request);

		if(!$entity) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->getTab()) {
			case Company::TAB_INFORMATION:
			case Company::TAB_ACTIONS:
			case Company::TAB_CONTACTS:
			case Company::TAB_OPPORTUNITIES:
			case Company::TAB_PROJECTS:
			case Company::TAB_PURCHASES:
			case Company::TAB_INVOICES:
				if(!$user->hasAccess(BM::MODULE_CRM)) return false;

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$read |= (new CanReadThroughGroupAgenciesBUPoles(BM::MODULE_CRM))
					->or_( new CanReadWriteThroughHierarchyInfluencersBUPoles )
					->isSatisfiedBy($request);

				return boolval($read);
			case Company::TAB_DEFAULT:
				// check a right access on any other tab
				$read = new self(Company::TAB_INFORMATION);
				foreach(Company::getAllTabs() as $tab){
					$read->setTab($tab);
					if($read->isSatisfiedBy($request)) return true;
				}
				return false;
			default:
				return false;
		}
	}
}
