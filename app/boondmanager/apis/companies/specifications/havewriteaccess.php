<?php
/**
 * havewriteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use BoondManager\Services\BM;
use BoondManager\Models\Company;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadThroughGroupAgenciesBUPoles;
use BoondManager\OldModels\Specifications\RequestAccess\CRM\CanReadWriteThroughHierarchyInfluencersBUPoles;
use BoondManager\OldModels\Specifications\RequestAccess\HaveRight;
use BoondManager\Lib\Specifications\TabBehavior;

class HaveWriteAccess extends AbstractCompany{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user   = $request->user;
		$entity = $this->getData($request);

		if (!$entity) return false;

		if ($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->getTab()) {
			case Company::TAB_INFORMATION:
				if(!$user->hasAccess(BM::MODULE_CRM)) return false;

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$read |= (new CanReadThroughGroupAgenciesBUPoles(BM::MODULE_CRM))
				          ->or_(
				               (new CanReadWriteThroughHierarchyInfluencersBUPoles)
				               ->and_(new HaveRight(BM::RIGHT_WRITEALL, BM::MODULE_CRM))
				          )
				          ->isSatisfiedBy($request);
				return $read;

			default:
				return false;
		}

	}
}
