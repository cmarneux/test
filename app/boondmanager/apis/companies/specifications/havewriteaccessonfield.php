<?php
/**
 * userhavewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractCompany {

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * check if the object match the specification
	 * @param mixed $object
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($object)
	{
		/** @var CurrentUser $user*/
		$user = $object->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$company = $this->getData($object);

		switch($this->field){
			case 'creationDate':
				return $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_CRM);
			case 'influencers':
			case 'mainManager':
			case 'agency':
				return !$company || $user->hasRight(BM::RIGHT_ASSIGNMENT, BM::MODULE_CRM);
			default:
				throw new \Exception('no rules for field "'.$this->field.'"');
		}
	}
}
