<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Companies\Specifications;

use BoondManager\APIs\Purchases\Specifications\HaveCreateAccess;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Candidate;
use BoondManager\Models\Company;

class IsActionAllowed extends AbstractCompany {

	const RIGHTS_ACTIONS = ['share', 'addAction', 'addContact', 'addOpportunity', 'addProject', 'addPurchase'];

	private $action;

	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->action){
			case 'share':
				$allow = true;
				break;
			case 'addAction':
				$allow = (new HaveWriteAccess(Company::TAB_ACTIONS))->isSatisfiedBy($request);
				break;
			case 'addContact':
				$allow = (new \BoondManager\APIs\Contacts\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			case 'addOpportunity':
				$allow = (new \BoondManager\APIs\Opportunities\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			case 'addProject':
				$allow = (new \BoondManager\APIs\Projects\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			case 'addPurchase':
				$allow = (new HaveCreateAccess())->isSatisfiedBy($request);
				break;
			default:
				$allow = false;
				break;
		}
		return $allow;
	}
}
