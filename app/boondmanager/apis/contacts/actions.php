<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Contact;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Contacts\Specifications\HaveReadAccess;

class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		$entity = Services\Contacts::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Contact::TAB_ACTIONS), $entity);

		$filter = new SearchActions();
		$filter->setIndifferentPerimeter();
		$filter->setAvailableCategories([BM::CATEGORY_CRM_CONTACT]);
		$filter->sort->setDefaultValue(SearchActions::ORDERBY_DATE);
		$filter->order->setDefaultValue(SearchActions::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());

		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
