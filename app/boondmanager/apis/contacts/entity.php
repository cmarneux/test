<?php
/**
 * contacts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Company;
use BoondManager\Models\Contact;
use BoondManager\Services;
use BoondManager\APIs\Contacts\Specifications\HaveReadAccess;
use BoondManager\APIs\Contacts\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Contacts\Specifications\HaveCreateAccess;

class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'mainManager' => [
			'id'
		],
		'agency' => [
			'id'
		],
		'pole' => [
			'id'
		],
	];

	const ALLOWED_FIELDS_FOR_GET = [
		'updateDate',
		'civility',
		'firstName',
		'lastName',
		'company' => [
			'id'
		],
		'agency' => [
			'id'
		],
		'mainManager' => [
			'id'
		],
		'pole' => [
			'id'
		]
	];

	const ALLOWED_FIELDS_FOR_DEFAULT = [
		'state',
		'country',
		'origin',
		'company' => [
			'id',
			'name',
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing profil
			$entity = Services\Contacts::get($this->requestAccess->id);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			$this->checkAccessWithSpec( new HaveReadAccess(Contact::TAB_DEFAULT), $entity );
			$ALLOWED_FIELDS = array_merge(self::ALLOWED_FIELDS,self::ALLOWED_FIELDS_FOR_GET);
		} else {
			// get an empty entity for a future creation
			$company = Services\Companies::get($this->requestAccess->get('company'), Company::TAB_INFORMATION);
			if(!$company) $this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE, 'No company found, please check the value of the company parameter');

			$this->checkAccessWithSpec( new HaveCreateAccess ); // TODO
			$entity = Services\Contacts::getNew($company);
			$ALLOWED_FIELDS = array_merge(self::ALLOWED_FIELDS,self::ALLOWED_FIELDS_FOR_DEFAULT);
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$profil = Services\Contacts::get($this->requestAccess->id);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $profil );

		$deleted = Services\Contacts::delete($profil);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
