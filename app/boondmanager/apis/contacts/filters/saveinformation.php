<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contacts\Filters;

use BoondManager\APIs\Application\Dictionary;
use BoondManager\Lib\Filters\Inputs\Attributes\Address;
use BoondManager\Lib\Filters\Inputs\Attributes\Country;
use BoondManager\Lib\Filters\Inputs\Attributes\Email;
use BoondManager\Lib\Filters\Inputs\Attributes\Phone;
use BoondManager\Lib\Filters\Inputs\Attributes\PostCode;
use BoondManager\Lib\Filters\Inputs\Attributes\SourceOrOrigin;
use BoondManager\Lib\Filters\Inputs\Attributes\Town;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\Company;
use BoondManager\Lib\Filters\Inputs\Relationships\Influencers;
use BoondManager\Lib\Filters\Inputs\Relationships\MainManager;
use BoondManager\Lib\Filters\Inputs\Relationships\Pole;
use BoondManager\Models\Contact;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use BoondManager\Lib\Filters\Inputs\Attributes\WebSocial;

/**
 * Class SaveInformation
 * @package BoondManager\Models\Filters\Profiles\Contacts
 * @property InputString firstName
 * @property InputString lastName
 * @property InputDateTime creationDate
 * @property InputDict civility
 * @property InputDict state
 * @property Email email1
 * @property Email email2
 * @property Email email3
 * @property Phone phone1
 * @property Phone phone2
 * @property SourceOrOrigin origin
 * @property InputMultiObjects socialNetwarks
 * @property \BoondManager\Lib\Filters\Inputs\Attributes\WebSocial[] socialNetworks
 */
class SaveInformation extends AbstractJsonAPI
{
	/**
	 * @var Contact
	 */
	private $_contact;

	/**
	 * SaveInformation constructor.
	 * @param Contact $contact
	 */
	public function __construct(Contact $contact = null)
	{

		$this->_contact = $contact;

		parent::__construct();

		$this->addInput( new InputString('firstName') );
		$this->addInput( new InputString('lastName') );
		$this->addInput( new InputDateTime('creationDate') );
		$this->addInput( new InputDict('civility', 'specific.setting.civility') );
		$this->addInput( new InputDict('state', 'specific.setting.state.contact' ) );

		for($i=1; $i<=3; $i++) {
			$this->addInput( new Email('email'.$i) );
		}

		for($i=1; $i<=2; $i++) {
			$this->addInput( new Phone('phone'.$i) );
		}

		$this->addInput( new Phone('fax') );
		$this->addInput( new Address() );
		$this->addInput( new PostCode() );
		$this->addInput( new Town() );
		$this->addInput( new Country() );

		$input = new InputString('function');
		$input->setMaxLength(100);
		$this->addInput($input);

		$input = new InputString('department');
		$input->setMaxLength(100);
		$this->addInput($input);

		$this->addInput(new InputString('informationComments'));

		$this->addInput( new SourceOrOrigin('origin') );
		$this->addInput( new InputMultiDict('activityAreas', 'specific.setting.activityArea' ) );
		$this->addInput( new InputMultiDict('tools', 'specific.setting.tool' ) );

		$WebSocial = new WebSocial();
		$input = new InputMultiObjects('socialNetworks', $WebSocial);
		$this->addInput($input);

		$this->addInput( new MainManager());
		$this->addInput( new Agency() );
		$this->addInput( new Influencers());
		$this->addInput( new Company() );
		$this->addInput( new Pole() );

		if($this->isCreation()) {
			$this->adaptForCreation();
		}else{
			$this->adaptForEdition();
		}
	}

	/**
	 * @return bool
	 */
	private function isCreation() {
		return !$this->_contact || !$this->_contact->id;
	}

	private function adaptForCreation() {
		$this->firstName->setRequired(true);
		$this->lastName->setRequired(true);
	}

	private function adaptForEdition() {
		$this->firstName->setAllowEmptyValue( false );
		$this->lastName->setAllowEmptyValue( false );
	}
}
