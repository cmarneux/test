<?php
/**
 * contacts.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Contacts
 * build a filter for lists/contacts (search & sort)
 * @package BoondManager\Models\Filters\Search
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputMultiValues sort
 * @property InputMultiValues states
 * @property InputMultiValues origins
 * @property InputMultiValues activityAreas
 * @property InputMultiValues expertiseAreas
 * @property InputMultiValues tools
 * @property InputValue period
 *
 * @property InputBoolean returnLastAction
 */
class SearchContacts extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_CRM;

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_COMPANY_EXPERTISEAREA = 'company.expertiseArea',
		ORDERBY_LASTNAME = 'lastName',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName',
		ORDERBY_UPDATE_DATE = 'updateDate',
		ORDERBY_TYPE = 'type',
		ORDERBY_TOWN = 'town',
		ORDERBY_FUNCTION = 'function',
		ORDERBY_COMPANY_NAME = 'company.name';
	/**#@-*/

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_UPDATED = 'updated',
		PERIOD_ACTIONS = 'actions';
	/**#@-*/

	/**#@+
	 * @var int types of keywords
	 */
	const
		KEYWORD_TYPE_DEFAULT = 'default',
		KEYWORD_TYPE_LASTNAME = 'lastName',
		KEYWORD_TYPE_FIRSTNAME = 'firstName',
		KEYWORD_TYPE_FULLNAME = 'fullName',
		KEYWORD_TYPE_COMPANYFULLNAME = 'companyFullName',
		KEYWORD_TYPE_EMAILS = 'emails',
		KEYWORD_TYPE_PHONES = 'phones';
	/**#@- */

	/**
	 * Contacts constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		// should search return last action of each contact
		$returnLastAction = new InputBoolean('returnLastAction');
		$this->addInput($returnLastAction);

		//Type de mots clefs
		$keywordsType = new InputEnum('keywordsType');
		$keywordsType->setModeDefaultValue(self::KEYWORD_TYPE_DEFAULT);
		$keywordsType->setAllowedValues([
			self::KEYWORD_TYPE_DEFAULT, self::KEYWORD_TYPE_LASTNAME, self::KEYWORD_TYPE_FIRSTNAME, self::KEYWORD_TYPE_FULLNAME,
			self::KEYWORD_TYPE_COMPANYFULLNAME, self::KEYWORD_TYPE_EMAILS, self::KEYWORD_TYPE_PHONES
		]);
		$this->addInput($keywordsType);

		$this->addInput(new InputMultiDict('states', 'specific.setting.state.contact'));
		$this->addInput(new InputMultiDict('origins', 'specific.setting.origin'));
		$this->addInput(new InputMultiDict('activityAreas','specific.setting.activityArea'));
		$this->addInput(new InputMultiDict('expertiseAreas','specific.setting.expertiseArea'));
		$this->addInput(new InputMultiDict('tools','specific.setting.tool'));

		// period
		$period = new InputEnum('period');
		$period->setModeDefaultValue( BM::INDIFFERENT );
		$tabPS = [self::PERIOD_CREATED, self::PERIOD_UPDATED, self::PERIOD_ACTIONS];
		foreach(Dictionary::getDict('specific.setting.action.contact') as $item)
			$tabPS[] = self::PERIOD_ACTIONS.'_'.$item['id'];
		$period->setAllowedValues($tabPS);
		$this->addInput($period);

		// sorting column
		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_COMPANY_EXPERTISEAREA, self::ORDERBY_LASTNAME, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_UPDATE_DATE, self::ORDERBY_TYPE,
			self::ORDERBY_TOWN, self::ORDERBY_FUNCTION, self::ORDERBY_COMPANY_NAME
		]);
		$this->addInput($sort);

		// period start & end dates
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);

	}

	/*
	 * définition de quelques aides pour manipuler ce filtre
	 */

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 *
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
