<?php
/**
 * contacts.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts;

use BoondManager\Models\Contact;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Contacts\Specifications\HaveCreateAccess;
use BoondManager\APIs\Contacts\Specifications\HaveSearchAccess;

class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'civility',
		'firstName',
		'lastName',
		'state',
		'function',
		'department',
		'email1',
		'phone1',
		'phone2',
		'town',
		'country',
		'isEntityUpdating',
		'isEntityDeleting',
		'mainManager' => [
			'id',
			'firstName',
			'lastName'
		],
		'company' => [
			'id',
			'name',
			'expertiseArea'
		],
		'lastAction' => [
			'id',
			'startDate',
			'typeOf',
			'text'
		]
	];

	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchContacts();
		$filter->setAndFilterData($this->requestAccess->getParams());

		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Services\Extraction\Contacts('contacts.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return;
		}

		$result = Services\Contacts::search($filter);
		foreach ($result->rows as $entity){
			/** @var Contact $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess );

		// retrieve filtered params
		$filter = new Filters\SaveInformation();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$profil = Services\Contacts::buildFromFilter($filter);

		if(Services\Contacts::create($profil)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(Information::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
