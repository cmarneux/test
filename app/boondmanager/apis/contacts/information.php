<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Contact;
use BoondManager\Services;
use BoondManager\APIs\Contacts\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Information extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'updateDate',
		'firstName',
		'lastName',
		'state',
		'email1',
		'email2',
		'email3',
		'phone1',
		'phone2',
		'fax',
		'postcode',
		'address',
		'town',
		'country',
		'origin' => [
			'typeOf',
			'detail'
		],
		'activityAreas',
		'informationComments',
		'tools',
		'department',
		'function',
		'socialNetworks' => [
			'network',
			'url'
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		],
		'influencers' => [
			'id',
			'lastName',
			'firstName'
		],
		'company' => [
			'id',
			'name',
			'phone1',
			'postcode',
			'website',
			'registrationNumber',
			'fax',
			'town',
			'address',
			'country',
			'state',
			'staff',
			'informationComments',
			'departments',
			'expertiseArea'
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Contacts::get($id, Contact::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			//$this->checkAccessWithSpec( new HaveReadAccess(Company::TAB_INFORMATION), $entity ); //TODO
		} else $this->error(404);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$entity = Services\Contacts::get($this->requestAccess->id, Contact::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Contact::TAB_INFORMATION), $entity );

		$filter = new Filters\SaveInformation($entity);
		$filter->setData($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		//Build project
		$entity = Services\Contacts::buildFromFilter($filter, $entity);

		if(Services\Contacts::update($entity, Contact::TAB_INFORMATION)) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
