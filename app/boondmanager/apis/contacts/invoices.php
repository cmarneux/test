<?php
/**
 * invoices.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts;

use BoondManager\APIs\Contacts\Specifications\HaveReadAccess;
use BoondManager\APIs\Invoices\Filters\SearchInvoices;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Contact;
use BoondManager\Services;

class Invoices extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'expectedPaymentDate',
		'turnoverInvoicedExcludingTax',
		'turnoverInvoicedIncludingTax',
		'paymentMethod',
		'creditNote',
		'reference',
		'state',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'canReadInvoice',
		'canWriteInvoice'
	];

	public function api_get() {
		$entity = Services\Contacts::get($this->requestAccess->id, Contact::TAB_INVOICES);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Contact::TAB_INVOICES), $entity);

		$filter = new SearchInvoices();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchInvoices::ORDERBY_CREATIONDATE);
		$filter->order->setDefaultValue(SearchInvoices::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Invoices::search($filter);
		Services\Invoices::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"turnoverInvoicedExcludingTax" => $result->turnoverExcludingTax,
					"turnoverInvoicedIncludingTax" => $result->turnoverIncludingTax,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
