<?php
/**
 * opportunities.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts;

use BoondManager\APIs\Contacts\Specifications\HaveReadAccess;
use BoondManager\APIs\Orders\Filters\SearchOrders;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Contact;
use BoondManager\Services;

class Orders extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'startDate',
		'endDate',
		'number',
		'reference',
		'customerAgreement',
		'turnoverInvoicedExcludingTax',
		'turnoverOrderedExcludingTax',
		'deltaInvoicedExcludingTax',
		'state',
		'canReadOrder',
		'canWriteOrder',
	];

	public function api_get() {
		$entity = Services\Contacts::get($this->requestAccess->id, Contact::TAB_ORDERS);
		if(!$entity) $this->error(404);


		$this->checkAccessWithSpec( new HaveReadAccess(Contact::TAB_ORDERS), $entity);

		$filter = new SearchOrders();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchOrders::ORDERBY_CREATIONDATE);
		$filter->order->setDefaultValue(SearchOrders::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Orders::search($filter);
		Services\Orders::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"turnoverInvoicedExcludingTax" => $result->turnoverInvoicedExcludingTax,
					"turnoverOrderedExcludingTax" => $result->turnoverOrderedExcludingTax,
					"deltaInvoicedExcludingTax" => $result->deltaInvoicedExcludingTax
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
