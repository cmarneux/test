<?php
/**
 * abstractcontact.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Contacts\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Contact;
use BoondManager\Lib\RequestAccess;

abstract class AbstractContact extends AbstractSpecificationItem{
    /**
     * get the contact from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Contact|null
     */
    public function getData($request){
        if($request->data instanceof Contact) return $request->data;
        else return null;
    }
}
