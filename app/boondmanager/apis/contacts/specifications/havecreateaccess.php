<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contacts\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveCreateAccess extends AbstractContact{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// TODO: faire la spec basé sur celle de loic
		// TODO 2: s'assurer qu'on ai un droit de lecture sur la société parente (car on récupère toutes les infos lors du getDefault)

		return $user->hasAccess(BM::MODULE_CRM);
	}
}
