<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contacts\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveDeleteAccess extends AbstractContact{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// TODO: faire la spec basé sur celle de loic
		return true;
	}
}
