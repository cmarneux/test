<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contacts\Specifications;

use BoondManager\Models\Contact;
use BoondManager\Lib\RequestAccess;
use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadThroughGroupAgenciesBUPoles;
use BoondManager\OldModels\Specifications\RequestAccess\CRM\CanReadWriteThroughHierarchyInfluencersBUPoles;
use BoondManager\Services\BM;

class HaveReadAccess extends AbstractContact
{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;
		$entity = $this->getData($request);

		if(!$entity) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->getTab()) {
			case Contact::TAB_INFORMATION:
			case Contact::TAB_ACTIONS:
			case Contact::TAB_OPPORTUNITIES:
			case Contact::TAB_PROJECTS:
			case Contact::TAB_PURCHASES:
			case Contact::TAB_INVOICES:
			case Contact::TAB_ORDERS:
				if(!$user->hasAccess(BM::MODULE_CRM)) return false;

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$read |= (new CanReadThroughGroupAgenciesBUPoles(BM::MODULE_CRM))
					->or_( new CanReadWriteThroughHierarchyInfluencersBUPoles )
					->isSatisfiedBy($request);

				return boolval($read);
			case Contact::TAB_DEFAULT:
				// check a right access on any other tab
				$read = new self(Contact::TAB_INFORMATION);
				foreach(Contact::getAllTabs() as $tab){
					$read->setTab($tab);
					if($read->isSatisfiedBy($request)) return true;
				}
				return false;
			default:
				return false;
		}
	}
}
