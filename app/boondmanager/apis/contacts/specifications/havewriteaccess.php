<?php
/**
 * havewriteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contacts\Specifications;

use BoondManager\Models\Contact;
use BoondManager\Lib\RequestAccess;
use BoondManager\Lib\Specifications\TabBehavior;

class HaveWriteAccess extends AbstractContact
{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){


		$user = $request->user;
		$entity = $this->getData($request);

		if(!$entity) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->getTab()) {
			case Contact::TAB_INFORMATION:
				return (new HaveReadAccess($this->getTab()))->isSatisfiedBy($request);
			default:
				return false;
		}
	}
}
