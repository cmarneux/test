<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contracts;

use BoondManager\APIs\Advantages\Filters\SearchAdvantages;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\AdvantageType;
use BoondManager\Services;
use BoondManager\APIs\Contracts\Specifications\HaveReadAccess;

class Advantages extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'date',
		'advantageType' => [
			'reference',
			'name'
		],
		'quantity',
		'costPaid',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'agency' => [
			'id',
			'name'
		],
		'resource' => [
			'id',
			'lastName',
			'firstName'
		],
	];

	public function api_get() {
		$contract = Services\Contracts::getWithAgencyConfig($this->requestAccess->id);
		if(!$contract) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(), $contract);

		$filter = new SearchAdvantages();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($contract->getReference());

		$this->checkFilter($filter);

		$result = Services\Advantages::search($filter);

		$result->filterFields(self::ALLOWED_FIELDS);

		$types = array_map(function(AdvantageType $value) {
			return [
				'id'=>$value->id,
				'value' => $value->name
			];
		}, $contract->agency->advantageTypes);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				],
				'advantagesTypes' => $types
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

}
