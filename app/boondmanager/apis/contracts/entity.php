<?php
/**
 * contracts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contracts;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\APIs\Contracts\Specifications\HaveCreateAccess;
use BoondManager\APIs\Contracts\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Contracts\Specifications\HaveReadAccess;
use BoondManager\APIs\Contracts\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'typeOf', 'employeeType', 'workingTimeType', 'numberOfHoursPerWeek', 'classification', 'endDate',
		'probationEndDate', 'renewalProbationEndDate', 'hourlySalary', 'forceHourlySalary', 'contractAverageDailyCost',
		'monthlyExpenses', 'numberOfWorkingDays', 'chargeFactor', 'startDate', 'endDate', 'contractAverageDailyProductionCost',
		'forceContractAverageDailyProductionCost', 'currency', 'exchangeRate', 'currencyAgency', 'exchangeRateAgency',
		'informationComments', 'monthlySalary', 'dailyExpenses',
		'expensesDetails' => [
			'id', 'reference', 'periodicity', 'netAmount', 'name'
		],
		'exceptionalScales' => [
			'reference',
			'rules' => [
				'reference', 'priceExcludingTaxOrPriceRate', 'grossCostOrSalaryRate', 'name'
			]
		],
		'advantages' => [
			'reference', 'frequency', 'category', 'participationQuota', 'employeeQuota', 'agencyQuota'
		],
		'dependsOn' => [
			'id', 'firstName', 'lastName', 'typeOf',
			'mainManager' => ['id', 'lastName', 'firstName'],
			'hrManager' => ['id', 'lastName', 'firstName']
		],
		'agency' => [
			'id', 'name',
			'expenseTypes' => [
				'reference', 'taxRate', 'name'
			],
			'advantageTypes' => [
				'reference', 'name', 'frequency', 'category', 'participationQuota', 'employeeQuota', 'agencyQuota'
			],
			'exceptionalScaleTypes' => [
				'reference',
				'name',
				'rules' => [
					'reference', 'priceExcludingTaxOrPriceRate', 'grossCostOrSalaryRate', 'name'
				]
			]
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			$contract = Services\Contracts::getWithAgencyConfig($this->requestAccess->id);
			if(!$contract) $this->error(404);

			$this->checkAccessWithSpec( new HaveReadAccess(), $contract);
		} else {
			$resource = $this->requestAccess->get('resource');
			$candidate = ($resource) ? null : $this->requestAccess->get('candidate');
			$parentContract = $this->requestAccess->get('contract');
			$contract = Services\Contracts::getNew($resource, $candidate, $parentContract);

			$this->checkAccessWithSpec( new HaveCreateAccess(), $contract);
		}

		Services\Contracts::cleanUpContract($contract);

		$this->sendJSONResponse([
			'data' => $contract->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	public function api_put() {
		$contract = Services\Contracts::getWithAgencyConfig($this->requestAccess->id);
		if(!$contract) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(), $contract);

		$filter = new Filters\SaveEntity($contract);
		$filter->setData( $this->requestAccess->get('data') );

		$this->checkFilter($filter);

		$contract = Services\Contracts::buildFromFilter($filter, $contract);

		if( Services\Contracts::update($contract) ) {
			$this->sendJSONResponse([
				'data' => $contract->filterFields(self::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}

	public function api_delete() {
		$contract = Services\Contracts::getWithAgencyConfig($this->requestAccess->id);
		if(!$contract) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $contract);

		if( $status = Services\Contracts::delete($contract) ) {
			$this->sendJSONResponse([
				'data' => true
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
		}

	}
}
