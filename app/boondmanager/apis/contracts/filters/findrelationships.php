<?php
/**
 * save.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contracts\Filters;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputRelationship;


/**
 * Class FindRelationships
 * @package BoondManager\Models\Filters\Profiles\Contracts
 * @property \Wish\Filters\Inputs\InputRelationship dependsOn
 * @property InputRelationship contract
 */
class FindRelationships extends AbstractJsonAPI{
	public function __construct()
	{
		parent::__construct();

		$this->addInput( (new InputRelationship('dependsOn'))->setRequired(true) );
		$this->addInput( (new InputRelationship('contract')) );
	}

	public function getResourceID(){
		if($this->dependsOn->getType() === 'resource'){
			return $this->dependsOn->getValue();
		} else {
			return null;
		}
	}

	public function getCandidateID(){
		if($this->dependsOn->getType() === 'candidate'){
			return $this->dependsOn->getValue();
		}else{
			return null;
		}
	}

	public function getContractID(){
		return $this->contract->getValue();
	}
}


