<?php
/**
 * Save.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contracts\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Currency;
use BoondManager\Lib\Filters\Inputs\Attributes\ExchangeRate;
use BoondManager\Lib\Filters\Inputs\Relationships\Contact;
use BoondManager\Models;
use BoondManager\Models\Candidate;
use BoondManager\Models\Contract;
use BoondManager\Lib\Filters\Inputs\Attributes\AdvantageContract;
use BoondManager\OldModels\Filters\Profiles\ExceptionalScales;
use BoondManager\OldModels\Filters\Profiles\ExpenseDetails;
use BoondManager\Services\Candidates;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Employees;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;

/**
 * Class Save
 * @package BoondManager\Models\Filters\Profiles\Contracts
 * @property InputValue typeOf
 * @property InputValue employeeType
 * @property InputValue workingTimeType
 * @property InputInt numberOfHoursPerWeek
 * @property InputValue classification
 * @property InputDate startDate
 *
 * @property InputDate endDate
 *
 * @property InputDate probationEndDate
 * @property InputDate renewalProbationEndDate
 * @property InputFloat monthlySalary
 *
 * @property InputFloat hourlySalary
 * @property InputBoolean forceHourlySalary
 *
 * @property InputFloat contractAverageDailyCost
 * @property InputFloat dailyExpenses
 *
 * @property InputFloat monthlyExpenses
 * @property InputFloat numberOfWorkingDays
 * @property InputFloat chargeFactor
 * @property InputString informationComments
 *
 * @property InputValue currency
 * @property InputValue currencyAgency
 * @property InputFloat exchangeRate
 *
 * @property InputFloat exchangeRateAgency
 *
 * @property InputMultiObjects advantages
 * @property InputMultiObjects expensesDetails
 * @property InputMultiObjects exceptionalScales
 *
 * @property InputRelationship dependsOn
 * @property Contact contract
 *
 * @property InputFloat contractAverageDailyProductionCost
 * @property InputBoolean forceContractAverageDailyProductionCost
 */
class SaveEntity extends AbstractJsonAPI
{

	/**
	 * @var Contract
	 */
	protected $_contract;

	public function __construct(Contract $contract)
	{
		$this->_contract = $contract;

		parent::__construct();

		$type = new InputDict('typeOf', 'specific.setting.typeOf.contract', -1);
		$this->addInput($type);

		$employee = new InputDict('employeeType', 'specific.setting.typeOf.employee', -1);
		$this->addInput($employee);

		$input = new InputDict('workingTimeType', 'specific.setting.typeOf.workingTime', -1);
		$this->addInput($input);

		$input = new InputInt('numberOfHoursPerWeek', 35);
		$input->setMin(0)->setMax(168);
		$this->addInput($input);

		$input = new InputDict('classification', 'specific.setting.classification');
		$this->addInput($input);

		$start = new InputDate('startDate');
		$this->addInput($start);

		$end = new InputDate('endDate');
		$end->convertEmptyDate();
		$this->addInput($end);

		$input = new InputDate('probationEndDate');
		$input->setDefaultValue(null);
		$this->addInput($input);

		$input = new InputDate('renewalProbationEndDate');
		$input->setDefaultValue(null);
		$this->addInput($input);

		$this->addInput( new InputFloat('monthlySalary'));
		$this->addInput( new InputFloat('hourlySalary'));
		$this->addInput( new InputBoolean('forceHourlySalary'));
		$this->addInput( new InputFloat('contractAverageDailyCost'));
		$this->addInput( new InputBoolean('forceContractAverageDailyProductionCost'));
		$this->addInput( new InputFloat('contractAverageDailyProductionCost'));
		$this->addInput( new InputFloat('dailyExpenses'));
		$this->addInput( new InputFloat('monthlyExpenses'));

		$input = new InputFloat('numberOfWorkingDays', CurrentUser::instance()->getNumberOfWorkingDays()); //valeur stocké en session
		$input->setMax(365)->setMinExcluded(0);
		$this->addInput($input);

		$input = new InputFloat('chargeFactor'); //TODO default value
		$input->setMinExcluded(0);
		$this->addInput($input);

		$this->addInput( new InputString('informationComments', null) );
		$this->addInput( new Currency() );
		$this->addInput( new Currency('currencyAgency') );
		$this->addInput( new ExchangeRate() );
		$this->addInput( new ExchangeRate('exchangeRateAgency') );

		$expenses = new ExpenseDetails('expensesDetails');
		$expenses->setExpenseReferenceAllowed( $this->getExpensesTypesReferencesAllowed() );
		$this->addInput( new InputMultiObjects('expensesDetails', $expenses) );

		$advantages = new AdvantageContract('advantages');
		$advantages->setAllowedAdvantageTypes( $this->getAdvantagesTypes() );
		$input = new InputMultiObjects('advantages', $advantages);
		$this->addInput($input);

		$input = new InputMultiObjects('exceptionalScales', new ExceptionalScales('exceptionalScales'));
		$this->addInput($input);


		$depends = new InputRelationship('dependsOn');
		$depends->addFilterType(['resource','candidate']);
		$depends->addFilter(FILTER_CALLBACK, function ($value) use ($depends) {
			if ($depends->getType() == 'resource') {
				return Employees::find($value);
			} else {
				return Candidates::find($value);
			}
		});
		$this->addInput($depends);

		$contract = new \BoondManager\Lib\Filters\Inputs\Relationships\Contract();;
		$this->addInput($contract);

		$this->isCreation() ? $this->adaptForCreation() : $this->adaptForEdition();
	}

	/**
	 * @return bool
	 */
	private function isCreation() {
		return !$this->_contract || !$this->_contract->id;
	}

	private function isEdition() {
		return !$this->isCreation();
	}

	private function adaptForCreation() {
		$this->dependsOn->setRequired(true);
		$this->startDate->setRequired(true);

		$this->numberOfWorkingDays->markDefaultValueAsDefined(true);
		$this->numberOfHoursPerWeek->markDefaultValueAsDefined(true);
		$this->workingTimeType->markDefaultValueAsDefined(true);
		$this->typeOf->markDefaultValueAsDefined(true);
		$this->employeeType->markDefaultValueAsDefined(true);
	}

	private function adaptForEdition() {
		if($this->_contract->childContract) {
			$this->startDate->setDisabled(true);
			$this->endDate->setDisabled(true);
			$this->dependsOn->setDisabled(true);
			$this->contract->setDisabled(true);
		}
	}

	/**
	 * @return Models\Agency
	 */
	private function getAgency() {
		return $this->_contract->agency;
	}

	private function getDependsOn() {
		return $this->_contract->dependsOn;
	}

	protected function postValidation()
	{
		$ok = true;

		$profil     = $this->getDependsOn();
		$ptype      = $profil->typeOf;

		// save basic tests in variables
		$isResource = $ptype != Candidate::TYPE_CANDIDATE;
		$isExternal = $isResource && $profil->isExternalConsultant();

		if ($this->expensesDetails->getValue()) {
			$monthly = $daily = 0;

			foreach ($this->expensesDetails->getItems() as $subFilter) {
				/** @var ExpenseDetails $subFilter */
				if ($subFilter->periodicity->getValue() == 0) $daily += $subFilter->netAmount->getValue();
				else $monthly += $subFilter->netAmount->getValue();
			}

			$this->monthlyExpenses->setValue($monthly)->isValid();
			$this->dailyExpenses->setValue($daily)->isValid();
		}

		// si relation sur candidat alors impossible de fournir ExceptionalScale
		if (!$isResource) {
			$this->disableInput('exceptionalScales');
		} else {
			// si relation ressource & consultant externe alors CTR_CHARGE = 0 et non modifiable
			if ($isExternal) {
				$this->chargeFactor->setValue(0)->isValid();
			}
		}

		// si relation ressource & consultant OU durée hebo = 0 , et qu'on ne force pas le salaire horaire (forceHourly) alors le salaire heure vaut 0
		// sinon, si le salaire horaire est forcé, on prend celui du filtre, sinon calculé automatiquement a partir du salaire mensuel
		if ($isExternal || !$this->numberOfHoursPerWeek->getValue() && !$this->forceHourlySalary->getValue())
			$this->hourlySalary->setValue(0)->filter();
		else if (!$this->forceHourlySalary->getValue()) {
			$this->hourlySalary->setValue($this->monthlySalary->getValue() * 12 / 52 / $this->numberOfHoursPerWeek->getValue())->filter();
		}

		// construction du cjmAvantage
		$cjmAvantage = 0;
		$avantagesTypes = $this->getAdvantagesTypes();
		foreach ($this->advantages->getItems() as $subFilter) {
			/** @var AdvantageContract $subFilter */
			$avantageReference = $avantagesTypes[ $subFilter->reference->getValue() ];
			//employeeAmount est modifiable uniquement si la category est fixedAmount (Sinon, rien n'est modifiable)
			$employeeAmount = $avantageReference->category == Models\AdvantageType::CATEGORY_FIXEDAMOUNT ? $subFilter->employeeQuota->getValue() : $avantageReference->employeeQuota;

			//$subFilter = $avantageReference;
			$subFilter->agencyQuota->setValue($avantageReference->agencyQuota)->filter();
			$subFilter->participationQuota->setValue($avantageReference->participationQuota)->filter();
			$subFilter->employeeQuota->setValue($employeeAmount)->filter();
			$subFilter->category->setValue($avantageReference->category)->filter();
			$subFilter->frequency->setValue($avantageReference->frequency)->filter();

			$cout = 0;
			switch ($subFilter->category->getValue()) {
				case 0:
					$cout = $subFilter->employeeQuota->getValue() * $subFilter->agencyQuota->getValue();
					break;//Montant Fixe
				case 1:
					$cout = ($isExternal) ? 0 : $this->monthlySalary->getValue() * ($subFilter->employeeQuota->getValue() + $subFilter->agencyQuota->getValue() - $subFilter->participationQuota->getValue()) / 100;
					break;//Variable - Base Salaire
				case 2:
					$cout = $subFilter->employeeQuota->getValue() + $subFilter->agencyQuota->getValue() - $subFilter->participationQuota->getValue();
					break;//Forfait
				case 3:
					break;//Prêt

			}

			switch ($subFilter->frequency->getValue()) {
				case 1:
					$cjmAvantage += $cout;
					break;//Journalier
				case 2:
					$cjmAvantage += $cout * 12 / $this->numberOfWorkingDays->getValue();
					break;//Mensuel
				case 3:
					$cjmAvantage += $cout * 4 / $this->numberOfWorkingDays->getValue();
					break;//Trimestriel
				case 4:
					$cjmAvantage += $cout * 2 / $this->numberOfWorkingDays->getValue();
					break;//Semestriel
				case 5:
					$cjmAvantage += $cout / $this->numberOfWorkingDays->getValue();
					break;//Annuel
			}
		}

		// si on force le CJM de production ou le profil est ressource externe alors CJM PROD est celui envoyé et CJM Contrat calculé automatiquement
		// sinon CJM PROD ET CJM Contrat calculé automatiquement
		if ($this->forceContractAverageDailyProductionCost->getValue() || $isExternal) {
			$cost = $this->contractAverageDailyProductionCost->getValue() + $this->dailyExpenses->getValue() + $this->monthlyExpenses->getValue() * 12 / $this->numberOfWorkingDays->getValue();
			$cost += $cjmAvantage;
			$this->contractAverageDailyCost->setValue($cost)->filter();
		} else {
			$this->contractAverageDailyProductionCost->setValue($cjmAvantage + $this->dailyExpenses->getValue() + ($this->monthlySalary->getValue() * $this->chargeFactor->getValue() + $this->monthlyExpenses->getValue()) * 12 / $this->numberOfWorkingDays->getValue())->filter();
			$this->contractAverageDailyCost->setValue($this->contractAverageDailyProductionCost->getValue())->filter();
		}

		//if(isset($newData['CONTRAT']['CTR_DEBUT']) && $newData['CONTRAT']['CTR_DATEPE1'] < $newData['CONTRAT']['CTR_DEBUT'] || $newData['CONTRAT']['CTR_DATEPE1'] < $dataBDD->get('CTR_DEBUT')) $newData['CONTRAT']['CTR_DATEPE1'] = 'NULL';
		if ($this->startDate && $this->startDate->getValue()
			&& $this->probationEndDate && $this->probationEndDate->getValue()
			&& $this->startDate->getDateTime()->diff($this->probationEndDate->getDateTime())->invert
		) {
			$this->probationEndDate->setValue(null)->filter();
		}

		// renewalProbationEndDate ne peut pas etre après avant probationEndDate
		if ($p1 = $this->probationEndDate->getValue() && $p2 = $this->renewalProbationEndDate->getValue()) {
			if (!$this->probationEndDate->getDateTime()->diff($this->renewalProbationEndDate->getDateTime())->invert) {
				$this->renewalProbationEndDate->setValue(null)->filter();
			}
		}

		// la date de fin ne peut etre anterieure a la date de debut
		if ($this->startDate && $this->endDate && $this->startDate->getValue() && $this->endDate->getValue()
			&& $this->startDate->getDateTime()->diff($this->endDate->getDateTime())->invert
		) {
			$this->endDate->setValue(null)->filter();
		}

		return $ok;
	}

	/**
	 * @return Models\AdvantageType[]
	 */
	private function getAdvantagesTypes()
	{
		// agency advantages
		$types = Tools::useColumnAsKey('reference', $this->getAgency()->advantageTypes, $castString = true);

		if($this->isEdition()) {
			$types = array_merge( Tools::useColumnAsKey('reference', $this->_contract->advantages, $castString = true));
		}

		return $types;
	}

	/**
	 * @return Models\ExpenseType[] indexed by reference
	 */
	private function getExpensesTypesReferencesAllowed()
	{
		// taking expenses type from agency
		$expContract = Tools::useColumnAsKey('reference', $this->getAgency()->expenseTypes, $castString = true);

		if($this->isEdition()) {
			// adding expenses type from the contract
			foreach ($this->_contract->expensesDetails as $detail) {
				$expContract[ strval($detail->expenseType->reference) ] = $detail->expenseType;
			}
		}

		return $expContract;
	}

	private function getExceptionalScales()
	{
		//TODO
	}

	private function getExceptionalScalesReferencesAllowed()
	{
		//TODO
	}

	public static function getFilterForRelationships()
	{
		return new FindRelationships();
	}
}
