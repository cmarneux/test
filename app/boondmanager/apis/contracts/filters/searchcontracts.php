<?php
/**
 * contracts.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Contracts\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputValue;


/**
 * Class Resources
 * @property-read InputValue $user
 * @property-read \Wish\Filters\Inputs\InputMultiValues $contractTypes
 * @property-read \Wish\Filters\Inputs\InputValue $resourceTypes
 * @property-read \Wish\Filters\Inputs\InputMultiValues excludeResourceTypes
 * @property-read InputValue $resourceStates
 * @property-read \Wish\Filters\Inputs\InputValue $sort
 * @property-read \Wish\Filters\Inputs\InputValue $period
 * @property-read InputDate $startDate
 * @property-read InputDate $endDate
 * @package BoondManager\Models\Filters\Search
 */
class SearchContracts extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_RESOURCES;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_OVERLAPS = 'overlaps';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_RESOURCE_LASTNAME = 'resource.lastName',
		ORDERBY_TYPE = 'typeOf',
		ORDERBY_RESOURCE_FUNCTION = 'resource.fonction',
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_ENDDATE = 'endDate';
	/**#@-*/

	/**
	 * Advantages constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addInput(new InputMultiDict('contractTypes', 'specific.setting.typeOf.contract'));
		$this->addInput(new InputMultiDict('resourceTypes', 'specific.setting.typeOf.resource'));
		$this->addInput(new InputMultiDict('excludeResourceTypes', 'specific.setting.typeOf.resource'));
		$this->addInput(new InputDict('resourceStates', 'specific.setting.state.resource'));

		$period = new InputEnum('period');
		$period->setModeDefaultValue(BM::INDIFFERENT);
		$period->setAllowedValues([self::PERIOD_OVERLAPS]);
		$this->addInput($period);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([self::ORDERBY_STARTDATE, self::ORDERBY_TYPE, self::ORDERBY_ENDDATE, self::ORDERBY_RESOURCE_LASTNAME, self::ORDERBY_RESOURCE_FUNCTION]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}
}
