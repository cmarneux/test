<?php
/**
 * contracts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contracts;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\APIs\Contracts\Specifications\HaveCreateAccess;
use BoondManager\APIs\Contracts\Specifications\HaveSearchAccess;

class Index extends AbstractController{

	const ALLOWED_FIELDS = [
		'id'
	];

	public function api_get() {

		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchContracts;
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = Services\Contracts::search($filter);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post(){

		$relationShips = Filters\SaveEntity::getFilterForRelationships();
		$relationShips->setData($this->requestAccess->get('data'));
		$this->checkFilter($relationShips);

		//construction d'un nouveau contrat de référence
		$contract = Services\Contracts::getNew($relationShips->getResourceID(), $relationShips->getCandidateID(), $relationShips->getContractID());

		// verif des droits
		$this->checkAccessWithSpec( new HaveCreateAccess(), $contract);

		// creation du vrai contrat
		$filter = new Filters\SaveEntity($contract);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		$contract = Services\Contracts::buildFromFilter($filter, $contract);

		if(Services\Contracts::create($contract)) {
			$this->sendJSONResponse([
				'data' => $contract->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(Services\BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
