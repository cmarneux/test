<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Contracts;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Contracts;
use BoondManager\Services\BM;

class Rights extends AbstractController {
	public function api_get() {
		$entity = Contracts::get( $this->requestAccess->id );
		if(!$entity) $this->error(404);

		$this->sendJSONResponse([
			'data' => Contracts::getRights( $entity )
		]);
	}
}
