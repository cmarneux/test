<?php
/**
 * abstractcontract.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Contracts\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Models\Candidate;
use BoondManager\Models\Contract;
use BoondManager\Models\Employee;
use BoondManager\Lib\RequestAccess;

abstract class AbstractContract extends AbstractSpecificationItem{
	/**
	* get the contract from the request
	* @param RequestAccess $request
	* @return \BoondManager\Models\Contract|null
	*/
	public function getContract($request){
		if($request->data instanceof Contract) return $request->data;
		else return null;
	}

	/**
	 * @param Employee $resource
	 * @param CurrentUser $user
	 * @param Contract $contract
	 * @return bool
	 */
	protected function checkResource($resource, $user, $contract){
		if(!$user->hasAccess(BM::MODULE_RESOURCES, BM::MODULE_RESOURCES_ADMINISTRATIVE)) return false;

		$read = false;
		if($resource->visibility || $resource->id == $user->getEmployeeId() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)){
			$profilAccess = $user->checkHierarchyAccess($contract, BM::MODULE_RESOURCES );

			$notHierarchy = false;

			if($profilAccess > 0 && !$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && $resource->isManager() && !$user->isMyManager($resource->id))
				$notHierarchy = true;

			switch($profilAccess) {
				case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY:
					if(!$notHierarchy)
						$read = true;
					break;
				case BM::PROFIL_ACCESS_READ_WRITE:
				case BM::PROFIL_ACCESS_READ_ONLY:
					if($resource->id == $user->getEmployeeId()) {//Un manager qui consulte sa fiche
						if($resource->isTopManager() || ($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)))
							$read = true;
					} else if($user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && !$notHierarchy) {
						$read = true;
					}
					break;
			}
		}

		return $read;
	}

	/**
	 * @param Candidate $profil
	 * @param CurrentUser $user
	 * @param Contract $contract
	 * @return bool
	 */
	protected function checkCandidate($profil, $user, $contract){
		if(!$user->hasAccess(BM::MODULE_CANDIDATES)) return false;

		$read = false;

		if($profil->visibility || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)){
			$profilAccess = $user->checkHierarchyAccess( $contract, BM::MODULE_CANDIDATES );

			switch ($profilAccess){
				case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY: $read = true; break;
				case BM::PROFIL_ACCESS_READ_WRITE:
					if ( ! $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_CANDIDATES)) return false;
			}
		}

		return $read;
	}
}
