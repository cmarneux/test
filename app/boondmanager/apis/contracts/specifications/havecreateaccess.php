<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contracts\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveCreateAccess extends AbstractContract{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object) {
		$user = $object->getUser();

		if($user->isGod()) return true;

		return (new HaveWriteAccess())->isSatisfiedBy($object);
	}
}
