<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contracts\Specifications;

use BoondManager\Models\Candidate;
use BoondManager\Models\Contract;
use BoondManager\Lib\RequestAccess;

class HaveReadAccess extends AbstractContract{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $requestAccess
	 * @return bool
	 */
	public function isSatisfiedBy($requestAccess) {
		$user = $requestAccess->getUser();
		$contract = $this->getContract($requestAccess);
		if(!$contract) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		if($contract->dependsOn->typeOf != Candidate::TYPE_CANDIDATE){
			$read = $this->checkResource($contract->dependsOn, $user, $contract);
		}else{ // candidate
			$read = $this->checkCandidate($contract->dependsOn, $user, $contract);
		}

		return $read;
	}
}
