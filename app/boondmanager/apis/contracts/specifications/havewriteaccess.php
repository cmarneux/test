<?php
/**
 * havewriteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */


namespace BoondManager\APIs\Contracts\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveWriteAccess extends AbstractContract{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object) {
		$user = $object->user;

		if($user->isGod()) return true;

		return (new HaveReadAccess())->isSatisfiedBy($object);
	}
}
