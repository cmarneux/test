<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Contracts\Specifications;

use BoondManager\Lib\RequestAccess;
use Wish\Specifications\AbstractSpecificationItem;

class IsACtionAllowed extends AbstractSpecificationItem{

	const RIGHTS_ACTIONS = ['share'];

	private $action;

	public function __construct($action)
	{
		if(!in_array($action, self::RIGHTS_ACTIONS)) throw new \Exception("unknow action '$action'");
		$this->action = $action;
	}

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();
		if($user->isGod()) return true;

		switch ($this->action){
			case 'share' :
				return !$user->isEmployee();
		}

		return false;
	}
}
