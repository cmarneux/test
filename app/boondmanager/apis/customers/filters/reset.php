<?php
/**
 * reset.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Customers\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputString;
use BoondManager\Models;
use BoondManager\Services;

/**
 * Class Reset
 *
 * @property InputString typeOf
 * @package BoondManager\APIs\Customers\Filters
 */
class Reset extends AbstractFilters{
	/**
	 * @var Models\Customer
	 */
	protected $customer;

	/**#@+
	 * @var string
	 */
	const
		TYPEOF_DEMONSTRATION = 'demonstration',
		TYPEOF_TEST = 'test';
	/**#@-*/

	/**
	 * Reset constructor.
	 * @param Models\Customer $customer
	 */
	public function __construct(Models\Customer $customer) {
		parent::__construct();

		$this->customer = $customer;

		$input = new InputEnum('typeOf');
		$input->setAllowedValues( [self::TYPEOF_DEMONSTRATION, self::TYPEOF_TEST] );
		$input->setModeDefaultValue(self::TYPEOF_DEMONSTRATION, $defined = true);
		$this->addInput($input);
	}

	/**
	 *
	 */
	protected function postValidation() {
		$administratorLogin = Services\Login::get(
			Services\Customers::getResetAdministratorLogin($this->customer, $this->typeOf->getValue())
		);

		$managerLogin = Services\Login::get(
			Services\Customers::getResetFirstManagerLogin($this->customer, $this->typeOf->getValue())
		);

		if($administratorLogin->customerCode != $this->customer->code)
			$this->typeOf->invalidate(Models\Customer::ERROR_CUSTOMER_WRONG_ADMINISTRATOR_LOGIN);

		if($managerLogin->customerCode != $this->customer->code)
			$this->typeOf->invalidate(Models\Customer::ERROR_CUSTOMER_WRONG_MANAGER_LOGIN);
	}
}
