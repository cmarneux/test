<?php
/**
 * searchcustomers.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Managers;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class SearchCustomers
 * @property InputValue keywordsType
 * @property InputMultiValues perimeterSupports
 * @property InputMultiValues subscriptionStates
 * @property InputMultiValues sort
 * @package BoondManager\APIs\Customers\Filters
 */
class SearchCustomers extends AbstractSearch {
	/**#@+
	 * @var string types of keywords
	 */
	const
		KEYWORD_TYPE_NAME = 'name',
		KEYWORD_TYPE_CODE = 'code';
	/**#@- */

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_SUBSCRIPTION_STARTDATE = 'subscription.startDate',
		ORDERBY_SUBSCRIPTION_ENDDATE = 'subscription.endDate',
		ORDERBY_SUBSCRIPTION_RESOURCES_PER_MANAGER = 'subscription.numberOfResourcesMaxPerManager',
		ORDERBY_SUBSCRIPTION_MAX_RESOURCES_INTRANET = 'subscription.numberOfIntranetMax',
		ORDERBY_MAINSUPPORT_LASTNAME = 'mainSupport.lastName',
		ORDERBY_NAME = 'name',
		ORDERBY_COMPANY = 'company.id',
		ORDERBY_SUBSCRIPTION_STATE = 'subscription.state',
		ORDERBY_CREATIONDATE = 'creationDate';
	/**#@-*/

	/**
	 * SearchCustomers constructor.
	 */
	public function __construct()
	{
		parent::__construct($addPerimeter = false, $addFlags = false);

		$input = new InputEnum('keywordsType');
		$input->setAllowedValues([
			self::KEYWORD_TYPE_NAME, self::KEYWORD_TYPE_CODE
		]);
		$this->addInput($input);

		$input = new InputMultiEnum('perimeterSupports');
		$input->setAllowedValues(Managers::getAllSupportIds());
		$input->setAllowEmptyValue( true );
		$this->addInput($input);

		$input = new InputMultiEnum('subscriptionStates');
		$input->setAllowedValues([
			BM::USER_ACCESS_INACTIVE,
			BM::USER_ACCESS_DEMO,
			BM::USER_ACCESS_ACTIVE
		]);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_SUBSCRIPTION_STARTDATE,
			self::ORDERBY_SUBSCRIPTION_ENDDATE,
			self::ORDERBY_SUBSCRIPTION_RESOURCES_PER_MANAGER,
			self::ORDERBY_SUBSCRIPTION_MAX_RESOURCES_INTRANET,
			self::ORDERBY_MAINSUPPORT_LASTNAME,
			self::ORDERBY_NAME,
			self::ORDERBY_COMPANY,
			self::ORDERBY_SUBSCRIPTION_STATE,
			self::ORDERBY_CREATIONDATE
		]);
		$this->addInput($input);
	}
}
