<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers;

use BoondManager\APIs\Customers\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use Wish\Models\Model;
use BoondManager\APIs\Customers\Filters;
use BoondManager\Services\Customers;

/**
 * Class Index
 * @package BoondManager\APIs\Customers
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'name',
		'subscription' => [
			'id',
			'state',
			'startDate',
			'endDate',
			'numberOfActivableManagers',
			'numberOfIntranetMax',
			'numberOfResourcesMaxPerManager'
		],
		'company' => [
			'id'
		],
		'mainSupport' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Search projects
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$filter = new Filters\SearchCustomers();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = Customers::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
