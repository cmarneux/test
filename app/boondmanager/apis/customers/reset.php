<?php
/**
 * reset.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Customers;
use BoondManager\APIs\Customers\Specifications\HaveResetAccess;
use BoondManager\APIs\Customers\Filters;

/**
 * Class Reset
 * @package BoondManager\APIs\Customers
 */
class Reset extends AbstractController {
	/**
	 * Reset customer's database
	 */
	public function api_post() {
		$customer = Customers::get($this->requestAccess->id);
		if(!$customer) $this->error(404);

		$this->checkAccessWithSpec(new HaveResetAccess(), $customer);
		//Construction des filtres
		$filter = new Filters\Reset($customer);
		$filter->setData($this->requestAccess->getParams());
		$this->checkFilter($filter);

		if(!Customers::reset($customer, $filter->typeOf->getValue()))
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
