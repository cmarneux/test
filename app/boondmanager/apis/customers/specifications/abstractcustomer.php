<?php
/**
 * abstractcustomer.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers\Specifications;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Customer;
use BoondManager\Lib\RequestAccess;

abstract class AbstractCustomer extends AbstractSpecificationItem {
	/**
	 * Get the customer from the request
	 * @param RequestAccess $request
	 * @return Customer|null
	 */
	public function getCustomer($request) {
		if($request->data instanceof Customer) return $request->data;
		else return null;
	}

	/**
	 * Is user customer's main support ?
	 * @param CurrentUser $user
	 * @param Customer $customer
	 * @return bool
	 */
	protected function isMainSupport($user, $customer) {
		return $customer->mainSupport->id == $user->getUserId();
	}

	/**
	 * Is customer's subscription not active
	 * @param Customer $customer
	 * @return bool
	 */
	protected function isSubscriptionTypeNotActive($customer) {
		return $customer->subscription->state != BM::USER_ACCESS_ACTIVE;
	}

	/**
	 * Get read & write access
	 * @param CurrentUser $user
	 * @param Customer $customer
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($user, $customer) {
		list($read, $write) = [false, false];

		return [$read, $write];
	}
}
