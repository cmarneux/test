<?php
/**
 * haveresetaccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveResetAccess
 * @package BoondManager\APIs\Customers\Specifications
 */
class HaveCreateFreeTrialAccess extends AbstractCustomer{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		//TODO : Il faut vérifier que le client n'existe pas déjà et que le login n'est pas utilisé ==> A METTRE DANS LES FILTRE
		/*if(!self::isCustomerExists($customer->code) && !Login::isLoginExists($customer->administrator->login) && !Login::isLoginExists($customer->email) && ($customer->id = self::create($customer))) {

		}*/
		return true;
	}
}
