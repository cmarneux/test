<?php
/**
 * haveresetaccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveResetAccess
 * @package BoondManager\APIs\Customers\Specifications
 */
class HaveResetAccess extends AbstractCustomer{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		$customer = $this->getCustomer($request);

		return ($user->isRoot() || $user->isSupport() && $this->isMainSupport($user, $customer->mainSupport)) &&  $this->isSubscriptionTypeNotActive($customer) && (BM::isDevelopmentMode() || BM::isModeDemo());
	}
}
