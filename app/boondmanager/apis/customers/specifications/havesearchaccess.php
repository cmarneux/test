<?php
/**
 * havesearchaccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Customers\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveSearchAccess
 * @package BoondManager\APIs\Customers\Specifications
 */
class HaveSearchAccess extends AbstractCustomer{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		return $user->isRoot() || $user->isSupport();
	}
}
