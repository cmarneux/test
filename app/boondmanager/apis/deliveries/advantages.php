<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries;

use BoondManager\APIs\Advantages\Filters\SearchAdvantages;
use BoondManager\APIs\Deliveries\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\AdvantageType;
use BoondManager\Models\Employee;
use BoondManager\Services;

class Advantages extends AbstractController{
	const ALLOWED_FIELDS = [
		'id',
		'date',
		'advantageType' => [
			'reference',
			'name'
		],
		'quantity',
		'costPaid',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'agency' => [
			'id',
			'name'
		],
		'resource' => [
			'id',
			'lastName',
			'firstName'
		],
	];

	public function api_get()
	{
		$delivery = Services\Deliveries::get($this->requestAccess->id);
		if (!$delivery) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(), $delivery);

		if (!$delivery->dependsOn instanceof Employee && !$delivery->project->isInactivity())
			$this->error(404);

		$filter = new SearchAdvantages();
		$filter->setIndifferentPerimeter();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($delivery->getReference());

		$this->checkFilter($filter);

		$result = Services\Advantages::search($filter);

		$result->filterFields(self::ALLOWED_FIELDS);

		$types = array_map(function (AdvantageType $value) {
			return [
				'id'    => $value->id,
				'value' => $value->name
			];
		}, $delivery->dependsOn->agency->advantageTypes);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				],
				'advantagesTypes' => $types
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
