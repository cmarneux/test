<?php
/**
 * entity.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries;

use BoondManager\APIs\Deliveries\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Deliveries\Specifications\HaveReadAccess;
use BoondManager\APIs\Deliveries\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Services\BM;

class Entity extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'title',
		'state',
		'averageDailyPriceExcludingTax',
		'averageDailyCost',
		'averageDailyContractCost',
		'numberOfDaysInvoicedOrQuantity',
		'numberOfDaysFree',
		'informationComments',
		'conditions',
		'turnoverSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'profitabilitySimulated',
		'occupationRate',
		'dailyExpenses',
		'monthlyExpenses',
		'numberOfWorkingDays',
		'weeklyWorkingHours',
		'averageHourlyPriceExcludingTax',
		'forceAverageHourlyPriceExcludingTax',
		'additionalTurnoverAndCosts' => [
			'id',
			'date',
			'state',
			'title',
			'turnoverExcludingTax',
			'costsExcludingTax',
			'purchase' => [
				'id',
				'subscription'
			],
		],
		'expensesDetails' => [
			'id',
			'expenseType' => [
				'reference',
				'name'
			],
			'periodicity',
			'netAmount',
			'agency' => [
				'id',
				'name',
				'expenseTypes' => [
					'reference',
					'name',
					'taxRate'
				]
			]
		],
		'advantages' => [
			'reference',
			'name',
			'frequency',
			'category',
			'participationQuota',
			'agencyQuota',
			'employeeQuota',
			'agency' => [
				'id',
				'advantageTypes' => [
					'reference',
					'name',
					'frequency',
					'category',
					'participationQuota',
					'agencyQuota',
					'employeeQuota'
				]
			]
		],
		'exceptionalScales' => [
			'reference',
			'name',
			'exceptionalRules' => [
				'reference',
				'name',
				'priceExcludingTaxOrPriceRate',
				'grossCostOrSalaryRate'
			],
			'dependsOn' => [
				'id',
				'type',
				'exceptionalScaleTypes' => [
					'reference',
					'name',
					'exceptionalRules' => [
						'reference',
						'name',
						'priceExcludingTaxOrPriceRate',
						'grossCostOrSalaryRate'
					]
				]
			]
		],
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency',
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			],
			'opportunity' => [
				'id',
				'reference',
				'title'
			],
			'agency' => [
				'id',
				'name',
				'exchangeRate',
				'currency',
				'allowAdvantagesOnProjects',
				'allowExceptionalScalesOnProjects'
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name',
				'exceptionalScaleTypes' => [
					'reference',
					'name',
					'exceptionalRules' => [
						'reference',
						'name',
						'priceExcludingTaxOrPriceRate',
						'grossCostOrSalaryRate'
					]
				]
			],
			'technical' => [
				'id',
				'lastName',
				'firstName',
				'company' => [
					'id',
					'name'
				]
			],
			'deliveries' => [
				'id',
				'title',
				'dependsOn' => [
					'id',
					'lastName',
					'firstName',
					'typeOf'
				]
			],
		],
		'dependsOn' => [
			'id',
			// resource
			'firstName',
			'lastName',
			'typeOf',
			'agency' => [
				'id',
				'name',
				'calendar',
				'expenseTypes' => [
					'reference',
					'name',
					'taxRate'
				],
				'advantageTypes' => [
					'reference',
					'name',
					'frequency',
					'category',
					'participationQuota',
					'agencyQuota',
					'employeeQuota'
				],
				'exceptionalScaleTypes' => [
					'reference',
					'name',
					'exceptionalRules' => [
						'reference',
						'name',
						'priceExcludingTaxOrPriceRate',
						'grossCostOrSalaryRate'
					]
				]
			],
			// product
			'name',
		],
		'contract' => [
			'id',
			'contractAverageDailyCost',
			'monthlySalary',
			'dailyExpenses',
			'monthlyExpenses',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency'
		],
		'purchase' => [
			'id',
			'reference',
			'title'
		],
		'master' => [
			'id',
			'title',
			'startDate',
			'endDate',
			'dependsOn' => [
				'id',
				'lastName',
				'firstName',
				'typeOf'
			]
		],
		'slave' => [
			'id',
			'title',
			'startDate',
			'endDate',
			'dependsOn' => [
				'id',
				'lastName',
				'firstName',
				'typeOf'
			]
		],
		'contracts' => [
			'id',
			'contractAverageDailyCost',
			'monthlySalary',
			'dailyExpenses',
			'monthlyExpenses',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency'
		],
		'files' => [
			'id',
			'name'
		],
		'groupment' => [
			'id'
		]
	];

	public function api_get() {
		$delivery = Services\Deliveries::get($this->requestAccess->id);
		if(!$delivery) $this->error(403);

		$this->checkAccessWithSpec( new HaveReadAccess(), $delivery);

		$outputData = [
			'data' => $delivery->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($outputData);
	}

	public function api_put() {

		$delivery = Services\Deliveries::get($this->requestAccess->id);
		if(!$delivery) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $delivery);

		//Build filters
		$filter = new Filters\Entity($delivery);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Services\Deliveries::buildFromFilter($filter, $delivery);

		if(Services\Deliveries::update($delivery) ) {
			$this->sendJSONResponse([
				'data' => $delivery->filterFields(self::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}

	public function api_delete() {
		$delivery = Services\Deliveries::get($this->requestAccess->id);
		if(!$delivery) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $delivery); // TODO

		$this->sendJSONResponse([
			'data' => [
				'success' => Services\Deliveries::delete($delivery->id)
			]
		]);
	}
}
