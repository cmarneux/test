<?php
/**
 * entity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries\Filters;

use BoondManager\APIs\Deliveries\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\Filters\Inputs\Relationships\Project;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Agency;
use BoondManager\Models\Company;
use BoondManager\Models\Contract;
use BoondManager\Models\Delivery;
use BoondManager\Models\DeliveryDetail;
use BoondManager\Models\Employee;
use BoondManager\Models\ExpenseDetail;
use BoondManager\Models\Product;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Services;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Entity
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputString title
 * @property InputValue state
 * @property InputFloat averageDailyPriceExcludingTax
 * @property InputFloat averageDailyCost
 * @property InputFloat averageDailyContractCost
 * @property InputFloat numberOfDaysInvoicedOrQuantity
 * @property InputFloat numberOfDaysFree
 * @property InputInt numberOfWorkingDays
 * @property InputString informationComments
 * @property InputString conditions
 * @property InputFloat dailyExpenses
 * @property InputFloat monthlyExpenses
 * @property InputFloat weeklyWorkingHours
 * @property InputFloat averageHourlyPriceExcludingTax
 * @property InputBoolean forceAverageHourlyPriceExcludingTax
 * @property InputMultiObjects additionalTurnoverAndCosts
 * @property InputMultiObjects expensesDetails
 * @property InputMultiObjects advantages
 * @property InputMultiObjects exceptionalscales
 * @property InputBoolean forceTransferCreation
 * @property Relationships\Contract contract
 * @property Relationships\Delivery slave
 * @property InputRelationship dependsOn
 * @property Relationships\Project project
 *
 * @method Contract|null getContract
 *
 * @package BoondManager\APIs\Deliveries\Filters
 */
class Entity extends AbstractJsonAPI {

	protected $_objectClass = Delivery::class;

	/**
	 * @var Delivery
	 */
	protected $_delivery;

	/**
	 * @var Agency
	 */
	protected $_agency;

	/**
	 * Entity constructor.
	 * @param Delivery|null $delivery
	 */
	public function __construct(Delivery $delivery = null)
	{
		parent::__construct();

		$this->_delivery = $delivery;

		$this->addInput( (new InputDate('startDate')));
		$this->addInput( (new InputDate('endDate')));

		$input = new InputString('title');
		$input->setMaxLength(100);
		$this->addInput($input);

		$this->addInput( new InputDict('state', 'specific.setting.state.delivery') );

		$this->addInput( new InputFloat('averageDailyPriceExcludingTax') );
		$this->addInput( new InputFloat('averageDailyCost') );
		$this->addInput( new InputFloat('averageDailyContractCost') );
		$this->addInput( new InputFloat('numberOfDaysInvoicedOrQuantity') );
		$this->addInput( new InputFloat('numberOfDaysFree') );

		$input = new InputInt('numberOfWorkingDays');
		$input->setMin(0)->setMax(365);
		$this->addInput( $input );

		$input = new InputString('informationComments');
		$input->setMaxLength(4000);
		$this->addInput($input);

		$input = new InputString('conditions');
		$input->setMaxLength(4000);
		$this->addInput($input);

		$this->addInput(new InputFloat('dailyExpenses'));
		$this->addInput(new InputFloat('monthlyExpenses'));
		$this->addInput((new InputFloat('weeklyWorkingHours', 35))->setMin(0)->setMax(168));
		$this->addInput(new InputFloat('averageHourlyPriceExcludingTax'));
		$this->addInput(new InputBoolean('forceAverageHourlyPriceExcludingTax'));

		// en cas d'edition, on n'edite qu'un cout additionnel appartenant deja a la prestation
		// todo2 : changer la syntaxe et appliquer le changement sur projet
		$template = new Attributes\AdditionalTurnoverAndCosts();
		if($this->isEdition()) $template->setAllowedTurnoverAndCosts($delivery->additionalTurnoverAndCosts);
		$input = new InputMultiObjects('additionalTurnoverAndCosts', $template );
		$this->addInput($input);

		$template = new Attributes\ExpensesDetails();
		$template->setAllowedDetails($delivery ? $delivery->expensesDetails : []);
		$template->agency->addFilterCallback(function($value){
			return new Agency(['id' => $value]);
		});
		$input = new InputMultiObjects('expensesDetails', $template);
		$this->addInput($input);

		$template = new Attributes\Advantages();
		$template->agency->addFilterCallback(function ($value) {
			return new Agency(['id' => $value]);
		});
		$input = new InputMultiObjects('advantages', $template );
		$this->addInput($input);

		$input = new InputMultiObjects('exceptionalscales', (new Attributes\ExceptionalScale()) );
		$this->addInput($input);

		$this->addInput( new InputBoolean('forceTransferCreation') );

		$input = new Relationships\Contract();
		$input->setAllowEmptyValue(true);
		$this->addInput( $input );

		$input = new Relationships\Delivery('slave');
		$input->setAllowEmptyValue(true);
		$this->addInput($input);

		$input = new InputRelationship('dependsOn');
		$input->setRequired(true);
		$input->addFilterCallback(function($id){
			/** @var InputRelationship $this */
			switch ($this->getType()) {
				case Delivery::TYPE_RESOURCE: return Services\Employees::find($id);
				case Delivery::TYPE_PRODUCT: return Services\Products::find($id);
				default: return false;
			}
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);

		$this->addInput($input);

		$input = new Project();
		$input->setRequired(true);
		$this->addInput($input);

		if($this->isCreation())
			$this->adaptForCreation();
		else
			$this->adaptForEdition();
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return !$this->_delivery || !$this->_delivery->id;
	}

	private function isEdition() {
		return ! $this->isCreation();
	}

	/**
	 * Set inputs for an update
	 */
	private function adaptForEdition() {
		if($this->project->isDefined())
			$this->project->invalidateIfDebug(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);

		if($this->dependsOn->isDefined())
			$this->dependsOn->invalidateIfDebug(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);

		$this->startDate->setModeDefaultValue( $this->_delivery->startDate, $defined = true );
		$this->endDate->setModeDefaultValue( $this->_delivery->endDate , $defined = true);
	}

	private function adaptForCreation() {

		$this->startDate->markDefaultValueAsDefined(true);
		$this->endDate->markDefaultValueAsDefined(true);

		$this->numberOfWorkingDays->setModeDefaultValue(Services\CurrentUser::instance()->getNumberOfWorkingDays(), $defined = true);

		$this->weeklyWorkingHours->setModeDefaultValue(35, $defined = true);

		$this->state->setModeDefaultValue(Delivery::STATE_SIGNED);

		$this->dependsOn->addFilterCallback(function($entity){
			if($entity instanceof Employee) {
				$entity->agency = Services\Agencies::get($entity->agency->id, Agency::TAB_ACTIVITYEXPENSES);
			}
			return $entity;
		});
	}

	/**
	 * @return \BoondManager\Models\Project|false
	 */
	public function getProject(){
		return $this->isCreation() ? $this->project->getValue() : $this->_delivery->project;
	}

	public function getDependsOn() {
		return $this->isCreation() ? $this->dependsOn->getValue() : $this->_delivery->dependsOn;
	}

	/**
	 * @return Agency
	 */
	public function getEmployeeAgency(){
		return $this->getDependsOn()->agency;
	}

	/**
	 * @return Company
	 */
	public function getProjectCompany(){
		return $this->getProject()->company;
	}

	/**
	 * @return Delivery|null
	 */
	public function getSlave(){
		return $this->isCreation() ? $this->slave->getValue() : $this->_delivery->slave;
	}

	/**
	 * @return bool
	 */
	public function isSlave(){
		return $this->isCreation() ? false : $this->_delivery->master != null;
	}

	public function belongsToGroupment() {
		return $this->isCreation() ? false : $this->_delivery->groupment != null;
	}

	public function isMaster(){
		return $this->forceTransferCreation->isDefined() && $this->forceTransferCreation->getValue() || $this->getSlave();
	}

	/**
	 * @param Agency $agency
	 * @return $this
	 */
	public function setAgency(Agency $agency){
		$this->_agency = $agency;
		return $this;
	}

	protected function postValidation() {

		$request = new RequestAccess();
		$request->setUser( Services\CurrentUser::instance() );
		$request->setData( $this->toObject() ); // useless, but just to make things right

		foreach(HaveWriteAccessOnField::FIELD_LIST as $field) {
			if($this->$field && $this->$field->isDefined() && !(new HaveWriteAccessOnField($field))->isSatisfiedBy($request)) {
				$this->$field->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);
			}
			if($field=='slave' && $this->forceTransferCreation->isDefined()) {
				$this->forceTransferCreation->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);
			}
		}

		if($this->isEdition() && $this->numberOfDaysInvoicedOrQuantity->isDefined() && ($this->_delivery->master || $this->_delivery->groupment))
			$this->numberOfDaysInvoicedOrQuantity->invalidateIfDebug(Delivery::ERROR_DELIVERY_NUMBEROFDAYSINVOICEDORQUANTITY_CANNOT_BE_SET);

		if($this->isEdition() && $this->numberOfDaysFree->isDefined() && ($this->_delivery->master))
			$this->numberOfDaysFree->invalidateIfDebug(Delivery::ERROR_DELIVERY_NUMBEROFDAYSFREE_CANNOT_BE_SET);

		if($this->isEdition() && $this->_delivery->groupment)
			$this->title->invalidateIfDebug(Delivery::ERROR_DELIVERY_TITLE_CANNOT_BE_SET);

		if($this->isSlave()) {
			if($this->startDate->isDefined())
				$this->startDate->invalidateIfDebug(Delivery::ERROR_DELIVERY_STARTDATE_CANNOT_BE_SET);

			if($this->endDate->isDefined())
				$this->endDate->invalidateIfDebug(Delivery::ERROR_DELIVERY_ENDDATE_CANNOT_BE_SET);

			if($this->state->isDefined())
				$this->state->invalidateIfDebug(Delivery::ERROR_DELIVERY_STATE_CANNOT_BE_SET);
		}

		if($this->belongsToGroupment()) {
			if( $this->title->isDefined())
				$this->title->invalidateIfDebug(Delivery::ERROR_DELIVERY_TITLE_CANNOT_BE_SET);
		}

		if($this->isSlave() || $this->belongsToGroupment()){
			if($this->numberOfDaysInvoicedOrQuantity->isDefined())
				$this->numberOfDaysInvoicedOrQuantity->invalidateIfDebug(Delivery::ERROR_DELIVERY_NUMBEROFDAYSINVOICEDORQUANTITY_CANNOT_BE_SET);
		}

		if($this->isSlave() || $this->belongsToGroupment() || $this->getDependsOn() instanceof Product) {
			if ($this->numberOfDaysFree->isDefined())
				$this->numberOfDaysFree->invalidateIfDebug(Delivery::ERROR_DELIVERY_NUMBEROFDAYSFREE_CANNOT_BE_SET);
		}

		if($this->averageDailyContractCost->isDefined() && $this->isMaster() || $this->getContract()) {
			$this->averageDailyContractCost->invalidateIfDebug(Delivery::ERROR_DELIVERY_AVERAGEDAILYCONTRACTCOST_CANNOT_BE_SET);
		}

		if($this->startDate->getTimestamp() > $this->endDate->getTimestamp()) {
			$this->startDate->invalidateIfDebug(Delivery::ERROR_DELIVERY_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE);
			$this->endDate->invalidateIfDebug(Delivery::ERROR_DELIVERY_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE);
		}

		if($this->slave->isDefined() && $this->forceTransferCreation->isDefined() && $this->forceTransferCreation->getValue()) {
			$this->slave->invalidateIfDebug(Delivery::ERROR_DELIVERY_SLAVE_CANNOT_BE_SET_IF_FORCETRANSFER);
			$this->forceTransferCreation->invalidateIfDebug(Delivery::ERROR_DELIVERY_SLAVE_CANNOT_BE_SET_IF_FORCETRANSFER);
		}

		$additionalTurnoverAndCosts = $this->_delivery->additionalTurnoverAndCosts ? $this->_delivery->additionalTurnoverAndCosts : [];

		foreach($this->additionalTurnoverAndCosts->getItems() as $addData) {
			/** @var Attributes\AdditionalTurnoverAndCosts $addData */
			foreach($additionalTurnoverAndCosts as $prjAddData) {
				/** @var DeliveryDetail $prjAddData */
				if ($addData->id->isDefined() && $addData->id->getValue() == $prjAddData->id && $prjAddData->purchase)
					$addData->id->invalidateIfDebug(Delivery::ERROR_DELIVERY_CANNOT_UPDATE_TURNOVERANDCOSTS);
			}
		}

		if($this->slave->isDefined()){
			if($this->slave->getValue()->project->id == $this->getProject()->id || $this->slave->getValue()->master || $this->slave->getValue()->slave) {
				$this->slave->invalidateIfDebug(Delivery::ERROR_DELIVERY_SLAVE_IS_INCORRECT);
			}
		}

		if($this->slave->isDefined()){
			if($this->isSlave() || $this->isMaster())
				$this->slave->invalidateIfDebug(Delivery::ERROR_DELIVERY_SLAVE_CANNOT_BE_SET);
		}

		if($this->forceTransferCreation->isDefined()){
			if($this->isSlave() || $this->isMaster())
				$this->forceTransferCreation->invalidateIfDebug(Delivery::ERROR_DELIVERY_FORCETRANSFER_CANNOT_BE_SET);
		}

		if($this->getProject()->mode == BM::PROJECT_TYPE_PRODUCT) {
			$this->postValidationProduct();
		}else{
			$this->postValidationEmployee();
		}
	}

	private function postValidationEmployee()
	{
		$employeeAgency = $this->getEmployeeAgency();
		$projectCompany = $this->getProjectCompany();

		/** @var Agency[] $expensesDetailsAgencies list of valid agencies for expensesDetails */
		$expensesDetailsAgencies                        = [];
		$expensesDetailsAgencies[ $employeeAgency->id ] = $employeeAgency;

		/** @var Agency[] $advantagesAgencies list of valid agencies for advantages */
		$advantagesAgencies                        = [];
		$advantagesAgencies[ $employeeAgency->id ] = $employeeAgency;

		/** @var Agency[] $exceptionalScalesAgencies list of valid agencies for exceptionalScales */
		$exceptionalScalesAgencies                        = [];
		$exceptionalScalesAgencies[ $employeeAgency->id ] = $employeeAgency;

		/** @var Company[] $exceptionalScalesCompanies list of valid companies for exceptionalScales */
		$exceptionalScalesCompanies                        = [];
		$exceptionalScalesCompanies[ $projectCompany->id ] = $projectCompany;

		if ($this->isEdition()) {
			foreach ($this->_delivery->advantages as $advantage) {
				$advantagesAgencies[ $advantage->agency->id ] = $advantage->agency;
			}
			foreach ($this->_delivery->expensesDetails as $expenseDetail) {
				$expensesDetailsAgencies[ $expenseDetail->agency->id ] = $expenseDetail->agency;
			}
			foreach ($this->_delivery->exceptionalScales as $exceptionalScale) {
				if ($exceptionalScale->dependsOn instanceof Agency)
					$exceptionalScalesAgencies[ $exceptionalScale->dependsOn->id ] = $exceptionalScale->dependsOn;
				else if ($exceptionalScale->dependsOn instanceof Company)
					$exceptionalScalesCompanies[ $exceptionalScale->dependsOn->id ] = $exceptionalScale->dependsOn;
			}
		}

		foreach ($this->advantages->getItems() as $advantageInput) {
			/** @var Attributes\Advantages $advantageInput */

			$found = false;
			if (array_key_exists($advantageInput->agency->getValue()->id, $advantagesAgencies)) {
				$agency = $advantagesAgencies[ $advantageInput->agency->getValue()->id ];
				foreach ($agency->advantageTypes as $at) {
					if ($at->reference == $advantageInput->reference) {
						$found = true;
						break;
					}
				}
			}
			if (!$found)
				// the reference doesn't belong to a valid agency
				$advantageInput->reference->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}

		foreach ($this->expensesDetails->getItems() as $expenseInput) {
			/** @var Attributes\ExpensesDetails $expenseInput */

			$found = false;
			if (array_key_exists($expenseInput->agency->getValue()->id, $expensesDetailsAgencies)) {
				$agency = $expensesDetailsAgencies[ $expenseInput->agency->getValue()->id ];
				foreach ($agency->expenseTypes as $et) {
					if ($et->reference == $expenseInput->expenseType->reference->getValue()) {
						$found = true;
						break;
					}
				}
			}
			if (!$found)
				// the reference doesn't belong to a valid agency
				$expenseInput->expenseType->reference->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}

		foreach ($this->exceptionalscales->getItems() as $exceptionalScaleInput) {
			/** @var Attributes\ExceptionalScale $exceptionalScaleInput */

			$found = false;
			if ($exceptionalScaleInput->dependsOn->getValue() instanceof Agency && array_key_exists($exceptionalScaleInput->dependsOn->getValue()->id, $exceptionalScalesAgencies)) {
				$agency = $exceptionalScalesAgencies[ $exceptionalScaleInput->dependsOn->getValue()->id ];
				foreach ($agency->exceptionalScaleTypes as $est) {
					if ($est->reference == $exceptionalScaleInput->reference) {
						$found = true;
						break;
					}
				}
			}
			if ($exceptionalScaleInput->dependsOn->getValue() instanceof Company && array_key_exists($exceptionalScaleInput->dependsOn->getValue()->id, $exceptionalScalesCompanies)) {
				$agency = $exceptionalScalesCompanies[ $exceptionalScaleInput->dependsOn->getValue()->id ];
				foreach ($agency->exceptionalScaleTypes as $est) {
					if ($est->reference == $exceptionalScaleInput->reference) {
						$found = true;
						break;
					}
				}
			}
			if (!$found)
				// the reference doesn't belong to a valid agency
				$exceptionalScaleInput->reference->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}
	}

	/**
	 * magic method to retrieve a field value or the value from the object
	 * @param string $name
	 * @param array $arguments
	 * @throws \Exception
	 */
	public function __call($name, array $arguments) {
		if(strlen($name) > 3 && substr($name, 0, 3) == 'get'){
			$this->getFieldIfDefined( lcfirst(substr($name, 3)) );
		} else {
			throw new \Exception("undefined method '$name'");
		}
	}

	private function getFieldIfDefined($field) {
		return $this->$field->isDefined() ? $this->$field->getValue() : $this->_delivery->$field;
	}

	private function postValidationProduct(){
		if($this->numberOfWorkingDays->isDefined())
			$this->numberOfWorkingDays->invalidateIfDebug(Delivery::ERROR_DELIVERY_NUMBEROFWORKINGDAYS_CANNOT_BE_SET);

		if($this->dailyExpenses->isDefined())
			$this->dailyExpenses->invalidateIfDebug(Delivery::ERROR_DELIVERY_DAILYEXPENSES_CANNOT_BE_SET);

		if($this->monthlyExpenses->isDefined())
			$this->monthlyExpenses->invalidateIfDebug(Delivery::ERROR_DELIVERY_MONTHLYEXPENSES_CANNOT_BE_SET);

		if($this->weeklyWorkingHours->isDefined())
			$this->weeklyWorkingHours->invalidateIfDebug(Delivery::ERROR_DELIVERY_WEKKLYWORKINGHOURS_CANNOT_BE_SET);

		if($this->averageHourlyPriceExcludingTax->isDefined())
			$this->averageHourlyPriceExcludingTax->invalidateIfDebug(Delivery::ERROR_DELIVERY_AVERAGEHOURLYPRICEEXCLUDINGTAX_CANNOT_BE_SET);

		if($this->forceAverageHourlyPriceExcludingTax->isDefined())
			$this->forceAverageHourlyPriceExcludingTax->invalidateIfDebug(Delivery::ERROR_DELIVERY_FORCEAVERAGEHOURLYPRICEEXCLUDINGTAX_CANNOT_BE_SET);

		if($this->exceptionalscales->isDefined())
			$this->exceptionalscales->invalidateIfDebug(Delivery::ERROR_DELIVERY_EXCEPTIONALSCALES_CANNOT_BE_SET);

		if($this->advantages->isDefined())
			$this->advantages->invalidateIfDebug(Delivery::ERROR_DELIVERY_ADVANTAGES_CANNOT_BE_SET);

		if($this->expensesDetails->isDefined())
			$this->expensesDetails->invalidateIfDebug(Delivery::ERROR_DELIVERY_EXPENSESDETAILS_CANNOT_BE_SET);
	}
}
