<?php
/**
 * deliveries.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Deliveries;

use BoondManager\APIs\Deliveries\Specifications\HaveCreateAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Deliveries\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\Deliveries
 */
class Index extends AbstractController {

	/**
	 * Create a delivery
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default project
		$delivery = Services\Deliveries::buildFromFilter($filter);
		if(!$delivery) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $delivery);

		if( Services\Deliveries::create($delivery) ) {
			$this->sendJSONResponse([
				'data' => $delivery->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
