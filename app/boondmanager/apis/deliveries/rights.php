<?php
/**
 * rights.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Deliveries;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Deliveries;

/**
 * Class Rights
 * @package BoondManager\APIs\Deliveries
 */
class Rights extends AbstractController {
	/**
	 * Get delivery's rights
	 */
	public function api_get() {
		$deliveries = Deliveries::get($this->requestAccess->id);
		if(!$deliveries) $this->error(404);

		$this->sendJSONResponse([
			'data' => Deliveries::getRights($deliveries)
		]);
	}
}
