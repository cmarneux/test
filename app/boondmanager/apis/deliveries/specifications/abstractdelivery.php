<?php
/**
 * abstractdelivery.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Deliveries\Specifications;

use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Models\Delivery;
use BoondManager\Lib\RequestAccess;

abstract class AbstractDelivery extends AbstractSpecificationItem {
	/**
	 * Get the project from the request
	 * @param RequestAccess $request
	 * @return Delivery|null
	 */
	public function getDelivery($request) {
		if($request->data instanceof Delivery) return $request->data;
		else return null;
	}

	/**
	 * Get read & write access
	 * @param string $tab
	 * @param CurrentUser $user
	 * @param Delivery $delivery
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($tab, $user, $delivery) {
		list($state, $read, $write) = [false, false, false];

		return [$read, $write];
	}
}
