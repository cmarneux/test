<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveCreateAccess extends AbstractDelivery{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();
		$delivery = $this->getDelivery($object);

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$req = clone $object;
		$req->setData($delivery->project);

		$projectCreation = (new \BoondManager\APIs\Projects\Specifications\HaveCreateAccess())->isSatisfiedBy($req);

		return $projectCreation && $delivery->project->mode != BM::PROJECT_TYPE_PRODUCT;
	}
}
