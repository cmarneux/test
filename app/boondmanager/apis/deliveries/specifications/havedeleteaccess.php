<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveDeleteAccess extends AbstractDelivery{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$delivery = $this->getDelivery($object);

		if($delivery->isGroupment) {
			return (new HaveWriteAccess())->isSatisfiedBy($object) && !$delivery->project->isInactivity();
		} else {
			(new HaveWriteAccess())->isSatisfiedBy($object);
		}
	}
}
