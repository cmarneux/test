<?php
/**
 * havereadaccess.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Employee;
use BoondManager\Services\BM;

class HaveReadAccess extends AbstractDelivery{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$delivery = $this->getDelivery($object);
		$user = $object->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		if(!$delivery->project->isInactivity())
			return ($delivery->project->mode != BM::PROJECT_TYPE_RECRUITMENT) && (in_array($user->checkHierarchyAccess($delivery->project, BM::MODULE_PROJECTS), [BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY, BM::PROFIL_ACCESS_READ_WRITE]));

		if(!$user->hasAccess(BM::MODULE_RESOURCES)) return false;

		if($delivery->dependsOn instanceof Employee) {
			$myProfil = $delivery->dependsOn->id == $user->getEmployeeId();
			if($delivery->dependsOn->visibility || $myProfil || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) ) {
				switch ($user->checkHierarchyAccess($delivery->dependsOn, BM::MODULE_RESOURCES) ){
					case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY: return true;
					case BM::PROFIL_ACCESS_READ_WRITE:
					case BM::PROFIL_ACCESS_READ_ONLY:
						return (
							$myProfil && !$delivery->dependsOn->mainManager
							|| (
								$user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)
								&& (
									$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
									|| !$delivery->dependsOn->isManager()
									|| $user->isMyManager($delivery->dependsOn->id)
								)
							)
						);
				}
			}
		}

		return false;
	}
}
