<?php
/**
 * userhavewriteaccessonfield.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Deliveries\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractDelivery {

	const FIELD_LIST = [
		'numberOfWorkingDays', 'exceptionalScales', 'expensesDetails', 'averayDailyContractCost', 'contract', 'advantages',
		'weeklyWorkingHours', 'forceAverageHourlyPriceExcludingTax', 'dailyExpenses', 'monthlyExpenses',
		'averageDailyCost', 'slave'
	];

	private $field;

	public function __construct($field) {
		$this->field = $field;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request)
	{
		/** @var CurrentUser $user*/
		$user = $request->user;

		if($user->isGod()) return true;

		$write = false;

		switch($this->field){
			case 'numberOfWorkingDays':
			case 'exceptionalScales':
			case 'expensesDetails':
			case 'averayDailyContractCost':
			case 'contract':
			case 'advantages':
			case 'weeklyWorkingHours':
			case 'forceAverageHourlyPriceExcludingTax':
			case 'dailyExpenses':
			case 'monthlyExpenses':
			case 'averageDailyCost':
			case 'slave':
				$write = $user->hasRight(BM::RIGHT_ACCESS_ADMINISTRATIVE, BM::MODULE_PROJECTS);
				break;
		}

		return $write;
	}
}
