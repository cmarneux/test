<?php
/**
 * searchgroupments.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\DeliveriesInactivitiesGroupments\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;

/**
 * Class SearchDeliveries
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputBoolean sumAdditionalData
 * @property InputBoolean showInactivity
 * @property InputBoolean showGroupment
 * @property InputBoolean returnNbCorrelatedOrders
 * @property InputString transferType
 * @property InputMultiValues deliveryStates
 * @property InputMultiValues projectTypes
 * @property InputMultiValues projectStates
 * @property InputMultiValues activityAreas
 * @property InputMultiValues expertiseAreas
 * @property InputString period
 * @property InputMultiValues sort
 * @package BoondManager\APIs\Deliveries\Filters
 */
class SearchDeliveries extends AbstractSearch {
	/**
	 * @var string
	 */
	const PERIMETER_MODULE = BM::MODULE_PROJECTS;

	/**#@+
	 * @var int PERIODS
	 */
	const
		PERIOD_STARTED = 'started',
		PERIOD_STOPPED = 'stopped',
		PERIOD_RUNNING_AND_SUM = 'runningAndSum',
		PERIOD_RUNNING = 'running',
		PERIOD_PROJECT_RUNNING_AND_SUM = 'projectRunningAndSum',
		PERIOD_PROJECT_RUNNING = 'projectRunning',
		PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM = 'hasAdditionalDataOrPurchaseAndSum',
		PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE = 'hasAdditionalDataOrPurchase';
	/**#@-*/

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_ID = 'id',
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_ENDDATE = 'endDate',
		ORDERBY_PROJECT_REFERENCE = 'project.reference',
		ORDERBY_PROJECT_COMPANY_NAME = 'project.company.name',
		ORDERBY_PROJECT_MAINMANAGER_LASTNAME = 'project.mainManager.lastName',
		ORDERBY_GROUPMENT_ID = 'groupment.id';
	/**#@-*/

	/**#@+
	 * @var string transfer types
	 */
	const
		TRANSFER_MASTER = 'master',
		TRANSFER_SLAVE = 'slave',
		TRANSFER_NONE = 'none',
		TRANSFER_NOT_SLAVE = 'notSlave',
		TRANSFER_SLAVE_INFO = 'slaveInfo',
		TRANSFER_NOT_MASTER = 'notMaster';
	/**#@-*/

	/**
	 * Projects constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$input = new InputBoolean('sumAdditionalData');
		$input->setModeDefaultValue(true);
		$this->addInput($input);

		$input = new InputBoolean('showInactivity');
		$input->setModeDefaultValue(false);
		$this->addInput($input);

		$input = new InputBoolean('showGroupment');
		$input->setModeDefaultValue(false);
		$this->addInput($input);

		$input = new InputBoolean('returnNbCorrelatedOrders');
		$input->setModeDefaultValue(false);
		$this->addInput($input);

		$this->addInput(new InputMultiDict('deliveryStates', 'specific.setting.state.delivery'));
		$this->addInput(new InputMultiDict('projectStates', 'specific.setting.state.project'));
		$this->addInput(new InputMultiDict('projectTypes', 'specific.setting.typeOf.project'));
		$this->addInput(new InputMultiDict('activityAreas','specific.setting.activityArea'));
		$this->addInput(new InputMultiDict('expertiseAreas','specific.setting.expertiseArea'));

		$input = new InputMultiEnum('transferType');
		$input->setAllowedValues([
			self::TRANSFER_MASTER,
			self::TRANSFER_SLAVE,
			self::TRANSFER_NONE,
			self::TRANSFER_NOT_SLAVE,
			self::TRANSFER_SLAVE_INFO,
			self::TRANSFER_NOT_MASTER
		]);
		$this->addInput($input);

		$input = new InputEnum('period');
		$input->setAllowedValues([
			self::PERIOD_RUNNING_AND_SUM,
			self::PERIOD_RUNNING,
			self::PERIOD_PROJECT_RUNNING_AND_SUM,
			self::PERIOD_PROJECT_RUNNING,
			self::PERIOD_STARTED,
			self::PERIOD_STOPPED,
			self::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM,
			self::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE
		]);
		$this->addInput($input);

		$input = new InputDate('startDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputDate('endDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_ID,
			self::ORDERBY_STARTDATE,
			self::ORDERBY_ENDDATE,
			self::ORDERBY_PROJECT_REFERENCE,
			self::ORDERBY_PROJECT_COMPANY_NAME,
			self::ORDERBY_PROJECT_MAINMANAGER_LASTNAME,
			self::ORDERBY_GROUPMENT_ID
		]);
		$this->addInput($input);
	}

	/**
	 *
	 */
	protected function postValidation() {
		//Check if extraction is allowed
		if(!$this->extraction->isDefined() || $this->extraction->getValue() != 'csv' || !CurrentUser::instance()->hasRight(BM::RIGHT_EXTRACTION, BM::MODULE_PROJECTS))
			$this->extraction->setDisabled(true);
	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
