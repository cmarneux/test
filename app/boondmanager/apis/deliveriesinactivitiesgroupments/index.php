<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\DeliveriesInactivitiesGroupments;

use BoondManager\APIs\DeliveriesInactivitiesGroupments\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use Wish\Models\Model;
use BoondManager\Services\Extraction;

/**
 * Class Index
 * @package BoondManager\APIs\Deliveries
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'title',
		'startDate',
		'endDate',
		'turnoverSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'profitabilitySimulated',
		'isGroupment',
		'isInactivity',
		'inactivityType',
		'dependsOn' => [
			'id',
			'lastName',
			'firstName',
			'name'
		],
		'groupment' => [
			'id'
		],
		'project' => [
			'id',
			'typeOf',
			'mode',
			'reference',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			],
			'opportunity' => [
				'id',
				'title'
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name'
			]
		]
	];

	/**
	 * Search deliveries
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$filter = new Filters\SearchDeliveries();
		$filter->setAndFilterData($this->requestAccess->getParams());

		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Extraction\Deliveries('deliveries.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Services\DeliveriesInactivitiesGroupments::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverSimulatedExcludingTax' => $result->turnoverSimulatedExcludingTax,
					'marginSimulatedExcludingTax' => $result->marginSimulatedExcludingTax,
					'profitabilitySimulated' => $result->profitabilitySimulated,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
