<?php
/**
 * HaveSearchAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\DeliveriesInactivitiesGroupments\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveSearchAccess
 * @package BoondManager\APIs\Deliveries\Specifications
 */
class HaveSearchAccess extends AbstractDelivery{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// CONFORME v6
		return $user->hasAccess(BM::MODULE_PROJECTS);
	}
}
