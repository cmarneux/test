<?php
/**
 * document.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Documents;

use BoondManager\APIs;
use BoondManager\Models\Document;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\File;
use BoondManager\Models\Proof;
use BoondManager\Models\Resume;
use BoondManager\Services\BM;
use BoondManager\Services;

/**
 * Document profile controller.
 * @namespace \BoondManager\Controllers\Profiles
 */
class Entity extends AbstractController  {

	/**
	 * Get document's data.
	 */
	public function api_get() {
		// no custom error in json
		BM::setPageErrorHandler();

		$id = $this->requestAccess->get('id');
		list($id, $type) = explode('_',$id);

		switch($type) {
			case Document::SUBTYPE:$file = Services\GED::getDocument($id); break;
			case Resume::SUBTYPE: $file = Services\GED::getResume($id); break;
			case Proof::SUBTYPE: $file = Services\GED::getProof($id); break;
			case File::SUBTYPE: $file = Services\GED::getFile($id); break;
			default:
				$this->error( BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE );
				return;
		}

		if(!$file) $this->error(404);

		$file->dependsOn = Services\GED::getExtendedEntity($file->dependsOn);
		$spec = Services\GED::getReadSpec($file);
		$this->checkAccessWithSpec($spec, $file->dependsOn);

		Services\GED::download($file);
	}

	public function api_delete() {
		$id = $this->requestAccess->get('id');
		list($id, $type) = explode('_',$id);

		$done = false;

		switch($type) {
			case Document::SUBTYPE: $file = Services\GED::getDocument($id); break;
			case Resume::SUBTYPE: $file = Services\GED::getResume($id); break;
			case Proof::SUBTYPE: $file = Services\GED::getProof($id); break;
			case File::SUBTYPE: $file = Services\GED::getFile($id); break;
			default:
				$this->error( BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE );
				return;
		}

		if(!$file) $this->error(404);

		$file->dependsOn = Services\GED::getExtendedEntity($file->dependsOn);
		$spec = Services\GED::geWriteSpec($file);
		$this->checkAccessWithSpec($spec, $file->dependsOn);

		Services\GED::deleteFile($file);

		$this->sendJSONResponse([
			'data'=>[
				'success'=> $done
			]
		]);
	}
}
