<?php
/**
 * creation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Documents\Filters;

use BoondManager\Databases\Local\Company;
use BoondManager\Lib\Models\AbstractFile;
use BoondManager\Models\AbsencesReport;
use BoondManager\Models\Action;
use BoondManager\Models\Candidate;
use BoondManager\Models\Delivery;
use BoondManager\Models\Document;
use BoondManager\Models\Employee;
use BoondManager\Models\ExpensesReport;
use BoondManager\Models\File;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Order;
use BoondManager\Models\Positioning;
use BoondManager\Models\Product;
use BoondManager\Models\Project;
use BoondManager\Models\Proof;
use BoondManager\Models\Purchase;
use BoondManager\Models\Resume;
use BoondManager\Models\TimesReport;
use BoondManager\Services\GED;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputFiles;

/**
 * Class Creation
 * @property InputFiles file
 * @property InputInt parentId
 * @property InputString parentType
 * @package BoondManager\APIs\Documents\Filtes
 */
class Creation extends AbstractFilters{

	private $entity;

	public function __construct()
	{
		parent::__construct();

		$file = new InputFiles('file');
		$file->setRequired(true);
		$this->addInput($file);

		$parentId = new InputInt('parentId');
		$parentId->setRequired(true);
		$this->addInput($parentId);

		$parentType = new InputString('parentType');
		$parentType->setRequired(true);
		$parentType->addFilterInArray([
			'action', 'company', 'project', 'order', 'product', 'purchase', 'delivery', 'positioning', 'resourceResume',
			'candidateResume', 'resource', 'candidate', 'expenseReport', 'absencesReport', 'timesReport', 'payment'
		]);
		$this->addInput($parentType);
	}

	/**
	 * @return int
	 */
	public function getDocumentType(){
		return GED::getTypeFromParentType($this->parentType->getValue());
	}

	/**
	 * @return int
	 */
	public function getParentID(){
		return $this->parentId->getValue();
	}

	/**
	 * @return int
	 */
	public function getDocumentParentType(){
		return GED::getDocumentParentType($this->parentType->getValue());
	}


	public function getParentEntity() {
		if(isset($this->entity)) return $this->entity;

		$data = ['id' => $this->getParentID()];

		switch($this->parentType->getValue()){
			case Document::PARENT_TYPE_ACTION: $this->entity = new Action($data); break;
			case Document::PARENT_TYPE_COMPANY: $this->entity = new Company($data); break;
			case Document::PARENT_TYPE_PROJECT: $this->entity = new Project($data); break;
			case Document::PARENT_TYPE_ORDER: $this->entity = new Order($data); break;
			case Document::PARENT_TYPE_PRODUCT: $this->entity = new Product($data); break;
			case Document::PARENT_TYPE_PURCHASE: $this->entity = new Purchase($data); break;
			case Document::PARENT_TYPE_DELIVERY: $this->entity = new Delivery($data); break;
			case Document::PARENT_TYPE_OPPORTUNITY: $this->entity = new Opportunity($data); break;
			case Document::PARENT_TYPE_POSITIONING: $this->entity = new Positioning($data); break;
			case File::PARENT_TYPE_RESOURCE:
			case Resume::PARENT_TYPE_RESOURCE_RESUME: $this->entity = new Employee($data); break;
			case File::PARENT_TYPE_CANDIDATE:
			case Resume::PARENT_TYPE_CANDIDATE_RESUME: $this->entity = new Candidate($data); break;
			case Proof::PARENT_TYPE_EXPENSES_REPORT: $this->entity = new ExpensesReport($data); break;
			case Proof::PARENT_TYPE_ABSENCES_REPORT: $this->entity = new AbsencesReport($data); break;
			case Proof::PARENT_TYPE_TIMES_REPORT: $this->entity = new TimesReport($data); break;
		}

		return $this->entity;
	}

	/**
	 * @return int
	 */
	public function getTotalFile(){
		return $this->file->getTotalFile();
	}
}
