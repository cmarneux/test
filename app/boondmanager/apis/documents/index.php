<?php
/**
 * index.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Documents;

use BoondManager\APIs;
use BoondManager\Lib\GED;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Databases\Local\File;

class Index extends AbstractController{

	/**
	 * @var File
	 */
	private $fileBDD;

	/**
	 * @var GED
	 */
	private $ged;

	/**
	 * Add a document.
	 */
	public function api_post() {
		$this->ged = new GED();
		$this->fileBDD = new File($this->ged);

		$filter = new Filters\Creation();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		$files = [];
		foreach($filter->file->getValue() as $k=>$file) {
			$entity = Services\GED::getExtendedEntity( $filter->getParentEntity() );
			$newFile = Services\GED::create( $filter->getDocumentType(), $entity);
			$spec = Services\GED::geWriteSpec($newFile);
			$this->checkAccessWithSpec($spec, $entity);

			try {
				$files[ $k ] = Services\GED::upload($entity, $file, $filter->getDocumentType());
			} catch (\Exception $e){
				$this->error($e->getCode(), $e->getMessage());
			}
		}

		$resp = ($filter->getTotalFile() == 1) ? $files[0] : $files;

		$this->sendJSONResponse([
			'data'=> $resp
		]);
	}
}
