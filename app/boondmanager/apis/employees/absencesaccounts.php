<?php
/**
 * absencesaccounts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees;

use BoondManager\APIs\Employees\Specifications\HaveFullAccessOnAbsencesAccounts;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\AbsencesQuota;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

class AbsencesAccounts extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'period',
		'year',
		'amountAcquired',
		'amountBeingAcquired',
		'amountAcquiredUse',
		'amountAcquiredAsked',
		'amountBeingAcquiredUsed',
		'amountBeingAcquiredAsked',
		'deltaAcquiredUsed',
		'deltaAcquiredAsked',
		'deltaBeingAcquiredUsed',
		'deltaBeingAcquiredAsked',
		'deltaAskedAndUsed',
		'comments',
		'automaticDescription',
		'workUnitType' => [
			'reference',
			'name'
		],
		'agency' => [
			'id',
			'name',
			'absencesCalculationMethod'
		],
	];

	public function api_get(){

		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_ABSENCESACCOUNTS);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_ABSENCESACCOUNTS), $entity);


		//if(!isset($entity->absencesAccounts)) $entity->absencesAccounts = self::getAllQuotas($entity->id);
		Services\Employees::attachUserConfig($entity);

		$absPosée = Services\Absences::getListeAbsencesPoses($entity->id); //LISTE_ABSENCESPOSEES
		$absPrise = Services\Absences::getListeAbsencesPrises($entity->id); //LISTE_ABSENCES
		$quotas = Services\Absences::getAllQuotas($entity->id); //LISTE_QUOTAS

		$fullAccess = new HaveFullAccessOnAbsencesAccounts();
		$absencesAccounts = Services\Absences::getAllAccounts($entity, $absPosée, $absPrise, $quotas, $fullAccess->isSatisfiedBy($this->requestAccess));

		foreach($absencesAccounts as $aa) {
			/** @var AbsencesQuota $absencesAccounts */
			$aa->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'data' => $absencesAccounts
		];

		$this->sendJSONResponse($tabData);
	}
}
