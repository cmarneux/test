<?php
/**
 * absencesreports.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\AbsencesReports\Filters\SearchAbsencesReports;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\AbsencesReport;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

/**
 * Class AbsencesReports
 * @package BoondManager\Controllers\Profiles\Resources
 */
class AbsencesReports extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'state',
		'title',
		'absencesPeriods' => [
			'id',
			'startDate',
			'endDate',
			'duration',
			'title',
			'workUnitType' => [
				'reference'
			]
		]
	];

	/**
	 * Get AbsencesReports data on a resource profil
	 */
	public function api_get() {

		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_ABSENCESREPORTS);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_ABSENCESREPORTS), $profil);

		$filter = new SearchAbsencesReports();
		$filter->setData($this->requestAccess->getParams());
		$filter->disableMaxResultLimit();
		$filter->endMonth->setValue("3000-01");
		$filter->startMonth->setValue("1900-01");
		$filter->keywords->setValue($profil->getReference());

		$this->checkFilter($filter);

		$result = Services\AbsencesReports::search($filter);

		foreach($result->rows as $absencesReport) {
			/** @var AbsencesReport $absencesReport */
			$absencesReport->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
