<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Action;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\Services\BM;

/**
 * Class Actions
 * @package BoondManager\APIs\Projects
 */
class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get project's actions
	 */
	public function api_get() {
		$project = Services\Employees::get($this->requestAccess->id, Employee::TAB_ACTIONS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_ACTIONS), $project);

		$filter = new SearchActions();
		$filter->setAvailableCategories([BM::CATEGORY_RESOURCE, BM::CATEGORY_CANDIDATE]);
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		foreach($result->rows as $entity){
			/** @var Action $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
