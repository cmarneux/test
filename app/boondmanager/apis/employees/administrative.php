<?php
/**
 * administrative.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;

class Administrative extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'reference',
		'dateOfBirth',
		'placeOfBirth',
		'nationality',
		'healthCareNumber',
		'situation',
		'administrativeComments',
		//~ 'contracts',
		'contracts' => [
			'id',
			'typeOf',
			'employeeType',
			'startDate',
			'endDate',
			'monthlySalary',
			'category',
			'contractAverageDailyCost',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'agency' => [
				'id',
				'name',
				'advantageTypes' => [
					'reference',
					'name'
				]
			],
			'relatedContract' => [
				'id'
			]
		],
		//~ 'files',
		'files' => [
			'id',
			'name'
		],
		'candidate' => [
			'id',
			'lastName',
			'firstName'
		],
		'providerContact' => [
			'id',
			'lastName',
			'firstName'
		],
		'providerCompany' => [
			'id',
			'name'
		],
	];

	public function api_get() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_ADMINISTRATIVE);
		if(!$profil) $this->error(404); //objet non trouvé

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_ADMINISTRATIVE), $profil );

		$tabData = [
			'data' => $profil->filterFields(self::ALLOWED_FIELDS)
		];

		$warning = Services\Employees::getWarnings($profil, $this->requestAccess);

		if($warning) {
			$tabData['meta'] = [
				'warnings' => $warning
			];
		}

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		// chargement de la fiche
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_ADMINISTRATIVE);
		if(!$profil) $this->error(404); //objet non trouvé

		// vérification des droits
		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_ADMINISTRATIVE), $profil );

		$filter = new Filters\SaveAdministrative($profil);
		$filter->setData($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		$profil = Services\Employees::buildFromFilter($filter, $profil);

		if(Services\Employees::updateProfil($profil, Employee::TAB_ADMINISTRATIVE)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
