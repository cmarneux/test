<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\Advantages\Filters\SearchAdvantages;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Advantage;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

class Advantages extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'date',
		'quantity',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'agency' => [
			'id',
			'name'
		],
		'costPaid',
		'advantageType' => [
			'reference',
			'name'
		]
		// 'typeOf' => ['TYPEA_NAME'], //TODO
	];

	/**
	 * Proxy method for search/advantages->api_get
	 * @return mixed
	 */
	public function api_get() {
		$profil = Services\Employees::get($this->requestAccess->id);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_ADMINISTRATIVE), $profil);

		$filter = new SearchAdvantages();
		$filter->setData($this->requestAccess->getParams());
		//$filter->maxResults->setValue(500);
		$filter->keywords->setValue($profil->getReference());
		//~ $filter->advantageTypes->setValue(liste des avantages récupérés sur resources/:id/technicalData);

		$this->checkFilter($filter);

		$result = Services\Advantages::search($filter);

		foreach($result->rows as $advantage) {
			/** @var Advantage $advantage */
			$advantage->filterFields(self::ALLOWED_FIELDS);
		}

		$types = Services\Advantages::getAdvantagesTypes($profil->agency->id);
		$mTypes = [];
		foreach($types as $t){
			$mTypes[] = ['id' => $t->id, 'value' => $t->name];
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				],
				'advantagesTypes' => $mTypes
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
