<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_FLAGS);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_FLAGS), $entity);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($entity);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
