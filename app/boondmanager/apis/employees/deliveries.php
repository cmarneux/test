<?php
/**
 * deliveries.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\DeliveriesInactivitiesGroupments\Filters\SearchDeliveries;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Delivery;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

/**
 * Class Deliveries
 * @package BoondManager\Controllers\Profiles\Resources
 */
class Deliveries extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'title',
		'numberOfDaysInvoicedOrQuantity',
		'numberOfDaysFree',
		'turnoverSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'profitabilitySimulated',
		'occupationRate',
		'canReadProject',
		'canReadDelivery',
		'canWriteDelivery',
		'isInactivity',
		'inactivityType',
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'opportunity' => [
				'id',
				'title'
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name'
			]
		],
	];

	/**
	 * Get employee's deliveries
	 *
	 */
	public function api_get() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_DELIVERIES);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_DELIVERIES), $profil);

		$filter = new SearchDeliveries();
		$filter->setIndifferentPerimeter();
		$filter->narrowPerimeter->setValue(0);
		$filter->sort->setDefaultValue(SearchDeliveries::ORDERBY_ENDDATE);
		$filter->order->setDefaultValue(SearchDeliveries::ORDER_DESC);
		// TODO : vérifier que les valeurs par default suivantes sont utiles
		$filter->deliveryStates->setDefaultValue([0]);//~ TODO : 29-04-2016 : Valeur en dur, pas terrible ça ... (Signée) Voir si on ne peut pas lier les valeurs du dictionnaire à des constantes des RowObject ...
		$filter->transferType->setDefaultValue(SearchDeliveries::TRANSFER_NOT_SLAVE);
		$filter->showInactivity->setDefaultValue(true);
		$filter->sumAdditionalData->setDefaultValue(false);
		$filter->setData($this->requestAccess->getParams('sort', 'order', 'maxResults', 'page'));
		$filter->keywords->setValue($profil->getReference());

		$this->checkFilter($filter);

		/** @var \BoondManager\Models\SearchResults\Deliveries $result */
		$result = Services\DeliveriesInactivitiesGroupments::search($filter);

		Services\Projects::rebindProjectDatas($result);

		foreach($result->rows as $entity) {
			/** @var Delivery $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverSimulatedExcludingTax' => $result->turnoverSimulatedExcludingTax,
					'costsSimulatedExcludingTax' => $result->costsSimulatedExcludingTax,
					'marginSimulatedExcludingTax' => $result->marginSimulatedExcludingTax,
					'profitabilitySimulated' => $result->profitabilitySimulated,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
