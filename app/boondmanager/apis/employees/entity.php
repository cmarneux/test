<?php
/**
 * resource.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveCreateAccess;
use BoondManager\APIs\Employees\Specifications\HaveDeleteAccess;

/**
 * Resource profile controller.
 * @package Resources
 * @namespace \BoondManager\Controllers\Profiles
 */
class Entity extends AbstractController {
	/**
	 * Get information data on a resource profile
	*/
	public function api_get() {

		$ALLOWED_FIELDS = [
			'id',
			'creationDate',
			'civility',
			'typeOf',
			'mainManager' => [
				'id'
			],
			'hrManager' => [
				'id'
			],
			'agency' => [
				'id'
			],
			'pole' => [
				'id'
			],
		];

		if($id = $this->requestAccess->id) {
			// get an existing profil
			$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_DEFAULT);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			// instead of using one spec, let's check any access to a tab
			$tabs = Services\Employees::getVisibleTabs($entity);
			if(!$tabs) $this->error(403);

			$ALLOWED_FIELDS = array_merge($ALLOWED_FIELDS,[
				'lastName',
				'firstName',
				'updateDate',
			]);
		} else {
			$this->checkAccessWithSpec( new HaveCreateAccess );
			$entity = Services\Employees::getNewProfil();

			$ALLOWED_FIELDS = array_merge($ALLOWED_FIELDS,[
				'state',
				'country',
				'currency',
				'availability',
				'forceAvailability',
			]);
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_TECHNICALDATA); //using technical data tab in order to rertrieve ID_DT
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $profil );

		$deleted = Services\Employees::deleteResource($profil);

		if($deleted)
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
	}
}
