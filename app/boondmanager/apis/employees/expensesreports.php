<?php
/**
 * expensesreports.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\ExpensesReports\Filters\SearchExpensesReports;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Account;
use BoondManager\Models\ExpensesReport;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

/**
 * Class ExpensesReports
 * @package BoondManager\Controllers\Profiles\Resources
 */
class ExpensesReports extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'term',
		'state',
		'paid'
	];

	/**
	 * Get ExpensesReports data on a resource profil
	 */
	public function api_get() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_EXPENSESREPORTS);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_EXPENSESREPORTS), $profil);

		$filter = new SearchExpensesReports();
		$filter->setData($this->requestAccess->getParams());
		$filter->disableMaxResultLimit();
		$filter->endMonth->setValue(date('Y-m'));
		$start = $profil->activityExpensesStartDate;
		if(!$start || $start == Account::HIRING_DATE) $start = $profil->startContract;
		if(!$start) $start = date('Y-m-d');
		$start = explode('-', $start); $start = $start[0].'-'.$start[1];
		$filter->startMonth->setValue($start);
		$filter->keywords->setValue($profil->getReference());

		$this->checkFilter($filter);

		$result = Services\ExpensesReports::search($filter);

		foreach($result->rows as $expensesReport) {
			/** @var ExpensesReport $expensesReport */
			$expensesReport->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				],
				'missingReports' => Services\ExpensesReports::getMissingMonths($result->rows, $filter->startMonth->getValue(), $filter->endMonth->getValue())
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
