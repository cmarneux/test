<?php
/**
 * files.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\OldModels\Filters;
use BoondManager\Databases\Local\File;
use BoondManager\Models\Employee;
use BoondManager\Services\BM;
use BoondManager\Services\GED;
use BoondManager\Services\Employees;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

class Files extends AbstractController {
	public function api_get() {
		$filter = Filters\Search\Documents::getUserFilter();
		$filter->category->resetFilters();
		$filter->category->addFilterInArray([
			File::TYPE_RESOURCE_RESUME
		]);
		$filter->setData($this->requestAccess->getParams());
		$filter->parent->setValue($this->requestAccess->id);
		$this->checkFilter($filter);

		$profil = Employees::get($this->requestAccess->id);
		if(!$profil) $this->error(404);

		switch($filter->category->getValue()) {
			case File::TYPE_RESOURCE_RESUME:
				$spec = (new HaveReadAccess(Employee::TAB_INFORMATION));
				break;
			default:
				$this->error(403);
				break;
		}

		$this->checkAccessWithSpec( $spec, $profil );

		$ged = new GED();
		$result = $ged->search($filter);

		$data = [
			'meta'=>[
				'page' => $filter->page->getValue(),
				'totalRows' => $result->total
			],
			'data'=>$result->rows
		];

		$this->sendJSONResponse($data);
	}
}
