<?php
/**
 * reference.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;

/**
 * Class Reference
 * @property \Wish\Filters\Inputs\InputInt id
 * @property InputString title
 * @property InputString description
 * @package BoondManager\Models\Filters\Profiles\Resources
 */
class Reference extends AbstractFilters
{
	protected $_objectClass = \BoondManager\Models\Reference::class;

	/**
	 * Reference constructor.
	 */
	public function __construct(){
		parent::__construct();

		$id = new InputInt('id');
		$titre = new InputString('title');
		$description = new InputString('description');

		$this->addInput([$id, $titre, $description]);

	}
}
