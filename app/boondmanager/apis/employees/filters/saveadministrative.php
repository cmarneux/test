<?php
/**
 * saveadministrative.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Country;
use BoondManager\Lib\Filters\Inputs\Relationships\Company;
use BoondManager\Lib\Filters\Inputs\Relationships\Contact;
use BoondManager\Services\BM;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputRelationship;
use Wish\MySQL\Where;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Employee;

/**
 * Class Profil
 * @property \Wish\Filters\Inputs\InputString reference
 * @property InputDate dateOfBirth
 * @property \Wish\Filters\Inputs\InputString placeOfBirth
 * @property InputString nationality
 * @property InputString healthCareNumber
 * @property InputInt situation
 * @property \Wish\Filters\Inputs\InputString administrativeComments
 * @property InputRelationship providerContact
 * @property \Wish\Filters\Inputs\InputRelationship providerCompany
 * @package BoondManager\Models\Filters\Profiles\Resources
 */
class SaveAdministrative extends AbstractJsonAPI
{
	/** @var Employee  */
	private $_employee;

	/**
	 * Profil constructor.
	 * @param Employee $profil
	 * @throws \Exception
	 */
	public function __construct(Employee $profil){

		$this->_employee = $profil;

		parent::__construct();

		$ref = new InputString('reference');
		$ref->setMaxLength(250);
		$this->addInput($ref);

		$this->addInput(new InputDate('dateOfBirth'));

		$place = new InputString('placeOfBirth');
		$place->setMaxLength(100);
		$this->addInput($place);

		$this->addInput( new Country('nationality') );

		$hcn = new InputString('healthCareNumber');
		$hcn->setMaxLength(21);
		$this->addInput($hcn);

		$this->addInput( new InputDict('situation', 'specific.setting.situation') );

		$this->addInput( new InputString('administrativeComments') );

		$this->addInput( new Contact('providerContact'));
		$this->addInput( new Company('providerCompany'));

		$this->addInput([
		(new InputRelationship('providerContact'))
			->setMode( $profil->isExternalConsultant() ? (InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE):(InputRelationship::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) )
			->addFilter(FILTER_CALLBACK, function($value) use ($profil){
				return $profil->isExternalConsultant()?$value:false;
			})
			->addFilterExistsInDB('TAB_CRMCONTACT', new Where('ID_CRMCONTACT=?')),
		(new InputRelationship('providerCompany'))
			->setMode( $profil->isExternalConsultant() ? (InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE):(InputRelationship::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) )
			->addFilter(FILTER_CALLBACK, function($value) use ($profil){
				return $profil->isExternalConsultant()?$value:false;
			})
			->addFilterExistsInDB('TAB_CRMSOCIETE', new Where('ID_CRMSOCIETE=?')),
		]);
	}

	public function postValidation() {
		if(!$this->_employee->isExternalConsultant()) {
			if($this->providerContact->isDefined()) $this->providerContact->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
			if($this->providerCompany->isDefined()) $this->providerCompany->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}
	}
}
