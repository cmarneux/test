<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Address;
use BoondManager\Lib\Filters\Inputs\Attributes\Country;
use BoondManager\Lib\Filters\Inputs\Attributes\Currency;
use BoondManager\Lib\Filters\Inputs\Attributes\Email;
use BoondManager\Lib\Filters\Inputs\Attributes\ExchangeRate;
use BoondManager\Lib\Filters\Inputs\Attributes\Phone;
use BoondManager\Lib\Filters\Inputs\Attributes\PostCode;
use BoondManager\Lib\Filters\Inputs\Attributes\Town;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\HrManager;
use BoondManager\Lib\Filters\Inputs\Relationships\MainManager;
use BoondManager\Lib\Filters\Inputs\Relationships\Pole;
use BoondManager\Services\BM;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Tools;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Lib\Filters\Inputs\Attributes\WebSocial;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccessOnField;

/**
 * Class Profil
 * @property InputString $lastName
 * @property InputString $firstName
 * @property Phone $phone1
 * @property Phone $phone2
 * @property Phone $phone3
 * @property Phone $fax
 * @property Address $address
 * @property PostCode $postcode
 * @property Town $town
 * @property Country $country
 * @property InputString $informationComments
 * @property Email $email1
 * @property Email $email2
 * @property Email $email3
 * @property InputValue $civility
 * @property InputFloat $averageDailyPriceExcludingTax
 * @property InputBoolean $visibility
 * @property InputDate $dateOfBirth
 * @property InputDate $creationDate
 * @property InputDate availability
 * @property InputMultiValues $mobilityAreas
 * @property MainManager $hrManager
 * @property MainManager $mainManager
 * @property Agency $agency
 * @property Pole $pole
 * @property InputString $function
 * @property InputDict $state
 * @property InputMultiObjects $socialNetworks
 * @package BoondManager\Models\Filters\Profiles\Resources
 */
class SaveInformation extends AbstractJsonAPI{

	/**
	 * @var Employee
	 */
	private $_employee;

	/**
	 * Profil constructor.
	 * @param Employee $profil
	 */
	public function __construct($profil = null){

		$this->_employee = $profil;

		parent::__construct();

		$firstName = new InputString('firstName');
		$this->addInput($firstName);

		$firstName = new InputString('lastName');
		$this->addInput($firstName);

		$creationDate = new InputDateTime('creationDate');
		$this->addInput($creationDate);

		$civ = new InputDict('civility', 'specific.setting.civility');
		$this->addInput($civ);

		$typeOf = new InputDict('typeOf', 'specific.setting.typeOf.resource');
		$this->addInput($typeOf);

		$state = new InputDict('state', 'specific.setting.state.resource');
		$this->addInput($state);

		for($i=1; $i<=3; $i++){
			$email = new Email('email'.$i);
			$this->addInput($email);
		}

		for($i=1; $i<=3; $i++){
			$phone = new Phone('phone'.$i);
			$this->addInput($phone);
		}

		$input = new Phone('fax');
		$this->addInput($input);

		$input = new Address();
		$this->addInput($input);

		$input = new PostCode();
		$this->addInput($input);

		$input = new Town();
		$this->addInput($input);

		$input = new Country();
		$this->addInput($input);

		$mainManager = new MainManager();
		$this->addInput($mainManager);

		$hrManager = new HrManager('hrManager');
		$this->addInput($hrManager);

		$agency = new Agency();
		$this->addInput($agency);

		$agency = new Pole('pole');
		$this->addInput($agency);

		$WebSocial = new WebSocial();
		$input = new InputMultiObjects('socialNetworks', $WebSocial);
		$input->addGroupFilterCallback(function($data) {
			$ids = [];
			foreach ($data as $social) {
				if (in_array($social->network->getValue(), $ids)) return false;
				else $ids[] = $social->network->getValue();
			}
			return $data;
		}, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
		$this->addInput($input);

		$visible = new InputBoolean('visibility');
		$this->addInput($visible);

		$birthDate = new InputDate('dateOfBirth');
		$this->addInput($birthDate);

		$input = new InputString('informationComments');
		$this->addInput($input);

		$mobility = new InputMultiDict('mobilityAreas', 'specific.setting.mobilityArea');
		$this->addInput($mobility);

		$function = new InputString('function');
		$function->setMaxLength(100);
		$this->addInput($function);

		$price = new InputFloat('averageDailyPriceExcludingTax');
		$this->addInput($price);

		$currency = new Currency();
		$this->addInput($currency);

		$currencyAgency = new Currency('currencyAgency');
		$this->addInput($currencyAgency);

		$exchangeRate = new ExchangeRate();
		$this->addInput($exchangeRate);

		$exchangeRateAgency = new ExchangeRate('exchangeRateAgency');
		$this->addInput($exchangeRateAgency);

		$input = new InputBoolean('forceAvailability');
		$this->addInput($input);

		$input = new InputValue('availability');
		$input->addFilterCallback(function($value){
			if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value)) {
				return $value;
			}else if ($date = \DateTime::createFromFormat(\DateTime::ISO8601, $value)){
				return $date->format('Y-m-d');
			}else if($value === 1 || $value === '1' || $value === true) {
				return true;
			}else{
				return false;
			}
		});
		$this->addInput($input);
		/*

		$delay = new InputEnum('availabilityDelay', self::DELAY_UNDEFINED);
		$delay->setAllowedValues([
			self::DELAY_1_3_MONTH, self::DELAY_3_MONTH, self::DELAY_LESS_1_MONTH, self::DELAY_MORE_3_MONTH, self::DELAY_DATED, self::DELAY_IMMEDIATE
		]);
		$this->addInput($delay);

		$input = new InputDate('availability');
		$this->addInput($input);

		$input = new InputBoolean('importResume');
		$this->addInput($input);
		*/

		if($this->isCreation()) {
			$this->adaptForCreation();
		}else{
			$this->adaptForEdition();
		}
	}

	private function adaptForCreation() {
		$this->firstName->setRequired(true);
		$this->lastName->setRequired(true);
		$this->mainManager->setRequired(true);
		$this->agency->setRequired(true);
	}

	private function adaptForEdition() {
		$this->firstName->setAllowEmptyValue(false);
		$this->lastName->setAllowEmptyValue(false);
	}

	protected function postValidation() {
		$request = new RequestAccess();
		$request->setData($this->_employee);

		foreach(['creationDate', 'visibility', 'mainManager', 'agency', 'pole'] as $publicField){
			if( $this->$publicField->isDefined() && !(new HaveWriteAccessOnField( $publicField ))->isSatisfiedBy($request) ){
				$this->$publicField->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);
			}
		}

		// if candidate has been hired, its state can not be changed
		if( ($this->isCreation() || $this->_employee->state == Employee::STATE_ARCHIVED)
			&& $this->state->isDefined() && $this->state->getValue() != Employee::STATE_ARCHIVED
			&& !Services\Subscriptions::getLicencesLeft() ){
			$this->state->invalidateIfDebug(BM::ERROR_GLOBAL_RESOURCE_QUOTA_MAX);
		}
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->_employee && $this->_employee->id ? false : true;
	}
}
