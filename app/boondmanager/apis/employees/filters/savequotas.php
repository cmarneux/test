<?php
/**
 * savequotas.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Filters;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Tools;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Lib\Filters\Inputs\Attributes\WebSocial;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccessOnField;

/**
 * Class Profil
 * @property \Wish\Filters\Inputs\InputString $lastName
 * @property \Wish\Filters\Inputs\InputString $firstName
 * @property \Wish\Filters\Inputs\InputString $phone1
 * @property \Wish\Filters\Inputs\InputString $phone2
 * @property \Wish\Filters\Inputs\InputString $phone3
 * @property \Wish\Filters\Inputs\InputString $fax
 * @property \Wish\Filters\Inputs\InputString $address
 * @property \Wish\Filters\Inputs\InputString $postcode
 * @property InputString $town
 * @property \Wish\Filters\Inputs\InputString $country
 * @property \Wish\Filters\Inputs\InputString $informationComments
 * @property InputString $email1
 * @property InputString $email2
 * @property InputString $email3
 * @property InputValue $civility
 * @property \Wish\Filters\Inputs\InputFloat $averageDailyPriceExcludingTax
 * @property InputBoolean $visibility
 * @property \Wish\Filters\Inputs\InputString $dateOfBirth
 * @property InputDate $creationDate
 * @property \Wish\Filters\Inputs\InputDate availability
 * @property InputMultiValues $mobilityAreas
 * @property \Wish\Filters\Inputs\InputRelationship $hrManager
 * @property \Wish\Filters\Inputs\InputRelationship $mainManager
 * @property InputRelationship $agency
 * @property InputRelationship $pole
 * @property \Wish\Filters\Inputs\InputString $function
 * @property \Wish\Filters\Inputs\InputValue $state
 * @property \Wish\Filters\Inputs\InputMultiObjects $socialNetworks
 * @package BoondManager\Models\Filters\Profiles\Resources
 */
class SaveQuotas extends AbstractJsonAPI{

    /**
     * Profil constructor.
     */
    public function __construct(){
        parent::__construct();

		$this->addInput([
		(new InputMultiObjects('absencesQuotas', new WebSocial())),
		]);
    }

	public static function getUserFilter(CurrentUser $user, Employee $resource){

		if(!$user) $user = CurrentUser::instance();
		$filter = new self();

		return $filter;
	}
}
