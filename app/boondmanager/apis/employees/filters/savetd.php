<?php
/**
 * savedt.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Filters;

use BoondManager\Models\Language;
use Wish\Filters\AbstractFilters;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputBoolean;
use BoondManager\Services\BM;

/**
 * Class SaveDT
 * @property InputString title
 * @property InputMultiObjects tools
 * @property InputMultiDict activityAreas
 * @property InputMultiDict expertiseAreas
 * @property InputString skills
 * @property InputDict experience
 * @property InputDict training
 * @property InputMultiObjects diplomas
 * @property InputMultiObjects languages
 * @property InputMultiObjects references
 */
class SaveTD extends AbstractJsonAPI
{
	public function __construct()
	{
		parent::__construct();

		$title = new InputString('title');
		$title->setMaxLength(150);
		$this->addInput($title);

		$skills = new InputString('skills');
		$this->addInput($skills);

		$exp = new InputDict('experience', 'specific.setting.experience');
		$exp->addAllowedValue(BM::INDIFFERENT);
		$this->addInput($exp);

		$training = new InputDict('training', 'specific.setting.training');
		$this->addInput($training);

		$this->addInput( new InputMultiObjects('languages', new Languages()));

		$this->addInput( new InputMultiValues('diplomas'));
		$this->addInput( new InputMultiObjects('tools', new Tool()));

		$this->addInput( new InputMultiDict('activityAreas', 'specific.setting.activityArea'));
		$this->addInput( new InputMultiDict('expertiseAreas', 'specific.setting.expertiseArea'));

		$this->addInput( new InputMultiObjects('references', new Reference()));
		$this->addInput( new InputBoolean('resourceCanModifyTechnicalData'));
	}

	/**
	 * return tools serialized for database
	 * @return string
	 */
	public function serializeTools(){
		return implode('#', array_map(function($value){
			return $value['tool'].'|'.$value['level'];
		}, $this->tools->getValue()));
	}

	/**
	 * return languages serialized for database
	 * @return string
	 */
	public function serializeLanguages(){
		return implode('#', array_map(function($value){
			return $value['language'].'|'.$value['level'];
		}, $this->languages->getValue()));
	}
}

/**
 * Class Tool
 * @property InputDict tool
 * @property InputEnum level
 */
class Tool extends AbstractFilters
{
	protected $_objectClass = \BoondManager\Models\Tool::class;

	public function __construct()
	{
		parent::__construct();

		$tool = new InputDict('tool', 'specific.setting.tool');

		$level = new InputEnum('level');
		$level->setAllowedValues([1,2,3,4,5]);

		$this->addInput([$tool, $level]);
	}
}

/**
 * Class Languages
 * @property InputString language
 * @property InputDict level
 */
class Languages extends AbstractFilters
{
	protected $_objectClass = Language::class;

	public function __construct()
	{
		parent::__construct();

		$language = new InputString('language');

		$level = new InputDict('level', 'specific.setting.languageLevel');

		$this->addInput([$language, $level]);
	}
}
