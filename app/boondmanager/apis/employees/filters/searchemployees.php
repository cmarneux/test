<?php
/**
 * resources.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\APIs\Employees\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;

/**
 * Class Ressources
 * build a filter for lists/resources (search & sort)
 * @property-read InputValue $keywordsType (int) types de mot cle _(cf const KEYWORD_TYPE_***)_
 * @property-read InputBoolean $excludeManager uniquement si pas de compte manager
 * @property-read InputBoolean $returnHRManager responsable rh
 * @property-read InputMultiEnum $sort (string) colonne de tri
 * @property-read InputEnum $period (string) _(cf constantes PERIODE_***)_ type de période
 * @property-read InputDate $startDate (date) date de début
 * @property-read InputDate $endDate (date) date de fin
 * @property-read InputMultiDict $resourceStates état du profil
 * @property-read InputMultiDict $excludeResourceTypes exclusion selon profil type
 * @property-read InputMultiDict $resourceTypes (array) type de profil
 * @property-read InputMultiDict $activityAreas (array) competences applications
 * @property-read InputMultiDict $expertiseAreas (array) competences intervention
 * @property-read InputMultiDict $mobilityAreas (array) mobilitié
 * @property-read InputMultiDict $tools (array) domaine technique (DT) outils
 * @property-read InputMultiDict $experiences (array) domaine technique (DT) expérience
 * @property-read InputMultiDict $trainings domaine technique (DT) formation
 * @property InputBoolean visibleProfile
 * @package BoondManager\APIs\Employees\Filters
 */
class SearchEmployees extends AbstractSearch{
	/**
	 * @var string
	 */
	const PERIMETER_MODULE = BM::MODULE_RESOURCES;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_WORKING = 'working',
		PERIOD_ABSENT = 'absent',
		PERIOD_IDLE = 'idle', //intercontrat
		PERIOD_HIRED = 'hired',
		PERIOD_LEFT = 'left',
		PERIOD_EMPLOYED = 'employed', //means 'with a contract'
		PERIOD_AVAILABLE = 'available',
		PERIOD_UNEMPLOYED = 'unemployed', //means 'with no contract'
		PERIOD_ACTIONS = 'actions';
	/**#@-*/

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_UPDATEDATE = 'updateDate',
		ORDERBY_LASTNAME = 'lastName',
		ORDERBY_TITLE = 'title',
		ORDERBY_AVAILABILITY = 'availability',
		ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS = 'numberOfActivePositionings',
		ORDERBY_PRICEEXCLUDINGTAX = 'priceExcludingTax',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**#@+
	 * @var int types of keywords
	 */
	const
		KEYWORD_TYPE_RESUMETD = 'resumeTd',
		KEYWORD_TYPE_LASTNAME = 'lastName',
		KEYWORD_TYPE_FIRSTNAME = 'firstName',
		KEYWORD_TYPE_FULLNAME = 'fullName',
		KEYWORD_TYPE_EMAILS = 'emails',
		KEYWORD_TYPE_TITLE = 'title',
		KEYWORD_TYPE_PHONES = 'phones',
		KEYWORD_TYPE_NUMBER = 'number',
		KEYWORD_TYPE_RESUME = 'resume',
		KEYWORD_TYPE_TD = 'td';
	/**#@- */

	/**
	 * Resources constructor.
	 */
	public function __construct()
	{
		//~ TODO : 12-05-2016 : Tin : manque le filtre languages (migration v6.7.4 > v6.8.3)
		parent::__construct();

		//profile visibility
		$visibleProfile = new InputBoolean('visibleProfile');
		$visibleProfile->setModeDefaultValue( true );
		$this->addInput($visibleProfile);

		//~ Filter resources available Immediatly or 15 days before or after
		$availabilityType = new InputBoolean('availabilityType');
		$this->addInput($availabilityType);

		//Type de mots clefs
		$keywordsType = new InputEnum('keywordsType');
		$keywordsType->setModeDefaultValue(self::KEYWORD_TYPE_RESUMETD);
		$keywordsType->setAllowedValues([
			self::KEYWORD_TYPE_RESUMETD, self::KEYWORD_TYPE_LASTNAME, self::KEYWORD_TYPE_FIRSTNAME,
			self::KEYWORD_TYPE_FULLNAME, self::KEYWORD_TYPE_EMAILS, self::KEYWORD_TYPE_TITLE,
			self::KEYWORD_TYPE_PHONES, self::KEYWORD_TYPE_NUMBER, self::KEYWORD_TYPE_RESUME, self::KEYWORD_TYPE_TD
		]);
		$this->addInput($keywordsType);

		$this->addInput( new InputMultiDict('resourceStates', 'specific.setting.state.resource'));
		$this->addInput(new InputBoolean('returnHRManager'));
		$this->addInput( new InputMultiDict('excludeResourceTypes', 'specific.setting.typeOf.resource'));
		$this->addInput(new InputBoolean('excludeManager')); //UNIQUEMENT SI PAS DE COMPTE MANAGER

		$this->addInput( new InputMultiDict('resourceTypes', 'specific.setting.typeOf.resource'));
		$this->addInput( new InputMultiDict('activityAreas', 'specific.setting.activityArea'));
		$this->addInput( new InputMultiDict('expertiseAreas', 'specific.setting.expertiseArea'));
		$this->addInput( new InputMultiDict('tools', 'specific.setting.tool'));
		$this->addInput( new InputMultiDict('mobilityAreas', 'specific.setting.mobilityArea'));
		$this->addInput( new InputMultiDict('experiences', 'specific.setting.experience'));
		$this->addInput( new InputMultiDict('trainings', 'specific.setting.training'));

		//DT_LANGUES
		$languages = new InputMultiValues('languages');
		$languages->addFilterCallback(function($value) {
			$language_level = explode('|', $value);
			if(count($language_level) != 2 || !in_array($language_level[1], array_column( Dictionary::getDict('specific.setting.languageLevel') , 'id'))) return false;
			return $value;
		}, 'Value of languages is incorrect !');
		$this->addInput($languages);

		// Colonne de tri
		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_UPDATEDATE, self::ORDERBY_LASTNAME, self::ORDERBY_TITLE, self::ORDERBY_AVAILABILITY,
			self::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS, self::ORDERBY_PRICEEXCLUDINGTAX, self::ORDERBY_MAINMANAGER_LASTNAME
		]);
		$this->addInput($sort);

		// periode
		$period = new InputEnum('period');
		$period->setModeDefaultValue( BM::INDIFFERENT );
		// TODO: amelioration de cette construction dynamique (pourquoi prefixer par -7_ par ex)
		$tabPS = [
			self::PERIOD_WORKING, self::PERIOD_ABSENT, self::PERIOD_IDLE, self::PERIOD_HIRED,
			self::PERIOD_LEFT, self::PERIOD_EMPLOYED, self::PERIOD_AVAILABLE, self::PERIOD_UNEMPLOYED
		];
		foreach(Dictionary::getDict('specific.setting.action.resource') as $item)
			$tabPS[] = self::PERIOD_ACTIONS.'_'.$item['id'];
		$period->setAllowedValues($tabPS);
		$this->addInput($period);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);

	}

	/*
	 * définition de quelques aides pour manipuler ce filtre
	 */

	/**
	 * @var int the period type
	 */
	private $_periodeType;
	/**
	 * get the period type _(@see self::PERIODS)_
	 * @return string
	 */
	public function getPeriodType(){
		if(is_null($this->_periodeType)) {
			$pr = $this->period->getValue();
			$pr = explode('_', $pr);
			$this->_periodeType = $pr[0];
		}
		return $this->_periodeType;
	}

	/**
	 * get the period action (only for a special period type)
	 * @return int|null
	 */
	public function getPeriodAction(){
		$pr = $this->period->getValue();
		$pr = explode('_', $pr);
		if(count($pr)==2){
			return $pr[1];
		}else
			return null;
	}

	/**
	 * is the choosen period the default one
	 * @return bool
	 */
	public function isDefaultPeriod(){
		return $this->period->getValue() == $this->period->getDefaultValue();
	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 *
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
