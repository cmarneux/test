<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveCreateAccess;
use BoondManager\APIs\Employees\Specifications\HaveSearchAccess;

/**
 * Resources list controller.
 * @package Resources
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'civility',
		'firstName',
		'lastName',
		'reference',
		'typeOf',
		'state',
		'visibility',
		'skills',
		'mobilityAreas',
		'title',
		'availability',
		'averageDailyPriceExcludingTax',
		'email1',
		'phone1',
		'phone2',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'numberOfResumes',
		'numberOfActivePositionings',
		'isEntityUpdating',
		'isEntityDeleting',
		'resume',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'hrManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchEmployees();
		$filter->setAndFilterData($this->requestAccess->getParams());

		// handle extraction to file if requested
		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Services\Extraction\Employees('resources.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return;
		}

		// otherwise, filter data for output
		$result = Services\Employees::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess );

		// retrieve filtered params
		$filter = new Filters\SaveInformation();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$profil = Services\Employees::buildFromFilter($filter);

		if(Services\Employees::create($profil)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(Information::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
