<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\APIs\Employees\Specifications\HaveReadAccessOnField;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Information extends AbstractController {
 const ALLOWED_FIELDS = [
	 'id',
	 'creationDate',
	 'updateDate',
	 'civility',
	 'lastName',
	 'firstName',
	 'typeOf',
	 'state',
	 'email1',
	 'email2',
	 'email3',
	 'phone1',
	 'phone2',
	 'phone3',
	 'fax',
	 'address',
	 'postcode',
	 'town',
	 'country',
	 'function',
	 'dateOfBirth',
	 'mobilityAreas',
	 'averageDailyPriceExcludingTax',
	 'currency',
	 'currencyAgency',
	 'exchangeRate',
	 'exchangeRateAgency',
	 'forceAvailability',
	 'availability',
	 'visibility',
	 'informationComments',
	 'socialNetworks' => [
		 'network',
		 'url'
	 ],
	 'mainManager'    => [
		 'id',
		 'lastName',
		 'firstName'
	 ],
	 'hrManager'      => [
		 'id',
		 'lastName',
		 'firstName'
	 ],
	 'pole'           => [
		 'id',
		 'name'
	 ],
	 'agency'         => [
		 'id',
		 'name'
	 ],
	 'resumes'        => [
		 'id',
		 'name'
	 ]
	 ];
	public function api_get() {

		if($id = $this->requestAccess->id) {
			// get an existing profil
			$profil = Services\Employees::get($id, Employee::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$profil) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_INFORMATION), $profil );

			$warning = Services\Employees::getWarnings($profil, $this->requestAccess);

		} else $this->error(404);

		$removeFields = [];
		if(! (new HaveReadAccessOnField('visibility')) ) $removeFields[] = 'visibility';

		$tabData = [
			'data' => $profil->removeFields($removeFields)->filterFields(self::ALLOWED_FIELDS)
		];

		if(isset($warning) && $warning) {
			$tabData['meta'] = [
				'warnings' => $warning
			];
		}

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Update a resource profile data.
	 */
	public function api_put() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_INFORMATION);
		// if profil not found, throw an error (404 not found)
		if(!$profil) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_INFORMATION), $profil );

		$filter = new Filters\SaveInformation($profil);
		$filter->setData( $this->requestAccess->get('data') );

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		//Build project
		Services\Employees::buildFromFilter($filter, $profil);

		if(Services\Employees::updateProfil($profil, Employee::TAB_INFORMATION)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
