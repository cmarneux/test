<?php
/**
 * positionings.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\Positionings\Filters\SearchPositionings;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Positioning;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

/**
 * Class Positionings
 * @package BoondManager\Controllers\Profiles\Resources
 */
class Positionings extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'updateDate',
		'state',
		'startDate',
		'endDate',
		'informationComments',
		'canReadContact',
		'canReadCompany',
		'canReadOpportunity',
		'opportunity' => [
			'id',
			'title',
			'reference',
			'typeOf',
			'mode',
			'state',
			'visibility',
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name'
			]
		],
	];

	public function api_get() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_POSITIONINGS);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_POSITIONINGS), $profil);

		$filter = SearchPositionings::getUserFilter();
		$filter->setIndifferentPerimeter();
		$filter->narrowPerimeter->setValue(0);
		$filter->perimeterManagersType->setDefaultValue(SearchPositionings::PERIMETER_PROFILES);
		$filter->sort->setDefaultValue(SearchPositionings::ORDERBY_UPDATE_DATE);
		$filter->order->setDefaultValue(SearchPositionings::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($profil->getReference().' '.$profil->getCandidateReference());

		$this->checkFilter($filter);

		$result = Services\Positionings::search($filter);

		foreach($result->rows as $positioning){
			/** @var Positioning $positioning */
			$positioning->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
