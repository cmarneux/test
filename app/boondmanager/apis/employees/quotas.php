<?php
/**
 * quotas.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees;

use Wish\Encoders\JsonAPI;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\OldModels\Filters;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;

class Quotas extends AbstractController{

	const ALLOWED_FIELDS = [
		'ID_INTQUO',
		'INTQUO_PERIODE',
		'INTQUO_YEAR',
		'INTQUO_QUOTA',
		'workUnitType',
		'agency',
	];

	/**
	 * Proxy method for search/absencesquotas->api_get
	 * @param \Base $f3
	 * @return mixed
	 */
	public function api_get(){

		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_SETTING_ABSENCESACCOUNTS);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_SETTING_ABSENCESACCOUNTS), $entity);

		foreach($entity->absencesQuotas as $absencesQuota) $absencesQuota->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'data' => $entity->absencesQuotas
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Update a resource quotas data.
	 */
	public function api_put() {
		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_SETTING_ABSENCESACCOUNTS);
		// if profil not found, throw an error (404 not found)
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_SETTING_ABSENCESACCOUNTS), $entity );

		$filter = \BoondManager\APIs\Employees\Filters\SaveQuotas::getUserFilter($this->currentUser, $entity);
		$filter->setDefaultValue( JsonAPI::instance()->serialize($entity) );
		$filter->setData( $this->requestAccess->get('data') );

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		Services\Employees::updateProfil($entity, $filter);

		$tabData = [
			'data' => $entity
		];

		$this->sendJSONResponse($tabData);
	}
}
