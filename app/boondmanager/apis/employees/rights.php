<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Employees;

/**
 * Class Rights
 * @package BoondManager\APIs\Projects
 */
class Rights extends AbstractController {
	/**
	 * Get project's rights
	 */
	public function api_get() {
		$project = Employees::get($this->requestAccess->id);
		if(!$project) $this->error(404);

		$this->sendJSONResponse([
			'data' => Employees::getRights($project)
		]);
	}
}
