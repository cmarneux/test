<?php
/**
 * absencesaccounts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Setting;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

class AbsencesAccounts extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'absencesAccounts' => [
			'id',
			'period',
			'year',
			'amountAcquired',
			'amountBeingAcquired',
			'comments',
			'workUnitType' => [
				'reference'
			],
			'agency' => [
				'id',
				'name'
			],
		],
		'agency' => [
			'id',
			'name',
			'defaultAbsencesAccountsPeriod',
			'absencesQuotas' => [
				'period',
				'workUnitType' => [
					'reference'
				],
				'quotaAcquired'
			]
		]
	];

	public function api_get(){

		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_ABSENCESACCOUNTS);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_ABSENCESACCOUNTS), $entity);

		$entity->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'data' => $entity
		];

		$this->sendJSONResponse($tabData);
	}
}
