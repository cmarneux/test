<?php
/**
 * intranet.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Setting;

use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;

class Intranet extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'level',
		'subscription',
		'language',
		'homePage',
		'defaultSearch',
		'narrowPerimeter',
		'activityExpensesStartDate',
		'documentsAutoCreation',
		'allowExceptionalTimes',
		'workUnitRate',
		'timesReportsWorkflow',
		'expensesReportsWorkflow',
		'absencesReportsWorkflow',
		'workUnitTypesAllowedForAbsences',
		'login',
		'dashboardGraphsAvailable',
		'dashboardGraphsPerimeter' => [
			'agencies' => [
				'id', 'name'
			],
			'businessUnits' => [
				'id', 'name'
			],
			'managers' => [
				'id', 'firstName', 'lastName'
			],
			'poles' => [
				'id', 'name'
			]
		],
		'dashboardGraphsPeriod',
		'navigationBar'
	];

	public function api_get() {

		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_SETTING_INTRANET);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_SETTING_INTRANET), $entity);

		$absences = Services\Employees::buildListAbsences($entity);

		$entity->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'absences' => $absences
			],
			'data' => $entity
		];

		$this->sendJSONResponse($tabData);
	}
}
