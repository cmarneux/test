<?php
/**
 * intranet.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Setting;

use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;

class Security extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'lastName',
		'firstName',
		'userToken',
		'warningForNewDevice',
		'logInOnlyFromThirdParty',
		'manageAllowedDevices',
		'microsoftThirdPartyExists',
		'googleThirdPartyExists',
		'trustelemThirdPartyExists',
		'devices' => [
			'id',
			'isManaged',
			'isSessionExists',
			'name',
			'browser',
			'authorizationDate',
			'lastLogInDate',
			'internetProtocol'
		]
	];

	public function api_get() {

		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_SETTING_SECURITY);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_SETTING_SECURITY), $entity);

		$entity->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'data' => $entity
		];

		$this->sendJSONResponse($tabData);
	}
}
