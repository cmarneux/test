<?php
/**
 * targets.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Setting;

use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Services;

class Targets extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'category',
		'operator',
		'periodType',
		'periodNumber',
		'periodYear',
		'value',
		'scorecard'
	];

	public function api_get() {

		$entity = Services\Employees::get($this->requestAccess->id, Employee::TAB_SETTING_TARGET);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_SETTING_TARGET), $entity);

		$result = Services\Targets::getAllEmployeeObjectives( $entity );
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
