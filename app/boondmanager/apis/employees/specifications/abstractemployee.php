<?php
/**
 * abstractressource.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Employee;
use BoondManager\Lib\RequestAccess;

abstract class AbstractEmployee extends AbstractSpecificationItem{
    /**
     * get the resource from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Employee|null
     */
    public function getData($request){
        if($request->data instanceof Employee) return $request->data;
        else return null;
    }
}
