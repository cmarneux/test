<?php
/**
 * usecanaccessreporting.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class CanAccessReporting extends AbstractEmployee{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;
		$resource = $this->getData($request);
		if(!$resource) return false;

		return $user->hasAccess(BM::MODULE_PROJECTS);
	}
}
