<?php
/**
 * UserHaveCreateAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;

/**
 * Class CanCreateResource
 *
 * Indicate if the user have the right to create a new Resource
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveCreateAccess extends AbstractEmployee
{
	/**
	* check if the user match the specification
	* @param RequestAccess $request
	* @throws \Exception if the resource is a wrong type
	* @return bool
	*/
	public function isSatisfiedBy($request){
		$user = $request->user;

		if($user->isGod()) return true;
		if( !$user->isManager() ) return false;

		return $user->hasRight(BM::RIGHT_CREATION, BM::MODULE_RESOURCES);
	}
}
