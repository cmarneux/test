<?php
/**
 * UserHaveDeleteAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a delete on a resources
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Resources
 */
class HaveDeleteAccess extends AbstractEmployee{

	/**
	* check if the object match the specification
	* @param RequestAccess $request
	* @return bool
	*/
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if( !$user->isManager() ) return false;

		return $user->hasRight(BM::RIGHT_DELETION, BM::MODULE_RESOURCES);
	}
}
