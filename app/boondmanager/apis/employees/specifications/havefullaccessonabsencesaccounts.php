<?php
/**
 * havefullaccessonabsencesaccounts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Employee;
use BoondManager\Services\BM;

class HaveFullAccessOnAbsencesAccounts extends AbstractEmployee{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();
		$employee = $this->getData($object);

		if($user->isGod()) return true;

		// verification accès lecture à la fiche
		if(! (new HaveReadAccess(Employee::TAB_ABSENCESACCOUNTS))->isSatisfiedBy($object)) return false;

		return $employee->isTopManager() || $employee->id != $user->getEmployeeId() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
	}
}
