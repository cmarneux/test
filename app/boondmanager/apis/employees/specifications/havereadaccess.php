<?php
/**
 * UserHaveReadAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Services\BM;
use Wish\MySQL\AbstractDb;
use BoondManager\Models\Employee;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughAgenciesOrBUs;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughBUsUsingHRorManager;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughHierarchyOrBUs;
use BoondManager\OldModels\Specifications\RequestAccess\HRinUserHierarchy;
use BoondManager\OldModels\Specifications\RequestAccess\ManagerInUserHierarchy;
use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;


/**
 * Class CanCreateResource
 *
 * Indicate if the user have the right to road a Resource
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveReadAccess extends AbstractEmployee{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;
		$resource = $this->getData($request);
		if(!$resource) return false;

		if($user->isGod()) return true;

		$myProfil = ($resource->id == $user->getEmployeeId());

		// récupération des ACLs, l'utilisateur doit à minima avoir un acces client pour acceder aux fiches ressources
		if( !(new UserTypeIs([BM::USER_TYPE_MANAGER, BM::USER_TYPE_RESOURCE]))->isSatisfiedBy($request))
			return false;

		switch($this->getTab()) {
			case Employee::TAB_FLAGS:
			case Employee::TAB_INFORMATION:
				$read = $myProfil && ($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $user->hasAccess(BM::MODULE_RESOURCES));
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible()
					&& (new CanAccessThroughAgenciesOrBUs)->or_(new HRinUserHierarchy)->or_(new ManagerInUserHierarchy)->isSatisfiedBy($request);
				return boolval($read);
			case Employee::TAB_ADVANTAGES:
			case Employee::TAB_ADMINISTRATIVE: // 1
				$readTab = $user->hasAccess(BM::MODULE_RESOURCES, BM::MODULE_RESOURCES_ADMINISTRATIVE, [
					BM::RIGHT_ACCESS_ADMINISTRATIVE_READWRITE,
					BM::RIGHT_ACCESS_ADMINISTRATIVE_READ
				]);

				if(!$readTab) return false;

				$profilVisible = $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible();

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$read |= $profilVisible && (new HRinUserHierarchy)->or_(new ManagerInUserHierarchy)->isSatisfiedBy($request);
				$read |= $profilVisible && $resource->isManager() && (new CanAccessThroughBUsUsingHRorManager)->isSatisfiedBy($request);
				$read |= $profilVisible && $resource->isManager() && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request);

				return boolval($read);

			case Employee::TAB_TECHNICALDATA: // 2
				$read = $myProfil && ($resource->isManager() || $resource->PROFIL_ALLOWCHANGE || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE));
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && ((new CanAccessThroughHierarchyOrBUs)->or_(new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);

				return boolval($read);
			case Employee::TAB_ACTIONS: // 3

				$read =  $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
					(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || (!$resource->isManager() && !$myProfil && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) &&
						($user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) || $user->hasAccess(BM::MODULE_RESOURCES, BM::MODULE_RESOURCES_ACTIONS) )
					)
				);
				$read |= $myProfil && ( (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || !$resource->isTopManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));

				return boolval($read);

			case Employee::TAB_POSITIONINGS: // 4

				if(!$user->hasAccess(BM::MODULE_OPPORTUNITIES)) return false;

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
						(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
						|| (!$resource->isManager() && !$myProfil && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)

						));
				$read |= $myProfil && ( (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || !$resource->isTopManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));

				return boolval($read);
			case Employee::TAB_DELIVERIES: //5

				if(!$user->hasAccess(BM::MODULE_PROJECTS)) return false;

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$read |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
						(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
						|| (!$resource->isManager() && !$myProfil && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)

					));
				$read |= $myProfil && ( (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || !$resource->isTopManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));

				return boolval($read);
			case Employee::TAB_TIMESREPORTS: //6
			case Employee::TAB_EXPENSESREPORTS: //7
			case Employee::TAB_ABSENCESREPORTS: //8
				$accessActivities = $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES);

				$read = $myProfil && ($user->isIntranetEnabled() || $accessActivities);
				$read |= $accessActivities && $user->hasAccess(BM::MODULE_RESOURCES) && ($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $resource->isVisible() &&
						(
							(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
							|| ($user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && !$resource->isManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request))
						)
					);

				return boolval($read);
			case Employee::TAB_ABSENCESACCOUNTS:case Employee::TAB_SETTING_ABSENCESACCOUNTS: // 9

				$accessActivities = $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES);

				$read = $myProfil && ($user->isIntranetEnabled() && $user->hasAccess(BM::MODULE_ABSENCESACCOUNTS) || $accessActivities);
				$read |= $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES) && $user->hasAccess(BM::MODULE_RESOURCES) && (
							$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
							|| (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
							|| ($user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && !$resource->isManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request))
						);

				return boolval($read);
			case Employee::TAB_SETTING_INTRANET:
			case Employee::TAB_SETTING_TARGET:
			case Employee::TAB_SETTING_SECURITY:
				return (new HaveWriteAccess(Employee::TAB_INFORMATION))->isSatisfiedBy($request);
			default: return false;
		}
	}
}
