<?php
/**
 * userhavereadaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveReadAccessOnField extends AbstractEmployee{

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($object)
	{
		/** @var CurrentUser $user*/
		$user = $object->user;

		if($user->isGod()) return true;

		switch($this->field){
			case 'visibility':
				return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
			default:
				throw new \Exception('no rules for field "'.$this->field.'"');
		}
	}
}
