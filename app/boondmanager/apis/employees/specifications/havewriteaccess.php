<?php
/**
 * UserHaveWriteAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Services\BM;
use Wish\MySQL\AbstractDb;
use BoondManager\Models\Employee;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughAgenciesOrBUs;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughBUsUsingHRorManager;
use BoondManager\OldModels\Specifications\RequestAccess\CanAccessThroughHierarchyOrBUs;
use BoondManager\OldModels\Specifications\RequestAccess\HRinUserHierarchy;
use BoondManager\OldModels\Specifications\RequestAccess\ManagerInUserHierarchy;
use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;

/**
 * Class CanWriteResource
 *
 * Indicate if the user have the right to write into Resource
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveWriteAccess extends AbstractEmployee{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;
		$resource = $this->getData($request);
		if(!$resource) return false;

		if($user->isGod()) return true;

		$myProfil = ($resource->id == $user->getEmployeeId() );

		// récupération des ACLs, l'utilisateur doit à minima avoir un acces client pour acceder aux fiches ressources
		if( !(new UserTypeIs([BM::USER_TYPE_MANAGER, BM::USER_TYPE_RESOURCE]))->isSatisfiedBy($request))
			return false;

		switch($this->getTab()) {
			case Employee::TAB_INFORMATION:
				$write = $myProfil && $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$write |= $myProfil && (new CanAccessThroughBUsUsingHRorManager)->isSatisfiedBy($request);
				$write |= $myProfil && $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isTopManager();

				$write |= $user->hasAccess(BM::MODULE_RESOURCES) && $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$write |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
							(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
							|| (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && !$resource->isManager()
						);

				return boolval($write);
			case Employee::TAB_ADMINISTRATIVE: // 1
				$writeTab = $user->hasRight(BM::RIGHT_ACCESS_ADMINISTRATIVE, BM::MODULE_RESOURCES, BM::RIGHT_ACCESS_ADMINISTRATIVE_READWRITE);

				if(!$writeTab) return false;

				$profilVisible = $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible();

				$write = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$write |= $profilVisible && (new HRinUserHierarchy)->or_(new ManagerInUserHierarchy)->isSatisfiedBy($request);
				$write |= $profilVisible && !$resource->isManager() && (new CanAccessThroughBUsUsingHRorManager())->isSatisfiedBy($request);
				$write |= $profilVisible && !$resource->isManager() && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request);

				return boolval($write);

			case Employee::TAB_TECHNICALDATA: // 2
				$write = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$write |= $myProfil && (
							$resource->PROFIL_ALLOWCHANGE
							|| $resource->isTopManager() && $resource->isManager()
							|| (new CanAccessThroughBUsUsingHRorManager)->isSatisfiedBy($request)
					);
				$write |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
							(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
							|| !$resource->isManager() && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request)
					);

				return boolval($write);
			case Employee::TAB_ACTIONS: // 3

				$write = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$write |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
						(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
						|| !$resource->isManager() && !$myProfil && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)
					);
				$write |= $myProfil && ( (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || !$resource->isTopManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));

				return boolval($write);

			case Employee::TAB_POSITIONINGS: // 4

				if(!$user->hasAccess(BM::MODULE_OPPORTUNITIES)) return false;

				$write = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$write |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
						(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
						|| (!$resource->isManager() && !$myProfil && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)

						));
				$write |= $myProfil && ( (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || !$resource->isTopManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));

				return boolval($write);
			case Employee::TAB_DELIVERIES: //5

				if(!$user->hasAccess(BM::MODULE_PROJECTS)) return false;

				$write = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && ($myProfil || $user->hasAccess(BM::MODULE_RESOURCES));
				$write |= $user->hasAccess(BM::MODULE_RESOURCES) && $resource->isVisible() && (
						(new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
						|| (!$resource->isManager() && !$myProfil && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request) && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)

						));
				$write |= $myProfil && ( (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) || !$resource->isTopManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request));

				return boolval($write);
			case Employee::TAB_TIMESREPORTS: //6
			case Employee::TAB_EXPENSESREPORTS: //7
			case Employee::TAB_ABSENCESREPORTS: //8
				$accessActivities = $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES);

				$write = $myProfil && ($user->isIntranetEnabled() || $accessActivities) && (
						$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
						|| $resource->isTopManager()
						|| (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
					);
				$write |= $accessActivities && $user->hasAccess(BM::MODULE_RESOURCES) && (
							$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
							|| $resource->isVisible() && ( (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request)
								|| (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request) && !$myProfil && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && !$resource->isManager()
							)
						);

				return boolval($write);
			case Employee::TAB_SETTING_ABSENCESACCOUNTS:

				$accessActivities = $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES);

				$write = $myProfil && ($user->isIntranetEnabled() && $user->hasAccess(BM::MODULE_ABSENCESACCOUNTS) || $accessActivities);
				$write &= $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $resource->isTopManager() || (new CanAccessThroughBUsUsingHRorManager)->isSatisfiedBy($request);
				$write |= $accessActivities && $user->hasAccess(BM::MODULE_RESOURCES) && (
							$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
							|| $resource->isVisible() && (new CanAccessThroughHierarchyOrBUs)->isSatisfiedBy($request)
							|| $resource->isVisible() && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && !$resource->isManager() && (new CanAccessThroughAgenciesOrBUs)->isSatisfiedBy($request)
					);

				return boolval($write);
			default: return false;
		}
	}
}
