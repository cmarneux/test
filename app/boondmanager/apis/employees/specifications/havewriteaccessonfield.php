<?php
/**
 * havewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveWriteAccessOnField extends AbstractEmployee{
	private $field;

	public function __construct($field) {
		$this->field = $field;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {

		$user = $request->user;

		$resource = $this->getData($request);

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->field){
			case 'visibility':
				$write = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				break;
			case 'mainManager':
				return (!$resource || !$resource->isManager())
					&& $user->hasRight(BM::RIGHT_SHOWALLMANAGERS_AFFECTATION, BM::MODULE_RESOURCES);
			case 'pole':
			case 'agency':
				return ($resource && $resource->id != $user->getAccount()->id || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE))
					&& $user->hasRight(BM::RIGHT_SHOWALLMANAGERS_AFFECTATION, BM::MODULE_RESOURCES);
				break;
			case 'creationDate':
				$write = $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_RESOURCES);
				break;
			default:
				$write = false;
				break;
		}
		return $write;
	}
}
