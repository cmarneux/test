<?php
/**
 * isactionallowed.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Employees\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Employee;

class IsActionAllowed extends AbstractEmployee {
	private $action;

	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;
		$resource = $this->getData($request);
		if(!$resource) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$allow = true;
		switch($this->action){
			case 'downloadResumes':
				$allow = $resource->resume != 0;
				break;
			case 'addAction':
				$allow = (new HaveWriteAccess(Employee::TAB_ACTIONS))->isSatisfiedBy($request);
				break;
			case 'seeProductionForecast':
				$allow = true;
				break;
			case 'downloadTD':
				$allow = true;
				break;
			case 'addOpportunity':
				$allow = true;
				break;
			case 'addPositioning':
				$allow = true;
				break;
			case 'share':
				$allow = true;
				break;
			default:
				$allow = false;
				break;
		}
		return $allow;
	}
}
