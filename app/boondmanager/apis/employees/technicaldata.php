<?php
/**
 * td.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;
use BoondManager\Services;
use BoondManager\Services\BM;

/**
 * Class TechnicalData
 * @package BoondManager\Controllers\Profiles\Resources
 */
class TechnicalData extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'skills',
		'activityAreas',
		'expertiseAreas',
		'languages',
		'title',
		'tools',
		'diplomas',
		'experience',
		'training',
		'resourceCanModifyTechnicalData',
		'references' => [
			'id',
			'title',
			'description'
		],
	];

	/**
	 * Gets TechnicalData data of a resource
	 */
	public function api_get() {

		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_TECHNICALDATA);
		if(!$profil) $this->error(404); //objet non trouvé

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_TECHNICALDATA), $profil );

		$tabData = [
			'data' => $profil->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		// chargement de la fiche
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_TECHNICALDATA);
		if(!$profil) $this->error(404); //objet non trouvé

		// vérification des droits
		$this->checkAccessWithSpec( new HaveWriteAccess(Employee::TAB_TECHNICALDATA), $profil );

		$filter = new Filters\SaveTD($profil);
		$filter->setData($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		//Build project
		$profil = Services\Employees::buildFromFilter($filter, $profil);

		if(Services\Employees::updateProfil($profil, Employee::TAB_TECHNICALDATA)) {
			$this->sendJSONResponse([
				'data' => $profil->filterFields(self::ALLOWED_FIELDS)
			]);
		}else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
