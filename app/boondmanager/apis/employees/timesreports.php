<?php
/**
 * timesreports.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Employees;

use BoondManager\APIs\TimesReports\Filters\SearchTimesReports;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Account;
use BoondManager\Models\TimesReport;
use BoondManager\Services;
use BoondManager\Models\Employee;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;

/**
 * Class TimesReports
 * @package BoondManager\Controllers\Profiles\Resources
 */
class TimesReports extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'term',
		'state',
		/* Exemple utilisable pour tester la méthode récursive filterFields 'VALIDATIONS' => [
			'ID_VALIDATION',
			'VAL_ETAT',
		],*/
	];

	/**
	 * Get TimesReports data on a resource profil
	 */
	public function api_get() {
		$profil = Services\Employees::get($this->requestAccess->id, Employee::TAB_TIMESREPORTS);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Employee::TAB_TIMESREPORTS), $profil);

		$filter = new SearchTimesReports();
		$filter->setData($this->requestAccess->getParams());
		$filter->disableMaxResultLimit();
		//~ La date de fin est la date actuelle formatée comme il faut
		$filter->endMonth->setValue(date('Y-m'));
		//~ si USER_TPSFRSSTART est défini on l'utilise, sinon on utilise la date de début min des contrats, sinon la date actuelle
		$start = $profil->activityExpensesStartDate;
		if(!$start || $start == Account::HIRING_DATE) $start = $profil->startContract;
		if(!$start) $start = date('Y-m-d');
		$start = explode('-', $start); $start = $start[0].'-'.$start[1];
		$filter->startMonth->setValue($start);
		$filter->keywords->setValue($profil->getReference());

		$this->checkFilter($filter);

		$result = Services\TimesReports::search($filter);

		foreach($result->rows as $timesReport) {
			/** @var TimesReport */
			$timesReport->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				],
				'missingReports' => Services\TimesReports::getMissingMonths($result->rows, $filter->startMonth->getValue(), $filter->endMonth->getValue())
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
