<?php
/**
 * expensesreport.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\ExpensesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\ExpensesReport;
use BoondManager\Services;
use BoondManager\Services\BM;
use BoondManager\APIs\ExpensesReports\Specifications\HaveCreateAccess;
use BoondManager\APIs\ExpensesReports\Specifications\HaveDeleteAccess;
use BoondManager\APIs\ExpensesReports\Specifications\HaveReadAccess;
use BoondManager\APIs\ExpensesReports\Specifications\HaveWriteAccess;

/**
 * ExpensesReport controller.
 * @package ExpensesReports
 * @namespace \BoondManager\Controllers\Profiles
 */
class Entity extends AbstractController {
	const INCLUDED_BATCH = [
		'id',
		'title',
	];

	const INCLUDED_DELIVERY = [
		'id',
		'title',
		'startDate',
		'endDate',
		'expensesDetails' => [
			'id',
			'expenseType' => [
				'reference',
				'name'
			],
			'periodicity',
			'netAmount'
		]
	];

	const INCLUDED_PROJECT = [
		'id',
		'reference',
		'company' => [
			'id',
			'name',
		],
		'deliveries' => self::INCLUDED_DELIVERY,
		'batches' => self::INCLUDED_BATCH,
	];

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'term',
		'informationComments',
		'closed',
		'paid',
		'advance',
		'ratePerKilometerType' => [
			'reference',
			'amount',
			'name'
		],
		'currencyAgency',
		'exchangeRateAgency',
		'validationWorkflow' => [
			'id',
			'lastName',
			'firstName'
		],
		'actualExpenses' => [
			'id',
			'startDate',
			'number',
			'title',
			'currency',
			'exchangeRate',
			'numberOfKilometers',
			'amountIncludingTax',
			'tax',
			'reinvoiced',
			'isKilometricExpense',
			'activityType',
			'expenseType' => [
				'reference',
				'taxRate',
				'name'
			],
			'batch' => [
				'id',
				'title'
			],
			'delivery' => [
				'id',
				'title',
				'startDate',
				'endDate'
			],
			'project' => [
				'id',
				'reference'
			]
		],
		'fixedExpenses' => [
			'id',
			'startDate',
			'row',
			'amountIncludingTax',
			'numberOfKilometers',
			'isKilometricExpense',
			'activityType',
			'expenseType' => [
				'reference',
				'taxRate',
				'name'
			],
			'batch' => [
				'id',
				'title'
			],
			'delivery' => [
				'id',
				'title',
				'startDate',
				'endDate'
			],
			'project' => [
				'id',
				'reference'
			]
		],
		'resource' => [
			'id',
			'lastName',
			'firstName',
			'function',
			'account' => [
				'id',
			],
		],
		'agency' => [
			'id',
			'name',
			'expenseTypes' => [
				'reference',
				'name',
				'taxRate'
			],
			'ratePerKilometerTypes' => [
				'reference',
				'name',
				'amount'
			]
		],
		'files' => [
			'id',
			'name'
		],
		'timesReport' => [
			'id',
			'regularTimes' => [
				'id',
				'startDate',
				'duration',
				'row',
				'workUnitType' => [
					'reference'
				],
				'batch' => [
					'id',
				],
				'delivery' => [
					'id',
				],
				'project' => [
					'id',
				],
			],
		],
		'orders' => [
			'id'
		],
		'projects' => self::INCLUDED_PROJECT,
		'validations' => [
			'id',
			'date',
			'state',
			'reason',
			'realValidator' => [
				'id',
				'lastName',
				'firstName',
				'function'
			],
			'expectedValidator' => [
				'id',
				'lastName',
				'firstName'
			]
		]
	];

	/**
	 * Get information data on a expensesreport
	*/
	public function api_get() {
		if($id = $this->requestAccess->id) {
			// gets existing
			$entity = Services\ExpensesReports::get($id);
			// if not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(ExpensesReport::TAB_DEFAULT), $entity );
		} else {
			$filter = new Filters\GetDefault();
			$filter->setData( $this->requestAccess->getParams() );

			// check params
			$this->checkFilter($filter);

			$this->checkAccessWithSpec( new HaveCreateAccess ); // TODO

			$entity = Services\ExpensesReports::getNew($filter);
		}

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		//~ TODO : Tin : Renseigner la meta 'warnings' ...
		$this->sendJSONResponse($tabData);
	}

	/**
	 * Update a expensesreport's data.
	 */
	public function api_put() {

		$entity = Services\ExpensesReports::get($this->requestAccess->id);
		// if entity not found, throw an error (404 not found)
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec( new HaveWriteAccess(), $entity );

		$filter = new Filters\SaveInformation($entity);
		$filter->setData( $this->requestAccess->get('data') );

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		$entity = Services\ExpensesReports::buildFromFilter($filter, $entity);

		if(Services\ExpensesReports::update($entity)) {
			$tabData = [
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			];

			$this->sendJSONResponse($tabData);
		}else{
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}

	}

	public function api_delete() {
		$entity = Services\ExpensesReports::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $entity );

		$deleted = Services\ExpensesReports::delete($entity);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
