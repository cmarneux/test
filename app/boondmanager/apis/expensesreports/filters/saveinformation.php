<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\ExpensesReports\Filters;

use BoondManager\Lib\Filters\ActualExpense;
use BoondManager\Lib\Filters\Expense;
use BoondManager\Lib\Filters\FixedExpense;
use BoondManager\Lib\Filters\Inputs\Attributes\RatePerKilometerTypeReference;
use BoondManager\Lib\Filters\Inputs\InputMonth;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\Employee;
use BoondManager\Services\Agencies;
use BoondManager\Services\BM;
use BoondManager\Services\Employees;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use BoondManager\Models\ExpensesReport;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class SaveInformation
 * @package BoondManager\Models\Filters\Profiles\ExpensesReports
 * @property InputString informationComments
 * @property InputMultiObjects actualExpenses
 * @property InputMultiObjects fixedExpenses
 * @property InputMonth term
 * @property Employee resource
 * @property Agency agency
 */
class SaveInformation extends AbstractJsonAPI{

	/**
	 * @var ExpensesReport|null
	 */
	private $_expenseReport;

	/**
	 * Profil constructor.
	 * @param ExpensesReport $expenseReport
	 */
	public function __construct($expenseReport = null){

		$this->_expenseReport = $expenseReport;

		parent::__construct();

		$this->addInput( new InputString('informationComments') );

		$this->addInput( new InputMonth('term') );

		$times = new InputMultiObjects('actualExpenses', new ActualExpense('actualExpenses'));
		$times->setMode( InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE );
		$this->addInput( $times );

		$times = new InputMultiObjects('fixedExpenses', new FixedExpense('fixedExpenses'));
		$times->setMode( InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE );
		$this->addInput( $times );

		$this->addInput( new InputBoolean('paid') );
		$this->addInput( new InputFloat('advance') );

		$this->addInput( new RatePerKilometerTypeReference() );

		$this->addInput( new Employee() );
		$agency = new Agency();
		$agency->addFilterCallback(function($value){
			return Agencies::get($value->id, Models\Agency::TAB_ACTIVITYEXPENSES);
		});
		$this->addInput( $agency );

		if( $this->isCreation() ) {
			$this->adaptForCreation();
		} else {
			$this->adaptForEdition();
		}
	}

	private function isCreation() {
		return !$this->_expenseReport || !$this->_expenseReport->id;
	}

	private function adaptForEdition() {
		$this->resource->setDisabled(true);
		$this->agency->setDisabled(true);
		$this->term->setDisabled(true);
	}

	private function adaptForCreation() {
		$this->term->setRequired(true);
		$this->agency->setRequired(true);
		$this->resource->setRequired(true);
	}

	/**
	 * @return string
	 */
	private function getStartPeriod() {
		if($this->isCreation()) {
			$report = new ExpensesReport(['term' => $this->term->getValue()]);
			return $report->getStartPeriod();
		} else {
			return $this->_expenseReport->getStartPeriod();
		}
	}

	/**
	 * @return false|string
	 */
	private function getEndPeriod() {
		if($this->isCreation()) {
			$report = new ExpensesReport(['term' => $this->term->getValue()]);
			return $report->getEndPeriod();
		} else {
			return $this->_expenseReport->getEndPeriod();
		}
	}

	private function getResource() {
		if($this->isCreation()) return $this->resource->getValue();
		else return $this->_expenseReport->resource;
	}

	protected function postValidation() {

		$agency = $this->isCreation() ? $this->agency->getValue() : $this->_expenseReport->agency;

		/** @var ActualExpense $model */
		$model = $this->actualExpenses->getModel();
		$model->expenseType->setAllowedExpenseTypes($agency->expenseTypes);

		/** @var FixedExpense $model */
		$model = $this->fixedExpenses->getModel();
		$model->expenseType->setAllowedExpenseTypes($agency->expenseTypes);

		$projects = Employees::getAllProjects( $this->getResource()->id, $this->getStartPeriod(), $this->getEndPeriod());

		$projectsIDs = Tools::getFieldsToArray($projects, 'id');
		$deliveriesIDs = [];
		$batchesIDs = [];

		foreach($projects as $project) {
			$deliveriesIDs = array_merge($deliveriesIDs, Tools::getFieldsToArray($project->deliveries, 'id'));
			$batchesIDs = array_merge($batchesIDs, Tools::getFieldsToArray($project->batches, 'id'));
		}

		foreach( $this->actualExpenses->getItems() as $expense) {
			/** @var ActualExpense $expense */
			if(!$expense->isKilometricExpense->getValue())
				continue;

			/** @var Models\ExpenseType $expType */
			$expType = $expense->expenseType->getExpenseType();
			$activity = $expense->activityType->getValue();

			if($activity == Models\Expense::TYPE_PRODUCTION) {

			}

			if($expense->project->isDefined() && $expense->project->getValue()) {
				if($activity !== Models\Expense::TYPE_PRODUCTION) {
					$expense->project->invalidate(ExpensesReport::ERROR_PROJECT_INCOMPATIBLE_WITH_WORKUNITTYPE);
				} else if(!in_array($expense->project->id->getValue(), $projectsIDs)) {
					$expense->project->id->invalidate(ExpensesReport::ERROR_PROJECT_ID_NOT_ALLOWED);
				}
			}

			if($expense->delivery->isDefined() && $expense->delivery->getValue()) {
				if ($activity !== Models\Expense::TYPE_PRODUCTION) {
					$expense->delivery->invalidate(ExpensesReport::ERROR_DELIVERY_INCOMPATIBLE_WITH_WORKUNITTYPE);
				} else if (!in_array($expense->delivery->id->getValue(), $deliveriesIDs)) {
					$expense->delivery->id->invalidate(ExpensesReport::ERROR_DELIVERY_ID_NOT_ALLOWED);
				}
			}

			if($expense->batch->isDefined() && $expense->batch->getValue()) {
				if ($activity !== Models\Expense::TYPE_PRODUCTION) {
					$expense->batch->invalidate(ExpensesReport::ERROR_BATCH_INCOMPATIBLE_WITH_WORKUNITTYPE);
				} else if (!in_array($expense->batch->id->getValue(), $batchesIDs)) {
					$expense->batch->id->invalidate(ExpensesReport::ERROR_BATCH_ID_NOT_ALLOWED);
				}
			}
		}

		foreach( $this->fixedExpenses->getItems() as $expense) {
			/** @var FixedExpense $expense */

			if($expense->project->isDefined() && $expense->project->getValue() && !in_array($expense->project->id->getValue(), $projectsIDs)) {
				$expense->project->id->invalidate(ExpensesReport::ERROR_PROJECT_ID_NOT_ALLOWED);
			}

			if($expense->delivery->isDefined() && $expense->delivery->getValue() && !in_array($expense->delivery->id->getValue(), $deliveriesIDs)) {
				$expense->delivery->id->invalidate(ExpensesReport::ERROR_DELIVERY_ID_NOT_ALLOWED);
			}

			if($expense->batch->isDefined() && $expense->batch->getValue() && !in_array($expense->batch->id->getValue(), $batchesIDs)) {
				$expense->batch->id->invalidate(ExpensesReport::ERROR_BATCH_ID_NOT_ALLOWED);
			}
		}

		if($this->isCreation() && $this->resource->getValue()->agency->id != $this->agency->getValue()->id) {
			$this->agency->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}
	}
}
