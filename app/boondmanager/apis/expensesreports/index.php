<?php
/**
 * expenses.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\ExpensesReports;

use BoondManager\APIs\ExpensesReports\Filters\SearchExpensesReports;
use BoondManager\Controllers\Profiles;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\ExpensesReport;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\ExpensesReports\Specifications\HaveCreateAccess;
use BoondManager\APIs\ExpensesReports\Specifications\HaveSearchAccess;
use BoondManager\OldModels\Specifications\RequestAccess\HaveRight;

class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'term',
		'state',
		'paid',
		'agency' => [
			'id',
			'name'
		],
		'resource' => [
			'id',
			'lastName',
			'firstName'
		],
		'validations' => [
			'id',
			'date',
			'state',
			'expectedvalidator' => [
				'id',
				'lastName',
				'firstName'
			]
		]
	];

	public function api_get() {

		$searchAccess = new HaveSearchAccess;
		$extractAccess = $searchAccess
			->and_( new HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_ACTIVITIES_EXPENSES) )
			->isSatisfiedBy($this->requestAccess);

		$this->checkAccessWithSpec( $searchAccess );

		$filter = new SearchExpensesReports();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		if( false && $extractAccess ) { // TODO
			$extractRequest = new Filters\Extract\ExpensesReports();
			$extractRequest->setAndFilterData($this->requestAccess->getParams());
			if($extractRequest->isValid() && $this->requestAccess->get('format') == 'csv'){
				$service = new Services\Extraction\ExpensesReports(
					'expenses-reports.csv',
					$extractRequest->encoding->getValue(),
					$extractRequest->fullExtract->getValue()
				);
				$service->extract($filter);
				return;
			}
		}

		$result = Services\ExpensesReports::search($filter);

		foreach($result->rows as $entity){
			/** @var ExpensesReport $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess() );

		// retrieve filtered params
		$filter = new Filters\SaveInformation();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$entity = Services\ExpensesReports::buildFromFilter($filter);

		if(Services\ExpensesReports::create($entity)) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
