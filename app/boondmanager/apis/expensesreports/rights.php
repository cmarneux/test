<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\ExpensesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\ExpensesReports;

class Rights extends AbstractController{
	public function api_get(){
		$timesReport = ExpensesReports::get($this->requestAccess->id);
		if(!$timesReport) $this->error(404);

		$this->sendJSONResponse([
			'data' => ExpensesReports::getRights($timesReport)
		]);
	}
}
