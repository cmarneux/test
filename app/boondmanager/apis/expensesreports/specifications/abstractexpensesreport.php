<?php
/**
 * abstractexpensesreport.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\ExpensesReports\Specifications;

use BoondManager\Lib\Specifications\AbstractActivity;
use BoondManager\Models\ExpensesReport;
use BoondManager\Lib\RequestAccess;

abstract class AbstractExpensesReport extends AbstractActivity {
	/**
	 * get the expensesreport from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\ExpensesReport|null
	 */
	public function getExpensesReport($request){
		if($request->data instanceof ExpensesReport) return $request->data;
		else return null;
	}
}
