<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\ExpensesReports\Specifications;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Indicate if the user have the right to create a expensesreport
 * @package BoondManager\Models\Specifications\User
 */
class HaveCreateAccess extends AbstractExpensesReport{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->user;

		if($user->isGod()) return true;

		// TODO: Implement isSatisfiedBy() method. from Loic's spec
		if(!$user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES)) return false;

		return true;
	}
}
