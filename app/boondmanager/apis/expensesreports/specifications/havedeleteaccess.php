<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\ExpensesReports\Specifications;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class CanWriteExpensesReport
 *
 * Indicate if the user have the right to write into ExpensesReport
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractExpensesReport
{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the positioning is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user    = $request->user;
		$entity = $this->getExpensesReport($request);
		if (!$entity) return false;

		if ($user->isGod()) return true;
		if(!$user->isManager()) return false;

		//~ TODO : Tin : Adapter les tests supplémentaires pour chaque onglets suivant les specs de Loïc
		return $user->hasRight(BM::RIGHT_DELETION, BM::MODULE_ACTIVITIES_EXPENSES);
	}
}
