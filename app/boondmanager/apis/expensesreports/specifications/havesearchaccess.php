<?php
/**
 * UserHaveSearchAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\ExpensesReports\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on absences
 *
 * @package BoondManager\Models\Specifications\RequestAccess\ExpensesReports
 */
class HaveSearchAccess extends AbstractExpensesReport{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// CONFORME v6 !
		return $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES);
	}
}
