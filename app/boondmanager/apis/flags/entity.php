<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Flags;

use BoondManager\APIs\Flags\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Flags\Specifications\HaveReadAccess;
use BoondManager\APIs\Flags\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Flags;

/**
 * Class Entity
 * @package BoondManager\APIs\Flags
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'name',
		'mainManager' => [
			'id', 'firstName', 'lastName'
		]
	];

	/**
	 * Get flag's basic data
	 */
	public function api_get() {
		if($this->requestAccess->id) {
			$flag = Flags::get($this->requestAccess->id);
			if(!$flag) $this->error(404);

			$this->checkAccessWithSpec(new HaveReadAccess(), $flag);
		} else $this->error(404);

		$this->sendJSONResponse([
			'data' => $flag->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	/**
	 * Update flag's basic data
	 */
	public function api_put() {
		$flag = Flags::get($this->requestAccess->id);
		if(!$flag) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $flag);

		//Build filters
		$filter = new Filters\Entity($flag);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build flaf
		Flags::buildFromFilter($filter, $flag);

		if(Flags::update($flag))
			$this->sendJSONResponse([
				'data' => $flag->filterFields(self::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}

	/**
	 * Delete a flag
	 */
	public function api_delete() {
		$flag = Flags::get($this->requestAccess->id);
		if(!$flag) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $flag);

		if(Flags::delete($flag->getID()))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}
}
