<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Flags\Filters;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputString;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Models;

/**
 * Class Entity
 * @property InputString name
 * @property Relationships\MainManager mainManager
 * @package BoondManager\APIs\Flags\Filters
 */
class Entity extends AbstractJsonAPI {
	/**
	 * @var Models\Flag
	 */
	protected $flag;

	/**
	 * Information constructor.
	 * @param Models\Flag|null $flag
	 */
	public function __construct(Models\Flag $flag = null) {
		parent::__construct();

		$this->flag = $flag;

		$input = new InputString('name');
		$input->setMaxLength(100);
		$this->addInput($input);

		$this->addInput(new Relationships\MainManager());

		if($this->isCreation())
			$this->adaptForCreation();
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->flag->id ? false : true;
	}

	/**
	 * Set inputs for a creation
	 */
	private function adaptForCreation() {
		$this->name->setRequired(true);
		$this->mainManager->setRequired(true);
	}
}


