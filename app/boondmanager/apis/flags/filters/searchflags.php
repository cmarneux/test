<?php
/**
 * searchflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Flags\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;

/**
 * Class SearchFlags
 * @property InputMultiValues sort
 * @package BoondManager\APIs\Flags\Filters
 */
class SearchFlags extends AbstractSearch {
	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_NAME = 'name',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**
	 * SearchProjects constructor.
	 */
	public function __construct()
	{
		parent::__construct($addPerimeter = true, $addFlags = false);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_NAME, self::ORDERBY_MAINMANAGER_LASTNAME
		]);
		$this->addInput($input);
	}
}
