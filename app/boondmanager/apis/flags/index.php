<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Flags;

use BoondManager\APIs\Flags\Specifications\HaveCreateAccess;
use BoondManager\APIs\Flags\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use Wish\Models\Model;
use BoondManager\Services\BM;
use BoondManager\Services\Flags;
use BoondManager\APIs\Flags\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\Flags
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'name',
		'mainManager' => [
			'id', 'lastName', 'firstName'
		]
	];

	/**
	 * Search flags
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$filter = new Filters\SearchFlags();
		$filter->setData($this->requestAccess->getParams());
		$this->checkFilter($filter);

		$result = Flags::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a flag
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default flag
		$flag = Flags::buildFromFilter($filter);

		if(!$flag) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $flag);

		if(Flags::create($flag)) {
			$this->sendJSONResponse([
				'data' => $flag->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
