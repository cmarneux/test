<?php
/**
 * abstractflag.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Flags\Specifications;

use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Models\Flag;
use BoondManager\Lib\RequestAccess;

abstract class AbstractFlag extends AbstractSpecificationItem {
	/**
	 * Get the flag from the request
	 * @param RequestAccess $request
	 * @return Flag|null
	 */
	public function getFlag($request) {
		if($request->data instanceof Flag) return $request->data;
		else return null;
	}

	/**
	 * Get read & write access
	 * @param CurrentUser $user
	 * @param Flag $flag
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($user, $flag) {
		list($read, $write) = [false, false, false];

		$hierarchyAccess = $user->checkHierarchyAccess(
			[0, 0, $flag->getManagerID()],
			BM::MODULE_FLAGS
		);

		switch ($hierarchyAccess) {
			case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY:case BM::PROFIL_ACCESS_READ_WRITE:
				$read = true;
				$write = true;
				break;
		}

		return [$read, $write];
	}
}
