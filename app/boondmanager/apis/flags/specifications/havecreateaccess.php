<?php
/**
 * HaveCreateAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Flags\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveCreateAccess
 * @package BoondManager\APIs\Flags\Specifications
 */
class HaveCreateAccess extends AbstractFlag{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		//Check this API authorization & access
		if(!$user->isManager() || !$user->hasAccess(BM::MODULE_FLAGS)) return false;

		//Check this API access (cf. V6 => listeflags.php => isAccessible & majView)
		return (new HaveWriteAccess())->isSatisfiedBy($request);
	}
}
