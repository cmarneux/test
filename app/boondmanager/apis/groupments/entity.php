<?php
/**
 * entity.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Groupments;

use BoondManager\APIs\Groupments\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Groupments\Specifications\HaveReadAccess;
use BoondManager\APIs\Groupments\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Services\BM;

class Entity extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'title',
		'state',
		'averageDailyPriceExcludingTax',
		'averageDailyCost',
		'averageDailyContractCost',
		'numberOfDaysInvoicedOrQuantity',
		'informationComments',
		'turnoverSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'profitabilitySimulated',
		'loadDistribution',
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency',
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			],
			'opportunity' => [
				'id',
				'reference',
				'title'
			],
			'agency' => [
				'id',
				'name',
				'exchangeRate',
				'currency',
				'allowAdvantagesOnProjects',
				'allowExceptionalScalesOnProjects'
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name',
				'exceptionalScaleTypes' => [ // pas sur
					'reference',
					'name',
					'exceptionalRules' => [
						'reference',
						'name',
						'priceExcludingTaxOrPriceRate',
						'grossCostOrSalaryRate'
					]
				]
			],
			'technical' => [
				'id',
				'lastName',
				'firstName',
				'company' => [
					'id',
					'name'
				]
			],
			'deliveries' => [
				'id',
				'title',
				'dependsOn' => [
					'id',
					'lastName',
					'firstName',
					'typeOf'
				]
			],
		],
		'deliveries' => [
			'delivery' => [
				'id'
			],
			'weighting',
			'schedule'
		],
		'files' => [
			'id',
			'name'
		]
	];

	public function api_get() {
		$groupment = Services\Groupments::get($this->requestAccess->id);
		if(!$groupment) $this->error(403);

		$this->checkAccessWithSpec( new HaveReadAccess(), $groupment);

		$outputData = [
			'data' => $groupment->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($outputData);
	}

	public function api_put() {

		$groupment = Services\Groupments::get($this->requestAccess->id);
		if(!$groupment) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $groupment);

		//Build filters
		$filter = new Filters\Entity($groupment);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Services\Groupments::buildFromFilter($filter, $groupment);

		if(Services\Groupments::update($groupment) ) {
			$this->sendJSONResponse([
				'data' => $groupment->filterFields(self::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}

	public function api_delete() {
		$delivery = Services\Groupments::get($this->requestAccess->id);
		if(!$delivery) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $delivery);

		$this->sendJSONResponse([
			'data' => [
				'success' => Services\Groupments::delete($delivery->id)
			]
		]);
	}
}
