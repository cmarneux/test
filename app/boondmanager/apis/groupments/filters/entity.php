<?php
/**
 * entity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Groupments\Filters;

use BoondManager\Models\Delivery;
use BoondManager\Models\Groupment;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Entity
 * @property InputString title
 * @property InputValue state
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputFloat averageDailyPriceExcludingTax
 * @property InputFloat averageDailyCost
 * @property InputFloat numberOfDaysInvoicedOrQuantity
 * @property InputFloat numberOfDaysFree
 * @property InputString informationComments
 * @property InputValue loadDistribution
 * @property InputMultiObjects deliveries
 * @property Relationships\Project project
 * @package BoondManager\APIs\Groupments\Filters
 */
class Entity extends AbstractJsonAPI {

	protected $_objectClass = Groupment::class;

	/**
	 * @var Groupment
	 */
	protected $_groupment;

	/**
	 * Entity constructor.
	 * @param Groupment|null $groupment
	 */
	public function __construct(Groupment $groupment = null)
	{
		parent::__construct();

		$this->_groupment = $groupment;

		$input = new InputString('title');
		$input->setMaxLength(100);
		$this->addInput($input);

		$this->addInput( new InputDate('startDate') );
		$this->addInput( new InputDate('endDate') );

		$this->addInput(new InputFloat('averageDailyPriceExcludingTax'));
		$this->addInput(new InputFloat('averageDailyCost'));
		$this->addInput(new InputFloat('numberOfDaysInvoicedOrQuantity', 0));
		$this->addInput(new InputFloat('numberOfDaysFree', 0));

		$input = new InputString('informationComments');
		$input->setMaxLength(4000);
		$this->addInput($input);

		$input = new InputEnum('loadDistribution');
		$input->setModeDefaultValue(Groupment::TYPE_PROPORTIONAL);
		$input->setAllowedValues(Groupment::DISTRIBUTION_MAPPER);
		$this->addInput($input);

		$template = new Attributes\GroupmentDelivery($this->_groupment, $this->isCreation());
		$input = new InputMultiObjects('deliveries', $template);
		$this->addInput($input);

		$project = new Relationships\Project();
		$this->addInput($project);

		if($this->isCreation())
			$this->adaptForCreation();
		else
			$this->adaptForEdition();
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return !$this->_groupment || !$this->_groupment->id;
	}

	/**
	 * Set inputs for an update
	 */
	private function adaptForEdition() {
		$this->project->setDisabled(true);
	}

	private function adaptForCreation() {
		$this->project->setRequired(true);
		$this->title->setRequired(true);
		$this->loadDistribution->markDefaultValueAsDefined(true);
	}

	public function getProject() {
		return ($this->isCreation()) ? $this->project->getValue() : $this->_groupment->project;
	}

	private function totalDeliveries() {
		if($this->deliveries->isDefined()) return $this->deliveries->count();
		else if ($this->_groupment) return count($this->_groupment->deliveries);
		else return 0;
	}

	private function hasDeliveries(){
		return $this->totalDeliveries() != 0;
	}

	protected function postValidation(){
		if($this->hasDeliveries()) {
			// dates are automatically calculated from deliveries in groupment
			if($this->startDate->isDefined()) $this->startDate->invalidateIfDebug(Groupment::ERROR_GROUPMENT_DATE_CANT_BE_SET_IF_SUB_DELIVERIES);
			if($this->endDate->isDefined()) $this->endDate->invalidateIfDebug(Groupment::ERROR_GROUPMENT_DATE_CANT_BE_SET_IF_SUB_DELIVERIES);
		} else {
			// dates can be set manually because there is no deliveries in the groupment
			$startDate = $this->startDate->isDefined() ? $this->startDate->getDateTime() : \DateTime::createFromFormat($this->_groupment->startDate, 'Y-m-d');
			$endDate = $this->endDate->isDefined() ? $this->endDate->getDateTime() : \DateTime::createFromFormat($this->_groupment->endDate, 'Y-m-d');

			if($startDate->getTimestamp() > $endDate->getTimestamp()) {
				$this->startDate->invalidate(Groupment::ERROR_GROUPMENT_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE);
				$this->endDate->invalidate(Groupment::ERROR_GROUPMENT_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE);
			}
		}

		foreach($this->deliveries->getItems() as $delivery){
			/** @var Attributes\GroupmentDelivery $delivery */
			/** @var Delivery $del */
			$del = $delivery->delivery->getValue();

			// the delivery is not related to the groupment project
			if($del->project->id != $this->getProject()->id){
				$delivery->delivery->invalidate(Groupment::ERROR_GROUPMENT_DELIVERY_MUST_BELONGS_TO_SAME_PROJECT);
				continue;
			}

			// the delivery is already related to a groupment, then it must be the current groupment (edition only)
			if($del->groupment) {
				if($this->isCreation() || $del->groupment->id != $this->_groupment->id) {
					$delivery->delivery->invalidate(Groupment::ERROR_GROUPMENT_DELIVERY_MUST_BE_FREE_OF_GROUPMENT);
					continue;
				}
			}

			switch ($this->loadDistribution->getValue()) {
				case Groupment::TYPE_PROPORTIONAL:
					if($delivery->weighting->isDefined()) $delivery->weighting->invalidateIfDebug(Groupment::ERROR_GROUPMENT_WEIGHTING_CANT_BE_SET);
					if($delivery->schedule->isDefined()) $delivery->schedule->invalidateIfDebug(Groupment::ERROR_GROUPMENT_SCHEDULE_CANT_BE_SET);
					break;
				case Groupment::TYPE_WEIGHTED :
					if($delivery->schedule->isDefined()) $delivery->schedule->invalidateIfDebug(Groupment::ERROR_GROUPMENT_SCHEDULE_CANT_BE_SET);
					break;
				default : // MANUAL
					if($delivery->weighting->isDefined()) $delivery->weighting->invalidateIfDebug(Groupment::ERROR_GROUPMENT_WEIGHTING_CANT_BE_SET);
			}
		}
	}
}
