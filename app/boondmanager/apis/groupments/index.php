<?php
/**
 * deliveries.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Groupments;

use BoondManager\APIs\Groupments\Specifications\HaveCreateAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Groupments\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\Groupments
 */
class Index extends AbstractController {

	/**
	 * Create a delivery
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default project
		$groupment = Services\Groupments::buildFromFilter($filter);
		if(!$groupment) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $groupment);

		if( Services\Groupments::create($groupment) ) {
			$this->sendJSONResponse([
				'data' => $groupment
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
