<?php
/**
 * abstractgroupment.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Groupments\Specifications;

use BoondManager\Models\Groupment;
use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Delivery;
use BoondManager\Lib\RequestAccess;

abstract class AbstractGroupment extends AbstractSpecificationItem {
	/**
	 * Get the project from the request
	 * @param RequestAccess $request
	 * @return Groupment|null
	 */
	public function getGroupment($request) {
		if($request->data instanceof Groupment) return $request->data;
		else return null;
	}

	/**
	 * Get read & write access
	 * @param string $tab
	 * @param CurrentUser $user
	 * @param Delivery $delivery
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($tab, $user, $delivery) {
		list($state, $read, $write) = [false, false, false];

		return [$read, $write];
	}
}
