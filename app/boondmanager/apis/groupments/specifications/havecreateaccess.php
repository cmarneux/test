<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Groupments\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveCreateAccess extends AbstractGroupment{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();
		$groupment = $this->getGroupment($object);

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$req = clone $object;
		$req->setData($groupment->project);

		$projectCreation = (new \BoondManager\APIs\Projects\Specifications\HaveCreateAccess())->isSatisfiedBy($req);

		return $projectCreation && in_array($groupment->project->mode, [BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_TA]);
	}
}
