<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Groupments\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveDeleteAccess extends AbstractGroupment{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$delivery = $this->getGroupment($object);

		return (new HaveWriteAccess())->isSatisfiedBy($object) && !$delivery->project->isInactivity();
	}
}
