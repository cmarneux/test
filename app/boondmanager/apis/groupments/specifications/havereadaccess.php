<?php
/**
 * havereadaccess.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Groupments\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveReadAccess extends AbstractGroupment{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$delivery = $this->getGroupment($object);
		$user = $object->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		return ($delivery->project->mode != BM::PROJECT_TYPE_RECRUITMENT) && (in_array($user->checkHierarchyAccess($delivery->project, BM::MODULE_PROJECTS), [BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY, BM::PROFIL_ACCESS_READ_WRITE]));
	}
}
