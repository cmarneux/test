<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Groupments\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;


/**
 * Class IsActionAllowed
 * @package BoondManager\APIs\Deliveries\Specifications
 */
class IsActionAllowed extends AbstractGroupment
{
	const AVAILABLE_ACTIONS = ['share'];

	private $action;

	/**
	 * IsActionAllowed constructor.
	 * @param string $action
	 */
	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		//cf. V6 => ficheprojet.php => majView
		$delivery = $this->getGroupment($request);
		if(!$delivery) return false;

		$allow = false;
		switch($this->action) {
			case 'share':
				$allow = $user->isManager() && (new HaveReadAccess())->isSatisfiedBy($request);
				break;
		}
		return $allow;
	}
}
