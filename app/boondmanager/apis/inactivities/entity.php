<?php
/**
 * entity.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities;

use BoondManager\APIs\Inactivities\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Inactivities\Specifications\HaveReadAccess;
use BoondManager\APIs\Inactivities\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Services\BM;

class Entity extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'title',
		'state',
		'averageDailyPriceExcludingTax',
		'averageDailyCost',
		'averageDailyContractCost',
		'numberOfDaysInvoicedOrQuantity',
		'informationComments',
		'costsSimulatedExcludingTax',
		'occupationRate',
		'dailyExpenses',
		'monthlyExpenses',
		'numberOfWorkingDays',
		'expensesDetails' => [
			'id',
			'expenseType' => [
				'reference',
				'name'
			],
			'periodicity',
			'netAmount',
			'agency' => [
				'id',
				'name',
				'expenseTypes' => [
					'reference',
					'name',
					'taxRate'
				]
			]
		],
		'inactivityType',
		'resource' => [
			'id',
			'firstName',
			'lastName',
			'typeOf',
			'agency' => [
				'id',
				'name',
				'calendar',
				'expenseTypes' => [
					'reference',
					'name',
					'taxRate'
				],
				'advantageTypes' => [
					'reference',
					'name',
					'frequency',
					'category',
					'participationQuota',
					'agencyQuota',
					'employeeQuota'
				],
				'exceptionalScaleTypes' => [
					'reference',
					'name',
					'exceptionalRules' => [
						'reference',
						'name',
						'priceExcludingTaxOrPriceRate',
						'grossCostOrSalaryRate'
					]
				]
			]
		],
		'contract' => [
			'id',
			'contractAverageDailyCost',
			'monthlySalary',
			'dailyExpenses',
			'monthlyExpenses',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency'
		],
		'contracts' => [
			'id',
			'contractAverageDailyCost',
			'monthlySalary',
			'dailyExpenses',
			'monthlyExpenses',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency'
		],
		'files' => [
			'id',
			'name'
		]
	];

	public function api_get() {
		$delivery = Services\Inactivities::get($this->requestAccess->id);
		if(!$delivery) $this->error(403);

		$this->checkAccessWithSpec( new HaveReadAccess(), $delivery);

		$outputData = [
			'data' => $delivery->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($outputData);
	}

	public function api_put() {

		$delivery = Services\Inactivities::get($this->requestAccess->id);
		if(!$delivery) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $delivery);

		//Build filters
		$filter = new Filters\Entity($delivery);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Services\Inactivities::buildFromFilter($filter, $delivery);

		if(Services\Inactivities::update($delivery) ) {
			$this->sendJSONResponse([
				'data' => $delivery->filterFields(self::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}

	public function api_delete() {
		$delivery = Services\Inactivities::get($this->requestAccess->id);
		if(!$delivery) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $delivery); // TODO

		$this->sendJSONResponse([
			'data' => [
				'success' => Services\Inactivities::delete($delivery->id)
			]
		]);
	}
}
