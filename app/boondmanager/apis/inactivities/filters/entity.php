<?php
/**
 * entity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities\Filters;

use BoondManager\APIs\Inactivities\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Agency;
use BoondManager\Models\Contract;
use BoondManager\Models\Delivery;
use BoondManager\Models\ExpenseDetail;
use BoondManager\Models\Inactivity;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Services;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Lib\Filters\Inputs\Attributes;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Entity
 *
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputString title
 * @property InputValue state
 * @property InputFloat averageDailyContractCost
 * @property InputFloat numberOfDaysInvoicedOrQuantity
 * @property InputInt numberOfWorkingDays
 * @property InputString informationComments
 * @property InputFloat dailyExpenses
 * @property InputFloat monthlyExpenses
 * @property InputValue inactivityType
 * @property InputMultiObjects expensesDetails
 * @property Relationships\Contract contract
 * @property Relationships\Employee resource
 *
 * @method Contract|null getContract
 *
 * @package BoondManager\APIs\Deliveries\Filters
 */
class Entity extends AbstractJsonAPI {

	protected $_objectClass = Delivery::class;

	/**
	 * @var Inactivity
	 */
	protected $_inactivity;

	/**
	 * @var Agency
	 */
	protected $_agency;

	/**
	 * Entity constructor.
	 * @param Inactivity|null $inactivity
	 */
	public function __construct(Inactivity $inactivity = null)
	{
		parent::__construct();

		$this->_inactivity = $inactivity;

		$this->addInput( (new InputDate('startDate')) );
		$this->addInput( (new InputDate('endDate')) );

		$input = new InputString('title');
		$input->setMaxLength(100);
		$this->addInput($input);

		$this->addInput(new InputDict('state', 'specific.setting.state.delivery'));

		$this->addInput( new InputFloat('averageDailyContractCost') );
		$this->addInput( new InputFloat('numberOfDaysInvoicedOrQuantity') );

		$input = new InputInt('numberOfWorkingDays');
		$input->setMin(0)->setMax(365);
		$this->addInput( $input );

		$input = new InputString('informationComments');
		$input->setMaxLength(4000);
		$this->addInput($input);

		$this->addInput(new InputFloat('dailyExpenses'));
		$this->addInput(new InputFloat('monthlyExpenses'));

		$input = new InputEnum('inactivityType');
		$input->setAllowedValues(Inactivity::INACTIVITY_MAPPER);
		$this->addInput( $input );

		$template = new Attributes\ExpensesDetails();
		$template->setAllowedDetails($inactivity ? $inactivity->expensesDetails : []);
		$template->agency->addFilterCallback(function($value){
			return new Agency(['id' => $value]);
		});
		$input = new InputMultiObjects('expensesDetails', $template);
		$this->addInput($input);

		$input = new Relationships\Contract();
		$input->setAllowEmptyValue(true);
		$this->addInput( $input );

		$input = new Relationships\Employee();
		$this->addInput($input);

		if($this->isCreation())
			$this->adaptForCreation();
		else
			$this->adaptForEdition();
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return !$this->_inactivity || !$this->_inactivity->id;
	}

	private function isEdition() {
		return ! $this->isCreation();
	}

	/**
	 * Set inputs for an update
	 */
	private function adaptForEdition() {
		if($this->resource->isDefined())
			$this->resource->invalidateIfDebug(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);

		$this->inactivityType->setModeDefaultValue(Inactivity::INACTIVITY_INTERNAL, $defined = true);
	}

	private function adaptForCreation() {

		$this->startDate->markDefaultValueAsDefined(true);
		$this->endDate->markDefaultValueAsDefined(true);

		$this->numberOfWorkingDays->setModeDefaultValue(Services\CurrentUser::instance()->getNumberOfWorkingDays(), $defined = true);

		$this->state->setModeDefaultValue(Delivery::STATE_SIGNED);

		$this->resource->addFilterCallback(function($entity){
			$entity->agency = Services\Agencies::get($entity->agency->id, Agency::TAB_ACTIVITYEXPENSESRESOURCES);
			return $entity;
		});
	}

	public function getEmployee() {
		return $this->isCreation() ? $this->resource->getValue() : $this->_inactivity->resource;
	}

	/**
	 * @return Agency
	 */
	public function getEmployeeAgency(){
		return $this->getEmployee()->agency;
	}

	protected function postValidation() {

		$request = new RequestAccess();
		$request->setUser( Services\CurrentUser::instance() );
		$request->setData( $this->toObject() ); // useless, but just to make things right

		foreach(HaveWriteAccessOnField::FIELD_LIST as $field) {
			if($this->$field->isDefined() && !(new HaveWriteAccessOnField($field))->isSatisfiedBy($request)) {
				$this->$field->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);
			}
		}

		if($this->getContract() && $this->averageDailyContractCost->isDefined() ) {
				$this->averageDailyContractCost->invalidateIfDebug(Delivery::ERROR_DELIVERY_AVERAGEDAILYCONTRACTCOST_CANNOT_BE_SET);
		}

		if($this->startDate->getTimestamp() > $this->endDate->getTimestamp()) {
			$this->startDate->invalidateIfDebug(Delivery::ERROR_DELIVERY_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE);
			$this->endDate->invalidateIfDebug(Delivery::ERROR_DELIVERY_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE);
		}

		$employeeAgency = $this->getEmployeeAgency();

		/** @var Agency[] $expensesDetailsAgencies list of valid agencies for expensesDetails */
		$expensesDetailsAgencies                        = [];
		$expensesDetailsAgencies[ $employeeAgency->id ] = $employeeAgency;

		if ($this->isEdition()) {
			foreach ($this->_inactivity->expensesDetails as $expenseDetail) {
				$expensesDetailsAgencies[ $expenseDetail->agency->id ] = $expenseDetail->agency;
			}
		}

		foreach ($this->expensesDetails->getItems() as $expenseInput) {
			/** @var Attributes\ExpensesDetails $expenseInput */

			$found = false;
			if (array_key_exists($expenseInput->agency->getValue()->id, $expensesDetailsAgencies)) {
				$agency = $expensesDetailsAgencies[ $expenseInput->agency->getValue()->id ];
				foreach ($agency->expenseTypes as $et) {
					if ($et->reference == $expenseInput->expenseType->reference) {
						$found = true;
						break;
					}
				}
			}
			if (!$found)
				// the reference doesn't belong to a valid agency
				$expenseInput->expenseType->reference->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}
	}

	/**
	 * magic method to retrieve a field value or the value from the object
	 * @param string $name
	 * @param array $arguments
	 * @throws \Exception
	 */
	public function __call($name, array $arguments) {
		if(strlen($name > 3) && substr($name, 0, 3) == 'get'){
			$this->getFieldIfDefined( lcfirst(substr($name, 3)) );
		} else {
			throw new \Exception("undefined method '$name'");
		}
	}

	private function getFieldIfDefined($field) {
		return $this->$field->isDefined() ? $this->$field->getValue() : $this->_inactivity->$field;
	}
}
