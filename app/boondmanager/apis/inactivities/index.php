<?php
/**
 * deliveries.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Inactivities;

use BoondManager\APIs\Inactivities\Specifications\HaveCreateAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Employee;
use BoondManager\Models;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Deliveries\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\Inactivities
 */
class Index extends AbstractController {

	/**
	 * Create a delivery
	 */
	public function api_post() {
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default project
		$delivery = Services\Deliveries::buildFromFilter($filter);
		if(!$delivery) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $delivery);

		if( Services\Inactivities::create($delivery) ) {
			$this->sendJSONResponse([
				'data' => $delivery
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
