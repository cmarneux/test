<?php
/**
 * abstractinactivity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Inactivities\Specifications;

use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Inactivity;
use BoondManager\Lib\RequestAccess;

abstract class AbstractInactivity extends AbstractSpecificationItem {
	/**
	 * Get the project from the request
	 * @param RequestAccess $request
	 * @return Inactivity|null
	 */
	public function getDelivery($request) {
		if($request->data instanceof Inactivity) return $request->data;
		else return null;
	}

	/**
	 * Get read & write access
	 * @param string $tab
	 * @param CurrentUser $user
	 * @param Inactivity $delivery
	 * @return array [$read, $write]
	 */
	protected function getReadWriteAccess($tab, $user, $delivery) {
		list($state, $read, $write) = [false, false, false];

		return [$read, $write];
	}
}
