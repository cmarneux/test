<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveCreateAccess extends AbstractInactivity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$projectCreation = (new \BoondManager\APIs\Projects\Specifications\HaveCreateAccess())->isSatisfiedBy($object);

		return $projectCreation ;
	}
}
