<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveDeleteAccess extends AbstractInactivity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$delivery = $this->getDelivery($object);

		if($delivery->isGroupment) {
			return (new HaveWriteAccess())->isSatisfiedBy($object) && !$delivery->project->isInactivity();
		} else {
			(new HaveWriteAccess())->isSatisfiedBy($object);
		}
	}
}
