<?php
/**
 * havereadaccess.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Employee;
use BoondManager\Services\BM;

class HaveReadAccess extends AbstractInactivity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$delivery = $this->getDelivery($object);
		$user = $object->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		if(!$user->hasAccess(BM::MODULE_RESOURCES)) return false;

		if($delivery->resource instanceof Employee) {
			$myProfil = $delivery->resource->id == $user->getEmployeeId();
			if($delivery->resource->visibility || $myProfil || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) ) {
				switch ($user->checkHierarchyAccess($delivery->resource, BM::MODULE_RESOURCES) ){
					case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY: return true;
					case BM::PROFIL_ACCESS_READ_WRITE:
					case BM::PROFIL_ACCESS_READ_ONLY:
						return (
							$myProfil && !$delivery->resource->mainManager
							|| (
								$user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)
								&& (
									$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
									|| !$delivery->resource->isManager()
									|| $user->isMyManager($delivery->resource->id)
								)
							)
						);
				}
			}
		}

		return false;
	}
}
