<?php
/**
 * havewriteaccess.php
 * @author  Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities\Specifications;

use BoondManager\Lib\RequestAccess;

class HaveWriteAccess extends AbstractInactivity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$read = (new HaveReadAccess())->isSatisfiedBy($object);

		return $read;
	}
}
