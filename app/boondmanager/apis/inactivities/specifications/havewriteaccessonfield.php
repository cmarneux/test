<?php
/**
 * userhavewriteaccessonfield.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Inactivities\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Delivery;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractInactivity {

	const FIELD_LIST = [
		'numberOfWorkingDays', 'expensesDetails', 'averayDailyContractCost', 'contract',
		'dailyExpenses', 'monthlyExpenses'
	];

	private $field;

	public function __construct($field) {
		$this->field = $field;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request)
	{
		/** @var CurrentUser $user*/
		$user = $request->user;

		if($user->isGod()) return true;

		$write = false;

		switch($this->field){
			case 'numberOfWorkingDays':
			case 'expensesDetails':
			case 'averayDailyContractCost':
			case 'contract':
			case 'dailyExpenses':
			case 'monthlyExpenses':
				// TODO check right for inactivity
				$write = $user->hasRight(BM::RIGHT_ACCESS_ADMINISTRATIVE, BM::MODULE_PROJECTS);
				break;
		}

		return $write;
	}
}
