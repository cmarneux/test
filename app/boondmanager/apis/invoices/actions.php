<?php
/**
 * actions.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Invoices;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Invoice;
use BoondManager\Services;
use BoondManager\APIs\Invoices\Specifications\HaveReadAccess;

/**
 * Class Actions
 * @package BoondManager\APIs\Projects
 */
class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get project's actions
	 */
	public function api_get() {
		$order = Services\Invoices::get($this->requestAccess->id, Invoice::TAB_ACTIONS);
		if(!$order) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Invoice::TAB_ACTIONS), $order);

		$filter = new SearchActions();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($order->getReference());

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
