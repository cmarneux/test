<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Invoices;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Invoice;
use BoondManager\Services;
use BoondManager\APIs\Invoices\Specifications\HaveReadAccess;
use Wish\Models\Model;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$order = null;

		$order = Services\Invoices::get($this->requestAccess->id, Invoice::TAB_FLAGS);
		if(!$order) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Invoice::TAB_FLAGS), $order);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($order);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
