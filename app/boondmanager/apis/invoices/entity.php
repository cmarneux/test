<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Invoices;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Invoices;
use BoondManager\APIs\Invoices\Specifications\HaveReadAccess;

class Entity extends AbstractController{

	const DELIVERIES_FIELDS = [
		'id',
		'title',
		'startDate',
		'endDate',
		'dependsOn' => [
			'id',
			// employee
			'lastName',
			'firstName',
			// product
			'name'
		]
	];
	const PURCHASES_FIELDS = [
		'id',
		'title',
		'reference',
		'startDate',
		'endDate'
	];

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'reference',
		'order' => [
			'id',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency',
			'project' => [
				'id',
				'agency' => [
					'id',
					'name'
				],
				'pole' => [
					'id',
					'name'
				]
			],
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			]
		]
	];

	public function api_get() {

		$entity = Invoices::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(BM::TAB_DEFAULT), $entity);

		$this->sendJSONResponse([
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		]);
	}
}
