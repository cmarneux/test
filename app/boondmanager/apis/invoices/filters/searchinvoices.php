<?php
/**
 * invoices.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Invoices\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;

/**
 * Class SearchInvoices
 * @property InputMultiValues paymentMethods
 * @property InputMultiValues states
 * @property InputBoolean closing
 * @property InputBoolean creditNote
 * @property InputMultiValues projectTypes
 * @property InputString period
 * @property InputMultiValues sort
 * @property InputDate startDate
 * @property InputDate endDate
 * @package BoondManager\APIs\Invoices\Filters
 */
class SearchInvoices extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_BILLING;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_EXPECTEDPAYMENT = 'expectedPayment',
		PERIOD_PERFORMEDPAYMENT = 'performedPayment',
		PERIOD_PERIOD = 'period';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_TURNOVERINCLUDEDTAX = 'turnoverIncludedTax',
		ORDERBY_TURNOVEREXCLUDEDTAX = 'turnoverExcludedTax',
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_EXPECTEDPAYMENTDATE= 'expectedPaymentDate',
		ORDERBY_STATE = 'state',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_ORDER_REFERENCE= 'order.reference',
		ORDERBY_ORDER_PROJECT_REFERENCE= 'order.project.reference',
		ORDERBY_ORDER_PROJECT_COMPANY_NAME= 'order.project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * SearchInvoices constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addInput( new InputMultiDict('paymentMethods', 'specific.setting.paymentMethod'));
		$this->addInput( new InputMultiDict('states', 'specific.setting.state.invoice') );
		$this->addInput( new InputBoolean('closing'));
		$this->addInput( new InputBoolean('creditNote'));
		$this->addInput( new InputMultiDict('projectTypes', 'specific.setting.typeOf.project'));

		$input = new InputMultiEnum('period');
		$input->setModeDefaultValue( BM::INDIFFERENT );
		$input->setAllowedValues([self::PERIOD_CREATED, self::PERIOD_EXPECTEDPAYMENT, self::PERIOD_PERFORMEDPAYMENT, self::PERIOD_PERIOD]);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([self::ORDERBY_CREATIONDATE, self::ORDERBY_EXPECTEDPAYMENTDATE, self::ORDERBY_MAINMANAGER_LASTNAME,
			self::ORDERBY_ORDER_PROJECT_COMPANY_NAME, self::ORDERBY_ORDER_PROJECT_REFERENCE, self::ORDERBY_ORDER_REFERENCE,
			self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_TURNOVEREXCLUDEDTAX, self::ORDERBY_TURNOVERINCLUDEDTAX]);
		$this->addInput($input);

		$input = new InputDate('startDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputDate('endDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);
	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
