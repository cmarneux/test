<?php
/**
 * invoices.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Invoices;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\Invoices\Specifications\HaveCreateAccess;
use BoondManager\APIs\Invoices\Specifications\HaveSearchAccess;
use BoondManager\Services\Invoices;
use BoondManager\Services\Extraction;
use BoondManager\APIs\Invoices\Filters;
use Wish\Models\Model;

/**
 * Invoices list controller.
 * @package Invoices
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'expectedPaymentDate',
		'turnoverInvoicedExcludingTax',
		'turnoverInvoicedIncludingTax',
		'paymentMethod',
		'creditNote',
		'reference',
		'state',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'order' => [
			'id',
			'number',
			'reference',
			'mainManager' => [
				'id',
				'firstName',
				'lastName',
			],
			'project' => [
				'id',
				'reference',
				'typeOf',
				'mode',
				'opportunity' => [
					'id',
					'title',
				],
				'contact' => [
					'id',
					'firstName',
					'lastName',
				],
				'company' => [
					'id',
					'name',
				],
			],
		],
		'termOfPayment' => [
			'id',
			'date',
			'title',
		],
	];

	/**
	 * Search invoices
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchInvoices();
		$filter->setAndFilterData($this->requestAccess->getParams());
		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			die('TODO : Update extract function');
			$service = new Extraction\Invoices('invoices.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Invoices::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverExcludingTax' => $result->turnoverExcludingTax,
					'turnoverIncludingTax' => $result->turnoverIncludingTax,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create an invoice
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Information();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default invoice
		$invoice = Invoices::buildFromFilter($filter);
		if(!$invoice) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $invoice);

		if(Invoices::create($invoice))
			$this->sendJSONResponse([
				'data' => $invoice->filterFields(Information::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
