<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Invoices;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use BoondManager\Services\Invoices;
use BoondManager\APIs\Invoices\Specifications\HaveReadAccess;

class Information extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'startDate',
		'endDate',
		'reference',
		'expectedPaymentDate',
		'state',
		'performedPaymentDate',
		'paymentMethod',
		'informationComments',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'discountRate',
		'closed',
		'activityDetails' => [
			'resource' => [
				'id',
				'firstName',
				'lastName',
				'regularTimes' => [
					'workUnitType' => [
						'reference',
						'activityType',
						'name'
					],
					'agency' => [
						'id',
						'name'
					],
					'averageDailyCost',
					'numberOfWorkingDays'
				],
				'exceptionalTimes' => [
					'workUnitType' => [
						'reference',
						'activitiyType',
						'name'
					],
					'agency' => [
						'id',
						'name'
					],
					'priceExcludingTax',
					'duration'
				],
				'exceptionalCalendars' => [
					'workUnitType' => [
						'reference',
						'activityType',
						'name'
					],
					'agency' => [
						'id',
						'name'
					],
					'priceExcludingTax',
					'duration'
				],
				'timesReports' => [
					'id',
					'term'
				],
				'expensesReports' => [
					'id',
					'term'
				],
			]
		],
		'showCommentsOnPDF',
		'invoiceRecords' => [
			'id',
			'invoiceRecordType' => [
				'reference',
			],
			'description',
			'amountExcludingTax',
			'quantity',
			'turnoverExcludingTax',
			'turnoverIncludingTax',
			'taxRate',
		],
		'order' => [
			'id',
			'reference',
			'number',
			'legals',
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			],
			'project' => [
				'id',
				'reference',
				'typeOf',
				'mode',
				'currency',
				'exchangeRate',
				'currencyAgency',
				'exchangeRateAgency',
				'agency' => [
					'id',
					'name',
					'invoicesLockingStates',
					'email1',
					'phone1',
					'registrationNumber',
					'legalStatus',
					'registeredOffice',
					'vatNumber',
					'address',
					'postcode',
					'town',
					'country',
					'apeCode'
				],
				'pole' => [
					'id',
					'name',
				],
				'company' => [
					'id',
					'name',
					'vatNumber',
				],
				'contact' => [
					'id',
					'lastName',
					'firstName'
				],
				'opportunity' => [
					'id',
					'title',
					'reference'
				]
			],
			'billingDetail' => [
				'id',
				'name',
				'contact',
				'phone1',
				'email1',
				'email2',
				'email3',
				'address1',
				'address2',
				'address3',
				'postcode',
				'town',
				'country',
				'state'
			],
			'factor' => [
				'id',
				'name',
				'registrationNumber',
				'legalStatus',
				'registeredOffice',
				'vatNumber',
				'address',
				'postcode',
				'town',
				'country',
				'apeCode'
			],
			'bankDetail' => [
				'id',
				'description',
				'iban',
				'bic'
			],
		],
		'schedule' => [
			'id',
			'date',
			'title'
		]
	];

	public function api_get() {

		$entity = Invoices::get($this->requestAccess->id, Order::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Order::TAB_INFORMATION), $entity);

		$this->sendJSONResponse([
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		]);
	}
}
