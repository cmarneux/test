<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Invoices;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Invoices;

/**
 * Class Rights
 * @package BoondManager\APIs\Invoices
 */
class Rights extends AbstractController {
	/**
	 * Get Invoice's rights
	 */
	public function api_get() {
		$order = Invoices::get($this->requestAccess->id);
		if(!$order) $this->error(404);

		$this->sendJSONResponse([
			'data' => Invoices::getRights($order)
		]);
	}
}
