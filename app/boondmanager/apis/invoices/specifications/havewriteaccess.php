<?php
/**
 * havewriteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */


namespace BoondManager\APIs\Invoices\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;


/**
 * Class CanCreateResource
 *
 * Indicate if the user have the right to road a Resource
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveWriteAccess extends AbstractInvoice {

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();
		$invoice = $this->getInvoice($object);

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$hierarchy = $user->checkHierarchyAccess($invoice, BM::MODULE_BILLING );

		return $hierarchy == BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY
		    || $hierarchy == BM::PROFIL_ACCESS_READ_WRITE && $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_BILLING);
	}
}
