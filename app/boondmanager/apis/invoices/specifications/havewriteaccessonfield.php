<?php
/**
 * havewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Invoices\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveWriteAccessOnField extends AbstractInvoice {

	const FIELDS = ['date', 'reference'];

	private $field;

	public function __construct($field) {
		$this->field = $field;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;
		$order = $this->getInvoice($request);
		if(!$order) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->field){
			case 'reference':
				$write = $user->hasRight(BM::RIGHT_EDIT_BDC_REFERENCES, BM::MODULE_BILLING);
				break;
			case 'date':
				$write = $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_BILLING);
				break;
			default:
				$write = false;
		}
		return $write;
	}
}
