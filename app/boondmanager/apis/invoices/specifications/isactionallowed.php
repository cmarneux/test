<?php
/**
 * isactionallowed.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Invoices\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Invoice;

class IsActionAllowed extends AbstractInvoice {

	const ALLOWED_ACTIONS = ['share', 'addAction', 'downloadClientPDF', 'sendClientMail'];

	private $action;

	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;
		$order = $this->getInvoice($request);
		if(!$order) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->action){
			case 'share':
				$allow = true;
				break;
			case 'addAction':
				$allow = (new HaveWriteAccess(Invoice::TAB_ACTIONS))->isSatisfiedBy($request);
				break;
			case 'downloadClientPDF':
				$allow = true;
				break;
			case 'sendClinentMail':
				$allow = true;
				break;
			default:
				$allow = false;
				break;
		}
		return $allow;
	}
}
