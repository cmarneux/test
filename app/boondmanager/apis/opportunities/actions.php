<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Opportunity;
use BoondManager\Services;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;

class Actions extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {
		// get an existing entity
		$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_ACTIONS);
		// if profil not found, throw an error
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error)
		$this->checkAccessWithSpec( new HaveReadAccess(Opportunity::TAB_ACTIONS), $entity );

		// init filter
		$filter = new SearchActions();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchActions::ORDERBY_DATE);
		$filter->order->setDefaultValue(SearchActions::ORDER_DESC);
		$filter->setAvailableCategories([BM::CATEGORY_OPPORTUNITY]);

		// reading input data
		$filter->setData($this->requestAccess->getParams());

		// overwriting keywords to restrain the research
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
