<?php
/**
 * opportunies.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Contact;
use BoondManager\Models\Opportunity;
use BoondManager\Services;
use BoondManager\OldModels\Specifications\RequestAccess\Opportunities\HaveDeleteAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveCreateAccess;

class Entity extends AbstractController {
	public function api_get() {
		$ALLOWED_FIELDS = [
			'id',
			'creationDate',
			'typeOf',
			'mainManager' => ['id'],
			'agency' => ['id'],
			'pole' => ['id'],
		];

		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_DEFAULT);
			// if entity not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			$this->checkAccessWithSpec( new HaveReadAccess(Opportunity::TAB_DEFAULT), $entity );
			$ALLOWED_FIELDS = array_merge($ALLOWED_FIELDS,[
				'updateDate',
				'title',
				'reference',
				'mode',
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);
			// get an empty entity for a future creation
			$this->checkAccessWithSpec( new HaveCreateAccess ); // TODO
			$contact = Services\Contacts::get($this->requestAccess->get('contact'), Contact::TAB_INFORMATION);
			if(!$contact) $this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE, 'No contact found, please check the value of the contact parameter');

			$entity = Services\Opportunities::getNew($contact);
			$ALLOWED_FIELDS = array_merge($ALLOWED_FIELDS,[
				'AO_ETAT',
				'AO_DEVISE',
				'AO_DEVISEAGENCE',
				'AO_CHANGE',
				'AO_CHANGEAGENCE',
				'company' => [
					'ID_CRMSOCIETE',
					'CSOC_SOCIETE',
				],
				'contact' => [
					'ID_CRMCONTACT',
					'CCON_NOM',
					'CCON_PRENOM',
				],
			]);
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);
		$profil = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_DEFAULT);
		if(!$profil) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $profil );

		$deleted = Services\Opportunities::delete($profil);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
