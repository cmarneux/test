<?php
/**
 * opportunities.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;

/**
 * Class SearchOpportunities
 * @property InputMultiValues opportunityStates
 * @property InputMultiValues positioningStates
 * @property InputMultiValues opportunityTypes
 * @property InputMultiValues origins
 * @property InputMultiValues activityAreas
 * @property InputMultiValues expertiseAreas
 * @property InputMultiValues places
 * @property InputMultiValues durations
 * @property InputMultiValues tools
 * @property InputString period
 * @property InputMultiValues sort
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputBoolean onlyVisible
 * @package BoondManager\APIs\Opportunities\Filters
 */
class SearchOpportunities extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_OPPORTUNITIES;

	/**#@+
	 * @var int PERIODS
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_UPDATED_POSITIONING = 'updatedPositioning',
		PERIOD_STARTED = 'started',
		PERIOD_UPDATED = 'updated';
	/**#@-*/

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_TITLE = 'title',
		ORDERBY_COMPANY_NAME = 'company.name',
		ORDERBY_PLACE = 'place',
		ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS = 'numberOfActivePositionings',
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_DURATION = 'duration',
		ORDERBY_STATE = 'state',
		ORDERBY_TOTAL_WEIGHTED_TURNOVER_EXCLUDED_TAX = 'totalWeightedTurnOverExcludedTax',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**
	 * SearchOpportunities constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$input = new InputBoolean('onlyVisible');
		$input->setModeDefaultValue(true);
		$this->addInput($input);

		$this->addInput( new InputMultiDict('opportunityStates', 'specific.setting.state.opportunity'));

		$input = new InputMultiDict('positioningStates');
		$input->setDict(Dictionary::getDict('specific.setting.state.positioning'), ['-2']);
		$this->addInput($input);

		$this->addInput( new InputMultiDict('opportunityTypes', 'specific.setting.typeOf.project'));
		$this->addInput( new InputMultiDict('origins', 'specific.setting.origin'));
		$this->addInput( new InputMultiDict('activityAreas', 'specific.setting.activityArea'));
		$this->addInput( new InputMultiDict('expertiseAreas', 'specific.setting.expertiseArea'));
		$this->addInput( new InputMultiDict('places', 'specific.setting.mobilityArea'));
		$this->addInput( new InputMultiDict('durations', 'specific.setting.duration'));
		$this->addInput( new InputMultiDict('tools', 'specific.setting.tool'));

		$input = new InputMultiEnum('period');
		$input->setAllowedValues([self::PERIOD_CREATED, self::PERIOD_UPDATED_POSITIONING, self::PERIOD_STARTED, self::PERIOD_UPDATED]);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_CREATIONDATE, self::ORDERBY_TITLE, self::ORDERBY_COMPANY_NAME, self::ORDERBY_PLACE, self::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS,
			self::ORDERBY_STARTDATE, self::ORDERBY_DURATION, self::ORDERBY_STATE, self::ORDERBY_TOTAL_WEIGHTED_TURNOVER_EXCLUDED_TAX,
			self::ORDERBY_MAINMANAGER_LASTNAME,
		]);
		$this->addInput($input);

		$input = new InputDate('startDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputDate('endDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);
	}

	/**
	 *
	 */
	protected function postValidation() {
		if($this->onlyVisible->isDefined() && (!CurrentUser::instance()->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || !CurrentUser::instance()->isGod()))
			$this->onlyVisible->setDisabled(true);
	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
