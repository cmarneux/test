<?php
/**
 * opportunities.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\Opportunities\Specifications\HaveCreateAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveSearchAccess;
use BoondManager\Services\Extraction;
use BoondManager\Services\Opportunities;
use BoondManager\APIs\Opportunities\Filters;
use Wish\Models\Model;

/**
 * Opportunities list controller.
 * @package Opportunities
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'title',
		'reference',
		'typeOf',
		'mode',
		'state',
		'place',
		'visibility',
		'startDate',
		'duration',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'turnoverWeightedExcludingTax',
		'numberOfActivePositionings',
		'canReadContact',
		'canReadCompany',
		'mainManager' => [
			'id',
			'firstName',
			'lastName',
		],
		'contact' => [
			'id',
			'firstName',
			'lastName',
		],
		'company' => [
			'id',
			'name',
		],
	];

	/**
	 * Search opportunities
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchOpportunities();
		$filter->setAndFilterData($this->requestAccess->getParams());

		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Extraction\Opportunities('opportunities.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Opportunities::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverWeightedExcludingTax' => $result->turnoverWeightedExcludingTax,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a project
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Information();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default opportunity
		$opportunity = Opportunities::buildFromFilter($filter);
		if(!$opportunity) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $opportunity);

		if(Opportunities::create($opportunity))
			$this->sendJSONResponse([
				'data' => $opportunity->filterFields(Information::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
