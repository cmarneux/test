<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Opportunity;
use BoondManager\Services;
use BoondManager\OldModels\Filters;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveWriteAccess;

class Information extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'updateDate',
		'title',
		'reference',
		'state',
		'typeOf',
		'mode',
		'origin' => [
			'typeOf',
			'detail'
		],
		'description',
		'criteria',
		'expertiseArea',
		'activityAreas',
		'tools',
		'place',
		'duration',
		'turnoverWeightedExcludingTax',
		'estimatesExcludingTax',
		'turnoverEstimatedExcludingTax',
		'weighting',
		'visibility',
		'startDate',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'canReadContact',
		'canReadCompany',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		],
		'company' => [
			'id',
			'name'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		],
		'files' => [
			'id',
			'name'
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Opportunities::get($id, Opportunity::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Opportunity::TAB_INFORMATION), $entity );
		} else $this->error(404);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		//if($entity->numberOfProjects) $tabData['meta'] = ['limitedMode' => true];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);
		$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Opportunity::TAB_INFORMATION), $entity );

		$filter = Filters\Profiles\Opportunities\SaveInformation::getUserFilter($this->currentUser);
		if($entity->numberOfProjects) $filter->restrictMode($entity->mode);
		$filter->setDefaultValue( $entity->encode() );
		$filter->setData($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		Services\Opportunities::update($entity, $filter);

		$this->sendJSONResponse([
			'data'=>$entity
		]);
	}
}
