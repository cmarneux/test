<?php
/**
 * positionings.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\APIs\Positionings\Filters\SearchPositionings;
use BoondManager\Services\BM;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Positioning;
use BoondManager\Services;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;

class Positionings extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'updateDate',
		'state',
		'informationComments',
		'numberOfDaysInvoicedOrQuantity',
		'averageDailyPriceExcludingTax',
		'includedInSimulation',
		'canReadProject',
		'dependsOn' => [
			'id',
			// candidate / employee
			'lastName',
			'firstName',
			'typeOf', // employee only
			// product
			'name',
			'reference',
			'subscription'
		]
	];

	public function api_get() {
		// get an existing entity
		$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_POSITIONINGS);
		// if profil not found, throw an error
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error)
		$this->checkAccessWithSpec( new HaveReadAccess(Opportunity::TAB_POSITIONINGS), $entity );

		// init filter
		$filter = new SearchPositionings();
		$filter->setIndifferentPerimeter();

		// reading input data
		$filter->setData($this->requestAccess->getParams());

		// overwriting keywords & positioningType to restrain the research
		$filter->keywords->setValue( $entity->getReference() );
		if($entity->mode == BM::PROJECT_TYPE_PRODUCT) {
			$filter->positioningType->setValue(Positioning::ENTITY_PRODUCT);
		} else {
			$filter->positioningType->setValue(Positioning::ENTITY_PROFILE);
		}

		$this->checkFilter($filter);

		$result = Services\Positionings::search($filter);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverForecastExcludingTax' => floatval($entity->turnoverEstimatedExcludingTax),
					'costsForecastExcludingTax' => floatval($entity->AO_INVESTISSEMENT),
					'marginForecastExcludingTax' => floatval($entity->turnoverEstimatedExcludingTax - $entity->AO_INVESTISSEMENT),
					'profitabilityForecast' => $entity->turnoverEstimatedExcludingTax ? floatval(100 * ($entity->turnoverEstimatedExcludingTax - $entity->AO_INVESTISSEMENT ) / $entity->turnoverEstimatedExcludingTax) : 0
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
