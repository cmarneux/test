<?php
/**
 * projects.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\APIs\Projects\Filters\SearchProjects;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Opportunity;
use BoondManager\Services;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;

class Projects extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'typeOf',
		'mode',
		'reference',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'turnoverSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'canReadProject',
		'canWriteProject',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];
	public function api_get() {
		// get an existing entity
		$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_PROJECTS);
		// if profil not found, throw an error
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error)
		$this->checkAccessWithSpec( new HaveReadAccess(Opportunity::TAB_PROJECTS), $entity );

		// init filter
		$filter = new SearchProjects();

		// reading input data
		$filter->setData($this->requestAccess->getParams());

		// overwriting keywords to restrain the research
		$filter->keywords->setValue( $entity->getReference() );
		$filter->sumAdditionalData->setValue(true);

		$this->checkFilter($filter);

		$result = Services\Projects::search($filter);
		Services\Projects::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"turnoverSimulatedExcludingTax" => $result->turnoverSimulatedExcludingTax,
					"costsSimulatedExcludingTax" => $result->costsSimulatedExcludingTax,
					"marginSimulatedExcludingTax" => $result->marginSimulatedExcludingTax,
					"profitabilitySimulated" => $result->profitabilitySimulated
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
