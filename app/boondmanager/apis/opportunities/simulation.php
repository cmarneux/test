<?php
/**
 * simulation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Opportunities;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Opportunity;
use BoondManager\Services;
use BoondManager\OldModels\Filters;
use BoondManager\Services\BM;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveWriteAccess;

class Simulation extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		/*
		'mode',
		'typeOf',
		'title',
		'visibility',
		*/
		//'costsPositioningsExcludingTax',
		//'marginPositioningsExcludingTax',
		//'profitabilityPositioningsExcludingTax',
		//'turnoverPositioningsExcludingTax',
		'validateSimulation',
		'additionalTurnoverAndCosts' => [
			'id',
			'title',
			'turnoverExcludingTax',
			'costsExcludingTax',
		],
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		/*
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		],
		'company' => [
			'id',
			'name'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		]
		*/
	];

	public function api_get() {
		// get an existing entity
		$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_SIMULATION);
		// if profil not found, throw an error
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error) (same as positioning)
		$this->checkAccessWithSpec( new HaveReadAccess(Opportunity::TAB_SIMULATION), $entity );

		$entity->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'data' => $entity
		]);
	}

	public function api_put() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);
		$entity = Services\Opportunities::get($this->requestAccess->id, Opportunity::TAB_SIMULATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Opportunity::TAB_SIMULATION), $entity );

		$filter = Filters\Profiles\Opportunities\SavePositionsDetails::getUserFilter($this->currentUser);
		$filter->setDefaultValue( $entity->encode() );
		$filter->setData($this->requestAccess->get('data'));

		$this->checkFilter($filter);

		Services\Opportunities::update($entity, $filter);

		$this->sendJSONResponse([
			'data'=>$entity
		]);
	}
}
