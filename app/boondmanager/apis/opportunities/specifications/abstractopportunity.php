<?php
/**
 * abstractopportunity.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Opportunities\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Opportunity;
use BoondManager\Lib\RequestAccess;

abstract class AbstractOpportunity extends AbstractSpecificationItem{
	/**
	 * get the opportunity from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Opportunity|null
	 */
	public function getOpportunity($request){
		if($request->data instanceof Opportunity) return $request->data;
		else return null;
	}
}
