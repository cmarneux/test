<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Opportunities\Specifications;
use BoondManager\OldModels\Specifications\RequestAccess\Opportunities\AbstractOpportunity;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadThroughGroupAgenciesBUPoles;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadWriteThroughHierarchyBUPoles;
use BoondManager\Lib\Specifications\TabBehavior;

/**
 * Indicate if the user have the right to read an opportunity
 * @package BoondManager\Models\Specifications\User
 */
class HaveReadAccess extends AbstractOpportunity{
	use TabBehavior;

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		// FIXME: Implemented from Loic's spec on /products/{id}/opportunities. Check the spec when GET opportunity/{id} is done

		$user = $object->getUser();
		$opportunity = $this->getOpportunity($object);

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		return
			$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
			|| $opportunity->isVisible()
			&& (
				(new CanReadWriteThroughHierarchyBUPoles())
				->or_(new CanReadThroughGroupAgenciesBUPoles(BM::MODULE_OPPORTUNITIES))
				->isSatisfiedBy($object)
			);
	}
}
