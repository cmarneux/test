<?php
/**
 * havewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Opportunities\Specifications;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\OldModels\Specifications\RequestAccess\HaveRight;

class HaveWriteAccessOnField extends AbstractOpportunity{

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * check if the object match the specification
	 * @param mixed $object
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($object)
	{
		/** @var CurrentUser $user*/
		$user = $object->user;
		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->field){
			case 'visibility':
				return (new HaveRight(BM::RIGHT_GLOBAL_SHOWGROUPE))->isSatisfiedBy($object);
		default:
			throw new \Exception('no rules for field "'.$this->field.'"');
		}
	}

	public static function getFieldsTested(){
		return ['visibility'];
	}
}
