<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Opportunities\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Company;
use BoondManager\APIs\Positionings;
use BoondManager\APIs\Projects;

class IsActionAllowed extends AbstractOpportunity {

	const RIGHTS_ACTIONS = ['share', 'addAction', 'duplicate', 'addPositioning', 'addProject'];

	private $action;

	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->action){
			case 'share':
				$allow = true;
				break;
			case 'addAction':
				$allow = (new HaveWriteAccess(Company::TAB_ACTIONS))->isSatisfiedBy($request);
				break;
			case 'duplicate':
				$allow = true;
				break;
			case 'addPositioning':
				$allow = (new Positionings\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			case 'addProject':
				$allow = (new Projects\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			default:
				$allow = false;
				break;
		}
		return $allow;
	}
}
