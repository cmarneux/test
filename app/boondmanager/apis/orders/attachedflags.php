<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use BoondManager\Services;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;
use Wish\Models\Model;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$order = null;

		$order = Services\Orders::get($this->requestAccess->id, Order::TAB_ATTACHED_FLAGS);
		if(!$order) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Order::TAB_ATTACHED_FLAGS), $order);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($order);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
