<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use BoondManager\Services\BM;
use BoondManager\Services\Orders;
use BoondManager\APIs\Orders\Specifications\HaveReadAccess;

class Entity extends AbstractController{

	const DELIVERIES_FIELDS = [
		'id',
		'title',
		'startDate',
		'endDate',
		'dependsOn' => [
			'id',
			// employee
			'lastName',
			'firstName',
			// product
			'name'
		]
	];
	const PURCHASES_FIELDS = [
		'id',
		'title',
		'reference',
		'startDate',
		'endDate'
	];

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'number',
		'typeOf',
		'reference',
		'project' => [
			'id',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'agency' => [
				'id',
				'name',
			],
			'pole' => [
				'id',
				'name'
			],
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function api_get() {

		$entity = Orders::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(BM::TAB_DEFAULT), $entity);

		$this->sendJSONResponse([
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		]);
	}
}
