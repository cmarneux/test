<?php
/**
 * orders.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Resources
 * @property-read InputValue $user
 * @property-read InputValue $resourceTypes
 * @property-read InputMultiValues excludeResourceTypes
 * @property-read InputValue $sort
 * @property-read InputValue $period
 * @property-read InputDate $startDate
 * @property-read InputDate $endDate
 * @package BoondManager\Models\Filters\Search
 */
class SearchOrders extends AbstractSearch
{
	const PERIMETER_MODULE = BM::MODULE_BILLING;

	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_PERIOD = 'period';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_CUSTOMERAGREEMENT= 'customerAgreement',
		ORDERBY_TURNOVERINVOICEDINCLUDEDTAX = 'turnoverInvoicedExcludedTax',
		ORDERBY_TURNOVERORDEREDEXCLUDEDTAX = 'turnoverOrderedExcludedTax',
		ORDERBY_DELTAINVOICEDEXCLUDEDTAX = 'deltaOrderdExcludedTax',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * Orders constructor.
	 */
	public function __construct(){
		parent::__construct();

		$this->addInput( new InputMultiDict('paymentMethods', 'specific.setting.paymentMethod'));
		$this->addInput( new InputMultiDict('states', 'specific.setting.state.order'));
		$this->addInput( new InputMultiDict('projectTypes', 'specific.setting.typeOf.project'));

		$period = new InputEnum('period');
		$period->setModeDefaultValue(BM::INDIFFERENT);
		$period->setAllowedValues([self::PERIOD_CREATED, self::PERIOD_PERIOD, BM::INDIFFERENT]);
		$this->addInput($period);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_CREATIONDATE, self::ORDERBY_CUSTOMERAGREEMENT, self::ORDERBY_DELTAINVOICEDEXCLUDEDTAX,
			self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_PROJECT_COMPANY_NAME, self::ORDERBY_PROJECT_REFERENCE,
			self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_TURNOVERINVOICEDINCLUDEDTAX, self::ORDERBY_TURNOVERORDEREDEXCLUDEDTAX
		]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}
}
