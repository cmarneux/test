<?php
/**
 * orders.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use Wish\MySQL\AbstractDb;
use BoondManager\Services;
use BoondManager\APIs\Orders\Specifications\HaveSearchAccess;

class Index extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'date',
		//'startDate',
		//'endDate',
		'number',
		'reference',
		'customerAgreement',
		'turnoverInvoicedExcludingTax',
		'turnoverOrderedExcludingTax',
		'deltaInvoicedExcludingTax',
		'state',
		//'canReadOrder',
		//'canWriteOrder'
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'opportunity' => [
				'id',
				'title'
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name'
			]
		]
	];

	public function api_get() {

		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchOrders();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		$result = Services\Orders::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverInvoicedExcludingTax' => $result->turnoverInvoicedExcludingTax,
					'turnoverOrderedExcludingTax' => $result->turnoverOrderedExcludingTax,
					'deltaInvoicedExcludingTax' => $result->turnoverInvoicedExcludingTax - $result->turnoverOrderedExcludingTax,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
