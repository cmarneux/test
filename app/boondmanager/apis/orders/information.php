<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use BoondManager\Services\Orders;
use BoondManager\APIs\Orders\Specifications\HaveReadAccess;

class Information extends AbstractController{

	const DELIVERIES_FIELDS = [
		'id',
		'title',
		'startDate',
		'endDate',
		'dependsOn' => [
			'id',
			// employee
			'lastName',
			'firstName',
			// product
			'name'
		]
	];
	const PURCHASES_FIELDS = [
		'id',
		'title',
		'reference',
		'startDate',
		'endDate'
	];

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'number',
		'customerAgreement',
		'state',
		'typeOf',
		'reference',
		'endDate',
		'startDate',
		'language',
		'paymentMethod',
		'paymentTerm',
		'taxRate',
		'turnoverOrderedExcludingTax',
		'informationComments',
		'legals',
		'createInvoiceAutomatically',
		'copyCommentsOnNewInvoice',
		'showCommentsOnPDF',
		'showFactorOnPDF',
		'showCompanyVATNumberOnPDF',
		'showCompanyNumberOnPDF',
		'showBankDetailsOnPDF',
		'showProjectReferenceOnPDF',
		'showResourcesNameOnPDF',
		'showAverageDailyPriceOnPDF',
		'showNumberOfWorkignDaysOnPDF',
		'showFooterOnPDF',
		'separateActivityExpensesAndPurchases',
		'groupDeliveries',
		'schedules' => [
			'id',
			'date',
			'title',
			'turnoverTermOfPaymentExcludingTax',
			'invoice'
		],
		'project' => [
			'id',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'agency' => [
				'id',
				'name',
			],
			'pole' => [
				'id',
				'name'
			],
			'company' => [
				'id',
				'name',
				'vatNumber',
				'billingDetails' => [
					'id',
					'name',
					'contact',
					'phone1',
					'email1',
					'email2',
					'email3',
					'address1',
					'address2',
					'address3',
					'postcode',
					'town',
					'country',
					'state'
				]
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'opportunity' => [
				'id',
				'title',
				'reference'
			],
			'deliveries' => self::DELIVERIES_FIELDS,
			'purchases' => self::PURCHASES_FIELDS
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'bankDetail' => [
			'id',
			'description',
			'iban',
			'bic'
		],
		'billingDetail' => [
			'id',
		],
		'factor' => [
			'id',
			'name'
		],
		'deliveriesPurchases' => [

			'id',
			'title',
			'startDate',
			'endDate',
			'dependsOn' => [
				'id',
				// employee
				'lastName',
				'firstName',
				// product
				'name'
			],
			'reference'
		],
		'files' => [
			'id',
			'name'
		]
	];

	public function api_get() {

		$entity = Orders::get($this->requestAccess->id, Order::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Order::TAB_INFORMATION), $entity);

		$this->sendJSONResponse([
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		]);
	}
}
