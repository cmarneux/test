<?php
/**
 * invoices.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Orders;

use BoondManager\APIs\Orders\Specifications\HaveReadAccess;
use BoondManager\APIs\Invoices\Filters\SearchInvoices;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use BoondManager\Services;

class Invoices extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'startDate',
		'endDate',
		'reference',
		'state',
		//'expectedPaymentDate',
		'turnoverInvoicedExcludingTax',
		'turnoverInvoicedIncludingTax',
		/*
		'paymentMethod',
		'creditNote',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'canReadInvoice',
		'canWriteInvoice',
		'order' => [
			'id',
			'project' => [
				'id',
				'contact' => [
					'id',
					'lastName',
					'firstName'
				]
			]
		]
		*/
		'schedule' => [
			'id',
			'title'
		]
	];

	public function api_get() {
		$entity = Services\Orders::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Order::TAB_INVOICES), $entity);

		$filter = new SearchInvoices();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchInvoices::ORDERBY_CREATIONDATE);
		$filter->order->setDefaultValue(SearchInvoices::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Invoices::search($filter);
		Services\Invoices::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"turnoverInvoicedExcludingTax" => $result->turnoverExcludingTax,
					"turnoverInvoicedIncludingTax" => $result->turnoverIncludingTax,
					"turnoverOrderedExcludingTax" => $entity->turnoverOrderedExcludingTax,
					"deltaInvoicedExcludingTax" => $entity->turnoverOrderedExcludingTax - $result->turnoverExcludingTax,
					"states" => $result->getStatesAmounts()
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
