<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Orders;

/**
 * Class Rights
 * @package BoondManager\APIs\Orders
 */
class Rights extends AbstractController {
	/**
	 * Get project's rights
	 */
	public function api_get() {
		$order = Orders::get($this->requestAccess->id);
		if(!$order) $this->error(404);

		$this->sendJSONResponse([
			'data' => Orders::getRights($order)
		]);
	}
}
