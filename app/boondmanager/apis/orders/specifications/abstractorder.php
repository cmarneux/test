<?php
/**
 * abstractorder.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Orders\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Order;
use BoondManager\Lib\RequestAccess;

abstract class AbstractOrder extends AbstractSpecificationItem{
	/**
	 * get the order from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Order|null
	 */
	public function getOrder($request){
		if($request->data instanceof Order) return $request->data;
		else return null;
	}
}
