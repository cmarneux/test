<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders\Specifications;


use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveReadAccess extends AbstractOrder {

	/**
	 * @param RequestAccess $object
	 * @return boolean
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();
		$order = $this->getOrder($object);

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$hierarchy = $user->checkHierarchyAccess($order, BM::MODULE_BILLING);

		return in_array($hierarchy, [BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY, BM::PROFIL_ACCESS_READ_WRITE]);
	}
}
