<?php
/**
 * isactionallowed.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Orders\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Employee;

class IsActionAllowed extends AbstractOrder {

	const ALLOWED_ACTIONS = ['share', 'addAction', 'addInvoice'];

	private $action;

	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;
		$order = $this->getOrder($request);
		if(!$order) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->action){
			case 'share':
				$allow = true;
				break;
			case 'addAction':
				$allow = (new HaveWriteAccess(Employee::TAB_ACTIONS))->isSatisfiedBy($request);
				break;
			case 'addInvoice':
				$allow = true;
				break;
			default:
				$allow = false;
				break;
		}
		return $allow;
	}
}
