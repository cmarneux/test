<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Payments;

use Wish\Encoders\JsonAPI;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Services\BM;
use BoondManager\APIs\Payments\Specifications\HaveReadAccess;
use BoondManager\APIs\Payments\Specifications\HaveCreateAccess;
use BoondManager\APIs\Payments\Specifications\HaveWriteAccess;
use BoondManager\APIs\Payments\Specifications\HaveDeleteAccess;
use BoondManager\OldModels\Filters;

class Entity extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'state',
		'paymentMethod',
		'taxRate',
		'startDate',
		'endDate',
		'purchase' => [
			'id',
			'title',
			'reference',
			'subscription',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'mainManager' => [
				'id',
				'firstName',
				'lastName',
			],
			'company' => [
				'id',
				'name',
			],
			'contact' => [
				'id',
				'firstName',
				'lastName',
			],
			'project' => [
				'id',
				'reference',
			],
			'delivery' => [
				'id',
				'title'
			],
		],
	];

	const ALLOWED_FIELDS_FOR_GET = [
		'number',
		'expectedDate',
		'performedDate',
		'amountExcludingTax',
		'amountIncludingTax',
		'informationComments',
		'activityDetails' => [
			'id',
			'firstName',
			'lastName',
			'averageDailyCost',
			'costsProduction',
			'numberOfWorkingDays',
			'timesReports' => [
				'id',
				'term',
			],
			'expensesReports' => [
				'id',
				'term',
			],
		],
		'files',
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Payments::get($this->requestAccess->id);
			// if entity not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(), $entity );

			$ALLOWED_FIELDS = array_merge(self::ALLOWED_FIELDS,self::ALLOWED_FIELDS_FOR_GET);
		} else {
			// retrieve filtered params
			$filter = Filters\Payment::getUserFilter();
			$filter->setData([
				'attributes' => [
					'term' => $this->requestAccess->get('term'),
				],
				'relationships' => [
					'purchase' => ['id' => $this->requestAccess->get('purchase'),'type' => 'purchase'],
				]
			]);

			$this->checkFilter($filter);

			$this->checkAccessWithSpec( new HaveCreateAccess );

			$entity = Services\Payments::getNew($filter);

			$ALLOWED_FIELDS = self::ALLOWED_FIELDS;
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Update a payment's data.
	 */
	public function api_put() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED); // TODO
		$entity = Services\Payments::get($this->requestAccess->id);
		// if entity not found, throw an error (404 not found)
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec( new HaveWriteAccess, $entity );

		$filter = Filters\Payment::getUserFilter($this->currentUser, $entity);
		$filter->setDefaultValue( JsonAPI::instance()->serialize($entity->filterFields(array_merge(self::ALLOWED_FIELDS,self::ALLOWED_FIELDS_FOR_GET))) );
		$filter->setData( $this->requestAccess->get('data') );

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		Services\Payments::put($entity, $filter);

		$tabData = [
			'data' => $entity->filterFields(array_merge(self::ALLOWED_FIELDS,self::ALLOWED_FIELDS_FOR_GET))
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED); // TODO
		$entity = Services\Payments::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess, $entity );

		$deleted = Services\Payments::delete($entity);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
