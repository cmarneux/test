<?php
/**
 * payment.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Payments\Filters;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use BoondManager\Services\BM;

class Payment extends \Wish\Filters\AbstractJsonAPI{

	/**
	 * Profil constructor.
	 * @property InputDate date
	 * @property InputInt state
	 * @property \Wish\Filters\Inputs\InputInt paymentMethod
	 * @property InputFloat taxRate
	 * @property InputDate startDate
	 * @property InputDate endDate
	 * @property \Wish\Filters\Inputs\InputString number
	 * @property InputDate expectedDate
	 * @property InputDate performedDate
	 * @property InputFloat amountExcludingTax
	 * @property \Wish\Filters\Inputs\InputFloat amountIncludingTax
	 * @property InputString informationComments
	 * @property \Wish\Filters\Inputs\InputString term
	 * @property InputRelationship purchase
	 */
	public function __construct(){
		parent::__construct();

		$this->addInput([
		(new InputDate('date')),
		(new InputInt('state'))
			->addFilterInDict( Dictionary::getDict('specific.setting.state.payment') ),
		(new InputInt('paymentMethod'))
			->addFilterInDict( Dictionary::getDict('specific.setting.paymentMethod') ),
		(new InputFloat('taxRate'))
			->addFilterInDict( Dictionary::getDict('specific.setting.taxRate') )
			->addFilter(FILTER_CALLBACK, function($value){
				if(!$this->taxRate->isDefined()){
					$this->amountExcludingTax->isValid();
					$this->amountIncludingTax->isValid();
					$excTax = $this->amountExcludingTax->getValue();
					$incTax = $this->amountIncludingTax->getValue();
					if($excTax <= 0) return false;
					else if($incTax <= $excTax){
						return 0;
					}else{
						return ($incTax / $excTax - 1) * 100;
					}
				}else return $value;
			}),
		(new InputDate('startDate')),
		(new InputDate('endDate')),
		(new InputString('number'))->setMinLength(1)->setMaxLength(250),
		(new InputDate('expectedDate')),
		(new InputDate('performedDate')),
		(new InputFloat('amountExcludingTax')),
		(new InputFloat('amountIncludingTax')),
		(new InputString('informationComments')),
		//filtres utilisés par GET payments/default
		(new InputString('term'))
			->addFilter(FILTER_CALLBACK, function($value){
				if(preg_match('/^([0-9]{4})-([0-9]{1,2})/', $value, $matches))
					return date('Y-m-d', mktime(0,0,0,$matches[2]+1, 0, $matches[1]));
				else
					return false;
			}, null, BM::ERROR_GLOBAL_INCORRECT_DATE_FORMAT)
			->setDisabled(true),
		(new InputRelationship('purchase'))
			->addFilterExistingEntity()
			->setDisabled(true),
		]);
	}

	/**
	 * @param CurrentUser $currentUser
	 * @return Payment
	 */
	public static function getUserFilter(CurrentUser $user = null, Models\Payment $payment = null)
	{
		if(!$user) $user = CurrentUser::instance();
		$filter = new self();

		if(!$payment) {//On est en création ou GET default
			$filter->term->setDisabled(false);
			$filter->purchase->setRequired(true);
		}
		//~ TODO : Tin : finish rights testing
		return $filter;
	}
}
