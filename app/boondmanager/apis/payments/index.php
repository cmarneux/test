<?php
/**
 * payments.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Payments;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\Payments\Specifications\HaveCreateAccess;
use BoondManager\APIs\Payments\Specifications\HaveSearchAccess;
use BoondManager\Services\Extraction;
use BoondManager\Services\Payments;
use Wish\Models\Model;
use BoondManager\APIs\Payments\Filters;

/**
 * Payments list controller.
 * @package Payments
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'performedDate',
		'expectedDate',
		'state',
		'number',
		'amountExcludingTax',
		'amountIncludingTax',
		'purchase' => [
			'id',
			'title',
			'subscription',
			'typeOf',
			'reference',
			'currency',
			'exchangeRate',
			'currencyAgency',
			'exchangeRateAgency',
			'mainManager' => [
				'id',
				'firstName',
				'lastName',
			],
			'company' => [
				'id',
				'name',
			],
			'contact' => [
				'id',
				'firstName',
				'lastName',
			],
			'project' => [
				'id',
				'reference',
			],
		],
	];

	/**
	 * Search payments
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

        $filter = new Filters\SearchPayments();
        $filter->setAndFilterData($this->requestAccess->getParams());
		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			die('TODO : Update extract function');
			$service = new Extraction\Payments('payments.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Payments::search($filter);

		foreach($result->rows as $entity) {
			/** @var Payments $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'amountExcludingTax' => $result->TOTAL_PAIEMENTSHT,
					'amountIncludingTax' => $result->TOTAL_PAIEMENTSTTC
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a payment
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Information();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default product
		$payment = Payments::buildFromFilter($filter);
		if(!$payment) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $payment);

		if(Payments::create($payment))
			$this->sendJSONResponse([
				'data' => $payment->filterFields(Information::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
