<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Payments;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Payments;

/**
 * Class Rights
 * @package BoondManager\APIs\Inactivities
 */
class Rights extends AbstractController {
	/**
	 * Get action's rights
	 */
	public function api_get() {
		$action = Payments::get($this->requestAccess->id);
		if(!$action) $this->error(404);

		$this->sendJSONResponse([
			'data' => Payments::getRights($action)
		]);
	}
}
