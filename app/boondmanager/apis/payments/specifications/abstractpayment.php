<?php
/**
 * abstractpayment.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Payments\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Payment;
use BoondManager\Lib\RequestAccess;

abstract class AbstractPayment extends AbstractSpecificationItem{
	/**
	 * get the payment from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Payment|null
	 */
	public function getPayment($request){
		if($request->data instanceof Payment) return $request->data;
		else return null;
	}
}
