<?php
/**
 * havedeleteaccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */


namespace BoondManager\APIs\Payments\Specifications;

class HaveDeleteAccess extends AbstractPayment{

	/**
	 * check if the object match the specification
	 * @param mixed $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		// TODO : rights
		return true;
	}
}
