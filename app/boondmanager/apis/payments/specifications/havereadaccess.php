<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Payments\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

class HaveReadAccess extends AbstractPayment{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		$payment = $this->getPayment($request);

		return in_array($user->checkHierarchyAccess($payment, BM::MODULE_PURCHASES), [BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY, BM::PROFIL_ACCESS_READ_WRITE]);
	}
}
