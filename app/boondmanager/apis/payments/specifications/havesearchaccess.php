<?php
/**
 * UserHaveSearchAccess.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Payments\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on positionings
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Payments
 */
class HaveSearchAccess extends AbstractPayment{

	/**
	* check if the object match the specification
	* @param RequestAccess $request
	* @return bool
	*/
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		return $user->hasAccess(BM::MODULE_PURCHASES);
	}
}
