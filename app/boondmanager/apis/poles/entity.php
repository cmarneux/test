<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Poles;

use BoondManager\APIs\Poles\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Poles\Specifications\HaveReadAccess;
use BoondManager\APIs\Poles\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Pole;
use BoondManager\Services\BM;
use BoondManager\Services\Poles;

/**
 * Class Entity
 * @package BoondManager\APIs\Poles
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'name',
		'state'
	];

	/**
	 * @param Pole $pole
	 * @return Pole
	 */
	public function getJSONData(Pole $pole) {
		return $pole->filterFields(self::ALLOWED_FIELDS);
	}

	/**
	 * Get pole's basic data
	 */
	public function api_get() {
		$pole = Poles::get($this->requestAccess->id);
		if(!$pole) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(), $pole);

		$this->sendJSONResponse([
			'data' => $this->getJSONData($pole)
		]);
	}

	/**
	 * Delete a pole
	 */
	public function api_delete() {
		$pole = Poles::get($this->requestAccess->id);
		if(!$pole) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $pole);

		if(Poles::delete($pole->getID()))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}

	/**
	 * Update pole's information data
	 */
	public function api_put() {
		$pole = Poles::get($this->requestAccess->id);
		if(!$pole) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(), $pole);

		//Build filters
		$filter = new Filters\Entity($pole);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build pole
		Poles::buildFromFilter($filter, $pole);

		if(Poles::update($pole)) {
			$this->sendJSONResponse([
				'data' => $this->getJSONData($pole)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
