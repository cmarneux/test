<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Poles\Filters;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputString;
use BoondManager\Models;

/**
 * Class Entity
 * @property InputString name
 * @property InputBoolean state
 * @package BoondManager\APIs\Poles\Filters
 */
class Entity extends AbstractJsonAPI {
	/**
	 * @var Models\Pole
	 */
	protected $pole;

	/**
	 * Information constructor.
	 * @param Models\Pole|null $pole
	 */
	public function __construct(Models\Pole $pole = null) {
		parent::__construct();

		$this->pole = $pole;

		$input = new InputString('name');
		$input->setMinLength(1);
		$input->setMaxLength(250);
		$this->addInput($input);

		$input = new InputBoolean('state');
		$this->addInput($input);

		if($this->isCreation())
			$this->adaptForCreation();
	}

	/**
	 * Test if the pole is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->pole->id ? false : true;
	}

	/**
	 * Set inputs for a creation
	 */
	private function adaptForCreation() {
		$this->name->setRequired(true);
	}
}


