<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Poles;

use BoondManager\APIs\Poles\Specifications\HaveCreateAccess;
use BoondManager\APIs\Poles\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Poles;
use BoondManager\APIs\Poles\Filters;
use Wish\Cache;

/**
 * Class Index
 * @package BoondManager\APIs\Projects
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'name', 'state'
	];

	/**
	 * Search poles
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$cache = Cache::instance();
		$cache->setEnabled(false);
		$result = Poles::getAllPoles();
		$cache->setEnabled(true);

		foreach($result as $pole)
			$pole->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => sizeof($result),
				]
			],
			'data' => $result
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a pole
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Entity();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default pole
		$pole = Poles::buildFromFilter($filter);
		if(!$pole) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $pole);

		if(Poles::create($pole)) {
			$apiPole = new Entity();
			$this->sendJSONResponse([
				'data' => $apiPole->getJSONData($pole)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
