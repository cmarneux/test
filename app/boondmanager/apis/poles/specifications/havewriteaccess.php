<?php
/**
 * HaveWriteAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Poles\Specifications;

use BoondManager\Lib\RequestAccess;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveWriteAccess
 * @package BoondManager\APIs\Poles\Specifications
 */
class HaveWriteAccess extends AbstractSpecificationItem {

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;

		//Check this API authorization & access
		return $user->isAdministrator();
	}
}
