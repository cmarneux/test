<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Positionings;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\APIs\Positionings\Specifications\HaveReadAccess;
use Wish\Models\Model;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$positionings = null;

		$positionings = Services\Positionings::get($this->requestAccess->id);
		if(!$positionings) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(), $positionings);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($positionings);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
