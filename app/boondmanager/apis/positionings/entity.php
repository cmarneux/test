<?php
/**
 * positioning.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Positionings;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Models\Positioning;
use BoondManager\Services;
use BoondManager\APIs\Positionings\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Positionings\Specifications\HaveReadAccess;
use BoondManager\APIs\Positionings\Specifications\HaveWriteAccess;

/**
 * Positioning controller.
 * @package Positionings
 * @namespace \BoondManager\Controllers\Profiles
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'updateDate',
		'state',
		'startDate',
		'endDate',
		'averageDailyPriceExcludingTax',
		'averageDailyCost',
		'numberOfDaysInvoicedOrQuantity',
		'numberOfDaysFree',
		'informationComments',
		'includedInSimulation',
		'additionalTurnoverAndCosts' => [
			'id',
			'title',
			'turnoverExcludingTax',
			'costsExcludingTax'
		],
		'turnoverSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'profitabilitySimulated',
		'occupationRate',
		'mainManager' => [
			'id',
			'lastName',
			'firstName',
			'typeOf'
		],
		'opportunity' => [
			'id',
			'title',
			'reference',
			'typeOf',
			'mode',
			'currency',
			'currencyAgency',
			'exchangeRate',
			'exchangeRateAgency',
			'canReadContact',
			'canReadCompany',
			'mainManager' => [
				'id',
				'lastName',
				'firstName'
			],
			'contact' => [
				'id',
				'lastName',
				'firstName'
			],
			'company' => [
				'id',
				'name'
			]
		],
		'project' => [
			'id',
			'reference'
		],
		'files' => [
			'id',
			'name'
		],
		'dependsOn' => [
			'id',
			// employee | candidate
			'lastName',
			'firstName',
			'typeOf',
			'agency' => [
				'id',
				//'name',
				'calendar'
			],
			// product
			'name',
			// both
		],
	];

	/**
	 * Get information data on a positioning
	*/
	public function api_get() {
		if($id = $this->requestAccess->id) {
			// gets existing
			$entity = Services\Positionings::get($id);
			// if not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Positioning::TAB_DEFAULT), $entity );
		} else $this->error(404);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Update a positioning's data.
	 */
	public function api_put() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);
		$entity = Services\Positionings::get($this->requestAccess->id);
		// if entity not found, throw an error (404 not found)
		if(!$entity) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec( new HaveWriteAccess(), $entity );

		$filter = \BoondManager\OldModels\Filters\Profiles\Positionings\SaveInformation::getUserFilter($this->currentUser, $entity);
		$filter->setDefaultValue( $entity->encode() );
		$filter->setData( $this->requestAccess->get('data') );

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		Services\Positionings::put($entity, $filter);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED);

		$entity = Services\Positionings::get($this->requestAccess->id); //TD pour récupérer l'id TD
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $entity );

		$deleted = Services\Positionings::delete($entity);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
