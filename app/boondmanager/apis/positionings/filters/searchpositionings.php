<?php
/**
 * positionings.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Positionings\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;
use BoondManager\Models\Positioning;

/**
 * Class Positionings
 * build a filter for lists/positionings (search & sort)
 * @property-read InputBoolean $returnHRManager afficher le responsable rh du profil positionné ?
 * @property-read InputBoolean $returnMainManager afficher le responsable manager du profil positionné ?
 * @property-read InputBoolean $positioningType type d'entité positionnée
 * @property-read InputValue $sort (string) colonne de tri
 * @property-read InputValue $perimeterManagersType (int) entité concernée par le périmètre défini.
 * @property-read InputValue $period (string) _(cf constantes PERIOD_***)_ type de période
 * @property-read InputDate $startDate (date) date de début
 * @property-read InputDate $endDate (date) date de fin
 * @property-read InputMultiValues $positioningStates état du positionnement
 * @property-read InputMultiValues $opportunityStates état du besoin
 * @property-read InputMultiValues $opportunityTypes état du besoin
 * @property-read InputMultiValues $entityTypes type d'entité positionné(e)
 * @property-read InputValue $resource
 * @property-read InputValue $candidate
 * @package BoondManager\Models\Filters\Search
 * @TODO à migrer
 */
class SearchPositionings extends AbstractSearch{

	/**#@+
	 * @var int PERIMETER_ENTITY
	 */
	const
		PERIMETER_OPPORTUNITIES = 'opportunities', //~ opportunity manager
		PERIMETER_PROFILES = 'profiles', //~ profile manager
		PERIMETER_POSITIONINGS = 'positionings', //~ positioning manager
		PERIMETER_PRODUCTS = 'products'; //~ product manager
	/**#@-*/

	/**#@+
	 * @var int PERIODS
	 */
	const
		PERIOD_UPDATED = '0',
		PERIOD_CREATED = '1';
	/**#@-*/

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_UPDATE_DATE = 'updateDate',
		ORDERBY_RESOURCE_LASTNAME = 'resource.lastName',
		ORDERBY_CANDIDATE_LASTNAME = 'candidate.lastName',
		ORDERBY_PRODUCT_NAME = 'product.name',
		ORDERBY_OPPORTUNITY_TITLE = 'opportunity.title',
		ORDERBY_STATE = 'state',
		ORDERBY_OPPORTUNITY_COMPANY_NAME = 'opportunity.company.name',
		ORDERBY_RESOURCE_MAINMANAGER_LASTNAME = 'resource.mainManager.lastName',
		ORDERBY_CANDIDATE_MAINMANAGER_LASTNAME = 'candidate.mainManager.lastName',
		ORDERBY_PRODUCT_MAINMANAGER_LASTNAME = 'product.mainManager.lastName',
		ORDERBY_OPPORTUNITY_MAINMANAGER_LASTNAME = 'opportunity.mainManager.lastName',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**
	 * Positionings constructor.
	 * @param Perimeter|null $perimeter
	 * @param FlagsList|null $flags
	 */
	public function __construct(Perimeter $perimeter = null, FlagsList $flags = null)
	{
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		// should search be wide (using OR) or narrow (using AND) on perimeter categories (user, agency, bus, ...)
		$narrowPerimeter = new InputBoolean('narrowPerimeter', 0);
		$this->addInput($narrowPerimeter);

		// opportunity visibility
		$visibleOpportunity = new InputBoolean('visibleOpportunity', 1);
		$this->addInput($visibleOpportunity);

		// entity used for perimeter
		$perimeterManagersType = new InputEnum('perimeterManagersType',  self::PERIMETER_OPPORTUNITIES);
		$perimeterManagersType->setAllowedValues([
			self::PERIMETER_OPPORTUNITIES, self::PERIMETER_PROFILES, self::PERIMETER_PRODUCTS, self::PERIMETER_POSITIONINGS]);
		$this->addInput($perimeterManagersType);


		$this->addInput( new InputMultiDict('positioningStates', 'specific.setting.state.positioning'));
		$this->addInput( new InputMultiDict('opportunityStates', 'specific.setting.state.opportunity'));
		$this->addInput( new InputMultiDict('opportunityTypes', 'specific.setting.typeOf.project'));

		//Displays responsible manager
		$returnMainManager = new InputBoolean('returnMainManager');
		$this->addInput($returnMainManager);

		//Displays responsible HR
		$returnHRManager = new InputBoolean('returnHRManager');
		$this->addInput($returnHRManager);

		//type of positioned entity
		$positioningType = new InputEnum('positioningType', Positioning::ENTITY_PROFILE);
		$positioningType->setMode(InputEnum::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$positioningType->setAllowedValues([Positioning::ENTITY_PRODUCT, Positioning::ENTITY_PROFILE]);
		// FIXME ne plus faire ce genre de callback !
		$positioningType->addFilter(FILTER_CALLBACK, function($value) use ($perimeterManagersType){
			//~ If perimeterManagersType is not explicitly set and positioningType == Positioning::ENTITY_PRODUCT then we set it to self::PERIMETER_PRODUCTS
			if($perimeterManagersType->isDefined()){
				$perimeterManagersType->filter();
				if($value == Positioning::ENTITY_PRODUCT && $perimeterManagersType->getValue() != self::PERIMETER_PRODUCTS
				|| $value != Positioning::ENTITY_PRODUCT && $perimeterManagersType->getValue() == self::PERIMETER_PRODUCTS) return false;
			} else if($value == Positioning::ENTITY_PRODUCT) $perimeterManagersType->setValue(self::PERIMETER_PRODUCTS)->filter();
			return $value;
		}, null, 'Value of perimeterManagersType is not compatible !');
		$this->addInput($positioningType);

		//entity types
		$entityTypes = new InputMultiValues('entityTypes');
		$entityTypes->addFilter(FILTER_CALLBACK, function($value) use ($positioningType){
			$positioningType->isValid(); // on s'assure que le positioningType soit vaildé avant de continuer
			switch($positioningType->getValue()){
				case Positioning::ENTITY_PRODUCT:
					 if(!in_array($value, array_column( Dictionary::getDict('specific.setting.typeOf.subscription') , 'id'))) return false;
					 break;
				case Positioning::ENTITY_PROFILE:default;
					 if(!in_array($value, array_column( Dictionary::getDict('specific.setting.typeOf.resource') , 'id'))) return false;
					 break;
			}
			return $value;
		}, null, 'Value of entityTypes is incorrect !');
		$this->addInput($entityTypes);

		// Ordering column
		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_RESOURCE_LASTNAME, self::ORDERBY_CANDIDATE_LASTNAME, self::ORDERBY_PRODUCT_NAME, self::ORDERBY_OPPORTUNITY_TITLE,
			self::ORDERBY_STATE, self::ORDERBY_OPPORTUNITY_COMPANY_NAME, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_RESOURCE_MAINMANAGER_LASTNAME,
			self::ORDERBY_CANDIDATE_MAINMANAGER_LASTNAME, self::ORDERBY_PRODUCT_MAINMANAGER_LASTNAME, self::ORDERBY_OPPORTUNITY_MAINMANAGER_LASTNAME,
			self::ORDERBY_UPDATE_DATE
		]);
		$this->addInput($sort);

		// period
		$period = new InputEnum('period', BM::INDIFFERENT);
		$period->setAllowedValues([
			self::PERIOD_CREATED, self::PERIOD_UPDATED
		]);
		$this->addInput($period);

		// period start & end dates
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);

		$resource = new InputValue('resource');
		$candidat = new InputValue('candidate');
		$this->addInput([$resource, $candidat]);

	}

	/*
	 * définition de quelques aides pour manipuler ce filtre
	 */

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 *
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}

	/**
	 * generate a filter adapted for the user
	 * @param CurrentUser $user
	 * @return SearchPositionings
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_POSITIONINGS ), $user->getSearchFlags());

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		$filter->visibleOpportunity->setDefaultValue(!$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE))
							  ->addFilterInArray($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) ? [0,1]:[1]);

		return $filter;
	}
}
