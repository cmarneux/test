<?php
/**
 * positionings.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Positionings;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Positioning;
use BoondManager\Services\BM;
use BoondManager\Services;
use BoondManager\APIs\Positionings\Specifications\HaveSearchAccess;
use BoondManager\APIs\Positionings\Specifications\HaveCreateAccess;

/**
 * Positionings list controller.
 * @package Positionings
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'updateDate',
		'state',
		'startDate',
		'endDate',
		'informationComments',
		'canReadItem',
		'canReadContact',
		'canReadCompany',
		'canReadOpportunity',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'opportunity' => [
			'id',
			'name'
		],
		// product | employee | candidate
		'dependsOn' => [
			'id',
			// employee | candidate
			'lastName',
			'firstName',
			// product
			'name'
		],
	];

	/**
	 * Search positionings.
	 */
	public function api_get() {

		$filter = new Filters\SearchPositionings();
		$filter->setAndFilterData( $this->requestAccess->getParams() );

		$this->checkFilter($filter);

		$this->checkAccessWithSpec( new HaveSearchAccess);

		// handle extraction to file if requested
		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Services\Extraction\Positionings('positionings.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return;
		}

		$result = Services\Positionings::search($filter);

		foreach($result->rows as $entity) {
			/** @var Positioning $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess );

		// retrieve filtered params
		$filter = Filters\Profiles\Positionings\SaveInformation::getUserFilter();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$entity = Services\Positionings::post($filter);

		// check the entity was successfully created
		if(!$entity) {
			$this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}

		$tabData['data'] = $entity;

		$this->sendJSONResponse($tabData);
	}
}
