<?php
/**
 * abstractpositioning.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Models\Positioning;
use BoondManager\Lib\RequestAccess;

abstract class AbstractPositioning extends AbstractSpecificationItem{
    /**
     * get the positioning from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Positioning|null
     */
    public function getPositioning($request){
        if($request->data instanceof Positioning) return $request->data;
        else return null;
    }

	/**
	 * @param CurrentUser $user
	 * @param string $module
	 * @return bool
	 */
	private function readBUModule(CurrentUser $user, $module){
		return
			$user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES)
			|| $user->hasRight(BM::RIGHT_WRITEALL, $module) && $user->hasRight(BM::RIGHT_SHOWALL, $module, [BM::RIGHT_SHOW_ALL_BUS, BM::RIGHT_SHOW_ALL_AGENCIES_BUS]);
	}

	/**
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function readBUOpportunity(CurrentUser $user){
		return $this->readBUModule($user, BM::MODULE_OPPORTUNITIES);
	}

	/**
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function readBUCandidate(CurrentUser $user){
		return $this->readBUModule($user, BM::MODULE_CANDIDATES);
	}

	/**
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function readBUResource(CurrentUser $user){
		return $this->readBUModule($user, BM::MODULE_RESOURCES);
	}


	/**
	 * @param CurrentUser $user
	 * @param string $module
	 * @param int $idAgency
	 * @return bool
	 */
	private function readAgencyModule(CurrentUser $user, $module, $idAgency){

		return
			$user->hasRight(BM::RIGHT_SHOWALL, $module, BM::RIGHT_SHOW_ALL_GROUP)
			|| (
				$user->hasRight(BM::RIGHT_SHOWALL, $module, [BM::RIGHT_SHOW_ALL_AGENCIES, BM::RIGHT_SHOW_ALL_AGENCIES_BUS])
				&& $user->isMyAgency($idAgency)
			);
	}

	/**
	 * @param CurrentUser $user
	 * @param Positioning $positioning
	 * @return bool
	 */
	protected function readAgencyOpportunity(CurrentUser $user, Positioning $positioning){
		return $this->readAgencyModule($user, BM::MODULE_POSITIONINGS, $positioning->opportunity->agency->id);
	}

	/**
	 * @param CurrentUser $user
	 * @param Positioning $positioning
	 * @return bool
	 */
	protected function readAgencyCandidate(CurrentUser $user, Positioning $positioning){
		return $this->readAgencyModule($user, BM::MODULE_CANDIDATES, $positioning->agency->id);
	}

	/**
	 * @param CurrentUser $user
	 * @param Positioning $positioning
	 * @return bool
	 */
	protected function readAgencyResource(CurrentUser $user, Positioning $positioning){
		return $this->readAgencyModule($user, BM::MODULE_RESOURCES, $positioning->agency->id);
	}
}
