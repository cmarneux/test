<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;
use BoondManager\Lib\RequestAccess;

/**
 * Indicate if the user have the right to create an opportunity
 * @package BoondManager\Models\Specifications\User
 */
class HaveCreateAccess extends AbstractPositioning{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// TODO: Implement isSatisfiedBy() method. from Loic's spec
		return true;
	}
}
