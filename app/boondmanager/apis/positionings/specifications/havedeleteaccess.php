<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;

/**
 * Class CanWritePositioning
 *
 * Indicate if the user have the right to write into Positioning
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractPositioning
{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the positioning is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user    = $request->user;
		$entity = $this->getPositioning($request);
		if (!$entity) return false;

		if ($user->isGod()) return true;
		if (!$user->isManager()) return false;

		//~ TODO: 07-04-2016 : Tin : Adapter les tests supplémentaires pour chaque onglets suivant les specs de Loïc
		return true;
	}
}
