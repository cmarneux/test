<?php
/**
 * UserHaveReadAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;


/**
 * Class CanCreatePositioning
 *
 * Indicate if the user have the right to road a Positioning
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveReadAccess extends AbstractPositioning{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->getUser();
		$entity = $this->getPositioning($request);

		if(!$entity) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;
		if(!$user->hasAccess(BM::MODULE_OPPORTUNITIES)) return false;

		return true;
	}
}
