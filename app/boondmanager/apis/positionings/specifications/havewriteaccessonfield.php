<?php
/**
 * userhavewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractPositioning{

    private $field;

    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * check if the object match the specification
     * @param mixed $object
     * @return bool
     */
    public function isSatisfiedBy($object)
    {
        /** @var CurrentUser $user*/
        $user = $object->user;
        if($user->isGod()) return true;
        $positioning = $this->getData($object);

        switch($this->field){
            case 'POS_DATE':
                return $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_OPPORTUNITIES);
			default:
				throw new \Exception('no rules for field "'.$this->field.'"');
        }
    }
}
