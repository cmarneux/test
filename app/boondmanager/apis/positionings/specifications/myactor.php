<?php
/**
 * myactor.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;

use BoondManager\Services\BM;
use BoondManager\Models\Candidate;
use BoondManager\Lib\RequestAccess;

class MyActor extends AbstractPositioning{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$positioning = $this->getPositioning($request);

		if($user->isGod()) return true;

		else if( (new MyOpportunity())->isSatisfiedBy($request) ) return true;

		else if(!$positioning->isVisible()) return false;

		$readBU = ($positioning->dependsOn instanceof Candidate) ? $this->readBUCandidate($user) : $this->readBUResource($user);
		$readAgency = ($positioning->dependsOn instanceof Candidate) ? $this->readAgencyCandidate($user, $positioning) : $this->readAgencyResource($user, $positioning);

		// verification manager
		$test =  $user->isMyManager($positioning->mainManager->id);
		//$test |= $user->isMyManager($positioning->ID_RESPRH);
		// verification bu
		$test |= ($user->isMyBusinessUnitManager($positioning->mainManager->id) /* || $user->isMyBusinessUnitManager($positioning->ID_RESPRH) */) && $readBU;
		// verification agence
		$test |= $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_OPPORTUNITIES) && $readAgency;

		return $test;
	}
}
