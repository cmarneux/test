<?php
/**
 * mycrm.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class MyCRM extends AbstractPositioning{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$positioning = $this->getPositioning($request);

		if($user->isGod() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)) return true;

		else if( (new MyOpportunity())->isSatisfiedBy($request) && !$user->hasRight(BM::RIGHT_MASKDATA, BM::MODULE_POSITIONINGS)) return true;

		return $user->isMyManager($positioning->CRMRESP_IDPROFIL, $isProfileID = true)
			|| ($user->isMyBusinessUnitManager($positioning->CRMRESP_IDPROFIL, $isProfileID = true) && $user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES))
			//|| (on my influence)
			|| $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_CRM) && (
				$user->hasRight(BM::RIGHT_SHOW_ALL_GROUP, BM::MODULE_CRM)
				|| $user->isMyAgency($positioning->CRMRESP_IDSOCIETE) && $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])
				|| $user->isMyBusinessUnitManager($positioning->CRMRESP_IDPROFIL, $isProfileID = true) && $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])
			);
	}
}
