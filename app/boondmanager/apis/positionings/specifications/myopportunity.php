<?php
/**
 * myOpportunity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Positionings\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class MyOpportunity extends AbstractPositioning{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$positioning = $this->getPositioning($request);

		if($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $user->isGod()) return true;

		else if(!$positioning->isVisible()) return false;

		// verification manager
		$test =  $user->isMyManager($positioning->opportunity->mainManager->id, $isProfileID = true);
		// verification BUs
		$test |= $user->isMyBusinessUnitManager($positioning->opportunity->mainManager->id, $isProfileID = true) && $this->readBUOpportunity($user);
		// vérification agences
		$test |= $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_OPPORTUNITIES) && $this->readAgencyOpportunity($user, $positioning);

		return $test;
	}
}
