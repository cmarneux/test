<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Products;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Product;
use BoondManager\Services\BM;
use BoondManager\Services;
use Wish\MySQL\AbstractDb;
use BoondManager\APIs\Products\Specifications\HaveReadAccess;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\OldModels\Filters;

class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Proxy method for search/actions->api_get
	 * @return mixed
	 */
	public function api_get() {
		$entity = Services\Products::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Product::TAB_ACTIONS), $entity);

		$filter = new SearchActions();
		$filter->setIndifferentPerimeter();

		// on restreint la recherche à produit
		$filter->setAvailableCategories([BM::CATEGORY_PRODUCT]);

		$filter->sort->setDefaultValue(SearchActions::ORDERBY_DATE);
		$filter->order->setDefaultValue(SearchActions::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());

		//on ecrase les keywords pour restreindre aux produits
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
