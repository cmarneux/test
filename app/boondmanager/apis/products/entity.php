<?php
/**
 * product.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Products;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Product;
use BoondManager\Services;
use BoondManager\APIs\Products\Specifications\HaveReadAccess;
use BoondManager\APIs\Products\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Products\Specifications\HaveCreateAccess;
use BoondManager\Services\BM;

/**
 * Product profile controller.
 * @package Products
 * @namespace \BoondManager\Controllers\Profiles
 */
class Entity extends AbstractController {

	const ALLOWED_FIELDS_DEFAULT = [
		'id',
		'state',
		'subscription',
		'taxRate',
		'currency',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		]
	];

	const ALLOWED_FIELDS = [
		'id',
		'updateDate',
		'name',
		'reference',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'pole' => [
			'id',
			'name'
		]
	];

	/**
	 * Get information data on a product profile
	 */
	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Products::get($this->requestAccess->id, Product::TAB_DEFAULT);
			// if entity not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Product::TAB_DEFAULT), $entity );

			$ALLOWED_FIELDS = self::ALLOWED_FIELDS;
		} else {
			$this->checkAccessWithSpec( new HaveCreateAccess );
			$entity = Services\Products::getNew();

			$ALLOWED_FIELDS = self::ALLOWED_FIELDS_DEFAULT;
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$entity = Services\Products::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess() , $entity);

		if(Services\Products::delete($entity))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}
}
