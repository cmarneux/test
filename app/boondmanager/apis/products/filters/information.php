<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Products\Filters;

use BoondManager\APIs\Products\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Product;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Lib\Filters\Inputs\Attributes;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;

/**
 * Class Information
 * @package BoondManager\Models\Filters\Profiles\Products
 * @property InputString name
 * @property \Wish\Filters\Inputs\InputString reference
 * @property InputString description
 * @property \Wish\Filters\Inputs\InputValue state
 * @property \Wish\Filters\Inputs\InputValue subscription
 * @property InputValue taxRate
 * @property \Wish\Filters\Inputs\InputValue currency
 * @property InputValue currencyAgency
 * @property InputValue exchangeRate
 * @property \Wish\Filters\Inputs\InputValue exchangeRateAgency
 * @property \Wish\Filters\Inputs\InputValue priceExcludingTax
 * @property InputRelationship mainManager
 * @property \Wish\Filters\Inputs\InputRelationship agency
 * @property InputRelationship pole
 */
class Information extends AbstractJsonAPI
{
	protected $_objectClass = Product::class;

	private $_product;

	/**
	 * Information constructor.
	 * @param Product $product
	 */
	public function __construct( Product $product = null)
	{
		parent::__construct();

		$this->_product = $product;

		$name = new InputString('name');
		$name->setMinLength(1)->setMaxLength(100);
		$this->addInput($name);

		$reference = new InputString('reference');
		$this->addInput($reference);

		$description = new InputString('description');
		$this->addInput($description);

		$state = new InputValue('state');
		$state->addFilterInDict( Dictionary::getDict('specific.setting.state.product') );
		$this->addInput($state);

		$subscription = new InputValue('subscription');
		$subscription->addFilterInDict( Dictionary::getDict('specific.setting.typeOf.subscription') );
		$this->addInput($subscription);

		$taxRate = new InputValue('taxRate');
		$taxRate->addFilterInArray(Dictionary::getDict('specific.setting.taxRate'));
		$this->addInput($taxRate);

		$this->addInput( new Attributes\Currency() );
		$this->addInput( new Attributes\Currency('currencyAgency') );
		$this->addInput( new Attributes\ExchangeRate() );
		$this->addInput( new Attributes\ExchangeRate('exchangeRateAgency') );

		$priceExcludingTax = new InputFloat('priceExcludingTax');
		$priceExcludingTax->setMin(0);
		$this->addInput($priceExcludingTax);

		$this->addInput( new Relationships\MainManager() );
		$this->addInput( new Relationships\Agency() );
		$this->addInput( new Relationships\Pole() );

		if($this->isCreation()) {
			$this->adaptForCreation();
		}
	}

	private function isCreation(){
		return !$this->_product || !$this->_product->id;
	}

	private function adaptForCreation(){
		$this->name->setRequired(true);
	}

	protected function postValidation() {

		$request = new RequestAccess();
		$request->setData($this->_product);
		$request->setUser(CurrentUser::instance());

		if($this->mainManager->isDefined() && !(new HaveWriteAccessOnField('mainManager'))->isSatisfiedBy($request))
			$this->mainManager->invalidateIfDebug();

		if($this->agency->isDefined() && !(new HaveWriteAccessOnField('agency'))->isSatisfiedBy($request))
			$this->agency->invalidateIfDebug();

		if($this->pole->isDefined() && !(new HaveWriteAccessOnField('pole'))->isSatisfiedBy($request))
			$this->pole->invalidateIfDebug();
	}
}
