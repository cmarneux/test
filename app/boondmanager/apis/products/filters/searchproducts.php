<?php
/**
 * products.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Products\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputMultiValues;

/**
 * Class SearchProducts
 * @property InputMultiValues projectStates
 * @property InputMultiValues productTypes
 * @property InputMultiValues sort
 * @package BoondManager\APIs\Products\Filters;
 */
class SearchProducts extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_PRODUCTS;

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_UPDATEDATE = 'updateDate',
		ORDERBY_REFERENCE = 'reference',
		ORDERBY_NAME = 'name',
		ORDERBY_TYPE = 'type',
		ORDERBY_PRICEEXCLUDINGTAX = 'priceExcludingTax',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**
	 * SearchProducts constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$input = new InputMultiValues('productStates');
		$input->addFilterInDict(Dictionary::getDict('specific.setting.state.product'));
		$this->addInput($input);

		$input = new InputMultiValues('productTypes');
		$input->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.subscription'));
		$this->addInput($input);

		$input = new InputMultiValues('sort');
		$input->addFilterInArray([
			self::ORDERBY_REFERENCE,
			self::ORDERBY_NAME,
			self::ORDERBY_TYPE,
			self::ORDERBY_PRICEEXCLUDINGTAX,
			self::ORDERBY_MAINMANAGER_LASTNAME
		]);
		$this->addInput($input);
	}
}
