<?php
/**
 * products.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Products;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\APIs\Products\Specifications\HaveCreateAccess;
use BoondManager\APIs\Products\Specifications\HaveSearchAccess;
use BoondManager\Services\Extraction;
use BoondManager\Services\Products;
use BoondManager\APIs\Products\Filters;
use Wish\Models\Model;

/**
 * Products list controller.
 * @package Products
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'reference',
		'subscription',
		'name',
		'priceExcludingTax',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'mainManager' => [
			'id',
			'firstName',
			'lastName',
		],
	];

	/**
	 * Search products
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchProducts();
		$filter->setAndFilterData($this->requestAccess->getParams());
		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Extraction\Products('products.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Products::search($filter);

		foreach($result->rows as $entity) {
			/**
			 * @var Model $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a product
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Information();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default product
		$product = Products::buildFromFilter($filter);
		if(!$product) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec( new HaveCreateAccess(), $product);

		if(Products::create($product))
			$this->sendJSONResponse([
				'data' => $product->filterFields(Information::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
