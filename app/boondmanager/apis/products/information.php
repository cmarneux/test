<?php
/**
 * profile.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Products;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Models\Product;
use BoondManager\APIs\Products\Specifications\HaveReadAccess;
use BoondManager\APIs\Products\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Information extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'reference',
		'subscription',
		'name',
		'priceExcludingTax',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'updateDate',
		'state',
		'taxRate',
		'description',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name'
		],
		'file' => [
			'id',
			'name'
		]
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Products::get($id, Product::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Product::TAB_INFORMATION), $entity );
		} else $this->error(404);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$entity = Services\Products::get($this->requestAccess->id, Product::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveWriteAccess(Product::TAB_INFORMATION), $entity );

		$filter = new Filters\Information($entity);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Services\Products::buildFromFilter($filter, $entity);

		if(Services\Products::update($entity, Product::TAB_INFORMATION))
			$this->sendJSONResponse([
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

	}
}
