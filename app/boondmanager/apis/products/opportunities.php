<?php
/**
 * opportunities.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Products;

use BoondManager\APIs\Opportunities\Filters\SearchOpportunities;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Models\Product;
use BoondManager\APIs\Products\Specifications\HaveReadAccess;

/**
 * Class Opportunities
 * @package BoondManager\Controllers\Profiles\Products
 */
class Opportunities extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'creationDate',
		'title',
		'reference',
		'typeOf',
		'mode',
		'state',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'turnoverWeightedExcludingTax',
		'canReadOpportunity',
		'canWriteOpportunity',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get Opportunities data on a product
	 * @param \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base)
	 */
	public function api_get() {
		$product = Services\Products::get($this->requestAccess->id);
		if(!$product) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Product::TAB_OPPORTUNITIES), $product);

		$filter = new SearchOpportunities();

		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchOpportunities::ORDERBY_CREATIONDATE);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre aux produits
		$filter->keywords->setValue( $product->getReference() );

		$this->checkFilter($filter);

		$result = Services\Opportunities::search($filter);
		Services\Opportunities::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverWeightedExcludingTax' => $result->turnoverWeightedExcludingTax
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
