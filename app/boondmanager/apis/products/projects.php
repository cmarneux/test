<?php
/**
 * projects.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Products;

use BoondManager\APIs\Projects\Filters\SearchProjects;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Models\Product;
use BoondManager\APIs\Products\Specifications\HaveReadAccess;

/**
 * Class Projects
 * @package BoondManager\Controllers\Profiles\Products
 */
class Projects extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'typeOf',
		'mode',
		'reference',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'turnoverSimulatedExcludingTax',
		'marginSimulatedExcludingTax',
		'costsSimulatedExcludingTax',
		'canReadProject',
		'canWriteProject',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'opportunity' => [
			'id',
			'title'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get Projects data on a product profil
	 */
	public function api_get() {
		$entity = Services\Products::get($this->requestAccess->id, Product::TAB_PROJECTS);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Product::TAB_PROJECTS), $entity);

		$filter = new SearchProjects();
		$filter->order->setDefaultValue(SearchProjects::ORDER_DESC);
		$filter->sort->setValue(SearchProjects::ORDERBY_ENDDATE);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre aux produits
		$filter->keywords->setValue($entity->getReference());
		$filter->sumAdditionalData->setValue(true);

		$this->checkFilter($filter);

		$result = Services\Projects::search($filter);
		Services\Projects::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"turnoverSimulatedExcludingTax" => $result->turnoverSimulatedExcludingTax,
					"costsSimulatedExcludingTax" => $result->costsSimulatedExcludingTax,
					"marginSimulatedExcludingTax" => $result->marginSimulatedExcludingTax,
					"profitabilitySimulated" => $result->profitabilitySimulated
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
