<?php
/**
 * abstractopportunity.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Products\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Product;
use BoondManager\Lib\RequestAccess;

abstract class AbstractProduct extends AbstractSpecificationItem{
	/**
	 * get the product from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Product|null
	 */
	public function getProduct($request){
		if($request->data instanceof Product) return $request->data;
		else return null;
	}
}
