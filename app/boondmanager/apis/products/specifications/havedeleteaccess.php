<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Products\Specifications;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Product;
use BoondManager\Services\BM;

/**
 * Class CanWriteProduct
 *
 * Indicate if the user have the right to write into Product
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractProduct
{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->getUser();

		if( $user->isGod()) return true;
		if( !$user->isManager()) return false;

		return $user->hasRight(BM::RIGHT_DELETION, BM::MODULE_PRODUCTS) && (new HaveWriteAccess(Product::TAB_INFORMATION))->isSatisfiedBy($request);
	}
}
