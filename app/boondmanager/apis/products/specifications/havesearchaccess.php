<?php
/**
 * UserHaveSearchAccess.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\APIs\Products\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on positionings
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Products
 */
class HaveSearchAccess extends AbstractProduct{

	/**
	* check if the object match the specification
	* @param RequestAccess $request
	* @return bool
	*/
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// CONFORME v6
		return $user->hasAccess(BM::MODULE_PRODUCTS);
	}
}
