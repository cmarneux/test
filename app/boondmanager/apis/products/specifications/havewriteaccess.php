<?php
/**
 * UserHaveWriteAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Products\Specifications;

use BoondManager\Services\BM;
use BoondManager\Models\Product;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadThroughGroupAgenciesBUPoles;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadWriteThroughHierarchyBUPoles;
use BoondManager\Lib\Specifications\TabBehavior;

/**
 * Class CanWriteProduct
 *
 * Indicate if the user have the right to write into Product
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveWriteAccess extends AbstractProduct{
	use TabBehavior;

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->getUser();
		$entity = $this->getProduct($request);
		if(!$entity) return false;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->getTab()) {
			case Product::TAB_INFORMATION:
				if (!$user->hasAccess(BM::MODULE_PRODUCTS)) return false;

				$read = $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE);
				$read |= (new CanReadWriteThroughHierarchyBUPoles)->or_(new CanReadThroughGroupAgenciesBUPoles(BM::MODULE_PRODUCTS))->isSatisfiedBy($request);

				return boolval($read);
			default :
				return false;
		}
	}
}
