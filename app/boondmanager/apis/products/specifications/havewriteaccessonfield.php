<?php
/**
 * havewriteaccessonfield.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Products\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractProduct {

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request)
	{
		/** @var CurrentUser $user*/
		$user = $request->user;

		if($user->isGod()) return true;

		$write = false;
		switch($this->field){
			case 'mainManager':
			case 'agency':
			case 'pole':
				$write = $user->hasRight(BM::RIGHT_ASSIGNMENT, BM::MODULE_PRODUCTS);
				break;
		}

		return boolval($write);
	}
}
