<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Products\Specifications;

use BoondManager\APIs\Purchases\Specifications\HaveCreateAccess;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Candidate;
use BoondManager\Models\Company;
use BoondManager\Models\Contact;

class IsActionAllowed extends AbstractProduct {

	private $action;

	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * Check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		switch($this->action){
			case 'share':
				$allow = true;
				break;
			case 'addOpportunity':
				$allow = (new \BoondManager\APIs\Opportunities\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			case 'addProject':
				$allow = (new \BoondManager\APIs\Projects\Specifications\HaveCreateAccess())->isSatisfiedBy($request);
				break;
			default:
				$allow = false;
				break;
		}
		return $allow;
	}
}
