<?php
/**
 * actions.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Action;
use BoondManager\Services;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;

/**
 * Class Actions
 * @package BoondManager\APIs\Projects
 */
class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get project's actions
	 */
	public function api_get() {
		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_ACTIONS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Project::TAB_ACTIONS), $project);

		$filter = new SearchActions();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
