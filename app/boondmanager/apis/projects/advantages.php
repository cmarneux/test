<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Advantages\Filters\SearchAdvantages;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Advantage;
use BoondManager\Services;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;

/**
 * Class Actions
 * @package BoondManager\APIs\Projects
 */
class Advantages extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'date',
		'advantageType' => [
			'reference',
			'name'
		],
		'quantity',
		'costPaid',
		'costCharged',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'resource' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get project's advantages
	 */
	public function api_get() {
		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_ADVANTAGES);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Project::TAB_ADVANTAGES), $project);

		$filter = new SearchAdvantages();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\Advantages::search($filter);
		foreach($result->rows as $entity){
			/** @var Advantage $entity */
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
