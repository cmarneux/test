<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;
use Wish\Models\Model;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$project = null;

		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_FLAGS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Project::TAB_FLAGS), $project);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($project);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
