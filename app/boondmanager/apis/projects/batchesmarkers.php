<?php
/**
 * batchmarkers.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\APIs\Projects\Specifications\HaveReadAccessOnField;
use BoondManager\APIs\Projects\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Projects;
use BoondManager\APIs\Projects\Filters;
use BoondManager\Models\Project;
use BoondManager\Models\BatchMarkers;

/**
 * Class BatchesMarkers
 * @package BoondManager\APIs\Projects
 */
class BatchesMarkers extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'synchronizeRemainsToBeDone',
		'allowCreateMarkersOnBatches',
		'updateProductivityDate',
		'remainsToBeDone',
		'batchesMarkers',
		'resources',
		'agency',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'batchesMarkers' => [
			'id',
			'title',
			'durationForecast',
			'timesProduction',
			'balance',
			'remainsToBeDone',
			'difference',
			'progressRate',
			'distributionRate',
			'turnoverSignedDistributedExcludingTax',
			'turnoverProgressDistributedExcludingTax',
			'markers' => [
				'id',
				'title',
				'date',
				'durationForecast',
				'remainsToBeDone',
				'progressRate',
				'resource' => [
					'id'
				]
			],
			'resources' => [
				'id',
				'timesProduction'
			]
		],
		'aloneMarkers' => [
			'id',
			'title',
			'date',
			'progressRate',
			'resource' => [
				'id'
			]
		],
		'resources' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name',
			'allowAlonesMarkers'
		]
	];

	public function getJSONData(Project $project) {
		$forbiddenFields = [];
		foreach(['synchronizeRemainsToBeDone', 'allowCreateMarkersOnBatches', 'updateProductivityDate', 'remainsToBeDone', 'batchesMarkers', 'resources', 'agency',
					'currency', 'exchangeRate', 'currencyAgency', 'exchangeRateAgency'] as $field) {
			if (!(new HaveReadAccessOnField($field))->isSatisfiedBy($this->requestAccess)) $forbiddenFields[] = $field;
		}

		$forbiddenBatchesMarkersFields = [];
		foreach(['batchesMarkers.distributionRate', 'batchesMarkers.turnoverSignedDistributedExcludingTax', 'batchesMarkers.turnoverProgressDistributedExcludingTax'] as $field) {
			list($firstField, $secondField) = explode('.', $field);
			if (!(new HaveReadAccessOnField($field))->isSatisfiedBy($this->requestAccess)) $forbiddenBatchesMarkersFields[] = $secondField;
		}

		$jsonData = $project->filterFields(self::ALLOWED_FIELDS)->removeFields($forbiddenFields);
		if($jsonData->batchesMarkers) {
			foreach ($jsonData->batchesMarkers as $batch) {
				/**
				 * @var BatchMarkers $batch
				 */
				$batch->removeFields($forbiddenBatchesMarkersFields);
			}
		}
		return $jsonData;
	}

	/**
	 * Get project's batches & markers data
	 */
	public function api_get() {
		$project = Projects::get($this->requestAccess->id, Project::TAB_BATCHES_MARKERS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Project::TAB_BATCHES_MARKERS), $project);

		$this->sendJSONResponse([
			'data' => $this->getJSONData($project)
		]);
	}

	/**
	 * Update project's batches & markers data
	 */
	public function api_put() {
		$project = Projects::get($this->requestAccess->id, Project::TAB_BATCHES_MARKERS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Project::TAB_BATCHES_MARKERS), $project);

		$filter = new Filters\BatchesMarkers($project);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Projects::buildFromFilter($filter, $project);

		if(Projects::update($project, Project::TAB_BATCHES_MARKERS))
			$this->sendJSONResponse([
				'data' => $this->getJSONData($project)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
