<?php
/**
 * deliveries.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveReadAccessOnField;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\APIs\DeliveriesInactivitiesGroupments\Filters;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;

/**
 * Class Deliveries
 * @package BoondManager\APIs\Projects
 */
class Deliveries extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'endDate',
		'state',
		'numberOfDaysInvoicedOrQuantity',
		'numberOfDaysFree',
		'title',
		'occupationRate',
		'numberOfCorrelatedOrders',
		'averageDailyPriceExcludingTax',
		'costsSimulatedExcludingTax',
		'profitabilitySimulated',
		'dependsOn' => [
			'id',
			'lastName',
			'firstName',
			'typeOf',
			'name'
		],
		'master' => [
			'id'
		],
		'slave' => [
			'id'
		],
		'groupment' => [
			'id',
			'name'
		]
	];

	/**
	 * Get project's deliveries
	 */
	public function api_get() {
		$project = null;

		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_DELIVERIES);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Project::TAB_DELIVERIES), $project);

		$forbiddenFields = [];
		foreach(['numberOfCorrelatedOrders', 'averageDailyPriceExcludingTax', 'costsSimulatedExcludingTax', 'profitabilitySimulated'] as $field)
			if(! (new HaveReadAccessOnField($field))->isSatisfiedBy($this->requestAccess) ) $forbiddenFields[] = $field;

		$filter = new Filters\SearchDeliveries();
		$filter->disableMaxResultLimit();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(Filters\SearchDeliveries::ORDERBY_GROUPMENT_ID);
		$filter->order->setDefaultValue(Filters\SearchDeliveries::ORDER_ASC);
		$filter->showGroupment->setDefaultValue(true);
		$filter->returnNbCorrelatedOrders->setDefaultValue(true);
		$filter->transferType->setDefaultValue(Filters\SearchDeliveries::TRANSFER_SLAVE_INFO);
		$filter->sumAdditionalData->setDefaultValue(false);
		$filter->setData($this->requestAccess->getParams('sort', 'order', 'maxResults', 'page'));
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\DeliveriesInactivitiesGroupments::search($filter);

		list($turnoverSignedExcludingTax, $costsSignedExcludingTax, $marginSignedExcludingTax, $profitabilitySigned,
			 $turnoverForecastExcludingTax, $costsForecastExcludingTax, $marginForecastExcludingTax, $profitabilityForecast) = Services\Projects::calculateFinancialResumeForDeliveries($result->rows);

		foreach($result->rows as $entity) {
			/**
			 * @var Project $entity
			 */
			$entity->filterFields(self::ALLOWED_FIELDS)->removeFields($forbiddenFields);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		if( (new HaveReadAccessOnField('turnoverSignedExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['turnoverSignedExcludingTax'] = $turnoverSignedExcludingTax;
		if( (new HaveReadAccessOnField('costsSignedExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['costsSignedExcludingTax'] = $costsSignedExcludingTax;
		if( (new HaveReadAccessOnField('marginSignedExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['marginSignedExcludingTax'] = $marginSignedExcludingTax;
		if( (new HaveReadAccessOnField('profitabilitySigned'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['profitabilitySigned'] = $profitabilitySigned;
		if( (new HaveReadAccessOnField('turnoverForecastExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['turnoverForecastExcludingTax'] = $turnoverForecastExcludingTax;
		if( (new HaveReadAccessOnField('costsForecastExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['costsForecastExcludingTax'] = $costsForecastExcludingTax;
		if( (new HaveReadAccessOnField('marginForecastExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['marginForecastExcludingTax'] = $marginForecastExcludingTax;
		if( (new HaveReadAccessOnField('profitabilityForecast'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['profitabilityForecast'] = $profitabilityForecast;

		$this->sendJSONResponse($tabData);
	}
}
