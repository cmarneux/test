<?php
/**
 * entity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveDeleteAccess;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Projects;

/**
 * Class Entity
 * @package BoondManager\APIs\Projects
 */
class Entity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'reference',
		'typeOf',
		'mode',
		'creationDate',
		'updateDate',
		'currencyAgency',
		'currency',
		'exchangeRate',
		'exchangeRateAgency',
		'agency' => [
			'id'
		],
		'mainManager' => [
			'id'
		],
		'pole' => [
			'id'
		]
	];

	/**
	 * Get project's basic data
	 */
	public function api_get() {
		$project = Projects::get($this->requestAccess->id, BM::TAB_DEFAULT);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(BM::TAB_DEFAULT), $project);

		$this->sendJSONResponse([
			'data' => $project->filterFields(self::ALLOWED_FIELDS)
		]);
	}

	/**
	 * Delete a project
	 */
	public function api_delete() {
		$project = Projects::get($this->requestAccess->id);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveDeleteAccess(), $project);

		if(Projects::delete($project->getID()))
			$this->sendJSONResponse();
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
	}
}
