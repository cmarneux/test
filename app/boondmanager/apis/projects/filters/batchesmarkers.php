<?php
/**
 * batchmarkers.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Filters;

use BoondManager\APIs\Projects\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputMultiObjects;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;

/**
 * Class BatchesMarkers
 * @property InputBoolean synchronizeRemainsToBeDone
 * @property InputBoolean allowCreateMarkersOnBatches
 * @property InputFloat remainsToBeDone
 * @property InputMultiObjects batchesMarkers
 * @property InputMultiObjects aloneMarkers
 * @package BoondManager\APIs\Projects\Filters
 */
class BatchesMarkers extends AbstractJsonAPI {
	/**
	 * @var Models\Project
	 */
	protected $project;

	/**
	 * BatchesMarkers constructor.
	 * @param Models\Project|null $project
	 */
	public function __construct(Models\Project $project) {
		parent::__construct();

		$this->project = $project;

		$this->addInput(new InputBoolean('synchronizeRemainsToBeDone'));
		$this->addInput(new InputBoolean('allowCreateMarkersOnBatches'));
		$this->addInput(new InputFloat('remainsToBeDone'));

		$batches = [];
		$markers = [];
        foreach($this->project->batchesMarkers as $batch) {
            $batches[] = ['id' => $batch->id];
            if($batch->markers) {
                foreach($batch->markers as $marker)
                    $markers[] = ['id' => $marker->id];
            }
        }

		//TODO : Ajouter un filtre qui doit supprimer les doublons
		$input = new InputMultiObjects('batchesMarkers', (new Attributes\BatchMarkers($batches, $markers, $this->project->resources)));
		$this->addInput($input);

		//TODO : Ajouter un filtre qui doit supprimer les doublons
		$input = new InputMultiObjects('aloneMarkers', (new Attributes\AloneMarker($this->project->aloneMarkers)));
		$this->addInput($input);
	}

	protected function postValidation() {
		$request = new RequestAccess();
		$request->setData($this->project);
		$request->setUser(CurrentUser::instance());

		$batchesMarkers = $this->project->batchesMarkers ? $this->project->batchesMarkers : [];
		$allowCreateMarkersOnBatches = $this->allowCreateMarkersOnBatches->isDefined() ? $this->allowCreateMarkersOnBatches->getValue() : $this->project->allowCreateMarkersOnBatches;
		$synchronizeRemainsToBeDone = $this->synchronizeRemainsToBeDone->isDefined() ? $this->synchronizeRemainsToBeDone->getValue() : $this->project->synchronizeRemainsToBeDone;

		//Check if batchesMarkers is writable
		if($this->batchesMarkers->isDefined() && !(new HaveWriteAccessOnField('batchesMarkers'))->isSatisfiedBy($request))
			$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS);

		//Check allowCreateMarkersOnBatches is writable
		if($this->allowCreateMarkersOnBatches->isDefined() && !(new HaveWriteAccessOnField('allowCreateMarkersOnBatches'))->isSatisfiedBy($request))
			$this->allowCreateMarkersOnBatches->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_ALLOWCREATEMARKERSONBATCHES);

		//If batchesMarkers.title is not writable then batches has to be exactly the same if defined
		if($this->batchesMarkers->isDefined() && sizeof($this->batchesMarkers->getItems()) != sizeof($batchesMarkers) && !(new HaveWriteAccessOnField('batchesMarkers.title'))->isSatisfiedBy($request))
			$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_BATCH_CANNOT_BE_CREATED_DELETED);

		//FIXME : Quand le problème des erreurs de chaque objet du multiobject sera géré, il faudra lever une erreur par attribut et par objet
		foreach($this->batchesMarkers->getItems() as $batch) {
			/**
			 * @var Attributes\BatchMarkers $batch
			 */
			if((!$batch->id->isDefined() || $batch->title->isDefined()) && !(new HaveWriteAccessOnField('batchesMarkers.title'))->isSatisfiedBy($request))
				$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS_TITLE);

			if((!$batch->id->isDefined() || $batch->distributionRate->isDefined()) && !(new HaveWriteAccessOnField('batchesMarkers.distributionRate'))->isSatisfiedBy($request))
				$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS_DISTRIBUTIONRATE);

			foreach($batch->markers->getItems() as $marker) {
				/**
				 * @var Attributes\Marker $marker
				 */
				if(($marker->durationForecast->isDefined() || $marker->remainsToBeDone->isDefined()) && (!$marker->durationForecast->isDefined() || !$marker->remainsToBeDone->isDefined()))
					$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_DURATIONFORECAST_AND_REMAINSTOBEDONE);
			}

			foreach($batch->markers->getItems() as $marker) {
				if(!$batch->id->isDefined() && $marker->id->isDefined())
					$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_ID);
			}

			if(!$allowCreateMarkersOnBatches) {
				if(!$batch->markers->isDefined() || sizeof($batch->markers->getItems()) != 1) {
					$this->allowCreateMarkersOnBatches->setDisabled(true);
					$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_ONE_ELEMENT);
				}

				foreach($batch->markers->getItems() as $marker) {
					if($batch->id->isDefined() && !$marker->id->isDefined() || !$batch->id->isDefined() && $marker->id->isDefined())
						$this->batchesMarkers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_ID);
				}
			}
		}

		//synchronizeRemainsToBeDone can be update only for package
		if($this->synchronizeRemainsToBeDone->isDefined() && !(new HaveWriteAccessOnField('synchronizeRemainsToBeDone'))->isSatisfiedBy($request))
			$this->synchronizeRemainsToBeDone->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_SYNCHRONIZEREMAINSTOBEDONE);

		//remainsToBeDone can be update only for package and if synchronizeRemainsToBeDone is false
		if($this->remainsToBeDone->isDefined() && $synchronizeRemainsToBeDone && !(new HaveWriteAccessOnField('remainsToBeDone'))->isSatisfiedBy($request))
			$this->remainsToBeDone->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_REMAINSTOBEDONE);
	}
}


