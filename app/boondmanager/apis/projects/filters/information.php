<?php
/**
 * information.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Filters;

use BoondManager\APIs\Projects\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\RequestAccess;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Lib\Filters\Inputs\Attributes;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Models;

/**
 * Class Information
 * @property InputDateTime creationDate
 * @property InputString reference
 * @property InputValue state
 * @property InputValue typeOf
 * @property Attributes\Address address
 * @property Attributes\PostCode postcode
 * @property Attributes\Town town
 * @property Attributes\Country country
 * @property InputString informationComments
 * @property InputDate startDate
 * @property InputDate endDate
 * @property Attributes\Currency currency
 * @property Attributes\Currency currencyAgency
 * @property InputFloat exchangeRate
 * @property InputFloat exchangeRateAgency
 * @property InputBoolean showBatchesMarkersTab
 * @property Relationships\MainManager mainManager
 * @property Relationships\Agency agency
 * @property Relationships\Pole pole
 * @property Relationships\Opportunity opportunity
 * @property Relationships\Contact contact
 * @property Relationships\Company company
 * @property Relationships\Contact technical
 * @package BoondManager\APIs\Projects\Filters
 */
class Information extends AbstractJsonAPI {
	/**
	 * @var Models\Project
	 */
	protected $project;

	/**
	 * Information constructor.
	 * @param Models\Project|null $project
	 */
	public function __construct(Models\Project $project = null) {
		parent::__construct();

		$this->project = $project;

		$this->addInput(new InputDateTime('creationDate'));

		$input = new InputString('reference');
		$input->setMaxLength(100);
		$this->addInput($input);

		$this->addInput(new InputDict('typeOf', 'specific.setting.typeOf.project'));
		$this->addInput(new InputDict('state', 'specific.setting.state.project'));

		$this->addInput(new Attributes\Address());
		$this->addInput(new Attributes\PostCode());
		$this->addInput(new Attributes\Town());
		$this->addInput(new Attributes\Country());

		$input = new InputString('informationComments');
		$input->setMaxLength(2000);
		$this->addInput($input);

		$this->addInput(new InputDate('startDate'));
		$this->addInput(new InputDate('endDate'));

		$this->addInput(new Attributes\Currency());
		$this->addInput(new Attributes\Currency('currencyAgency'));
		$this->addInput(new InputFloat('exchangeRate'));
		$this->addInput(new InputFloat('exchangeRateAgency'));
		$this->addInput(new InputBoolean('showBatchesMarkersTab'));

		$this->addInput(new Relationships\MainManager());
		$this->addInput(new Relationships\Agency());
		$this->addInput(new Relationships\Pole());
		$this->addInput(new Relationships\Opportunity());
		$this->addInput(new Relationships\Contact());
		$this->addInput(new Relationships\Company());
		$this->addInput(new Relationships\Contact('technical'));

		if($this->isCreation())
			$this->adaptForCreation();
		else
			$this->adaptForEdition();
	}

	/**
	 *
	 */
	protected function postValidation() {
		$mode = Dictionary::lookup('specific.setting.typeOf.project', $this->typeOf->getValue(), null, 'id', 'mode');
		if(is_null($mode)) $mode = $this->project->mode;

		if($this->isCreation()) {
			//POST : project's mode has to be the same as opportunity's mode
			$modeOpportunity = Dictionary::lookup('specific.setting.typeOf.project', $this->opportunity->getValue()->typeOf, null, 'id', 'mode');
			if($modeOpportunity != $mode)
				$this->typeOf->invalidate(Models\Project::ERROR_PROJECT_DIFFERENT_MODE_FROM_OPPORTUNITY);

			//POST : only product or package are allowed
			if(!in_array($mode, [BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_PRODUCT]))
				$this->typeOf->invalidate(Models\Project::ERROR_PROJECT_WRONG_CREATION_TYPE);

			$project = new Models\Project();
			$project->mode = $mode;
		} else {
			//PUT : unable to change project's mode
			if($this->typeOf->isDefined() && $this->project->mode != $mode)
				$this->typeOf->invalidate(Models\Project::ERROR_PROJECT_WRONG_MODE);

			$project = $this->project;
		}

		//Si contact ou company est défini alors les 2 doivent être valides
		if(($this->contact->isDefined() || $this->company->isDefined()) &&
			(!$this->contact->isDefined() ||
			!$this->company->isDefined()) ||
		   (is_null($this->contact->getValue()) || is_null($this->company->getValue())) &&
		   	(!is_null($this->contact->getValue()) ||
		   	 !is_null($this->company->getValue()))
		) {
			$this->contact->invalidate(Models\Project::ERROR_PROJECT_COMPANY_CONTACT_HAVE_TO_BE_DEFINED);
			$this->company->invalidate(Models\Project::ERROR_PROJECT_COMPANY_CONTACT_HAVE_TO_BE_DEFINED);
		}

		$request = new RequestAccess();
		$request->setData($project);
		$request->setUser(CurrentUser::instance());

		//Check the right to change mainManager
		if(!$this->isCreation() && $this->mainManager->isDefined() && !(new HaveWriteAccessOnField('mainManager'))->isSatisfiedBy($request))
			$this->mainManager->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		//Check the right to change agency
		if(!$this->isCreation() && $this->agency->isDefined() && !(new HaveWriteAccessOnField('agency'))->isSatisfiedBy($request))
			$this->agency->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		//Check the right to change pole
		if(!$this->isCreation() && $this->pole->isDefined() && !(new HaveWriteAccessOnField('pole'))->isSatisfiedBy($request))
			$this->pole->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		//Check the right to change creationDate
		if($this->creationDate->isDefined() && !(new HaveWriteAccessOnField('creationDate'))->isSatisfiedBy($request))
			$this->creationDate->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		//startDate can be update only for package & recruitment
        $rightStartDate = (new HaveWriteAccessOnField('startDate'))->isSatisfiedBy($request);
        if($this->isCreation() && $rightStartDate && !$this->startDate->isDefined())
            $this->startDate->setValue($this->startDate->getDefaultValue());

		if($this->startDate->isDefined() && !$rightStartDate)
			$this->startDate->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_STARTDATE);

		//endDate can be update only for package
        $rightEndDate = (new HaveWriteAccessOnField('endDate'))->isSatisfiedBy($request);
        if($this->isCreation() && $rightEndDate && !$this->endDate->isDefined())
            $this->endDate->setValue($this->endDate->getDefaultValue());

		if($this->endDate->isDefined() && !$rightEndDate)
			$this->endDate->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_ENDDATE);

		//endDate >= startDate
		if($this->endDate->isDefined() || $this->startDate->isDefined()) {
			$startDate = $this->startDate->isDefined() ? $this->startDate->getValue() : $this->project->startDate;
			$endDate = $this->endDate->isDefined() ? $this->endDate->getValue() : $this->project->endDate;
			if($endDate < $startDate) {
				if($this->startDate->isDefined()) $this->startDate->invalidate(Models\Project::ERROR_PROJECT_ENDDATE_SUPERIOR_OR_EQUAL_STARTDATE);
				if($this->endDate->isDefined()) $this->endDate->invalidate(Models\Project::ERROR_PROJECT_ENDDATE_SUPERIOR_OR_EQUAL_STARTDATE);
			}
		}

		//showBatchesMarkersTab can be update only for package & recruitment
		if($this->showBatchesMarkersTab->isDefined() && !in_array($mode, [BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_PRODUCT]))
			$this->showBatchesMarkersTab->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_ALLOWBATCHESMARKERSTAB);
	}

	/**
	 * Test if the project is creating
	 * @return boolean
	 */
	private function isCreation() {
		return $this->project && $this->project->id ? false : true;
	}

	/**
	 * Set inputs for an update
	 */
	private function adaptForEdition() {
		$this->opportunity->setDisabled(true);
	}

	/**
	 * Set inputs for a creation
	 */
	private function adaptForCreation() {
		$this->reference->setRequired(true);
		$this->mainManager->setRequired(true);
		$this->agency->setRequired(true);
		$this->opportunity->setRequired(true);

        $this->creationDate->setModeDefaultValue(BM::getDefaultDateTime(), $defined = true);

        $this->typeOf->setModeDefaultValue(BM::PROJECT_TYPE_PACKAGE, $defined = true);

        $this->state->setModeDefaultValue(Models\Project::DEFAULT_PROJECT_STATE, $defined = true);

        $this->country->setModeDefaultValue(CurrentUser::instance()->getCountry(), $defined = true);

        $this->currency->setModeDefaultValue(CurrentUser::instance()->getCurrency(), $defined = true);

        $this->currencyAgency->setModeDefaultValue(CurrentUser::instance()->getCurrency(), $defined = true);

        $this->exchangeRate->setModeDefaultValue(BM::DEFAULT_EXCHANGERATE, $defined = true);

        $this->exchangeRateAgency->setModeDefaultValue(CurrentUser::instance()->getExchangeRate(), $defined = true);

        $this->showBatchesMarkersTab->setModeDefaultValue(false, $defined = true);
    }
}


