<?php
/**
 * searchprojects.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;

/**
 * Class SearchProjects
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputBoolean sumAdditionalData
 * @property InputMultiDict projectStates
 * @property InputMultiDict projectTypes
 * @property InputMultiDict activityAreas
 * @property InputMultiDict expertiseAreas
 * @property InputString period
 * @property InputMultiEnum sort
 * @package BoondManager\APIs\Projects\Filters
 */
class SearchProjects extends AbstractSearch {
	/**
	 * @var string
	 */
	const PERIMETER_MODULE = BM::MODULE_PROJECTS;

	/**#@+
	 * @var int PERIODS
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_CLOSED = 'closed',
		PERIOD_STARTED = 'started',
		PERIOD_STOPPED = 'stopped',
		PERIOD_RUNNING_AND_SUM = 'runningAndSum',
		PERIOD_RUNNING = 'running',
		PERIOD_DELIVERIES_RUNNING_AND_SUM = 'deliveriesRunningAndSum',
		PERIOD_DELIVERIES_RUNNING = 'deliveriesRunning',
		PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM = 'hasAdditionalDataOrPurchaseAndSum',
		PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE = 'hasAdditionalDataOrPurchase';
	/**#@-*/

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_ENDDATE = 'endDate',
		ORDERBY_REFERENCE = 'reference',
		ORDERBY_COMPANY_NAME = 'company.name',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**
	 * SearchProjects constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$input = new InputBoolean('sumAdditionalData');
		$input->setModeDefaultValue(true);
		$this->addInput($input);

		$this->addInput( new InputMultiDict('projectStates', 'specific.setting.state.project'));
		$this->addInput( new InputMultiDict('projectTypes', 'specific.setting.typeOf.project'));
		$this->addInput( new InputMultiDict('activityAreas', 'specific.setting.activityArea'));
		$this->addInput( new InputMultiDict('expertiseAreas', 'specific.setting.expertiseArea'));

		$input = new InputEnum('period');
		$input->setAllowedValues([
			self::PERIOD_CREATED,
			self::PERIOD_CLOSED,
			self::PERIOD_RUNNING_AND_SUM,
			self::PERIOD_RUNNING,
			self::PERIOD_DELIVERIES_RUNNING_AND_SUM,
			self::PERIOD_DELIVERIES_RUNNING,
			self::PERIOD_STARTED,
			self::PERIOD_STOPPED,
			self::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM,
			self::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE
		]);
		$this->addInput($input);

		$input = new InputDate('startDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputDate('endDate');
		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputMultiEnum('sort');
		$input->setAllowedValues([
			self::ORDERBY_STARTDATE,
			self::ORDERBY_ENDDATE,
			self::ORDERBY_REFERENCE,
			self::ORDERBY_COMPANY_NAME,
			self::ORDERBY_MAINMANAGER_LASTNAME,
		]);
		$this->addInput($input);
	}

	/**
	 *
	 */
	protected function postValidation() {
		//Check if extraction is allowed
		if(!$this->extraction->isDefined() || $this->extraction->getValue() != 'csv' || !CurrentUser::instance()->hasRight(BM::RIGHT_EXTRACTION, BM::MODULE_PROJECTS))
			$this->extraction->setDisabled(true);
	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
