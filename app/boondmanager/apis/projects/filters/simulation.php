<?php
/**
 * simulation.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Filters;

use BoondManager\APIs\Projects\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services\CurrentUser;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputMultiRelationships;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Models;
use BoondManager\Lib\Filters\Inputs\Attributes;

/**
 * Class Simulation
 * @property InputBoolean showProductivityPerDelivery
 * @property InputMultiObjects additionalTurnoverAndCosts
 * @property InputMultiRelationships projectManagers
 * @package BoondManager\APIs\Projects\Filters
 */
class Simulation extends AbstractJsonAPI {
	/**
	 * @var Models\Project
	 */
	protected $project;

	/**
	 * Simulation constructor.
	 * @param Models\Project|null $project
	 */
	public function __construct(Models\Project $project) {
		parent::__construct();

		$this->project = $project;

		$this->addInput(new InputBoolean('showProductivityPerDelivery'));

		$template = new Attributes\AdditionalTurnoverAndCosts();
		$template->setAllowedTurnoverAndCosts($this->project->additionalTurnoverAndCosts);
		$this->addInput( new InputMultiObjects('additionalTurnoverAndCosts', $template));

		$resources = $this->project->resources;

		//TODO : Add a filter to delete duplicates
		$input = new InputMultiRelationships('projectManagers');
		$input->addFilter(FILTER_CALLBACK, function($value) use($resources) {
			$resources = Tools::useColumnAsKey('id', $resources);
			if(array_key_exists($value, $resources)) {
				$instance = new Models\Employee();
				$instance->fromArray(['id' => $value]);
				return $instance;
			} else return false;
		}, null, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		$input->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);
		$this->addInput($input);
	}

	protected function postValidation() {
		$request = new RequestAccess();
		$request->setData($this->project);
		$request->setUser(CurrentUser::instance());

		$additionalTurnoverAndCosts = $this->project->additionalTurnoverAndCosts ? $this->project->additionalTurnoverAndCosts : [];

		foreach($this->additionalTurnoverAndCosts->getItems() as $addData) {
			/**
			 * @var Attributes\AdditionalTurnoverAndCosts $addData
			 */
			foreach($additionalTurnoverAndCosts as $prjAddData) {
				/**
				 * @var Models\DeliveryDetail $prjAddData
				 */
				if ($addData->id->isDefined() && $addData->id->getValue() == $prjAddData->id && $prjAddData->purchase)
					$addData->id->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_ADDITIONALTURNOVERCOSTS_PURCHASE);
			}
		}

		//showProductivityPerDelivery can be update only for package & technical assistance
		if($this->showProductivityPerDelivery->isDefined() && !(new HaveWriteAccessOnField('showProductivityPerDelivery'))->isSatisfiedBy($request) )
			$this->showProductivityPerDelivery->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_SHOWPRODUCTIVITYPERDELIVERY);

		//projectManagers can be update only for package & technical assistance
		if($this->projectManagers->isDefined() && !(new HaveWriteAccessOnField('projectManagers'))->isSatisfiedBy($request))
			$this->projectManagers->invalidateIfDebug(Models\Project::ERROR_PROJECT_WRONG_PROJECTMANAGERS);
	}
}


