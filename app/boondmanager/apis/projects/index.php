<?php
/**
 * index.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveCreateAccess;
use BoondManager\APIs\Projects\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Extraction;
use BoondManager\Services\Projects;
use BoondManager\APIs\Projects\Filters;

/**
 * Class Index
 * @package BoondManager\APIs\Projects
 */
class Index extends AbstractController {
	const ALLOWED_FIELDS = [
		'id', 'typeOf', 'mode', 'reference',
		'startDate', 'endDate',
		'currency', 'exchangeRate', 'currencyAgency', 'exchangeRateAgency',
		'turnoverSimulatedExcludingTax', 'marginSimulatedExcludingTax', 'profitabilitySimulated',
		'mainManager' => [
			'id', 'lastName', 'firstName'
		],
		'opportunity' => [
			'id', 'title'
		],
		'contact' => [
			'id', 'lastName', 'firstName'
		],
		'company' => [
			'id', 'name'
		]
	];

	const EMPLOYEE_ALLOWED_FIELDS = [
		'id', 'typeOf', 'mode', 'reference',
		'company' => [
			'id', 'name'
		]
	];

	/**
	 * Search projects
	 */
	public function api_get() {
		$this->checkAccessWithSpec(new HaveSearchAccess);

		$filter = new Filters\SearchProjects();
		$filter->setData($this->requestAccess->getParams());
		if(CurrentUser::instance()->isEmployee()) $filter->keywords->setValue( CurrentUser::instance()->getAccount()->getReference() );

		$this->checkFilter($filter);

		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			die('TODO : Update extract function');
			$service = new Extraction\Projects('projects.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Projects::search($filter);
		$result->filterFields( CurrentUser::instance()->isManager() ? self::ALLOWED_FIELDS : self::EMPLOYEE_ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		if(CurrentUser::instance()->isManager()) {
			$tabData['meta']['totals'] = array_merge($tabData['meta']['totals'], [
				'turnoverSimulatedExcludingTax' => $result->turnoverSimulatedExcludingTax,
				'marginSimulatedExcludingTax' => $result->marginSimulatedExcludingTax,
				'profitabilitySimulated' => $result->profitabilitySimulated,
			]);
		}

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a project
	 */
	public function api_post() {
		//Get relationships needed for creation
		$filter = new Filters\Information();
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build default project
		$project = Projects::buildFromFilter($filter);
		if(!$project) $this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);

		$this->checkAccessWithSpec(new HaveCreateAccess(), $project);

		if(Projects::attachAgencyConfig($project) && Projects::create($project)) {
			$apiProject = new Information();
			$this->sendJSONResponse([
				'data' => $apiProject->getJSONData($project, $this->requestAccess)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
