<?php
/**
 * information.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveReadAccessOnField;
use BoondManager\APIs\Projects\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\Projects;
use BoondManager\APIs\Projects\Filters;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;

/**
 * Class Information
 * @package BoondManager\APIs\Projects
 */
class Information extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'reference',
		'typeOf',
		'mode',
		'creationDate',
		'updateDate',
		'state',
		'informationComments',
		'startDate',
		'endDate',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'address',
		'postcode',
		'town',
		'country',
		'showBatchesMarkersTab',
		'agency' => [
			'id',
			'name'
		],
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'pole' => [
			'id',
			'name'
		],
		'opportunity' => [
			'id',
			'title',
			'reference'
		],
		'contact' => [
			'id',
			'lastName',
			'firstName'
		],
		'company' => [
			'id',
			'name'
		],
		'technical' => [
			'id',
			'lastName',
			'firstName',
			'company' => [
				'id',
				'name'
			],
		],
		'candidate' => [
			'id',
			'lastName',
			'firstName'
		],
		'files' => [
			'id',
			'name'
		]
	];

	/**
	 * @param Project $project
	 * @param RequestAccess $request
	 * @return Project
	 */
	public function getJSONData(Project $project, $request = null) {
		if(!$request) $request = $this->requestAccess;
		$forbiddenFields = [];
		foreach(['endDate', 'showBatchesMarkersTab', 'currency', 'exchangeRate', 'currencyAgency', 'exchangeRateAgency'] as $field)
			if(! (new HaveReadAccessOnField($field))->isSatisfiedBy($request) ) $forbiddenFields[] = $field;
		return $project->removeFields($forbiddenFields)->filterFields(self::ALLOWED_FIELDS);
	}

	/**
	 * Get project's information data
	 */
	public function api_get() {
		$project = Projects::get($this->requestAccess->id, Project::TAB_INFORMATION);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Project::TAB_INFORMATION), $project);

		$this->sendJSONResponse([
			'data' => $this->getJSONData($project)
		]);
	}

	/**
	 * Update project's information data
	 */
	public function api_put() {
		$project = Projects::get($this->requestAccess->id, Project::TAB_INFORMATION);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Project::TAB_INFORMATION), $project);

		//Build filters
		$filter = new Filters\Information($project);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Projects::buildFromFilter($filter, $project);

		if(Projects::attachAgencyConfig($project) && Projects::update($project, Project::TAB_INFORMATION))

			$this->sendJSONResponse([
				'data' => $this->getJSONData($project)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
