<?php
/**
 * orders.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Orders\Filters\SearchOrders;
use BoondManager\APIs\Projects\Specifications\HaveReadAccessOnField;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Order;
use BoondManager\Services;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;

/**
 * Class Orders
 * @package BoondManager\APIs\Projects
 */
class Orders extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'date',
		'startDate',
		'endDate',
		'number',
		'reference',
		'customerAgreement',
		'turnoverInvoicedExcludingTax',
		'turnoverOrderedExcludingTax',
		'deltaInvoicedExcludingTax',
		'state',
		'canReadOrder',
		'canWriteOrder'
	];

	/**
	 * Get project's orders
	 */
	public function api_get() {
		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_ORDERS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Project::TAB_ORDERS), $project);

		$filter = new SearchOrders();
		$filter->setIndifferentPerimeter();
		$filter->disableMaxResultLimit();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\Orders::search($filter);

		list(
			$invoiceSummary, $deltaOrderedExcludingTax, $marginOrderedExcludingTax, $profitabilityOrdered,
			$marginInvoicedExcludingTax, $profitabilityInvoiced, $turnoverPaidExcludingTax, $marginPaidExcludingTax, $profitabilityPaid
		) = Services\Projects::calculateFinancialResumeForOrders($project, $result);

		foreach($result->rows as $entity){
			/** @var Order $entity */
			Services\Orders::attachRights($entity);
			$entity->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'turnoverInvoicedExcludingTax' => $result->turnoverInvoicedExcludingTax, // fait par la recherche
					'turnoverOrderedExcludingTax' => $result->turnoverOrderedExcludingTax, // fait par la recherche
					// données financières
					'deltaOrderedExcludingTax' => $deltaOrderedExcludingTax,
					'turnoverSignedExcludingTax' => $project->turnoverSignedExcludingTax,
					'turnoverProductionExcludingTax' => $project->turnoverProductionExcludingTax,
					'turnoverPaidExcludingTax' => $turnoverPaidExcludingTax,
					'costSignedExcludingTax' => $project->costsSignedExcludingTax,
					'costProductionExcludingTax' => $project->costsProductionExcludingTax,
					'marginSignedExcludingTax' => $project->marginSignedExcludingTax,
					'profitabilitySigned' => $project->profitabilitySigned,
					'marginOrderedExcludingTax' => $marginOrderedExcludingTax,
					'profitabilityOrdered' => $profitabilityOrdered,
					'marginProductionExcludingTax' => $project->marginProductionExcludingTax,
					'profitabilityProduction' => $project->profitabilityProduction,
					'marginInvoicedExcludingTax' => $marginInvoicedExcludingTax,
					'profitabilityInvoiced' => $profitabilityInvoiced,
					'marginPaidExcludingTax' => $marginPaidExcludingTax,
					'profitabilityPaid' => $profitabilityPaid,
					'stateInvoices' => $invoiceSummary
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
