<?php
/**
 * productivity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveReadAccessOnField;
use BoondManager\Lib\AbstractController;
use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Delivery;
use BoondManager\Services\CurrentUser;
use BoondManager\Services;
use BoondManager\APIs\DeliveriesInactivitiesGroupments\Filters;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Project;

/**
 * Class Productivity
 * @package BoondManager\APIs\Projects
 */
class Productivity extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'title',
		'startDate',
		'endDate',
		'regularTimesSimulated',
		'regularTimesProduction',
		'exceptionalTimesProduction',
		'exceptionalCalendarsProduction',
		'expensesSimulated',
		'expensesProduction',
		'turnoverProductionExcludingTax',
		'costsProductionExcludingTax',
		'dependsOn' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get project's productivity data
	 */
	public function api_get() {
		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_PRODUCTIVITY);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Project::TAB_PRODUCTIVITY), $project);

		$filter = new Filters\SearchDeliveries();
		$filter->disableMaxResultLimit();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(Filters\SearchDeliveries::ORDERBY_GROUPMENT_ID);
		$filter->order->setDefaultValue(AbstractSearch::ORDER_ASC);
		$filter->showGroupment->setDefaultValue(true);
		$filter->sumAdditionalData->setDefaultValue(false);
		//$filter->projectTypes->setValue($project->typeOf);
		$filter->setData($this->requestAccess->getParams('sort', 'order', 'maxResults', 'page'));
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\DeliveriesInactivitiesGroupments::search($filter);

		$result->rows = Services\Projects::attachDeliveriesProductivity($project, $result->rows);

		list($turnoverSignedExcludingTax, $costsSignedExcludingTax, $marginSignedExcludingTax, $profitabilitySigned,
			$turnoverProductionExcludingTax, $costsProductionExcludingTax, $marginProductionExcludingTax, $profitabilityProduction,
			$turnoverResourcesExcludingTax, $costsResourcesExcludingTax,
			$turnoverPurchasesAndAdditionalExcludingTax, $costsPurchasesAndAdditionalExcludingTax) = Services\Projects::calculateFinancialResumeForProductivity($project, $result->rows);



		foreach($result->rows as $entity) {
			/**
			 * @var Delivery $entity
			 */
			$request = new RequestAccess();
			$request->setData($entity);
			$request->setUser(CurrentUser::instance());

			$forbiddenFields = [];
			foreach(['delivery.expensesSimulated', 'delivery.expensesProduction', 'delivery.turnoverProductionExcludingTax', 'delivery.costsProductionExcludingTax'] as $field)
				if(! (new HaveReadAccessOnField($field))->isSatisfiedBy($this->requestAccess) ) $forbiddenFields[] = $field;
			$entity->removeFields($forbiddenFields)->filterFields(self::ALLOWED_FIELDS);
		}

		$result->rows = array_filter($result->rows, function($d){
			/** @var Delivery $d */
			return !$d->isGroupment;
		});
		$result->total = count($result->rows);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		if( (new HaveReadAccessOnField('turnoverSignedExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['turnoverSignedExcludingTax'] = $turnoverSignedExcludingTax;
		if( (new HaveReadAccessOnField('costsSignedExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['costsSignedExcludingTax'] = $costsSignedExcludingTax;
		if( (new HaveReadAccessOnField('marginSignedExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['marginSignedExcludingTax'] = $marginSignedExcludingTax;
		if( (new HaveReadAccessOnField('profitabilitySigned'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['profitabilitySigned'] = $profitabilitySigned;
		if( (new HaveReadAccessOnField('turnoverProductionExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['turnoverProductionExcludingTax'] = $turnoverProductionExcludingTax;
		if( (new HaveReadAccessOnField('costsProductionExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['costsProductionExcludingTax'] = $costsProductionExcludingTax;
		if( (new HaveReadAccessOnField('marginProductionExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['marginProductionExcludingTax'] = $marginProductionExcludingTax;
		if( (new HaveReadAccessOnField('profitabilityProduction'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['profitabilityProduction'] = $profitabilityProduction;
		if( (new HaveReadAccessOnField('turnoverResourcesExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['turnoverResourcesExcludingTax'] = $turnoverResourcesExcludingTax;
		if( (new HaveReadAccessOnField('costsResourcesExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['costsResourcesExcludingTax'] = $costsResourcesExcludingTax;
		if( (new HaveReadAccessOnField('turnoverPurchasesAndAdditionalExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['turnoverPurchasesAndAdditionalExcludingTax'] = $turnoverPurchasesAndAdditionalExcludingTax;
		if( (new HaveReadAccessOnField('costsPurchasesAndAdditionalExcludingTax'))->isSatisfiedBy($this->requestAccess) ) $tabData['meta']['totals']['costsPurchasesAndAdditionalExcludingTax'] = $costsPurchasesAndAdditionalExcludingTax;

		$this->sendJSONResponse($tabData);
	}
}
