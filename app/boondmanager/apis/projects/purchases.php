<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Purchases\Filters\SearchPurchases;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Project;
use BoondManager\Services;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;

class Purchases extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'title',
		'subscription',
		'typeOf',
		'reference',
		'state',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'totalAmountExcludingTax',
		'deltaExcludingTax',
		'numberOfCorrelatedOrders',
		'engagedPaymentsAmountExcludingTax',
		'canReadPurchase',
		'canWritePurchase',
		'mainManager' => [
			'id',
			'firstName',
			'lastName',
		],
		'company' => [
			'id',
			'name',
		],
		'contact' => [
			'id',
			'firstName',
			'lastName',
		]
	];

	const ALLOWED_FIELDS_FOR_GET = [
		'title',
		'reference',
	];

	const ALLOWED_FIELDS_FOR_DEFAULT = [
		'state',
		'paymentTerm',
		'paymentMethod',
		'taxRate',
		'createPayments',
		'currency',
		'company' => 'basic',
		'contact' => 'basic',
		'project' => 'basic',
	];

	public function api_get() {

		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_PURCHASES);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Project::TAB_PURCHASES), $project);

		$filter = new SearchPurchases();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\Purchases::search($filter);
		Services\Purchases::attachRights($result->rows);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'amountExcludingTax' => $result->totalAmountExcludingTax,
					'deltaExcludingTax' => $result->deltaExcludingTax,
					'engagedPaymentsAmountExcludingTax' => $result->engagedPaymentsAmountExcludingTax
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
