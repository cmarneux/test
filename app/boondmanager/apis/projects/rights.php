<?php
/**
 * rights.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Projects;

/**
 * Class Rights
 * @package BoondManager\APIs\Projects
 */
class Rights extends AbstractController {
	/**
	 * Get project's rights
	 */
	public function api_get() {
		$project = Projects::get($this->requestAccess->id);
		if(!$project) $this->error(404);

		$this->sendJSONResponse([
			'data' => Projects::getRights($project)
		]);
	}
}
