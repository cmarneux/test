<?php
/**
 * simulation.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects;

use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\APIs\Projects\Specifications\HaveReadAccessOnField;
use BoondManager\APIs\Projects\Specifications\HaveWriteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Projects;
use BoondManager\Services;
use BoondManager\APIs\Projects\Filters;
use BoondManager\Models\Project;

/**
 * Class Simulation
 * @package BoondManager\APIs\Projects
 */
class Simulation extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'showProductivityPerDelivery',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'additionalTurnoverAndCosts' => [
			'id',
			'turnoverExcludingTax',
			'costsExcludingTax',
			'title',
			'date',
			'state',
			'purchase' => [
				'id',
				'subscription'
			]
		],
		'projectManagers' => [
			'id'
		],
		'resources' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	public function getJSONData(Project $project) {
		$forbiddenFields = [];
		foreach(['showProductivityPerDelivery', 'projectManagers', 'resources'] as $field)
			if(! (new HaveReadAccessOnField($field))->isSatisfiedBy($this->requestAccess) ) $forbiddenFields[] = $field;
		return $project->removeFields($forbiddenFields)->filterFields(self::ALLOWED_FIELDS);
	}

	/**
	 * Get project's simulation data
	 */
	public function api_get() {
		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_SIMULATION);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveReadAccess(Project::TAB_SIMULATION), $project);

		$this->sendJSONResponse([
			'data' => $this->getJSONData($project)
		]);
	}

	/**
	 * Update project's simulation data
	 */
	public function api_put() {
		$project = Services\Projects::get($this->requestAccess->id, Project::TAB_SIMULATION);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Project::TAB_SIMULATION), $project);

		$filter = new Filters\Simulation($project);
		$filter->setData($this->requestAccess->get('data'));
		$this->checkFilter($filter);

		//Build project
		Projects::buildFromFilter($filter, $project);

		if(Services\Projects::update($project, Project::TAB_SIMULATION))
			$this->sendJSONResponse([
				'data' => $this->getJSONData($project)
			]);
		else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
