<?php
/**
 * abstractproject.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Services\CurrentUser;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Models\Project;
use BoondManager\Lib\RequestAccess;
use Wish\Tools;

abstract class AbstractProject extends AbstractSpecificationItem {
    /**
     * Get the project from the request
     * @param RequestAccess $request
     * @return Project|null
     */
    public function getProject($request) {
        if($request->data instanceof Project) return $request->data;
        else return null;
    }

    /**
     * Is user a project manager ?
     * @param CurrentUser $user
     * @param Project $project
     * @return bool
     */
    protected function isProjectManager($user, $project) {
        return isset($project->projectManagers) && in_array($user->getEmployeeId(), Tools::buildMaping($project->projectManagers, 'id', 'id'));
    }

    /**
     * Is project's type allowed ?
     * @param array $allowedTypes
     * @param Project $project
     * @return bool
     */
    protected function isProjectTypeAllowed($allowedTypes, $project) {
        if(!is_array($allowedTypes)) $allowedTypes = [$allowedTypes];
        return in_array($project->mode, $allowedTypes);
    }

    /**
     * Get read & write access
     * @param string $tab
     * @param CurrentUser $user
     * @param Project $project
     * @return array [$read, $write]
     */
	protected function getReadWriteAccess($tab, $user, $project) {
        list($state, $read, $write) = [false, false, false];
        switch($tab) {
            case Project::TAB_INFORMATION:
                $state = true;
                break;
            case Project::TAB_BATCHES_MARKERS:
                $state = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_PRODUCT], $project);
                break;
            case Project::TAB_ACTIONS:
                $state = true;
                break;
            case Project::TAB_SIMULATION:
                $state = true;
                break;
            case Project::TAB_DELIVERIES:
                $state = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_PRODUCT], $project);
                break;
            case Project::TAB_ADVANTAGES:
                $state = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE], $project);
                break;
            case Project::TAB_PURCHASES:
                $state = true;
                break;
            case Project::TAB_PRODUCTIVITY:
                $state = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE], $project);
                break;
            case Project::TAB_ORDERS:
                $state = true;
                break;
            case BM::TAB_DEFAULT:
                $state = true;
                break;
			case Project::TAB_FLAGS:
				$state = true;
				break;
        }

        if($state) {
			$hierarchyAccess = $user->checkHierarchyAccess(
				$project,
				BM::MODULE_PROJECTS,
				$tab,
				[Project::TAB_INFORMATION, Project::TAB_FLAGS, Project::TAB_BATCHES_MARKERS, Project::TAB_ACTIONS, Project::TAB_SIMULATION, Project::TAB_DELIVERIES, Project::TAB_ORDERS]
			);

			switch ($hierarchyAccess) {
				case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY:
					$read = true;
					$write = true;
					break;
				case BM::PROFIL_ACCESS_READ_WRITE:
					$read = true;
					break;
			}

			if (!$read && in_array($tab, [Project::TAB_INFORMATION, Project::TAB_FLAGS, Project::TAB_BATCHES_MARKERS, Project::TAB_ACTIONS, Project::TAB_DELIVERIES, Project::TAB_PRODUCTIVITY]) && $this->isProjectManager($user, $project)) $read = true;
			if ($read && ($user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_PROJECTS) || in_array($tab, [Project::TAB_BATCHES_MARKERS, Project::TAB_PRODUCTIVITY]) && $this->isProjectManager($user, $project))) $write = true;
        }

        if(in_array($tab, [Project::TAB_FLAGS, Project::TAB_ACTIONS, Project::TAB_DELIVERIES, Project::TAB_ADVANTAGES, Project::TAB_PURCHASES, Project::TAB_PRODUCTIVITY, Project::TAB_ORDERS, BM::TAB_DEFAULT])) $write = false;
        return [$read, $write];
	}
}
