<?php
/**
 * HaveDeleteAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Models\Project;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveDeleteAccess
 * @package BoondManager\APIs\Projects\Specifications
 */
class HaveDeleteAccess extends AbstractProject {
    /**
     * @param RequestAccess $request
     * @return bool
     */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		//Check this API authorization (cf. V6 => boondmanagerController.php)
		if(!$user->isManager() || !$user->hasAccess(BM::MODULE_PROJECTS)) return false;

		//Check this API access (cf. V6 => ficheprojet.php => isAccessible & majView)
		return $user->hasRight(BM::RIGHT_DELETION, BM::MODULE_PROJECTS) &&
			   (new HaveWriteAccess(Project::TAB_INFORMATION))->isSatisfiedBy($request);
	}
}
