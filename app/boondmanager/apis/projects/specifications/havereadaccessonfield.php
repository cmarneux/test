<?php
/**
 * userhavereadaccessonfield.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Project;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveReadAccessOnField extends AbstractProject {

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request)
	{
		/** @var CurrentUser $user*/
		$user = $request->user;

		if($user->isGod()) return true;

		$project = $this->getProject($request);
		if(!$project) return false;

		$read = false;
		switch($this->field){
			case 'showProductivityPerDelivery':case 'projectManagers':case 'resources':
				$read = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_TA], $project);
				break;
			case 'numberOfCorrelatedOrders': case 'averageDailyPriceExcludingTax': case 'costsSimulatedExcludingTax': case 'profitabilitySimulated':
			case 'turnoverSignedExcludingTax': case 'costsSignedExcludingTax': case 'marginSignedExcludingTax': case 'profitabilitySigned':
			case 'turnoverForecastExcludingTax': case 'costsForecastExcludingTax': case 'marginForecastExcludingTax': case 'profitabilityForecast':
			case 'turnoverProductionExcludingTax': case 'costsProductionExcludingTax': case 'marginProductionExcludingTax': case 'profitabilityProduction':
			case 'turnoverResourcesExcludingTax': case 'costsResourcesExcludingTax':
			case 'turnoverPurchasesAndAdditionalExcludingTax': case 'costsPurchasesAndAdditionalExcludingTax':
			case 'delivery.expensesSimulated': case 'delivery.expensesProduction':case 'delivery.turnoverProductionExcludingTax': case 'delivery.costsProductionExcludingTax':
				list($read, $write) = $this->getReadWriteAccess(Project::TAB_SIMULATION, $user, $project);
				break;
			case 'synchronizeRemainsToBeDone': case 'allowCreateMarkersOnBatches': case 'updateProductivityDate': case 'remainsToBeDone': case 'batchesMarkers':
			case 'agency':
				$read = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE], $project);
				break;
			case 'currency': case  'exchangeRate': case  'currencyAgency':case  'exchangeRateAgency':
			case 'batchesMarkers.distributionRate': case 'batchesMarkers.turnoverSignedDistributedExcludingTax': case 'batchesMarkers.turnoverProgressDistributedExcludingTax':
				list($read, $write) = $this->getReadWriteAccess(Project::TAB_SIMULATION, $user, $project);
				break;
			case 'endDate':
				$read = !$this->isProjectTypeAllowed([BM::PROJECT_TYPE_RECRUITMENT], $project);
				break;
			case 'showBatchesMarkersTab':
				$read = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_PRODUCT], $project);
				break;
		}
		return $read;
	}
}
