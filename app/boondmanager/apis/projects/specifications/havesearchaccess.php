<?php
/**
 * HaveSearchAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveSearchAccess
 * @package BoondManager\APIs\Projects\Specifications
 */
class HaveSearchAccess extends AbstractProject{
	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		//Check this API authorization & access
		return $user->isManager() && $user->hasAccess(BM::MODULE_PROJECTS) || $user->isSimpleResource();
	}
}
