<?php
/**
 * HaveWriteAccess.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\Services\CurrentUser;

/**
 * Class HaveWriteAccess
 * @package BoondManager\APIs\Projects\Specifications
 */
class HaveWriteAccess extends AbstractProject {
    use TabBehavior;

    /**
     * @param RequestAccess $request
     * @return bool
     */
    public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
        $user = $request->user;
        if($user->isGod()) return true;

        //Check this API authorization (cf. V6 => boondmanagerController.php)
        if((!$user->isManager() || !$user->hasAccess(BM::MODULE_PROJECTS)) && !$user->isSimpleResource()) return false;

        //Check this API access (cf. V6 => ficheprojet.php => isAccessible & majView)
        $project = $this->getProject($request);
        if(!$project) return false;

        list($read, $write) = $this->getReadWriteAccess($this->getTab(), $user, $project);
        return $write;
    }
}
