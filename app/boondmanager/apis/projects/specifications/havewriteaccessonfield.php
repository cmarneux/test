<?php
/**
 * userhavewriteaccessonfield.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Project;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

class HaveWriteAccessOnField extends AbstractProject {

	private $field;

	public function __construct($field)
	{
		$this->field = $field;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 * @throws \Exception
	 */
	public function isSatisfiedBy($request)
	{
		/** @var CurrentUser $user*/
		$user = $request->user;

		if($user->isGod()) return true;

		$project = $this->getProject($request);

		$write = false;
		switch($this->field){
			case 'showProductivityPerDelivery':
			case 'projectManagers':
				$write = $project && $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_TA], $project);
				break;
			case 'synchronizeRemainsToBeDone':
			case 'allowCreateMarkersOnBatches':
			case 'remainsToBeDone':
			case 'batchesMarkers':
				$write = $project && $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE], $project);
				break;
			case 'batchesMarkers.distributionRate':
			case 'batchesMarkers.title':
				if($project && $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE], $project))
					list($read, $write) = $this->getReadWriteAccess(Project::TAB_SIMULATION, $user, $project);
				break;
			case 'startDate':
				$write = $project && $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_RECRUITMENT], $project);
				break;
			case 'endDate':
				$write = $project && $this->isProjectTypeAllowed([BM::PROJECT_TYPE_PACKAGE], $project);
				break;
			case 'creationDate':
				$write = $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_PROJECTS);
				break;
			case 'mainManager':
			case 'agency':
			case 'pole':
				$write = $user->hasRight(BM::RIGHT_ASSIGNMENT, BM::MODULE_PROJECTS);
				break;
		}
		return $write;
	}
}
