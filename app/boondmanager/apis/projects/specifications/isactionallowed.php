<?php
/**
 * isactionallowed.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Projects\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Models\Project;
use BoondManager\Lib\Specifications\HaveAccess;
use BoondManager\Services\CurrentUser;

/**
 * Class IsActionAllowed
 * @package BoondManager\APIs\Projects\Specifications
 */
class IsActionAllowed extends AbstractProject {
    private $action;

    /**
     * IsActionAllowed constructor.
     * @param string $action
     */
    public function __construct($action) {
        $this->action = $action;
    }

    /**
     * @param RequestAccess $request
     * @return bool
     */
    public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
        $user = $request->user;
        if($user->isGod()) return true;

        //cf. V6 => ficheprojet.php => majView
        $project = $this->getProject($request);
        if(!$project) return false;

        $allow = false;
        switch($this->action) {
            case 'share':
                $allow = $user->isManager() && (new HaveReadAccess(Project::TAB_INFORMATION))
                            ->isSatisfiedBy($request);
                break;
            case 'addAction':
                $allow = $this->isProjectManager($user, $project) ||
                         (new HaveWriteAccess(Project::TAB_INFORMATION))
                            ->isSatisfiedBy($request);
                break;
            case 'addPositioning':
                $allow = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE], $project) &&
                         (new HaveWriteAccess(Project::TAB_INFORMATION))
                            ->and_(new HaveAccess(BM::MODULE_RESOURCES))
                            ->isSatisfiedBy($request);
                break;
            case 'addPurchase':
                $allow = (new HaveWriteAccess(Project::TAB_INFORMATION))
                            ->and_(new HaveAccess(BM::MODULE_PURCHASES))
                            ->isSatisfiedBy($request);
                break;
            case 'addAdvantage':
                $allow = $this->isProjectTypeAllowed([BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE], $project) &&
                         (new HaveWriteAccess(Project::TAB_INFORMATION))
                            ->and_(new HaveAccess(BM::MODULE_RESOURCES, BM::MODULE_RESOURCES_ADMINISTRATIVE, BM::ACCESS_RESOURCES_ADMINISTRATIVES_READ_WRITE))
                            ->isSatisfiedBy($request);
                break;
            case 'addOrder':
                $allow = (new HaveWriteAccess(Project::TAB_INFORMATION))
                            ->and_(new HaveAccess(BM::MODULE_BILLING))
                            ->isSatisfiedBy($request);
                break;
            case 'seeActivityReports':
                $allow = $user->getEmployeeId() == $project->getManagerID() ||
                        !$this->isProjectManager($user, $project);
                break;
            case 'showBatchesMarkersTab':
                $allow = $project->showBatchesMarkersTab && ($user->isManager() || $this->isProjectManager($user, $project));
                break;
            case 'showAdvantagesTab':
                $allow = $user->isManager() && ($project->numberOfdvantages > 0);
                break;
            case 'showPurchasesTab':
                $allow = $user->isManager() && ($project->numberOfPurchases > 0);
                break;
        }
        return $allow;
    }
}
