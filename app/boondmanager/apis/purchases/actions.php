<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Purchases;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\Purchase;
use BoondManager\Services;
use BoondManager\APIs\Purchases\Specifications\HaveReadAccess;

class Actions extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'startDate',
		'typeOf',
		'text',
		'priority',
		'state',
		'numberOfFiles',
		'canReadAction',
		'canWriteAction',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		]
	];

	/**
	 * Get Purchase's actions
	 */
	public function api_get() {
		$project = Services\Purchases::get($this->requestAccess->id, Purchase::TAB_ACTIONS);
		if(!$project) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Purchase::TAB_ACTIONS), $project);

		$filter = new SearchActions();
		$filter->setData($this->requestAccess->getParams());
		$filter->keywords->setValue($project->getReference());

		$this->checkFilter($filter);

		$result = Services\Actions::search($filter);
		Services\Actions::attachRights($result->rows);

		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
