<?php
/**
 * attachedflags.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\Purchases;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Purchase;
use BoondManager\Services;
use BoondManager\APIs\Purchases\Specifications\HaveReadAccess;
use Wish\Models\Model;

/**
 * Class AttachedFlags
 * @package BoondManager\APIs\Projects
 */
class AttachedFlags extends AbstractController {
	const ALLOWED_FIELDS = [
		'id',
		'flag' => [
			'id',
			'name',
			'mainManager' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	/**
	 * Get project's attached flags
	 */
	public function api_get() {
		$purchase = null;

		$purchase = Services\Purchases::get($this->requestAccess->id, Purchase::TAB_FLAGS);
		if(!$purchase) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Purchase::TAB_FLAGS), $purchase);

		$result = Services\AttachedFlags::getAttachedFlagsFromEntity($purchase);
		$result->filterFields(self::ALLOWED_FIELDS);

		$this->sendJSONResponse([
			'meta' => [
				'totals' => [
					'rows' => $result->total
				]
			],
			'data' => $result->rows
		]);
	}
}
