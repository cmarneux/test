<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Purchases;

use BoondManager\Models\Contact;
use BoondManager\Models\Project;
use BoondManager\Models\Purchase;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\Services\BM;
use BoondManager\APIs\Purchases\Specifications\HaveReadAccess;
use BoondManager\APIs\Purchases\Specifications\HaveCreateAccess;
use BoondManager\APIs\Purchases\Specifications\HaveDeleteAccess;

class Entity extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'typeOf',
		'subscription',
		'mainManager' => [
			'id'
		],
		'agency' => [
			'id'
		],
		'pole' => [
			'id'
		],
	];

	const ALLOWED_FIELDS_FOR_GET = [
		'title',
		'reference',
	];

	const ALLOWED_FIELDS_FOR_DEFAULT = [
		'state',
		'paymentTerm',
		'paymentMethod',
		'taxRate',
		'createPayments',
		'currency',
		'company' => [
			'id',
			'name',
		],
		'contact' => [
			'id',
			'firstName',
			'lastName',
		],
		'project' => [
			'id',
			'reference',
		],
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing entity
			$entity = Services\Purchases::get($this->requestAccess->id, Purchase::TAB_DEFAULT);
			// if entity not found, throw an error
			if(!$entity) $this->error(404);
			$this->requestAccess->data = $entity;
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Purchase::TAB_INFORMATION), $entity );

			$ALLOWED_FIELDS = array_merge(self::ALLOWED_FIELDS,self::ALLOWED_FIELDS_FOR_GET);
		} else {
			$this->checkAccessWithSpec( new HaveCreateAccess(Purchase::TAB_DEFAULT) ); // TODO
			// get an empty entity for a future creation
			$contact = Services\Contacts::get($this->requestAccess->get('contact'), Contact::TAB_INFORMATION);
			if(!$contact) $this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE, 'No contact found, please check the value of the contact parameter');
			$project = Services\Projects::get($this->requestAccess->get('project'), Project::TAB_INFORMATION);
			if(!$project) $this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE, 'No project found, please check the value of the project parameter');
			//TODO développer Deliveries::get
			$delivery = $this->requestAccess->get('delivery');
//            $delivery = Services\Deliveries::get($this->requestAccess->get('delivery'), Delivery::TAB_INFORMATION);
//            if(!$delivery) $this->error(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE, 'No delivery found, please check the value of the delivery parameter');

			$entity = Services\Purchases::getNew($contact, $project, $delivery);

			$ALLOWED_FIELDS = array_merge(self::ALLOWED_FIELDS, self::ALLOWED_FIELDS_FOR_DEFAULT);
		}

		$tabData = [
			'data' => $entity->filterFields($ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_delete() {
		$this->error(BM::ERROR_GLOBAL_UPGRADE_REQUIRED); // TODO: nécéssite encore de gerer le updateMissionData dans le fichier local
		$entity = Services\Purchases::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess, $entity );

		$deleted = Services\Purchases::delete($entity);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
