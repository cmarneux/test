<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Purchases\Filters;

use BoondManager\APIs\Purchases\Specifications\HaveWriteAccessOnField;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Company;
use BoondManager\Models\Contact;
use BoondManager\Models\ContactDetails;
use BoondManager\Models\Delivery;
use BoondManager\Models\Project;
use BoondManager\Models\Purchase;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Lib\Filters\Inputs\Relationships;
use BoondManager\Lib\Filters\Inputs\Attributes;

/**
 * Class Information
 * @package BoondManager\Models\Filters\Profiles\Purchases
 * @property InputDate date
 * @property InputDate endDate
 * @property InputDate startDate
 * @property InputString title
 * @property InputString reference
 * @property InputString number
 * @property InputFloat amountIncludingTax
 * @property InputFloat amountExcludingTax
 * @property InputFloat taxRate
 * @property InputDict subscription
 * @property InputDict state
 * @property InputDict typeOf
 * @property InputBoolean toReinvoice
 * @property InputFloat reinvoiceRate
 * @property InputFloat reinvoiceAmountExcludingTax
 * @property InputInt paymentMethod
 * @property InputInt paymentTerm
 * @property Attributes\Currency currencyAgency
 * @property Attributes\ExchangeRate exchangeRateAgency
 * @property InputInt quantity
 * @property Relationships\Agency agency
 * @property Relationships\MainManager mainManager
 * @property InputRelationship pole
 * @property Relationships\Contact contact
 * @property Relationships\Company company
 * @property Relationships\Project project
 * @property Relationships\Delivery delivery
 * @property Relationships\ContactDetails billingDetail
 *
 * @method Project getProject
 * @method Company getCompany
 * @method Contact getContact
 * @method bool getToReinvoice
 * @method float getAmountExcludingTax
 * @method float getAmountIncludingTax
 * @method Delivery getDelivery
 * @method ContactDetails getBillingDetail
 */
class Information extends AbstractJsonAPI
{
	/**
	 * @var Purchase
	 */
	private $_purchase;

	/**
	 * Profil constructor.
	 * @param Purchase $purchase
	 */
	public function __construct($purchase = null)
	{
		parent::__construct();

		$this->_purchase = $purchase;

		$this->addInput( new InputDate('date') );
		$this->addInput( new InputDate('endDate') );
		$this->addInput( new InputDate('startDate') );

		$title = new InputString('title');
		$title->setMinLength(1)->setMaxLength(150);
		$this->addInput($title);

		$reference = new InputString('reference');
		$reference->setMinLength(1)->setMaxLength(250);
		$this->addInput($reference);

		$number = new InputString('number');
		$number->setMinLength(1)->setMaxLength(250);
		$this->addInput($number);

		$this->addInput( new InputDict('state', 'specific.setting.state.purchase') );
		$this->addInput( new InputDict('typeOf', 'specific.setting.typeOf.purchase') );
		$this->addInput( new InputDict('subscription', 'specific.setting.typeOf.subscription') );
		$this->addInput( new InputDict('paymentTerm', 'specific.setting.paymentTerm') );
		$this->addInput( new InputDict('paymentMethod', 'specific.setting.paymentMethod') );
		$this->addInput( new InputDict('taxRate', 'specific.setting.taxRate') );
		$this->addInput( new InputString('informationComments') );
		$this->addInput( new InputInt('quantity') );
		$this->addInput( new InputFloat('amountExcludingTax') );
		$this->addInput( new InputFloat('amountIncludingTax') );
		$this->addInput( new InputBoolean('toReinvoice') );

		$input = new InputFloat('reinvoiceRate');
		$input->setMax(100);
		$this->addInput($input);

		$this->addInput( new InputFloat('reinvoiceAmountExcludingTax') );
		$this->addInput( new InputBoolean('showInformationCommentsOnPDF') );
		$this->addInput( new Attributes\Currency() );
		$this->addInput( new Attributes\Currency('currencyAgency') );
		$this->addInput( new Attributes\ExchangeRate() );
		$this->addInput( new Attributes\ExchangeRate('exchangeRateAgency') );

		$input = new InputEnum('createPayments');
		$input->setAllowedValues([
			Purchase::CREATION_MANUALLY, Purchase::CREATION_AUTO_PLANNED, Purchase::CREATION_AUTO_CONFIRMED, Purchase::CREATION_AUTO_ORDERED
		]);
		$this->addInput($input);

		$this->addInput( new Relationships\MainManager() );
		$this->addInput( new Relationships\Agency() );
		$this->addInput( new Relationships\Pole() );
		$this->addInput( new Relationships\Company() );
		$this->addInput( new Relationships\Contact() );
		$this->addInput( new Relationships\Project() );
		$this->addInput( new Relationships\Delivery() );
		$this->addInput( new Relationships\ContactDetails('billingDetail') );

		if( $this->isCreation() ) {
			$this->adaptForCreation();
		} else {
			$this->adaptForEdition();
		}
	}

	/**
	 * is the filter used for a creation
	 * @return bool
	 */
	private function isCreation() {
		return !$this->_purchase || !$this->_purchase->id;
	}

	private function adaptForCreation() {
		$this->startDate->markDefaultValueAsDefined(true);
		$this->endDate->markDefaultValueAsDefined(true);

		$this->mainManager->setRequired(true);
		$this->agency->setRequired(true);
	}

	private function adaptForEdition() {
		// suppression des champs non editable en dehors de la création
		$this->currencyAgency->setDisabled(true);
		$this->exchangeRateAgency->setDisabled(true);
	}

	protected function postValidation() {

		$startDate = $this->startDate->isDefined()
			? $this->startDate->getDateTime()
			: ($this->isCreation()
				? new \DateTime()
				: \DateTime::createFromFormat($this->_purchase->startDate, 'Y-m-d')
			);

		$endDate = $this->endDate->isDefined()
			? $this->endDate->getDateTime()
			: ($this->isCreation()
				? new \DateTime()
				: \DateTime::createFromFormat($this->_purchase->endDate, 'Y-m-d')
			);

		if($startDate->getTimestamp() > $endDate->getTimestamp()) {
			$this->startDate->invalidate(Purchase::ERROR_ENDDATE_SUPERIOR_AT_STARTDATE);
			$this->endDate->invalidate(Purchase::ERROR_ENDDATE_SUPERIOR_AT_STARTDATE);
		}

		if( $this->subscription->getValue() == BM::SUBSCRIPTION_TYPE_UNITARY ) {
			if($this->startDate->isDefined()) $this->startDate->invalidateIfDebug(Purchase::ERROR_NO_STARTDATE_IF_UNITARY);
			if($this->endDate->isDefined()) $this->endDate->invalidateIfDebug(Purchase::ERROR_NO_ENDDATE_IF_UNITARY);
		}

		if( $this->getDelivery() ) {

			$disabledFields = [
				'quantity', 'typeOf', 'subscription', 'amountExcludingTax', 'amountIncludingTax',
				'toReinvoice', 'reinvoiceRate', 'reinvoiceAmountExcludingTax', 'project'
			];

			foreach($disabledFields as $field) {
				$this->$field->invalidate(Purchase::ERROR_FIELD_CANTBE_SET_WITH_DELIVERY);
			}

		}

		if($this->getAmountExcludingTax() > $this->getAmountIncludingTax()) {
			if($this->amountExcludingTax->isDefined()) $this->amountExcludingTax->invalidateIfDebug(Purchase::ERROR_AMOUNT_EXCLUDING_TAX_MUST_BE_INFERIOR_AT_INCLUDING_TAX);
			if($this->amountIncludingTax->isDefined()) $this->amountIncludingTax->invalidateIfDebug(Purchase::ERROR_AMOUNT_EXCLUDING_TAX_MUST_BE_INFERIOR_AT_INCLUDING_TAX);
		}

		if(!$this->taxRate->isDefined()) {
			$excTax = $this->getAmountExcludingTax();
			$incTax = $this->getAmountIncludingTax();
			if($excTax) $this->taxRate->setValue( ($incTax / $excTax - 1) * 100 );
		}

		if(!$this->getToReinvoice()) {
			if ($this->reinvoiceRate->isDefined() ) $this->reinvoiceRate->invalidateIfDebug(Purchase::ERROR_PURCHASE_IS_NOT_REINVOICED);
			if ($this->reinvoiceAmountExcludingTax->isDefined() ) $this->reinvoiceAmountExcludingTax->invalidateIfDebug(Purchase::ERROR_PURCHASE_IS_NOT_REINVOICED);
		} else {
			if( $this->reinvoiceAmountExcludingTax->isDefined() && $this->reinvoiceRate->isDefined() ) {
				$this->reinvoiceAmountExcludingTax->invalidateIfDebug(Purchase::ERROR_REINVOICED_AMOUNT_AND_RATE_CANT_BE_BOTH_SET);
				$this->reinvoiceRate->invalidateIfDebug(Purchase::ERROR_REINVOICED_AMOUNT_AND_RATE_CANT_BE_BOTH_SET);
			}
		}

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());
		if(!$this->isCreation()) $request->setData($this->_purchase);

		//Check the right to change mainManager
		if(!$this->isCreation() && $this->mainManager->isDefined() && !(new HaveWriteAccessOnField('mainManager'))->isSatisfiedBy($request))
			$this->mainManager->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		//Check the right to change agency
		if(!$this->isCreation() && $this->agency->isDefined() && !(new HaveWriteAccessOnField('agency'))->isSatisfiedBy($request))
			$this->agency->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		//Check the right to change pole
		if(!$this->isCreation() && $this->pole->isDefined() && !(new HaveWriteAccessOnField('pole'))->isSatisfiedBy($request))
			$this->pole->invalidateIfDebug(BM::ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET);

		if($this->getCompany() && !$this->getContact() || $this->getContact() && !$this->getCompany()) {
			if($this->company->isDefined()) $this->company->invalidate(Purchase::ERROR_COMPANY_AND_CONTACTS_MUST_BOTH_BE_SET);
			if($this->contact->isDefined()) $this->contact->invalidate(Purchase::ERROR_COMPANY_AND_CONTACTS_MUST_BOTH_BE_SET);
		}

		if($this->getBillingDetail() && !$this->getCompany()) {
			$this->billingDetail->invalidate(Purchase::ERROR_BILLING_DETAILS_REQUIRE_COMPANY);
		}
	}

	/**
	 * magic method to retrieve a field value or the value from the object
	 * @param string $name
	 * @param array $arguments
	 * @throws \Exception
	 */
	public function __call($name, array $arguments) {
		if(strlen($name) > 3 && substr($name, 0, 3) == 'get'){
			$this->getFieldIfDefined( lcfirst(substr($name, 3)) );
		} else {
			throw new \Exception("undefined method '$name'");
		}
	}

	/**
	 * @param $field
	 * @return mixed|null
	 */
	private function getFieldIfDefined($field) {
		return $this->$field->isDefined() ? $this->$field->getValue() : ($this->isCreation() ? null : $this->_purchase->$field);
	}
}
