<?php
/**
 * purchases.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\APIs\Purchases\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Purchases
 * @property InputMultiValues purchaseStates
 * @property InputMultiValues subscriptionTypes
 * @property InputMultiValues purchaseTypes
 * @property InputMultiValues paymentMethods
 * @property InputString period
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputMultiValues sort
 * @package BoondManager\APIs\Purchases\Filters
 */
class SearchPurchases extends AbstractSearch{

	const PERIMETER_MODULE = BM::MODULE_PURCHASES;

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_STATE = 'state',
		ORDERBY_TITLE = 'title',
		ORDERBY_PROJECT_REFERENCE = 'project.reference',
		ORDERBY_COMPANY_NAME = 'company.name',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
	/**#@-*/

	/**#@+
	 * @var int PERIODS
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_SUBSCRIPTION = 'subscription';
	/**#@-*/

	/**
	 * SearchPurchases constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addInput( new InputMultiDict('purchaseStates', 'specific.setting.state.purchase'));
		$this->addInput( new InputMultiDict('subscriptionTypes', 'specific.setting.typeOf.subscription'));
		$this->addInput( new InputMultiDict('purchaseTypes', 'specific.setting.typeOf.purchase'));
		$this->addInput( new InputMultiDict('paymentMethods', 'specific.setting.paymentMethod'));

		$input = new InputEnum('period');
		$input->setAllowedValues([
			self::PERIOD_CREATED,
			self::PERIOD_SUBSCRIPTION
		]);
		$this->addInput($input);

		$input = new InputDate('startDate');
		$input->setMode(InputValue::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$input = new InputDate('endDate');
		$input->setMode(InputValue::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->addInput($input);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_CREATIONDATE,
			self::ORDERBY_STATE,
			self::ORDERBY_TITLE,
			self::ORDERBY_PROJECT_REFERENCE,
			self::ORDERBY_COMPANY_NAME,
			self::ORDERBY_MAINMANAGER_LASTNAME
		]);
		$this->addInput($sort);
	}

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 *
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}
}
