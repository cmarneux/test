<?php
/**
 * purchases.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\APIs\Purchases;

use BoondManager\APIs\Purchases\Specifications\HaveCreateAccess;
use BoondManager\APIs\Purchases\Specifications\HaveSearchAccess;
use BoondManager\Lib\AbstractController;
use Wish\Models\Model;
use BoondManager\Services\BM;
use BoondManager\Services\Extraction;
use BoondManager\Services\Purchases;
use BoondManager\Services\CurrentUser;

/**
 * Purchases list controller.
 * @package Purchases
 * @namespace \BoondManager\Controllers\Search
 */
class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'title',
		'subscription',
		'typeOf',
		'reference',
		'state',
		'currency',
		'exchangeRate',
		'currencyAgency',
		'exchangeRateAgency',
		'totalAmountExcludingTax',
		'deltaExcludingTax',
		'engagedPaymentsAmountExcludingTax',
		'mainManager' => [
			'id',
			'firstName',
			'lastName',
		],
		'company' => [
			'id',
			'name',
		],
		'contact' => [
			'id',
			'firstName',
			'lastName',
		],
		'project' => [
			'id',
			'reference',
		],
	];

	/**
	 * Search purchases
	 */
	public function api_get() {
		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = new Filters\SearchPurchases();
		$filter->setAndFilterData($this->requestAccess->getParams());
		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			$service = new Extraction\Purchases('purchases.csv', $filter->encoding->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Purchases::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					'deltaExcludingTax' => $result->deltaExcludingTax,
					'engagedPaymentsAmountExcludingTax' => $result->engagedPaymentsAmountExcludingTax,
					'totalAmountExcludingTax' => $result->totalAmountExcludingTax,
				]
			],
			'data' => $result->rows
		];
		$this->sendJSONResponse($tabData);
	}

	/**
	 * Create a purchase
	 */
	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess );

		$filter = new Filters\Information();
		$filter->setData( $this->requestAccess->get('data') );
		$this->checkFilter( $filter );

		$entity = Purchases::buildFromFilter($filter);

		if( Purchases::create($entity) ) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(Information::ALLOWED_FIELDS)
			]);
		} else {
			$this->error( BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY );
		}
	}
}
