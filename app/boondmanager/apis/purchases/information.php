<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Purchases;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\Purchase;
use BoondManager\Services;
use BoondManager\APIs\Purchases\Specifications\HaveReadAccess;
use BoondManager\APIs\Purchases\Specifications\HaveWriteAccess;
use BoondManager\Services\BM;

class Information extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'startDate',
		'endDate',
		'title',
		'reference',
		'number',
		'state',
		'typeOf',
		'subscription',
		'paymentTerm',
		'paymentMethod',
		'taxRate',
		'informationComments',
		'quantity',
		'amountExcludingTax',
		'amountIncludingTax',
		'totalAmountExcludingTax',
		'totalAmountIncludingTax',
		'toReinvoice',
		'reinvoiceRate',
		'reinvoiceAmountExcludingTax',
		'showInformationCommentsOnPDF',
		'currency',
		'currencyAgency',
		'exchangeRate',
		'exchangeRateAgency',
		'additionalTurnoverAndCosts',
		'mainManager' => [
			'id',
			'lastName',
			'firstName'
		],
		'agency' => [
			'id',
			'name',
		],
		'pole' => [
			'id',
			'name',
		],
		'company' => [
			'id',
			'name',
		],
		'contact' => [
			'id',
			'firstName',
			'lastName',
		],
		'project' => [
			'id',
			'reference',
		],
		'delivery' => [
			'id',
			'title'
		],
		'files',
	];

	public function api_get() {
		if($id = $this->requestAccess->id) {
			// get an existing profil
			$entity = Services\Purchases::get($id, Purchase::TAB_INFORMATION);
			// if profil not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(Purchase::TAB_INFORMATION), $entity );
		} else $this->error(404);

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {
		$entity = Services\Purchases::get($this->requestAccess->id, Purchase::TAB_INFORMATION);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec(new HaveWriteAccess(Purchase::TAB_INFORMATION), $entity);

		$filter = new Filters\Information($entity);
		$filter->setData( $this->requestAccess->get('data') );

		$this->checkFilter($filter);

		Services\Purchases::buildFromFilter($filter, $entity);

		if(Services\Purchases::update($entity)) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			]);
		}else{
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
