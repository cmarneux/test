<?php
/**
 * payments.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\Purchases;

use BoondManager\APIs\Payments\Filters\SearchPayments;
use BoondManager\Models\Purchase;
use BoondManager\Lib\AbstractController;
use BoondManager\Services;
use BoondManager\APIs\Purchases\Specifications\HaveReadAccess;

class Payments extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'number',
		'performedDate',
		'startDate',
		'endDate',
		'state',
		'amountExcludingTax',
		'amountIncludingTax',
		'informationComments',
	];

	public function api_get() {
		$entity = Services\Purchases::get($this->requestAccess->id, Purchase::TAB_PAYMENTS);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveReadAccess(Purchase::TAB_PAYMENTS), $entity);

		$filter = new SearchPayments();
		$filter->setIndifferentPerimeter();
		$filter->sort->setDefaultValue(SearchPayments::ORDERBY_CREATIONDATE);
		$filter->order->setDefaultValue(SearchPayments::ORDER_DESC);
		$filter->setData($this->requestAccess->getParams());
		//on ecrase les keywords pour restreindre a la société
		$filter->keywords->setValue( $entity->getReference() );

		$this->checkFilter($filter);

		$result = Services\Payments::search($filter);
		$result->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
					"totalAmountExcludingTax" => $entity->totalAmountExcludingTax,
					"totalAmountIncludingTax" => $entity->totalAmountIncludingTax,
					"engagedPaymentsAmountExcludingTax" => $result->TOTAL_PAIEMENTSHT,
					"engagedPaymentsAmountIncludingTax" => $result->TOTAL_PAIEMENTSTTC,
					"deltaExcludingTax" => $result->TOTAL_CONSOMMESHT - $entity->totalAmountExcludingTax,
					"deltaIncludingTax" => $result->TOTAL_CONSOMMESTTC - $entity->totalAmountIncludingTax,
					"statePayments" => $result->getStatesAmounts()
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
