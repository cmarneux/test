<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Purchases;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\Purchases;

/**
 * Class Rights
 * @package BoondManager\APIs\Projects
 */
class Rights extends AbstractController {
	/**
	 * Get project's rights
	 */
	public function api_get() {
		$purchase = Purchases::get($this->requestAccess->id);
		if(!$purchase) $this->error(404);

		$this->sendJSONResponse([
			'data' => Purchases::getRights($purchase)
		]);
	}
}
