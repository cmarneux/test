<?php
/**
 * abstractopportunity.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Purchases\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Purchase;
use BoondManager\Lib\RequestAccess;

abstract class AbstractPurchase extends AbstractSpecificationItem{
	/**
	 * get the purchase from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Purchase|null
	 */
	public function getPurchase($request){
		if($request->data instanceof Purchase) return $request->data;
		else return null;
	}
}
