<?php
/**
 * havecreateaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Purchases\Specifications;
use BoondManager\Lib\RequestAccess;

/**
 * Class HaveCreateAccess
 *
 * Indicate if the user have the right to create a new Product
 */
class HaveCreateAccess extends AbstractPurchase
{
	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		return true;
	}
}
