<?php
/**
 * havereadaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Purchases\Specifications;

use BoondManager\Lib\Specifications\TabBehavior;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class HaveReadAccess extends AbstractPurchase{
	use TabBehavior;

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		return in_array($user->checkHierarchyAccess($this->getPurchase($request), BM::MODULE_PURCHASES), [BM::PROFIL_ACCESS_READ_WRITE, BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY]);
	}
}
