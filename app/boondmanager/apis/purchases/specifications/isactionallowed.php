<?php
/**
 * isactionallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Purchases\Specifications;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class IsActionAllowed
 * @package BoondManager\APIs\Inactivities\Specifications
 */
class IsActionAllowed extends AbstractPurchase
{
	const AVAILABLE_ACTIONS = ['share', 'addPayment', 'dowloadClientPDF'];

	private $action;

	/**
	 * IsActionAllowed constructor.
	 * @param string $action
	 */
	public function __construct($action) {
		$this->action = $action;
	}

	/**
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request) {
		/**
		 * @var CurrentUser $user
		 */
		$user = $request->user;
		if($user->isGod()) return true;

		$purchase = $this->getPurchase($request);
		if(!$purchase) return false;

		$allow = false;
		switch($this->action) {
			case 'share':
				$allow = $user->isManager() && (new HaveReadAccess(BM::TAB_DEFAULT))->isSatisfiedBy($request);
				break;
			case 'addPayment' :
				$allow = (new HaveWriteAccess(BM::TAB_DEFAULT))->isSatisfiedBy($request) && $purchase->quantity*$purchase->amountExcludingTax > 0;
				break;
			case 'downloadClientPDF' :
				$allow = $user->isManager() && (new HaveReadAccess(BM::TAB_DEFAULT))->isSatisfiedBy($request);
				break;
		}
		return $allow;
	}
}
