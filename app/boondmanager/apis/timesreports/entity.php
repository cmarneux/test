<?php
/**
 * timesreport.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\APIs\TimesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\TimesReport;
use BoondManager\Services;
use BoondManager\Services\BM;
use BoondManager\APIs\TimesReports\Specifications\HaveCreateAccess;
use BoondManager\APIs\TimesReports\Specifications\HaveDeleteAccess;
use BoondManager\APIs\TimesReports\Specifications\HaveReadAccess;
use BoondManager\APIs\TimesReports\Specifications\HaveWriteAccess;

/**
 * TimesReport controller.
 * @package TimesReports
 * @namespace \BoondManager\Controllers\Profiles
 */
class Entity extends AbstractController {
	const INCLUDED_BATCH = [
		'id',
		'title',
	];

	const INCLUDED_DELIVERY = [
		'id',
		'title',
		'startDate',
		'endDate',
	];

	const INCLUDED_PROJECT = [
		'id',
		'reference',
		'company' => [
			'id',
			'name',
		],
		'deliveries' => self::INCLUDED_DELIVERY,
		'batches' => self::INCLUDED_BATCH,
	];

	const ALLOWED_FIELDS = [
		'id',
		'informationComments',
		'closed',
		'term',
		'validationWorkflow' => [
			'id',
			'lastName',
			'firstName',
		],
		'regularTimes' => [
			'id',
			'startDate',
			'duration',
			'row',
			'workUnitType' => [
				'reference',
				'activityType',
				'name'
			],
			'delivery' => [
				'id',
				'title',
				'startDate',
				'endDate',
			],
			'batch' => [
				'id',
				'title'
			],
			'project' => [
				'id',
				'reference'
			]
		],
		'exceptionalTimes' => [
			'id',
			'startDate',
			'endDate',
			'duration',
			'recovering',
			'description',
			'workUnitType' => [
				'reference',
				'activityType',
				'name'
			],
			'delivery' => [
				'id',
				'title',
				'startDate',
				'endDate',
			],
			'batch' => [
				'id',
				'title'
			],
			'project' => [
				'id',
				'reference'
			]
		],

		'resource' => [
			'id',
			'lastName',
			'firstName',
			'function',
			'account' => [
				'id',
				'workUnitRate',
				'allowExceptionalTimes'
			]
		],
		'agency' => [
			'id',
			'name',
			'workUnitTypes',
			'workUnitRate'
		],
		'expensesReport' => [
			'id'
		],
		'projects' => self::INCLUDED_PROJECT,
		'files',
		'orders' => [
			'id',
		],
		'validations',
	];

	/**
	 * Get information data on a timesreport
	*/
	public function api_get() {
		if($id = $this->requestAccess->id) {
			// gets existing
			$entity = Services\TimesReports::get($id);
			// if not found, throw an error
			if(!$entity) $this->error(404);
			// checking read access (if none found, throw an error)
			$this->checkAccessWithSpec( new HaveReadAccess(TimesReport::TAB_DEFAULT), $entity );

			$warnings = Services\TimesReports::getWarnings($entity);
		} else {
			// retrieve filtered params
			$filter = new Filters\GetDefault();
			$filter->setData( $this->requestAccess->getParams() );

			// check params
			$this->checkFilter($filter);

			$this->checkAccessWithSpec( new HaveCreateAccess ); // TODO

			$entity = Services\TimesReports::getNew($filter);
		}

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		if(isset($warnings)) $tabData['meta'] = ['warnings' => $warnings];

		$this->sendJSONResponse($tabData);
	}

	/**
	 * Update a timesreport's data.
	 */
	public function api_put()
	{
		$entity = Services\TimesReports::get($this->requestAccess->id);
		// if entity not found, throw an error (404 not found)
		if (!$entity) $this->error(404);

		// checking read access (if none found, throw an error 403 forbidden access)
		$this->checkAccessWithSpec(new HaveWriteAccess(TimesReport::TAB_DEFAULT), $entity);

		$filter = new Filters\SaveInformation($entity);
		$filter->setData($this->requestAccess->get('data'));

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		$entity = Services\TimesReports::buildFromFilter($filter, $entity);

		if (Services\TimesReports::update($entity)) {
			$tabData = [
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			];
			$this->sendJSONResponse($tabData);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}

	}

	public function api_delete() {
		$entity = Services\TimesReports::get($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $entity );

		$deleted = Services\TimesReports::delete($entity);

		$this->sendJSONResponse([
			'data' => [
				'success' => $deleted
			]
		]);
	}
}
