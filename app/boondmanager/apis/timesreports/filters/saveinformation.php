<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\TimesReports\Filters;

use BoondManager\Lib\Filters\ExceptionalTime;
use BoondManager\Lib\Filters\Inputs\InputMonth;
use BoondManager\Lib\Filters\Inputs\Relationships\Agency;
use BoondManager\Lib\Filters\Inputs\Relationships\Employee;
use BoondManager\Lib\Filters\RegularTime;
use BoondManager\Models\TimesReport;
use BoondManager\Models\WorkUnitType;
use BoondManager\Services\Agencies;
use BoondManager\Services\BM;
use BoondManager\Services\Employees;
use BoondManager\Models;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use Wish\Tools;

/**
 * Class SaveInformation
 * @package BoondManager\APIs\TimesReports\Filters
 * @property InputString informationComments
 * @property InputMultiObjects regularTimes
 * @property InputMultiObjects exceptionalTimes
 * @property InputMonth term
 * @property Employee resource
 * @property Agency agency
 */
class SaveInformation extends AbstractJsonAPI{

	/**
	 * @var TimesReport|null
	 */
	private $_timeReport;

	/**
	 * Profil constructor.
	 * @param TimesReport $timeReport
	 */
	public function __construct($timeReport = null){

		$this->_timeReport = $timeReport;

		parent::__construct();

		$this->addInput( new InputString('informationComments') );

		$times = new InputMultiObjects('regularTimes', new RegularTime());
		$times->setMode( InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE );
		$this->addInput( $times );

		$times = new InputMultiObjects('exceptionalTimes', new ExceptionalTime());
		$times->setMode( InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE );
		$this->addInput( $times );

		$this->addInput( new InputMonth('term') );

		$this->addInput( new Employee() );
		$agency = new Agency();
		$agency->addFilterCallback(function($value){
			return Agencies::get($value->id, Models\Agency::TAB_ACTIVITYEXPENSES);
		});
		$this->addInput( $agency );

		if( $this->isCreation() ) {
			$this->adaptForCreation();
		} else {
			$this->adaptForEdition();
		}
	}

	private function isCreation() {
		return !$this->_timeReport;
	}

	private function adaptForEdition() {
		$this->resource->setDisabled(true);
		$this->agency->setDisabled(true);
		$this->term->setDisabled(true);
	}

	private function adaptForCreation() {
		$this->term->setRequired(true);
		$this->agency->setRequired(true);
		$this->resource->setRequired(true);
	}

	/**
	 * @return \BoondManager\Models\Employee
	 */
	private function getResource() {
		if($this->isCreation()) {
			return $this->resource->getValue();
		} else {
			return $this->_timeReport->resource;
		}
	}

	/**
	 * @return string
	 */
	private function getStartPeriod() {
		if($this->isCreation()) {
			$report = new TimesReport(['term' => $this->term->getValue()]);
			return $report->getStartPeriod();
		} else {
			return $this->_timeReport->getStartPeriod();
		}
	}

	/**
	 * @return false|string
	 */
	private function getEndPeriod() {
		if($this->isCreation()) {
			$report = new TimesReport(['term' => $this->term->getValue()]);
			return $report->getEndPeriod();
		} else {
			return $this->_timeReport->getEndPeriod();
		}
	}

	protected function postValidation() {

		$agency = $this->isCreation() ? $this->agency->getValue() : $this->_timeReport->agency;

		/** @var ExceptionalTime $model */
		$model = $this->exceptionalTimes->getModel();
		$model->workUnitType->onlyExceptionalWorkUnit( $agency->workUnitTypes );

		/** @var RegularTime $model */
		$model = $this->regularTimes->getModel();
		$model->workUnitType->setAllowedWorkUnitTypes( $agency->workUnitTypes );

		$projects = Employees::getAllProjects( $this->getResource()->id, $this->getStartPeriod(), $this->getEndPeriod());

		$projectsIDs = Tools::getFieldsToArray($projects, 'id');

		$deliveriesIDs = [];
		$batchesIDs = [];

		foreach($projects as $project) {
			$deliveriesIDs = array_merge($deliveriesIDs, Tools::getFieldsToArray($project->deliveries, 'id'));
			$batchesIDs = array_merge($batchesIDs, Tools::getFieldsToArray($project->batches, 'id'));
		}

		foreach( $this->regularTimes->getItems() as $times) {
			/** @var RegularTime $times */
			/** @var WorkUnitType $wut */
			$wut = $times->workUnitType->getWorkUnitType();

			if($times->project->isDefined() && $times->project->getValue()) {
				if($wut && $wut->isAbsenceOrInternal()) {
					$times->project->invalidate(TimesReport::ERROR_PROJECT_INCOMPATIBLE_WITH_WORKUNITTYPE);
				} else if(!in_array($times->project->id->getValue(), $projectsIDs)) {
					$times->project->id->invalidate(TimesReport::ERROR_PROJECT_ID_NOT_ALLOWED);
				}
			}

			if($times->delivery->isDefined() && $times->delivery->getValue()) {
				if ($wut && $wut->isAbsenceOrInternal()) {
					$times->delivery->invalidate(TimesReport::ERROR_DELIVERY_INCOMPATIBLE_WITH_WORKUNITTYPE);
				} else if (!in_array($times->delivery->id->getValue(), $deliveriesIDs)) {
					$times->delivery->id->invalidate(TimesReport::ERROR_DELIVERY_ID_NOT_ALLOWED);
				}
			}

			if($times->batch->isDefined() && $times->batch->getValue()) {
				if ($wut && $wut->isAbsenceOrInternal()) {
					$times->batch->invalidate(TimesReport::ERROR_BATCH_INCOMPATIBLE_WITH_WORKUNITTYPE);
				} else if (!in_array($times->batch->id->getValue(), $batchesIDs)) {
					$times->batch->id->invalidate(TimesReport::ERROR_BATCH_ID_NOT_ALLOWED);
				}
			}
		}

		foreach( $this->exceptionalTimes->getItems() as $times) {
			/** @var ExceptionalTime $times */

			if($times->project->isDefined() && $times->project->getValue() && !in_array($times->project->id->getValue(), $projectsIDs)) {
				$times->project->id->invalidate(TimesReport::ERROR_PROJECT_ID_NOT_ALLOWED);
			}

			if($times->delivery->isDefined() && $times->delivery->getValue() && !in_array($times->delivery->id->getValue(), $deliveriesIDs)) {
				$times->delivery->id->invalidate(TimesReport::ERROR_DELIVERY_ID_NOT_ALLOWED);
			}

			if($times->batch->isDefined() && $times->batch->getValue() && !in_array($times->batch->id->getValue(), $batchesIDs)) {
				$times->batch->id->invalidate(TimesReport::ERROR_BATCH_ID_NOT_ALLOWED);
			}
		}

		if($this->isCreation() && $this->resource->getValue()->agency->id != $this->agency->getValue()->id) {
			$this->agency->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}
	}
}
