<?php
/**
 * timesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\TimesReports\Filters;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Lib\Filters\Inputs\InputEndMonth;
use BoondManager\Lib\Filters\Inputs\InputMonth;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputMultiDict;
use Wish\Filters\Inputs\InputMultiEnum;
use Wish\Filters\Inputs\InputMultiValues;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Validation;

/**
 * Class TimesReports
 * @package BoondManager\Models\Filters\Search
 * @property InputBoolean closed
 * @property \Wish\Filters\Inputs\InputValue startMonth
 * @property \Wish\Filters\Inputs\InputValue endMonth
 * @property InputMultiValues sort
 * @property InputMultiValues validationStates
 * @property \Wish\Filters\Inputs\InputMultiValues resourceTypes
 * @property \Wish\Filters\Inputs\InputValue perimeterManagersType
 * @property InputBoolean fullExtract
 */
class SearchTimesReports extends AbstractSearch{

	/**#@+
	 * @var string ORDERBY
	 */
	const ORDERBY_LASTNAME = 'resource.lastName';
	const ORDERBY_STATE = 'state';
	const ORDERBY_TERM = 'term';
	const ORDERBY_VALIDATOR_LASTNAME = 'validator.lastName';
	const ORDERBY_DATE = 'date';
	/**#@-*/

	/**#@+
	 * @var string PERIMETER_TYPE
	 */
	const PERIMETER_TYPE_VALIDATOR = 'validator';
	const PERIMETER_TYPE_RESOURCE = 'resource';
	/**#@-*/

	/**
	 * TimesReports constructor.
	 */
	public function __construct(){
		parent::__construct();

		$closed = new InputBoolean('closed');
		$this->addInput($closed);

		$endMonth = new InputEndMonth('endMonth');
		$endMonth->setRequired(true);
		$this->addInput($endMonth);

		$startMonth = new InputMonth('startMonth');
		$startMonth->setRequired(true);
		$this->addInput($startMonth);

		$this->addInput(new InputMultiDict('resourceTypes', 'specific.setting.typeOf.resource'));

		$validationStates = new InputMultiEnum('validationStates');
		$validationStates->setAllowedValues([Validation::VALIDATION_WAITING, Validation::VALIDATION_VALIDATED, Validation::VALIDATION_REJECTED]);
		$this->addInput($validationStates);

		$sort = new InputMultiEnum('sort');
		$sort->setAllowedValues([
			self::ORDERBY_TERM, self::ORDERBY_STATE, self::ORDERBY_LASTNAME, self::ORDERBY_VALIDATOR_LASTNAME, self::ORDERBY_DATE
		]);
		$this->addInput($sort);

		$fullExtract = new InputBoolean('fullExtract');
		$fullExtract->setModeDefaultValue(false);
		$this->addInput($fullExtract);

		$perimeterManagersType = new InputEnum('perimeterManagersType', self::PERIMETER_TYPE_VALIDATOR);
		$perimeterManagersType->setModeDefaultValue(self::PERIMETER_TYPE_VALIDATOR);
		$perimeterManagersType->setAllowedValues([
			self::PERIMETER_TYPE_RESOURCE, self::PERIMETER_TYPE_VALIDATOR
		]);
		$this->addInput($perimeterManagersType);
	}
}
