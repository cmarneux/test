<?php
/**
 * timesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\APIs\TimesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Models\ExpensesReport;
use BoondManager\Services\BM;
use BoondManager\Services\Extraction;
use BoondManager\Services;
use BoondManager\OldModels\Specifications\RequestAccess\HaveRight;
use BoondManager\APIs\TimesReports\Specifications\HaveCreateAccess;
use BoondManager\APIs\TimesReports\Specifications\HaveSearchAccess;

class Index extends AbstractController {

	const ALLOWED_FIELDS = [
		'id',
		'term',
		'state',
		'agency' => [
			'id',
			'name'
		],
		'resource' => [
			'id',
			'firstName',
			'lastName'
		],
		'validations' => [
			'id',
			'date',
			'state',
			'expectedValidator' => [
				'id',
				'firstName',
				'lastName'
			]
		]
	];

	public function api_get() {

		$searchAccess = new HaveSearchAccess;

		$this->checkAccessWithSpec( $searchAccess );

		$filter = new Filters\SearchTimesReports();
		$filter->setData($this->requestAccess->getParams());

		$this->checkFilter($filter);


		if($filter->extraction->isDefined() && !$filter->extraction->isDisabled()) {
			throw new \Exception('TODO'); //TODO
			if( ! $searchAccess
				  ->and_( new HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_ACTIVITIES_EXPENSES))
				  ->isSatisfiedBy($this->requestAccess)
			){
				$this->error(403);
			}
			$service = new Extraction\TimesReports('times-reports.csv', $filter->encoding->getValue(), $filter->fullExtract->getValue());
			$service->extract($filter);
			return ;
		}

		$result = Services\TimesReports::search($filter);

		foreach($result->rows as $report) {
			/** @var ExpensesReport $report */
			$report->filterFields(self::ALLOWED_FIELDS);
		}

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post() {
		$this->checkAccessWithSpec( new HaveCreateAccess() );

		// retrieve filtered params
		$filter = new Filters\SaveInformation();
		$filter->setData($this->requestAccess->get('data'));

		// check params
		$this->checkFilter($filter);

		$entity = Services\TimesReports::buildFromFilter($filter);

		if(Services\TimesReports::create($entity)) {
			$this->sendJSONResponse([
				'data' => $entity->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
	}
}
