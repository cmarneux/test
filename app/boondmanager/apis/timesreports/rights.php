<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\TimesReports;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\TimesReports;

class Rights extends AbstractController{
	public function api_get(){
		$timesReport = TimesReports::get($this->requestAccess->id);
		if(!$timesReport) $this->error(404);

		$this->sendJSONResponse([
			'data' => TimesReports::getRights($timesReport)
		]);
	}
}
