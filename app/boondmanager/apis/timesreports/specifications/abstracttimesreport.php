<?php
/**
 * abstracttimesreport.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\TimesReports\Specifications;

use BoondManager\Lib\Specifications\AbstractActivity;
use BoondManager\Models\TimesReport;
use BoondManager\Lib\RequestAccess;

abstract class AbstractTimesReport extends AbstractActivity {
	/**
	 * get the timesreport from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\TimesReport|null
	 */
	public function getTimesReport($request){
		if($request->data instanceof TimesReport) return $request->data;
		else return null;
	}
}
