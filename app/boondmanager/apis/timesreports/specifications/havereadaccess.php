<?php
/**
 * UserHaveReadAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\TimesReports\Specifications;

use BoondManager\OldModels\Specifications\RequestAccess\UserTypeIs;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;


/**
 * Class CanCreateTimesReport
 *
 * Indicate if the user have the right to road a TimesReport
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveReadAccess extends AbstractTimesReport{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){
		// ! CONFORME V6 !
		$user = $request->getUser();
		$entity = $this->getTimesReport($request);

		if(!$entity) return false;

		if($user->isGod()) return true;

		if( !(new UserTypeIs([BM::USER_TYPE_MANAGER, BM::USER_TYPE_RESOURCE]))->isSatisfiedBy($request))
			return false;

		$isUserProfil = $user->getEmployeeId() == $entity->resource->id;

		// employee's case
		if($user->isIntranetEnabled() && $isUserProfil && $user->getAccount()->hasStartedActivity())
			return true;

		// manager's case
		if(!$user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES))
			return false;

		var_dump('testa');
		$access = $user->checkHierarchyAccess($entity, BM::MODULE_RESOURCES);
		switch ($access) {
			case BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY:
				return true;
			case BM::PROFIL_ACCESS_READ_WRITE:
			case BM::PROFIL_ACCESS_READ_ONLY:
				if($isUserProfil) {
					if ($entity->resource->isTopManager() || $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES)) return true;
				} else if ($user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES) && ($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || !$entity->resource->isManager() || $user->isMyManager($entity->resource->id))){
					return true;
				}
				break;
		}

		// checking special access
		$validators = $entity->getValidationWorkflow();
		foreach($validators as $entry){
			if(is_array($entry) && isset($entry['id']) && $entry['id'] == $user->getEmployeeId()) return true;
		}

		return false;
	}
}
