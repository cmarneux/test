<?php
/**
 * UserHaveSearchAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\TimesReports\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on absences
 *
 * @package BoondManager\Models\Specifications\RequestAccess\TimesReports
 */
class HaveSearchAccess extends AbstractTimesReport{

	/**
	* check if the object match the specification
	* @param RequestAccess $request
	* @return bool
	*/
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// CONFORME v6 !
		return $user->hasAccess(BM::MODULE_ACTIVITIES_EXPENSES);
	}
}
