<?php
/**
 * UserHaveWriteAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\TimesReports\Specifications;

use BoondManager\Lib\RequestAccess;

/**
 * Class CanWriteTimesReport
 *
 * Indicate if the user have the right to write into TimesReport
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveWriteAccess extends AbstractTimesReport{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the product is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->getUser();
		$entity = $this->getTimesReport($request);

		if(!$entity) return false;

		if($user->isGod()) return true;

		// nécéssite à minima un accès lecture
		if ( !(new HaveReadAccess())->isSatisfiedBy($request) ) return false;

		return $this->canSaveActivity($entity, $user);
	}
}
