<?php
/**
 * entity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Validations;

use BoondManager\APIs\TimesReports;
use BoondManager\APIs\AbsencesReports;
use BoondManager\APIs\ExpensesReports;
use BoondManager\APIs\Validations\Specifications\HaveDeleteAccess;
use BoondManager\Lib\AbstractController;
use BoondManager\Models\AbsencesReport;
use BoondManager\Models\ExpensesReport;
use BoondManager\Models\TimesReport;
use BoondManager\Services;
use BoondManager\Services\BM;

class Entity extends AbstractController{

	const ALLOWED_FIELDS = [
		'id',
		'date',
		'state',
		'reason',
		'realValidator' => [
			'id',
			'lastName',
			'firstName',
			'function'
		],
		'expectedValidator' => [
			'id',
			'lastName',
			'firstName',
			'function'
		],
		'dependsOn' => [
			'id'
		]
	];

	public function api_get() {
		// gets existing
		$entity = Services\Validations::getWithFullDependsOn($this->requestAccess->id);
		// if not found, throw an error
		if(!$entity) $this->error(404);
		// checking read access (if none found, throw an error)

		// TODO : move it to a specification
		switch (get_class($entity->dependsOn)) {
			case TimesReport::class: $this->checkAccessWithSpec(new TimesReports\Specifications\HaveReadAccess(TimesReport::TAB_DEFAULT), $entity->dependsOn); break;
			case ExpensesReport::class: $this->checkAccessWithSpec(new ExpensesReports\Specifications\HaveReadAccess(ExpensesReport::TAB_DEFAULT), $entity->dependsOn); break;
			case AbsencesReport::class: $this->checkAccessWithSpec(new AbsencesReports\Specifications\HaveReadAccess(AbsencesReport::TAB_DEFAULT), $entity->dependsOn); break;
			default: $this->error(403);
		}

		$tabData = [
			'data' => $entity->filterFields(self::ALLOWED_FIELDS)
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_put() {

		$entity = Services\Validations::getWithFullDependsOn($this->requestAccess->id);
		// if entity not found, throw an error (404 not found)
		if (!$entity) $this->error(404);

		// TODO : move it to a specification
		// checking write access (if none found, throw an error 403 forbidden access)
		switch (get_class($entity->dependsOn)) {
			case TimesReport::class: $this->checkAccessWithSpec(new TimesReports\Specifications\HaveReadAccess(TimesReport::TAB_DEFAULT), $entity->dependsOn); break;
			case ExpensesReport::class: $this->checkAccessWithSpec(new ExpensesReports\Specifications\HaveReadAccess(ExpensesReport::TAB_DEFAULT), $entity->dependsOn); break;
			case AbsencesReport::class: $this->checkAccessWithSpec(new AbsencesReports\Specifications\HaveReadAccess(AbsencesReport::TAB_DEFAULT), $entity->dependsOn); break;
			default: $this->error(403);
		}
		$filter = new Filters\SaveEntity($entity);
		$filter->setData($this->requestAccess->get('data'));

		// check the filter validity and throw an error if invalid
		$this->checkFilter($filter);

		$entity = Services\Validations::buildFromFilter($filter, $entity);

		if (Services\Validations::update($entity)) {
			$tabData = [
				'data' => $entity->filterFields(self::ALLOWED_FIELDS)
			];
			$this->sendJSONResponse($tabData);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}

	}

	public function api_delete() {
		$entity = Services\Validations::getWithFullDependsOn($this->requestAccess->id);
		if(!$entity) $this->error(404);

		$this->checkAccessWithSpec( new HaveDeleteAccess(), $entity );

		if( Services\Validations::delete($entity)) {
			$this->sendJSONResponse([
				'data' => [
					'success' => true
				]
			]);
		} else {
			$this->error( BM::ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY);
		}
	}
}
