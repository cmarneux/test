<?php
/**
 * saveentity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Validations\Filters;

use BoondManager\Lib\Filters\Inputs\Relationships\MainManager;
use BoondManager\Lib\Models\AbstractActivityReport;
use BoondManager\Models\Validation;
use BoondManager\Services\AbsencesReports;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\ExpensesReports;
use BoondManager\Services\Managers;
use BoondManager\Services\TimesReports;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;

/**
 * Class SaveEntity
 * @package BoondManager\APIs\Validations\Filters
 * @property InputDate date
 * @property InputEnum state
 * @property InputString reason
 * @property InputEnum reasonTypeOf
 * @property MainManager realValidator
 * @property MainManager expectedValidator
 * @property InputRelationship dependsOn
 */
class SaveEntity extends AbstractJsonAPI{

	protected $_objectClass = Validation::class;

	/**
	 * @var Validation|null
	 */
	private $_validation;

	/**
	 * SaveEntity constructor.
	 * @param Validation $validation
	 */
	public function __construct($validation = null) {
		$this->_validation = $validation;
		parent::__construct();

		$date = new InputDate('date');
		$this->addInput($date);

		$state = new InputEnum('state');
		$state->setAllowedValues( [Validation::VALIDATION_VALIDATED, Validation::VALIDATION_WAITING, Validation::VALIDATION_REJECTED] );
		$this->addInput($state);

		$reason = new InputString('reason');
		$this->addInput($reason);

		$reason = new InputEnum('reasonTypeOf');
		$reason->setAllowedValues([ Validation::REASON_TYPE_CORRECTION_FOR_ALL_VALIDATOR, Validation::REASON_TYPE_CORRECTION_FOR_PREVIOUS_VALIDATOR, Validation::REASON_TYPE_REFUSED]);
		$this->addInput($reason);

		$realValidator = new InputRelationship('realValidator');
		$realValidator->addFilterType('resource');
		$realValidator->setModeDefaultValue( CurrentUser::instance()->getAccount() );
		$realValidator->markDefaultValueAsDefined(true);
		$this->addInput($realValidator);

		$expectedValidator = new InputRelationship('expectedValidator');
		$expectedValidator->addFilterType('resource');
		$this->addInput($expectedValidator);

		$dependsOn = new InputRelationship('dependsOn');
		$dependsOn->addFilterType( ['absencesreport', 'expensesreport', 'timesreport'] );
		$dependsOn->addFilterCallback(function($value) {
			/** @var InputRelationship $this */
			switch($this->getType()) {
				case 'absencesreport': return AbsencesReports::get($value);
				case 'expensesreport': return ExpensesReports::get($value);
				case 'timesreport': return TimesReports::get($value);
				default: return false;
			}
		});
		$this->addInput($dependsOn);

		$this->isCreation() ? $this->adaptForCreation() : $this->adaptForEdition();
	}

	private function isCreation() {
		return !$this->_validation || !$this->_validation->id;
	}

	private function adaptForCreation() {
		$this->state->setRequired(true);
		$this->expectedValidator->setRequired(true);
		$this->dependsOn->setRequired(true);
	}

	private function adaptForEdition() {
		$this->expectedValidator->setDisabled(true);
		$this->dependsOn->setDisabled(true);
	}

	/**
	 * @return AbstractActivityReport
	 */
	private function getDependsOn() {
		return $this->isCreation() ? $this->dependsOn->getValue() : $this->_validation->dependsOn;
	}

	protected function postValidation()
	{
		$activity = $this->getDependsOn();

		$this->realValidator->addFilterCallback(function($value) use ($activity) {
			foreach( $activity->validationWorkflow as $act) {
				if( $act->id == $value ) {
					return Managers::get($value, $withFallback = true);
				}
			}
			return false;
		});

		$this->expectedValidator->addFilterCallback(function($value) use ($activity) {
			foreach( $activity->validationWorkflow as $act) {
				if( $act->id == $value ) {
					return Managers::get($value, $withFallback = true);
				}
			}
			return false;
		});
	}
}
