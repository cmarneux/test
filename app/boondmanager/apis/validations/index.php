<?php
/**
 * index.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Validations;

use BoondManager\Lib\AbstractController;
use BoondManager\Services\BM;
use BoondManager\Services\Validations;

class Index extends AbstractController{

	public function api_post() {

		$filter = new Filters\SaveEntity();
		$filter->setData( $this->requestAccess->get('data'));

		$this->checkFilter($filter);

		$validation = Validations::buildFromFilter($filter);

		if( Validations::create($validation) ) {
			$this->sendJSONResponse([
				'data' => $validation->filterFields(Entity::ALLOWED_FIELDS)
			]);
		} else {
			$this->error(BM::ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY);
		}
	}
}
