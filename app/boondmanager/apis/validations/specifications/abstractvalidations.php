<?php
/**
 * abstractvalidations.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\APIs\Validations\Specifications;

use BoondManager\Lib\Specifications\AbstractActivity;
use BoondManager\Models\Validation;
use BoondManager\Lib\RequestAccess;

abstract class AbstractValidations extends AbstractActivity {
	/**
	 * get the validation object from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Validation|null
	 */
	public function getValidation($request){
		if($request->data instanceof Validation) return $request->data;
		else return null;
	}
}
