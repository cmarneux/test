<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\APIs\Validations\Specifications;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class CanWriteTimesReport
 *
 * Indicate if the user have the right to write into TimesReport
 *
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractValidations
{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the positioning is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$entity = $this->getValidation($request);
		if (!$entity) return false;

		if ($user->isGod()) return true;

		// TODO : add havereadaccess check
		if( !$user->hasRight(BM::RIGHT_DELETION, BM::MODULE_ACTIVITIES_EXPENSES))
			return false;

		if( ! ($this->canDevalidate($entity->dependsOn, $user) || $this->canSaveActivity($entity->dependsOn, $user) || $this->canValidate($entity->dependsOn, $user) || $this->canReject($entity->dependsOn, $user) ) ){
			return false;
		}

		foreach($entity->dependsOn->validations as $val) {
			if($val->id == $entity->id) return true;
		}

		return false;
	}
}
