<?php
/**
 * expenses.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Controllers\Search;

use BoondManager\Lib\AbstractController;
use Wish\MySQL\AbstractDb;
use BoondManager\Services;
use BoondManager\OldModels\Filters;
use BoondManager\OldModels\Specifications\RequestAccess\Expenses\HaveSearchAccess;

class Expenses extends AbstractController{

    public function api_get() {

		$this->checkAccessWithSpec( new HaveSearchAccess);

        $filter = Filters\Search\Expenses::getUserFilter();
        $filter->setAndFilterData($this->requestAccess->getParams());

		$result = Services\Expenses::search($filter);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

        $this->sendJSONResponse($tabData);
    }

	public function api_post(){
		return $this->api_get();
	}
}
