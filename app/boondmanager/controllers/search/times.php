<?php
/**
 * times.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Controllers\Search;

use BoondManager\Lib\AbstractController;
use Wish\MySQL\AbstractDb;
use BoondManager\Services;
use BoondManager\OldModels\Filters;
use BoondManager\OldModels\Specifications\RequestAccess\Times\HaveSearchAccess;

class Times extends AbstractController{

	const ALLOWED_FIELDS = [
		'ID_TEMPS',
		'TEMPS_DATE',
		'TEMPS_DUREE',
		'TEMPS_TYPE',
		'ID_LIGNETEMPS',
		'TEMPSEXP_FIN',
		'TEMPSEXP_RECUPERATION',
		'TEMPSEXP_DESCRIPTION',
		'workUnitType',
		'ID_PROJET',
		'MP_NOM',
		'ID_LOT',
		'timesReport',
		'rules',
//        'batch',
		'delivery',
		'project',
	];

	public function api_get() {

		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = Filters\Search\Times::getUserFilter();
		$filter->setAndFilterData($this->requestAccess->getParams());

		$result = Services\Times::search($filter);

		foreach($result->rows as $entity) $entity->filterFields(self::ALLOWED_FIELDS);

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}

	public function api_post(){
		return $this->api_get();
	}
}
