<?php
/**
 * validations.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Controllers\Search;

use BoondManager\Lib\AbstractController;
use BoondManager\OldModels\Specifications\RequestAccess\Validations\HaveSearchAccess;
use BoondManager\OldModels\Filters;
use BoondManager\Services;

class Validations extends AbstractController{

	public function api_get(){

		$this->checkAccessWithSpec( new HaveSearchAccess);

		$filter = Filters\Search\Validations::getUserFilter($this->currentUser);
		$filter->setAndFilterData($this->requestAccess->getParams());

		$this->checkFilter($filter);

		$result = Services\Validations::search($filter);

		/*
		foreach($result->rows as $report){
			$report->resource->filterFields(['ID_PROFIL', 'PROFIL_NOM','PROFIL_PRENOM']);
		}
		*/

		$tabData = [
			'meta' => [
				'totals' => [
					'rows' => $result->total,
				]
			],
			'data' => $result->rows
		];

		$this->sendJSONResponse($tabData);
	}
}
