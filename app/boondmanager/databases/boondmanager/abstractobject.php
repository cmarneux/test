<?php
/**
 * abstractobject.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Databases\BoondManager;

use Wish\Models\SearchResult;
use Wish\MySQL\AbstractDb;
use BoondManager\Lib\MySQL\DbFactory;
use Wish\MySQL\Query;

/**
 * Class AbstractObject
 *
 * Use automatically the boondManager DB
 *
 * @package BoondManager\Databases\BoondManager;
 */
class AbstractObject extends AbstractDb {
    /**
     * AbstractObject constructor.
     */
    public function __construct(){
        $db = DbFactory::instance()->getDbBoondManager();
        parent::__construct($db);
    }

	/**
	 * do a search and store the number of all matching rows {@see self::$totalRows}
	 * @param Query $query a query used to perform a search
	 * @param string $resultClass class for the result
	 * @return SearchResult
	 */
	protected function launchSearch(Query $query, $resultClass = SearchResult::class) {
		$count = $this->singleExec($query->getCountQuery(), $query->getArgs());
		if($count && intval($count['NB_ROWS']) > 0) {
			$result = new $resultClass($this->exec($query), intval($count['NB_ROWS']), $count);
			return $result;
		} else return new $resultClass([], 0);
	}
}
