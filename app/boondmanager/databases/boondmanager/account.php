<?php
/**
 * account.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\BoondManager;

use BoondManager\Services\BM;
use Wish\Models\Model;
use Wish\MySQL\Query;

/**
 * Gestionnaire de traitement des utilisateurs en base de données.
 * @package BoondManager\Databases\BoondManager;
 */
class Account extends AbstractObject {
	/**
	 * Load an account given its login and password
	 * @param  string  $login
	 * @param  string  $pwd
	 * @return Model|false
	 */
	public function getAccountFromLoginAndPwd($login, $pwd) {
		$query = $this->getBasicAccountQuery();
		$query->addWhere('USER_ABONNEMENT='.BM::DB_ACCESS_ACTIVE.' AND USER_LOGIN=:login AND USER_PWD=:pwd', [':login' => $login, ':pwd' => $pwd]);
		return $this->singleExec($query);
	}

	/**
	 * Load an account given its id
	 * @param  integer $id
	 * @return Model|false
	 */
	public function getAccount($id) {
		$query = $this->getBasicAccountQuery();
		$query->addWhere('ID_USER=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * build a basic mysql query for account
	 * @return Query
	 */
	private function getBasicAccountQuery(){
		$query = new Query();
		$query->setColumns(['ID_USER', 'USER_LOGIN', 'USER_LOGIN', 'USER_CONNECTIONTYPE', 'USER_HOMEPAGE', 'USER_LANGUE', 'USER_DEFAULTSEARCH', 'USER_JOINCATEGORY',
							'USER_ABONNEMENT', 'USER_TYPE', 'USER_LASTCONNEXION', 'USER_SECURITYALERT', 'USER_SECURITYCOOKIE', 'USER_NOM', 'USER_PRENOM'])
			->from('TAB_USER');
		return $query;
	}

	/**
	 * Retrive login & password account data from its ID
	 * @param $id
	 * @return Model|false
	 */
	public function getLoginAndPwd($id) {
		$query = new Query();
		$query->setColumns(['ID_USER', 'USER_LOGIN', 'USER_PWD'])
			->from('TAB_USER');
		$query->addWhere('ID_USER=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * Create an account
	 * @param  array  $data
	 * The array can contains the following keys:
	 * - `USER` : an array with the account data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * @return integer the account ID
	 */
	public function createAccount($data) {
		if(isset($data['USER'])) {
			if(!isset($data['USER']['USER_LASTCONNEXION'])) $data['USER']['USER_LASTCONNEXION'] = 'NULL';
			if(!isset($data['USER']['USER_LANGUE'])) $data['USER']['USER_LANGUE'] = BM::getLanguage();

			$id = $this->insert('TAB_USER', $data['USER']);

			if ($id && isset($data['USER']['USER_LOGIN'])) {
				$dbLogin = new Login();
				$dbLogin->createLogin(['ID_USER' => $id, 'USER_LOGIN' => $data['USER']['USER_LOGIN']]);
			}
			return $id;
		}
		return false;
	}

	/**
	 * Update an account
	 * @param  array  $data
	 * The array can contains the following keys:
	 * - `USER` : an array with the account data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * - `PROFIL` : an array with the account Profil data, cf. [TAB_PROFIL](../../bddclient/classes/TAB_PROFIL.html)
	 * - `CONFIG` : an array with the account config data, cf. [TAB_USERCONFIG](../../bddclient/classes/TAB_USERCONFIG.html)
	 * @param  integer  $id
	 * @return integer the account ID
	 */
	public function updateAccount($data, $id) {
		if(isset($data['USER'])) {
			$this->update('TAB_USER', $data['USER'], 'ID_USER=:id', array(':id' => $id));
			if(isset($data['USER']['USER_LOGIN'])) {
				$dbLogin = new Login();
				$login = $dbLogin->getLogin($data['USER']['USER_LOGIN']);
				if($login)
					$dbLogin->updateLoginFromAccount(['USER_LOGIN' => $data['USER']['USER_LOGIN']], $id, BM::getCustomerCode());
				else
					$dbLogin->createLogin([
						'USER_LOGIN' => $data['USER']['USER_LOGIN'],
						'ID_USER' => $this->escape($id)
					]);
			}
		}
		return true;
	}

	/**
	 * Delete an account
	 * @param  integer $id
	 * @return boolean
	 */
	public function deleteAccount($id) {
		$dbLogin = new Login();
		$dbLogin->deleteLoginFromAccount($id);

		$this->delete('TAB_DEVICEALLOWED', 'ID_USER=?', $id);
		$this->delete('TAB_USER', 'ID_USER=?', $id);
		return true;
	}
}
