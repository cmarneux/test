<?php
/**
 * apps.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\BoondManager;

use BoondManager\APIs\Apps\Filters\SearchApps;
use BoondManager\Lib\Cluster;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use Wish\Models\Model;
use Wish\Models\SearchResult;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use BoondManager\Models;
use Wish\Tools;

/**
 * Gestionnaire de recherche des ressources en base de données.
 * @package BoondManager\Databases\BoondManager;
 */
class App extends AbstractObject {
	/**
	 * @param SearchApps $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchMainApps(SearchApps $filter) {
		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('ID_MAINAPI, MAINAPI_NOM, MAINAPI_TITRE, MAINAPI_DESCRIPTION, MAINAPI_CATEGORIE, MAINAPI_VALIDATION, MAINAPI_VISIBILITE, MAINAPI_WEBPRODUIT,
							 MAINAPI_URL, MAINAPI_KEY, VENDEUR_NOM, VENDEUR_WEB,
							 CLIENT_WEB, AVG(REVIEWS_NOTE) AS MAINAPI_NOTE');
		$query->from('TAB_MAINAPI');
		$query->addJoin('INNER JOIN TAB_VENDEUR USING(ID_VENDEUR) INNER JOIN TAB_CLIENT USING(ID_CLIENT) LEFT JOIN TAB_REVIEWS USING(ID_MAINAPI)');
		$query->groupBy('TAB_MAINAPI.ID_MAINAPI');

		$where = new Where();

		$keywordsMapping = ['API' => 'ID_MAINAPI', 'VENDOR' => 'ID_VENDEUR'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if(!$whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where();
			$whereKeywords->or_('MAINAPI_NOM LIKE ?', $likeSearch)
				->or_('VENDEUR_NOM LIKE ?', $likeSearch)
				->or_('MATCH(MAINAPI_DESCRIPTION) AGAINST(?)', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_($this->getFilterSearch($filter->typeOf->getValue(), 'MAINAPI_CATEGORIE'))
			->and_($this->getFilterSearch($filter->validation->getValue(), 'MAINAPI_VALIDATION'))
			->and_($this->getFilterSearch($filter->visibility->getValue(), 'MAINAPI_VISIBILITE'));

		if(BM::isCustomerInterfaceActive()) {
			if ($filter->myApps->getValue())
				$where->and_('CLIENT_WEB=?', BM::getCustomerCode());
			else
				$where->and_('CLIENT_WEB=? OR MAINAPI_VISIBILITE=?', [BM::getCustomerCode(), Models\App::VISIBILITY_ON]);
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

		$query->addWhere($where);

		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchApps::ORDERBY_NAME         => 'MAINAPI_NOM',
			SearchApps::ORDERBY_VALIDATION   => 'MAINAPI_VALIDATION',
			SearchApps::ORDERBY_VENDOR       => 'VENDEUR_NOM',
			SearchApps::ORDERBY_NOTE         => 'MAINAPI_NOTE'
		];

		if(!$column) {
			$query->addOrderBy('MAINAPI_NOTE DESC');
			$query->addOrderBy('ID_MAINAPI ASC');
		}

		foreach($column as $c) {
			if (array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c] . ' ' . $order);
			if(in_array($c, [SearchApps::ORDERBY_VENDOR, SearchApps::ORDERBY_NOTE]))
				$query->addOrderBy('ID_MAINAPI ASC');
		}
	}

    /**
     * get all apps details for the given ids
     * @param array $tabIds
     * @return Model[]
     */
    public function getSpecificMainApps($tabIds = []){
        $query = new Query();
        $query->groupBy('TAB_MAINAPI.ID_MAINAPI')
            ->setColumns(['ID_MAINAPI', 'MAINAPI_NOM', 'MAINAPI_TITRE', 'MAINAPI_DESCRIPTION', 'MAINAPI_CATEGORIE', 'MAINAPI_VALIDATION', 'MAINAPI_VISIBILITE', 'MAINAPI_WEBPRODUIT',
				'MAINAPI_URL', 'MAINAPI_KEY', 'VENDEUR_NOM', 'VENDEUR_WEB',
                'CLIENT_WEB', 'MAINAPI_NOTE'=>'AVG(REVIEWS_NOTE)'
            ])
            ->from('TAB_MAINAPI')
            ->addJoin('INNER JOIN TAB_VENDEUR USING(ID_VENDEUR) INNER JOIN TAB_CLIENT USING(ID_CLIENT) LEFT JOIN TAB_REVIEWS USING(ID_MAINAPI)');
        if($tabIds) $query->addWhere('ID_MAINAPI IN (?)', $tabIds);
        return $this->exec($query);
    }

    /*
     * TODO: revoir tout ce qui suit
     */

    /**
     * Retrieve all app data from its Web Connection URL
     * @param  string  $customerCode
     * @return Model|false
     */
    public function getMainAppFromCustomerCode($customerCode) {
		$query = $this->getBasicMainAppQuery();
		$query->addWhere('MAINAPI_WEB=?', $customerCode);
		$result = $this->singleExec($query);
		if($result) $result->REVIEWS = $this->getAllReviewsFromApp($result['ID_MAINAPI']);
		return $result;
    }

    /**
     * Retrieve all app data from its ID
     * @param  integer  $id
     * @return Model|false
     */
    public function getMainApp($id) {
		$query = $this->getBasicMainAppQuery();
		$query->addWhere('ID_MAINAPI=?', $id);
		$result = $this->singleExec($query);
		if($result) $result->REVIEWS = $this->getAllReviewsFromApp($result['ID_MAINAPI']);
		return $result;
    }

    /**
     * build a basic mysql query for app
     * @return Query
     */
    private function getBasicMainAppQuery() {
		$query = new Query();
		$query->setColumns(['ID_MAINAPI', 'MAINAPI_NOM', 'MAINAPI_TITRE', 'MAINAPI_KEY', 'MAINAPI_WEB', 'MAINAPI_DESCRIPTION', 'MAINAPI_URL', 'MAINAPI_HOSTSALLOWED', 'MAINAPI_APIALLOWED', 'MAINAPI_VERSION',
							'MAINAPI_CATEGORIE', 'MAINAPI_PRIX', 'MAINAPI_WEBPRODUIT', 'MAINAPI_WEBTEMOIGNAGES', 'MAINAPI_WEBCGV', 'MAINAPI_CREATION', 'MAINAPI_VISIBILITE', 'MAINAPI_VALIDATION',
							'ID_VENDEUR', 'VENDEUR_NOM', 'VENDEUR_WEB', 'VENDEUR_TEL', 'VENDEUR_CP', 'VENDEUR_ADR', 'VENDEUR_VILLE', 'VENDEUR_PAYS', 'VENDEUR_DESCRIPTION', 'VENDEUR_EMAIL', 'ID_CLIENT'])
			->from('TAB_MAINAPI')
			->addJoin('INNER JOIN TAB_VENDEUR USING(ID_VENDEUR) INNER JOIN TAB_CLIENT USING(ID_CLIENT)')
			->setLimit(1);
		return $query;
    }

    /**
     * Create an app
     * @param  array  $data
     * This array can contains the following keys:
     * - `MAINAPI` : app data, cf. [TAB_MAINAPI](../../bddclient/classes/TAB_MAINAPI.html)
     * @return integer App ID
     */
    public function createMainApp($data) {
        if(isset($data['MAINAPI']))
			return $this->insert('TAB_MAINAPI', $data['MAINAPI']);
        return false;
    }

	/**
	 * Update an app
	 * @param  array  $data
	 * This array can contains the following keys:
	 * - `MAINAPI` : app data, cf. [TAB_MAINAPI](../../bddclient/classes/TAB_MAINAPI.html)
	 * @param  integer  $id
	 * @return bool
	 */
	public function updateMainApp($data, $id) {
		if(isset($data['MAINAPI'])) $this->update('TAB_MAINAPI', $data['MAINAPI'], 'ID_MAINAPI=:id', array(':id' => $id));
		return true;
	}

    /**
     * Delete an app
     * @param  integer  $id
     * @return boolean
     */
    public function deleteMainApp($id) {
		$cluster = new Cluster();
		$imgFile = Tools::whichImageWithExtensionExist(BM::getRootPath().'/www/img/marketplace/logo/api/'.$id);
		$cluster->deleteFile('www/img/marketplace/logo/api/'.$imgFile);

        $this->delete('TAB_REVIEWS', 'ID_MAINAPI=?', $id);
        $this->delete('TAB_MAINAPI', 'ID_MAINAPI=?', $id);
        return true;
    }

	/**
	 * @param $id
	 * @return bool
	 */
	public function deleteAllMainAppsFromVendor($id) {
		$apps = $this->exec('SELECT ID_MAINAPI FROM TAB_MAINAPI WHERE ID_VENDEUR=?', $id);
		foreach($apps as $app) $this->deleteMainApp($app['ID_MAINAPI']);
		return true;
	}

	/**
	 * Retrieve all reviews from an app
	 * @param  integer  $id
	 * @return Model[]|false
	 */
	public function getAllReviewsFromApp($id) {
		$query = $this->getBasicReviewQuery();
		$query->addWhere('ID_MAINAPI=?', $id);
		return $this->exec($query);
	}

	/**
	 * Retrieve all app data from its ID
	 * @param  integer  $id
	 * @return Model|false
	 */
	public function getReview($id) {
		$query = $this->getBasicReviewQuery();
		$query->addWhere('ID_REVIEWS=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * build a basic mysql query for review
	 * @return Query
	 */
	private function getBasicReviewQuery() {
		$query = new Query();
		$query->setColumns(['ID_REVIEWS', 'REVIEWS_TITLE', 'REVIEWS_AUTEUR', 'REVIEWS_DATE', 'REVIEWS_DESCRIPTION', 'REVIEWS_NOTE'])
			->from('TAB_REVIEWS');
		return $query;
	}

    /**
     * Create an app review
     * @param  array  $data  Tableau des données.
     * @return integer Review ID
     */
    public function createReview($data) {
		return $this->insert('TAB_REVIEWS', $data);
    }

	/**
	 * Update an app review
	 * @param  array  $data
	 * @param  integer  $id
	 * @return bool
	 */
	public function updateReview($data, $id) {
		$this->update('TAB_REVIEWS', $data, 'ID_REVIEWS=:id', array(':id' => $id));
		return true;
	}

    /**
     * Delete a review
     * @param  integer  $id
     * @return boolean
     */
    public function deleteReview($id) {
        $this->delete('TAB_REVIEWS', 'ID_REVIEWS=?', $id);
        return true;
    }

    /**
     * Retrieve a client from the connection URL
     * @param string $customerCode
     * @return Model|false
     */
    public function getClientFromCustomerCode($customerCode) {
		$query = $this->getBasicClientQuery();
		$query->addWhere('CLIENT_WEB=?', $customerCode);
		return $this->singleExec($query);
    }

	/**
	 * Retrieve a client from the connection URL
	 * @param int $id
	 * @return Model|false
	 */
	public function getClient($id) {
		$query = $this->getBasicClientQuery();
		$query->addWhere('ID_CLIENT=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * build a basic mysql query for client
	 * @return Query
	 */
	private function getBasicClientQuery() {
		$query = new Query();
		$query->setColumns(['ID_CLIENT', 'CLIENT_GROUPE', 'CLIENT_WEB', 'CLIENT_EXPIRATION', 'CLIENT_TOKEN', 'CLIENT_DATEINSCRIPTION'])
			->from('TAB_CLIENT');
		return $query;
	}

    /**
     * Create a client
     * @param  array  $data
     * @return integer Client ID
     */
    public function createClient($data) {
		if(!isset($data['CLIENT_DATEINSCRIPTION'])) $data['CLIENT_DATEINSCRIPTION'] = date('Y-m-d H:i:s', time());
		if(!isset($data['CLIENT_EXPIRATION'])) $data['CLIENT_EXPIRATION'] = 0;
		return $this->insert('TAB_CLIENT', $data);
    }

	/**
	 * Update a client
	 * @param  array  $data
	 * @param  integer  $id
	 * @return bool
	 */
	public function updateClient($data, $id) {
		$this->update('TAB_CLIENT', $data, 'ID_CLIENT=:id', array(':id' => $id));
		return true;
	}

    /**
     * Delete a client
     * @param  integer  $id
     * @return boolean
     */
    public function deleteClient($id) {
        $this->delete('TAB_CLIENT', 'ID_CLIENT=?', $id);
        return true;
    }
}
