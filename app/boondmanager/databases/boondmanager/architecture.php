<?php
/**
 * architecture.php
 * @author Tanguy Lambert <tanguy.lambert@gmail.com>
 */
namespace BoondManager\Databases\BoondManager;

use Wish\Models\Model;

/**
 * Class Architecture
 * @package BoondManager\Databases\BoondManager
 */
class Architecture extends AbstractObject{
	/**
	 * @return false|int|Model[]
	 */
	public function getAllDatabaseServers() {
		return $this->exec('SELECT ID_SERVEURBDD, BDD_URL, BDD_PORT, BDD_USER, BDD_PASS, BDD_DEFAULT FROM TAB_SERVEURBDD ORDER BY ID_SERVEURBDD ASC');
	}

	/**
	 * @return false|int|Model[]
	 */
	public function getAllNodeServers() {
		return $this->exec('SELECT ID_SERVEURNODE, NODE_URL, NODE_SERVER, NODE_DEFAULT FROM TAB_SERVEURNODE ORDER BY ID_SERVEURNODE ASC');
	}

	/**
	 * @return false|int|Model[]
	 */
	public function getAllGEDServers() {
		return $this->exec('SELECT ID_SERVEURGED, GED_URL, GED_PATH, GED_DEFAULT FROM TAB_SERVEURGED ORDER BY ID_SERVEURGED ASC');
	}
}
