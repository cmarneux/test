<?php
/**
 * client.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\BoondManager;

use BoondManager\APIs\Accounts\Filters\SearchAccounts;
use BoondManager\APIs\Customers\Filters\SearchCustomers;
use BoondManager\Lib\Cluster;
use BoondManager\Lib\GED;
use BoondManager\Lib\SolR;
use BoondManager\Services\Agencies;
use BoondManager\Services\BM;
use BoondManager\Services\Apps;
use BoondManager\Databases\Local;
use BoondManager\Services\Customers;
use BoondManager\Services\Dictionary;
use BoondManager\Services;
use BoondManager\APIs\Employees\Filters\SearchEmployees;
use BoondManager\Models;
use Wish\Models\Model;
use Wish\Models\SearchResult;
use Wish\MySQL\DbLink;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Tools;

/**
 * Class Customer
 * @package BoondManager\Databases\BoondManager
 */
class Customer extends  AbstractObject  {
	/**
	 * Search customers
	 * @param SearchCustomers $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchCustomers(SearchCustomers $filter) {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('ID_CLIENT, CLIENT_GROUPE, CLIENT_WEB, CLIENT_DATEINSCRIPTION, CLIENT_EMAIL, ID_ABONNEMENT, AB_DEBUT, AB_FIN, AB_NBMANAGERS, AB_TYPE, AB_MAXRESSOURCESPERMANAGER, AB_MAXRESSOURCESINTRANET, AB_MAXSTORAGE, ID_CRMSOCIETE, ID_USER, USER_PRENOM, USER_NOM');

		$where = new Where();
		$where->and_($this->getFilterSearch($filter->perimeterSupports->getValue(), 'ID_USER'));

		$query->from('TAB_CLIENT');
		$query->addJoin('INNER JOIN TAB_ABONNEMENT USING(ID_CLIENT) 
							LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=TAB_CLIENT.ID_RESPUSER)');
		$query->groupBy('ID_CLIENT');

		$keywordsMapping = ['CUST'=>'ID_CLIENT'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if(!$whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			switch($filter->keywordsType->getValue()) {
				case SearchCustomers::KEYWORD_TYPE_NAME:
					$where->and_('CLIENT_GROUPE LIKE ?', $likeSearch);
					break;
				case SearchCustomers::KEYWORD_TYPE_CODE:
					$where->and_('CLIENT_WEB LIKE ?', $keywords);
					break;
				default:
					$whereKeywords = new Where();
					$whereKeywords->or_('CLIENT_GROUPE LIKE ?', $likeSearch)
						->or_('CLIENT_WEB LIKE ?', $keywords);
					$where->and_($whereKeywords);
					break;
			}
		}

		$where->and_($this->getFilterSearch(Tools::reverseMapData($filter->subscriptionStates->getValue(), BM::USER_ACCESS_MAPPING), 'AB_TYPE'));

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

		$query->addWhere($where);
		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchCustomers::ORDERBY_SUBSCRIPTION_STARTDATE           => 'AB_DEBUT',
			SearchCustomers::ORDERBY_SUBSCRIPTION_ENDDATE              => 'AB_FIN',
			SearchCustomers::ORDERBY_SUBSCRIPTION_RESOURCES_PER_MANAGER            => 'AB_MAXRESSOURCESPERMANAGER',
			SearchCustomers::ORDERBY_SUBSCRIPTION_MAX_RESOURCES_INTRANET         => 'AB_MAXRESSOURCESINTRANET',
			SearchCustomers::ORDERBY_MAINSUPPORT_LASTNAME => 'USER_NOM',
			SearchCustomers::ORDERBY_NAME		 => 'CLIENT_GROUPE',
			SearchCustomers::ORDERBY_COMPANY		 => 'ID_CRMSOCIETE',
			SearchCustomers::ORDERBY_SUBSCRIPTION_STATE		 => 'AB_TYPE',
			SearchCustomers::ORDERBY_CREATIONDATE		 => 'CLIENT_DATEINSCRIPTION'
		];

		if(!$column) $query->addOrderBy('CLIENT_DATEINSCRIPTION DESC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping)) {
				$query->addOrderBy($mapping[$c] . ' ' . $order);
				if($c == SearchCustomers::ORDERBY_MAINSUPPORT_LASTNAME)
					$query->addOrderBy('USER_PRENOM '.$order);
			}
	}

	/**
	 * Retrieve all client's data from its connection url
     * @param  string  $customerCode
	 * @return Model|false
     */
	public function getCustomerFromCode($customerCode) {
		$query = $this->getBasicCustomerQuery();
		$query->addWhere('CLIENT_WEB=?', $customerCode);
		$result = $this->singleExec($query);
		if($result)
			$result = array_merge($result->toArray(), $this->getCustomerConfigFromCode($result['CLIENT_WEB']));
		return $result;
	}

	/**
	 * Retrieve all client's data from its ID
     * @param  integer  $id Client ID
	 * @return Model|false
     */
    public function getCustomer($id) {
    	$query = $this->getBasicCustomerQuery();
    	$query->addWhere('ID_CLIENT=?', $id);
    	$result = $this->singleExec($query);
		if($result)
			$result = array_merge($result->toArray(), $this->getCustomerConfigFromCode($result['CLIENT_WEB']));
		return $result;
    }

    /**
	 * build a mysql query for a customer
	 * @return Query
	 */
    private function getBasicCustomerQuery() {
    	$query =new Query();
    	$query->setColumns(['TAB_CLIENT.ID_CLIENT', 'ID_ABONNEMENT', 'ID_RESPUSER', 'ID_CRMSOCIETE', 'CLIENT_GROUPE', 'CLIENT_WEB', 'CLIENT_VERSION', 'CLIENT_DATEINSCRIPTION', 'CLIENT_EMAIL',
							'AB_DEBUT', 'AB_FIN', 'AB_NBMANAGERS', 'AB_TYPE', 'AB_MAXSTORAGE', 'AB_MAXRESSOURCESPERMANAGER', 'AB_MAXRESSOURCESINTRANET'])
			->from('TAB_CLIENT')
			->addJoin('INNER JOIN TAB_ABONNEMENT USING(ID_CLIENT)');
		return $query;
    }

	/**
	 * Retrieve all client's data from its ID
	 * @param  string  $customerCode
	 * @return array|false
	 */
	public function getCustomerConfigFromCode($customerCode) {
		$result = [];

		BM::loadCustomerInterface($customerCode);

		$result['MYSQLSERVER']['BDD_URL'] = BM::getCustomerInterfaceDbServer();
		$result['NODESERVER']['NODE_SERVER'] = BM::getCustomerInterfaceNodeServer();
		$result['GEDSERVER']['GED_PATH'] = BM::getCustomerInterfaceGEDDirectory();

		$dbAgency = new Local\Agency();
		$tabConfig = $dbAgency->getLightAgencyFromCustomerCode($customerCode);

		$dbUser = new Local\Account();
		$result['ROOT'] = $dbUser->getAdministrator();
		if($tabConfig && $result['ROOT']) {
			$result['GRPCONF_INTRANET'] = $tabConfig['GRPCONF_INTRANET'];
			$result['GRPCONF_DOWNLOADCENTER'] = $tabConfig['GRPCONF_DOWNLOADCENTER'];
			$result['GRPCONF_HASH'] = $tabConfig['GRPCONF_HASH'];

			//exclud archived employees
			$tabState = Dictionary::getMapping('specific.setting.state.resource');
			unset($tabState[Models\Employee::STATE_ARCHIVED]);

			$filter = new SearchEmployees();
			$filter->disableMaxResultLimit();
			$filter->setIndifferentPerimeter();
			$filter->setData([
				'state'=> array_keys($tabState),
				'excludeManager' => true,
				'page' => 1
			]);
			$dbEmployee = new Local\Employee();
			$search = $dbEmployee->searchEmployees($filter);
			$result['NBRESSOURCES'] = $search->total;

			$filter = new SearchAccounts();
			$filter->disableMaxResultLimit();
			$filter->setIndifferentPerimeter();
			$filter->setData([
				'userTypes'=> [BM::USER_TYPE_RESOURCE],
				'userSubscriptions'=> [BM::USER_ACCESS_ACTIVE],
				'page' => 1
			]);
			$dbUser = new Local\Account();
			$search = $dbUser->searchAccounts($filter);
			$result['NBINTRANET'] = $search->total;
		}
		BM::clearCustomerInterface();
		return $result ? $result : false;
	}

	/**
	 * Delee all client's data from its ID
	 * @param  integer  $id Client ID
	 * @param string $customerCode
	 * @return bool
	 */
	public function deleteCustomerWithCustomerCode($id, $customerCode) {
		if(!preg_match(BM::CUSTOMER_INTERFACE_REGEX, $customerCode)) \Base::instance()->error(500, 'Wrong customer code');

		BM::loadCustomerInterface($customerCode);

		$dbAgency = new Local\Agency();
		$tabAgencies = $dbAgency->getAllAgencies(true);

		//Uninstall all customer's apps
		$apps = Apps::getAllApps(false);
		foreach($apps as $app) Apps::uninstall($app);

		//Delete customer's database mysql
		$sqlFile = BM::getRootPath() . '/tmp/dropBDD_' . $customerCode . '.sql';
		if(file_put_contents($sqlFile, 'DROP DATABASE `'.$customerCode.'`;')) {
			$cmd = \Base::instance()->get('BMMYSSQL.EXECUTABLE_PATH').'mysql'.
					' -h '.BM::getCustomerInterfaceDbServer().
					' -P '.BM::getCustomerInterfaceDbPort().
					' -u '.\Base::instance()->get('BMMYSQL.ROOTUSER').
					' -p'.\Base::instance()->get('BMMYSQL.ROOTPWD');
			exec($cmd.' < ' .$sqlFile);
			unlink($sqlFile);

			//On vérifie que la BDD a bien été supprimée
			try{
				$dsn = 'mysql:host='.BM::getCustomerInterfaceDbServer().';port='.BM::getCustomerInterfaceDbPort().';dbname='.$customerCode;
				$db = new DbLink($dsn, \Base::instance()->get('BMMYSQL.ROOTUSER'), \Base::instance()->get('BMMYSQL.ROOTPWD'));
				$db->closeConnection();
				return false;
			} catch(\Exception $e) {}
		}

		//Delete customer's database solr
		SolR::instance()->deleteClient(BM::getCustomerInterfaceDbServer(), BM::SOLR_MODULE_CANDIDATES, $customerCode);
		SolR::instance()->deleteClient(BM::getCustomerInterfaceDbServer(), BM::SOLR_MODULE_RESOURCES, $customerCode);
		SolR::instance()->deleteClient(BM::getCustomerInterfaceDbServer(), BM::SOLR_MODULE_CRM_CONTACTS, $customerCode);
		SolR::instance()->deleteClient(BM::getCustomerInterfaceDbServer(), BM::SOLR_MODULE_CRM_COMPANIES, $customerCode);

		//Delete customer's vendor entity and all his apps
		$dbVendor = new Vendor();
		$vendor = $dbVendor->getVendorFromClient($id);
		if($vendor) $dbVendor->deleteVendor($vendor['ID_VENDEUR'], $customerCode);

		//Delete all customer's login on boondmanager's database
		$dbLogin = new Login();
		$dbLogin->deleteAllLogin($customerCode);

		//Delete customer's subscription
		$dbSubscription = new Subscription();
		$dbSubscription->deleteSubscriptionFromClient($id);

		//Delete customer's record on boondmanager's database
		$this->delete('TAB_CLIENT', 'ID_CLIENT=?', $id);

		//Delete customer's GED
		$ged = new GED();
		$gedDirectory = $ged->getGEDDirectory();
		if(file_exists($gedDirectory)) exec('rm -R '.$gedDirectory);

		$cluster = new Cluster();
		//Delete customer's interface
		$cluster->deleteFile(BM::buildCustomerInterfaceFilePath($customerCode));
		//FIXME (Tanguy) : Patch to maintain V6 in use
		$cluster->deleteFile('../application.boondmanager/application/configs/'.$customerCode.'.php');
		$cluster->deleteFile('../services.boondmanager/application/configs/'.$customerCode.'.php');

		//Delete customer's specific dictionary
		$cluster->deleteDirectory(Dictionary::getSpecificDictPath());
		//FIXME (Tanguy) : Patch to maintain V6 in use
		$cluster->deleteDirectory('../application.boondmanager/application/languages/fr/PAS/'.$customerCode);
		$cluster->deleteDirectory('../services.boondmanager/application/languages/fr/PAS/'.$customerCode);
		$cluster->deleteDirectory('../application.boondmanager/application/languages/en/PAS/'.$customerCode);
		$cluster->deleteDirectory('../services.boondmanager/application/languages/en/PAS/'.$customerCode);

		//Delete customer's logos
		$imgLogo = Customers::getLogoPath();
		if($imgLogo) $cluster->deleteFile($imgLogo);

		foreach($tabAgencies as $agency) {
			$imgLogo = Agencies::getLogoOnInvoicesPath($agency['ID_SOCIETE']);
			if($imgLogo) $cluster->deleteFile($imgLogo);

			$imgLogo = Agencies::getLogoOnActivityExpensesPath($agency['ID_SOCIETE']);
			if($imgLogo) $cluster->deleteFile($imgLogo);
		}
		//FIXME (Tanguy) : Patch to maintain V6 in use
		foreach(['/../BoondManager_Data/www/images/clients/factures/', '/../BoondManager_Data/www/images/clients/tpsfrsabs/'] as $directory) {
			$pointeur = opendir(BM::getRootPath() . $directory);
			while ($entree = readdir($pointeur)) {
				if (preg_match("#^" . $customerCode . ".*$#", $entree, $match))
					$cluster->DeleteFile($directory . $entree);
			}
			closedir($pointeur);
		}

		BM::clearCustomerInterface();
		return true;
	}

	/**
	 * Create a customer
	 * @param  array  $data
	 * The array can contains the following keys:
	 * - `CLIENT` : an array with the Client data, cf. [TAB_USER](../../bddclient/classes/TAB_CLIENT.html)
	 * - `ABONNEMENT` : an array with the Subscription data, cf. [TAB_USER](../../bddclient/classes/TAB_ABONNEMENT.html)
	 * - `ROOT` : an array with the Administrator data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * - `CONFIGROOT` : an array with the Administrator's config data, cf. [TAB_USER](../../bddclient/classes/TAB_USERCONFIG.html)
	 * - `CONFIGGROUPE` : an array with the Agency data, cf. [TAB_USER](../../bddclient/classes/TAB_GROUPECONFIG.html)
	 * @return integer the user ID
	 */
	public function createCustomer($data) {
		if(isset($data['CLIENT'])) {
			if(!preg_match(BM::CUSTOMER_INTERFACE_REGEX, $data['CLIENT']['CLIENT_WEB'])) \Base::instance()->error(500, 'Wrong database name');

			//Create customer's database mysql
			$sqlFile = BM::getRootPath() . '/tmp/createBDD_' . $data['CLIENT']['CLIENT_WEB'] . '.sql';
			if(file_put_contents($sqlFile, 'CREATE DATABASE `'.$data['CLIENT']['CLIENT_WEB'].'`;')) {
				$cmd = \Base::instance()->get('BMMYSSQL.EXECUTABLE_PATH').'mysql'.
					' -h '.$data['MYSQLSERVER']['BDD_URL'].
					' -P '.$data['MYSQLSERVER']['BDD_PORT'].
					' -u '.\Base::instance()->get('BMMYSQL.ROOTUSER').
					' -p'.\Base::instance()->get('BMMYSQL.ROOTPWD');
				exec($cmd.' < ' .$sqlFile);
				exec($cmd.' '.$data['CLIENT']['CLIENT_WEB'].' < ' .BM::getRootPath() . '/cgi-bin/SQL/BoondManager/database.sql');
				unlink($sqlFile);

				try{
					$dsn = 'mysql:host='.$data['MYSQLSERVER']['BDD_URL'].';port='.$data['MYSQLSERVER']['BDD_PORT'].';dbname='.$data['CLIENT']['CLIENT_WEB'];
					$db = new DbLink($dsn, \Base::instance()->get('BMMYSQL.ROOTUSER'), \Base::instance()->get('BMMYSQL.ROOTPWD'));
					$db->closeConnection();
				} catch(\Exception $e) {
					echo 'error';
					return false;
				}

				$data['CLIENT']['CLIENT_VERSION'] = \Base::instance()->get('BOONDMANAGER.VERSION');
				if(!isset($data['CLIENT']['CLIENT_DATEINSCRIPTION'])) $data['CLIENT']['CLIENT_DATEINSCRIPTION'] = date('Y-m-d H:i:s');
				$id = $this->insert('TAB_CLIENT', $data['CLIENT']);

				if(!isset($data['ABONNEMENT'])) $data['ABONNEMENT'] = [];
				if(!isset($data['ABONNEMENT']['AB_TYPE'])) $data['ABONNEMENT']['AB_TYPE'] = BM::DB_ACCESS_ACTIVE;
				$data['ABONNEMENT']['ID_CLIENT'] = $id;
				$id = $this->insert('TAB_ABONNEMENT', $data['ABONNEMENT']);

				if($id) {
					$iniFile = '[SESSION]'.PHP_EOL.
								'CLIENT.BMMYSQL.SERVER="'.$data['MYSQLSERVER']['BDD_URL'].'"'.PHP_EOL.
								'CLIENT.BMMYSQL.PORT="'.$data['MYSQLSERVER']['BDD_PORT'].'"'.PHP_EOL.
								'CLIENT.BMMYSQL.USER="'.$data['MYSQLSERVER']['BDD_USER'].'"'.PHP_EOL.
								'CLIENT.BMMYSQL.PWD="'.$data['MYSQLSERVER']['BDD_PASS'].'"'.PHP_EOL.
								'CLIENT.BMMYSQL.DATABASE="'.$data['CLIENT']['CLIENT_WEB'].'"'.PHP_EOL.
								'CLIENT.BMGED.DIRECTORY="'.$data['GEDSERVER']['GED_PATH'].'"'.PHP_EOL.
								'CLIENT.BMNODE.URL="'.$data['NODESERVER']['NODE_URL'].'"'.PHP_EOL.
								'CLIENT.BMNODE.SERVER="'.$data['NODESERVER']['NODE_SERVER'].'"'.PHP_EOL;
					$initFilePath = BM::getRootPath().'/'.BM::buildCustomerInterfaceFilePath($data['CLIENT']['CLIENT_WEB']);

					if(file_put_contents($initFilePath, $iniFile)) {
						exec('chmod -R 777 ' . $initFilePath);

						BM::loadCustomerInterface($data['CLIENT']['CLIENT_WEB']);

						//Add the administrator account
						$dbUser = new Local\Account();
						$data['ROOT']['USER_TYPE'] = BM::DB_TYPE_ADMINISTRATOR;
						$data['ROOT']['USER_ABONNEMENT'] = $data['ABONNEMENT']['AB_TYPE'] == BM::DB_ACCESS_INACTIVE ? BM::DB_ACCESS_INACTIVE : BM::DB_ACCESS_ACTIVE;
						$data['CONFIGROOT']['ID_USER'] = $dbUser->createAdministrator($data['ROOT']);
						$data['CONFIGROOT']['CONFIG_SHOWFACTURES'] = 1;
						$dbUser->createAccountSetting($data['CONFIGROOT']);

						//Save the login
						$dbLogin = new Login();
						$dbLogin->insert('TAB_LOGIN', ['ID_USER' => $data['ROOT']['ID_USER'], 'USER_LOGIN' => $data['ROOT']['USER_LOGIN'], 'CLIENT_WEB' => $data['CLIENT']['CLIENT_WEB']]);

						//Build the agency
						$dbAgency = new Local\Agency();
						$tabSociete = [];
						$tabSociete['SOCIETE_GROUPE'] = $data['CLIENT']['CLIENT_GROUPE'];
						$tabSociete['SOCIETE_RAISON'] = $data['CLIENT']['CLIENT_GROUPE'];
						$data['CONFIGGROUPE']['ID_SOCIETE'] = $dbAgency->insert('TAB_SOCIETE', $tabSociete);

						//On configure les types de temps
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 1,'TYPEH_NAME' => 'Normale','TYPEH_TYPEACTIVITE' => 0,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 2,'TYPEH_NAME' => 'CP','TYPEH_TYPEACTIVITE' => 1,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 3,'TYPEH_NAME' => 'RTT','TYPEH_TYPEACTIVITE' => 1,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 4,'TYPEH_NAME' => 'Congés Sans Solde','TYPEH_TYPEACTIVITE' => 1,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 5,'TYPEH_NAME' => 'Maladie','TYPEH_TYPEACTIVITE' => 1,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 6,'TYPEH_NAME' => 'Exceptionnelle','TYPEH_TYPEACTIVITE' => 1,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 7,'TYPEH_NAME' => 'Structure','TYPEH_TYPEACTIVITE' => 2,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 8,'TYPEH_NAME' => 'Intercontrat','TYPEH_TYPEACTIVITE' => 2,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEHEURE', array('TYPEH_REF' => 9,'TYPEH_NAME' => 'Formation','TYPEH_TYPEACTIVITE' => 2,'TYPEH_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));

						//On configure les barêmes kilométriques
						$dbAgency->insert('TAB_BAREMEKM', array('BKM_REF' => 1,'BKM_NAME' => '5CV < 5000km','BKM_VALUE' => 0.540,'BKM_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));

						//On configure les types de frais
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 1,'TYPEFRS_NAME' => 'Restaurant','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 2,'TYPEFRS_NAME' => 'Hôtel','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 3,'TYPEFRS_NAME' => 'Train','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 4,'TYPEFRS_NAME' => 'Avion','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 5,'TYPEFRS_NAME' => 'Parking','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 6,'TYPEFRS_NAME' => 'Péages','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFRAIS', array('TYPEFRS_REF' => 7,'TYPEFRS_NAME' => 'Divers','TYPEFRS_TVA' => 0,'TYPEFRS_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));

						//On configure les types d'items de factures
						$dbAgency->insert('TAB_TYPEFACTURATION', array('TYPEF_REF' => 1,'TYPEF_NAME' => 'Normale','TYPEF_CATEGORIES' => '0_1_1|0_2_1|0_13_1|0_14_1','ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFACTURATION', array('TYPEF_REF' => 2,'TYPEF_NAME' => 'Echéance','TYPEF_CATEGORIES' => '2','ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFACTURATION', array('TYPEF_REF' => 3,'TYPEF_NAME' => 'Frais refacturés','TYPEF_CATEGORIES' => '1_0_1|1_1_1|1_2_1|1_3_1|1_4_1|1_5_1','ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$dbAgency->insert('TAB_TYPEFACTURATION', array('TYPEF_REF' => 4,'TYPEF_NAME' => 'Achats refacturés','TYPEF_CATEGORIES' =>'3','ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));

						//On configure un RIB par défaut
						$dbAgency->insert('TAB_RIB', array('RIB_DESCRIPTION' => 'A renseigner !','RIB_IBAN' => '','RIB_BIC' => '','RIB_TYPE' => 1,'ID_PARENT' => $data['CONFIGGROUPE']['ID_SOCIETE']));

						//On configure les types d'avantages
						$dbAgency->insert('TAB_TYPEAVANTAGE', array('TYPEA_REF' => 1,'TYPEA_NAME' => 'Prime','TYPEA_QUOTAPARTICIPATION' => 0,'TYPEA_QUOTARESSOURCE' => 0,'TYPEA_QUOTASOCIETE' => 1.48,'TYPEA_TYPE' => 0,'TYPEA_CATEGORIE' => 0,'TYPEA_ETAT' => 1,'ID_SOCIETE' => $data['CONFIGGROUPE']['ID_SOCIETE']));

						$data['CONFIGGROUPE']['GRPCONF_WEB'] = $data['CLIENT']['CLIENT_WEB'];
						$data['CONFIGGROUPE']['GRPCONF_VALIDATIONTEMPS'] = '0';
						$data['CONFIGGROUPE']['GRPCONF_VALIDATIONFRAIS'] = '0';
						$data['CONFIGGROUPE']['GRPCONF_VALIDATIONABSENCES'] = '0';
						$data['CONFIGGROUPE']['GRPCONF_BDCCONDREGLEMENT'] = 1;
						$data['CONFIGGROUPE']['GRPCONF_MAILFACTURATION'] = '';
						$data['CONFIGGROUPE']['GRPCONF_BDCTAUXTVA'] = 20;
						$data['CONFIGGROUPE']['GRPCONF_BDCSHOWRIB'] = 1;
						$data['CONFIGGROUPE']['GRPCONF_BDCSHOWPRJREFERENCE'] = 1;
						$data['CONFIGGROUPE']['GRPCONF_BDCSHOWFOOTER'] = 1;
						$data['CONFIGGROUPE']['GRPCONF_BDCMENTIONS'] = 'Escompte pour règlement anticipé : 0%'.chr(10).'En cas de retard de paiement, seront exigibles, conformément à l\'article L441-6 du code du commerce, une indemnité calculée sur la base de 3 fois le taux d\'intérêt légal en vigueur ainsi qu\'une indemnité forfaitaire pour frais de recouvrement de 40 €.';
						$data['CONFIGGROUPE']['GRPCONF_MASKREFFACTURATION'] = 'F[AAAA] 00001';
						$data['CONFIGGROUPE']['GRPCONF_ETATFIXEFACTURATION'] = '1|4|5|9|2|3';
						$data['CONFIGGROUPE']['GRPCONF_BDCIDRIB'] = $dbAgency->insert('TAB_RIB', array('RIB_TYPE' => 0,'ID_PARENT' => $data['CONFIGGROUPE']['ID_SOCIETE']));
						$data['CONFIGGROUPE']['GRPCONF_ACHATTAUXTVA'] = 20;
						$dbAgency->insert('TAB_GROUPECONFIG', $data['CONFIGGROUPE']);

						//On créé l'abonnement du client
						$tabAbonnement = array();
						$tabAbonnement['AB_DEBUT'] = $data['ABONNEMENT']['AB_DEBUT'];
						$tabAbonnement['AB_FIN'] = $data['ABONNEMENT']['AB_FIN'];
						$tabAbonnement['AB_NBMANAGERS'] = $data['ABONNEMENT']['AB_NBMANAGERS'];
						$tabAbonnement['AB_MAXRESSOURCESPERMANAGER'] = $data['ABONNEMENT']['AB_MAXRESSOURCESPERMANAGER'];
						$tabAbonnement['AB_MAXSTORAGE'] = $data['ABONNEMENT']['AB_MAXSTORAGE'];
						$tabAbonnement['AB_MAXRESSOURCESINTRANET'] = $data['ABONNEMENT']['AB_MAXRESSOURCESINTRANET'];
						$dbAgency->insert('TAB_ABONNEMENT', $data['ABONNEMENT']);

						//On indique la version actuelle de l'interface du client
						$dbAgency->insert('TAB_BOONDMANAGER', array('BD_VERSION' => \Base::instance()->get('BOONDMANAGER.VERSION'), 'ID_BOONDMANAGER' => 1));

						//Add customer's GED
						$ged = new GED();
						$gedDirectory = $ged->getGEDDirectory();
						exec('mkdir -p '.$gedDirectory.'/'.$ged::GED_DT);
						exec('mkdir -p '.$gedDirectory.'/'.$ged::GED_DOCUMENTS);
						exec('mkdir -p '.$gedDirectory.'/'.$ged::GED_FILES);
						exec('mkdir -p '.$gedDirectory.'/'.$ged::GED_JUSTIFICATIVES);
						exec('mkdir -p '.$gedDirectory.'/'.$ged::GED_CANDIDATES);
						exec('chmod -R 775 -p '.$gedDirectory);

						$cluster = new Cluster();
						//Add customer's specific dictionary
						//FIXME (Tanguy) : Patch to maintain V6 in use
						$cluster->createDirectory('../application.boondmanager/application/languages/fr/PAS/'.$data['CLIENT']['CLIENT_WEB']);
						$cluster->createDirectory('../application.boondmanager/application/languages/fr/PAS/'.$data['CLIENT']['CLIENT_WEB'].'/common');
						$cluster->createDirectory('../services.boondmanager/application/languages/fr/PAS/'.$data['CLIENT']['CLIENT_WEB'].'/common/configuration');
						$cluster->createDirectory('../application.boondmanager/application/languages/en/PAS/'.$data['CLIENT']['CLIENT_WEB']);
						$cluster->createDirectory('../application.boondmanager/application/languages/en/PAS/'.$data['CLIENT']['CLIENT_WEB'].'/common');
						$cluster->createDirectory('../services.boondmanager/application/languages/en/PAS/'.$data['CLIENT']['CLIENT_WEB'].'/common/configuration');

						//Save default Apps
						//TODO : Save default Apps (cf. V6)

						//Save default Role
						//TODO : Save default Role (cf. V6)

						//Add customer's interface
						if($cluster->uploadFile(BM::buildCustomerInterfaceFilePath($data['CLIENT']['CLIENT_WEB']), BM::buildCustomerInterfaceFilePath($data['CLIENT']['CLIENT_WEB']))) {
							//FIXME (Tanguy) : Patch to maintain V6 in use
							$phpFile = "<?php Wish_Session::getInstance()->set('login_pasbdd', array('SERVER' => '" . $data['MYSQLSERVER']['BDD_URL'] . ":".$data['MYSQLSERVER']['BDD_PORT']."', 'USER' => '" . $data['MYSQLSERVER']['BDD_USER'] . "', 'PASS' => '" . $data['MYSQLSERVER']['BDD_PASS'] . "', 'DATABASE' => '" . $data['CLIENT']['CLIENT_WEB'] . "', 'GED' => '" . $data['GEDSERVER']['GED_PATH'] . "', 'NODE_URL' => '" . $data['NODESERVER']['NODE_URL'] . "', 'NODE_SERVER' => '" . $data['NODESERVER']['NODE_SERVER'] . "'));";
							$phpFilePath = BM::getRootPath().'/tmp/' . $data['CLIENT']['CLIENT_WEB'] . '.php';
							if (file_put_contents($phpFilePath, $phpFile)) {
								$cluster->uploadFile('tmp/' . $data['CLIENT']['CLIENT_WEB'] . '.php', '../application.boondmanager/application/configs/' . $data['CLIENT']['CLIENT_WEB'] . '.php');
								$cluster->uploadFile('tmp/' . $data['CLIENT']['CLIENT_WEB'] . '.php', '../services.boondmanager/application/configs/' . $data['CLIENT']['CLIENT_WEB'] . '.php');
								unlink($phpFilePath);
							}
						} else {
							echo 'erreur';
						}

						BM::clearCustomerInterface();
						return $id;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Install tha test database
	 * @param  array  $data
	 * The array can contains the following keys:
	 * - `CLIENT` : an array with the Client data, cf. [TAB_USER](../../bddclient/classes/TAB_CLIENT.html)
	 * - `ABONNEMENT` : an array with the Subscription data, cf. [TAB_USER](../../bddclient/classes/TAB_ABONNEMENT.html)
	 * - `ROOT` : an array with the Administrator data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * - `CONFIGROOT` : an array with the Administrator's config data, cf. [TAB_USER](../../bddclient/classes/TAB_USERCONFIG.html)
	 * - `CONFIGGROUPE` : an array with the Agency data, cf. [TAB_USER](../../bddclient/classes/TAB_GROUPECONFIG.html)
	 * @param string $managerLoginToIgnore
	 * @return integer the user ID
	 */
	public function installTestDatabase($data, $managerLoginToIgnore = '') {
		if(isset($data['CLIENT']) && isset($data['ROOT'])) {
			//Install customer's test database mysql
			$cmd = \Base::instance()->get('BMMYSSQL.EXECUTABLE_PATH') . 'mysql' .
				' -h ' . $data['MYSQLSERVER']['BDD_URL'] .
				' -P ' . $data['MYSQLSERVER']['BDD_PORT'] .
				' -u ' . \Base::instance()->get('BMMYSQL.ROOTUSER') .
				' -p' . \Base::instance()->get('BMMYSQL.ROOTPWD');
			exec($cmd . ' ' . $data['CLIENT']['CLIENT_WEB'] . ' < ' . BM::getRootPath() . '/tests/postman/database.sql');

			BM::loadCustomerInterface($data['CLIENT']['CLIENT_WEB']);
			$dbUser = new Local\Account();
			$dbUser->updateAdministrator($data['ROOT']);

			$dbAgency = new Local\Agency();
			$data['GROUPECONFIG']['GRPCONF_WEB'] = $data['CLIENT']['CLIENT_WEB'];
			$dbAgency->updateAllAgencies($data['GROUPECONFIG']);

			//Uninstall all customer's apps
			$apps = Apps::getAllApps(false);
			foreach($apps as $app) Apps::uninstall($app);

			$tabUserConfig = [
				'CONFIG_APIS' => '',
				'CONFIG_APICALENDAR' => 0,
				'CONFIG_APIMAIL' => 0,
				'CONFIG_APIVIEWER' => 0,
				'CONFIG_APIEMAILING' => 0,
				'CONFIG_MENUBAR' => ''
			];

			//Check each login and update TAB_USERCONFIG
			$nbManagers = 0;
			$filter = new SearchAccounts();
			$filter->disableMaxResultLimit();
			$filter->setIndifferentPerimeter();
			$filter->setData([
				'userTypes'=> [BM::USER_TYPE_MANAGER, BM::USER_TYPE_RESOURCE],
				'userSubscriptions'=> [BM::USER_ACCESS_ACTIVE],
				'page' => 1
			]);
			$search = $dbUser->searchAccounts($filter);
			foreach($search->rows as $user) {
				$tabUser = [
					'USER_LOGIN' => $user['USER_LOGIN'],
					'USER_SECURITYALERT' => 0,
					'USER_SECURITYCOOKIE' => 0,
					'ID_PROFIL' => $user['ID_PROFIL']
				];

				if($user['USER_LOGIN'] != $managerLoginToIgnore && Services\Login::isLoginExists($user['USER_LOGIN'])) {
					$tabUser['USER_LOGIN'] = '';
					$tabUser['USER_PWD'] = '';
					$tabUser['USER_ABONNEMENT'] = BM::DB_ACCESS_INACTIVE;
					$tabUser['USER_TYPE'] = BM::DB_TYPE_RESOURCE;
				} else if($user['USER_TYPE'] == BM::DB_TYPE_MANAGER)
					$nbManagers++;

				$dbUser->updateAccount([
					'USER' => $tabUser,
					'CONFIGUSER' => $tabUserConfig
				], $user['ID_USER']);
			}

			$dbSubscription = new Local\Subscription();
			$tabSubscription = $dbSubscription->getGroupSubscription()->toArray();

			$newSubscription = [];
			$newSubscription['AB_DEBUT'] = isset($tabSubscription['AB_DEBUT']) ? $tabSubscription['AB_DEBUT'] : null;
			$newSubscription['AB_FIN'] = isset($tabSubscription['AB_FIN']) ? $tabSubscription['AB_FIN'] : null;
			$newSubscription['AB_NBMANAGERS'] = $nbManagers > 1 ? $nbManagers : 1;
			$newSubscription['AB_MAXSTORAGE'] = Services\Customers::calculateMaxStorage($nbManagers);
			$newSubscription['AB_MAXRESSOURCESPERMANAGER'] = Models\Subscription::DEFAULT_RESOURCES_PER_MANAGER;
			$newSubscription['AB_MAXRESSOURCESINTRANET'] = Models\Subscription::DEFAULT_MAX_RESOURCES_INTRANET;

			$dbSubscription->updateGroupSubscription($newSubscription);

			$dbBMSubscription = new Subscription();
			$tabBMSubscription = $dbBMSubscription->getSubscriptionFromCustomerCode($data['CLIENT']['CLIENT_WEB']);
			if($tabBMSubscription)
				$dbBMSubscription->updateSubscriptionFromClient($newSubscription, $tabBMSubscription['ID_CLIENT']);

			$dbSubscription->delete('TAB_DEVICEALLOWED');
			$dbSubscription->delete('TAB_SOLR');
			$dbSubscription->delete('TAB_DELETED');

			SolR::instance()->Modification(BM::SOLR_MODULE_CANDIDATES);
			SolR::instance()->Modification(BM::SOLR_MODULE_RESOURCES);
			SolR::instance()->Modification(BM::SOLR_MODULE_CRM_COMPANIES);
			SolR::instance()->Modification(BM::SOLR_MODULE_CRM_CONTACTS);

			$ged = new GED();
			$gedDirectory = $ged->getGEDDirectory();
			if(file_exists(BM::getRootPath().'/tests/postman/GED/'.$ged::GED_DT))  exec('cp '.BM::getRootPath().'/tests/postman/GED/DT/* '.$gedDirectory.'/'.$ged::GED_DT);
			if(file_exists(BM::getRootPath().'/tests/postman/GED/'.$ged::GED_DOCUMENTS))  exec('cp '.BM::getRootPath().'/tests/postman/GED/DT/* '.$gedDirectory.'/'.$ged::GED_DOCUMENTS);
			if(file_exists(BM::getRootPath().'/tests/postman/GED/'.$ged::GED_FILES))  exec('cp '.BM::getRootPath().'/tests/postman/GED/DT/* '.$gedDirectory.'/'.$ged::GED_FILES);
			if(file_exists(BM::getRootPath().'/tests/postman/GED/'.$ged::GED_JUSTIFICATIVES))  exec('cp '.BM::getRootPath().'/tests/postman/GED/DT/* '.$gedDirectory.'/'.$ged::GED_JUSTIFICATIVES);
			if(file_exists(BM::getRootPath().'/tests/postman/GED/'.$ged::GED_CANDIDATES))  exec('cp '.BM::getRootPath().'/tests/postman/GED/DT/* '.$gedDirectory.'/'.$ged::GED_CANDIDATES);
			exec('chmod -R 775 -p '.$gedDirectory);

			BM::execScript( 'php '.BM::getRootPath() .'/cgi-bin/Subscription/Subscription.php '.BM::getCustomerCode().' '.BM::getEnvironnement(), true);

			BM::clearCustomerInterface();
			return true;
		}
		return false;
	}
}
