<?php
/**
 * login.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\BoondManager;

use Wish\Models\Model;
use Wish\MySQL\Query;

/**
 * manage the database for the module "Login"
 * @@package BoondManager\Databases\BoondManager;
 */
class Login extends AbstractObject {
    /**
     * Retrieve a user connection URL from the login
     * @param  string  $login
     * @return Model|false
     */
    public function getLogin($login) {
		$query = $this->getBasicLoginQuery();
		$query->addWhere('USER_LOGIN=?', $login);
		return $this->singleExec($query);
    }

	/**
	 * build a basic mysql query for login
	 * @return Query
	 */
	private function getBasicLoginQuery() {
		$query = new Query();
		$query->setColumns(['ID_USER', 'CLIENT_WEB', 'USER_LOGIN'])
			->from('TAB_LOGIN');
		return $query;
	}

	/**
	 * Create a login
	 * @param  array  $data
	 * @return bool
	 */
	public function createLogin($data) {
		return $this->insert('TAB_LOGIN', $data);
	}

	/**
	 * Update a login
	 * @param  array  $data
	 * @param  integer  $iduser
	 * @param  string  $customerCode
	 * @return bool
	 */
	public function updateLoginFromAccount($data, $iduser, $customerCode = '') {
		$this->update('TAB_LOGIN', $data, 'ID_USER=:id AND CLIENT_WEB=:web', array(':id' => $iduser, ':web' => $customerCode));
		return true;
	}

	/**
	 * Delete a login
	 * @param  integer  $iduser
	 * @param  string  $customerCode
	 * @return boolean
	 */
	public function deleteLoginFromAccount($iduser, $customerCode = '') {
		$this->delete('TAB_LOGIN', 'ID_USER=:id AND CLIENT_WEB=:web', [
			':id' => $iduser,
			':web' => $customerCode
		]);
		return true;
	}

	/**
	 * Delete all login
	 * @param  string  $customerCode
	 * @return boolean
	 */
	public function deleteAllLogin($customerCode = '') {
		$this->delete('TAB_LOGIN', 'CLIENT_WEB=?', $customerCode);
		return true;
	}
}
