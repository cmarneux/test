<?php
/**
 * manager.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\BoondManager;

use BoondManager\Services\BM;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Manager
 * @package BoondManager\Databases\BoondManager
 */
class Manager extends AbstractObject {
	/**
	 * Retrieve all Support Manager
	 * @return \Wish\Models\Model[]
	 */
	public function getAllSupportManagers() {
		return $this->exec('SELECT ID_USER, USER_NOM, USER_PRENOM FROM TAB_USER WHERE USER_TYPE=?', BM::DB_TYPE_SUPPORT);
	}

	/**
	 * @return \BoondManager\Models\Account[]
	 */
	public function getAllBasicSupportManagers(){
		$managers    = $this->getAllSupportManagers();
		$mappedArray = [];
		foreach($managers as $manager){
			$mappedArray[$manager['ID_USER']] = new Model([
				'ID_USER' => $manager['ID_USER'],
				'USER_NOM' => $manager['USER_NOM'],
				'USER_PRENOM' => $manager['USER_PRENOM']
			]);
		}
		return $mappedArray;
	}

	/**
	 * return a manager with minimum info (id & lastname & firstname)
	 * @param $id
	 * @return Model|null
	 */
	public function getBasicSupportManager($id){
		$managers = $this->getAllSupportManagers();
		return Tools::mapData($id, $managers);
	}
}
