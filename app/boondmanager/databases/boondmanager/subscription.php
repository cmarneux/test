<?php
/**
 * subscription.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\BoondManager;

use Wish\MySQL\Query;
use Wish\Models\Model;

/**
 * Class Subscription
 * @package BoondManager\Databases\BoondManager
 */
class Subscription extends AbstractObject{
    /**
     * get the configuration for a customer
     * @param string $customerCode url web code for the company
     * @return Model|false
     */
    public function getSubscriptionFromCustomerCode($customerCode) {
        $query = new Query();
        $query->select([
            'ID_CLIENT', 'ID_CRMSOCIETE', 'AB_DEBUT', 'AB_FIN', 'AB_NBMANAGERS', 'AB_MAXRESSOURCESPERMANAGER', 'AB_MAXSTORAGE',
            'AB_MAXRESSOURCESINTRANET', 'ID_ABONNEMENT'
        ])
            ->from('TAB_ABONNEMENT')
            ->addJoin('INNER JOIN TAB_CLIENT USING(ID_CLIENT)')
            ->addWhere('CLIENT_WEB=?', $customerCode);

        return $this->singleExec($query);
    }

	/**
	 * @param array $data
	 * @param int $idclient
	 * @return bool
	 */
	public function updateSubscriptionFromClient($data, $idclient) {
		$this->update('TAB_ABONNEMENT', $data, 'ID_CLIENT=:idclient', ['idclient' => $idclient]);
		return true;
	}

	/**
	 * @param int $idclient
	 * @return bool
	 */
	public function deleteSubscriptionFromClient($idclient) {
		$this->delete('TAB_ABONNEMENT', 'ID_CLIENT=?', $idclient);
		return true;
	}

    /**
     * Create a new Subscription
     * @param array $data
     * @return int|false the new id
     */
    public function createOrder($data)
    {
		if(!isset($data['COMMANDE']['CMD_DATESIGNATURE'])) $data['COMMANDE']['CMD_DATESIGNATURE'] = date('Y-m-d',mktime());
		return $this->insert('TAB_COMMANDE', $data['COMMANDE']);
    }

    /**
     * Retrieve a command from its ID
     * @param integer $id
     * @return Model|false
     */
    public function getOrder($id)
    {
        return $this->singleExec('SELECT ID_COMMANDE, TAB_ABONNEMENT.ID_CLIENT, ID_ABONNEMENT, ID_BANCAIRE, CMD_DATE, CMD_TYPEPAIEMENT,
										CMD_MONTANTHT, CMD_MONTANTTVA, CMD_INTITULE, CMD_INCREMENT, CMD_MENSUALITE, CMD_DATEPAIEMENT,
										CMD_TAUXTVA, CMD_RUM, CMD_TYPESEQUENCE, CMD_DATESIGNATURE, ID_RESPUSER, CLIENT_WEB, ID_SOCIETE
								 FROM TAB_COMMANDE INNER JOIN TAB_ABONNEMENT USING(ID_ABONNEMENT) INNER JOIN TAB_CLIENT USING(ID_CLIENT)
								 WHERE ID_COMMANDE=?', $id);
    }

    /**
     * Update a command
     * @param array $data
     * @param int $id Command id
     * @return int
     */
    public function updateOrder($data, $id)
    {
        if(isset($data['COMMANDE']))
            $this->update('TAB_COMMANDE', $data['COMMANDE'], 'ID_COMMANDE=?', $id);
        return true;
    }

    /**
     * Get a list of all ongoing orders for a given customer
     * @param string $groupe client groupe name
	 * @param string $orderDate date
     * @return Model[]
     */
    function getAllCurrentOrdersFromGroupAndDate($groupe, $orderDate)
    {
        $query = 'SELECT TAB_COMMANDE.ID_COMMANDE, ID_FACTURE, FACT_DATE, CMD_DATE, CMD_DATEPAIEMENT, CMD_INTITULE, CMD_MENSUALITE, CMD_INCREMENT, ID_CLIENT, CLIENT_GROUPE, ID_SOCIETE
                     FROM TAB_COMMANDE INNER JOIN TAB_ABONNEMENT USING(ID_ABONNEMENT) INNER JOIN TAB_CLIENT USING(ID_CLIENT) LEFT JOIN TAB_FACTURE ON(TAB_FACTURE.ID_COMMANDE=TAB_COMMANDE.ID_COMMANDE)
                     WHERE CLIENT_GROUPE=? AND CMD_DATEPAIEMENT<=? ORDER BY CMD_DATE DESC, TAB_COMMANDE.ID_COMMANDE DESC, FACT_DATE DESC';
        return $this->exec($query, [$groupe, $orderDate]);
    }

    /**
     * Retrieve a  bill
     * @param int $id
     * @return Model|false
     */
    function getInvoice($id)
    {
        $query = $this->getBasicInvoiceQuery();
        $query->addWhere('ID_FACTURE=?', $id);
        return $this->singleExec($query);
    }

	/**
	 * @param $idorder
	 * @param $factDate
	 * @param $group
	 * @return bool|Model
	 */
	function getInvoiceFromOrderAndDateAndGroup($idorder, $factDate, $group)
	{
		$query = $this->getBasicInvoiceQuery();
		$query->addWhere('ID_COMMANDE=?', $idorder);
		$query->addWhere('FACT_DATE=?', $factDate);
		$query->addWhere('CLIENT_GROUPE=?', $group);
		return $this->singleExec($query);
	}

	/**
	 * @param $idorder
	 * @param $factDate
	 * @param $group
	 * @return bool|Model
	 */
	function getInvoiceFromOrderAndDate($idorder, $factDate)
	{
		$query = $this->getBasicInvoiceQuery();
		$query->addWhere('ID_COMMANDE=?', $idorder);
		$query->addWhere('FACT_DATE=?', $factDate);
		return $this->singleExec($query);
	}

	/**
	 * Build a mysql query for an invoice
	 * @return Query
	 */
	function getBasicInvoiceQuery()
	{
		$query = new Query();
		$query->select('ID_FACTURE, FACT_DATE, FACT_MONTANTHT, FACT_TAUXTVA, FACT_MONTANTTVA, FACT_REFERENCE, ID_COMMANDE, FACT_TVA, FACT_ADR, FACT_VILLE, FACT_CP, FACT_PAYS, FACT_SOCIETE,
                       CMD_DATE, CMD_TYPEPAIEMENT, CMD_INTITULE, CMD_INCREMENT, CMD_MENSUALITE, CMD_DATEPAIEMENT, CMD_RUM, CMD_DATESIGNATURE, ID_CLIENT, CLIENT_GROUPE')
			->from('TAB_FACTURE')
			->addJoin('INNER JOIN TAB_COMMANDE USING(ID_COMMANDE) INNER JOIN TAB_ABONNEMENT USING(ID_ABONNEMENT) INNER JOIN TAB_CLIENT USING(ID_CLIENT)');
		return $query;
	}

    /**
     * Create a new bill
     * @param array $data
     * @return int the bill ID
     */
    function createFacture($data)
    {
        return $this->insert('TAB_FACTURE', $data);
    }

    /**
     * Update a bill
     * @param array $data
     * @param int $id Bill ID
     * @return int
     */
    function updateFacture($data, $id)
    {
        return $this->update('TAB_FACTURE', $data, 'ID_FACTURE=?',$id);
    }
}
