<?php
/**
 * Created by PhpStorm.
 * User: Wish
 * Date: 03/12/2016
 * Time: 16:17
 */
namespace BoondManager\Databases\BoondManager;

use BoondManager\Lib\Cluster;
use BoondManager\Services\BM;
use Wish\Models\Model;
use Wish\MySQL\Query;
use Wish\Tools;

/**
 * Class Vendor
 * @package BoondManager\Databases\BoondManager
 */
class Vendor extends AbstractObject {
	/**
	 * Update a vendor
	 * @param $data
	 * @param $id
	 * @return bool
	 */
	public function updateVendor($data, $id)
	{
		if(isset($data['VENDEUR']))
			$this->update('VENDEUR', $data['VENDEUR'], 'ID_VENDEUR=:id', ['id'=>$id]);
		return true;
	}

	/**
	 * Create a vendor
	 * @param $data
	 * @return bool
	 */
	public function createVendor($data)
	{
		if(isset($data['VENDEUR']))
			return $this->insert('VENDEUR', $data['VENDEUR']);
		return false;
	}

	/**
	 * @param $id
	 * @return bool|Model
	 */
	public function getVendor($id)
	{
		$query = $this->getBasicVendorQuery();
		$query->addWhere('ID_VENDEUR=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * @param $id
	 * @return bool|Model
	 */
	public function getVendorFromClient($id)
	{
		$query = $this->getBasicVendorQuery();
		$query->addWhere('ID_CLIENT=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * Buil a mysql query for a vendor
	 * @return Query
	 */
	public function getBasicVendorQuery()
	{
		$query = new Query();
		$query->addColumns('ID_VENDEUR, VENDEUR_NOM, VENDEUR_DESCRIPTION, VENDEUR_INSCRIPTION, VENDEUR_TEL, VENDEUR_EMAIL, VENDEUR_WEB, VENDEUR_ADR, VENDEUR_CP, VENDEUR_VILLE, VENDEUR_PAYS, ID_CLIENT')
			->from('TAB_VENDEUR')
			->addJoin('INNER JOIN TAB_CLIENT USING(ID_CLIENT)');
		return $query;
	}

	/**
	 * @param $id
	 * @param $customerCode
	 * @return bool
	 */
	public function deleteVendor($id, $customerCode) {
		$cluster = new Cluster();
		$imgFile = Tools::whichImageWithExtensionExist(BM::getRootPath().'/www/img/marketplace/logo/vendor/'.$customerCode);
		$cluster->deleteFile('www/img/marketplace/logo/vendor/'.$imgFile);

		$dbApp = new App();
		$dbApp->deleteAllMainAppsFromVendor($id);
		$this->delete('TAB_VENDEUR', 'ID_VENDEUR=?', $id);
		return true;
	}
}
