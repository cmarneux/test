<?php
/**
 * absences.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\Models\Model;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;
use BoondManager\OldModels\Filters;

class Absence extends AbstractObject{

	/**
	 * perform a search on absence based on a given filter
	 * @param Filters\Search\Absences $filter
	 * @return \Wish\Models\SearchResult
	 */
	public function search(Filters\Search\Absences $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();

		$query->setModelClass(Models\Absence::class);
		$query->setMapper(new Mappers\SearchAbsence());
		$query->addWhere($this->getPerimeterSearch($filter->getSelectedPerimeter(), 'TAB_LISTEABSENCES.ID_SOCIETE', 'TAB_PROFIL.ID_POLE', ['USRESP.ID_PROFIL','USRH.ID_PROFIL']));

		//KEYWORDS
		if($keywords = $filter->keywords->getValue()) {
			$searchIds = $this->getListIdSearch($keywords, ['COMP'=>'TAB_PROFIL.ID_PROFIL']);
			if($searchIds->isEmpty()){
				$keywords2 = str_replace('*', '%', str_replace('%', '\%', $keywords));
				$searchIds = new Where('(PROFIL_NOM LIKE ? OR PROFIL_PRENOM LIKE ?)',[
					$keywords2.'%',
					$keywords2.'%'
				]);
			}
			$query->addWhere($searchIds);
		}

		$query->addWhere( $this->getFilterSearch($filter->resourceTypes->value, 'PROFIL_TYPE'));

		//ON construit la liste des exclusions
		if($excludeTypes = $filter->excludeResourceTypes->getValue()){
			$query->addWhere('PROFIL_TYPE NOT IN (?)', $excludeTypes);
		}

		if($filter->period->getValue() == Filters\Search\Absences::PERIOD_OVERLAPS)
			$query->addWhere('DATEDIFF (?, PABS_DEBUT) >= 0 AND DATEDIFF(PABS_FIN,?) >= 0', [
				$filter->endDate->getValue(),
				$filter->startDate->getValue()
			]);

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());
        $query->setLimit(  $filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->from('TAB_PABSENCE
			INNER JOIN TAB_LISTEABSENCES USING(ID_LISTEABSENCES)
			INNER JOIN TAB_PROFIL USING(ID_PROFIL)
			LEFT JOIN TAB_USER USRESP ON(TAB_PROFIL.ID_RESPMANAGER = USRESP.ID_USER)
			LEFT JOIN TAB_USER USRH ON(TAB_PROFIL.ID_RESPRH = USRH.ID_USER)
			INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE)
			INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE)
			LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=PABS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL)');
		$query->groupBy('TAB_PABSENCE.ID_PABSENCE');
		$query->select('TAB_LISTEABSENCES.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT, PROFIL_TYPE, TAB_SOCIETE.SOCIETE_RAISON, ID_PABSENCE, PABS_DEBUT, PABS_FIN, PABS_DUREE, PABS_INTITULE, PABS_TYPEHREF, ID_LISTEABSENCES, LISTEABSENCES_DATE, LISTEABSENCES_TITRE, LISTEABSENCES_COMMENTAIRES, TAB_LISTEABSENCES.ID_SOCIETE, GRPCONF_TAUXHORAIRE, TAB_USER.ID_USER, TAB_USER.USER_TAUXHORAIRE, TYPEH_NAME, LISTEABSENCES_ETAT AS VAL_STATE');

		return $this->launchSearch($query);
	}

	/**
	 * Add an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			Filters\Search\Absences::ORDERBY_RESOURCE_LASTNAME => 'TAB_PROFIL.PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy( 'TAB_PROFIL.ID_PROFIL ASC' );
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy( $mapping[$c].' '.$order);

		$query->addOrderBy('TAB_PABSENCE.ID_PABSENCE DESC');
	}

	/**
	 * Récupère la liste des lignes des demandes d'absences sur une période donnée appartenant à un profil (On ne tient compte que des DA validés ou en attente de validation)
	 * @param int $idprofil identifiant du profil
	 * @param int $idsociete
	 * @param string $debutPeriode `YYYY-MM-01`
	 * @param string $finPeriode `YYYY-MM-01`
	 * @return array
	 */
	public function getListeLignesDemandesAbsences($idprofil, $idsociete, $debutPeriode, $finPeriode) {
		$query = new Query();
		$query->select('ID_LISTEABSENCES, PABS_DEBUT, PABS_FIN, PABS_INTITULE, PABS_TYPEHREF, PABS_DUREE, LISTEABSENCES_DATE, LISTEABSENCES_TITRE')
			->from('TAB_PABSENCE')
			->addJoin('INNER JOIN TAB_LISTEABSENCES USING(ID_LISTEABSENCES)')
			->addWhere('TAB_LISTEABSENCES.ID_PROFIL= ?', $idprofil)
			->addWhere(' LISTEABSENCES_ETAT IN(1,2) ')
			->addWhere('TAB_LISTEABSENCES.ID_SOCIETE=?', $idsociete)
			->addWhere(' DATEDIFF( PABS_FIN, ?)>=0 AND DATEDIFF(?, PABS_DEBUT)>=0', [$debutPeriode, $finPeriode])
			->groupBy('ID_PABSENCE');

		return $this->exec($query);
	}

	/**
	 * Récupère la liste des absences d'un profil sur une période donnée (On ne tient compte que des documents totalement validés)
	 * @param int $idprofil identifiant du profil
	 * @param array $periode tableau d'identifiant d'absence et de période donnée au format YYYY-MM-01
	 * @return array
	 */
	public function getListeAbsencesPrises($idprofil, $periode = []) {
		$query = new Query();
		$query->select('ID_TEMPS, TYPEH_REF, TEMPS_DUREE, TEMPS_DATE, GRPCONF_TAUXHORAIRE, GRPCONF_CALCULDECOMPTE, SOCIETE_RAISON, TYPEH_NAME, TAB_LISTETEMPS.ID_SOCIETE')
			->from('TAB_LIGNETEMPS')
			->addJoin('INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS ON(TAB_LISTETEMPS.ID_LISTETEMPS=TAB_LIGNETEMPS.ID_LISTETEMPS)')
			->addJoin('INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE) INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_GROUPECONFIG.ID_SOCIETE)')
			->addJoin('INNER JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND TYPEH_TYPEACTIVITE=1)')
			->addWhere('ID_PROJET="-1" AND LISTETEMPS_ETAT=1')
			->addWhere('TAB_LISTETEMPS.ID_PROFIL=?', $idprofil) //"'.$this->db->escape($idprofil).'"'.$where_period.' ')
			->groupBy('ID_TEMPS')
			->addOrderBy('LISTETEMPS_DATE DESC');

		$where_period = new Where();
		foreach($periode as $arrPer) {
			$fin = explode('-', $arrPer[1]);
			$fin = date('Y-m-d', mktime(0,0,0,$fin[1],0,$fin[0]+1));
			$where_period->or_('LIGNETEMPS_TYPEHREF= ? AND TEMPS_DATE BETWEEN ? AND ?', [$arrPer[0], $arrPer[1], $fin]);
		}
		$query->addWhere($where_period);

		return $this->exec($query);
	}

	/**
	 * \brief Récupère la liste des absences posés d'un profil sur une période donnée (On ne tient compte que des DA validés ou en attente de validation)
	 * @param <type> $idprofil identifiant du profil
	 * @param <type> $periode tableau de période donnée au format YYYY-MM-01
	 * @return <type>
	 */
	public function getListeAbsencesPoses($idprofil, $periode = array()) {
		$query = new Query();
		$query->select('ID_PABSENCE, ID_LISTEABSENCES, PABS_DEBUT, PABS_FIN, PABS_INTITULE, PABS_TYPEHREF, TYPEH_REF, PABS_DUREE, TYPEH_NAME, LISTEABSENCES_DATE, LISTEABSENCES_TITRE, TAB_LISTEABSENCES.ID_SOCIETE, GRPCONF_TAUXHORAIRE, GRPCONF_CALCULDECOMPTE, SOCIETE_RAISON')
			->from('TAB_PABSENCE')
			->addJoin('INNER JOIN TAB_LISTEABSENCES USING(ID_LISTEABSENCES)')
			->addJoin('INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE) ')
			->addJoin(' INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_GROUPECONFIG.ID_SOCIETE)')
			->addJoin(' INNER JOIN TAB_TYPEHEURE ON(TYPEH_REF=PABS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE AND TYPEH_TYPEACTIVITE=1)')
			->addWhere('TAB_LISTEABSENCES.ID_PROFIL = ?', $idprofil)
			->addWhere('LISTEABSENCES_ETAT IN(1,2)')
			->groupBy('ID_PABSENCE');

		$where_period = new Where();
		foreach($periode as $arrPer) {
			$fin = explode('-', $arrPer[1]);
			$fin = date('Y-m-d', mktime(0,0,0,$fin[1],0,$fin[0]+1));
			$where_period->or_('PABS_TYPEHREF= ? AND DATEDIFF(?, PABS_DEBUT)>=0 AND DATEDIFF(PABS_FIN, ?)>=0', [$arrPer[0], $fin, $arrPer[1]]);
		}
		$query->addWhere($where_period);

		return $this->exec($query);
	}

	/**
	 * Récupère la liste des quotas d'un profil sur une période donnée
	 * @param int $idprofil identifiant du profil
	 * @param array $periode tableau d'identifiant d'absence et de période donnée au format YYYY-MM-01
	 * @return Model|false
	 */
	public function getAllQuotas($idprofil, $periode = []) {
		$query = new Query();
		$query->select([
				'ID_INTQUO', 'INTQUO_PERIODE', 'INTQUO_TYPEHREF', 'INTQUO_QUOTA', 'INTQUO_BEINGACQUIRED', 'INTQUO_COMMENTAIRE',
				'INTQUO_CRONTABDETAIL', 'TYPEH_NAME', 'TYPEH_REF', 'SOCIETE_RAISON', 'TAB_INTQUOTA.ID_SOCIETE', 'GRPCONF_CALCULDECOMPTE'
			])
			->from('TAB_INTQUOTA')
			->addJoin('INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE)')
			->addJoin('INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_SOCIETE.ID_SOCIETE)')
			->addJoin('LEFT JOIN TAB_TYPEHEURE ON(INTQUO_TYPEHREF=TYPEH_REF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE)')
			->addWhere('ID_PROFIL = ?', $idprofil)
			->addOrderBy('ID_INTQUO ASC');

		$wherePeriod = new Where();
		foreach($periode as $arrPer){
			$wherePeriod->or_('INTQUO_TYPEHREF=? AND INTQUO_PERIODE=?', [$arrPer[0], $arrPer[1]]);
		}
		$query->addWhere($wherePeriod);

		return $this->exec($query);
	}

	/**
	 * \brief Met à jour la liste des quotas d'un profil
	 * @param <type> $data tableau de données
	 * @param <type> $idprofil identifiant du profil
	 * @return <type>
	 */
	public function updateQuotas($data, $idprofil) {
		die('migrate updateQuotas !');
		//On supprime les quotas qui n'existent plus en BDD
		foreach($this->db->fetch_all_array('SELECT ID_INTQUO FROM TAB_INTQUOTA WHERE ID_PROFIL="'.$idprofil.'";') as $idquo) {
			$etat = false;
			foreach($data as $data_quota) if($data_quota[0] == $idquo['ID_INTQUO']) {$etat = true;break;}
			if(!$etat) $this->db->query_delete('INTQUOTA', 'ID_INTQUO="'.$idquo['ID_INTQUO'].'"');
		}

		//On met à jour les quotas du profil
		foreach($data as $data_quota) {
			if($data_quota[0] == 0) {//Il s'agit d'une nouveau quota
				$this->db->query_insert('INTQUOTA', array('INTQUO_TYPEHREF' => $data_quota[1],
					'INTQUO_PERIODE' => $data_quota[3],
					'INTQUO_QUOTA' => $data_quota[4],
					'ID_SOCIETE' => $data_quota[2],
					'ID_PROFIL' => $idprofil));
			} else {
				$this->db->query_update('INTQUOTA', array('INTQUO_TYPEHREF' => $data_quota[1],
					'INTQUO_PERIODE' => $data_quota[3],
					'INTQUO_QUOTA' => $data_quota[4],
					'ID_SOCIETE' => $data_quota[2]), 'ID_INTQUO="'.$this->db->escape($data_quota[0]).'"');
			}
		}
		return true;
	}

	/**
	 * \brief Récupère un quota personnalisé
	 * @param <type> $idquota
	 */
	public function getQuotaData($idquota) {
		$sql = 'SELECT ID_INTQUO, TAB_INTQUOTA.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, INTQUO_PERIODE, INTQUO_TYPEHREF, INTQUO_QUOTA, TYPEH_NAME, SOCIETE_RAISON, TAB_INTQUOTA.ID_SOCIETE
                FROM TAB_INTQUOTA INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE) INNER JOIN TAB_PROFIL USING(ID_PROFIL)
                    LEFT JOIN TAB_TYPEHEURE ON(INTQUO_TYPEHREF=TYPEH_REF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE)
                WHERE ID_INTQUO="'.$this->db->escape($idquota).'"';
		$this->data = $this->db->query_first($sql);
		return true;
	}

	/**
	 * \brief Met à jour un quota personnalisé
	 * @param <type> $data
	 * @param <type> $idquota
	 */
	public function updateQuotaData($data, $idquota=0) {
		if($idquota > 0) {
			$this->db->query_update('INTQUOTA', $data, 'ID_INTQUO="'.$this->db->escape($idquota).'"');
			return true;
		} else
			return $this->db->query_insert('INTQUOTA', $data);
	}

	/**
	 * \brief Supprime un quota personnalisé
	 * @param <type> $idquota
	 */
	public function deleteQuotaData($idquota) {
		$this->db->query_delete('INTQUOTA', 'ID_INTQUO="'.$this->db->escape($idquota).'"');
		return true;
	}
}
