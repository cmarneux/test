<?php
/**
 * absencesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\AbsencesReports\Filters\SearchAbsencesReports;
use Wish\MySQL\Query;
use Wish\Models\SearchResult;
use Wish\MySQL\Where;
use Wish\Tools;
use BoondManager\OldModels\MySQL;
use Wish\MySQL\AbstractDb;
use BoondManager\Models\Validation;

class AbsencesReport extends AbstractObject{

	/**
	 * Search for absences
	 * @param SearchAbsencesReports $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	public function search(SearchAbsencesReports $filter ){

		if(!$filter->isValid()) throw new \Exception('filter is not valid');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));
		$query->select('DOCUMENT.ID_LISTEABSENCES AS ID_DOCUMENT, DOCUMENT.LISTEABSENCES_DATE AS DOCUMENT_DATE, 
		                DOCUMENT.LISTEABSENCES_ETAT AS DOCUMENT_ETAT, AT.ID_PROFIL, AT.PROFIL_NOM, AT.PROFIL_PRENOM, 
		                AT.PROFIL_REFERENCE, AT.PROFIL_TYPE, DOCUMENT.ID_SOCIETE, DOCUMENT.LISTEABSENCES_TITRE');
		$query->from('TAB_LISTEABSENCES AS DOCUMENT');
		$query->addJoin('INNER JOIN TAB_PROFIL AT ON (AT.ID_PROFIL=DOCUMENT.ID_PROFIL)
							LEFT JOIN TAB_USER US ON (US.ID_USER=AT.ID_RESPMANAGER) 
							LEFT JOIN TAB_USER USRH ON (USRH.ID_USER=AT.ID_RESPRH)
							LEFT JOIN TAB_PABSENCE ON (ID_PABSENCE=(
							SELECT ID_PABSENCE FROM TAB_PABSENCE 
							WHERE TAB_PABSENCE.ID_LISTEABSENCES=DOCUMENT.ID_LISTEABSENCES 
							AND DATEDIFF('.$this->escape($filter->endMonth->getValue()).',PABS_DEBUT)>=0 
							AND DATEDIFF(PABS_FIN,'.$this->escape($filter->startMonth->getValue()).')>=0 
							GROUP BY TAB_PABSENCE.ID_LISTEABSENCES 
							LIMIT 1
							))');
		$query->addJoin('LEFT JOIN TAB_VALIDATION ON(ID_PARENT=DOCUMENT.ID_LISTEABSENCES AND VAL_TYPE=2)');
		$query->addWhere('(DOCUMENT.LISTEABSENCES_DATE BETWEEN ? AND ? OR ( DATEDIFF( ? , PABS_DEBUT )>=0 AND DATEDIFF( PABS_FIN , ? )>=0 ))',
			             [$filter->startMonth->getValue(), $filter->endMonth->getValue(), $filter->endMonth->getValue(), $filter->startMonth->getValue()]);

		$stateWhere = new Where();

		if($filter->perimeterManagersType->getValue() == SearchAbsencesReports::PERIMETER_TYPE_VALIDATOR){

			$query->select('ID_VALIDATION, VAL_DATE, VAL_ETAT AS VAL_STATE, RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM, RESP.ID_PROFIL AS RESP_ID_PROFIL');
			$query->addJoin('LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=TAB_VALIDATION.ID_PROFIL) LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)');
			$query->addWhere( $this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'RESP.ID_SOCIETE',
				'RESP.ID_POLE',
				'RESP.ID_PROFIL'
			));
			$this->setOrderExprForValidators($query, $filter->sort->getValue(), $filter->order->getValue());

			foreach($filter->validationStates->getValue() as $etat){
				switch($etat){
					case Validation::VALIDATION_VALIDATED:
						$stateWhere->or_('VAL_ETAT=1'); break;
					case Validation::VALIDATION_REJECTED:
						$stateWhere->or_('VAL_ETAT=3'); break;
					case Validation::VALIDATION_WAITING:
						$stateWhere->or_('VAL_ETAT=2 OR VAL_ETAT IS NULL'); break;
				}
			}
			$query->setCountColumn('DOCUMENT.ID_LISTEABSENCES');
		}else {
			$query->select('DOCUMENT.LISTEABSENCES_ETAT AS VAL_STATE, ID_VALIDATION, VAL_DATE');
			$query->addWhere($this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'AT.ID_SOCIETE',
				'AT.ID_POLE',
				['US.ID_PROFIL', 'USRH.ID_PROFIL']
			));
			$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

			foreach($filter->validationStates->getValue() as $etat){
				switch($etat){
					case Validation::VALIDATION_VALIDATED:
						$stateWhere->or_('LISTEABSENCES_ETAT=1'); break;
					case Validation::VALIDATION_REJECTED:
						$stateWhere->or_('LISTEABSENCES_ETAT=3'); break;
					case Validation::VALIDATION_WAITING:
						$stateWhere->or_('LISTEABSENCES_ETAT=2'); break;
				}
			}
			$query->groupBy('DOCUMENT.ID_LISTEABSENCES');
		}

		$query->addWhere($stateWhere);

		if($filter->keywords->isDefined()){
			$whereIds = $this->getListIdSearch($filter->keywords->getValue(), ['COMP'=>'AT.ID_PROFIL', 'DOC'=>'DOCUMENT.ID_LISTEABSENCES']);
			if(!$whereIds->isEmpty()){
				$query->addWhere($whereIds);
			}else{

				$keywords = str_replace('*', '%', str_replace('%', '\%', $filter->keywords->getValue()));
				$query->addWhere('AT.PROFIL_NOM LIKE ? OR AT.PROFIL_PRENOM LIKE ?', [$keywords.'%', $keywords.'%']);
			}
		}

		if($filter->resourceTypes->isDefined()){
			$query->addWhere('AT.PROFIL_TYPE IN (?)', $filter->resourceTypes->getValue());
		}


		$result = $this->launchSearch($query);

		$ids = Tools::getFieldsToArray($result->rows, 'ID_DOCUMENT');

		$groupedDemands = [];
		if($ids) {
			$sql = 'SELECT ID_LISTEABSENCES AS ID_DOCUMENT, ID_PABSENCE, PABS_DEBUT, PABS_FIN, PABS_DUREE, PABS_INTITULE, PABS_TYPEHREF, TYPEH_NAME 
			        FROM TAB_PABSENCE 
			        INNER JOIN TAB_LISTEABSENCES USING(ID_LISTEABSENCES) 
			        LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=PABS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE) 
			        WHERE TAB_PABSENCE.ID_LISTEABSENCES IN (' . Where::prepareWhereIN($ids). ')';
			$demands = $this->exec($sql, $ids);
			foreach($demands as $d){
				if(!array_key_exists($d->ID_DOCUMENT, $groupedDemands)) $groupedDemands[$d->ID_DOCUMENT] = [];
				$groupedDemands[$d->ID_DOCUMENT][] = $d;
			}
		}

		foreach($result->rows as $r){
			$r->LISTE_PABSENCES = array_key_exists($r->ID_DOCUMENT, $groupedDemands) ? $groupedDemands[$r->ID_DOCUMENT] : [];
		}

		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			SearchAbsencesReports::ORDERBY_LASTNAME => 'AT.PROFIL_NOM',
			SearchAbsencesReports::ORDERBY_DATE     => 'DOCUMENT.LISTEABSENCES_DATE',
			SearchAbsencesReports::ORDERBY_STATE    => 'DOCUMENT.LISTEABSENCES_ETAT',
		];

		if(!$column) $query->addOrderBy('DOCUMENT.LISTEABSENCES_DATE DESC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('DOCUMENT.ID_LISTEABSENCES DESC');
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExprForValidators(Query $query, $column, $order) {

		$mapping = [
			SearchAbsencesReports::ORDERBY_LASTNAME           => 'AT.PROFIL_NOM',
			SearchAbsencesReports::ORDERBY_STATE              => 'VAL_ETAT',
			SearchAbsencesReports::ORDERBY_VALIDATOR_LASTNAME => 'RESP.PROFIL_NOM',
			SearchAbsencesReports::ORDERBY_DATE               => 'DOCUMENT.LISTEABSENCES_DATE',
			SearchAbsencesReports::ORDERBY_VALIDATIONDATE     => 'VAL_DATE'
		];

		if(!$column) $query->addOrderBy('DOCUMENT.LISTEABSENCES_DATE DESC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('DOCUMENT.ID_LISTEABSENCES DESC');
	}

	/**
	 * @param $idprofil
	 * @param bool $fullinformation
	 * @return \Wish\Models\Model[]|false|int
	 * @throws \Exception
	 */
	public function getAllAbsences($idprofil, $fullinformation = false) {
		$result = $this->exec(
			'SELECT ID_LISTEABSENCES, LISTEABSENCES_DATE, LISTEABSENCES_ETAT AS VAL_STATE, LISTEABSENCES_TITRE, ID_VALIDATION 
			 FROM TAB_LISTEABSENCES 
			 LEFT JOIN TAB_VALIDATION ON(ID_PARENT=ID_LISTEABSENCES AND VAL_TYPE=2) 
			 WHERE TAB_LISTEABSENCES.ID_PROFIL= ? 
			 GROUP BY ID_LISTEABSENCES ORDER BY LISTEABSENCES_DATE DESC',
			$idprofil
		);
		if($result) {
			if($fullinformation) {
				$tabIDs = [];
				foreach($result as $resultat) $tabIDs[] = $resultat['ID_LISTEABSENCES'];
				$sql = 'SELECT ID_LISTEABSENCES, PABS_DEBUT, PABS_FIN, PABS_DUREE, PABS_INTITULE, PABS_TYPEHREF, TYPEH_NAME 
				        FROM TAB_PABSENCE 
				        INNER JOIN TAB_LISTEABSENCES USING(ID_LISTEABSENCES) 
				        LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=PABS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTEABSENCES.ID_SOCIETE) 
				        WHERE TAB_PABSENCE.ID_LISTEABSENCES IN ('.Where::prepareWhereIN($tabIDs).')';
				$liste_absences = $this->exec($sql, $tabIDs);
				foreach($liste_absences as $absence) {
					foreach($result as $resultat) {
						if($resultat['ID_LISTEABSENCES'] == $absence['ID_LISTEABSENCES']) {
							if(!isset($resultat->LISTE_PABSENCES)) $resultat->LISTE_PABSENCES = array();
							$resultat->LISTE_PABSENCES[] = $absence;
						}
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Retrieve all data for an absence
	 * @param int $idabsence absence id
	 * @return \BoondManager\Models\AbsencesReport|false
	 * @throws \Exception
	 */
	public function getAbsenceData($idabsence) {
		$query = new Query();
		$query->select('ID_LISTEABSENCES, TAB_LISTEABSENCES.ID_PROFIL, TAB_LISTEABSENCES.ID_SOCIETE, LISTEABSENCES_DATE, 
		                LISTEABSENCES_ETAT, LISTEABSENCES_TITRE, LISTEABSENCES_COMMENTAIRES, ID_POLE, ID_RESPMANAGER, 
		                PMANAGER.ID_PROFIL AS ID_PROFIL_RESPMANAGER, PRH.ID_PROFIL AS ID_PROFIL_RESPRH, ID_RESPRH, TAB_USER.USER_TYPE, TAB_USER.ID_USER'
			)->from('TAB_LISTEABSENCES')
			->addJoin('INNER JOIN TAB_PROFIL USING(ID_PROFIL)')
			->addJoin('LEFT JOIN TAB_USER USING(ID_PROFIL)')
			->addJoin('LEFT JOIN TAB_USER AS PMANAGER ON TAB_PROFIL.ID_RESPMANAGER = PMANAGER.ID_USER')
			->addJoin('LEFT JOIN TAB_USER AS PRH ON TAB_PROFIL.ID_RESPRH = PRH.ID_USER')
			->addWhere('ID_LISTEABSENCES = ?', $idabsence);

		$result = $this->singleExec($query);

		if($result) {

			//On récupère tous les justifiatifs
			$sqlProof = 'SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF WHERE ID_PARENT=? AND JUSTIF_TYPE=2';
			$result['PROOFS'] = $this->exec($sqlProof, $idabsence);

			//On récupère les absences de cette demande
			$sqlAbsences = 'SELECT ID_PABSENCE, PABS_DEBUT, PABS_FIN, PABS_DUREE, PABS_INTITULE, PABS_TYPEHREF, TYPEH_NAME 
			                FROM TAB_PABSENCE 
			                LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=PABS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE= ? ) 
			                WHERE ID_LISTEABSENCES= ?  
			                GROUP BY ID_PABSENCE 
			                ORDER BY ID_PABSENCE ASC';
			$result['LISTEABSENCES'] = $this->exec($sqlAbsences, [$result['ID_SOCIETE'], $idabsence]);

			$sqlValidations = 'SELECT ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, TAB_VALIDATION.ID_PROFIL, 
			                          PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT 
			                   FROM TAB_VALIDATION 
			                   INNER JOIN TAB_PROFIL ON(ID_VALIDATEUR=TAB_PROFIL.ID_PROFIL) 
			                   WHERE ID_PARENT= ?  AND VAL_TYPE=2 
			                   ORDER BY ID_VALIDATION ASC';
			$result['VALIDATIONS'] = $this->exec($sqlValidations, $idabsence);

		}
		return $result;
	}

	/**
	 * Update or create an absence
	 * @param array $data data to insert (or update)
	 * @param int $idabsence absence id (if update)
	 * @return int id of the newly created absence
	 */
	public function updateAbsenceData($data, $idabsence=0) {

		if(isset($data['DEMANDEABSENCES'])) {
			($idabsence > 0)
				? $this->update('TAB_LISTEABSENCES', $data['DEMANDEABSENCES'], 'ID_LISTEABSENCES=:id' , ['id'=>$idabsence])
				: $idabsence = $this->insert('TAB_LISTEABSENCES', $data['DEMANDEABSENCES']);
		}

		if(isset($data['LISTEABSENCES']) && $idabsence > 0) {
			//On supprime les absences qui n'existent plus
			foreach($this->exec('SELECT ID_PABSENCE FROM TAB_PABSENCE WHERE ID_LISTEABSENCES=?', $idabsence) as $ligneabs) {
				$etat = false;
				foreach($data['LISTEABSENCES'] as $data_ligneabs) {
					if($data_ligneabs['ID_PABSENCE'] == $ligneabs['ID_PABSENCE']) {
						$etat = true;
						break;
					}
				}
				if(!$etat) $this->delete('TAB_PABSENCE', 'ID_PABSENCE=? AND ID_LISTEABSENCES=?', [$ligneabs['ID_PABSENCE'], $idabsence]);
			}

			//On met à jour les absences
			if($data['LISTEABSENCES']) {
				foreach($data['LISTEABSENCES'] as $dataLigne) {
					if( $dataLigne['ID_PABSENCE'] > 0) {
						$this->update('TAB_PABSENCE', $dataLigne, 'ID_PABSENCE=:id AND ID_LISTEABSENCES= :idList',['id'=>$dataLigne['ID_PABSENCE'], 'idList' => $idabsence]);
					} else {
						$dataLigne['ID_LISTEABSENCES'] = $idabsence;
						$this->insert('TAB_PABSENCE', $dataLigne);
					}
				}
			}
		}
		return $idabsence;
	}

	/**
	 * Delete an absence
	 * @param int $idabsence Absence ID
	 * @return true
	 */
	public function deleteAbsenceData($idabsence) {
		$this->delete('TAB_PABSENCE', 'ID_LISTEABSENCES=?', $idabsence);
		$this->delete('TAB_VALIDATION', 'ID_PARENT= ? AND VAL_TYPE=2', $idabsence);
		$this->delete('TAB_LISTEABSENCES', 'ID_LISTEABSENCES= ?', $idabsence);

		//ON SUPPRIME LES JUSTIFICATIFS
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_JUSTIFICATIVE, $idabsence, 2);
		return true;
	}

	/**
	 * Retrieve a list of absences requests on a given profile and period (Note that the request must have been validaded by a manager)
	 * @param int $idprofil
	 * @param int $idsociete
	 * @param string $debutPeriode format YYYY-MM-01
	 * @param string $finPeriode format YYYY-MM-01
	 * @return \Wish\Models\Model[]|false
	 * @throws \Exception
	 */
	public function getListeLignesDemandesAbsences($idprofil, $idsociete, $debutPeriode, $finPeriode) {
		$sql ='SELECT ID_LISTEABSENCES, PABS_DEBUT, PABS_FIN, PABS_INTITULE, PABS_TYPEHREF, PABS_DUREE, LISTEABSENCES_DATE, LISTEABSENCES_TITRE
		       FROM TAB_PABSENCE 
		       INNER JOIN TAB_LISTEABSENCES USING(ID_LISTEABSENCES) 
		       INNER JOIN TAB_VALIDATION ON(ID_PARENT=TAB_LISTEABSENCES.ID_LISTEABSENCES AND VAL_TYPE=2 AND VAL_ETAT=1)
		       WHERE TAB_LISTEABSENCES.ID_PROFIL= ? 
		       AND TAB_LISTEABSENCES.ID_SOCIETE= ?
		       AND DATEDIFF(?, PABS_DEBUT)>=0 AND DATEDIFF(PABS_FIN,?)>=0
		       AND LISTEABSENCES_ETAT IN(1,2) GROUP BY ID_PABSENCE';
		return $this->exec($sql, [$idprofil, $idsociete, $finPeriode, $debutPeriode]);
	}

	/**
	 * \brief Récupère la liste des absences d'un profil sur une période donnée (Attention, la feuille des temps doit au minimum avoir été validé par un des validateurs)
	 * @param <type> $idprofil identifiant du profil
	 * @param <type> $periode tableau d'identifiant d'absence et de période donnée au format YYYY-MM-01
	 * @return <type>
	 */
	public function getListeAbsences($idprofil, $periode = array()) {
		$periodArgs = [];
		if(sizeof($periode) > 0) {
			$where_period = array();
			foreach($periode as $arrPer) {
				$fin = explode('-', $arrPer[1]);
				$fin = date('Y-m-d', mktime(0,0,0,$fin[1]-1,1,$fin[0]+1));
				$where_period[] = '(LIGNETEMPS_TYPEHREF= ? AND LISTETEMPS_DATE BETWEEN ? AND ?)';
				$periodArgs = array_merge($periodArgs, [$arrPer[0], $arrPer[1], $fin]);
			}
			$where_period = ' AND ('.implode(' OR ', $where_period).')';
		} else $where_period = '';

		$sql = 'SELECT TYPEH_REF, TEMPS_DUREE, LISTETEMPS_DATE, GRPCONF_TAUXHORAIRE, SOCIETE_RAISON, TYPEH_NAME, TAB_LISTETEMPS.ID_SOCIETE
		        FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS ON(TAB_LISTETEMPS.ID_LISTETEMPS=TAB_LIGNETEMPS.ID_LISTETEMPS)
		        INNER JOIN TAB_VALIDATION ON(1=(SELECT MAX(VAL_ETAT) AS VAL_STATE FROM TAB_VALIDATION WHERE ID_PARENT=TAB_LISTETEMPS.ID_LISTETEMPS AND VAL_TYPE=0))
		        INNER JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE) INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
		        INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_GROUPECONFIG.ID_SOCIETE)
		        WHERE ID_PROJET="-1" AND TAB_LISTETEMPS.ID_PROFIL= ? AND TYPEH_TYPEACTIVITE=1'.$where_period.' GROUP BY ID_TEMPS ORDER BY LISTETEMPS_DATE DESC;';
		return $this->exec($sql, array_merge([$idprofil], $periodArgs));
	}

	/**
	 * \brief Récupère la liste des quotas d'un profil sur une période donnée
	 * @param <type> $idprofil identifiant du profil
	 * @param <type> $periode tableau d'identifiant d'absence et de période donnée au format YYYY-MM-01
	 * @return <type>
	 */
	public function getAllQuotas($idprofil, $periode = array()) {
		$periodArgs = [];
		if(sizeof($periode) > 0) {
			$where_period = array();
			foreach($periode as $arrPer){
				$where_period[] = '(INTQUO_TYPEHREF=? AND INTQUO_PERIODE=?)';
				$periodArgs = array_merge($periodArgs, [$arrPer[0], $arrPer[1]]);
			}
			$where_period = ' AND ('.implode(' OR ', $where_period).')';
		} else $where_period = '';

		$sql = 'SELECT ID_INTQUO, INTQUO_PERIODE, INTQUO_TYPEHREF, INTQUO_QUOTA, TYPEH_NAME, SOCIETE_RAISON, TAB_INTQUOTA.ID_SOCIETE
		        FROM TAB_INTQUOTA 
		        INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE)
		        LEFT JOIN TAB_TYPEHEURE ON(INTQUO_TYPEHREF=TYPEH_REF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE)
		        WHERE ID_PROFIL= ? '.$where_period.' ORDER BY ID_INTQUO ASC';

		return $this->exec($sql, array_merge([$idprofil], $periodArgs));
	}

	/**
	 * Update a quotas for a given resource
	 * @param array $data data to update
	 * @param int $idprofil resource id
	 * @return true
	 */
	public function updateQuotas($data, $idprofil) {
		//On supprime les quotas qui n'existent plus en BDD
		foreach($this->exec('SELECT ID_INTQUO FROM TAB_INTQUOTA WHERE ID_PROFIL = ?', $idprofil) as $idquo) {
			$etat = false;
			foreach($data as $data_quota) if($data_quota[0] == $idquo['ID_INTQUO']) {
				$etat = true;
				break;
			}
			if(!$etat) $this->delete('TAB_INTQUOTA', 'ID_INTQUO=?',$idquo['ID_INTQUO']);
		}

		//On met à jour les quotas du profil
		foreach($data as $data_quota) {
			if($data_quota[0] == 0) {//Il s'agit d'une nouveau quota
				$this->insert('TAB_INTQUOTA', [
					'INTQUO_TYPEHREF' => $data_quota[1],
					'INTQUO_PERIODE'  => $data_quota[3],
					'INTQUO_QUOTA'    => $data_quota[4],
					'ID_SOCIETE'      => $data_quota[2],
					'ID_PROFIL'       => $idprofil
				]);
			} else {
				$this->update('TAB_INTQUOTA', [
					'INTQUO_TYPEHREF' => $data_quota[1],
					'INTQUO_PERIODE'  => $data_quota[3],
					'INTQUO_QUOTA'    => $data_quota[4],
					'ID_SOCIETE'      => $data_quota[2]
				], 'ID_INTQUO=?', $data_quota[0]);
			}
		}
		return true;
	}

	/**
	 * Retrieve a personalised quota
	 * @param int $idquota
	 * @return \Wish\Models\Model|false
	 */
	public function getQuotaData($idquota) {
		$sql = 'SELECT ID_INTQUO, TAB_INTQUOTA.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, INTQUO_PERIODE, INTQUO_TYPEHREF, 
		               INTQUO_QUOTA, TYPEH_NAME, SOCIETE_RAISON, TAB_INTQUOTA.ID_SOCIETE
		        FROM TAB_INTQUOTA 
		        INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE) 
		        INNER JOIN TAB_PROFIL USING(ID_PROFIL)
		        LEFT JOIN TAB_TYPEHEURE ON(INTQUO_TYPEHREF=TYPEH_REF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_INTQUOTA.ID_SOCIETE)
		        WHERE ID_INTQUO=?';
		return $this->singleExec($sql, $idquota);
	}

	/**
	 * Update (or create) a personalised Quota
	 * @param array $data
	 * @param int $idquota quota id to update
	 * @return int quota id
	 */
	public function updateQuotaData($data, $idquota=0) {
		if($idquota > 0) {
			$this->update('TAB_INTQUOTA', $data, 'ID_INTQUO=?', $idquota);
			return $idquota;
		} else
			return $this->insert('TAB_INTQUOTA', $data);
	}

	/**
	 * Delete a personalised quota
	 * @param $idquota
	 * @return int number of row deleted
	 */
	public function deleteQuotaData($idquota) {
		return $this->delete('TAB_INTQUOTA', 'ID_INTQUO=?', $idquota);
	}
}
