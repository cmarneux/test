<?php
/**
 * AbstractObject.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\Lib\MySQL\DbFactory;
use Wish\MySQL\Query;
use Wish\Models\SearchResult;
use Wish\MySQL\AbstractDb;

use Wish\MySQL\Where;
use BoondManager\Models;
use BoondManager\Services\CurrentUser;
use Wish\Tools;

/**
 * Class AbstractObject
 *
 * Base Object for all classes related to the database
 *
 * @package BoondManager\Databases\Local
 */
class AbstractObject extends AbstractDb {
	/**
	 * AbstractObject constructor.
	 */
	public function __construct(){
		$db = DbFactory::instance()->getDbClient();
		parent::__construct($db);
	}

	/**
	 * Gets table entry with id and returns the mapped RowObject
	 * @param integer $id
	 * @return \Wish\Models\Model|false
	 */
	public function getEntry($id) {
		$fullClass = get_called_class();
		$class = explode("\\", $fullClass);
		$class = strtoupper(end($class));
		$rowClass = str_replace("\\Local\\", "\\RowObject\\", $fullClass);
		$mapper = str_replace("\\Local\\", "\\Mappers\\", $fullClass);
		$idField = 'ID_'.$class;
		$table = 'TAB_'.$class;
		$sql = 'SELECT * FROM '.$table.' WHERE '.$idField.'=:id';
		return (new $mapper)->map($this->singleExec($sql, ['id' => $id], $rowClass));
	}

	/**
	 * do a search and store the number of all matching rows {@see self::$totalRows}
	 * @param Query $query a query used to perform a search
	 * @param string $resultClass class for the result
	 * @return SearchResult
	 */
	protected function launchSearch(Query $query, $resultClass = SearchResult::class) {
		$count = $this->singleExec($query->getCountQuery(), $query->getArgs());
		if($count && intval($count['NB_ROWS']) > 0) {
			$result = new $resultClass($this->exec($query), intval($count['NB_ROWS']), $count);
			return $result;
		} else return new $resultClass([], 0);
	}

	/**
	 * Build some where clauses based on the selected perimeter
	 *
	 * @param Models\Perimeter $selectedPerimeter
	 * @param  string $colSociete specify the table field for the agency
	 * @param  string $colPole specify the table field for the pole
	 * @param  string|array $colUser specify the table field(s) which can match for a user
	 * @param  boolean $allowMyAgency  tell if we can filter according to "My Perimeter":
	 * @param bool $narrowPerimeter if true all fields must match, if false any fields can match
	 * - `true` : If the filter "Perimeter" contain `0` the it will be filtered based on "My Perimeter" (All manager in the user hierarchy)
	 * @return \Wish\MySQL\Where
	 */
	protected function getPerimeterSearch(Models\Perimeter $selectedPerimeter, $colSociete, $colPole, $colUser, $allowMyAgency=true, $narrowPerimeter=null) {
		//TODO: voir pour enlever $allowMyAgency => possibilite de le gerer des la construction du filtre ?

		$tabSocietes = [];
		$tabManagers = [];
		$tabPoles = [];

		$user = CurrentUser::instance();

		if(count($selectedPerimeter))
		{
			//On construit le filtre de la société
			foreach ($selectedPerimeter->getAgencies() as $data){
				if ($data->getID() == 0 && $allowMyAgency) {//Mon agence
					if (sizeof($user->getManagers()) == 0)
						$tabManagers[ strval($user->getEmployeeId()) ] = $user->getEmployeeId();
					else
						foreach ($user->getManagers() as $manager)
							$tabManagers[ $manager->getID() ] = $manager->getID();
				} else {//Société
					$tabSocietes[] = $data->getID();
				}
			}
			foreach ($selectedPerimeter->getBUs() as $data){
				foreach($data->getSearchableManagers() as $manager)
					$tabManagers[$manager->getID()] = $manager->getID();
			}
			foreach ($selectedPerimeter->getPoles() as $data){
				$tabPoles[] = $data->getID();
			}
			foreach ($selectedPerimeter->getManagers() as $data){
				$tabManagers[$data->getID()] = $data->getID();
			}
		}

		$colWhere = array();
		$colArgs = array();
		if(sizeof($tabSocietes) > 0 && $colSociete != '') {
			$colWhere[] = $colSociete.' IN ('. Where::prepareWhereIN($tabSocietes) .')';
			$colArgs = array_merge($colArgs, $tabSocietes);
		}
		if(sizeof($tabPoles) > 0 && $colPole != '') {
			$colWhere[] = $colPole.' IN ('. Where::prepareWhereIN($tabPoles) .')';
			$colArgs = array_merge($colArgs, $tabPoles);
		}
		if(sizeof($tabManagers) > 0) {
			if(!is_array($colUser)) $colUser = [$colUser];
			foreach($colUser as $i => $tabUser) {
				if($tabUser=='') continue;
				$colWhere[] = $tabUser.' IN ('. Where::prepareWhereIN($tabManagers) .')';
				$colArgs = array_merge($colArgs, $tabManagers);
			}
		}

		if(sizeof($colWhere) > 0) return new Where(($narrowPerimeter == 1 || $user->getNarrowPerimeter() == 1 && $narrowPerimeter != 0)?implode(' AND ', $colWhere):'('.implode(' OR ', $colWhere).')', $colArgs);
		else return null;
	}

	/**
	 * Manage the deletion and creation of items inside a complex data into database
	 * @param array $data array of data
	 * @param string $keyTable database's table
	 * @param string $keyColumn database's field into the table
	 * @param array $tabArgs array of keys & values used to select/delete/update each items
	 * @return array
	 */
	public function deleteAndCreateItemsFromEntity($data, $keyTable, $keyColumn, $tabArgs) {
		$whereArgs = new Where();
		$j = 0;
		foreach($tabArgs as $key => $value) {
			$whereArgs->and_($key . '=:idwhere' . $j , ['idwhere' . $j => $value]);
			$j++;
		}

		$query = new Query();
		$query->select($keyColumn)
			->from($keyTable)
			->addWhere($whereArgs);
		$items = $this->exec($query);

		$idsExists =Tools::getFieldsToArray($items, $keyColumn);
		$idsInput = array_keys(Tools::useColumnAsKey($keyColumn, $data));
		$idIntersect = array_diff($idsExists, $idsInput);

		if($idIntersect) {
			$where = new Where($keyColumn . ' IN(' . Where::prepareNamedWhereIN('intersectId', $idIntersect) . ')', Where::prepareNamedWhereINArgs('intersectId', $idIntersect));
			$where->and_($whereArgs);
			$this->delete($keyTable, $where->getWhere(), $where->getArgs());
		}

		foreach ($data as $i => $item) {
			if (isset($item[$keyColumn])) {
				$where = new Where($keyColumn . '=:iditem', ['iditem' => $item[$keyColumn]]);
				$where->and_($whereArgs);
				$this->update($keyTable, $item, $where->getWhere(), $where->getArgs());
			} else {
				foreach ($tabArgs as $key => $value)
					$item[$key] = $value;

				$data[$i][$keyColumn] = $this->insert($keyTable, $item);
			}
		}
		return $data;
	}
}
