<?php
/**
 * account.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Local;

use BoondManager\APIs\Accounts\Filters\SearchAccounts;
use BoondManager\Databases\BoondManager\Login;
use BoondManager\Services\BM;
use Wish\Models\Model;
use BoondManager\Databases\BoondManager;
use BoondManager\Models;
use Wish\Models\SearchResult;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Tools;

/**
 * Class Account
 * @package BoondManager\Databases\Local
 */
class Account extends AbstractObject {
	/**
	 * Search projects
	 * @param SearchAccounts $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchAccounts(SearchAccounts $filter) {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('US.ID_USER, US.ID_ROLE, US.USER_TYPE, US.USER_LANGUE, US.USER_LOGIN, US.USER_ABONNEMENT, COMP.ID_PROFIL, 
							COMP.PROFIL_NOM, COMP.PROFIL_PRENOM, COMP.PROFIL_TYPE, COMP.PROFIL_EMAIL, COMP.ID_SOCIETE, SOCIETE_RAISON, COMP.ID_POLE, POLE_NAME, 
							RESP.ID_PROFIL AS ID_PROFIL_RESPMANAGER, RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM, ROLE_NAME');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'COMP.ID_SOCIETE',
			$colPole = '',
			$colUser = ''
		));

		//Les ressources peuvent ne pas encore avoir de comptes utilisateurs
		$userJoin = in_array(BM::USER_TYPE_RESOURCE, $filter->userTypes->getValue()) ? 'LEFT' : 'INNER';

		$query->from('TAB_PROFIL COMP');
		$query->addJoin($userJoin.' JOIN TAB_USER US ON(COMP.ID_PROFIL=US.ID_PROFIL)
							LEFT JOIN TAB_SOCIETE USING(ID_SOCIETE)
							LEFT JOIN TAB_POLE USING(ID_POLE)
						LEFT JOIN TAB_ROLE ON(US.ID_ROLE = TAB_ROLE.ID_ROLE)
						LEFT JOIN TAB_USER RESPUS ON(RESPUS.ID_USER=COMP.ID_RESPMANAGER) 
							LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=RESPUS.ID_PROFIL)');
		$query->groupBy('US.ID_USER');

		$keywordsMapping = ['USER'=>'US.ID_USER', 'COMP'=>'COMP.ID_PROFIL'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if(!$whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('COMP.PROFIL_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('COMP.PROFIL_PRENOM LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_('COMP.PROFIL_TYPE<>?', Models\Candidate::TYPE_CANDIDATE)
			->and_($this->getFilterSearch(Tools::reverseMapData($filter->userTypes->getValue(), BM::USER_TYPE_MAPPING), 'US.USER_TYPE'))
			->and_($this->getFilterSearch($filter->resourceTypes->getValue(), 'COMP.PROFIL_TYPE'))
			->and_($this->getFilterSearch($filter->resourceStates->getValue(), 'COMP.PROFIL_ETAT'));

		$whereStates = new Where();
		foreach($filter->userSubscriptions->getValue() as $value) {
			switch($value) {
				case BM::USER_ACCESS_INACTIVE:
					$whereStates->or_('US.ID_USER IS NULL OR US.USER_ABONNEMENT=?', BM::DB_ACCESS_INACTIVE);
					break;
				case BM::USER_ACCESS_ACTIVE:
					$whereStates->or_('US.ID_USER IS NULL OR US.USER_ABONNEMENT=?', BM::DB_ACCESS_ACTIVE);
					break;
			}
		}
		$where->and_($whereStates);

		$whereTypes = new Where();
		foreach($filter->userTypes->getValue() as $value) {
			switch($value) {
				default:
					$whereTypes->or_('US.USER_TYPE=?', $value);
					break;
				case BM::USER_TYPE_RESOURCE:
					$whereTypes->or_('US.ID_USER IS NULL OR US.USER_TYPE=?', BM::DB_TYPE_RESOURCE);
					break;
			}
		}
		$where->and_($whereTypes);

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

		$query->addWhere($where);
		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchAccounts::ORDERBY_AGENCY_NAME           => 'SOCIETE_RAISON',
			SearchAccounts::ORDERBY_LOGIN              => 'US.USER_LOGIN',
			SearchAccounts::ORDERBY_STATE            => 'US.USER_ABONNEMENT',
			SearchAccounts::ORDERBY_RESOURCE_TYPEOF         => 'COMP.PROFIL_TYPE',
			SearchAccounts::ORDERBY_MAINMANAGER_LASTNAME => 'RESP.PROFIL_NOM',
			SearchAccounts::ORDERBY_ROLE_NAME		 => 'ROLE_NAME'
		];

		if(!$column) {
			$query->addOrderBy('COMP.PROFIL_NOM ASC');
			$query->addOrderBy('COMP.PROFIL_PRENOM ASC');
		}
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);
	}

	/**
	 * Retrieve account data from its login & password
	 * @param string $login
	 * @param string $pwd
	 * @return Model|false
	 */
	public function getAccountFromLoginAndPwd($login, $pwd) {
		$query = $this->getBasicAccountQuery();
		$query->addWhere('US.USER_ABONNEMENT='.BM::DB_ACCESS_ACTIVE.' AND US.USER_LOGIN=:login AND US.USER_PWD=:pwd', [':login' => $login, ':pwd' => $pwd]);
		return $this->singleExec($query);
	}

	/**
	 * Retrieve account data from its login
	 * @param  string  $login
	 * @return Model|false
	 */
	public function getAccountFromLogin($login) {
		$query = $this->getBasicAccountQuery();
		$query->addWhere('US.USER_ABONNEMENT='.BM::DB_ACCESS_ACTIVE.' AND US.USER_LOGIN=:login', [':login' => $login]);
		return $this->singleExec($query);
	}

	/**
	 * Retrieve account data from its ID
	 * @param  integer  $id
	 * @return Model|false
	 */
	public function getAccount($id) {
		$query = $this->getBasicAccountQuery();
		$query->addWhere('US.ID_USER=?', $id);
		$result = $this->singleExec($query);
		if($result) $result['CONFIGUSER'] = $this->getAccountSettingFromAccount($id);
		return $result;
	}

	/**
	 * Load an account from its resource id
	 * @param integer $id
	 * @return Model|false
	 */
	public function getAccountFromEmployee($id){
		$query = $this->getBasicAccountQuery();
		$query->addWhere('US.ID_PROFIL=?', $id);
		$result = $this->singleExec($query);
		if($result) $result['CONFIGUSER'] = $this->getAccountSettingFromAccount($id);
		return $result;
	}

	/**
	 * build a basic mysql query for account
	 * @return Query
	 */
	private function getBasicAccountQuery(){
		$query = new Query();
		$query->setColumns(['US.ID_USER', 'US.USER_LOGIN', 'US.USER_LOGIN', 'US.USER_CONNECTIONTYPE', 'US.USER_HOMEPAGE', 'US.USER_LANGUE', 'US.USER_DEFAULTSEARCH', 'US.USER_JOINCATEGORY', 'US.USER_ABONNEMENT',
							'US.USER_TYPE', 'US.USER_LASTCONNEXION', 'US.USER_SECURITYALERT', 'US.USER_SECURITYCOOKIE', 'US.USER_TAUXHORAIRE', 'US.USER_VALIDATIONTEMPS', 'US.USER_VALIDATIONFRAIS', 'US.USER_VALIDATIONABSENCES',
							'US.ID_PROFIL', 'TAB_PROFIL.ID_SOCIETE', 'TAB_PROFIL.ID_POLE', 'SOCIETE_RAISON', 'SOCIETE_GROUPE', 'SOCIETE_PAYS', 'ID_RESPMANAGER', 'ID_RESPRH', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_EMAIL', 'PROFIL_TEL1',
							'ID_PROFIL_RESPMANAGER' => 'RESPUS.ID_PROFIL'])
			->from('TAB_USER US')
			->addJoin('LEFT JOIN TAB_PROFIL USING(ID_PROFIL) LEFT JOIN TAB_SOCIETE USING(ID_SOCIETE) LEFT JOIN TAB_USER RESPUS ON(RESPUS.ID_USER=TAB_PROFIL.ID_RESPMANAGER)');
		return $query;
	}

	/**
	 * Retrieve login & password account data from its ID
	 * @param $id
	 * @return Model|false
	 */
	public function getLoginAndPwd($id) {
		$query = new Query();
		$query->setColumns(['ID_USER', 'USER_LOGIN', 'USER_PWD'])
			->from('TAB_USER');
		$query->addWhere('ID_USER=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * check the intranet is on for a account
	 * @param $id
	 * @return Model|false
	 */
	public function getIntranetAccess($id){
		return $this->singleExec('SELECT GRPCONF_INTRANET FROM TAB_USER
									INNER JOIN TAB_PROFIL USING(ID_PROFIL)
									INNER JOIN TAB_GROUPECONFIG USING(ID_SOCIETE)
									WHERE GRPCONF_INTRANET=1 && ID_USER=?', $id);
	}

	/**
	 * Create account data
	 * @param  array  $data  data to save
	 *
	 * The array can contains the following keys:
	 * - `USER` : an array with the account data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * - `PROFIL` : an array with the account profil data, cf. [TAB_PROFIL](../../bddclient/classes/TAB_PROFIL.html)
	 * - `CONFIGUSER` : an array with the account config data, cf. [TAB_USERCONFIG](../../bddclient/classes/TAB_USERCONFIG.html)
	 * @param  boolean  $solr
	 * @return integer User ID
	 */
	public function createAccount($data, $solr = true) {
		if(isset($data['USER'])) {
			$dbEmployee = new Employee();

			if(isset($data['PROFIL'])) {
				if(isset($data['USER']['ID_PROFIL']))
					$dbEmployee->updateEmployee(['PROFIL' => $data['PROFIL']], $data['USER']['ID_PROFIL'], 0, $solr);
				else
					$data['USER']['ID_PROFIL'] = $dbEmployee->createEmployee(['PROFIL' => isset($data['PROFIL']) ? $data['PROFIL'] : [], 'PROFILPARAM' => ['PARAM_TYPEDISPO' => BM::DISPO_ASAP, 'PARAM_DATEDISPO' => date('Y-m-d', time())]], false, $solr);
			}

			if(!isset($data['USER']['USER_TPSFRSSTART'])) $data['USER']['USER_TPSFRSSTART'] = null;
			if(!isset($data['USER']['USER_LASTCONNEXION'])) $data['USER']['USER_LASTCONNEXION'] = null;
			if(!isset($data['USER']['USER_LANGUE'])) $data['USER']['USER_LANGUE'] = BM::getLanguage();

			$id = $this->insert('TAB_USER', $data['USER']);
			if($id) {
				if (!isset($data['PROFIL']) && isset($data['USER']['ID_PROFIL']) && $solr)
					$dbEmployee->updateEmployee([], $data['USER']['ID_PROFIL'], 0, $solr);

				if (isset($data['CONFIGUSER']) && isset($data['USER']['USER_TYPE']) && $data['USER']['USER_TYPE'] != BM::DB_TYPE_RESOURCE) {
					$data['CONFIGUSER']['ID_USER'] = $id;
					$this->createAccountSetting($data['CONFIGUSER']);
				}

				if (isset($data['USER']['USER_LOGIN'])) {
					$dbLogin = new BoondManager\Login();
					$dbLogin->createLogin(['ID_USER' => $id, 'USER_LOGIN' => $data['USER']['USER_LOGIN'], 'CLIENT_WEB' => BM::getCustomerCode()]);
				}
			}
			return $id;
		}
		return false;
	}

	/**
	 * Create/Update account data
	 * @param  array  $data  data to save
	 *
	 * The array can contains the following keys:
	 * - `USER` : an array with the account data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * - `PROFIL` : an array with the account profil data, cf. [TAB_PROFIL](../../bddclient/classes/TAB_PROFIL.html)
	 * - `CONFIGUSER` : an array with the account config data, cf. [TAB_USERCONFIG](../../bddclient/classes/TAB_USERCONFIG.html)
	 *
	 * @param  integer  $id  account id to update, cf. [TAB_USER.ID_USER](../../bddclient/classes/TAB_USER.html#property_ID_USER).
	 * @param  boolean  $solr  if true, synchronize the data with SolR
	 * @return boolean
	 */
	public function updateAccount($data, $id, $solr = true) {
		if(isset($data['USER'])) {
			$dbEmployee = Employee::instance();

			if(isset($data['PROFIL']) && isset($data['USER']['ID_PROFIL']))
				$dbEmployee->updateEmployee(['PROFIL' => $data['PROFIL']], $data['USER']['ID_PROFIL'], 0, $solr);

			$this->update('TAB_USER', $data['USER'], 'ID_USER=:id', [':id'=>$id]);

			if (!isset($data['PROFIL']) && isset($data['USER']['ID_PROFIL']) && $solr)
				$dbEmployee->updateEmployee([], $data['USER']['ID_PROFIL'], 0, $solr);

			if (isset($data['CONFIGUSER']) && isset($data['USER']['USER_TYPE']) && $data['USER']['USER_TYPE'] != BM::DB_TYPE_RESOURCE) {
				if(isset($data['USER']['ID_USERCONFIG'])) {
					$this->updateAccountSetting($data['CONFIGUSER'], $data['USER']['ID_USERCONFIG']);
				} else {
					$data['CONFIGUSER']['ID_USER'] = $id;
					$this->createAccountSetting($data['CONFIGUSER']);
				}
			}

			if (isset($data['USER']['USER_LOGIN'])) {
				$dbLogin = BoondManager\Login::instance();
				$login = $dbLogin->getLogin($data['USER']['USER_LOGIN']);
				if($login)
					$dbLogin->updateLoginFromAccount(['USER_LOGIN' => $data['USER']['USER_LOGIN']], $id, BM::getCustomerCode());
				else
					$dbLogin->createLogin([
						'USER_LOGIN' => $data['USER']['USER_LOGIN'],
						'ID_USER' => $this->escape($id),
						'CLIENT_WEB' => BM::getCustomerCode()
					]);
			}
		}
		return true;
	}

	/**
	 * Delete an account
	 * @param  integer  $id account id, cf. [TAB_USER.ID_USER](../../bddclient/classes/TAB_USER.html#property_ID_USER).
	 * @return boolean true if the account has been successfully deleted
	 */
	public function deleteAccount($id) {
		$dbLogin = new Login();
		$dbLogin->deleteLoginFromAccount($id, BM::getCustomerCode());

		$this->deleteAccountSettingFromAccount($id);

		$this->delete('TAB_DEVICEALLOWED', 'ID_USER=?', $id);//On supprime tous les périphériques autorisés du user
		$this->delete('TAB_OBJECTIF', 'ID_USER=?', $id);//On supprime tous les objectifs du user
		$this->delete('TAB_INBU', 'ID_USER=?', $id);//On supprime le USER de toutes les BU où il est affecté
		$this->delete('TAB_SHAREGROUPE', 'ID_USER=?', $id);//On supprime tous les groupes prédéfinis appartenant à ce user

		$dbFlag = new Flag();
		$tabFlags = $dbFlag->getFlags($id);
		foreach($tabFlags as $flag) $dbFlag->deleteFlag($flag['ID_USERFLAG']);

		return $this->delete('TAB_USER', 'ID_USER=?', $id);
	}

	/**
	 * Retrieve all data from the client super-admin account
	 * @return Model|false
	 */
	public function getAdministrator() {
		return $this->singleExec('SELECT ID_USER, USER_LOGIN, USER_TYPE, USER_LANGUE, USER_ABONNEMENT, USER_PWD, USER_CONNECTIONTYPE, CONFIG_RESSOURCE, CONFIG_BESOIN, CONFIG_PROJET, CONFIG_CANDIDAT, CONFIG_CRM, CONFIG_TPSFRS, CONFIG_FACTURATION, CONFIG_PRODUIT, CONFIG_ACHAT FROM TAB_USER INNER JOIN TAB_USERCONFIG USING(ID_USER) WHERE USER_TYPE IN(?,?)', [BM::DB_TYPE_ADMINISTRATOR, BM::DB_TYPE_ROOT]);
	}

	/**
	 * Create the client super-admin account
	 * @param array $data
	 * @return Model|false
	 */
	public function createAdministrator($data) {
		return $this->insert('TAB_USER', $data);
	}

	/**
	 * Update the data of the client super-admin
	 *
	 * @param array $data
	 *
	 * The array can contains the following keys
	 * - `ROOT` : array with the user data, cf. [TAB_USER](../../bddclient/classes/TAB_USER.html)
	 * - `CONFIG` : array with the user config, cf. [TAB_USERCONFIG](../../bddclient/classes/TAB_USERCONFIG.html)
	 * @return bool
	 */
	public function updateAdministrator($data) {
		$root = $this->singleExec('SELECT ID_USER FROM TAB_USER WHERE USER_TYPE IN(?,?)', [BM::DB_TYPE_ADMINISTRATOR, BM::DB_TYPE_ROOT]);
		$societe = $this->singleExec('SELECT GRPCONF_WEB FROM TAB_GROUPECONFIG');
		if($root && $societe) {
			if(isset($data['ROOT'])) {
				$this->update('TAB_USER', $data['ROOT'], 'ID_USER=:id', array(':id' => $root['ID_USER']));
				if(isset($data['ROOT']['USER_LOGIN'])) {
					$dbLogin = new BoondManager\Login();
					$dbLogin->updateLoginFromAccount(['USER_LOGIN' => $data['ROOT']['USER_LOGIN']], $root['ID_USER'], $societe['GRPCONF_WEB']);
				}
			}

			if(isset($data['CONFIGROOT'])) {
				$this->updateAccountSettingFromAccount($data['CONFIGROOT'], $root['ID_USER']);

				//Update modules on all Managers
				$tabConfig = [];
				$tabConfig['CONFIG_RESSOURCE'] = (isset($data['CONFIGROOT']['CONFIG_RESSOURCE']))?'(CONFIG_RESSOURCE && '.$data['CONFIGROOT']['CONFIG_RESSOURCE'].')':0;
				$tabConfig['CONFIG_BESOIN'] = (isset($data['CONFIGROOT']['CONFIG_BESOIN']))?'(CONFIG_BESOIN && '.$data['CONFIGROOT']['CONFIG_BESOIN'].')':0;
				$tabConfig['CONFIG_CANDIDAT'] = (isset($data['CONFIGROOT']['CONFIG_CANDIDAT']))?'(CONFIG_CANDIDAT && '.$data['CONFIGROOT']['CONFIG_CANDIDAT'].')':0;
				$tabConfig['CONFIG_PROJET'] = (isset($data['CONFIGROOT']['CONFIG_PROJET']))?'(CONFIG_PROJET && '.$data['CONFIGROOT']['CONFIG_PROJET'].')':0;
				$tabConfig['CONFIG_CRM'] = (isset($data['CONFIGROOT']['CONFIG_CRM']))?'(CONFIG_CRM && '.$data['CONFIGROOT']['CONFIG_CRM'].')':0;
				$tabConfig['CONFIG_TPSFRS'] = (isset($data['CONFIGROOT']['CONFIG_TPSFRS']))?'(CONFIG_TPSFRS && '.$data['CONFIGROOT']['CONFIG_TPSFRS'].')':0;
				$tabConfig['CONFIG_FACTURATION'] = (isset($data['CONFIGROOT']['CONFIG_FACTURATION']))?'(CONFIG_FACTURATION && '.$data['CONFIGROOT']['CONFIG_FACTURATION'].')':0;
				$tabConfig['CONFIG_ACHAT'] = (isset($data['CONFIGROOT']['CONFIG_ACHAT']))?'(CONFIG_ACHAT && '.$data['CONFIGROOT']['CONFIG_ACHAT'].')':0;
				$tabConfig['CONFIG_PRODUIT'] = (isset($data['CONFIGROOT']['CONFIG_PRODUIT']))?'(CONFIG_PRODUIT && '.$data['CONFIGROOT']['CONFIG_PRODUIT'].')':0;
				foreach($this->exec('SELECT ID_USER FROM TAB_USER WHERE USER_TYPE='.BM::DB_TYPE_MANAGER) as $user)
					$this->update('TAB_USERCONFIG', $tabConfig, 'ID_USER=?', $user['ID_USER']);
			}
			return true;
		}
		return false;
	}

	/**
	 * Retrieve all config from the account
	 *
	 * @param  string  $id  account ID, cf. [TAB_USERCONFIG.ID_USER](../../bddclient/classes/TAB_USERCONFIG.html#property_ID_USER).
	 * @return \BoondManager\Models\UserConfig|false
	 */
	public function getAccountSettingFromAccount($id) {
		$sql = 'SELECT CONFIG_ADMINISTRATOR, CONFIG_RESSOURCE, CONFIG_BESOIN, CONFIG_PROJET, CONFIG_CANDIDAT, CONFIG_CRM, CONFIG_TPSFRS, CONFIG_FACTURATION, CONFIG_PRODUIT, CONFIG_ACHAT, CONFIG_REPORTING,
					CONFIG_EXTRACTIONRESSOURCES, CONFIG_EXTRACTIONBESOINS, CONFIG_EXTRACTIONCANDIDATS, CONFIG_EXTRACTIONPRODUITS,	CONFIG_EXTRACTIONCRM, CONFIG_EXTRACTIONACHATS, CONFIG_EXTRACTIONPROJETS, CONFIG_EXTRACTIONTPSFRS, CONFIG_EXTRACTIONBDC, CONFIG_EXTRACTIONREPORTING, CONFIG_EXTRACTIONACTIONS, CONFIG_EXPORTATIONACHATS, CONFIG_EXPORTATIONTPSFRS, CONFIG_EXPORTATIONBDC, CONFIG_EXPORTATIONCRM, CONFIG_APIS, CONFIG_APICALENDAR, CONFIG_APIMAIL, CONFIG_APIVIEWER, CONFIG_APIEMAILING, CONFIG_MENUBAR, CONFIG_DASHBOARD, CONFIG_SOCIETES, CONFIG_POLES,
					CONFIG_SHOWGROUPE, CONFIG_SHOWBUPOLE, CONFIG_SHOWALLMANAGERS, CONFIG_SHOWALLAGENCIES, CONFIG_SHOWALLPOLES, CONFIG_SHOWALLRESSOURCES, CONFIG_SHOWALLBESOINS, CONFIG_SHOWALLCANDIDATS, CONFIG_SHOWALLPRODUITS, CONFIG_SHOWALLCRM, CONFIG_SHOWALLACHATS, CONFIG_SHOWALLPROJETS, CONFIG_SHOWFACTURES, CONFIG_SHOWALLBDC, CONFIG_SHOWALLFLAGS,
					CONFIG_WRITEALLRESSOURCES, CONFIG_WRITEALLBESOINS, CONFIG_WRITEALLCANDIDATS, CONFIG_WRITEALLPRODUITS, CONFIG_WRITEALLCRM, CONFIG_WRITEALLACHATS, CONFIG_WRITEALLPROJETS, CONFIG_WRITEALLBDC, CONFIG_WRITEALLACTIONS,
					CONFIG_ALLOWCREERRESSOURCES, CONFIG_ALLOWCREERPRODUITS, CONFIG_ALLOWCREERCANDIDATS, CONFIG_ALLOWMASKEDDATABESOINS, CONFIG_ALLOWMODIFYCREATIONDATERESSOURCES, CONFIG_ALLOWMODIFYCREATIONDATEBESOINS, CONFIG_ALLOWMODIFYCREATIONDATECANDIDATS, CONFIG_ALLOWMODIFYCREATIONDATEPROJETS, CONFIG_ALLOWMODIFYCREATIONDATECRM, CONFIG_ALLOWMODIFYDATEBILLS, CONFIG_ALLOWMODIFYREFERENCEBDC,
					CONFIG_ALLOWAFFECTATIONCRM, CONFIG_ALLOWSUPPRESSIONCRM, CONFIG_ALLOWAFFECTATIONCANDIDATS, CONFIG_ALLOWSUPPRESSIONCANDIDATS, CONFIG_ALLOWACCESSACTIONSCANDIDATS, CONFIG_ALLOWAFFECTATIONRESSOURCES, CONFIG_ALLOWSUPPRESSIONRESSOURCES, CONFIG_ALLOWACCESSACTIONSRESSOURCES, CONFIG_ALLOWACCESSADMINRESSOURCES, CONFIG_ALLOWAFFECTATIONBESOINS, CONFIG_ALLOWSUPPRESSIONBESOINS,
					CONFIG_ALLOWAFFECTATIONACHATS, CONFIG_ALLOWSUPPRESSIONACHATS, CONFIG_ALLOWAFFECTATIONPRODUITS, CONFIG_ALLOWSUPPRESSIONPRODUITS, CONFIG_ALLOWAFFECTATIONTPSFRS, CONFIG_ALLOWAFFECTATIONBDC, CONFIG_ALLOWSUPPRESSIONBDC, CONFIG_ALLOWAFFECTATIONPROJETS, CONFIG_ALLOWSUPPRESSIONPROJETS, CONFIG_ALLOWACCESSADMINPROJETS, CONFIG_ALLOWSUPPRESSIONTPSFRS, CONFIG_ALLOWAFFECTATIONACTIONS, CONFIG_ALLOWSUPPRESSIONACTIONS,
					CONFIG_REPORTINGAC, CONFIG_REPORTINGRH, CONFIG_REPORTINGAR, CONFIG_REPORTINGTF, CONFIG_REPORTINGFN, CONFIG_REPORTINGAG, CONFIG_REPARTITION, CONFIG_REPORTINGPRJ, CONFIG_REPORTINGCMP
				FROM TAB_USERCONFIG WHERE ID_USER=?';
		return $this->singleExec($sql, $id);
	}

	/**
	 * Create the configuration of an account
	 *
	 * @param  array  $data
	 * @return integer account id
	 */
	public function createAccountSetting($data) {
		if(!isset($data['CONFIG_APIS'])) $data['CONFIG_APIS'] = '';
		if(!isset($data['CONFIG_MENUBAR'])) $data['CONFIG_MENUBAR'] = '';
		if(!isset($data['CONFIG_DASHBOARD'])) $data['CONFIG_DASHBOARD'] = '';
		if(!isset($data['CONFIG_SOCIETES'])) $data['CONFIG_SOCIETES'] = '';
		if(!isset($data['CONFIG_POLES'])) $data['CONFIG_POLES'] = '';
		if(!isset($data['CONFIG_REPORTINGAC'])) $data['CONFIG_REPORTINGAC'] = '';
		if(!isset($data['CONFIG_REPORTINGRH'])) $data['CONFIG_REPORTINGRH'] = '';
		if(!isset($data['CONFIG_REPORTINGAR'])) $data['CONFIG_REPORTINGAR'] = '';
		if(!isset($data['CONFIG_REPORTINGTF'])) $data['CONFIG_REPORTINGTF'] = '';
		if(!isset($data['CONFIG_REPORTINGFN'])) $data['CONFIG_REPORTINGFN'] = '';
		if(!isset($data['CONFIG_REPORTINGAG'])) $data['CONFIG_REPORTINGAG'] = '';
		if(!isset($data['CONFIG_REPARTITION'])) $data['CONFIG_REPARTITION'] = '';
		if(!isset($data['CONFIG_REPORTINGPRJ'])) $data['CONFIG_REPORTINGPRJ'] = '';
		if(!isset($data['CONFIG_REPORTINGCMP'])) $data['CONFIG_REPORTINGCMP'] = '';
		return $this->insert('TAB_USERCONFIG', $data);
	}

	/**
	 * Update the configuration of an account
	 * @param  array  $data
	 * @param  integer  $id
	 * @return bool
	 */
	public function updateAccountSetting($data, $id) {
		$this->update('TAB_USERCONFIG', $data, 'ID_USERCONFIG=:id', [':id' => $id]);
		return true;
	}

	/**
	 * Update the configuration of anaccount from account's id
	 * @param  array  $data
	 * @param  integer  $id
	 * @return bool
	 */
	public function updateAccountSettingFromAccount($data, $id) {
		$this->update('TAB_USERCONFIG', $data, 'ID_USER=:id', [':id' => $id]);
		return true;
	}

	/**
	 * Delete config for a given account
	 * @param  integer  $id
	 * @return int
	 */
	public function deleteAccountSettingFromAccount($id) {
		$this->delete('TAB_USERCONFIG', 'ID_USER=?', $id);
		return true;
	}
}
