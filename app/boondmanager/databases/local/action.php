<?php
/**
 * actions.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Databases\Local;

use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\APIs\Actions\Filters;
use BoondManager\Models;
use Wish\MySQL\Query;
use Wish\MySQL\Where;

/**
 * Handler for all actions db operations
 * @package BoondManager\Models\MySQL\Local
 */
class Action extends AbstractObject{

	/**#@+
	 * @var int DEFAULT_ACTION_TYPE
	 */
	const
		ACTION_TYPE_CANDIDATE_NOTE = 13,
		ACTION_TYPE_CRM_CONTACT_NOTE = 2,
		ACTION_TYPE_RESOURCE_NOTE = 4,
		ACTION_TYPE_OPPORTUNITY_NOTE = 50,
		ACTION_TYPE_PROJECT_NOTE = 30,
		ACTION_TYPE_ORDER_NOTE = 20,
		ACTION_TYPE_BILLING_NOTE = 70;
	/**#@-*/

	/**#@+
	 * @var int NOTIFICATION_TYPE
	 */
	const
		NOTIFICATION_TYPE_CANDIDATE = 100,
		NOTIFICATION_TYPE_CRM = 101,
		NOTIFICATION_TYPE_RESOURCE = 102,
		NOTIFICATION_TYPE_OPPORTUNITY = 103,
		NOTIFICATION_TYPE_PROJECT = 104,
		NOTIFICATION_TYPE_ORDER = 105,
		NOTIFICATION_TYPE_CRM_COMPANY = 106,
		NOTIFICATION_TYPE_PRODUCT = 107,
		NOTIFICATION_TYPE_PURCHASE = 108,
		NOTIFICATION_TYPE_APP = 109,
		NOTIFICATION_TYPE_BILLING = 110;
	/**#@-*/

	/**
	 * map a category ID with a list of columns to retrieve
	 * @param int $catID
	 * @return array|null
	 */
	private function mapCategoryIDToKeywordsSearch($catID){
		return Tools::mapData($catID, [
			BM::CATEGORY_CANDIDATE => ['ACT' => 'ID_ACTION', 'CAND'=>'COMP.ID_PROFIL'],
			BM::CATEGORY_CRM_CONTACT => ['ACT' => 'ID_ACTION', 'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'],
			BM::CATEGORY_RESOURCE => ['ACT' => 'ID_ACTION', 'COMP'=>'COMP.ID_PROFIL'],
			BM::CATEGORY_PROJECT => ['ACT' => 'ID_ACTION', 'PRJ'=>'TAB_PROJET.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'],
			BM::CATEGORY_OPPORTUNITY => ['ACT' => 'ID_ACTION', 'AO'=>'TAB_AO.ID_AO','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'],
			BM::CATEGORY_ORDER => ['ACT' => 'ID_ACTION', 'BDC'=>'TAB_BONDECOMMANDE.ID_BONDECOMMANDE','PRJ'=>'TAB_PROJET.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'],
			BM::CATEGORY_CRM_COMPANY => ['ACT' => 'ID_ACTION', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'],
			BM::CATEGORY_PRODUCT => ['ACT' => 'ID_ACTION', 'PROD'=>'TAB_PRODUIT.ID_PRODUIT'],
			BM::CATEGORY_PURCHASE => ['ACT' => 'ID_ACTION', 'ACH'=>'TAB_ACHAT.ID_ACHAT'],
			BM::CATEGORY_APP => ['ACT' => 'ID_ACTION', 'API'=>'TAB_INSTALLAPI.ID_MAINAPI'],
			BM::CATEGORY_BILLING => ['ACT' => 'ID_ACTION', 'FACT'=>'TAB_FACTURATION.ID_FACTURATION','BDC'=>'TAB_BONDECOMMANDE.ID_BONDECOMMANDE','PRJ'=>'TAB_PROJET.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'],
		]);
	}

	/**
	 * map a category ID with a WHERE clause
	 * @param int $catID
	 * @param string $keywords
	 * @return Where
	 */
	private function mapCategoryIDToKeywordsDefaultSearch($catID, $keywords){
		switch($catID) {
			case BM::CATEGORY_CANDIDATE:
			case BM::CATEGORY_RESOURCE:
				return new Where('(COMP.PROFIL_NOM LIKE ? OR COMP.PROFIL_PRENOM LIKE ?)',[$keywords.'%', $keywords.'%']);
			case BM::CATEGORY_CRM_CONTACT:
			case BM::CATEGORY_OPPORTUNITY:
				return new Where('(TAB_CRMCONTACT.CCON_NOM LIKE ? OR TAB_CRMCONTACT.CCON_PRENOM LIKE ? OR TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?)', [$keywords.'%', $keywords.'%', '%'.$keywords.'%']);
			case BM::CATEGORY_ORDER:
			case BM::CATEGORY_BILLING:
				return new Where('(TAB_CRMCONTACT.CCON_NOM LIKE ? OR TAB_CRMCONTACT.CCON_PRENOM LIKE ? OR TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ? OR TAB_PROJET.PRJ_REFERENCE LIKE ? OR TAB_BONDECOMMANDE.BDC_REF LIKE ?)', [$keywords.'%', $keywords.'%', '%'.$keywords.'%', $keywords.'%', $keywords.'%']);
			case BM::CATEGORY_PROJECT: return new Where('(TAB_CRMCONTACT.CCON_NOM LIKE ? OR TAB_CRMCONTACT.CCON_PRENOM LIKE ? OR TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ? OR TAB_PROJET.PRJ_REFERENCE LIKE ?)', [$keywords.'%', $keywords.'%', '%'.$keywords.'%', $keywords.'%']);
			case BM::CATEGORY_CRM_COMPANY: return new Where('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', ['%'.$keywords.'%']);
			case BM::CATEGORY_PRODUCT: return new Where('(PRODUIT_NOM LIKE ? OR PRODUIT_REF LIKE ?)', [$keywords.'%', $keywords.'%']);
			case BM::CATEGORY_PURCHASE: return new Where('ACHAT_TITLE LIKE ?', [$keywords.'%']);
			default: return new Where();
		}
	}

	/**
	 * Perform a search on $filter
	 * @param Filters\SearchActions $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchActions(Filters\SearchActions $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		/**
		 * TODO (de Louis pour Louis):
		 *
		 * - ajouter un index multi-colonnes "CREATE INDEX superIndex ON TAB_ACTION (ACTION_DATE, ID_ACTION, ACTION_TYPE)" (on peut éventuellement revoir l'ordre des colonnes, mais avec précautions);
		 * - faire la requete en 2 temps : récupérer les id_action qui matchent les filtres puis reconstruire la requete finale en ajoutant les X Ids séléctionnés dans la clause WHERE
		 *
		 */

		/**
		* an array with a query for each category
		* @var \Wish\MySQL\Query[]
		*/
		$catQueries = [];
		$argQueries = [];

		$keywords = $filter->keywords->getValue();
		$likeKeywords = str_replace('*', '%', str_replace('%', '\%', $keywords));

		$whereDate = (!$filter->period->isDefaultValue())
			? new Where('DATE(ACTION_DATE) BETWEEN ? AND ?', [$filter->startDate->value, $filter->endDate->value])
			: null;

		$totalRows = 0;

		//On construit les clauses wheres
		foreach($filter->getSelectedCategories() as $catID) {
			$query = new Query();
			$query->addColumns('ID_ACTION, TAB_ACTION.ID_PARENT, TAB_ACTION.ID_RESPUSER, ACTION_DATE, ACTION_TYPE, ACTION_TEXTE, ACTION_TITRE, ACTION_STATUT, ACTION_ETAT, ID_DOCUMENT,  COUNT(DISTINCT ID_DOCUMENT) AS NB_DOCUMENT');
			$query->addJoin('LEFT JOIN TAB_DOCUMENT ON TAB_DOCUMENT.ID_PARENT=ID_ACTION AND DOC_TYPE=6');

			$types = $filter->getActionTypesForCat($catID);

			if(!$types) continue;
			$query->addWhere($this->getFilterSearch($types, 'ACTION_TYPE'));
			$query->addWhere($whereDate);

			if($flags = $filter->flags->getValue()) {
				$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ACTION.' AND TAB_FLAG.ID_PARENT=ID_ACTION)');
				$query->addWhere('TAB_FLAG.ID_USERFLAG IN (?)', $flags);
			}

			if($keywords != '') {
				$searchIds = $this->getListIdSearch($keywords, $this->mapCategoryIDToKeywordsSearch($catID));
				$query->addWhere( $searchIds->isEmpty()
					? $this->mapCategoryIDToKeywordsDefaultSearch($catID, $likeKeywords)
					: $searchIds
				);
			}

			switch($catID) {
				case BM::CATEGORY_CANDIDATE:
					$query->addColumns([
						'RAPPEL_ID'=>'COMP.ID_PROFIL',
						'RAPPEL_EMAIL'=>'COMP.PROFIL_EMAIL',
						'RAPPEL_TEL1'=>'COMP.PROFIL_TEL1',
						'RAPPEL_TEL2'=>'COMP.PROFIL_TEL2',
						'RAPPEL_NOM'=>'COMP.PROFIL_NOM',
						'RAPPEL_PRENOM'=>'COMP.PROFIL_PRENOM',
						'RAPPEL_SOCIETE' => 'COMP.PROFIL_STATUT',
						'RAPPEL_TYPE' => 'COMP.PROFIL_TYPE',
						'RESP_NOM' => 'RESP.PROFIL_NOM',
						'RESP_PRENOM' => 'RESP.PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => '\'\'',
						'RAPPEL_IDPARENT' => '\'\'',
						'RESP_ID' => 'RESP.ID_PROFIL'
					]);
					$query->from('TAB_ACTION LEFT JOIN TAB_PROFIL COMP ON COMP.ID_PROFIL=TAB_ACTION.ID_PARENT LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER LEFT JOIN TAB_PROFIL RESP ON RESP.ID_PROFIL=TAB_USER.ID_PROFIL ');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'RESP.ID_SOCIETE',
						'RESP.ID_POLE',
						'RESP.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'COMP.PROFIL_ETAT'));

					//~ Show only if profile is visible
					//TODO utiliser le filtre profilVisible
					if($filter->visibleProfile->getValue() && !$this->always_access) $query->addWhere('COMP.PROFIL_VISIBILITE=1');

					break;
				case BM::CATEGORY_CRM_CONTACT:
					$query->addColumns([
						'RAPPEL_ID' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
						'RAPPEL_EMAIL' => 'CCON_EMAIL',
						'RAPPEL_TEL1' => 'CCON_TEL1',
						'RAPPEL_TEL2' => 'CCON_TEL2',
						'RAPPEL_NOM' => 'CCON_NOM',
						'RAPPEL_PRENOM' => 'CCON_PRENOM',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE',
						'RAPPEL_TYPE' => 'CCON_TYPE',
						'RESP_NOM' => 'PROFIL_NOM',
						'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => '\'\'',
						'RAPPEL_IDPARENT' => 'TAB_CRMCONTACT.ID_CRMSOCIETE',
						'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->from('TAB_ACTION
					LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=TAB_ACTION.ID_PARENT
					LEFT JOIN TAB_CRMSOCIETE ON TAB_CRMCONTACT.ID_CRMSOCIETE=TAB_CRMSOCIETE.ID_CRMSOCIETE
					LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER
					LEFT JOIN TAB_PROFIL ON TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'CCON_TYPE'));

					if(!$filter->origins->isDefaultValue())
						$query->addWhere('CCON_TYPESOURCE IN (?)', $filter->origins->value);

					break;
				case BM::CATEGORY_RESOURCE:
					$query->addColumns([
						'RAPPEL_ID' => 'COMP.ID_PROFIL',
						'RAPPEL_EMAIL' => 'COMP.PROFIL_EMAIL',
						'RAPPEL_TEL1' => 'COMP.PROFIL_TEL1',
						'RAPPEL_TEL2' => 'COMP.PROFIL_TEL2',
						'RAPPEL_NOM' => 'COMP.PROFIL_NOM',
						'RAPPEL_PRENOM' => 'COMP.PROFIL_PRENOM',
						'RAPPEL_SOCIETE' => 'COMP.PROFIL_STATUT',
						'RAPPEL_TYPE' => 'COMP.PROFIL_TYPE',
						'RESP_NOM' => 'RESP.PROFIL_NOM',
						'RESP_PRENOM' => 'RESP.PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => '\'\'',
						'RAPPEL_IDPARENT' => '\'\'',
						'RESP_ID' => 'RESP.ID_PROFIL'
					]);
					$query->from(' TAB_ACTION LEFT JOIN TAB_PROFIL COMP ON COMP.ID_PROFIL=TAB_ACTION.ID_PARENT LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER LEFT JOIN TAB_PROFIL RESP ON RESP.ID_PROFIL=TAB_USER.ID_PROFIL');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'RESP.ID_SOCIETE',
						'RESP.ID_POLE',
						'RESP.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'COMP.PROFIL_ETAT'));

					//TODO utiliser le filtre profilVisible
					if(!CurrentUser::instance()->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && !$this->always_access)
						$query->addWhere('COMP.PROFIL_VISIBILITE=1');

					break;
				case BM::CATEGORY_PROJECT:
					$query->addColumns([
						'RAPPEL_ID' => 'ID_PROJET',
						'RAPPEL_EMAIL' => 'CCON_EMAIL',
						'RAPPEL_TEL1' => 'CCON_TEL1',
						'RAPPEL_TEL2' => 'CCON_TEL2',
						'RAPPEL_NOM' => 'CCON_NOM',
						'RAPPEL_PRENOM' => 'CCON_PRENOM',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE',
						'RAPPEL_TYPE' => 'TAB_PROJET.PRJ_REFERENCE',
						'RESP_NOM' => 'PROFIL_NOM',
						'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => 'TAB_CRMCONTACT.ID_CRMSOCIETE',
						'RAPPEL_IDPARENT' => 'TAB_PROJET.ID_CRMCONTACT',
						'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->from(' TAB_ACTION
					LEFT JOIN TAB_PROJET ON TAB_PROJET.ID_PROJET=TAB_ACTION.ID_PARENT
					LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER
					LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
					LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT)
					LEFT JOIN TAB_CRMSOCIETE ON TAB_CRMCONTACT.ID_CRMSOCIETE=TAB_CRMSOCIETE.ID_CRMSOCIETE');

					//if (Wish_Session::getInstance()->get('login_usertype') == USER_CONSULTANT) {
					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					));
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'PRJ_ETAT'));

					break;
				case BM::CATEGORY_OPPORTUNITY:
					$query->addColumns([
						'RAPPEL_ID' => 'ID_AO',
						'RAPPEL_EMAIL' => 'CCON_EMAIL',
						'RAPPEL_TEL1' => 'CCON_TEL1',
						'RAPPEL_TEL2' => 'CCON_TEL2',
						'RAPPEL_NOM' => 'CCON_NOM',
						'RAPPEL_PRENOM' => 'CCON_PRENOM',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE',
						'RAPPEL_TYPE' => 'AO_TITLE',
						'RESP_NOM' => 'PROFIL_NOM',
						'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => 'TAB_CRMCONTACT.ID_CRMSOCIETE',
						'RAPPEL_IDPARENT' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
						'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->from('TAB_ACTION
						LEFT JOIN TAB_AO ON TAB_AO.ID_AO=TAB_ACTION.ID_PARENT
						LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER
						LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
						LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=TAB_AO.ID_CRMCONTACT
						LEFT JOIN TAB_CRMSOCIETE ON TAB_CRMSOCIETE.ID_CRMSOCIETE = TAB_CRMCONTACT.ID_CRMSOCIETE'
					);

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					));
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'AO_ETAT'));

					if(!$filter->origins->isDefaultValue())
						$query->addWhere( 'AO_TYPESOURCE IN (?)', $filter->origins->getValues());

					//TODO utiliser le filtre profilVisible
					if(CurrentUser::instance()->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) && !$this->always_access)
						$query->addWhere('AO_VISIBILITE=1');

					break;
				case BM::CATEGORY_ORDER:
					$query->addColumns([
						'RAPPEL_ID' => 'ID_BONDECOMMANDE',
						'RAPPEL_EMAIL' => 'CCON_EMAIL',
						'RAPPEL_TEL1' => 'CCON_TEL1',
						'RAPPEL_TEL2' => 'CCON_TEL2',
						'RAPPEL_NOM' => 'CCON_NOM',
						'RAPPEL_PRENOM' => 'CCON_PRENOM',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE',
						'RAPPEL_TYPE' => 'BDC_REFCLIENT',
						'RESP_NOM' => 'PROFIL_NOM',
						'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => 'TAB_CRMCONTACT.ID_CRMSOCIETE',
						'RAPPEL_IDPARENT' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
						'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->from(' TAB_ACTION
					LEFT JOIN TAB_BONDECOMMANDE ON TAB_BONDECOMMANDE.ID_BONDECOMMANDE=TAB_ACTION.ID_PARENT
					LEFT JOIN TAB_PROJET USING(ID_PROJET)
					LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER
					LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
					LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT
					LEFT JOIN TAB_CRMSOCIETE ON TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_CRMCONTACT.ID_CRMSOCIETE');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'BDC_ETAT'));

					break;
				case BM::CATEGORY_CRM_COMPANY:
					$query->addColumns([
						'RAPPEL_ID' => 'TAB_CRMSOCIETE.ID_CRMSOCIETE',
						'RAPPEL_EMAIL' => '\'\'',
						'RAPPEL_TEL1' => 'CSOC_TEL',
						'RAPPEL_TEL2' => '\'\'',
						'RAPPEL_NOM' => '\'\'',
						'RAPPEL_PRENOM' => '\'\'',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE',
						'RAPPEL_TYPE' => 'CSOC_TYPE',
						'RESP_NOM' => 'PROFIL_NOM',
						'RESP_PRENOM' => 'PROFIL_PRENOM' ,
						'RAPPEL_IDPARENT2' => '\'\'',
						'RAPPEL_IDPARENT' => '\'\'',
						'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->from(' TAB_ACTION LEFT JOIN TAB_CRMSOCIETE ON TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_ACTION.ID_PARENT
					LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER
					LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'CSOC_TYPE'));

					if(!$filter->origins->isDefaultValue())
						$query->addWhere( 'CSOC_TYPESOURCE IN (?)', $filter->origins->getValues());

					break;
				case BM::CATEGORY_PRODUCT:
					$query->addColumns([
						'RAPPEL_ID' => 'ID_PRODUIT', 'RAPPEL_EMAIL' => '\'\'', 'RAPPEL_TEL1' => '\'\'', 'RAPPEL_TEL2' => '\'\'',
						'RAPPEL_NOM' => 'PRODUIT_NOM', 'RAPPEL_PRENOM' => '\'\'', 'RAPPEL_SOCIETE' => '\'\'',
						'RAPPEL_TYPE' => 'PRODUIT_REF', 'RESP_NOM' => 'PROFIL_NOM', 'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => '\'\'', 'RAPPEL_IDPARENT' => '\'\'', 'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->from(' TAB_ACTION LEFT JOIN TAB_PRODUIT ON TAB_PRODUIT.ID_PRODUIT=TAB_ACTION.ID_PARENT LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'PRODUIT_TYPE'));

					break;
				case BM::CATEGORY_PURCHASE:
					$query->addColumns([
						'RAPPEL_ID' => 'ID_ACHAT', 'RAPPEL_EMAIL' => 'CCON_EMAIL', 'RAPPEL_TEL1' => 'CCON_TEL1',
						'RAPPEL_TEL2' => 'CCON_TEL2', 'RAPPEL_NOM' => 'CCON_NOM', 'RAPPEL_PRENOM' => 'CCON_PRENOM',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE', 'RAPPEL_TYPE' => 'ACHAT_TITLE',
						'RESP_NOM' => 'PROFIL_NOM', 'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => 'TAB_CRMCONTACT.ID_CRMSOCIETE', 'RAPPEL_IDPARENT' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
						'RESP_ID' => 'TAB_PROFIL.ID_PROFIL',
					]);
					$query->from(' TAB_ACTION
					LEFT JOIN TAB_ACHAT ON TAB_ACHAT.ID_ACHAT=TAB_ACTION.ID_PARENT
					LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER
					LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
					LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_ACHAT.ID_CRMCONTACT)
					LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_CRMCONTACT.ID_CRMSOCIETE)');

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'ACHAT_TYPE'));

					break;
				case BM::CATEGORY_APP:
					$query->from(' TAB_ACTION LEFT JOIN TAB_INSTALLAPI ON TAB_INSTALLAPI.ID_MAINAPI=TAB_ACTION.ID_PARENT LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)');
					$query->addColumns([
						'RAPPEL_ID' => 'ID_MAINAPI', 'RAPPEL_EMAIL' => '\'\'', 'RAPPEL_TEL1' => '\'\'', 'RAPPEL_TEL2' => '\'\'',
						'RAPPEL_NOM' => '\'\'', 'RAPPEL_PRENOM' => '\'\'', 'RAPPEL_SOCIETE' => '\'\'', 'RAPPEL_TYPE' => '\'\'',
						'RESP_NOM' => 'PROFIL_NOM', 'RESP_PRENOM' => 'PROFIL_PRENOM',
						'RAPPEL_IDPARENT2' => '\'\'', 'RAPPEL_IDPARENT' => '\'\'', 'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);
					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );

					if($filter->myApps->value)
						$query->addWhere('ID_MAINAPI IN (?)', CurrentUser::instance()->getAppsId());

					break;
				case BM::CATEGORY_BILLING:
					$query->from(' TAB_ACTION LEFT JOIN TAB_FACTURATION ON TAB_FACTURATION.ID_FACTURATION=TAB_ACTION.ID_PARENT LEFT JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE) LEFT JOIN TAB_PROJET USING(ID_PROJET) LEFT JOIN TAB_USER ON TAB_USER.ID_USER=TAB_ACTION.ID_RESPUSER LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL) LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_CRMCONTACT.ID_CRMSOCIETE)');
					$query->addColumns([
						'RAPPEL_ID' => 'ID_FACTURATION', 'RAPPEL_EMAIL' => 'CCON_EMAIL', 'RAPPEL_TEL1' => 'CCON_TEL1',
						'RAPPEL_TEL2' => 'CCON_TEL2', 'RAPPEL_NOM' => 'CCON_NOM', 'RAPPEL_PRENOM' => 'CCON_PRENOM',
						'RAPPEL_SOCIETE' => 'CSOC_SOCIETE', 'RAPPEL_TYPE' => 'FACT_REF', 'RESP_NOM' => 'PROFIL_NOM',
						'RESP_PRENOM' => 'PROFIL_PRENOM', 'RAPPEL_IDPARENT2' => '\'\'', 'RAPPEL_IDPARENT' => '\'\'', 'RESP_ID' => 'TAB_PROFIL.ID_PROFIL'
					]);

					$query->addWhere( $this->getPerimeterSearch(
						$filter->getSelectedPerimeter(),
						'TAB_PROFIL.ID_SOCIETE',
						'TAB_PROFIL.ID_POLE',
						'TAB_USER.ID_PROFIL'
					) );
					$query->addWhere( $this->getFilterSearch($filter->states->getValues($catID), 'FACT_ETAT'));

					break;
			}

			$query->groupBy('ID_ACTION');
			$totalRows += $this->singleExec($query->getCountQuery(), $query->getArgs())->NB_ROWS;

			$catQueries[] = $query->getQuery();
			if($query->getArgs()) $argQueries = array_merge($argQueries, $query->getArgs());
		}

		$order = $filter->order->getValue();
		$sort = $filter->sort->getValue();
		$order_expr = [];
		foreach($sort as $s)
			switch($s) {
				case Filters\SearchActions::ORDERBY_DATE: $order_expr[] = 'ACTION_DATE '.$order; break;
				case Filters\SearchActions::ORDERBY_TYPE: $order_expr[] = 'ACTION_TYPE '.$order; break;
				case Filters\SearchActions::ORDERBY_REFERENCE: $order_expr[] = 'RAPPEL_NOM '.$order; break;
				case Filters\SearchActions::ORDERBY_EMAIL: $order_expr[] = 'RAPPEL_EMAIL '.$order; break;
				case Filters\SearchActions::ORDERBY_ACTION: $order_expr[] = 'ACTION_TEXTE '.$order; break;
				case Filters\SearchActions::ORDERBY_RESP: $order_expr[] = 'RESP_NOM '.$order; break;
				default: $order_expr[] = 'ACTION_DATE ASC'; break;
			}
		$order_expr[] = 'ID_ACTION '.$order;
		$order_expr = implode(', ', $order_expr);

		if($catQueries) {
			$sql_union = implode(' UNION DISTINCT ', $catQueries).' ORDER BY '.$order_expr.' LIMIT '.Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()).', '.$filter->maxResults->getValue();
			$result = $this->exec($sql_union, $argQueries);
			$userApps = CurrentUser::instance()->getApps();
			foreach($result as $row){
				if($row['ACTION_TITRE'] && preg_match("/\[\[__API([0-9]{1,})__\]\]/", $row['ACTION_TITRE'], $match) ){
					$app = (array_key_exists($match[1], $userApps)) ? $userApps[$match[1]]->MAINAPI_NOM : 'APP'.$match[1]; //FIXME (devrait etre en camel case)
					$row['ACTION_TITRE'] = str_replace('[[__API'.$match[1].'__]]', $app, $row['ACTION_TITRE']);
				}
			}

			$searchResult = new SearchResult();
			$searchResult->total = $totalRows;
			$searchResult->rows = $result;

			return $searchResult;
		}else{
			$searchResult = new SearchResult();
			$searchResult->total = 0;
			$searchResult->rows = [];
			return $searchResult;
		}
	}

	/**
	 * Retrieve an action
	 * @param int $idaction
	 * @return \BoondManager\Models\Action|false
	 * @throws \Exception
	 */
	function getObject($idaction)
	{
		$sql = 'SELECT ID_ACTION, ACTION_CREATEDAT, ACTION_TEXTE, ACTION_DATE, ACTION_TYPE,  TAB_ACTION.ID_PARENT, 
		               TAB_ACTION.ID_OTHER, ID_RESPUSER , TAB_USER.ID_PROFIL AS ID_PROFIL_RESPUSER
		        FROM TAB_ACTION 
		        LEFT JOIN TAB_USER ON TAB_USER.ID_USER = ID_RESPUSER
		        WHERE ID_ACTION=?';

		$result = $this->singleExec($sql, $idaction);
		if($result) {
			//On récupère tous les documents
			$result['DOCUMENTS'] = $this->exec('SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=6', $result->ID_ACTION);
		}
		return $result;
	}

	/**
	 * \brief Met à jour une action
	 * @param <type> $action tableau de données
	 * @param <type> $idaction identifiant de l'action
	 * @return <type> true si succes
	 */
	/**
	 * Update an action
	 * @param array $action
	 * @param int $idaction
	 * @return int number of row modified
	 * @throws \Exception
	 */
	function updateActionData($action, $idaction)
	{
		return $this->update('TAB_ACTION', $action, 'ID_ACTION=:id',['id'=>$idaction]);
	}

	/**
	 * Create an action
	 * @param array $action
	 * @return int action ID
	 */
	function newActionData($action) {
		return $this->insert('TAB_ACTION', $action);
	}

	/**
	 * Delete an action
	 * @param int $id action ID
	 * @return int number of row deleted
	 */
	function deleteActionData($id) {
		//On supprime tous les documents de cette action
		$fileData = new File();
		$fileData->deleteAllObjects(File::TYPE_DOCUMENT, $id, Models\Document::TYPE_ACTION);

		//On supprime tous les flags de cette action
		$flagsData = new AttachedFlag();
		$flagsData->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_ACTION);

		return $this->delete('TAB_ACTION', 'ID_ACTION=?',$id);
	}

	/**
	 * \brief Retourne une des conditions WHERE de recherche des actions
	 * @param <type> $array_actions tableau de type d'actions
	 * @param <type> $getall true = traite toutes les actions (même les actions non définies)
	 * @param <type> $exception tableau des actions à ne pas traiter
	 * @return <type> chaine WHERE
	 */
	function setSearchActions($array_actions, $getall=false, $exception=array())
	{
		die('do migration'); // TODO : on devrait retourner un objet Where
		$search_actions = array();
		foreach($array_actions as $id => $action) {if( ($action != '' || $getall == true) && !in_array($id, $exception)) $search_actions[] = $id;}
		return 'ACTION_TYPE IN('.implode(',', $search_actions).')';
	}

	function updateParentDateMAJ($actionType, $idParent) {
		die('do migration'); //TODO
		switch($actionType) {
			case ACTION_CANDIDAT:
				$candidatBDD = new BoondManager_ObjectBDD_Candidat($this->db);
				$candidatBDD->updateCandidatData(array(), $idParent);
				break;
			case ACTION_CRM:
				$crmBDD = new BoondManager_ObjectBDD_CRM($this->db);
				$crmBDD->updateCRMData(array('CONTACT' => array()), $idParent);
				break;
			case ACTION_PROJET:case GESTION_PROJET:
			$projetBDD = new BoondManager_ObjectBDD_Projet($this->db);
			$projetBDD->updateProjetData(array('PROJET' => array()), $idParent);
			break;
			case ACTION_CONSULTANT:
				$ressourceBDD = new BoondManager_ObjectBDD_Ressource($this->db);
				$ressourceBDD->updateRessourceData(array(), $idParent);
				break;
		}
	}
}
