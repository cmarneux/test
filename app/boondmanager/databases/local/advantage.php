<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Advantages\Filters\SearchAdvantages;
use Wish\MySQL\Query;
use Wish\MySQL\Where;

class Advantage extends AbstractObject{

	/**
	 * Delete an advantage
	 * @param int $id advantage id
	 * @return boolean
	 */
	public function deleteAdvantage($id){
		return $this->delete('TAB_AVANTAGE', 'ID_AVANTAGE=?',$id)==1;
	}

	/**
	* retrieve an advantage
	* @param int $id advantage id
	* @return \Wish\Models\Model
	*/
	public function getAdvantageData($id){
		$sql = 'SELECT TAB_PROFIL.ID_PROFIL, TAB_AVANTAGE.ID_SOCIETE, TAB_AVANTAGE.ID_PROJET, TAB_AVANTAGE.ID_MISSIONPROJET, 
					   TAB_AVANTAGE.ID_CONTRAT, TAB_AVANTAGE.ID_AVANTAGE, AVG_TYPEAREF, AVG_COUT, AVG_TYPE, AVG_CATEGORIE, 
					   AVG_DATE, AVG_FIN, AVG_QUOTAPARTICIPATION, AVG_QUOTARESSOURCE, AVG_QUOTASOCIETE, AVG_MONTANTPARTICIPATION, 
					   AVG_MONTANTRESSOURCE, AVG_MONTANTSOCIETE, AVG_QUANTITE, AVG_DEVISE, AVG_DEVISEAGENCE, AVG_CHANGE, AVG_CHANGEAGENCE, 
					   AVG_COMMENTAIRES, TYPEA_NAME, PRJ_REFERENCE, PROFIL_NOM, PROFIL_PRENOM, PROFIL_VISIBILITE, PROFIL_TYPE, 
					   ID_RESPMANAGER, UMANAGER.ID_PROFIL AS ID_PROFIL_MANAGER, ID_RESPRH, URH.ID_PROFIL AS ID_PROFIL_RH, 
					   TAB_PROFIL.ID_SOCIETE AS COMP_IDSOCIETE, TAB_USER.USER_TYPE, TAB_USER.ID_USER, TAB_PROFIL.ID_POLE
				FROM TAB_AVANTAGE 
				INNER JOIN TAB_PROFIL ON TAB_AVANTAGE.ID_PROFIL = TAB_PROFIL.ID_PROFIL
				LEFT JOIN TAB_USER ON TAB_USER.ID_PROFIL = TAB_PROFIL.ID_PROFIL 
				LEFT JOIN TAB_USER AS URH ON URH.ID_USER = TAB_PROFIL.ID_RESPRH 
				LEFT JOIN TAB_USER AS UMANAGER ON UMANAGER.ID_USER = TAB_PROFIL.ID_RESPMANAGER 
				LEFT JOIN TAB_TYPEAVANTAGE ON(TYPEA_REF=AVG_TYPEAREF AND TAB_TYPEAVANTAGE.ID_SOCIETE=TAB_AVANTAGE.ID_SOCIETE) 
				LEFT JOIN TAB_PROJET USING(ID_PROJET)
				WHERE ID_AVANTAGE=?';
		return $this->singleExec($sql, $id);
	}

	/**
	 * update an avantage
	 * @param int $id advantage id
	 * @param array $data data to update
	 * @return bool
	 * @throws \Exception
	 */
	public function updateAdvantage($id, $data){
		$this->update('TAB_AVANTAGE', $data, 'ID_AVANTAGE=:id', ['id'=>$id]);
		return true;
	}

	/**
	 * create a new advantage
	 * @param $data
	 * @return mixed
	 */
	public function create($data) {
		return $this->insert('TAB_AVANTAGE', $data);
	}

	/**
	 * perform a search from a given filter
	 * @param SearchAdvantages $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	public function search(SearchAdvantages $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->select('TAB_AVANTAGE.ID_PROFIL, TAB_AVANTAGE.ID_SOCIETE, TAB_SOCIETE.SOCIETE_RAISON, TAB_AVANTAGE.ID_PROJET, TAB_AVANTAGE.ID_MISSIONPROJET,
			TAB_AVANTAGE.ID_CONTRAT, PRJ_REFERENCE, PROFIL_NOM, PROFIL_PRENOM, PROFIL_VISIBILITE, PROFIL_REFERENCE, PROFIL_STATUT, PROFIL_TYPE,
			TAB_PROFIL.ID_RESPMANAGER, TAB_PROFIL.ID_RESPRH, USRESP.ID_PROFIL AS ID_PROFIL_RESPMANAGER, USRH.ID_PROFIL AS ID_PROFIL_RESPRH, TAB_PROFIL.ID_SOCIETE AS COMP_IDSOCIETE, TAB_PROFIL.ID_POLE AS COMP_IDPOLE, TAB_AVANTAGE.ID_AVANTAGE,
			AVG_TYPEAREF, AVG_TYPE, AVG_CATEGORIE, AVG_DATE, AVG_FIN, AVG_QUOTAPARTICIPATION, AVG_QUOTARESSOURCE, AVG_QUOTASOCIETE, 
			AVG_MONTANTPARTICIPATION, AVG_MONTANTRESSOURCE, AVG_MONTANTSOCIETE,
			AVG_QUANTITE, AVG_COUT, AVG_DEVISE, AVG_DEVISEAGENCE, AVG_CHANGE, AVG_CHANGEAGENCE, AVG_COMMENTAIRES, TYPEA_NAME'
		);
		$query->from('TAB_AVANTAGE');
		$query->addJoin('LEFT JOIN TAB_SOCIETE USING(ID_SOCIETE)
			LEFT JOIN TAB_TYPEAVANTAGE ON(TYPEA_REF=AVG_TYPEAREF AND TAB_TYPEAVANTAGE.ID_SOCIETE=TAB_AVANTAGE.ID_SOCIETE)
			LEFT JOIN TAB_PROJET USING(ID_PROJET)
			INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_AVANTAGE.ID_PROFIL)
			LEFT JOIN TAB_USER USRESP ON(USRESP.ID_USER=TAB_PROFIL.ID_RESPMANAGER)
			LEFT JOIN TAB_USER USRH ON(USRH.ID_USER=TAB_PROFIL.ID_RESPRH)');
		$query->groupBy('TAB_AVANTAGE.ID_AVANTAGE');

		$query->addWhere(
			$this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'TAB_AVANTAGE.ID_SOCIETE',
				'TAB_PROFIL.ID_POLE',
				['USRESP.ID_PROFIL','USRH.ID_PROFIL']
			)
		);

		//KEYWORDS
		if($keywords = $filter->keywords->getValue()) {
			$whereKeywords = $this->getListIdSearch($keywords, [
				'AVG'=>'TAB_AVANTAGE.ID_AVANTAGE',
				'COMP'=>'TAB_PROFIL.ID_PROFIL',
				'PRJ'=>'TAB_AVANTAGE.ID_PROJET',
				'CTR'=>'TAB_AVANTAGE.ID_CONTRAT',
				'MIS'=>'TAB_AVANTAGE.ID_MISSIONPROJET'
			]);
			if($whereKeywords->isEmpty()){
				$escapeKeywords = str_replace('*', '%', str_replace('%', '\%', $keywords));
				$whereKeywords = new Where('(PROFIL_NOM LIKE ? OR PROFIL_PRENOM LIKE ? OR PROFIL_REFERENCE LIKE ?)', [
					$escapeKeywords.'%',
					$escapeKeywords.'%',
					$escapeKeywords]
				);
			}
			$query->addWhere($whereKeywords);
		}

		$query->addWhere( $this->getFilterSearch($filter->resourceStates->getValue(), 'PROFIL_ETAT') );

		//TYPE D'AVANTAGE
		if($filter->advantageTypes->getValue()){
			$advantageWhere = new Where();
			foreach($filter->advantageTypes->getValue() as $type) {
				if($type != $filter->advantageTypes->getDefaultValue()){
					$tabType = explode(' ', $type);
					if(sizeof($tabType) ==2 )
						$advantageWhere->or_('AVG_TYPEAREF=? AND TAB_AVANTAGE.ID_SOCIETE=?', [$tabType[0], $tabType[1]]);
				}
			}
			$query->addWhere($advantageWhere);
		}

		//TYPE DE PROFIL
		$query->addWhere( $this->getFilterSearch($filter->resourceTypes->getValue(), 'PROFIL_TYPE') );

		if($filter->period->getValue() == SearchAdvantages::PERIOD_CREATED)
			$query->addWhere('AVG_DATE BETWEEN ? AND ?', [
				$filter->startDate->getValue(),
				$filter->endDate->getValue()
			]);

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());
		$query->setLimit(  $filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		return $this->launchSearch($query);
	}

	/**
	 * Add an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchAdvantages::ORDERBY_TYPEREFERENCE     => 'AVG_TYPEAREF',
			SearchAdvantages::ORDERBY_RESOURCE_FUNCTION => 'TAB_PROFIL.PROFIL_STATUT',
			SearchAdvantages::ORDERBY_RESOURCE_LASTNAME => 'TAB_PROFIL.PROFIL_NOM',
			SearchAdvantages::ORDERBY_CREATIONDATE      => 'AVG_DATE',
		];

		if(!$column) $query->addOrderBy( 'TAB_PROFIL.ID_PROFIL ASC' );
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy( $mapping[$c].' '.$order);

		$query->addOrderBy( 'TAB_AVANTAGE.ID_AVANTAGE DESC');
	}

	/**
	 * @param int $idAgence
	 * @return \BoondManager\Models\AdvantageType[]
	 */
	public function getAdvantagesTypes($idAgence){
		return $this->exec('SELECT ID_TYPEAVANTAGE, TYPEA_REF, TYPEA_NAME, TYPEA_TYPE, TYPEA_CATEGORIE FROM TAB_TYPEAVANTAGE WHERE ID_SOCIETE=?', $idAgence);
	}
}
