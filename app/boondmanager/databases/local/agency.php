<?php
/**
 * agency.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Local;

use BoondManager\Services\BM;
use Wish\Cache;
use Wish\Models\Model;
use Wish\MySQL\Query;
use Wish\Tools;
use BoondManager\Models;

/**
 * Class Agency
 * @namespace \BoondManager\Databases\Local
 */
class Agency extends AbstractObject {
    /**
     * Retrieve all config data for a specific agency
     *
     * @see self::exec_config_cmd()
     * @param  string  $id Agency idcf. [TAB_GROUPECONFIG.ID_SOCIETE](../../bddclient/classes/TAB_GROUPECONFIG.html#property_ID_SOCIETE).
     * @return Model|false
     */
    public function getLightAgency($id) {
		$query = $this->getBasicAgencyQuery();
		$query->addWhere('ID_SOCIETE=?', $id);
		return $this->singleExec($query);
    }

    /**
     * Retrieve all config data for a specific agency found with its connection URL
     *
     * @see self::exec_config_cmd()
     * @param  string  $customerCode  agency connection URL. cf. [TAB_GROUPECONFIG.GRPCONF_WEB](../../bddclient/classes/TAB_GROUPECONFIG.html#property_GRPCONF_WEB).
     * @return Model|false
     */
    public function getLightAgencyFromCustomerCode($customerCode) {
		$query = $this->getBasicAgencyQuery();
		$query->addWhere('GRPCONF_WEB=?', $customerCode);
		return $this->singleExec($query);
    }

    /**
     * Retrieve all config data from the first agency available
     *
     * @see self::exec_config_cmd()
     * @return Model|false
     */
    public function getFirstLightAgency() {
		$query = $this->getBasicAgencyQuery()->setLimit(1);
		return $this->singleExec($query);
    }

    /**
     * Build a basic mysql query for agency
     * @return Query
     */
    private function getBasicAgencyQuery() {
		$query = new Query();
		$query->setColumns([
				'ID_SOCIETE', 'SOCIETE_RAISON', 'SOCIETE_GROUPE', 'GRPCONF_CHANGE', 'GRPCONF_WEB', 'GRPCONF_INTRANET', 'GRPCONF_HASH',
				'GRPCONF_DOWNLOADCENTER', 'GRPCONF_DEVISE', 'GRPCONF_CHARGE', 'GRPCONF_NBJRSOUVRE',
				'GRPCONF_CALENDRIER', 'GRPCONF_SHOWDECOMPTE', 'GRPCONF_ALERTDECOMPTE', 'GRPCONF_TAUXHORAIRE',
				'GRPCONF_AUTOFACTURATION', 'GRPCONF_JDATERESSOURCEPLANNER', 'GRPCONF_BAREMESEXCEPTION'
			])
			->from('TAB_GROUPECONFIG')
			->addJoin('INNER JOIN TAB_SOCIETE USING(ID_SOCIETE)');
		return $query;
    }

    /**
     * Update all agencies with the given datas
     *
     * @param  array $data
     * @return boolean indicate if the config has been saved successfully
     */
    public function updateAllAgencies($data) {
        if($data) {
            $result = $this->exec('SELECT ID_SOCIETE FROM TAB_SOCIETE');
            foreach($result as $id)
                $this->update('TAB_GROUPECONFIG', $data, 'ID_SOCIETE=:id', [':id' => $id['ID_SOCIETE']]);
            return true;
        }
        return false;
    }
    /**
     * Retrieve an agency with its ID
     * @param  integer  $id
     * @param string $tab
     * @return Model|false
     */
    public function getAgency($id, $tab = BM::TAB_DEFAULT) {
		$baseQuery = new Query();
		$baseQuery->select(['TAB_SOCIETE.ID_SOCIETE', 'SOCIETE_RAISON'])
				->from('TAB_SOCIETE')
				->addJoin('INNER JOIN TAB_GROUPECONFIG USING(ID_SOCIETE)')
				->addWhere('TAB_SOCIETE.ID_SOCIETE=?', $id);

		switch ($tab) {
			default:
				$result = $this->singleExec($baseQuery);
				break;
			case Models\Agency::TAB_INFORMATION:
				$baseQuery->addColumns(['SOCIETE_EFFECTIF', 'SOCIETE_TVA', 'SOCIETE_SIREN', 'SOCIETE_NIC', 'SOCIETE_ADR', 'SOCIETE_CP', 'SOCIETE_VILLE', 'SOCIETE_PAYS', 'SOCIETE_EMAIL', 'SOCIETE_TEL', 'SOCIETE_RCS', 'SOCIETE_STATUT', 'SOCIETE_WEB', 'SOCIETE_NAF', 'SOCIETE_ETAT',
										'GRPCONF_DEVISE', 'GRPCONF_CHARGE', 'GRPCONF_NBJRSOUVRE', 'GRPCONF_CALENDRIER', 'GRPCONF_TAUXHORAIRE', 'GRPCONF_CHANGE']);
				$result = $this->singleExec($baseQuery);
				break;
			case Models\Agency::TAB_BILLING:
				$baseQuery->addColumns(['SOCIETE_NIC', 'SOCIETE_SIREN', 'GRPCONF_AUTOFACTURATION', 'GRPCONF_ETATBEFOREMAILFACTURATION', 'GRPCONF_ETATAFTERMAILFACTURATION', 'GRPCONF_DELTAFACTURATION', 'GRPCONF_ETATFIXEFACTURATION', 'GRPCONF_MASKREFFACTURATION',
										'GRPCONF_MAILFACTURATION', 'GRPCONF_BDCSHOWTVAIC', 'GRPCONF_BDCMENTIONS', 'GRPCONF_BDCCONDREGLEMENT', 'GRPCONF_BDCTAUXTVA', 'GRPCONF_BDCTYPEPAYMENT', 'GRPCONF_BDCSHOWINTNAME',
										'GRPCONF_BDCSHOWTARIFJRS', 'GRPCONF_BDCSHOWRIB', 'GRPCONF_BDCSHOWFOOTER', 'GRPCONF_BDCSHOWJRSOUVRES', 'GRPCONF_BDCSHOWFACTOR', 'GRPCONF_BDCSHOWNUMBER', 'GRPCONF_BDCSHOWPRJREFERENCE',
										'GRPCONF_BDCGROUPMISSION', 'GRPCONF_BDCCOPYCOMMENTS', 'GRPCONF_BDCSEPARATETPSFRS', 'GRPCONF_BDCIDRIB AS ID_RIB', 'GRPCONF_BDCIDCRMSOCIETE AS ID_FACTOR', 'CSOC_SOCIETE AS FACTOR_SOCIETE'])
						->addJoin('LEFT JOIN TAB_CRMSOCIETE ON(ID_CRMSOCIETE=GRPCONF_BDCIDCRMSOCIETE)');
				$result = $this->singleExec($baseQuery);
				if($result) $result['CONFIGFACTURATION'] = $this->getBillingConfig($id);
				break;
			case Models\Agency::TAB_OPPORTUNITIES:
				$baseQuery->addColumns(['GRPCONF_MASKREFAO', 'GRPCONF_MASKREFDEVIS', 'GRPCONF_DEVISSHOWNUMBER', 'GRPCONF_DEVISSHOWTVAIC', 'GRPCONF_DEVISSHOWAOREFERENCE', 'GRPCONF_DEVISSHOWFOOTER', 'GRPCONF_DEVISMENTIONS',
										'GRPCONF_DEVISCONDREGLEMENT', 'GRPCONF_DEVISTAUXTVA', 'GRPCONF_DEVISVALIDITE']);
				$result = $this->singleExec($baseQuery);
				break;
			case Models\Agency::TAB_PROJECTS:
				$baseQuery->addColumns(['GRPCONF_NBJRSOUVRE', 'GRPCONF_MASKREFPROJET', 'GRPCONF_TAUXREFACTURATION', 'GRPCONF_JDATERESSOURCEPLANNER', 'GRPCONF_PRJALLOWAVANTAGES', 'GRPCONF_PRJALLOWTEMPSEXCEPTION', 'GRPCONF_PRJALLOWJALONS']);
				$result = $this->singleExec($baseQuery);
				break;
			case Models\Agency::TAB_PURCHASES:
				$baseQuery->addColumns(['GRPCONF_MASKREFACHAT', 'GRPCONF_ACHATTAUXTVA', 'GRPCONF_ACHATCONDREGLEMENT', 'GRPCONF_ACHATTYPEPAYMENT']);
				$result = $this->singleExec($baseQuery);
				break;
			case Models\Agency::TAB_ACTIVITYEXPENSES:
                $baseQuery->addColumns([
                    'GRPCONF_TAUXHORAIRE', 'GRPCONF_VALIDATIONTEMPS', 'GRPCONF_VALIDATIONFRAIS', 'GRPCONF_VALIDATIONABSENCES',
                    'GRPCONF_MENTIONSTEMPS', 'GRPCONF_MENTIONSFRAIS', 'GRPCONF_MENTIONSABSENCES', 'GRPCONF_CALCULDECOMPTE',
                    'GRPCONF_ALERTDECOMPTE', 'GRPCONF_DEVISE', 'GRPCONF_CHANGE', 'GRPCONF_CALENDRIER', 'GRPCONF_DATEMAILTEMPS',
                    'GRPCONF_DATEMAILFRAIS', 'GRPCONF_STARTDECOMPTE', 'GRPCONF_SHOWDECOMPTE', 'GRPCONF_CTRAUTOFILLEXPENSES',
                    'GRPCONF_ALERTDECOMPTE'
                ]);
                $result = $this->singleExec($baseQuery);
                if ($result) $result['CONFIGTPSFRS'] = $this->getActivityExpensesConfig($id);
				break;
            case Models\Agency::TAB_RESOURCES:
                $baseQuery->addColumns(['GRPCONF_CTRDUREEHEBDOMADAIRE', 'GRPCONF_DEVISE', 'GRPCONF_CHANGE', 'GRPCONF_CHARGE', 'GRPCONF_PRJALLOWAVANTAGES', 'GRPCONF_CTRALLOWTEMPSEXCEPTION',
                    'GRPCONF_PRJALLOWTEMPSEXCEPTION', 'GRPCONF_BAREMESEXCEPTION', 'GRPCONF_AVANTAGESEXCEPTION']);
                $result = $this->singleExec($baseQuery);
                if ($result) $result['CONFIGRESSOURCES'] = $this->getEmployeesConfig($id);
				break;
		}
		return $result;
    }

    /**
     * Update data for a given Agency
	 * @param  integer  $id
     * @param  array $data data to updates
     *
     * This array can contains the following keys:
     * - `CONFIGTPSFRS`: an array with the agency's activity data
     * - `CONFIGFACTURATION`: an array with the agency's billing data
     * - `CONFIGRESSOURCES`: an array with the agency's resources data
     * - `GROUPECONFIG`: an array with the agency's special data, cf. [TAB_GROUPECONFIG](../../bddclient/classes/TAB_GROUPECONFIG.html)
     * - `SOCIETE`: an array with the agency data, cf. [TAB_SOCIETE](../../bddclient/classes/TAB_SOCIETE.html)
     *
     * @return bool
     */
    public function updateAgency($id, $data) {
		if(isset($data['GROUPECONFIG'])) $this->update('TAB_GROUPECONFIG', $data['GROUPECONFIG'], 'ID_SOCIETE=:id', array(':id' => $id));

		if(isset($data['SOCIETE'])) $this->update('TAB_SOCIETE', $data['SOCIETE'], 'ID_SOCIETE=:id', array(':id' => $id));

        if(isset($data['CONFIGTPSFRS']))
            $this->updateActivityExpensesConfig($id, $data['CONFIGTPSFRS']);

        if(isset($data['CONFIGFACTURATION']))
            $this->updateBillingConfig($id, $data['CONFIGFACTURATION']);

        if(isset($data['CONFIGRESSOURCES']))
            $this->updateEmployeesConfig($id, $data['CONFIGRESSOURCES']);
        return true;
    }

	/**
	 * Create data for a given Agency
	 * @param  array $data data to updates
	 *
	 * This array can contains the following keys:
	 * - `GROUPECONFIG`: an array with the user data, cf. [TAB_GROUPECONFIG](../../bddclient/classes/TAB_GROUPECONFIG.html)
	 * - `SOCIETE`: an array with the agency data, cf. [TAB_SOCIETE](../../bddclient/classes/TAB_SOCIETE.html)
	 *
	 * @return integer the agency id
	 */
	public function createAgency($data) {
		$id = $this->insert('TAB_SOCIETE', $data['SOCIETE']);
		if($id) {
			if (!isset($data['GROUPECONFIG'])) $data['GROUPECONFIG'] = array();
			if (!isset($data['GROUPECONFIG']['GRPCONF_MENTIONSTEMPS'])) $data['GROUPECONFIG']['GRPCONF_MENTIONSTEMPS'] = '';
			if (!isset($data['GROUPECONFIG']['GRPCONF_MENTIONSFRAIS'])) $data['GROUPECONFIG']['GRPCONF_MENTIONSFRAIS'] = '';
			if (!isset($data['GROUPECONFIG']['GRPCONF_MENTIONSABSENCES'])) $data['GROUPECONFIG']['GRPCONF_MENTIONSABSENCES'] = '';
			if (!isset($data['GROUPECONFIG']['GRPCONF_BDCMENTIONS'])) $data['GROUPECONFIG']['GRPCONF_BDCMENTIONS'] = '';
			if (!isset($data['GROUPECONFIG']['GRPCONF_DEVISMENTIONS'])) $data['GROUPECONFIG']['GRPCONF_DEVISMENTIONS'] = '';
			if (!isset($data['GROUPECONFIG']['GRPCONF_BAREMESEXCEPTION'])) $data['GROUPECONFIG']['GRPCONF_BAREMESEXCEPTION'] = '';
			if (!isset($data['GROUPECONFIG']['GRPCONF_AVANTAGESEXCEPTION'])) $data['GROUPECONFIG']['GRPCONF_AVANTAGESEXCEPTION'] = '';

			$data['GROUPECONFIG']['ID_SOCIETE'] = $id;
			$this->insert('TAB_GROUPECONFIG', $data['GROUPECONFIG']);
			return $id;
		}
		return false;
	}

    /**
     * Delete an agency
     * @param  integer $id
     * @return boolean true if the agency has been successfully deleted
     */
    public function deleteAgency($id) {
		//TODO : Delete logo
        //$webService = new \Wish\Soap\Client();
        //$webService->DeleteFile('www/images/_clients/invoices/'.($this->f3->exists('SESSION.user.company.web')?$this->f3->get('SESSION.user.company.web'):'').'_'.$id.'.gif');

        $this->delete('TAB_RIB', 'RIB_TYPE=0 AND ID_PARENT=?', $id);
        $this->delete('TAB_SOCIETE', 'ID_SOCIETE=?', $id);
        $this->delete('TAB_GROUPECONFIG', 'ID_SOCIETE=?', $id);
        $this->delete('TAB_TYPEAVANTAGE', 'ID_SOCIETE=?', $id);
        $this->delete('TAB_TYPEHEURE', 'ID_SOCIETE=?', $id);
        $this->delete('TAB_TYPEFRAIS', 'ID_SOCIETE=?', $id);
        $this->delete('TAB_TYPEDECOMPTE', 'ID_SOCIETE=?', $id);
        $this->delete('TAB_REGLEEXCEPTION', 'PARENT_TYPE=0 AND ID_PARENT=?', $id);
        return true;
    }
    /**
     * Check if the agency can be deleted
     * @param  integer  $id  Agency ID, cf. [TAB_SOCIETE.ID_SOCIETE](../../bddclient/classes/TAB_SOCIETE.html#property_ID_SOCIETE).
     * @return boolean
     */
    function isAgencyReducible($id) {
		$nbDocument = 0;
		$query = 'SELECT COUNT(ID_PROFIL) AS NB_DOCUMENT FROM TAB_PROFIL WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_AO) AS NB_DOCUMENT FROM TAB_AO WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_PROJET) AS NB_DOCUMENT FROM TAB_PROJET WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_PRODUIT) AS NB_DOCUMENT FROM TAB_PRODUIT WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_ACHAT) AS NB_DOCUMENT FROM TAB_ACHAT WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_LISTETEMPS) AS NB_DOCUMENT FROM TAB_LISTETEMPS WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_LISTEFRAIS) AS NB_DOCUMENT FROM TAB_LISTEFRAIS WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_LISTEABSENCES) AS NB_DOCUMENT FROM TAB_LISTEABSENCES WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_CONTRAT) AS NB_DOCUMENT FROM TAB_CONTRAT WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_AVANTAGE) AS NB_DOCUMENT FROM TAB_AVANTAGE WHERE ID_SOCIETE=:id
                UNION ALL
                SELECT COUNT(ID_BONDECOMMANDE) AS NB_DOCUMENT FROM TAB_BONDECOMMANDE INNER JOIN TAB_RIB USING(ID_RIB) WHERE RIB_TYPE=0 AND ID_PARENT=:id
                UNION ALL
                SELECT COUNT(ID_INTQUO) AS NB_DOCUMENT FROM TAB_INTQUOTA WHERE ID_SOCIETE=:id';
        foreach($this->exec($query, ['id'=>$id]) as $document) $nbDocument += $document['NB_DOCUMENT'];
        return ($nbDocument == 0);
    }

    /**
     * Retrieve all agencies
     * @param  boolean $fulldata if false, load only the ID and the name, otherwise, load all data
     * @return Model[]
     */
    function getAllAgencies($fulldata = false) {
        if($fulldata)
            return $this->exec('SELECT ID_SOCIETE, SOCIETE_RAISON, SOCIETE_ETAT, SOCIETE_EFFECTIF, SOCIETE_TVA, SOCIETE_SIREN, SOCIETE_NIC, SOCIETE_ADR, SOCIETE_CP, SOCIETE_VILLE, SOCIETE_PAYS, SOCIETE_EMAIL, SOCIETE_TEL, SOCIETE_RCS, SOCIETE_STATUT, SOCIETE_WEB, SOCIETE_NAF, GRPCONF_DEVISE, GRPCONF_CHARGE, GRPCONF_NBJRSOUVRE, GRPCONF_CALENDRIER, GRPCONF_TAUXHORAIRE, GRPCONF_CHANGE FROM TAB_SOCIETE INNER JOIN TAB_GROUPECONFIG USING(ID_SOCIETE)');
        else {
            $cache = Cache::instance();
            $key = 'agencies.list';
            if(!$cache->exists($key)) {
                $data = $this->exec('SELECT ID_SOCIETE, SOCIETE_RAISON FROM TAB_SOCIETE ORDER BY SOCIETE_RAISON ASC');
                $cache->set($key, $data);
            }
            return $cache->get($key);
        }
    }

	/**
	 * @return Model[]
	 */
	public function getAllBasicAgencies(){
		$cache = Cache::instance();
		$key = 'agencies.basic.list';
		if(!$cache->exists($key)) {
			$agencies    = $this->getAllAgencies();
			$mappedArray = Tools::useColumnAsKey('ID_SOCIETE', $agencies);
			$cache->set($key, $mappedArray);
		}
		return $cache->get($key);
	}

	/**
	 * return an agency with minimum info (id & name)
	 * @param $id
	 * @return Model|null
	 */
	public function getBasicAgency($id){
		$agencies = $this->getAllBasicAgencies();
		return Tools::mapData($id, $agencies);
	}

	/**
	 * Get agency's activity & expenses config
	 * @param int $id
	 * @return Model|false
	 */
	public function getActivityExpensesConfig($id) {
		$result['LISTE_TYPEFRAIS'] = $this->exec('SELECT ID_TYPEFRAIS, TYPEFRS_REF, TYPEFRS_NAME, TYPEFRS_TVA, TYPEFRS_ETAT FROM TAB_TYPEFRAIS WHERE ID_SOCIETE=? ORDER BY ID_TYPEFRAIS ASC', $id);
		$result['LISTE_TYPEHEURE'] = $this->exec('SELECT ID_TYPEHEURE, TYPEH_REF, TYPEH_NAME, TYPEH_TYPEACTIVITE, TYPEH_ETAT FROM TAB_TYPEHEURE WHERE ID_SOCIETE=? ORDER BY ID_TYPEHEURE ASC', $id);
		$result['LISTE_BAREMEKM'] = $this->exec('SELECT ID_BAREMEKM, BKM_REF, BKM_NAME, BKM_VALUE, BKM_ETAT FROM TAB_BAREMEKM WHERE ID_SOCIETE=? ORDER BY ID_BAREMEKM ASC', $id);
		$result['LISTE_TYPEDECOMPTE'] = $this->exec('SELECT ID_TYPEDECOMPTE, TYPED_TYPEHREF, ID_TYPEHEURE, TYPEH_REF, TYPED_STARTDECOMPTE, TYPED_QUOTA, TYPED_INCR, TYPED_REPORT, 
                                                             TYPED_USEBEINGACQUIRED, TYPEH_NAME 
                                                      FROM TAB_TYPEDECOMPTE 
                                                      LEFT JOIN TAB_TYPEHEURE ON(TYPED_TYPEHREF=TYPEH_REF AND TAB_TYPEHEURE.ID_SOCIETE=:id) 
                                                      WHERE TAB_TYPEDECOMPTE.ID_SOCIETE=:id ORDER BY ID_TYPEDECOMPTE ASC', ['id'=>$id]);

		return $result;
	}

	/**
	 * Update agency's activity & expenses config
     * @param  integer  $id
     * @param  array $data data to updates
	 * @return bool
	 */
	public function updateActivityExpensesConfig($id, $data) {
		if(isset($data['LISTE_TYPEHEURE'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_TYPEHEURE'], 'TAB_TYPEHEURE', 'ID_TYPEHEURE', ['ID_SOCIETE' => $id]);
		if(isset($data['LISTE_BAREMEKM'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_BAREMEKM'], 'TAB_BAREMEKM', 'ID_BAREMEKM', ['ID_SOCIETE' => $id]);
		if(isset($data['LISTE_TYPEFRAIS'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_TYPEFRAIS'], 'TAB_TYPEFRAIS', 'ID_TYPEFRAIS', ['ID_SOCIETE' => $id]);
		if(isset($data['LISTE_TYPEDECOMPTE'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_TYPEDECOMPTE'], 'TAB_TYPEDECOMPTE', 'ID_TYPEDECOMPTE', ['ID_SOCIETE' => $id]);
		return true;
	}

	/**
	 * Get agency's employees config
	 * @param int $id
	 * @return Model
	 */
	public function getEmployeesConfig($id) {
		$result['LISTE_TYPEAVANTAGE'] = $this->exec('SELECT ID_TYPEAVANTAGE, TYPEA_REF, TYPEA_NAME, TYPEA_TYPE, TYPEA_CATEGORIE, TYPEA_QUOTAPARTICIPATION, TYPEA_QUOTARESSOURCE, TYPEA_QUOTASOCIETE, TYPEA_ETAT, TYPEA_DEFAUT, ID_SOCIETE FROM TAB_TYPEAVANTAGE WHERE ID_SOCIETE=? ORDER BY ID_SOCIETE ASC, ID_TYPEAVANTAGE ASC', $id);
		$result['LISTE_REGLEEXCEPTION'] = $this->exec('SELECT ID_REGLEEXCEPTION, REGLE_REF, REGLE_BAREMEREF, REGLE_NAME, REGLE_TARIF, REGLE_COUT, REGLE_ETAT, ID_PARENT AS ID_SOCIETE FROM TAB_REGLEEXCEPTION WHERE ID_PARENT=?AND PARENT_TYPE=0 ORDER BY ID_PARENT ASC, ID_REGLEEXCEPTION ASC', $id);
		return $result;
	}

	/**
     * @param  integer  $id
     * @param  array $data data to updates
	 * @return bool
	 */
	public function updateEmployeesConfig($id, $data) {
		if(isset($data['LISTE_TYPEAVANTAGE'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_TYPEAVANTAGE'], 'TAB_TYPEAVANTAGE', 'ID_TYPEAVANTAGE', ['ID_SOCIETE' => $id]);
		if(isset($data['LISTE_REGLEEXCEPTION'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_REGLEEXCEPTION'], 'TAB_REGLEEXCEPTION', 'ID_REGLEEXCEPTION', ['ID_PARENT' => $id, 'PARENT_TYPE' => 0]);
		return true;
	}

	public function getBillingConfig($id) {
		$result['LISTE_TYPEFACTURATION'] = $this->exec('SELECT ID_TYPEFACTURATION, TYPEF_REF, TYPEF_NAME, TYPEF_CATEGORIES FROM TAB_TYPEFACTURATION WHERE ID_SOCIETE=? ORDER BY ID_TYPEFACTURATION ASC', $id);
		$result['LISTE_RIBS'] = $this->exec('SELECT ID_RIB, RIB_DESCRIPTION, RIB_IBAN, RIB_BIC FROM TAB_RIB WHERE RIB_TYPE=0 AND ID_PARENT=?', $id);
		return $result;
	}

	/**
	 * Met à jour les données du module de facturation
     * @param  integer  $id
     * @param  array $data data to updates
	 * @return bool
	 */
	public function updateBillingConfig($id, $data) {
		if(isset($data['LISTE_TYPEFACTURATION'])) $this->deleteAndCreateItemsFromEntity($data['LISTE_TYPEFACTURATION'], 'TAB_TYPEFACTURATION', 'ID_TYPEFACTURATION', ['ID_SOCIETE' => $id]);
		if(isset($data['LISTE_RIBS'])) {
			$newData = $this->deleteAndCreateItemsFromEntity($data['LISTE_RIBS'], 'TAB_RIB', 'ID_RIB', ['ID_PARENT' => $id, 'RIB_TYPE' => 0]);

			//If there is only ONE RIB we set it by default
			if(sizeof($newData) == 1) $this->update('TAB_GROUPECONFIG', ['GRPCONF_BDCIDRIB' => $newData[0]['ID_RIB']], 'ID_SOCIETE=:id', [':id' => $id]);
		}
		return true;
	}
}
