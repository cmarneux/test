<?php
/**
 * app.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\Services\Actions;
use BoondManager\Services\BM;
use Wish\Models\Model;
use Wish\MySQL\Where;
use Wish\Tools;
use Wish\MySQL\Query;
use BoondManager\Databases\BoondManager;

/**
 * Handler for all apps db operations
 * @package Apps
 * @namespace BoondManager\Databases\Local;
 */
class App extends AbstractObject {
	/**
	 * Retrieve installed apps
	 * @param  boolean   $fullInfos Indique si on récupère les informations complètes de chaque App :
	 * - `true` : On récupère les informations basiques + complètes (Identifiant, Expiration, Token, Nom, Description, ....)
	 * - `false` : On récupère uniquement les informations basiques (Identifiant, Expiration, Token, Date d'installation & Visibilité)
	 * @param array $tabIds Références des Apps à filtrer
	 * @return Model[]
	 */
	public function getInstalledApps($fullInfos = true, $tabIds = []) {
		$query = new Query();
		$query->groupBy('TAB_INSTALLAPI.ID_INSTALLAPI')
			->addColumns(['ID_MAINAPI', 'IAPI_DATEINSTALLATION', 'IAPI_EXPIRATION', 'IAPI_TOKEN', 'IAPI_VISIBILITY'])
			->from('TAB_INSTALLAPI');
		if($tabIds) $query->addWhere( $this->getListIdSearch(implode(' ', $tabIds), ['API' => 'ID_MAINAPI']) );

		$results = $this->launchSearch($query);
		if($results->rows) {
			if($fullInfos) {
				$dbApp = new BoondManager\App();
				if($apps = $dbApp->getSpecificMainApps($tabIds)) {
					$apps = Tools::useColumnAsKey('ID_MAINAPI', $apps);
					foreach($results->rows as $api)
						/**
						 * @var Model $api
						 */
						$api->mergeWith( $apps[$api['ID_MAINAPI']] );
				}
			}
		}
		return $results->rows;
	}

    /**
     * Create an app installed for a customer
     * @param array $data
     * the array should contains the following keys with the app data cf. [TAB_INSTALLAPI](../../bddclient/classes/TAB_INSTALLAPI.html)
     * - INSTALLAPI
     * @return integer App id
     */
    public function installApp($data) {
        if(isset($data['INSTALLAPI']))
            return $this->insert('INSTALLAPI', $data['INSTALLAPI']);
        return false;
    }

	/**
	 * Update an app installed for a customer
	 * @param array $data
	 * the array should contains the following keys with the app data cf. [TAB_INSTALLAPI](../../bddclient/classes/TAB_INSTALLAPI.html)
	 * - INSTALLAPI
	 *
	 * @param integer $id App id to update
	 * @return bool
	 */
	public function uninstallApp($data, $id) {
		if(isset($data['INSTALLAPI']))
			$this->update('INSTALLAPI', $data['INSTALLAPI'], 'ID_MAINAPI=:id', array(':id' => $id));
		return true;
	}

    /**
     * Retrieve all apps data from a token
     * @param  string  $token
     * @return Model|false
     */
    public function getAppFromToken($token) {
		$query = $this->getBasicAppQuery();
		$query->addWhere('IAPI_TOKEN=?', $token);
		return $this->singleExec($query);
    }

    /**
     * Retrieve all apps data from its ID
     * @param  integer  $id  App id
	 * @return Model|false
     */
    public function getApp($id) {
		$query = $this->getBasicAppQuery();
		$query->addWhere('ID_MAINAPI=?', $id);
		return $this->singleExec($query);
    }

    /**
     * Build a basic mysql query for app
     */
    private function getBasicAppQuery() {
		$query = new Query();
		$query->setColumns(['IAPI_TOKEN', 'IAPI_EXPIRATION', 'ID_MAINAPI', 'IAPI_DATEINSTALLATION', 'IAPI_VISIBILITY'])
			->from('TAB_INSTALLAPI');
		return $query;
    }

    /**
     * Delete an app installed for a customer
     * @param integer $id
     * @return boolean
     */
    public function deleteApp($id) {
		//Delete all actions
		$dict = Actions::getDictionaryForCategory(BM::CATEGORY_APP);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id'));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

        $this->delete('TAB_INSTALLAPI', 'ID_MAINAPI=?', $id);
        return true;
    }
}
