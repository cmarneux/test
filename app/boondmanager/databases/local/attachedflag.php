<?php
/**
 * attachedflag.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\Models\Model;
use BoondManager\Models;
use Wish\MySQL\Query;

/**
 * Handle the database work related to Attached Flags
 * @@package BoondManager\Databases\Local;
 */
class AttachedFlag extends AbstractObject {
	/**
	 * Create an attached flag on an entity
	 * @param integer $idParent parent ID
	 * @param integer $typeParent flag type, cf. [TAB_FLAG.FLAG_TYPE](../../bddclient/classes/TAB_FLAG.html#property_FLAG_TYPE).
	 * @param int $idFlag user's flag id
	 * @param bool $solr
	 * @return int
	 */
	public function createAttachedFlag($idParent, $typeParent, $idFlag, $solr = true) {
		$id = $this->insert('TAB_FLAG', array('FLAG_TYPE' => $typeParent, 'ID_PARENT' => $idParent, 'ID_USERFLAG' => $idFlag));
		if($id) {
			switch($typeParent) {
				case Models\AttachedFlag::TYPE_RESOURCE:
					$dbEmployee = new Employee();
					$dbEmployee->updateEmployee([], $idParent, 0, $solr);
					break;
				case Models\AttachedFlag::TYPE_CANDIDATE:
					$dbCandidate = new Candidate();
					$dbCandidate->updateCandidate([], $idParent, 0, $solr);
					break;
				case Models\AttachedFlag::TYPE_COMPANY:
					$dbCompany = new Company();
					$dbCompany->updateCompany([], $idParent, $solr);
					break;
				case Models\AttachedFlag::TYPE_CONTACT:
					$dbContact = new Contact();
					$dbContact->updateContact([], $idParent, $solr);
					break;
			}
			return $id;
		}
		return false;
	}


	/**
	 * Delete an attached flag of an entity
	 * @param integer $idParent parent ID
	 * @param integer $typeParent flag type, cf. [TAB_FLAG.FLAG_TYPE](../../bddclient/classes/TAB_FLAG.html#property_FLAG_TYPE).
	 * @param int $id flag id
	 * @param bool $solr
	 * @return int
	 */
	public function deleteAttachedFlag($idParent, $typeParent, $id, $solr = true) {
		$this->delete('TAB_FLAG', 'ID_FLAG=?', $id);
		switch($typeParent) {
			case Models\AttachedFlag::TYPE_RESOURCE:
				$dbEmployee = new Employee();
				$dbEmployee->updateEmployee([], $idParent, 0, $solr);
				break;
			case Models\AttachedFlag::TYPE_CANDIDATE:
				$dbCandidate = Candidate::instance();
				$dbCandidate->updateObject([], $idParent, 0, $solr);
				break;
			case Models\AttachedFlag::TYPE_COMPANY:
				$dbCompany = new Company();
				$dbCompany->updateCRMData([], $idParent, $solr);
				break;
			case Models\AttachedFlag::TYPE_CONTACT:
				$dbContact = new Contact();
				$dbContact->updateCRMData([], $idParent, $solr);
				break;
		}
		return true;
	}

	/**
	 * Delete all attached flags of an entity
	 * @param int $idEntity entity's id
	 * @param int $typeEntity entity's type
	 * @return bool
	 */
	public function removeAllAttachedFlagsFromEntity($idEntity, $typeEntity) {
		$this->delete('TAB_FLAG', 'ID_PARENT=? AND FLAG_TYPE=?', [$idEntity, $typeEntity]);
		return true;
	}

	/**
	 * Get a attached flag
	 * @param integer $id
	 * @return Model|false
	 */
	public function getAttachedFlag($id) {
		$query = $this->getBasicAttachedFlagQuery();
		$query->addWhere('ID_FLAG=?', $id);
		return $this->singleExec($query);
	}

	/**
	 * Get a attached flag
	 * @param integer $idParent parent ID
	 * @param integer $typeParent flag type, cf. [TAB_FLAG.FLAG_TYPE](../../bddclient/classes/TAB_FLAG.html#property_FLAG_TYPE).
	 * @param integer $idFlag
	 * @return Model|false
	 */
	public function getAttachedFlagFromParentAndFlag($idParent, $typeParent, $idFlag) {
		$query = $this->getBasicAttachedFlagQuery();
		$query->addWhere('ID_USERFLAG=? AND FLAG_TYPE=? AND ID_PARENT=?', [$idFlag, $typeParent, $idParent]);
		return $this->singleExec($query);
	}

	/**
	 * build a basic mysql query for flag
	 * @return Query
	 */
	private function getBasicAttachedFlagQuery(){
		$query = new Query();
		$query->setColumns(['ID_FLAG', 'FLAG_TYPE', 'ID_PARENT', 'ID_USERFLAG', 'FLAG_LIBELLE', 'TAB_PROFIL.ID_PROFIL', 'TAB_PROFIL.ID_SOCIETE', 'PROFIL_NOM', 'PROFIL_PRENOM'])
			->from('TAB_FLAG')
			->addJoin('INNER JOIN TAB_USERFLAG USING(ID_USERFLAG) INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL)');
		return $query;
	}

	/**
	 * Récupère tous les flags d'un objet pour une liste de flags utilisateurs donnés.
	 * @param integer $typeParent flag type, cf. [TAB_FLAG.FLAG_TYPE](../../bddclient/classes/TAB_FLAG.html#property_FLAG_TYPE).
	 * @param integer $idParent parent ID
	 * @param array $tabIDFlag all users flags, cf. [TAB_FLAG.ID_USERFLAG](../../bddclient/classes/TAB_FLAG.html#property_ID_USERFLAG).
	 * @return \Wish\Models\Model[]
	 */
	public function getAttachedFlagsFromParent($typeParent, $idParent, $tabIDFlag = array()) {
		$args = [1 => $typeParent, 2 => $idParent];
		foreach($tabIDFlag as $id) $args[] = $id;
		return $this->exec('SELECT ID_FLAG, ID_USERFLAG, ID_USER, FLAG_LIBELLE, FLAG_TYPE, ID_PARENT, TAB_PROFIL.ID_PROFIL, TAB_PROFIL.ID_SOCIETE, PROFIL_NOM, PROFIL_PRENOM FROM TAB_FLAG INNER JOIN TAB_USERFLAG USING(ID_USERFLAG) INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL) WHERE FLAG_TYPE=? AND ID_PARENT=?'.((sizeof($tabIDFlag) > 0)?' AND ID_USERFLAG IN('.rtrim(str_repeat('?,', sizeof($tabIDFlag)), ',').')':''), $args);
	}
}
