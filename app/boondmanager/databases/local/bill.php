<?php
/**
 * bill.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\Local;

use Wish\MySQL\Where;
use BoondManager\Services\BM;
use BoondManager\Services\Actions;

class Bill extends AbstractObject{

	/**
	 * \brief récupère les données d'une commande
	 * @param <type> $id identifiant de la commande
	 */
	public function getFactureData($id, $onglet=0, $activepage = 1, &$tabFilters = array(), $nbmaxresults = TABLEAU_NBRESULTATS_DEFAULT)
	{
		throw new \Exception('do migration'); //TODO
		$result = false;
		switch($onglet) {//On récupère + de détails suivant l'onglet à afficher
			case 0://Informations
				$sql = 'SELECT ID_FACTURATION, ECH_DESCRIPTION, ECH_DATE, TAB_FACTURATION.ID_ECHEANCIER, ID_PROJET, ID_RESPUSER, PRJ_TYPE, PRJ_TYPEREF, SOCIETE_GROUPE, PRJ_REFERENCE, SOCIETE_ADR, SOCIETE_VILLE, SOCIETE_RAISON, SOCIETE_CP, SOCIETE_PAYS, SOCIETE_EMAIL, SOCIETE_TEL, SOCIETE_TVA, SOCIETE_STATUT, SOCIETE_RCS, SOCIETE_SIREN, SOCIETE_NIC, SOCIETE_NAF,
                            SOC.CSOC_SOCIETE, TAB_PROJET.ID_CRMCONTACT, TAB_PROJET.ID_AO, TAB_PROJET.ID_SOCIETE, AO_TITLE, AO_REF, TAB_CRMCONTACT.ID_CRMSOCIETE, CCON_NOM, CCON_PRENOM, SOC.CSOC_TVA, SOC.CSOC_NUMERO, COORD_NOM, COORD_CONTACT, COORD_EMAIL, COORD_EMAIL2, COORD_EMAIL3, COORD_TEL, COORD_ADR1, COORD_ADR2, COORD_ADR3, COORD_CP, COORD_VILLE, COORD_PAYS, PRJ_DEBUT, PRJ_FIN, PRJ_DEVISE, PROFIL_EMAIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_TEL1,
                            BDC_REF, BDC_REFCLIENT, BDC_DATE, BDC_TYPEREGLEMENT, BDC_CONDREGLEMENT, BDC_TAUXTVA, BDC_LANGUE, BDC_COMMENTAIRE, TAB_BONDECOMMANDE.ID_BONDECOMMANDE, BDC_SHOWRIB, BDC_SHOWPRJREFERENCE, BDC_SHOWFOOTER, BDC_SHOWINTNAME, BDC_SHOWTARIFJRS, BDC_SHOWJRSOUVRES, TAB_BONDECOMMANDE.ID_CRMSOCIETE AS ID_FACTOR, BDC_SHOWFACTOR, BDC_SHOWNUMBER, BDC_SHOWTVAIC, BDC_MENTIONS,
                            FACT_AVOIR, FACT_REF, FACT_DATE, FACT_DATEREGLEMENTATTENDUE, FACT_DATEREGLEMENTRECUE, FACT_TAUXREMISE, FACT_COMMENTAIRE, FACT_DEVISEAGENCE, FACT_CHANGEAGENCE, FACT_DEVISE, FACT_CHANGE, FACT_ETAT, FACT_DEBUT, FACT_FIN, FACT_SHOWCOMMENTAIRE, FACT_CLOTURE,
                            RIB_BANQUE, RIB_GUICHET, RIB_COMPTE, RIB_CLE, RIB_DOMICILIATION, RIB_IBAN, RIB_BIC,
                            FACTOR.CSOC_SOCIETE AS FACTOR_SOCIETE, FACTOR.CSOC_SIREN AS FACTOR_SIREN, FACTOR.CSOC_NIC AS FACTOR_NIC, FACTOR.CSOC_STATUT AS FACTOR_STATUT, FACTOR.CSOC_RCS AS FACTOR_RCS, FACTOR.CSOC_TVA AS FACTOR_TVA, FACTOR.CSOC_ADR AS FACTOR_ADR, FACTOR.CSOC_CP AS FACTOR_CP, FACTOR.CSOC_VILLE AS FACTOR_VILLE, FACTOR.CSOC_PAYS AS FACTOR_PAYS, FACTOR.CSOC_NAF AS FACTOR_NAF
                        FROM TAB_FACTURATION INNER JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE) INNER JOIN TAB_PROJET USING(ID_PROJET) LEFT JOIN TAB_AO USING(ID_AO)
                            LEFT JOIN TAB_ECHEANCIER ON(TAB_ECHEANCIER.ID_ECHEANCIER=TAB_FACTURATION.ID_ECHEANCIER)
                            LEFT JOIN TAB_COORDONNEES ON(TAB_COORDONNEES.ID_COORDONNEES=TAB_BONDECOMMANDE.ID_COORDONNEES) LEFT JOIN TAB_RIB ON(TAB_RIB.ID_RIB=TAB_BONDECOMMANDE.ID_RIB)
                            LEFT JOIN TAB_CRMSOCIETE FACTOR ON(FACTOR.ID_CRMSOCIETE=TAB_BONDECOMMANDE.ID_CRMSOCIETE)
                            LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=TAB_BONDECOMMANDE.ID_RESPUSER) LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL) LEFT JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_PROJET.ID_SOCIETE)
                            LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT LEFT JOIN TAB_CRMSOCIETE SOC ON(SOC.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
                        WHERE ID_FACTURATION="'.$this->db->escape($id).'"';
				$result = $this->db->query_first($sql);
				if($result) {
					//On récupère tous les items
					$result['ITEMS'] = $this->db->fetch_all_array('SELECT ID_ITEMFACTURE, ITEM_DESCRIPTION, ITEM_MONTANTHT, ITEM_TAUXTVA, ITEM_QUANTITE, ITEM_TYPEF, TYPEF_NAME FROM TAB_ITEMFACTURE LEFT JOIN TAB_TYPEFACTURATION ON(TYPEF_REF=ITEM_TYPEF AND TAB_TYPEFACTURATION.ID_SOCIETE='.$result['ID_SOCIETE'].') WHERE ID_FACTURATION="'.$result['ID_FACTURATION'].'" ORDER BY ID_ITEMFACTURE ASC;');

					//On cherche toutes les missions/ventes/Achats corrélés à ce bon de commande
					$result['CORRELATIONS'] = $this->db->fetch_all_array('SELECT ID_CORRELATIONBONDECOMMANDE, ID_ITEM, CORBDC_TYPE FROM TAB_CORRELATIONBONDECOMMANDE WHERE ID_BONDECOMMANDE="'.$result['ID_BONDECOMMANDE'].'" ORDER BY ID_ITEM ASC;');
				}
				break;
			case 1://Actions
				$result = $this->db->query_first('SELECT ID_FACTURATION, ID_PROJET, ID_RESPUSER, TAB_PROJET.ID_AO, PRJ_TYPE, PRJ_TYPEREF, TAB_PROJET.ID_SOCIETE, ID_BONDECOMMANDE, FACT_REF FROM TAB_FACTURATION INNER JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE) INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE ID_FACTURATION="'.$this->db->escape($id).'";');
				if($result) {
					$bourseBDD = new BoondManager_ObjectBDD_Recherche_Actions($this->db);
					$filters_array = $bourseBDD->getDefaultFilters(false);
					$filters_array['perimetre'] = '';
					$filters_array['keywords'] = 'FACT'.$result['ID_FACTURATION'];
					$filters_array['order_colonne'] = 'date';
					$filters_array['order_type'] = 'desc';
					$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
					$filters_array['type_rappel'] = '10';//On recherche les actions des intervenants
					if(isset($tabFilters['mc']['liste_rappel']) && is_array($tabFilters['mc']['liste_rappel'])) $filters_array['mc']['liste_rappel'] = $tabFilters['mc']['liste_rappel']; else foreach(Wish_Session::getInstance()->get('login_clefactionsfacture') as $idaction => $action) $filters_array['mc']['liste_rappel'][] = $idaction;
					$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
					$result['NB_ACTIONS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
					$result['ACTIONS'] = ($search)?$bourseBDD->get('RESULTS'):array();

					//On initialise la navigation de la vue
					if($search) $bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-facture', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_FACTURATION'].'&onglet=1&'.setURLMultiCriteria('lr', $filters_array['mc']['liste_rappel']), 'actions_');
					$tabFilters = $filters_array;
				}
				break;
		}

		if($result) {
			$this->data = $result;
			return true;
		}
		return false;
	}

	/**
	 * \brief Création d'une nouvelle commande
	 * @param <type> $data tableau de données
	 */
	public function newFactureData($data)
	{
		throw new \Exception('do migration'); //TODO
		//On va poser un verrou sur cette table car
		$idfacture = $this->db->query_insert('FACTURATION', $data['FACTURE']);

		if($idfacture && isset($data['ITEMS']) && sizeof($data['ITEMS']) > 0) {
			foreach($data['ITEMS'] as $data_item) {
				$dataItem = array();
				$dataItem['ITEM_TYPEF'] = $data_item[1];
				$dataItem['ITEM_DESCRIPTION'] = $data_item[2];
				$dataItem['ITEM_MONTANTHT'] = $data_item[3];
				$dataItem['ITEM_QUANTITE'] = $data_item[4];
				$dataItem['ITEM_TAUXTVA'] = $data_item[5];
				$dataItem['ID_FACTURATION'] = $idfacture;
				$this->db->query_insert('ITEMFACTURE', $dataItem);
			}
		}
		return $idfacture;
	}

	/**
	 * \brief Met à jour les données d'une commande
	 * @param <type> $data tableau de données
	 * @param <type> $id identifiant de la commande
	 */
	public function updateFactureData($data, $id)
	{
		throw new \Exception('do migration'); //TODO
		if(isset($data['FACTURE'])) $this->db->query_update('FACTURATION', $data['FACTURE'], 'ID_FACTURATION="'.$this->db->escape($id).'"');
		if(isset($data['ITEMS'])) {
			//On supprime les items qui n'existent plus
			foreach($this->db->fetch_all_array('SELECT ID_ITEMFACTURE FROM TAB_ITEMFACTURE WHERE ID_FACTURATION="'.$this->db->escape($id).'";') as $item) {
				$etat = false;
				foreach($data['ITEMS'] as $data_item) if($data_item[0] == $item['ID_ITEMFACTURE']) {$etat = true;break;}
				if(!$etat) $this->db->query_delete('ITEMFACTURE', 'ID_ITEMFACTURE="'.$item['ID_ITEMFACTURE'].'"');
			}

			//On met à jour les items
			foreach($data['ITEMS'] as $data_item) {
				$dataItem = array();
				$dataItem['ITEM_TYPEF'] = $data_item[1];
				$dataItem['ITEM_DESCRIPTION'] = $data_item[2];
				$dataItem['ITEM_MONTANTHT'] = $data_item[3];
				$dataItem['ITEM_QUANTITE'] = $data_item[4];
				$dataItem['ITEM_TAUXTVA'] = $data_item[5];
				if($data_item[0] != 0)//Cet item existe déjà
					$this->db->query_update('ITEMFACTURE', $dataItem, 'ID_ITEMFACTURE="'.$data_item[0].'"');
				else {//Cet item est nouvelle
					$dataItem['ID_FACTURATION'] = $id;
					$this->db->query_insert('ITEMFACTURE', $dataItem);
				}
			}
		}
		return true;
	}

	/**
	 * \brief Récupère la liste des produits sur la période comprenant la date passée en paramètres
	 * @param <type> $idprojet identifiant du projet
	 * @param <type> $debut date de début de la période
	 * @param <type> $fin date de fin de la période
	 * @return <type> tableau des intervenants de la facture
	 */
	function getProduitsDataOnPeriod($idprojet, $idproduit, $debut, $fin, $tabCorrelations = array())
	{
		throw new \Exception('do migration'); //TODO
		if(sizeof($tabCorrelations) > 0) {
			$tabId = array();foreach($tabCorrelations as $correlation) if($correlation['CORBDC_TYPE'] == 0) $tabId[] = '"'.$this->db->escape($correlation['ID_ITEM']).'"';
			if(sizeof($tabId) > 0)
				$whereExpr = ' AND ID_MISSIONPROJET IN('.implode(',',$tabId).')';
			else {
				$this->data['LISTE_ABONNEMENTS'] = array();
				return false;
			}
		} else $whereExpr = '';

		$sql = 'SELECT MP_NOM, MP_TARIF, MP_DEBUT, MP_FIN, MP_NBJRSFACTURE
                FROM TAB_PROJET INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_PROJET=TAB_PROJET.ID_PROJET AND ITEM_TYPE=1)
                WHERE DATEDIFF(\''.$this->db->escape($fin).'\',MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,\''.$this->db->escape($debut).'\')>=0 AND TAB_PROJET.ID_PROJET="'.$this->db->escape($idprojet).'" AND TAB_PROJET.ID_PRODUIT="'.$this->db->escape($idproduit).'"'.$whereExpr.' GROUP BY ID_MISSIONPROJET;';
		$this->data['LISTE_ABONNEMENTS'] = $this->db->fetch_all_array($sql);
		return (sizeof($this->data['LISTE_ABONNEMENTS']) > 0)?true:false;
	}

	/**
	 * \brief Récupère la liste des intervenants ayant travaillés sur la période comprenant la date passée en paramètres
	 * @param <type> $idprojet identifiant du projet
	 * @param <type> $debut date de début de la période
	 * @param <type> $fin date de fin de la période
	 * @param <type> $tabCorrelations tableau contenant la liste des commandes/missions corrélés afin de ne traiter que celles-là
	 * @return <type> tableau des intervenants de la facture
	 */
	function getIntervenantsDataOnPeriod($idprojet, $debut, $fin, $tabCorrelations = array(), $withException = false)
	{
		throw new \Exception('do migration'); //TODO
		if(sizeof($tabCorrelations) > 0) {
			$tabId = array();
			foreach($tabCorrelations as $correlation) if($correlation['CORBDC_TYPE'] == 0) $tabId[] = '"'.$this->db->escape($correlation['ID_ITEM']).'"';
			if(sizeof($tabId) > 0)
				$whereExpr = ' AND TAB_MISSIONPROJET.ID_MISSIONPROJET IN('.implode(',',$tabId).')';
			else {
				$this->data['LISTE_TEMPSNORMAUX'] = array();
				$this->data['LISTE_TEMPSEXCEPTION'] = array();
				return false;
			}
		} else $whereExpr = '';

		$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_DEBUT, MP_FIN, SUM(TEMPS_DUREE) AS NB_JRSOUVRES, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, 0 AS TYPEH_TYPEACTIVITE
				FROM TAB_TEMPS INNER JOIN TAB_LIGNETEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
					INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
					INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
					LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
				WHERE TEMPS_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\' AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'"'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE
				 UNION ALL
				SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_DEBUT, MP_FIN, SUM(TEMPS_DUREE) AS NB_JRSOUVRES, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
				FROM TAB_TEMPS INNER JOIN TAB_LIGNETEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
					INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_LIGNETEMPS.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
					INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
					LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
				WHERE TEMPS_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\' AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'"'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE;';
		$this->data['LISTE_TEMPSNORMAUX'] = $this->db->fetch_all_array($sql);

		if($withException) {
			$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, SUM(TEMPSEXP_TARIF) AS MP_TARIF, TEMPSEXP_DEBUT, TEMPSEXP_FIN, SUM(IF(TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1,TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN))) AS TEMPS_DUREE, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
                    FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
                        INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
                        INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
                        LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
                    WHERE DATEDIFF(\''.$this->db->escape($fin).'\',TEMPSEXP_DEBUT)>=0 AND DATEDIFF(TEMPSEXP_FIN,\''.$this->db->escape($debut).'\')>=0 AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'"'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE
                     UNION ALL
                    SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, SUM(TEMPSEXP_ACHAT) AS MP_TARIF, TEMPSEXP_DEBUT, TEMPSEXP_FIN, SUM(IF(TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1,TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN))) AS TEMPS_DUREE, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
                    FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
                        INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
                        INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
                        LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
                    WHERE DATEDIFF(\''.$this->db->escape($fin).'\',TEMPSEXP_DEBUT)>=0 AND DATEDIFF(TEMPSEXP_FIN,\''.$this->db->escape($debut).'\')>=0 AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'"'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE;';
			$this->data['LISTE_TEMPSEXCEPTION'] = $this->db->fetch_all_array($sql);
		}

		return (sizeof($this->data['LISTE_TEMPSNORMAUX']) > 0 || ($withException && sizeof($this->data['LISTE_TEMPSEXCEPTION']) > 0))?true:false;
	}

	/**
	 * \brief Récupère la liste des frais à refacturer au client
	 * @param <type> $idprojet identifiant du projet
	 * @param <type> $debut date de début de la période
	 * @param <type> $fin date de fin de la période
	 * @return <type> tableau des frais à refacturer
	 */
	function getFraisRefactureOnPeriod($idprojet, $debut, $fin, $tabCorrelations = array())
	{
		throw new \Exception('do migration'); //TODO
		if(sizeof($tabCorrelations) > 0) {
			$tabId = array();foreach($tabCorrelations as $correlation) if($correlation['CORBDC_TYPE'] == 0) $tabId[] = '"'.$this->db->escape($correlation['ID_ITEM']).'"';
			if(sizeof($tabId) > 0)
				$whereExpr = ' AND TAB_MISSIONPROJET.ID_MISSIONPROJET IN('.implode(',',$tabId).')';
			else {
				$this->data['LISTE_FRAIS'] = array();
				return false;
			}
		} else $whereExpr = '';

		$sql = 'SELECT FRAISREEL_TYPEFRSREF, FRAISREEL_INTITULE, FRAISREEL_TVA, FRAISREEL_MONTANT, FRAISREEL_NUM, LISTEFRAIS_BKMVALUE, LISTEFRAIS_CHANGEAGENCE, TAB_LISTEFRAIS.ID_SOCIETE FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
                     INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_FRAISREEL.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTEFRAIS.ID_PROFIL AND ITEM_TYPE=0)
                WHERE FRAISREEL_REFACTURE=1 AND FRAISREEL_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\' AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'"'.$whereExpr.'
				 UNION ALL
				SELECT FRAISREEL_TYPEFRSREF, FRAISREEL_INTITULE, FRAISREEL_TVA, FRAISREEL_MONTANT, FRAISREEL_NUM, LISTEFRAIS_BKMVALUE, LISTEFRAIS_CHANGEAGENCE, TAB_LISTEFRAIS.ID_SOCIETE FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
					 INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_FRAISREEL.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTEFRAIS.ID_PROFIL AND ITEM_TYPE=0)
                WHERE FRAISREEL_REFACTURE=1 AND FRAISREEL_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\' AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'"'.$whereExpr.';';
		$this->data['LISTE_FRAIS'] = $this->db->fetch_all_array($sql);
		return (sizeof($this->data['LISTE_FRAIS']) > 0)?true:false;
	}

	/**
	 * \brief Récupère la liste des achats à refacturer au client
	 * @param <type> $idprojet identifiant du projet
	 * @param <type> $debut date de début de la période
	 * @param <type> $fin date de fin de la période
	 * @return <type> tableau des frais à refacturer
	 */
	function getAchatRefactureOnPeriod($idprojet, $debut, $fin, $tabCorrelations = array())
	{
		throw new \Exception('do migration'); //TODO
		if(sizeof($tabCorrelations) > 0) {
			$tabId = array();foreach($tabCorrelations as $correlation) if($correlation['CORBDC_TYPE'] == 1) $tabId[] = '"'.$this->db->escape($correlation['ID_ITEM']).'"';
			if(sizeof($tabId) > 0)
				$whereExpr = ' AND ID_ACHAT IN('.implode(',',$tabId).')';
			else {
				$this->data['LISTE_ACHATS'] = array();
				return false;
			}
		} else $whereExpr = '';

		$sql = 'SELECT ACHAT_TARIFFACTURE, ACHAT_TAUXTVA, ACHAT_TITLE, ACHAT_DEBUT, ACHAT_FIN, ACHAT_TYPE FROM TAB_ACHAT
                WHERE ACHAT_REFACTURE=1 AND ID_PROJET="'.$this->db->escape($idprojet).'" AND ((ACHAT_TYPE=0 AND ACHAT_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\') OR (ACHAT_TYPE<>0 AND DATEDIFF(\''.$this->db->escape($fin).'\',ACHAT_DEBUT)>=0 AND DATEDIFF(ACHAT_FIN,\''.$this->db->escape($debut).'\')>=0))'.$whereExpr;
		$this->data['LISTE_ACHATS'] = $this->db->fetch_all_array($sql);
		return (sizeof($this->data['LISTE_ACHATS']) > 0)?true:false;
	}

	/**
	 * \brief Récupère la liste des détails de mission à refacturer au client
	 * @param <type> $idprojet identifiant du projet
	 * @param <type> $debut date de début de la période
	 * @param <type> $fin date de fin de la période
	 * @return <type> tableau des détails à refacturer
	 */
	function getDetailsMissionRefactureOnPeriod($idprojet, $debut, $fin, $tabCorrelations = array())
	{
		throw new \Exception('do migration'); //TODO
		if(sizeof($tabCorrelations) > 0) {
			$tabId = array();foreach($tabCorrelations as $correlation) if($correlation['CORBDC_TYPE'] == 0) $tabId[] = '"'.$this->db->escape($correlation['ID_ITEM']).'"';
			if(sizeof($tabId) > 0)
				$whereExpr = ' AND TAB_MISSIONPROJET.ID_MISSIONPROJET IN('.implode(',',$tabId).')';
			else {
				$this->data['LISTE_DETAILS'] = array();
				return false;
			}
		} else $whereExpr = '';

		$sql = 'SELECT MISDETAILS_TITRE, MISDETAILS_TARIF FROM TAB_MISSIONDETAILS INNER JOIN TAB_PROJET ON(ID_PROJET=TAB_MISSIONDETAILS.ID_PARENT AND PARENT_TYPE=0)
	                WHERE TAB_MISSIONDETAILS.ID_ACHAT=0 AND MISDETAILS_DATE<>\'3000-01-01\'=1 AND MISDETAILS_ETAT=1 AND ID_PROJET="'.$this->db->escape($idprojet).'" AND MISDETAILS_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\' AND MISDETAILS_TARIF<>0
				UNION ALL
				SELECT MISDETAILS_TITRE, MISDETAILS_TARIF FROM TAB_MISSIONDETAILS INNER JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_MISSIONDETAILS.ID_PARENT AND PARENT_TYPE=1)
					WHERE TAB_MISSIONDETAILS.ID_ACHAT=0 AND MISDETAILS_DATE<>\'3000-01-01\'=1 AND MISDETAILS_ETAT=1 AND TAB_MISSIONPROJET.ID_PROJET="'.$this->db->escape($idprojet).'" AND MISDETAILS_DATE BETWEEN \''.$this->db->escape($debut).'\' AND \''.$this->db->escape($fin).'\' AND MISDETAILS_TARIF<>0'.$whereExpr;
		$this->data['LISTE_DETAILS'] = $this->db->fetch_all_array($sql);
		return (sizeof($this->data['LISTE_DETAILS']) > 0)?true:false;
	}

	/**
	 *\brief Dupliquer une facture
	 * @param <type> $data tableau des données de la facture à dupliquer
	 */
	function dupliquerFacture($dataFacture, BoondManager_Devise $modDevise)
	{
		throw new \Exception('do migration'); //TODO
		$newData['FACTURE']['FACT_AVOIR'] = $dataFacture['FACT_AVOIR'];
		$newData['FACTURE']['FACT_DATE'] = $dataFacture['FACT_DATE'];
		$newData['FACTURE']['FACT_DATEREGLEMENTATTENDUE'] = $dataFacture['FACT_DATEREGLEMENTATTENDUE'];
		$newData['FACTURE']['FACT_DATEREGLEMENTRECUE'] = $dataFacture['FACT_DATEREGLEMENTRECUE'];
		$newData['FACTURE']['FACT_TAUXREMISE'] = $dataFacture['FACT_TAUXREMISE'];
		$newData['FACTURE']['FACT_COMMENTAIRE'] = $dataFacture['FACT_COMMENTAIRE'];
		$newData['FACTURE']['FACT_ETAT'] = $dataFacture['FACT_ETAT'];
		$newData['FACTURE']['FACT_DEVISE'] = $dataFacture['FACT_DEVISE'];
		$newData['FACTURE']['FACT_CHANGE'] = $dataFacture['FACT_CHANGE'];
		$newData['FACTURE']['FACT_DEBUT'] = $dataFacture['FACT_DEBUT'];
		$newData['FACTURE']['FACT_FIN'] = $dataFacture['FACT_FIN'];
		$newData['FACTURE']['FACT_DEVISEAGENCE'] = $modDevise->getDeviseAgence();
		$newData['FACTURE']['FACT_CHANGEAGENCE'] = $modDevise->getTauxAgence();
		$newData['FACTURE']['ID_BONDECOMMANDE'] = $dataFacture['ID_BONDECOMMANDE'];

		if($newData['FACTURE']['FACT_ETAT'] != 10) {
			$this->lock(array('TAB_FACTURATION WRITE', 'TAB_ITEMFACTURE WRITE', 'TAB_FACTURATION AS FACT_READ READ'));    //On verrouille la table afin d'être sûr de construire une référence de facture unique
			$newData['FACTURE']['FACT_REF'] = $this->getNewReferenceFacture($dataFacture['GRPCONF_MASKREFFACTURATION'],false,$newData['FACTURE']['FACT_DATE']);
		}

		if(isset($dataFacture['ITEMS'])) {
			$newData['ITEMS'] = array();
			foreach($dataFacture['ITEMS'] as $item) {
				$modDevise->setDataAgence($dataFacture['FACT_DEVISEAGENCE'], $dataFacture['FACT_CHANGEAGENCE']);
				$lastMontant = $modDevise->getTarifProduit($item['ITEM_MONTANTHT'],$dataFacture['FACT_CHANGE']);

				$modDevise->setDataAgence(Wish_Session::getInstance()->get('login_valuedevise'), Wish_Session::getInstance()->get('login_txchange'));
				$newData['ITEMS'][] = array(0, $item['ITEM_TYPEF'], $item['ITEM_DESCRIPTION'], $modDevise->getTarifBDD($lastMontant, $dataFacture['FACT_CHANGE']), $item['ITEM_QUANTITE'], $item['ITEM_TAUXTVA']);
			}
		}
		$idfacture = $this->newFactureData($newData);
		if($newData['FACTURE']['FACT_ETAT'] != 10) $this->unlock();
		if($idfacture) BoondManager_Notification_Facture::getInstance($idfacture, 0, ($oldData = array('ID_RESPUSER' => $dataFacture['ID_RESPUSER'])), $newData, $this->db)->create();//Gestion des notifications
		return $idfacture;
	}

	/**
	 * \brief Retourne la prochaine référence de facturation en tenant compte du masque de facturation
	 * @param string $mask
	 */
	function getNewReferenceFacture($mask = '', $onlymask = false, $forceDate = '') {
		throw new \Exception('do migration'); //TODO
		if(preg_match("/([a-zA-Z0-9\[\]\{\}_]{0,}) ([0-9]{1,})/", $mask, $matchRef)) {
			if($forceDate != '' && preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $forceDate, $match)) {
				$forceDate = explode('-',$forceDate);
				$annee4 = $forceDate[0];
				$annee2 = substr($forceDate[0],2);
				$mois = $forceDate[1];
			} else {
				$actualTime = mktime();
				$annee4 = date('Y',$actualTime);
				$annee2 = date('y',$actualTime);
				$mois = date('m',$actualTime);
			}

			$mask = str_replace(array('[AAAA]','[AA]','[MM]','{AAAA}','{AA}','{MM}'), array($annee4,$annee2,$mois,$annee4,$annee2,$mois), $matchRef[1]);
			$maskWhere = str_replace(array('_','[AAAA]','[AA]','[MM]','{AAAA}','{AA}','{MM}'), array('\_',$annee4,$annee2,$mois,'____','__','__'), $matchRef[1]);//Masque de la clause Where
			$startNumber = $matchRef[2];
		} else {
			$mask = 'F';
			$maskWhere = 'F';//Masque de la clause Where
			$startNumber = '1000000000000';
		}

		$result = $this->db->query_first('SELECT MAX(CAST(SUBSTRING(FACT_REF,'.(strlen($mask)+1).') AS UNSIGNED)) AS MAX_REFERENCE FROM TAB_FACTURATION AS FACT_READ WHERE FACT_REF LIKE \''.$this->db->escape($maskWhere).'%\';');
		if($result) {
			$maxReference = $mask.$result['MAX_REFERENCE'];
			$addOne = 1;
		} else {
			$maxReference = $mask;
			$addOne = 0;
		}

		if(preg_match("/".$mask."([0-9]{1,})/", $maxReference, $matchMax)) $startNumber = str_pad(strval(intval($matchMax[1])+$addOne), strlen($startNumber), "0", STR_PAD_LEFT);
		if($onlymask) return $mask; else return $mask.$startNumber;
	}

	/**
	 * \brief Indique si une référence de facture existe déjà en BDD
	 */
	function isReferenceExist($reference) {
		throw new \Exception('do migration'); //TODO
		if($this->db->query_first('SELECT ID_FACTURATION FROM TAB_FACTURATION WHERE FACT_REF="'.$this->db->escape($reference).'"')) return true; else return false;
	}

	/**
	 * \brief Indique si la fiche est supprimable
	 */
	function isFactureReducible($id) {
		throw new \Exception('do migration'); //TODO
		$nbDocument = 0;
		$sql = 'SELECT IF(FACT_CLOTURE=1,1,0) AS NB_DOCUMENT FROM TAB_FACTURATION WHERE ID_FACTURATION ="'.$this->db->escape($id).'"';
		foreach($this->db->fetch_all_array($sql) as $document) $nbDocument += $document['NB_DOCUMENT'];
		if($nbDocument == 0) return true; else return false;
	}
}
