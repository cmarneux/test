<?php
/**
 * order.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use BoondManager\APIs\BillingDeliveriesPurchasesBalance\Filters\SearchBillingDeliveriesPurchasesBalance;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Services\BM;
use BoondManager\OldModels\MySQL\Mappers;

/**
 * Handle all database work related to orders
 * @namespace \BoondManager\Models\MySQL\Local
 */
class BillingDeliveriesPurchasesBalance extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchBillingDeliveriesPurchasesBalance $filter
	 * @return SearchResult
	 */
	public function searchBillingDeliveriesPurchasesBalance(SearchBillingDeliveriesPurchasesBalance $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$commonColumns = 'COUNT(DISTINCT BDCPRJ.ID_BONDECOMMANDE) AS NB_BDC, COUNT(DISTINCT COR.ID_BONDECOMMANDE) AS NB_COR,
		PRJ.ID_PROJET, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF, PRJ_ETAT, PRJ_DEVISE, PRJ_CHANGE, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ.ID_AO, AO_TITLE, TAB_USER.ID_PROFIL, PRJ_DATE, TAB_CRMCONTACT.ID_CRMCONTACT,
		CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE AS ID_BONDECOMMANDE,
		COR.BDC_REF AS BDC_REF, COR.BDC_REFCLIENT AS BDC_REFCLIENT, RESP.PROFIL_NOM, RESP.PROFIL_PRENOM';

		$dlvColumns = '0 AS DLVPRC_TYPE, DLVPRC.ID_MISSIONPROJET AS ID_ITEM, MP_DEBUT AS ITEM_DEBUT, MP_FIN AS ITEM_FIN, MP_TYPE AS ITEM_PREVISIONNEL,
		AT.ID_PROFIL AS COMP_IDPROFIL, AT.PROFIL_NOM AS COMP_NOM, AT.PROFIL_PRENOM AS COMP_PRENOM, TAB_PRODUIT.ID_PRODUIT,
		CASE WHEN AT.ID_PROFIL IS NOT NULL THEN CONCAT_WS(\' \',AT.PROFIL_NOM, AT.PROFIL_PRENOM) ELSE CASE WHEN TAB_PRODUIT.ID_PRODUIT IS NOT NULL
		THEN PRODUIT_NOM ELSE \'\' END END AS ITEM_TITRE, MP_NOM AS ITEM_INTITULE';

		$prcColumns = '1 AS DLVPRC_TYPE, DLVPRC.ID_ACHAT AS ID_ITEM, ACHAT_DEBUT AS ITEM_DEBUT, ACHAT_FIN AS ITEM_FIN, 0 AS ITEM_PREVISIONNEL,
		0 AS COMP_IDPROFIL, \'\'  AS COMP_NOM, \'\'  AS COMP_PRENOM, 0 AS ID_PRODUIT, ACHAT_TITLE AS ITEM_TITRE, \'\' AS ITEM_INTITULE';

		$dlvQuery = new Query();
		$dlvQuery->addColumns($commonColumns);
		$dlvQuery->addColumns($dlvColumns);
		$dlvQuery->from('TAB_MISSIONPROJET DLVPRC
		INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=DLVPRC.ID_PROJET)
		LEFT JOIN TAB_AO ON(TAB_AO.ID_AO=PRJ.ID_AO)
		LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=PRJ.ID_PROFIL)
		LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)
		'.(($filter->period->getValue() == SearchBillingDeliveriesPurchasesBalance::PERIOD_ADDITIONALDATAINPROGRESS)?'
		LEFT JOIN TAB_MISSIONDETAILS ON(TAB_MISSIONDETAILS.PARENT_TYPE=1 AND TAB_MISSIONDETAILS.ID_PARENT=DLVPRC.ID_MISSIONPROJET AND MISDETAILS_ETAT=1)':'').'
		LEFT JOIN TAB_PROFIL AT ON(AT.ID_PROFIL=DLVPRC.ID_ITEM AND ITEM_TYPE=0)
		LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=DLVPRC.ID_ITEM AND ITEM_TYPE=1)
		LEFT JOIN TAB_BONDECOMMANDE BDCPRJ ON(BDCPRJ.ID_PROJET=PRJ.ID_PROJET)
		LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
		LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
		LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=ID_MISSIONPROJET AND CORBDC_TYPE=0)
		LEFT JOIN TAB_BONDECOMMANDE COR ON(COR.ID_BONDECOMMANDE=TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE)');

		$prcQuery = new Query();
		$prcQuery->addColumns($commonColumns);
		$prcQuery->addColumns($prcColumns);
		$prcQuery->from('TAB_ACHAT DLVPRC
		INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=DLVPRC.ID_PROJET AND ACHAT_REFACTURE=1)
		LEFT JOIN TAB_AO ON(TAB_AO.ID_AO=PRJ.ID_AO)
		LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=PRJ.ID_PROFIL)
		LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)
		LEFT JOIN TAB_BONDECOMMANDE BDCPRJ ON(BDCPRJ.ID_PROJET=PRJ.ID_PROJET)
		LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
		LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
		LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=ID_ACHAT AND CORBDC_TYPE=1)
		LEFT JOIN TAB_BONDECOMMANDE COR ON(COR.ID_BONDECOMMANDE=TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE)');

		$commonWhere = new Where('PRJ.PRJ_TYPE>0');
		$commonWhere->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'PRJ.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$dlvQuery->groupBy('DLVPRC.ID_MISSIONPROJET');
		$prcQuery->groupBy('DLVPRC.ID_ACHAT');

		$keywordsMapping = ['PRJ'=>'PRJ.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$commonWhere->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch);
			$commonWhere->and_($whereKeywords);
		}

		$commonWhere->and_( $this->getFilterSearch($filter->projectStates->getValue(), 'PRJ_ETAT') )
				->and_( $this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$dlvQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PROJECT.' AND TAB_FLAG.ID_PARENT=PRJ.ID_PROJET)');
			$prcQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PROJECT.' AND TAB_FLAG.ID_PARENT=PRJ.ID_PROJET)');
			$commonWhere->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		switch ($filter->period->getValue()) {
			case SearchBillingDeliveriesPurchasesBalance::PERIOD_INPROGRESS:
				$dlvQuery->addWhere('ITEM_TYPE IN(0,1) AND DATEDIFF(MP_FIN,?)>=0 AND DATEDIFF(?,MP_DEBUT)>=0',
				[$filter->startDate->getValue(), $filter->endDate->getValue()]);
				$prcQuery->addWhere('DATEDIFF(ACHAT_FIN,?)>=0 AND DATEDIFF(?,ACHAT_DEBUT)>=0', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			case SearchBillingDeliveriesPurchasesBalance::PERIOD_ADDITIONALDATAINPROGRESS:
				$dlvQuery->addWhere('ITEM_TYPE IN(0,1) AND ((DATEDIFF(MP_FIN,?)>=0 AND DATEDIFF(?,MP_DEBUT)>=0) OR MISDETAILS_DATE BETWEEN ? AND ?)',
				[$filter->startDate->getValue(), $filter->endDate->getValue(), $filter->startDate->getValue(), $filter->endDate->getValue()]);
				$prcQuery->addWhere('DATEDIFF(ACHAT_FIN,?)>=0 AND DATEDIFF(?,ACHAT_DEBUT)>=0', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			default:case BM::INDIFFERENT:$dlvQuery->addWhere('ITEM_TYPE IN(0,1)');break;
		}

		$dlvQuery->addWhere($commonWhere);
		$prcQuery->addWhere($commonWhere);

		$this->setOrderExpr($dlvQuery, $filter->sort->getValue(), $filter->order->getValue());
		$this->setOrderExpr($prcQuery, $filter->sort->getValue(), $filter->order->getValue());

		/** @var Query[] $queries */
		$queries = [$dlvQuery, $prcQuery];

		$totalRows = 0;
		$subQueries = [];
		$argQueries = [];
		foreach($queries as $q){
			$result = $this->singleExec( $q->getCountQuery(), $q->getArgs() );
			$totalRows += $result->NB_ROWS;
			$subQueries[] = $q->getQuery();
			$argQueries = array_merge($argQueries, $q->getArgs());
		}

		if($totalRows){
			$sql_union = 'SELECT * FROM (('.implode(') UNION ALL (', $subQueries).')) AS DLVPRC '.$queries[0]->buildOrderBy().' LIMIT '.Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()).', '.$filter->maxResults->getValue();
			$result = $this->exec($sql_union, $argQueries);
		}else{
			$result = [];
		}

		return new SearchResult($result, $totalRows);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_STARTDATE => 'ITEM_DEBUT',
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_ENDDATE => 'ITEM_FIN',
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_ID => 'ID_ITEM',
			//~ SearchBillingDeliveriesPurchasesBalance::ORDERBY_STATE => 'ITEM_STATE', ? la vue ne permet pas ça
			//~ SearchBillingDeliveriesPurchasesBalance::ORDERBY_PROJECT_STATE => 'PRJ_STATE', ? la vue ne permet pas ça
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_ORDER_REFERENCE => 'BDC_REF',
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_PROJECT_REFERENCE => 'PRJ_REFERENCE',
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_PROJECT_COMPANY_NAME => 'CSOC_SOCIETE',
			SearchBillingDeliveriesPurchasesBalance::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy('ITEM_DEBUT DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('ID_PROJET DESC');
	}
}
