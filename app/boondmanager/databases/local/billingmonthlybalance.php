<?php
/**
 * order.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use Wish\MySQL\Query;
use Wish\Models\SearchResult;
use Wish\MySQL\Where;
use BoondManager\Services\BM;
use BoondManager\Models;
use BoondManager\APIs\BillingMonthlyBalance\Filters\SearchBillingMonthlyBalance;

/**
 * Handle all database work related to orders
 * @namespace \BoondManager\Models\MySQL\Local
 */
class BillingMonthlyBalance extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchBillingMonthlyBalance $filter
	 * @return SearchResult
	 */
	public function searchBillingMonthlyBalance(SearchBillingMonthlyBalance $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('COUNT(DISTINCT TAB_FACTURATION.ID_FACTURATION) AS NB_FACTURE, BDC.ID_BONDECOMMANDE, PRJ.ID_PROJET, PRJ_REFERENCE,
		PRJ_TYPE, PRJ_TYPEREF, ID_AO, AO_TITLE, TAB_USER.ID_PROFIL, BDC_DATE, BDC_REF, BDC_REFCLIENT, BDC_SEPARATETPSFRS, PRJ_DEVISEAGENCE, 
		PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, 
		TAB_CRMSOCIETE.ID_CRMSOCIETE, PROFIL_NOM, PROFIL_PRENOM, SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100))/CASE WHEN 
		COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_CAFACTUREHT, 
		0 AS TOTAL_CAPRODUCTIONHT, TAB_FACTURATION.ID_FACTURATION, FACT_REF, FACT_ETAT, FACT_DEBUT, FACT_FIN, FACT_DEVISEAGENCE, 
		FACT_CHANGEAGENCE, FACT_CHANGE, FACT_DEVISE, SUM(CASE WHEN ID_ITEMFACTURE IS NOT NULL THEN 
		ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100) ELSE 0 END)/CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE 
		COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS DOCUMENT_DELTA');

		$query->from('TAB_BONDECOMMANDE BDC');

		//~ We calculate $startDate & $endDate from $filter->startMonth
		$month = explode('-', $filter->startMonth->getValue());

		$startDate = date('Y-m-d',mktime(0,0,0,(int)$month[1],1,$month[0]));
		$endDate = date('Y-m-d',mktime(0,0,0,(int)$month[1]+1,0,$month[0]));
		$query->addJoin('INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=BDC.ID_PROJET) 
		LEFT JOIN TAB_AO USING(ID_AO) 
		LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
		LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
		LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
		LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
		LEFT JOIN TAB_FACTURATION ON(TAB_FACTURATION.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE 
		AND DATEDIFF('.$this->escape($endDate).',FACT_DEBUT)>=0 AND DATEDIFF(FACT_FIN,'.$this->escape($startDate).')>=0)
		LEFT JOIN TAB_ITEMFACTURE ON(TAB_ITEMFACTURE.ID_FACTURATION=TAB_FACTURATION.ID_FACTURATION)
		LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)
		LEFT JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=0 
		AND DATEDIFF('.$this->escape($endDate).',MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,'.$this->escape($startDate).')>=0)
		LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_ACHAT=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=1 
		AND DATEDIFF('.$this->escape($endDate).',ACHAT_DEBUT)>=0 AND DATEDIFF(ACHAT_FIN,'.$this->escape($startDate).')>=0)');

		$query->groupBy('BDC.ID_BONDECOMMANDE');

		$where = new Where('PRJ.PRJ_TYPE>0 AND BDC_TYPEREGLEMENT=1');

		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'PRJ.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['PRJ'=>'PRJ.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('BDC_REF LIKE ?', $likeSearch)
						  ->or_('BDC_REFCLIENT LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_( $this->getFilterSearch($filter->orderStates->getValue(), 'BDC_ETAT') )
				->and_( $this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF') )
				->and_( $this->getFilterSearch($filter->orderPaymentMethods->getValue(), 'BDC_TYPEPAYMENT') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);
		$whereGlobal = new Where('((ID_CORRELATIONBONDECOMMANDE IS NULL AND DATEDIFF(PRJ.PRJ_FIN,?)>=0) AND DATEDIFF(?,PRJ.PRJ_DEBUT)>=0 
		OR (ID_CORRELATIONBONDECOMMANDE IS NOT NULL AND (TAB_MISSIONPROJET.ID_MISSIONPROJET IS NOT NULL OR TAB_ACHAT.ID_ACHAT IS NOT NULL)))',
		[$startDate, $endDate]);
		$query->addWhere($whereGlobal);

		$result = $this->launchSearch($query);
		//~ TODO revoir le calcul des totaux
		$result->turnoverInvoicedExcludingTax = $result->turnoverProductionExcludingTax = $result->deltaProductionExcludingTax = 0;
		if($result->total > 0){
			$whereMissions = new Where('((ID_CORRELATIONBONDECOMMANDE IS NULL AND DATEDIFF(?,PRJ.PRJ_DEBUT)>=0 AND DATEDIFF(PRJ.PRJ_FIN,?)>=0) OR 
			(ID_CORRELATIONBONDECOMMANDE IS NOT NULL AND TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=TAB_MISSIONPROJET.ID_MISSIONPROJET AND 
			CORBDC_TYPE=0 AND DATEDIFF(?,MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,?)>=0))',
			[$endDate,$startDate,$endDate,$startDate]);

			$whereAchats = new Where('((ID_CORRELATIONBONDECOMMANDE IS NULL AND DATEDIFF(?,PRJ.PRJ_DEBUT)>=0 AND DATEDIFF(PRJ.PRJ_FIN,?)>=0) OR 
			(ID_CORRELATIONBONDECOMMANDE IS NOT NULL AND TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=TAB_ACHAT.ID_ACHAT AND CORBDC_TYPE=1 AND 
			DATEDIFF(?,ACHAT_DEBUT)>=0 AND DATEDIFF(ACHAT_FIN,?)>=0))',
			[$endDate,$startDate,$endDate,$startDate]);

			$whereDetails = new Where('ID_CORRELATIONBONDECOMMANDE IS NULL AND DATEDIFF(?,PRJ.PRJ_DEBUT)>=0 AND DATEDIFF(PRJ.PRJ_FIN,?)>=0 AND 
			MISDETAILS_ETAT=1',[$endDate,$startDate]);

			//On calcule le CA Facturé des projets (on distingue les projets corrélés aux missions des autres)
			$caFactQuery = (new Query())
			->addColumns('SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100))/CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 
			ELSE COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_CAFACTUREHT')
			->addJoin('TAB_BONDECOMMANDE BDC
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=BDC.ID_PROJET)
			INNER JOIN TAB_FACTURATION ON(TAB_FACTURATION.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE AND 
			DATEDIFF('.$this->escape($endDate).',FACT_DEBUT)>=0 AND DATEDIFF(FACT_FIN,'.$this->escape($startDate).')>=0)
			INNER JOIN TAB_ITEMFACTURE ON(TAB_ITEMFACTURE.ID_FACTURATION=TAB_FACTURATION.ID_FACTURATION)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)
			LEFT JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=0 AND 
			DATEDIFF('.$this->escape($endDate).',MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,'.$this->escape($startDate).')>=0)
			LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_ACHAT=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=1 AND 
			DATEDIFF('.$this->escape($endDate).',ACHAT_DEBUT)>=0 AND DATEDIFF(ACHAT_FIN,'.$this->escape($startDate).')>=0)')
			->addWhere($where)
			->addWhere($whereGlobal)
			->groupBy('BDC.ID_BONDECOMMANDE');

			if(sizeof($filter->flags->getValue()) > 0) $caFactQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caFactQuery->addWhere($whereMissions);
			$caFact = $this->exec($caFactQuery);
			foreach($caFact as $ca) $result->turnoverInvoicedExcludingTax += $ca->TOTAL_CAFACTUREHT;

			//On calcule le CA de production des temps des projets (on distingue les BDC corrélés aux missions des autres)
			$selectTemps = 'BDC.ID_BONDECOMMANDE, SUM(MP_TARIF*TEMPS_DUREE) AS TOTAL_CAPRODUCTIONHT';
			$joinTemp = 'INNER JOIN TAB_TEMPS ON(TAB_TEMPS.ID_LIGNETEMPS=TAB_LIGNETEMPS.ID_LIGNETEMPS AND TEMPS_DATE 
			BETWEEN '.$this->escape($startDate).' AND '.$this->escape($endDate).')
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_MISSIONPROJET.ID_PROJET) 
			INNER JOIN TAB_BONDECOMMANDE BDC ON(BDC.ID_PROJET=PRJ.ID_PROJET)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE USING(ID_BONDECOMMANDE)';

			$caProdQuery = (new Query())->addColumns($selectTemps)->from('TAB_LIGNETEMPS')
			->addJoin('INNER JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET)')
			->addJoin($joinTemp)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereMissions);
			$caProd = $this->exec($caProdQuery);
			$caProdQuery = (new Query())->addColumns($selectTemps)->from('TAB_LIGNETEMPS')
			->addJoin('INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_MASTER=TAB_LIGNETEMPS.ID_MISSIONPROJET)')
			->addJoin($joinTemp)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereMissions);
			$caProd = $this->exec($caProdQuery);

			//On calcule le CA de production des frais refacturés des projets (on distingue les BDC corrélés aux missions des autres)
			$selectFrais = 'BDC.ID_BONDECOMMANDE, SUM(CASE WHEN FRAISREEL_REFACTURE=1 THEN CASE WHEN FRAISREEL_TYPEFRSREF=0 THEN FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE ELSE 
			FRAISREEL_MONTANT END ELSE 0 END) AS TOTAL_CAPRODUCTIONHT';
			$joinFrais = 'INNER JOIN TAB_LISTEFRAIS ON(TAB_LISTEFRAIS.ID_LISTEFRAIS=TAB_FRAISREEL.ID_LISTEFRAIS AND FRAISREEL_DATE 
			BETWEEN '.$this->escape($startDate).' AND '.$this->escape($endDate).')
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_MISSIONPROJET.ID_PROJET) 
			INNER JOIN TAB_BONDECOMMANDE BDC ON(BDC.ID_PROJET=PRJ.ID_PROJET)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE USING(ID_BONDECOMMANDE)';

			$caProdQuery = (new Query())->addColumns($selectFrais)->from('TAB_FRAISREEL')
			->addJoin('INNER JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET)')
			->addJoin($joinFrais)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereMissions);
			$caProd = array_merge($caProd, $this->exec($caProdQuery));
			$caProdQuery = (new Query())->addColumns($selectFrais)->from('TAB_FRAISREEL')
			->addJoin('INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_MASTER=TAB_FRAISREEL.ID_MISSIONPROJET)')
			->addJoin($joinFrais)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereMissions);
			$caProd = array_merge($caProd, $this->exec($caProdQuery));

			//On calcule le CA de production des achats refacturés des projets (on distingue les BDC corrélés aux missions des autres)
			$selectAchats = 'BDC.ID_BONDECOMMANDE, SUM(CASE WHEN ACHAT_REFACTURE=1 THEN ACHAT_TARIFFACTURE*CASE WHEN ACHAT_TYPE=2 THEN 3 
			WHEN ACHAT_TYPE=3 THEN 6 WHEN ACHAT_TYPE=4 THEN 12 ELSE 1 END/
			IF(ACHAT_TYPE=0,1,PERIOD_DIFF(DATE_FORMAT(ACHAT_FIN,"%Y%m"),DATE_FORMAT(ACHAT_DEBUT,"%Y%m"))+1) ELSE 0 END) AS TOTAL_CAPRODUCTIONHT';
			$joinAchats = 'INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_ACHAT.ID_PROJET AND 
			DATEDIFF('.$this->escape($endDate).',ACHAT_DEBUT)>=0 AND DATEDIFF(ACHAT_FIN,'.$this->escape($startDate).')>=0) 
			LEFT JOIN TAB_AO USING(ID_AO) 
			INNER JOIN TAB_BONDECOMMANDE BDC ON(BDC.ID_PROJET=PRJ.ID_PROJET)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE USING(ID_BONDECOMMANDE)';

			$caProdQuery = (new Query())->addColumns($selectAchats)->from('TAB_ACHAT')
			->addJoin($joinAchats)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereAchats);
			$caProd = array_merge($caProd, $this->exec($caProdQuery));

			//On calcule le CA de production des produits des projets (on distingue les BDC corrélés aux missions des autres)
			$selectProduits = 'BDC.ID_BONDECOMMANDE, SUM(MP_TARIF/CASE WHEN PRODUIT_TYPE=2 THEN 3 WHEN PRODUIT_TYPE=3 THEN 6 WHEN PRODUIT_TYPE=4 THEN 12 ELSE 1 END) 
			AS TOTAL_CAPRODUCTIONHT';
			$joinProduits = 'INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_ITEM=TAB_PRODUIT.ID_PRODUIT AND ITEM_TYPE=1)
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_MISSIONPROJET.ID_PROJET) 
			INNER JOIN TAB_BONDECOMMANDE BDC ON(BDC.ID_PROJET=PRJ.ID_PROJET)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE USING(ID_BONDECOMMANDE)';

			$caProdQuery = (new Query())->addColumns($selectProduits)->from('TAB_PRODUIT')
			->addJoin($joinProduits)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereMissions);
			$caProd = array_merge($caProd, $this->exec($caProdQuery));


			////On calcule le CA de production des détails de mission des projets
			$selectDetail = 'BDC.ID_BONDECOMMANDE, SUM(MISDETAILS_TARIF) AS TOTAL_CAPRODUCTIONHT';
			$joinDetail = 'INNER JOIN TAB_BONDECOMMANDE BDC ON(BDC.ID_PROJET=PRJ.ID_PROJET)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)';

			$caProdQuery = (new Query())->addColumns($selectDetail)->from('TAB_MISSIONDETAILS')
			->addJoin('INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_MISSIONDETAILS.ID_PARENT AND PARENT_TYPE=0)')
			->addJoin($joinDetail)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereDetails);
			$caProd = array_merge($caProd, $this->exec($caProdQuery));

			$caProdQuery = (new Query())->addColumns($selectDetail)->from('TAB_MISSIONDETAILS')
			->addJoin('INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_MISSIONDETAILS.ID_PARENT AND PARENT_TYPE=1)
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_MISSIONPROJET.ID_PROJET)')
			->addJoin($joinDetail)
			->addWhere($where);
			if(sizeof($filter->flags->getValue()) > 0) $caProdQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caProdQuery->addWhere($whereMissions);
			$caProd = array_merge($caProd, $this->exec($caProdQuery));

			foreach($caProd as $ca) {
				$result->turnoverProductionExcludingTax += $ca->TOTAL_CAPRODUCTIONHT;
				foreach($result->rows as $order) {
					if($order->ID_BONDECOMMANDE == $ca->ID_BONDECOMMANDE) {
						$order->TOTAL_CAPRODUCTIONHT += $ca->TOTAL_CAPRODUCTIONHT;
						$order->DOCUMENT_DELTA -= $ca->TOTAL_CAPRODUCTIONHT;
						break;
					}
				}
			}
			$result->deltaProductionExcludingTax = $result->turnoverProductionExcludingTax - $result->turnoverInvoicedExcludingTax;
		}

		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchBillingMonthlyBalance::ORDERBY_CREATIONDATE => 'BDC_DATE',
			SearchBillingMonthlyBalance::ORDERBY_REFERENCE => 'FACT_REF',
			SearchBillingMonthlyBalance::ORDERBY_STATE => 'FACT_ETAT',
			SearchBillingMonthlyBalance::ORDERBY_NUMBEROFINVOICES => 'NB_FACTURE',
			SearchBillingMonthlyBalance::ORDERBY_PROJECT_REFERENCE => 'PRJ_REFERENCE',
			SearchBillingMonthlyBalance::ORDERBY_PROJECT_COMPANY_NAME => 'CSOC_SOCIETE',
			SearchBillingMonthlyBalance::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy('BDC.BDC_DATE DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('BDC.ID_BONDECOMMANDE DESC');
	}
}
