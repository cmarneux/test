<?php
/**
 * order.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use BoondManager\APIs\BillingProjectsBalance\Filters\SearchBillingProjectsBalance;
use Wish\MySQL\Query;
use Wish\Models\SearchResult;
use Wish\MySQL\Where;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;
use BoondManager\OldModels\Filters;
use BoondManager\Services\Actions;

/**
 * Handle all database work related to orders
 * @namespace \BoondManager\Models\MySQL\Local
 */
class BillingProjectsBalance extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchBillingProjectsBalance $filter
	 * @return SearchResult
	 */
	public function searchBillingProjectsBalance(SearchBillingProjectsBalance $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('COUNT(DISTINCT TAB_BONDECOMMANDE.ID_BONDECOMMANDE) AS NB_BDC, PRJ.ID_PROJET, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF, 
		PRJ.ID_AO, AO_TITLE, TAB_USER.ID_PROFIL, PRJ_DATE, SUM(BDC_MONTANTHT) AS TOTAL_MONTANTBDC, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, 
		PRJ_CHANGE, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, TAB_CRMSOCIETE.ID_CRMSOCIETE, PROFIL_NOM, PROFIL_PRENOM,
		0 AS TOTAL_CASIGNEHT, SUM(BDC_MONTANTHT) AS DOCUMENT_DELTA');

		$where = new Where('PRJ.PRJ_TYPE>0');
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'PRJ.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$query->from('TAB_PROJET PRJ');

		$query->addJoin('LEFT JOIN TAB_AO ON(TAB_AO.ID_AO=PRJ.ID_AO) 
		LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=PRJ.ID_PROFIL) 
		LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL)
		'.(($filter->period->getValue() == SearchBillingProjectsBalance::PERIOD_CREATEDORWITHADDITIONALDATAINPROGRESS)?'LEFT JOIN TAB_MISSIONDETAILS 
		ON(TAB_MISSIONDETAILS.PARENT_TYPE=0 	AND TAB_MISSIONDETAILS.ID_PARENT=PRJ.ID_PROJET AND MISDETAILS_ETAT=1)':'').'
		LEFT JOIN TAB_BONDECOMMANDE ON(TAB_BONDECOMMANDE.ID_PROJET=PRJ.ID_PROJET)
		LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
		LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)');

		$query->groupBy('PRJ.ID_PROJET');

		$keywordsMapping = ['PRJ'=>'PRJ.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_( $this->getFilterSearch($filter->projectStates->getValue(), 'PRJ_ETAT') )
				->and_( $this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PROJECT.' AND TAB_FLAG.ID_PARENT=PRJ.ID_PROJET)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		switch ($filter->period->getValue()) {
			case SearchBillingProjectsBalance::PERIOD_CREATED:
				$query->addWhere('PRJ_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			case SearchBillingProjectsBalance::PERIOD_CREATEDORWITHADDITIONALDATAINPROGRESS:
				$query->addWhere('PRJ_DATE BETWEEN ? AND ? OR MISDETAILS_DATE BETWEEN ? AND ?',
					[$filter->startDate->getValue(), $filter->endDate->getValue(), $filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			default:case BM::INDIFFERENT:break;
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		$result = $this->launchSearch($query, Models\SearchResults\BillingProjectsBalance::class);

		$result->turnoverSignedExcludingTax = $result->turnoverOrderedExcludingTax = $result->deltaOrderedExcludingTax = 0;
		if($result->total > 0){
			foreach($result->rows as $project) $result->turnoverOrderedExcludingTax += $project->TOTAL_MONTANTBDC;
			//On ajoute les CA des missions au TOTAL
			$caSigneQuery = (new Query())
			->addColumns('SUM(MP_TARIF*MP_NBJRSFACTURE+MP_TARIFADDITIONNEL)+SUM(PRJ_TARIFADDITIONNEL)/CASE WHEN 
			COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) END AS TOTAL_CASIGNEHT')
			->addJoin('TAB_PROJET PRJ 
			LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_PROJET=PRJ.ID_PROJET AND TAB_MISSIONPROJET.MP_TYPE<>1) 
			LEFT JOIN TAB_AO ON(TAB_AO.ID_AO=PRJ.ID_AO) 
			LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=PRJ.ID_PROFIL) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT 
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)')
			->addWhere($where)
			->groupBy('PRJ.ID_PROJET');

			if(sizeof($filter->flags->getValue()) > 0) $caSigneQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PROJECT.' AND 
			TAB_FLAG.ID_PARENT=PRJ.ID_PROJET)');
			$caSigne = $this->exec($caSigneQuery);
			foreach($caSigne as $ca) $result->turnoverSignedExcludingTax += $ca->TOTAL_CASIGNEHT;

			//Il faut maintenant récupérer les CA des missions
			$ids = Tools::getFieldsToArray($result->rows, 'ID_PROJET');
			//Il faut maintenant récupérer les CA et Investissements additionnels totaux des projets non en AT
			$sql =  'SELECT PRJ.ID_PROJET, SUM(MP_TARIF*MP_NBJRSFACTURE+MP_TARIFADDITIONNEL)+SUM(PRJ_TARIFADDITIONNEL)/
			CASE WHEN COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET)=0 THEN 1 ELSE 
			COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) END AS TOTAL_CASIGNEHT 
			FROM TAB_PROJET PRJ LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_PROJET=PRJ.ID_PROJET AND TAB_MISSIONPROJET.MP_TYPE<>1) 
			WHERE PRJ.ID_PROJET IN(' . Where::prepareWhereIN($ids). ') GROUP BY PRJ.ID_PROJET';
			$caSigne = $this->exec($sql,$ids);
			foreach($caSigne as $ca) {
					//On recherche le projet de la liste précédente et on modifie son CA, sa Marge et sa Renta.
					foreach($result->rows as $project) {
							if($project->ID_PROJET == $ca->ID_PROJET) {
									$project->TOTAL_CASIGNEHT += $ca->TOTAL_CASIGNEHT;
									$project->DOCUMENT_DELTA -= $ca->TOTAL_CASIGNEHT;
									break;
							}
					}
			}
			foreach($result->rows as $project) $result->deltaOrderedExcludingTax += $project->DOCUMENT_DELTA;
		}

		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchBillingProjectsBalance::ORDERBY_CREATIONDATE => 'PRJ_DATE',
			SearchBillingProjectsBalance::ORDERBY_STATE => 'PRJ_ETAT',
			SearchBillingProjectsBalance::ORDERBY_NUMBEROFORDERS => 'NB_BDC',
			SearchBillingProjectsBalance::ORDERBY_REFERENCE => 'PRJ_REFERENCE',
			SearchBillingProjectsBalance::ORDERBY_COMPANY_NAME => 'CSOC_SOCIETE',
			SearchBillingProjectsBalance::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy('PRJ.PRJ_DATE DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('PRJ.ID_PROJET DESC');
	}
}
