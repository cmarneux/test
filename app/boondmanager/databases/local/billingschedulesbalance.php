<?php
/**
 * order.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use BoondManager\APIs\BillingSchedulesBalance\Filters\SearchBillingSchedulesBalance;
use Wish\MySQL\Query;
use Wish\Models\SearchResult;
use Wish\MySQL\Where;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;
use BoondManager\OldModels\Filters;
use BoondManager\Services\Actions;

/**
 * Handle all database work related to orders
 * @namespace \BoondManager\Models\MySQL\Local
 */
class BillingSchedulesBalance extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchBillingSchedulesBalance $filter
	 * @return SearchResult
	 */
	public function searchBillingSchedulesBalance(SearchBillingSchedulesBalance $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('TAB_BONDECOMMANDE.ID_BONDECOMMANDE, PRJ.ID_PROJET, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF, ID_AO, AO_TITLE, TAB_USER.ID_PROFIL,
		BDC_DATE, BDC_REF, BDC_REFCLIENT, BDC_ACCORDCLIENT, BDC_SEPARATETPSFRS, BDC_MONTANTHT AS TOTAL_MONTANTBDC, PRJ_DEVISEAGENCE,
		PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, TAB_CRMSOCIETE.
		ID_CRMSOCIETE, PROFIL_NOM, PROFIL_PRENOM, SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)) AS TOTAL_CAFACTUREHT,
		COUNT(DISTINCT TAB_FACTURATION.ID_FACTURATION) AS NB_FACTURE, ECH.ID_ECHEANCIER, ECH_DATE, ECH_DESCRIPTION,
		ECH_MONTANTHT, SUM(CASE WHEN ID_ITEMFACTURE IS NOT NULL THEN ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)
		ELSE 0 END)-ECH_MONTANTHT AS DOCUMENT_DELTA');

		$where = new Where('PRJ.PRJ_TYPE>0 AND BDC_TYPEREGLEMENT=0');
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'TAB_USER.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$query->from('TAB_ECHEANCIER ECH');

		$query->addJoin('INNER JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE)
		INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_BONDECOMMANDE.ID_PROJET)
		LEFT JOIN TAB_AO USING(ID_AO)
		LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=TAB_BONDECOMMANDE.ID_RESPUSER)
		LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
		LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
		LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
		LEFT JOIN TAB_FACTURATION ON(TAB_FACTURATION.ID_ECHEANCIER=ECH.ID_ECHEANCIER)
		LEFT JOIN TAB_ITEMFACTURE ON(TAB_ITEMFACTURE.ID_FACTURATION=TAB_FACTURATION.ID_FACTURATION)');

		$query->groupBy('ECH.ID_ECHEANCIER');

		$keywordsMapping = ['BDC' => 'BDC.ID_BONDECOMMANDE','PRJ'=>'PRJ.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT','CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('BDC_REF LIKE ?', $likeSearch)
						  ->or_('BDC_REFCLIENT LIKE ?', $likeSearch)
						  ->or_('FACT_REF LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_( $this->getFilterSearch($filter->orderStates->getValue(), 'BDC_ETAT') )
				->and_( $this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF') )
				->and_( $this->getFilterSearch($filter->orderPaymentMethods->getValue(), 'BDC_TYPEPAYMENT') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		switch ($filter->period->getValue()) {
			case SearchBillingSchedulesBalance::PERIOD_PAYMENTDATE:
				$query->addWhere('ECH_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			default:case BM::INDIFFERENT:break;
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		$result = $this->launchSearch($query, Models\SearchResults\BillingSchedulesBalance::class);

		$result->turnoverInvoicedExcludingTax = $result->turnoverTermOfPaymentExcludingTax = $result->deltaInvoicedExcludingTax = 0;
		if($result->total > 0){
			//On ajoute les CA des missions au TOTAL
			$caEchQuery = (new Query())
			->addColumns('SUM(ECH_MONTANTHT)/CASE WHEN COUNT(DISTINCT TAB_FACTURATION.ID_FACTURATION)=0 THEN 1 ELSE 
			COUNT(DISTINCT TAB_FACTURATION.ID_FACTURATION) END AS TOTAL_ECHMONTANTHT')
			->addJoin('TAB_ECHEANCIER ECH INNER JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE) 
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_BONDECOMMANDE.ID_PROJET) 
			LEFT JOIN TAB_AO USING(ID_AO) 
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=TAB_BONDECOMMANDE.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT 
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_FACTURATION ON(TAB_FACTURATION.ID_ECHEANCIER=ECH.ID_ECHEANCIER)')
			->addWhere($where)
			->groupBy('ECH.ID_ECHEANCIER');

			if(sizeof($filter->flags->getValue()) > 0) $caEchQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$caEch = $this->exec($caEchQuery);
			foreach($caEch as $ca) $result->turnoverTermOfPaymentExcludingTax += $ca->TOTAL_ECHMONTANTHT;
			foreach($result->rows as $schedule){
				$result->turnoverInvoicedExcludingTax += $schedule->TOTAL_CAFACTUREHT;
				$result->deltaInvoicedExcludingTax += $schedule->DOCUMENT_DELTA;
			}
		}

		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchBillingSchedulesBalance::ORDERBY_CREATIONDATE => 'ECH_DATE',
			SearchBillingSchedulesBalance::ORDERBY_REFERENCE => 'FACT_REF',
			SearchBillingSchedulesBalance::ORDERBY_STATE => 'FACT_ETAT',
			SearchBillingSchedulesBalance::ORDERBY_NUMBEROFINVOICES => 'NB_FACTURE',
			SearchBillingSchedulesBalance::ORDERBY_PROJECT_REFERENCE => 'PRJ_REFERENCE',
			SearchBillingSchedulesBalance::ORDERBY_TURNOVERINVOICEDINCLUDEDTAX => 'TOTAL_CAFACTUREHT',
			SearchBillingSchedulesBalance::ORDERBY_TURNOVERTERMOFPAYMENTEXCLUDEDTAX => 'ECH_MONTANTHT',
			SearchBillingSchedulesBalance::ORDERBY_DELTAINVOICEDEXCLUDEDTAX => 'DOCUMENT_DELTA',
			SearchBillingSchedulesBalance::ORDERBY_PROJECT_COMPANY_NAME => 'CSOC_SOCIETE',
			SearchBillingSchedulesBalance::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy('ECH.ECH_DATE DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('ECH.ID_ECHEANCIER DESC');
	}
}
