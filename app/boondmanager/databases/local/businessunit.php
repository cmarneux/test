<?php
/**
 * businessunit.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\Cache;
use Wish\Models\Model;

/**
 * Handle all database work related to the business units
 * @@package BoondManager\Databases\Local;
 */
class BusinessUnit extends AbstractObject {
	/**
	* Retrieve all data related to a business unit
	* @param  integer  $id
	* @return Model|false
	*/
	public function getBusinessUnit($id) {
		$result = $this->singleExec('SELECT ID_BUSINESSUNIT, BU_NAME FROM TAB_BUSINESSUNIT WHERE ID_BUSINESSUNIT=?', $id);
		if($result) {
			$result->MANAGERS = $this->exec('SELECT INBU_EXCLUDE, ID_USER, ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM FROM TAB_INBU INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL) WHERE ID_BUSINESSUNIT=?', $id);
			return $result;
		}
		return false;
	}

    /**
     * Update all data from a BusinessUnit
	 * @param  integer  $id business unit ID to update, cf. [TAB_BUSINESSUNIT.ID_BUSINESSUNIT](../../bddclient/classes/TAB_BUSINESSUNIT.html#property_ID_BUSINESSUNIT).
     * @param array $data data to save
     *
     * This array can contains the following keys:
     * - `BUSINESSUNIT` : an array with business unit data, cf. [TAB_BUSINESSUNIT](../../bddclient/classes/TAB_BUSINESSUNIT.html)
     * - `MANAGERS` : an array with the business unit managers data
     *
     * @return bool
     */
    public function updateBusinessUnit($id, $data) {
        if(isset($data['BUSINESSUNIT']))
            $this->update('TAB_BUSINESSUNIT', $data['BUSINESSUNIT'], 'ID_BUSINESSUNIT=:id', array(':id' => $id));

		if(isset($data['MANAGERS'])) {
			$oldManagers = $this->exec('SELECT ID_USER, ID_INBU FROM TAB_INBU INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL) WHERE ID_BUSINESSUNIT=?', $id);
			foreach($data['MANAGERS'] as $i => $manager) {
				foreach($oldManagers as $oldManager) {
					if($oldManager['ID_USER'] == $manager['ID_USER']) {
						$data['MANAGERS'][$i]['ID_INBU'] = $oldManager['ID_INBU'];
						break;
					}
				}
			}

			$this->deleteAndCreateItemsFromEntity($data['MANAGERS'], 'TAB_INBU', 'ID_INBU', ['ID_BUSINESSUNIT' => $id]);
		}
        return true;
    }

	/**
	 * Create all data from a BusinessUnit
	 * @param array $data data to save
	 *
	 * This array can contains the following keys:
	 * - `BUSINESSUNIT` : an array with business unit data, cf. [TAB_BUSINESSUNIT](../../bddclient/classes/TAB_BUSINESSUNIT.html)
	 * - `MANAGERS` : an array with the business unit managers data
	 *
	 * @return integer the business unit ID
	 */
	public function createBusinessUnit($data) {
		if(isset($data['BUSINESSUNIT'])) {
			$id = $this->insert('TAB_BUSINESSUNIT', $data['BUSINESSUNIT']);

			if($id && isset($data['MANAGERS'])) {
				foreach($data['MANAGERS'] as $manager) {
					$manager['ID_BUSINESSUNIT'] = $id;
					$this->insert('TAB_INBU', $manager);
				}
			}
			return $id;
		}
		return false;
	}

    /**
     * Delete a business unit
     * @param  integer  $id
     * @return boolean.
     */
    public function deleteBusinessUnit($id) {
        $this->delete('TAB_BUSINESSUNIT', 'ID_BUSINESSUNIT=?', $id);
        $this->delete('TAB_INBU', 'ID_BUSINESSUNIT=?', $id);
        return true;
    }

    /**
     * Get data for all business units
     * @return BusinessUnit[]
     */
    function getAllBusinessUnits() {
        $cache = Cache::instance();
        $key = 'bu.group.list';
        if(!$cache->exists($key)) {
            $sql = 'SELECT ID_BUSINESSUNIT, INBU_EXCLUDE, BU_NAME, ID_USER, ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM
                    FROM TAB_BUSINESSUNIT
                    LEFT JOIN TAB_INBU USING(ID_BUSINESSUNIT)
                    LEFT JOIN TAB_USER USING(ID_USER)
                    LEFT JOIN TAB_PROFIL USING(ID_PROFIL)
                    ORDER BY PROFIL_NOM ASC';
            $result = $this->exec($sql);

            $tabBUs = [];
            foreach($result as $bu) {
				if(!isset($tabBUs[$bu['ID_BUSINESSUNIT']]))
					$tabBUs[$bu['ID_BUSINESSUNIT']] = [
						'ID_BUSINESSUNIT' => $bu['ID_BUSINESSUNIT'],
						'BU_NAME' => $bu['BU_NAME'],
						'MANAGERS' => $bu['MANAGERS']
					];

				$tabBUs[$bu['ID_BUSINESSUNIT']]['MANAGERS'][] = [
					'ID_USER' => $bu['ID_USER'],
					'ID_PROFIL' => $bu['ID_PROFIL'],
					'PROFIL_NOM' => $bu['PROFIL_NOM'],
					'PROFIL_PRENOM' => $bu['PROFIL_PRENOM'],
					'INBU_EXCLUDE' => $bu['INBU_EXCLUDE']
				];
			}
            $cache->set($key, $tabBUs);
        }
        return $cache->get($key);
    }
}
