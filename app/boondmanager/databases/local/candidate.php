<?php
/**
 * candidate.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Candidates\Filters\SearchCandidates;
use BoondManager\Lib\GED;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use BoondManager\Lib\SolR;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\OldModels\Filters;
use BoondManager\OldModels\MySQL\Mappers\Candidates\Information;
use BoondManager\Models;
use BoondManager\Services;

class Candidate extends AbstractObject{

	public function searchCandidates(SearchCandidates $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->select('AT.ID_PROFIL, AT.ID_POLE, AT.PROFIL_CIVILITE, AT.PROFIL_NOM, AT.PROFIL_PRENOM, COMP_APPLICATIONS, COMP_INTERVENTIONS,
		 	COMP_COMPETENCE, AT.PARAM_TYPEDISPO, AT.PARAM_TYPESOURCE, AT.PARAM_SOURCE, AT.PARAM_DATEDISPO, AT.PARAM_MOBILITE,
		 	DT_TITRE, DT_LANGUES, DT_OUTILS, DT_DIPLOMES, DT_EXPERIENCE, DT_FORMATION, AT.PROFIL_DATE, AT.PROFIL_DATENAISSANCE,
		 	AT.PROFIL_DATEUPDATE, AT.PROFIL_VISIBILITE, AT.PROFIL_TEL1, AT.PROFIL_TEL2, AT.PROFIL_TEL3, AT.PROFIL_EMAIL,
		 	AT.PROFIL_EMAIL2, AT.PROFIL_EMAIL3, AT.PROFIL_ETAT, AT.PROFIL_STATUT, AT.PROFIL_ADR, AT.PROFIL_CP, AT.PROFIL_VILLE,
		 	AT.PROFIL_PAYS, AT.PROFIL_NATIONALITE, AT.PROFIL_SITUATION, AT.PARAM_TARIF1, AT.PARAM_TARIF2, AT.PARAM_TARIF3,
		 	AT.PARAM_DEVISEAGENCE, AT.PARAM_CHANGEAGENCE, AT.PARAM_DEVISE, AT.PARAM_CHANGE, AT.PARAM_CONTRAT, 
		 	RESP.ID_PROFIL AS RESP_ID, RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM,
		 	ID_CV, COUNT(DISTINCT ID_CV) AS NB_CV');

		$query->from(' TAB_PROFIL AT LEFT JOIN TAB_DT USING(ID_DT) LEFT JOIN TAB_REFERENCE USING(ID_DT)
			LEFT JOIN TAB_CV ON TAB_CV.ID_PROFIL=AT.ID_PROFIL
			LEFT JOIN TAB_USER USRESP ON(USRESP.ID_USER=AT.ID_RESPMANAGER) LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=USRESP.ID_PROFIL)');

		// join with states
		$oppStates = Services\Dictionary::getActiveStatesIDs('specific.setting.state.opportunity');
		$posStates = Services\Dictionary::getActiveStatesIDs('specific.setting.state.positioning');

		$posQuery = (new Query())->select('COUNT(DISTINCT ID_POSITIONNEMENT)')
			->from('TAB_AO')
			->addJoin('INNER JOIN TAB_POSITIONNEMENT ON(TAB_POSITIONNEMENT.ID_AO=TAB_AO.ID_AO)')
			->addWhere('ID_ITEM=AT.ID_PROFIL AND ITEM_TYPE=0');

		if($posStates) $posQuery->addWhere('POS_ETAT NOT IN ('.implode(',', array_map(function($v){ return intval($v); }, $posStates)).')');
		if($oppStates) $posQuery->addWhere('AO_ETAT NOT IN ('.implode(',', array_map(function($v){ return intval($v); }, $oppStates)).')');

		//$query->addJoin('LEFT JOIN TAB_POSITIONNEMENT POSA ON(POSA.ID_POSITIONNEMENT=('.$posQuery->getQuery().'))');
		$query->addColumns('('.$posQuery->getQuery().') AS NB_POS');

		$query->addWhere('AT.PROFIL_TYPE = ?', Models\Candidate::TYPE_CANDIDATE);
		$query->groupBy('AT.ID_PROFIL');
		//$query->setMapper(new SearchCandidate());

		$aoKeywords = $this->getListIdSearch( $filter->keywords->getValue(), ['AO'=>'TAB_AO.ID_AO']);
		if(!$aoKeywords->isEmpty()){
			$query->addWhere($aoKeywords);
			$query->addJoin(' INNER JOIN TAB_POSITIONNEMENT POSB ON(POSB.ID_ITEM=AT.ID_PROFIL AND POSB.ITEM_TYPE=0) INNER JOIN TAB_AO ON(TAB_AO.ID_AO=POSB.ID_AO)');
		}

		if($filter->returnHRManager->getValue() == 1){
			$query->addColumns('RH.ID_PROFIL AS RESPRH_ID, RH.PROFIL_NOM AS RESPRH_NOM, RH.PROFIL_PRENOM AS RESPRH_PRENOM');
		}
		$query->addJoin('LEFT JOIN TAB_USER USRH ON(USRH.ID_USER=AT.ID_RESPRH) LEFT JOIN TAB_PROFIL RH ON(RH.ID_PROFIL=USRH.ID_PROFIL)');

		switch($filter->perimeterManagersType->getValue()){
			case SearchCandidates::MANAGER_TYPE_RESP:
				$query->addWhere( $this->getPerimeterSearch($filter->getSelectedPerimeter(), 'AT.ID_SOCIETE', 'AT.ID_POLE', 'USRESP.ID_PROFIL', true, $filter->narrowPerimeter->getValue()) );
				break;
			case SearchCandidates::MANAGER_TYPE_HR:
				$query->addWhere( $this->getPerimeterSearch($filter->getSelectedPerimeter(), 'AT.ID_SOCIETE', 'AT.ID_POLE', 'AT.ID_PROFIL', true, $filter->narrowPerimeter->getValue()) );
				break;
			default:
				$query->addWhere( $this->getPerimeterSearch($filter->getSelectedPerimeter(), 'AT.ID_SOCIETE', 'AT.ID_POLE', ['USRH.ID_PROFIL', 'USRESP.ID_PROFIL'], true, $filter->narrowPerimeter->getValue()) );
		}

		switch($filter->getPeriodType()){
			case SearchCandidates::PERIOD_CREATED:
				$query->addWhere('DATE(AT.PROFIL_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchCandidates::PERIOD_AVAILABLE:
				$query->addWhere('PARAM_DATEDISPO BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchCandidates::PERIOD_UPDATED:
				$query->addWhere('AT.PROFIL_DATEUPDATE BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchCandidates::PERIOD_ACTIONS:
				if(!is_null($filter->getPeriodAction())) {
					$query->addJoin(' INNER JOIN TAB_ACTION');
					$query->addWhere('ACTION_TYPE=? AND  TAB_ACTION.ID_PARENT=AT.ID_PROFIL', $filter->getPeriodAction());
					$query->addWhere('DATE(ACTION_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				}
		}

		$query->addWhere( $this->getFilterSearch( $filter->candidateStates->getValue(), 'AT.PROFIL_ETAT'))
			  ->addWhere( $this->getFilterSearch( $filter->availabilityTypes->getValue(), 'AT.PARAM_TYPEDISPO'))
			  ->addWhere( $this->getFilterSearch( $filter->contractTypes->getValue(), 'AT.PARAM_CONTRAT'))
			  ->addWhere( $this->getFilterSearch( $filter->sources->getValue(), 'AT.PARAM_TYPESOURCE'))
			  ->addWhere( $this->getFilterSearch( $filter->evaluations->getValue(), 'AT.PROFIL_STATUT'))
			  ->addWhere( $this->getFilterSearch( $filter->trainings->getValue(), 'DT_FORMATION'))
			  ->addWhere( $this->getFilterSearch( $filter->experiences->getValue(), 'DT_EXPERIENCE'));

		$query
			  ->addWhere( $this->getFilterSearch( $filter->activityAreas->getValue(), 'COMP_APPLICATIONS', [], true))
			  ->addWhere( $this->getFilterSearch( $filter->expertiseAreas->getValue(), 'COMP_INTERVENTIONS', [], true))
			  ->addWhere( $this->getFilterSearch( $filter->languages->getValue(), 'DT_LANGUES', [], true))
			  ->addWhere( $this->getFilterSearch( $filter->mobilityAreas->getValue(), 'AT.PARAM_MOBILITE', [], true))
			  ->addWhere( $this->getFilterSearch( $filter->tools->getValue(), 'DT_OUTILS', [], true));

        //~ Show only if visible
        if($filter->visibleProfile->getValue() && ! $this->always_access) $query->addWhere('AT.PROFIL_VISIBILITE=1');

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_CANDIDATE.' AND TAB_FLAG.ID_PARENT=AT.ID_PROFIL)');
			$query->addWhere('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$keywords = $filter->keywords->getValue();
		if($keywords){
			$whereKW = $this->getListIdSearch($keywords, ['CAND' => 'AT.ID_PROFIL']);
			$query->addWhere($whereKW);

			if($whereKW->isEmpty() && $aoKeywords->isEmpty()){
				$keywords = str_replace('%', '\%', $filter->keywords->getValue());
				$keywords = str_replace('*', '%', $keywords);

				switch($filter->keywordsType->getValue()){
					case SearchCandidates::KEYWORD_TYPE_LASTNAME:
						$query->addWhere( 'AT.PROFIL_NOM LIKE ?', $keywords.'%');
						break;
					case SearchCandidates::KEYWORD_TYPE_FIRSTNAME:
						$query->addWhere( 'AT.PROFIL_PRENOM LIKE ?', $keywords.'%');
						break;
					case SearchCandidates::KEYWORD_TYPE_FULLNAME:
						$tabKeywords = explode('#', $keywords);
						$query->addWhere('AT.PROFIL_NOM LIKE ?', $tabKeywords[0].'%');
						$query->addWhere('AT.PROFIL_PRENOM LIKE ?', $tabKeywords[1].'%');
						break;
					case SearchCandidates::KEYWORD_TYPE_EMAILS:
						$tabEmail = explode(' ',$keywords);
						array_filter($tabEmail, function($value){
							return filter_var($value, FILTER_VALIDATE_EMAIL)!==false;
						});

						if(sizeof($tabEmail) > 0) {
							$email = (new Where('AT.PROFIL_EMAIL IN ?', $tabEmail))
								->or_('AT.PROFIL_EMAIL2 IN ?', $tabEmail)
								->or_('AT.PROFIL_EMAIL3 IN ?', $tabEmail);
							$query->addWhere($email);
						}
						break;
					case SearchCandidates::KEYWORD_TYPE_TITLE:
						$query->addWhere('MATCH(DT_TITRE, DT_OUTILS) AGAINST(?)', $keywords);
						break;
					case SearchCandidates::KEYWORD_TYPE_PHONES:
						$query->addWhere('AT.PROFIL_TEL1 LIKE ? OR AT.PROFIL_TEL2 LIKE ? OR AT.PROFIL_TEL3 LIKE ?', [$keywords.'%', $keywords.'%', $keywords.'%']);
						break;
					case SearchCandidates::KEYWORD_TYPE_RESUME:
						$query->addWhere('MATCH(CV_TEXT) AGAINST(?)', $keywords);
						$query->addColumns([ 'PERTINENCE'=> 'MATCH(CV_TEXT) AGAINST(' . $this->escape($keywords) . ')' ]);
						$query->addOrderBy('PERTINENCE DESC');
						break;
					case SearchCandidates::KEYWORD_TYPE_TD:
						$query->addWhere('(MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(?)' .
							' OR MATCH(DT_TITRE, DT_OUTILS) AGAINST(?)' .
							' OR MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(?)' .
							' OR AT.PROFIL_NOM LIKE ?' .
							' OR AT.PROFIL_PRENOM LIKE ?)', array($keywords, $keywords, $keywords, $keywords.'%', $keywords.'%'));
						$escapedKeywords = $this->escape($keywords);
						$query->addColumns([
							'PERTINENCE'=>
								'MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(' . $escapedKeywords . ')
								+ MATCH(DT_TITRE, DT_OUTILS) AGAINST(' . $this->escape($keywords) . ')
								+ MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(' . $escapedKeywords .')'
						]);
						$query->addOrderBy('PERTINENCE DESC');
						break;
					default:case SearchCandidates::KEYWORD_TYPE_RESUMETD:
						$query->addWhere('(MATCH(CV_TEXT) AGAINST(?)' .
							' OR MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(?)' .
							' OR MATCH(DT_TITRE, DT_OUTILS) AGAINST(?)' .
							' OR MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(?)' .
							' OR AT.PROFIL_NOM LIKE ?' .
							' OR AT.PROFIL_PRENOM LIKE ?)', array($keywords, $keywords, $keywords, $keywords, $keywords.'%', $keywords.'%'));
						$escapedKeywords = $this->escape($keywords);
						$query->addColumns([
							'PERTINENCE'=>
								'MATCH(CV_TEXT) AGAINST(' . $escapedKeywords .  ')
								+ MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(' . $escapedKeywords .  ')
								+ MATCH(DT_TITRE, DT_OUTILS) AGAINST(' . $escapedKeywords .  ')
								+ MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(' . $escapedKeywords .')'
						]);
						$query->addOrderBy('PERTINENCE DESC');
				}
			}
		}

        $this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY (après l'éventuel ORDER_BY PERTINENCE DESC)

		$this->f3->set('BMDATA.MYSQLSEARCH', true); // TODO: a quoi ca sert ?

		return $this->launchSearch($query);
	}

	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			SearchCandidates::ORDERBY_LASTNAME                      => 'AT.PROFIL_NOM',
			SearchCandidates::ORDERBY_TITLE                         => 'DT_TITRE',
			SearchCandidates::ORDERBY_AVAILABILITY                  => 'AT.PARAM_DATEDISPO',
			SearchCandidates::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS => 'NB_POS',
			SearchCandidates::ORDERBY_MAINMANAGER_LASTNAME          => 'RESP.PROFIL_NOM',
			SearchCandidates::ORDERBY_UPDATEDATE                    => 'AT.PROFIL_DATEUPDATE',
			SearchCandidates::ORDERBY_STATE                         => 'AT.PROFIL_ETAT',
			SearchCandidates::ORDERBY_EXPERIENCE                    => 'DT_EXPERIENCE',
			SearchCandidates::ORDERBY_CREATIONDATE                  => 'AT.PROFIL_DATE',
			SearchCandidates::ORDERBY_EVALUATION                    => 'AT.PROFIL_STATUT',
			SearchCandidates::ORDERBY_HRMANAGER_LASTNAME            => 'RH.PROFIL_NOM',
			SearchCandidates::ORDERBY_SOURCE                        => 'AT.PARAM_TYPESOURCE',
		];

		if(!$column) $query->addOrderBy('AT.PROFIL_DATEUPDATE DESC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy( $mapping[$c] .' '. $order);

		/*
		 * TODO: check the period case

		switch($filters_array['order_colonne']) {
			case 'disponibilite':
				if($filters_array['periode'] == '1')
					$order_expr = ($solr)
						?'PARAM_DATEDISPO '.$filters_array['order_type']
						:'AT.PARAM_DATEDISPO '.$filters_array['order_type'];
				else
					$order_expr = ($solr)
						?'PARAM_TYPEDISPO '.$filters_array['order_type']
						:'AT.PARAM_TYPEDISPO '.$filters_array['order_type'];
				break;
		}
		*/
	}

	/**
	 * Update a candidate
	 * @param array $data an array with the new values group by category (DT, NOTATIONS, REFERENCES, WEBSOCIALS)
	 * @param integer $id candidate ID
	 * @param integer $iddt TD ID
	 * @param boolean $solrSync trigger a SolR Sync
	 */
	public function updateObject($data, $id, $iddt = 0, $solrSync = true)
	{
		if($solrSync) $solr = SolR::instance();

		if(isset($data['DT'])) {
			if($iddt)
				$this->update('TAB_DT', $data['DT'], 'ID_DT=:id', [':id'=>$iddt]);
			else {
				$defaultDT = $this->guessDefaultValue('TAB_DT');
				foreach($defaultDT as $field => $default)
					if(!isset($data['DT'][$field])) $data['DT'][$field] = $default;

				$iddt = $this->insert('TAB_DT', $data['DT']);
			}
			$data['PROFIL']['ID_DT'] = $iddt;
		}

		if(isset($data['NOTATIONS'])) {
			//On supprime les notations qui n'existent plus
			$ids = array_column($data['NOTATIONS'], 'ID_NOTATION');
			if($ids) {
				$this->delete('TAB_NOTATION', 'ID_PROFIL = ? AND ID_NOTATION NOT IN (' . Where::prepareWhereIN($ids) . ')', array_merge([$id] , $ids));
			} else {
				$this->delete('TAB_NOTATION', 'ID_PROFIL = ?', $id);
			}

			//On met à jour les réseaux sociaux de facturation
			foreach($data['NOTATIONS'] as $notation) {
				if(!isset($notation['ID_NOTATION'])) //Il s'agit d'une nouvelle coordonnnées
					$this->insert('TAB_NOTATION', array_merge($notation,[
						'NOTATION_DATE' => date('Y-m-d'),
						'ID_PROFIL' => $id
					]));
				else
					$this->update('TAB_NOTATION', $notation, 'ID_NOTATION=:id AND ID_PROFIL = :parent', ['id'=>$notation['ID_NOTATION'], 'parent'=>$id]);
			}
		}
		if(isset($data['REFERENCES']) && $iddt) {
			//On met à jour les références du DT
			$ids = array_column($data['REFERENCES'], 'ID_REFERENCE');
			if($ids) {
				$this->delete('TAB_REFERENCE', 'ID_DT = ?  AND ID_REFERENCE NOT IN (' . Where::prepareWhereIN($ids) . ')', array_merge([$iddt] , $ids));
			}else{
				$this->delete('TAB_REFERENCE', 'ID_DT = ?', $iddt);
			}

			foreach($data['REFERENCES'] as $reference)
				if(isset($reference['ID_REFERENCE'])) {
					$queryParams = [ ':id' => $reference['ID_REFERENCE'], ':iddt' => $iddt];
					$this->update('TAB_REFERENCE', $reference, 'ID_REFERENCE=:id AND ID_DT=:iddt', $queryParams);
				}else {
					$reference['ID_DT'] = $iddt;
					$this->insert('TAB_REFERENCE', $reference);
				}
		}

		// on met à jour les réseaux sociaux
		//~ - passer un tableau vide pour supprimer tous les réseaux
		//~ - passer une url vide pour supprimer le réseau correspondant (même comportement que sur l'ui v6)
		//~ - passer le réseau et une url valide pour l'ajouter ou le modifier
		if(isset($data['WEBSOCIALS'])) {
			$existingsNetworks = $this->exec('SELECT WEBSOCIAL_NETWORK FROM TAB_WEBSOCIAL WHERE ID_PARENT = ? AND WEBSOCIAL_TYPE = ?', [$id, Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE]);
			$existing_networks = Tools::getFieldsToArray($existingsNetworks ,'WEBSOCIAL_NETWORK');
			if($networks = array_column($data['WEBSOCIALS'], 'WEBSOCIAL_NETWORK')) {
				foreach($data['WEBSOCIALS'] as $data_social) {
					if(empty($data_social['WEBSOCIAL_URL'])){
						$this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ? AND WEBSOCIAL_NETWORK = ?',
						[$id, Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE, $data_social['WEBSOCIAL_NETWORK']]);
					} else if(in_array($data_social['WEBSOCIAL_NETWORK'], $existing_networks)) {
						$this->update('TAB_WEBSOCIAL', $data_social,
							'ID_PARENT = :parent AND WEBSOCIAL_TYPE = :type AND WEBSOCIAL_NETWORK = :network',
							['parent' => $id, 'type' => Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE, 'network' => $data_social['WEBSOCIAL_NETWORK']]);
					} else
						$this->insert('TAB_WEBSOCIAL', array_merge($data_social,[
							'WEBSOCIAL_TYPE' => Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE,
							'ID_PARENT' => $id
						]));
				}
			} else $this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ?', [$id, Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE]);
		}

		$data['PROFIL']['PROFIL_DATEUPDATE'] = date('Y-m-d H:i:s');
		if(isset($data['PROFIL']['PROFIL_DATE'])) $data['PROFIL']['PROFIL_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['PROFIL']['PROFIL_DATE'])->format('Y-m-d H:i:s');
		$this->update('TAB_PROFIL', $data['PROFIL'], 'ID_PROFIL=:id', [':id' => $id]);

		if(isset($solr)) {
			$solr->Modification(BM::SOLR_MODULE_CANDIDATES);
		}
	}

	/**
	 * delete all data related to a candidate
	 * @param int $id candidate ID
	 * @param int $iddt candidate ID_DT
	 * @param bool $solr perform a SolR sync
	 * @throws \Exception
	 */
	public function deleteCandidatData($id, $iddt = 0, $solr = true)
	{
		//On supprime les paramètres, le DT, la compétence et les références du profil
		$this->delete('TAB_DT', 'ID_DT=?', $iddt);
		$this->delete('TAB_REFERENCE', 'ID_DT=?', $iddt);
		$this->delete('TAB_NOTATION', 'ID_PROFIL=?', $id);

		//On supprime les réseaux sociaux de la société
		$this->delete('TAB_WEBSOCIAL', 'ID_PARENT=? AND WEBSOCIAL_TYPE=0', $id);

		//On supprime les actions associés à ce profil
		$dict = Services\Actions::getDictionaryForCategory(BM::CATEGORY_CANDIDATE);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 0));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//On supprime tous les CV et les pièces jointes de cette ressource
		$dbFile = new File();
		$dbFile->deleteAllObjects( File::TYPE_RESOURCE_RESUME, $id);
		$dbFile->deleteAllObjects( File::TYPE_OTHER, $id);

		//On supprime tous les positionnements de la ressource
		$this->delete('TAB_POSITIONNEMENT', 'ID_RESPPROFIL=?',$id);

		//On supprime tous les projets en Recrutement de ce candidat
		$dbProject = new Project();
		$projets = $this->exec(
			'SELECT ID_PROJET FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE ID_ITEM=? AND ITEM_TYPE=0 AND PRJ_TYPE=? GROUP BY ID_PROJET',
			[$id, BM::PROJECT_TYPE_RECRUITMENT]
		);
		foreach($projets as $projet){
			$dbProject->deleteProjetData($projet->ID_PROJET, $solr);
		}

		//On supprime tous les flags de cette action
		$dbFlag = new Flag();
		$dbFlag->removeAllFlagsFromEntity($id, Models\Flag::TYPE_CANDIDATE);

		//On supprime le profil
		$this->delete('TAB_PROFIL', 'ID_PROFIL=?', $id);

		//On indique la suppression dans la table TAB_DELETED
		$this->insert('TAB_DELETED', ['ID_PARENT' => $id, 'DEL_TYPE' => 8, 'DEL_DATE' => date('Y-m-d H:i:s')]);

		if($solr) SolR::instance()->Suppression(BM::SOLR_MODULE_CANDIDATES);
	}

	/**
	 * get a candidate from an ID
	 * @param int $id candidate ID
	 * @param int $tab TAB ID
	 * @return Models\Candidate|false
	 * @throws \Exception
	 */
	public function getObject($id, $tab= Models\Candidate::TAB_DEFAULT)
	{
		$baseQuery = new Query();
		$baseQuery->select(
			'TAB_PROFIL.ID_PROFIL, TAB_PROFIL.ID_SOCIETE, PROFIL_NOM, PROFIL_PRENOM, PROFIL_CIVILITE, PROFIL_DATE, PROFIL_DATEUPDATE, 
			ID_RESPMANAGER, US.ID_PROFIL AS ID_PROFIL_RESPMANAGER, USRH.ID_PROFIL AS ID_PROFIL_RESPRH, ID_RESPRH, 
			ID_CV, COUNT(ID_CV) AS NB_CV, PROFIL_VISIBILITE, TAB_PROFIL.ID_POLE'
		)
		->from('TAB_PROFIL')
		->addJoin('LEFT JOIN TAB_CV USING(ID_PROFIL)')
		->addJoin('LEFT JOIN TAB_USER AS US ON US.ID_USER=ID_RESPMANAGER')
		->addJoin('LEFT JOIN TAB_USER AS USRH ON USRH.ID_USER=ID_RESPRH')
		->addWhere('TAB_PROFIL.ID_PROFIL=?', $id)
		->addWhere('TAB_PROFIL.PROFIL_TYPE = ?', Models\Candidate::TYPE_CANDIDATE) // candidat
		->groupBy('TAB_PROFIL.ID_PROFIL');

		switch($tab) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Candidate::TAB_INFORMATION:
				$baseQuery->addColumns('PROFIL_TYPE, PROFIL_STATUT, PROFIL_DATENAISSANCE, PROFIL_EMAIL,
						PROFIL_EMAIL2, PROFIL_EMAIL3, PROFIL_TEL1, PROFIL_TEL2, PROFIL_TEL3, PROFIL_FAX, PROFIL_ADR, PROFIL_CP,
						PROFIL_VILLE, PROFIL_PAYS, ID_DT, PROFIL_ETAT, PARAM_TYPESOURCE, PARAM_SOURCE, PARAM_COMMENTAIRE, PARAM_MOBILITE,
						PARAM_TYPEDISPO, PARAM_DATEDISPO, ID_EMBAUCHE AS ID_PROFILCONSULTANT'
					)->addJoin('LEFT JOIN TAB_DT USING(ID_DT)');
				$result = $this->singleExec($baseQuery);

				if($result) {
					//On récupère toutes les notations
					$sql = 'SELECT ID_NOTATION, NOTATION_DATE, NOTATION_NOTES, NOTATION_COMMENTAIRE, TAB_USER.ID_PROFIL
                            FROM TAB_NOTATION INNER JOIN TAB_USER ON(ID_USER=ID_RESPUSER)
                            WHERE TAB_NOTATION.ID_PROFIL=? ORDER BY ID_NOTATION ASC';
					$result['NOTATIONS'] = $this->exec($sql, $id);

					//On récupère les CVs
					$sql = 'SELECT ID_CV, CV_NAME FROM TAB_CV WHERE ID_PROFIL=? ORDER BY ID_CV ASC';
					$result['CVs'] = $this->exec($sql, $id);

					// On récupère les infos relatives aux réseaux sociaux
					$sql = 'SELECT ID_WEBSOCIAL, WEBSOCIAL_NETWORK, WEBSOCIAL_URL FROM TAB_WEBSOCIAL WHERE WEBSOCIAL_TYPE=0 AND ID_PARENT=? ';
					$result['WEBSOCIALS'] = $this->exec($sql, $id);
				}
				return $result;
			case Models\Candidate::TAB_ADMINISTRATIVE:

				$baseQuery->addColumns(
					'PROFIL_DATENAISSANCE, PROFIL_LIEUNAISSANCE, PROFIL_NATIONALITE, PROFIL_SITUATION,
					PROFIL_NUMSECU, ID_EMBAUCHE AS ID_PROFILCONSULTANT, PARAM_CHANGE, PARAM_DEVISE, PARAM_CHANGEAGENCE,
					PARAM_DEVISEAGENCE, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, TAB_CRMSOCIETE.ID_CRMSOCIETE,
					CSOC_SOCIETE, PARAM_TARIF2, PARAM_TARIF1, PARAM_TARIF3, PARAM_CONTRAT, PARAM_COMMENTAIRE2, ID_CONTRAT'
				)->addJoin('LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT)')
				->addJoin('LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROFIL.ID_CRMSOCIETE)')
				->addJoin('LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_PROFIL.ID_PROFIL)');

				$result = $this->singleExec($baseQuery);

				if($result) {
					//On récupère les pièces jointes
					$sql = 'SELECT ID_FILE, FILE_NAME FROM TAB_FILE WHERE ID_PROFIL=? ORDER BY ID_FILE ASC;';
					$result['FILES'] = $this->exec($sql, $id);
				}
				return $result;
			case Models\Candidate::TAB_TD:
				$baseQuery->addColumns('
					PROFIL_TYPE, PROFIL_STATUT, PROFIL_EMAIL, PROFIL_TEL1, PROFIL_DATENAISSANCE, 
					COMP_COMPETENCE, COMP_APPLICATIONS, COMP_INTERVENTIONS,
					DT_LANGUES, DT_TITRE, DT_OUTILS, DT_DIPLOMES, DT_EXPERIENCE, DT_FORMATION, ID_DT, PARAM_MOBILITE,
					PARAM_TYPEDISPO, PARAM_DATEDISPO'
				)->addJoin('LEFT JOIN TAB_DT USING(ID_DT)');

				$result = $this->singleExec($baseQuery);
				if($result){
					$sql = 'SELECT ID_REFERENCE, REF_TITRE, REF_DESCRIPTION FROM TAB_REFERENCE WHERE ID_DT=? ORDER BY ID_REFERENCE ASC';
					$result['REFERENCES'] = $this->exec($sql, $result->ID_DT, Models\Reference::class);
				}
				return $result;
			case Models\Candidate::TAB_ACTIONS:
				$result = $this->singleExec($baseQuery);
				throw new \Exception('do migration'); // TODO
				if($result) {
					$bourseBDD = new BoondManager_ObjectBDD_Recherche_Actions($this->db);
					$filters_array = $bourseBDD->getDefaultFilters(false);
					$filters_array['perimetre'] = '';
					$filters_array['keywords'] = 'CAND'.$result['ID_PROFIL'];
					$filters_array['order_colonne'] = 'date';
					$filters_array['order_type'] = 'desc';
					$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
					$filters_array['type_rappel'] = '0';//On recherche les actions des candidats
					if(isset($tabFilters['mc']['liste_rappel']) && is_array($tabFilters['mc']['liste_rappel'])) $filters_array['mc']['liste_rappel'] = $tabFilters['mc']['liste_rappel']; else foreach(Wish_Session::getInstance()->get('login_clefactionscandidat') as $idaction => $action) $filters_array['mc']['liste_rappel'][] = $idaction;
					$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
					$result['NB_ACTIONS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
					$result['ACTIONS'] = ($search)?$bourseBDD->get('RESULTS'):array();

					//On initialise la navigation de la vue
					if($search)$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-candidat', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_PROFIL'].'&onglet=2&'.setURLMultiCriteria('lr', $filters_array['mc']['liste_rappel']), 'actions_');
					$tabFilters = $filters_array;
				}
				return $result;
			case Models\Candidate::TAB_POSITIONINGS:
                $result = $this->singleExec($baseQuery);
                if($result) {//~ À supprimer plus tard si appel via candidats/id/positionings direct
                    $filter = \BoondManager\APIs\Positionings\Filters\SearchPositionings::getUserFilter();
                    $filter->keywords->setDefaultValue('CAND'.$result->ID_PROFIL);
                    $filter->setIndifferentPerimeter();
                    $filter->narrowPerimeter->setValue(0);
                    $filter->perimeterManagersType->setDefaultValue(\BoondManager\APIs\Positionings\Filters\SearchPositionings::PERIMETER_PROFILES);
                    $filter->filter();

                    $positionings = Services\Positionings::search($filter);
					$result['NB_POSITIONINGS'] = $positionings->total;
					$result['POSITIONINGS'] = $positionings->rows;
                }
				return $result;
                break;
			default: return $this->singleExec($baseQuery);
		}
	}

	/**
	 * create a new Candidate
	 * @param array $tabProfil new data group by category (PROFIL, DT, REFERENCES, NOTATIONS, CV)
	 * @param bool $copy duplicate or move files (resumes)
	 * @param bool $solr trigger a SolR Sync
	 * @return bool|int the new candidate ID
	 */
	public function newCandidatData($tabProfil, $copy=false, $solr = true)
	{
		if(isset($tabProfil['PROFIL'])) {

			if(isset($tabProfil['DT'])) {
				$defaultDT = $this->guessDefaultValue('TAB_DT');
				foreach($defaultDT as $field => $default)
					if(!isset($tabProfil['DT'][$field])) $tabProfil['DT'][$field] = $default;

				$iddt = $this->insert('TAB_DT', $tabProfil['DT']);
				$tabProfil['PROFIL']['ID_DT'] = $iddt;

				//On met à jour les références du DT
				foreach($tabProfil['REFERENCES'] as $reference) {
					$reference['ID_DT'] = $iddt;
					$this->insert('TAB_REFERENCE', $reference);
				}
			}

			$tabProfil['PROFIL']['PROFIL_TYPE'] = Models\Candidate::TYPE_CANDIDATE;
			$defaultProfil = $this->guessDefaultValue('TAB_PROFIL');
			foreach($defaultProfil as $field => $default)
				if(!isset($tabProfil['PROFIL'][$field])) $tabProfil['PROFIL'][$field] = $default;

			$idProfil = $this->insert('TAB_PROFIL', $tabProfil['PROFIL']);

			//On créé les notations
			if($idProfil && isset($tabProfil['NOTATIONS']) && is_array($tabProfil['NOTATIONS']) ) {
				foreach($tabProfil['NOTATIONS'] as $data_notation) {
					$data_notation['ID_PROFIL'] = $idProfil;
					$this->insert('TAB_NOTATION', $data_notation);
				}
			}

			if($idProfil && isset($tabProfil['CVs']) && is_array($tabProfil['CVs'])) {
				$dbFile = new File();
				foreach($tabProfil['CVs'] as $cv)
					$dbFile->setObjectFromParent(File::TYPE_CANDIDATE_RESUME, $cv['CV_FILE'], $cv['CV_NAME'], $cv['CV_TEXT'], $idProfil, $copy);
			}

			// On ajoute les réseaux sociaux
			if($idProfil && isset($tabProfil['WEBSOCIALS']) && is_array($tabProfil['WEBSOCIALS'])) {
				foreach($tabProfil['WEBSOCIALS'] as $data_social)
					$data_social['ID_PARENT'] = $idProfil;
					$data_social['WEBSOCIAL_TYPE'] = Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE;
					$this->insert('TAB_WEBSOCIAL', $data_social);
			}

			if($solr)
				SolR::instance()->Creation(BM::SOLR_MODULE_CANDIDATES);
			return $idProfil;
		}
		return false;
	}

	/**
	 * create or update a candidate
	 * @param array $data new data group by category (PROFIL, DT, REFERENCES, NOTATIONS, CV)
	 * @param int $id candidate ID
	 * @param int $iddt TD ID
	 * @param bool $copy import resume (if new candidate)
	 * @param bool $solr trigger a SolR sync
	 * @return int the candidate ID
	 */
	public function setObject($data, $id = 0, $iddt = 0, $copy = false, $solr = true){
		if($id) $this->updateObject($data, $id, $iddt, $solr);
		else $id = $this->newCandidatData($data, $copy, $solr);
		return $id;
	}

	/**
	 * transform a candidate in a resource
	 * @param int $id candidate ID
	 * @param bool $importResume copy the candidate resumes
	 * @return int the new resource ID
	 * @throws \Exception
	 */
	public function ConvertCandidatToRessource($id, $importResume=false)
	{
		//throw new \Exception('do migration'); // TODO
		$resourceData = array();
		$dbResource = new Employee();
		//On récupère les données de l'onglet Informations
		if($data = $this->getObject($id)) {
			$resourceData['PROFIL']['PROFIL_CIVILITE'] = $data['PROFIL_CIVILITE'];
			$resourceData['PROFIL']['PROFIL_NOM'] = $data['PROFIL_NOM'];
			$resourceData['PROFIL']['PROFIL_PRENOM'] = $data['PROFIL_PRENOM'];
			$resourceData['PROFIL']['PROFIL_TEL1'] = $data['PROFIL_TEL1'];
			$resourceData['PROFIL']['PROFIL_TEL2'] = $data['PROFIL_TEL2'];
			$resourceData['PROFIL']['PROFIL_TEL3'] = $data['PROFIL_TEL3'];
			$resourceData['PROFIL']['PROFIL_EMAIL'] = $data['PROFIL_EMAIL'];
			$resourceData['PROFIL']['PROFIL_EMAIL2'] = $data['PROFIL_EMAIL2'];
			$resourceData['PROFIL']['PROFIL_EMAIL3'] = $data['PROFIL_EMAIL3'];
			$resourceData['PROFIL']['PROFIL_FAX'] = $data['PROFIL_FAX'];
			$resourceData['PROFIL']['PROFIL_CP'] = $data['PROFIL_CP'];
			$resourceData['PROFIL']['PROFIL_ADR'] = $data['PROFIL_ADR'];
			$resourceData['PROFIL']['PROFIL_VILLE'] = $data['PROFIL_VILLE'];
			$resourceData['PROFIL']['PROFIL_PAYS'] = $data['PROFIL_PAYS'];

			$resourceData['PROFIL']['PROFIL_ETAT'] = true;
			$resourceData['PROFIL']['ID_RESPMANAGER'] = $data['ID_RESPMANAGER'];
			$resourceData['PROFIL']['ID_RESPRH'] = $data['ID_RESPRH'];
			$resourceData['PROFIL']['ID_SOCIETE'] = $data['ID_SOCIETE'];
			if(isset($data['PROFIL_DATENAISSANCE']))
				$resourceData['PROFIL']['PROFIL_DATENAISSANCE'] = $data['PROFIL_DATENAISSANCE'];
			else
				$resourceData['PROFIL']['PROFIL_DATENAISSANCE'] = null;

			$resourceData['PROFIL']['PARAM_MOBILITE'] = $data['PARAM_MOBILITE'];
			$resourceData['PROFIL']['PARAM_TYPEDISPO'] = Models\Employee::DISPO_ASAP;
			$resourceData['PROFIL']['PARAM_DATEDISPO'] = date('Y-m-d');

			//On importe le CV
			$ged = new GED();
			$dbFile = new File();
			if($importResume && isset($data['CVs'])) {
				$resourceData['CVs'] = array();
				foreach($data['CVs'] as $cv) {
					$resume = $dbFile->getFileData(File::TYPE_CANDIDATE_RESUME, $cv['ID_CV']);
					$file = $ged->getFile(File::TYPE_CANDIDATE_RESUME, $resume['ID_CV']);
					if($file) {
						$filetemp = tempnam(\Base::instance()->get('MAIN_ROOTPATH').'/../tmp', 'CV');
						copy($file[0], $filetemp);
						$resourceData['CVs'][] = array('CV_FILE' => $filetemp, 'CV_NAME' => $resume['CV_NAME'], 'CV_TEXT' => $resume['CV_TEXT']);
					}
				}
			}
		}

		//On récupère les données de l'onglet Administratif
		if($data = $this->getObject($id, Models\Candidate::TAB_ADMINISTRATIVE)) {
			$resourceData['PROFIL']['PROFIL_LIEUNAISSANCE'] = $data['PROFIL_LIEUNAISSANCE'];
			$resourceData['PROFIL']['PROFIL_NUMSECU'] = $data['PROFIL_NUMSECU'];
			if(isset($data['PROFIL_DATENAISSANCE']))
				$resourceData['PROFIL']['PROFIL_DATENAISSANCE'] = $data['PROFIL_DATENAISSANCE'];
			else $resourceData['PROFIL']['PROFIL_DATENAISSANCE'] = null;

		}

		//On récupère les données de l'onglet DT
		if($data = $this->getObject($id, Models\Candidate::TAB_TD)) {
			$resourceData['DT']['DT_TITRE'] = $data['DT_TITRE'];
			$resourceData['DT']['DT_EXPERIENCE'] = $data['DT_EXPERIENCE'];
			$resourceData['DT']['DT_FORMATION'] = $data['DT_FORMATION'];
			$resourceData['DT']['DT_LANGUES'] = $data['DT_LANGUES'];
			$resourceData['DT']['DT_DIPLOMES'] = $data['DT_DIPLOMES'];
			$resourceData['DT']['COMP_COMPETENCE'] = $data['COMP_COMPETENCE'];
			$resourceData['DT']['COMP_APPLICATIONS'] = $data['COMP_APPLICATIONS'];
			$resourceData['DT']['COMP_INTERVENTIONS'] = $data['COMP_INTERVENTIONS'];
			if(isset($data['REFERENCES'])) $resourceData['REFERENCES'] = $data['REFERENCES'];
		}

		$idProfil = $dbResource->setObject($resourceData, 0, 0, true);
		if($idProfil) $this->setObject(['PROFIL' => ['ID_EMBAUCHE' => $idProfil]], $id);//On établit la relation entre le candidat et la ressource créée
		return $idProfil;
	}

	/**
	 * \brief Importe un Candidat
	 * @param <type> $xmlData flux XML contenant les données du candidat
	 * @param <type> $idmanager identifiant du responsable du profil à importer
	 * @param <type> $file flux binaire contenant les données des fichiers à importer
	 */
	public function ImportWSCandidat(Wish_XMLData $xmlData, $iduser=0, $idsociete=0, $urlweb = '', $file=null)
	{
		throw new \Exception('do migration'); // TODO
		$id_newprofil = 0;
		$fileGED = new BoondManager_FileGED();
		$fileData = new BoondManager_ObjectBDD_File($this->db, $fileGED);

		if( $xmlData->getTagString('/webservice/param/nom') == '' ) throw new Exception('Nom non défini', 6);
		if( $xmlData->getTagString('/webservice/param/prenom') == '' ) throw new Exception('Prénom non défini', 7);

		//On construit les états candidats définis par ce groupe
		$liste_etats = array();
		$xmlEtatsDefault = new Wish_XMLData(APPLICATION_PATH.'/languages/fr','/common/configuration/parametres.xml'); //on déclenche une exception si le fichier de langue n'existe pas
		$xmlEtats = new Wish_XMLData(APPLICATION_PATH.'/languages/fr');
		if($urlweb != '' && $xmlEtats->loadXMLFile('/PAS/'.$urlweb.'/common/configuration/parametres.xml')) $liste_etats = $xmlEtats->getTagListe('/configuration/etats/candidat');
		if(sizeof($liste_etats) == 0) $liste_etats = $xmlEtatsDefault->getTagListe('/configuration/etats/candidat');

		$societeBDD = new BoondManager_ObjectBDD_Societe($this->db);
		$societeBDD->getConfigSociete($idsociete);
		$configGroupe = $societeBDD->get('CONFIGSOCIETE');

		$newCandidat = array();
		$newCandidat['PROFIL']['PROFIL_ETAT'] = 0;
		foreach($liste_etats as $id => $etat) {if($id == $xmlData->getTagString('/webservice/param/etape')) {$newCandidat['PROFIL']['PROFIL_ETAT'] = $id;break;}}
		$newCandidat['PROFIL']['ID_RESPMANAGER'] = $iduser;
		$newCandidat['PROFIL']['ID_SOCIETE'] = $idsociete;
		$newCandidat['PROFIL']['PROFIL_TYPE'] = PROFIL_CANDIDAT;
		$newCandidat['PROFIL']['PROFIL_NOM'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/nom'));
		$newCandidat['PROFIL']['PROFIL_PRENOM'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/prenom'));
		$newCandidat['PROFIL']['PROFIL_EMAIL'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/email1'));
		$newCandidat['PROFIL']['PROFIL_EMAIL2'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/email2'));
		$newCandidat['PROFIL']['PROFIL_EMAIL3'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/email3'));
		$newCandidat['PROFIL']['PROFIL_TEL1'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/tel1'));
		$newCandidat['PROFIL']['PROFIL_TEL2'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/tel2'));
		$newCandidat['PROFIL']['PROFIL_TEL3'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/tel3'));
		$newCandidat['PROFIL']['PROFIL_FAX'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/fax'));
		$newCandidat['PROFIL']['PROFIL_ADR'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/adresse'));
		$newCandidat['PROFIL']['PROFIL_CP'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/codepostal'));
		$newCandidat['PROFIL']['PROFIL_VILLE'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/ville'));
		$newCandidat['PROFIL']['PROFIL_PAYS'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/pays'));
		$newCandidat['PROFIL']['PROFIL_CIVILITE'] = intval($xmlData->getTagString('/webservice/param/civilite')); //(string) "1", "2" ou "0" (défaut)
		$newCandidat['PROFIL']['PROFIL_STATUT'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/evaluation'));
		$newCandidat['PROFIL']['PROFIL_DATENAISSANCE'] = convertDate($xmlData->getTagString('/webservice/param/datenaissance'), false, 'NULL');
		$newCandidat['PROFIL']['PROFIL_DATE'] = convertDate($xmlData->getTagString('/webservice/param/datecreation'), false, date('Y-m-d', mktime())).' '.date('H:i:s', mktime());
		$newCandidat['PROFIL']['PROFIL_DATEUPDATE'] = convertDate($xmlData->getTagString('/webservice/param/datemaj'), false, date('Y-m-d', mktime())).' '.date('H:i:s', mktime());
		$newCandidat['PROFIL']['PARAM_COMMENTAIRE'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/commentaire'));
		$newCandidat['PROFIL']['PARAM_TARIF1'] = floatval($xmlData->getTagString('/webservice/param/salaire/actuel'));
		$newCandidat['PROFIL']['PARAM_TARIF2'] = floatval($xmlData->getTagString('/webservice/param/salaire/souhaite'));
		if(isset($configGroupe['GRPCONF_DEVISE'])) {
			$newCandidat['PROFIL']['PARAM_DEVISE'] = $configGroupe['GRPCONF_DEVISE'];
			$newCandidat['PROFIL']['PARAM_DEVISEAGENCE'] = $configGroupe['GRPCONF_DEVISE'];
		}
		$newCandidat['PROFIL']['PARAM_CHANGE'] = 1;
		if(isset($configGroupe['GRPCONF_CHANGE'])) $newCandidat['PROFIL']['PARAM_CHANGEAGENCE'] = $configGroupe['GRPCONF_CHANGE'];
		$newCandidat['PROFIL']['PARAM_TYPEDISPO'] = intval($xmlData->getTagString('/webservice/param/disponibilite/type'));//(int) 0, 1, 2, 3 ou 4 ou 9
		$newCandidat['PROFIL']['PARAM_TYPESOURCE'] = intval($xmlData->getTagString('/webservice/param/source/type'));
		$newCandidat['PROFIL']['PARAM_SOURCE'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/source/provenance'));
		if(intval($xmlData->getTagString('/webservice/param/disponibilite/type')) == 9) $newCandidat['PROFIL']['PARAM_DATEDISPO'] = convertDate($xmlData->getTagString('/webservice/param/disponibilite/date'), false, date('Y-m-d',mktime()));

		$newCandidat['DT']['DT_FORMATION'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/formation'));
		$newCandidat['DT']['DT_TITRE'] = str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/titre'));
		$newCandidat['DT']['DT_EXPERIENCE'] = intval($xmlData->getTagString('/webservice/param/experience'));

		//On traite les diplômes (Max. 5)
		if(sizeof($xmlData->getTagListe('/webservice/param/diplome')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/diplome')) <= 5) {
			$tabDiplome = array();
			foreach($xmlData->getTagListe('/webservice/param/diplome') as $diplome) $tabDiplome[] = str_replace('|','',$diplome);
			$newCandidat['DT']['DT_DIPLOMES'] = convertArray($tabDiplome, false);
		}

		//On traite les langues (Max 5.)
		if(sizeof($xmlData->getTagListe('/webservice/param/langue')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/langue')) <= 5) {
			$tabLangue = array();
			foreach($xmlData->getTagListe('/webservice/param/langue') as $langue) $tabLangue[str_replace(array('|','#'),array('',''),$langue->label)] = str_replace(array('|','#'),array('',''),$langue->niveau);
			$newCandidat['DT']['DT_LANGUES'] = convertDoubleArray($tabLangue, false);
		}

		//On traite les outils (Max 10.)
		if(sizeof($xmlData->getTagListe('/webservice/param/outil')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/outil')) <= 10) {
			$tabOutil = array();
			foreach($xmlData->getTagListe('/webservice/param/outil') as $outil) $tabOutil[str_replace(array('|','#'),array('',''),$outil->value)] = str_replace(array('|','#'),array('',''),$outil->niveau);
			$newCandidat['DT']['DT_OUTILS'] = convertDoubleArray($tabOutil, false);
		}

		//On traite les domaines d'application (Max 10.)
		if(sizeof($xmlData->getTagListe('/webservice/param/application')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/application')) <= 10) {
			$tabApplication = array();
			foreach($xmlData->getTagListe('/webservice/param/application') as $application) $tabApplication[] = str_replace('|','',$application);
			$newCandidat['DT']['COMP_APPLICATIONS'] = convertArray($tabApplication, false);
		}

		//On traite les domaines d'intervention (Max 10.)
		if(sizeof($xmlData->getTagListe('/webservice/param/intervention')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/intervention')) <= 10) {
			$tabIntervention = array();
			foreach($xmlData->getTagListe('/webservice/param/intervention') as $intervention) $tabIntervention[] = str_replace('|','',$intervention);
			$newCandidat['DT']['COMP_INTERVENTIONS'] = convertArray($tabIntervention, false);
		}

		//On traite la mobilité (Max 5.)
		if(sizeof($xmlData->getTagListe('/webservice/param/mobilite')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/mobilite')) <= 5) {
			$tabMobilite = array();
			foreach($xmlData->getTagListe('/webservice/param/mobilite') as $mobilite) $tabMobilite[] = str_replace('|','',$mobilite);
			$newCandidat['PROFIL']['PARAM_MOBILITE'] = convertArray($tabMobilite, false);
		}

		//On traite les références (Max 7.)
		if(sizeof($xmlData->getTagListe('/webservice/param/reference')) > 0 && sizeof($xmlData->getTagListe('/webservice/param/reference')) <= 7) {
			$newCandidat['REFERENCES'] = array();
			foreach($xmlData->getTagListe('/webservice/param/reference') as $reference) $newCandidat['REFERENCES'][] = array(0,str_replace(strtolower('null'),'',$reference->titre), str_replace(strtolower('null'),'',$reference->description));
		}

		//On crée le profil du candidat
		$id_newprofil = $this->newCandidatData($newCandidat);

		if($xmlData->getTagString('/webservice/param/note') != '') {
			$actionBDD = new BoondManager_ObjectBDD_Action($this->db);
			//On ajoute la Note/Action
			$newAction = array('ACTION_DATE' => date('Y-m-d H:i:s', mktime()),
				'ACTION_TEXTE' => str_replace(strtolower('null'),'',$xmlData->getTagString('/webservice/param/note')),
				'ACTION_TYPE' => ACTION_CANDIDAT,
				'ID_PARENT' => $id_newprofil,
				'ID_RESPUSER' => $userBDD->get('ID_USER'));
			$actionBDD->newActionData($newAction);
		}

		//On récupère la limite de définition des fichiers à importer
		if(isset($file) && $xmlData->getTagString('/webservice/param/boundary') != '') {
			$boundary = $xmlData->getTagString('/webservice/param/boundary');
			$files = explode($boundary, $file);

			//On ajoute le CV
			$k=0;
			//On construit les CV uniquement si elle existe (au max. 5)
			foreach($xmlData->getTagListe('/webservice/param/cv') as $j => $cv) {
				if($j < 5 && isset($files[$k]) && sizeof($files[$k]) <= 8192000) {
					$tempnam = tempnam(APPLICATION_PATH.'/../tmp','CVIMPORT');
					if(file_put_contents($tempnam, $files[$k])) {
						$filtertext = $fileGED->filterDocumentFile($tempnam, $cv->name, $tempnam);
						if(!isset($filtertext['CV_PATH']) || !isset($filtertext['CV_NAME']) || !isset($filtertext['CV_TEXT']) || !$fileData->newFileData(FILE_CVCANDIDAT, $filtertext['CV_PATH'], $filtertext['CV_NAME'], $filtertext['CV_TEXT'], $id_newprofil, true)) {
							$this->deleteCandidatData($id_newprofil);
							throw new Exception('Erreur dans le téléchargement des CVs', 2);
						}
					} else unlink($tempnam);
				}
				$k++;
			}

			//On construit les PJ uniquement si elle existe (au max. 10)
			foreach($xmlData->getTagListe('/webservice/param/pj') as $j => $pj) {
				if($j < 10 && isset($files[$k]) && sizeof($files[$k]) <= 8192000) {
					$tempnam = tempnam(APPLICATION_PATH.'/../tmp','PJIMPORT');
					if(file_put_contents($tempnam, $files[$k])) {
						if(!$fileData->newFileData(FILE_OTHERS, $tempnam, $pj->name, '', $id_newprofil, true)) {
							$this->deleteCandidatData($id_newprofil);
							throw new Exception('Erreur dans le téléchargement des pièces jointes', 3);
						}
					} else unlink($tempnam);
				}
				$k++;
			}
		}
		BoondManager_Notification_Candidat::getInstance($id_newprofil, '', ($emptyData = array()), $newCandidat, $this->db)->create();//Gestion des notifications
		return $id_newprofil;
	}

	/**
	 * \brief Construit la signature du webservice d'importation d'un candidat
	 * @param <type> $xmlData flux XML contenant les données du candidat
	 * @param <type> $hash clef hash de la société du manager responsable
	 */
	public function checkSignatureWSCandidat(Wish_XMLData $xmlData, $hash) {

		throw new \Exception('do migration'); // TODO
		return (sha1($xmlData->getTagString('/webservice/param/transaction/id').'+'.$xmlData->getTagString('/webservice/param/transaction/date').'+'.$xmlData->getTagString('/webservice/param/urlweb').'+'.$xmlData->getTagString('/webservice/param/reference').'+'.$hash) == $xmlData->getTagString('/webservice/param/signature'))?true:false;
	}

	/**
	 * indicate if the candidate can be deleted
	 * @return boolean
	 */
	function isCandidatReducible($id) {
		$nbDocument = 0;
		$sql = 'SELECT COUNT(ID_POSITIONNEMENT) AS NB_DOCUMENT FROM TAB_POSITIONNEMENT WHERE ID_ITEM= :id AND ITEM_TYPE=0
				UNION ALL
				SELECT COUNT(ID_MISSIONPROJET) AS NB_DOCUMENT FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE PRJ_TYPE= :ptype AND ITEM_TYPE=0 AND ID_ITEM= :id';
		foreach($this->exec($sql, [':id'=>$id, ':ptype'=>BM::PROJECT_TYPE_RECRUITMENT]) as $document)
			$nbDocument += $document['NB_DOCUMENT'];
		if($nbDocument == 0) return true; else return false;
	}
}
