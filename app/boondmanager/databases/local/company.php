<?php
/**
 * company.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\APIs\Companies\Filters;
use BoondManager\Models;
use BoondManager\Services\BM;

/**
 * Handle all database work related to resources
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Company extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param Filters\SearchCompanies $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchCompanies(Filters\SearchCompanies $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('CSOC.ID_CRMSOCIETE, CSOC.ID_POLE, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, CSOC.ID_SOCIETE, CSOC_SOCIETE,
								CSOC_INTERVENTION, CSOC_METIERS, CSOC_ADR, CSOC_CP, CSOC_VILLE, CSOC_PAYS, CSOC_WEB, CSOC_TEL, CSOC_TYPE, CSOC_DATEUPDATE');
		$query->from('TAB_CRMSOCIETE CSOC');
		$query->addJoin('LEFT JOIN TAB_PROFIL USING(ID_PROFIL)
							LEFT JOIN TAB_USER USING(ID_PROFIL)
							LEFT JOIN TAB_INFLUENCER ON(ID_ITEM=CSOC.ID_CRMSOCIETE AND ITEM_TYPE='.BM::INFLUENCER_CRM_COMPANY.')');
		$query->groupBy('CSOC.ID_CRMSOCIETE');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'CSOC.ID_SOCIETE',
			$colPole = 'CSOC.ID_POLE',
			$colUser = 'CSOC.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['CSOC' => 'CSOC.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			switch($filter->keywordsType->getValue()) {
				case Filters\SearchCompanies::KEYWORD_TYPE_NAME:
					$where->and_('CSOC.CSOC_SOCIETE LIKE ?', $keywords.'%');
					break;
				case Filters\SearchCompanies::KEYWORD_TYPE_PHONES:
					$where->and_(new Where('CSOC.CSOC_TEL LIKE ?', $likeSearch));
					break;
				default:case Filters\SearchCompanies::KEYWORD_TYPE_DEFAULT:
					$where->and_(new Where('CSOC.CSOC_SOCIETE LIKE ?', $likeSearch));
			}// fin du switch tk
		}

		$where->and_( $this->getFilterSearch( $filter->states->getValue(), 'CSOC_TYPE') )
		      ->and_( $this->getFilterSearch( $filter->origins->getValue(), 'CSOC_TYPESOURCE') )
		      ->and_( $this->getFilterSearch( $filter->expertiseAreas->getValue(), 'CSOC_INTERVENTION', [], true) );

		switch($filter->period->getValue()){
			case Filters\SearchCompanies::PERIOD_CREATED:
				$where->and_('DATE(CSOC_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case Filters\SearchCompanies::PERIOD_UPDATED:
				$where->and_('CSOC_DATEUPDATE BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			default:case BM::INDIFFERENT:break;
		}

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_CRM_COMPANY.' AND TAB_FLAG.ID_PARENT=CSOC.ID_CRMSOCIETE)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		$this->f3->set('BMDATA.MYSQLSEARCH', true); // TODO: a quoi ca sert ?

		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			Filters\SearchCompanies::ORDERBY_INFORMATION          => 'CSOC_METIERS',
			Filters\SearchCompanies::ORDERBY_EXPERTISEAREA        => 'CSOC_INTERVENTION',
			Filters\SearchCompanies::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM',
			Filters\SearchCompanies::ORDERBY_UPDATE_DATE          => 'CSOC_DATEUPDATE',
			Filters\SearchCompanies::ORDERBY_TYPE                 => 'CSOC_TYPE',
			Filters\SearchCompanies::ORDERBY_TOWN                 => 'CSOC_VILLE',
			Filters\SearchCompanies::ORDERBY_NAME                 => 'CSOC_SOCIETE'
		];

		if(!$column) $query->addOrderBy('CSOC_DATEUPDATE DESC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping)) $query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('CSOC.ID_CRMSOCIETE DESC');
	}


	/**
	 * @param $idCompany
	 * @return Models\Employee[]
	 * @throws \Exception
	 */
	private function getAllInfluencers($idCompany) {
		$query = new Query();
		$query->select(['INF.ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'TAB_USER.ID_USER'])
			->from('TAB_INFLUENCER AS INF')
			->addJoin('INNER JOIN TAB_PROFIL ON INF.ID_PROFIL=TAB_PROFIL.ID_PROFIL')
			->addJoin('INNER JOIN TAB_USER ON TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL')
			->addWhere('ID_ITEM = :id AND ITEM_TYPE = :type AND USER_TYPE = :userType', [
				'id' => $idCompany,
				'type' => BM::CATEGORY_CRM_COMPANY,
				'userType' => BM::DB_TYPE_MANAGER
			]);

		return $this->exec($query);
	}

	/**
	 * @param int $idcrm
	 * @param string $onglet
	 * @param int $activepage deprecated
	 * @param array $tabFilters deprecated
	 * @param $nbmaxresults deprecated
	 * @return Models\Company
	 * @throws \Exception
	 */
	public function getCRMData($idcrm, $onglet= Models\Company::TAB_INFORMATION, $activepage = 1, &$tabFilters = array(), $nbmaxresults = TABLEAU_NBRESULTATS_DEFAULT)
	{
		$baseQuery = new Query();

		// TODO, voir les champs nécéssaires pour les droits
		$baseQuery
			->select([
				'SOC.ID_CRMSOCIETE', 'SOC.CSOC_DATEUPDATE', 'SOC.CSOC_DATE', 'SOC.CSOC_SOCIETE', 'SOC.ID_PROFIL',
				'SOC.ID_SOCIETE', 'SOC.ID_POLE'
			])
			->from('TAB_CRMSOCIETE SOC')
			->addWhere('SOC.ID_CRMSOCIETE=?', $idcrm);

		$result = false;
		if(isset($tabFilters['nb_result_per_page'])) $nbmaxresults = $tabFilters['nb_result_per_page'];

		switch($onglet) {
			case Models\Company::TAB_INFORMATION://Informations
				$baseQuery->addColumns('RESPSOC.ID_CRMSOCIETE AS ID_RESPSOC, RESPSOC.CSOC_SOCIETE AS RESP_SOCIETE, SOC.CSOC_INTERVENTION, 
				                        SOC.CSOC_NUMERO, SOC.CSOC_TYPESOURCE, SOC.CSOC_SOURCE, SOC.CSOC_METIERS, 
				                        SOC.CSOC_TEL, SOC.CSOC_ADR, SOC.CSOC_CP, SOC.CSOC_WEB, SOC.CSOC_FAX, SOC.CSOC_VILLE, 
				                        SOC.CSOC_PAYS, SOC.CSOC_TYPE, SOC.CSOC_EFFECTIF, SOC.CSOC_SERVICES, SOC.CSOC_TVA, SOC.CSOC_SIREN, 
				                        SOC.CSOC_NIC, SOC.CSOC_STATUT, SOC.CSOC_RCS, SOC.CSOC_NAF');
				$baseQuery->addJoin('LEFT JOIN TAB_CRMSOCIETE RESPSOC ON(RESPSOC.ID_CRMSOCIETE=SOC.ID_RESPSOC)')
				          ->addJoin('LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=SOC.ID_PROFIL)');
				$result = $this->singleExec($baseQuery);
				if($result) {
					$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm);

					//On récupère tous les documents
					$result['DOCUMENTS'] = $this->exec(
						'SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT= ? AND DOC_TYPE= ?',
						[ $result['ID_CRMSOCIETE'], BM::CATEGORY_CRM_COMPANY ]
					);

					//On récupère la liste de toutes les sociétés filles de cette société
					$result['SOCIETEFILLES'] = $this->exec(
						'SELECT ID_CRMSOCIETE, CSOC_SOCIETE, CSOC_VILLE, CSOC_PAYS FROM TAB_CRMSOCIETE WHERE ID_RESPSOC= ? ',
						$result['ID_CRMSOCIETE']
					);

					//On récupère toutes les coordonnées de facturation de la société
					$sql = 'SELECT ID_COORDONNEES, COORD_NOM, COORD_CONTACT, COORD_EMAIL, COORD_EMAIL2, COORD_EMAIL3, COORD_TEL, COORD_ADR1, 
					               COORD_ADR2, COORD_ADR3, COORD_CP, COORD_VILLE, COORD_PAYS, COORD_ETAT 
					        FROM TAB_COORDONNEES WHERE ID_PARENT= ? AND COORD_TYPE=0 ORDER BY ID_COORDONNEES ASC;';
					$result['FACT_COORDONNEES'] = $this->exec($sql, $result->ID_CRMSOCIETE);

					// On récupère les infos relatives aux réseaux sociaux
					$sql = 'SELECT ID_WEBSOCIAL, WEBSOCIAL_NETWORK, WEBSOCIAL_URL FROM TAB_WEBSOCIAL WHERE WEBSOCIAL_TYPE=3 AND ID_PARENT= ? ';
					$result['WEBSOCIALS'] = $this->exec($sql, $result->ID_CRMSOCIETE);
				}
				break;
				return $result;
			case 1://Contacts
				$result = $this->singleExec('SELECT TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CRMSOCIETE.ID_POLE, TAB_CRMSOCIETE.ID_PROFIL, TAB_CRMSOCIETE.ID_SOCIETE, TAB_CRMSOCIETE.ID_POLE, CSOC_SOCIETE FROM TAB_CRMSOCIETE WHERE TAB_CRMSOCIETE.ID_CRMSOCIETE=?', $idcrm);
				if($result) {
					$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm);
					throw new \Exception('TODO'); //TODO
					$bourseBDD = new BoondManager_ObjectBDD_Recherche_CRM($this->db);
					$bourseBDD->type_join = 0;
					$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
					$filters_array = $bourseBDD->getDefaultFilters(false);
					switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallcrm', 'login_clefwriteallcrm', true, 0, array(), $result['INFLUENCERS'])) {
						case 3:case 2:$filters_array['perimetre'] = '';break;
						case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallcrm') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallcrm'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallcrm'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallcrm'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
					}
					$filters_array['keywords'] = 'CSOC'.$result['ID_CRMSOCIETE'];
					$filters_array['type_fiche'] = 1;
					$filters_array['order_colonne'] = 'contact';
					$filters_array['order_type'] = 'asc';
					if( $activepage != 0 ) $filters_array['page_active'] = $activepage; else $filters_array['page_active'] = 1;
					$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
					$result['NB_CONTACTS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
					$result['CONTACTS'] = ($search)?$bourseBDD->get('RESULTS'):array();

					//On initialise la navigation de la vue
					$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=1', 'contacts_');
				}
				break;
			case Models\Company::TAB_ACTIONS:
				throw new \Exception('TODO'); //TODO
				$result = $this->db->query_first('SELECT TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CRMSOCIETE.ID_PROFIL, TAB_CRMSOCIETE.ID_SOCIETE, TAB_CRMSOCIETE.ID_POLE, CSOC_SOCIETE FROM TAB_CRMSOCIETE WHERE TAB_CRMSOCIETE.ID_CRMSOCIETE="'.$this->db->escape($idcrm).'";');
				if($result) {
					$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

					$bourseBDD = new BoondManager_ObjectBDD_Recherche_Actions($this->db);
					$bourseBDD->type_join = 0;
					$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
					$filters_array = $bourseBDD->getDefaultFilters(false);
					switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallcrm', 'login_clefwriteallcrm', true, 0, array(), $result['INFLUENCERS'])) {
						case 3:case 2:$filters_array['perimetre'] = '';break;
						case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallcrm') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallcrm'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallcrm'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallcrm'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
					}
					$filters_array['keywords'] = 'CSOC'.$result['ID_CRMSOCIETE'];
					$filters_array['order_colonne'] = 'date';
					$filters_array['order_type'] = 'desc';
					$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
					$filters_array['type_rappel'] = '-1';//On recherche les actions de la crm
					if(isset($tabFilters['mc']['liste_rappel']) && is_array($tabFilters['mc']['liste_rappel'])) $filters_array['mc']['liste_rappel'] = $tabFilters['mc']['liste_rappel']; else foreach(Wish_Session::getInstance()->get('login_clefactionscrm') as $idaction => $action) $filters_array['mc']['liste_rappel'][] = $idaction;
					$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
					$result['NB_ACTIONS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
					$result['ACTIONS'] = ($search)?$bourseBDD->get('RESULTS'):array();
					//On initialise la navigation de la vue
					$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=2&'.setURLMultiCriteria('lr', $filters_array['mc']['liste_rappel']), 'actions_');
					$tabFilters = $filters_array;
				}
				break;
			case 3://Besoins
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_modulebesoins')) {
					$result = $this->db->query_first('SELECT TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CRMSOCIETE.ID_PROFIL, TAB_CRMSOCIETE.ID_SOCIETE, TAB_CRMSOCIETE.ID_POLE, CSOC_SOCIETE FROM TAB_CRMSOCIETE WHERE TAB_CRMSOCIETE.ID_CRMSOCIETE="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Besoins($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallbesoins', 'login_clefwriteallbesoins', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallbesoins') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallbesoins'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallbesoins'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallbesoins'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CSOC'.$result['ID_CRMSOCIETE'];
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'date';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
						$result['NB_BESOINS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['BESOINS'] = ($search)?$bourseBDD->get('RESULTS'):array();
						$result['TOTAL_CA'] = ($search)?$bourseBDD->get('TOTAL_CA'):0;

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=3', 'besoins_');
					}
				}
				break;
			case 4://Projets
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_moduleprojets')) {
					$result = $this->db->query_first('SELECT TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CRMSOCIETE.ID_PROFIL, TAB_CRMSOCIETE.ID_SOCIETE, TAB_CRMSOCIETE.ID_POLE, CSOC_SOCIETE FROM TAB_CRMSOCIETE WHERE TAB_CRMSOCIETE.ID_CRMSOCIETE="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Projets($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallprojets', 'login_clefwriteallprojets', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallprojets') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallprojets'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallprojets'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallprojets'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
			$influencer_type = 0;
						$filters_array['keywords'] = 'CSOC'.$result['ID_CRMSOCIETE'];
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'fin';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
						$result['NB_PROJETS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['TOTAL_CA'] = ($search)?$bourseBDD->get('TOTAL_CA'):0;
						$result['TOTAL_MARGE'] = ($search)?$bourseBDD->get('TOTAL_MARGE'):0;
						$result['TOTAL_RENTA'] = ($search)?$bourseBDD->get('TOTAL_RENTA'):0;
						$result['PROJETS'] = ($search)?$bourseBDD->get('RESULTS'):array();

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=4', 'projets_');
					}
				}
				break;
			case 5://Achats
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_moduleachats')) {
					$result = $this->db->query_first('SELECT TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CRMSOCIETE.ID_PROFIL, TAB_CRMSOCIETE.ID_SOCIETE, TAB_CRMSOCIETE.ID_POLE, CSOC_SOCIETE FROM TAB_CRMSOCIETE WHERE TAB_CRMSOCIETE.ID_CRMSOCIETE="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Achats($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallachats', 'login_clefwriteallachats', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallachats') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallachats'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallachats'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallachats'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CSOC'.$result['ID_CRMSOCIETE'];
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'date';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
						$result['NB_ACHATS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['ACHATS'] = ($search)?$bourseBDD->get('RESULTS'):array();
						$result['TOTAL_MONTANT'] = ($search)?$bourseBDD->get('TOTAL_MONTANT'):0;
						$result['TOTAL_CONSOMMES'] = ($search)?$bourseBDD->get('TOTAL_CONSOMMES'):0;

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=5', 'achats_');
					}
				}
				break;
			case 6://Facturation
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_modulefacturation')) {
					$result = $this->db->query_first('SELECT TAB_CRMSOCIETE.ID_CRMSOCIETE, TAB_CRMSOCIETE.ID_PROFIL, TAB_CRMSOCIETE.ID_SOCIETE, TAB_CRMSOCIETE.ID_POLE, CSOC_SOCIETE FROM TAB_CRMSOCIETE WHERE TAB_CRMSOCIETE.ID_CRMSOCIETE="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);
						//On récupère les commandes de ce projet
						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Facturation($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallfacturation', 'login_clefwriteallfacturation', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallcrm') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallfacturation'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallfacturation'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallfacturation'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CSOC'.$result['ID_CRMSOCIETE'];
						if(isset($tabFilters['type_document']) && in_array($tabFilters['type_document'], array(0,1))) $filters_array['type_document'] = $tabFilters['type_document']; else $filters_array['type_document'] = '0';
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'date';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array);
						$result['NB_FACTURATION'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['FACTURATION'] = ($search)?$bourseBDD->get('RESULTS'):array();
						if($filters_array['type_document'] == 1) {
							$result['TOTAL_MONTANTBDC'] = ($search)?$bourseBDD->get('TOTAL_MONTANTBDC'):0;
							$result['TOTAL_CAFACTUREHT'] = ($search)?$bourseBDD->get('TOTAL_CAFACTUREHT'):0;
						} else {
							$result['TOTAL_CAFACTUREHT'] = ($search)?$bourseBDD->get('TOTAL_CAFACTUREHT'):0;
							$result['TOTAL_CAFACTURETTC'] = ($search)?$bourseBDD->get('TOTAL_CAFACTURETTC'):0;
						}

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=6', 'facturation_');
						$tabFilters = $filters_array;
					}
				}
				break;
			case Models\Company::TAB_SETTING://Setting
				$baseQuery->addColumns('CSOC_BAREMESEXCEPTION');
				$result = $this->singleExec($baseQuery);
				if($result) {
					$result['LISTE_REGLEE'] = $this->exec('SELECT ID_REGLEEXCEPTION, REGLE_REF, REGLE_BAREMEREF, REGLE_NAME, REGLE_TARIF, REGLE_COUT, REGLE_ETAT, ID_PARENT AS ID_CRMSOCIETE FROM TAB_REGLEEXCEPTION WHERE ID_PARENT=? AND PARENT_TYPE=1 ORDER BY ID_PARENT ASC, ID_REGLEEXCEPTION ASC', $idcrm);
				}
				break;
			default:
				$result = $this->singleExec($baseQuery);
		}

		return $result;
	}

	/**
	 * \brief Création d'un nouveau contact CRM
	 * @param <type> $data tableau de données
	 * @return <type> identifiant de la CRM
	 */
	public function newCRMData($data, $solr = true)
	{
		if (isset($data['SOCIETE'])) {
			if (!isset($data['SOCIETE']['CSOC_DATEUPDATE'])) $data['SOCIETE']['CSOC_DATEUPDATE'] = date('Y-m-d H:i:s'); else $data['SOCIETE']['CSOC_DATEUPDATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['SOCIETE']['CSOC_DATEUPDATE'])->format('Y-m-d H:i:s');
			if (!isset($data['SOCIETE']['CSOC_DATE'])) $data['SOCIETE']['CSOC_DATE'] = date('Y-m-d H:i:s'); else $data['SOCIETE']['CSOC_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['SOCIETE']['CSOC_DATE'])->format('Y-m-d H:i:s');
			if (!isset($data['SOCIETE']['CSOC_BAREMESEXCEPTION'])) $data['SOCIETE']['CSOC_BAREMESEXCEPTION'] = '';

			$idsociete = $this->insert('TAB_CRMSOCIETE', $data['SOCIETE']);

			//On ajoute les coordonnées de facturation
			if (isset($data['FACT_COORDONNEES']))
				foreach ($data['FACT_COORDONNEES'] as $data_coord)
					$data_coord['ID_PARENT'] = $idsociete;
					$data_coord['COORD_TYPE'] = BM::CATEGORY_CRM_COMPANY;
					$this->insert('TAB_COORDONNEES', $data_coord);

			// On ajoute les réseaux sociaux
			if (isset($data['WEBSOCIALS']))
				foreach ($data['WEBSOCIALS'] as $data_social)
					$this->insert('TAB_WEBSOCIAL', [
						'WEBSOCIAL_NETWORK' => $data_social['WEBSOCIAL_NETWORK'],
						'WEBSOCIAL_URL' => $data_social['WEBSOCIAL_URL'],
						'ID_PARENT' => $idsociete,
						'WEBSOCIAL_TYPE' => 3
					]);

			// On ajoute les influenceurs
			if (isset($data['INFLUENCERS']))
				foreach ($data['INFLUENCERS'] as $id_profil)
					$this->insert('TAB_INFLUENCER', [
						'ID_PROFIL' => $id_profil,
						'ID_ITEM' => $idsociete,
						'ITEM_TYPE' => 0
					]);

			if ($solr) \BoondManager\Lib\SolR::instance()->Creation(BM::SOLR_MODULE_CRM_COMPANIES);

			return $idsociete;
		}

		return false;
	}

	/**
	 * Delete all data related to a company
	 * @param int $idcrm company ID
	 * @param bool $solr trigger a SolR Sync
	 * @return true
	 */
	public function deleteCRMData($idcrm, $solr = true)
	{
		$contactsDB = new Contact();
			//On supprime tous les contacts de cette société
			foreach($this->exec('SELECT ID_CRMCONTACT FROM TAB_CRMCONTACT WHERE ID_CRMSOCIETE=?',$idcrm) as $idcontact)
				$contactsDB->deleteCRMData($idcontact['ID_CRMCONTACT'], false);

			//On supprime tous les document de cette société
			$fileData = new File();
			$fileData->deleteAllObjects(File::TYPE_DOCUMENT, $idcrm, Models\Document::TYPE_COMPANY);

			//On supprime tous les flags de cette action
			$flagsData = new AttachedFlag();
			$flagsData->removeAllAttachedFlagsFromEntity($idcrm, Models\AttachedFlag::TYPE_COMPANY);

			$this->delete('TAB_RIB', 'RIB_TYPE=2 AND ID_PARENT = ?', $idcrm);//On supprime tous les RIB

			//On supprime les réseaux sociaux de la société
			$this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE=3', $idcrm);

			//On supprime les influenceurs
			$this->delete('TAB_INFLUENCER', 'ID_ITEM = ? AND ITEM_TYPE=0', $idcrm);

			//On supprime les coordonnées de facturation de la société
			$this->delete('TAB_COORDONNEES', 'ID_PARENT = ? AND COORD_TYPE=0', $idcrm);

			$this->delete('TAB_REGLEEXCEPTION', 'ID_PARENT = ? AND PARENT_TYPE=1', $idcrm);

			//On supprime la société de ce profil
			$this->delete('TAB_CRMSOCIETE', 'ID_CRMSOCIETE = ?', $idcrm);

			//On indique la suppression dans la table TAB_DELETED
			$this->insert('TAB_DELETED', [
				'ID_PARENT' => $idcrm,
				'DEL_TYPE' => 0,
				'DEL_DATE' => date('Y-m-d H:i:s')
			]);

			if($solr) {
				\BoondManager\Lib\SolR::instance()->Suppression(BM::SOLR_MODULE_CRM_COMPANIES);
				\BoondManager\Lib\SolR::instance()->Suppression(BM::SOLR_MODULE_CRM_CONTACTS);
			}
		return true;
	}

	/**
	 * update all data related to a company
	 * @param array $data
	 * @param int $idcrm company id
	 * @param boolean $solr trigger a solr sync
	 * @return true
	 */
	public function updateCRMData($data, $idcrm, $solr = true)
	{
		if(isset($data['SOCIETE'])) {
			$data['SOCIETE']['CSOC_DATEUPDATE'] = date('Y-m-d H:i:s');
			if(isset($data['SOCIETE']['CSOC_DATE']))
				$data['SOCIETE']['CSOC_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['SOCIETE']['CSOC_DATE'])->format('Y-m-d H:i:s');
			$this->update('TAB_CRMSOCIETE', $data['SOCIETE'], 'ID_CRMSOCIETE = :id', ['id'=>$idcrm]);
		}

		if(isset($data['FACT_COORDONNEES'])) {
			$ids = array_column($data['FACT_COORDONNEES'], 'ID_COORDONNEES');
			if($ids) {
				$this->delete('TAB_COORDONNEES', 'ID_PARENT = ? AND COORD_TYPE = 0 AND ID_COORDONNEES NOT IN (' . Where::prepareWhereIN($ids) . ')', array_merge([$idcrm] , $ids));
			}else{
				$this->delete('TAB_COORDONNEES', 'ID_PARENT = ? AND COORD_TYPE = 0', $idcrm);
			}

			//On met à jour les coordonnées de facturation
			foreach($data['FACT_COORDONNEES'] as $data_coord) {
				if(!isset($data_coord['ID_COORDONNEES'])) { //Il s'agit d'une nouvelle coordonnnées
					$data_coord = array_merge($data_coord, [
						'COORD_TYPE' => 0,
						'ID_PARENT'  => $idcrm
					]);
					$this->insert('TAB_COORDONNEES', $data_coord);
				}else {
					$this->update('TAB_COORDONNEES', $data_coord, 'ID_COORDONNEES= :id AND ID_PARENT = :parent', ['id' => $data_coord['ID_COORDONNEES'], 'parent'=>$idcrm]);
				}
			}
		}

		if(isset($data['CONTACT'])) {
			$data['CONTACT']['CCON_DATEUPDATE'] = date('Y-m-d H:i:s', mktime());
			if(isset($data['CONTACT']['CCON_DATE'])) $data['CONTACT']['CCON_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['CONTACT']['CCON_DATE'])->format('Y-m-d H:i:s');
			$this->update('TAB_CRMCONTACT', $data['CONTACT'], 'ID_CRMSOCIETE = :id', ['id'=>$idcrm]);
		}

		// on met à jour les réseaux sociaux
		//~ - passer un tableau vide pour supprimer tous les réseaux
		//~ - passer une url vide pour supprimer le réseau correspondant (même comportement que sur l'ui v6)
		//~ - passer le réseau et une url valide pour l'ajouter ou le modifier
		if(isset($data['WEBSOCIALS'])) {
			$existing_networks = Tools::getFieldsToArray(
				$this->exec('SELECT WEBSOCIAL_NETWORK FROM TAB_WEBSOCIAL WHERE ID_PARENT = ? AND WEBSOCIAL_TYPE = ?',
							[$idcrm, Models\WebSocial::WEBSOCIAL_TYPE_COMPANY_CRM])
				,'WEBSOCIAL_NETWORK');
			if($networks = array_column($data['WEBSOCIALS'], 'WEBSOCIAL_NETWORK')) {
				foreach($data['WEBSOCIALS'] as $data_social) {
					if(empty($data_social['WEBSOCIAL_URL'])){
						$this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ? AND WEBSOCIAL_NETWORK = ?',
						[$idcrm, Models\WebSocial::WEBSOCIAL_TYPE_COMPANY_CRM, $data_social['WEBSOCIAL_NETWORK']]);
					} else if(in_array($data_social['WEBSOCIAL_NETWORK'], $existing_networks)) {
						$this->update('TAB_WEBSOCIAL', $data_social,
							'ID_PARENT = :parent AND WEBSOCIAL_TYPE = :type AND WEBSOCIAL_NETWORK = :network',
							['parent' => $idcrm, 'type' => Models\WebSocial::WEBSOCIAL_TYPE_COMPANY_CRM, 'network' => $data_social['WEBSOCIAL_NETWORK']]);
					} else
						$this->insert('TAB_WEBSOCIAL', array_merge($data_social,[
							'WEBSOCIAL_TYPE' => Models\WebSocial::WEBSOCIAL_TYPE_COMPANY_CRM,
							'ID_PARENT' => $idcrm
						]));
				}
			} else $this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ?', [$idcrm, Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE]);
		}

		if(isset($data['INFLUENCERS']) && $data['INFLUENCERS']) {
			$inflResult = $this->exec('SELECT ID_PROFIL FROM TAB_INFLUENCER WHERE ID_ITEM=? AND ITEM_TYPE=0', $idcrm);
			$existingIDs = Tools::getFieldsToArray($inflResult, 'ID_PROFIL');
			$this->delete('TAB_INFLUENCER', 'ID_ITEM = ? AND ITEM_TYPE = 0 AND ID_PROFIL NOT IN (' . Where::prepareWhereIN($data['INFLUENCERS']) . ')', array_merge([$idcrm] , $data['INFLUENCERS']));

			foreach($data['INFLUENCERS'] as $id_profil) {
				if (!in_array($id_profil, $existingIDs))
					$this->insert('TAB_INFLUENCER', [
						'ID_PROFIL' => $id_profil,
						'ITEM_TYPE' => 0,
						'ID_ITEM'   => $idcrm
					]);
			}
		}
		if($solr) {
			\BoondManager\Lib\SolR::instance()->Modification(BM::SOLR_MODULE_CRM_COMPANIES);
		}
		return true;
	}

	/**
	 * find out if the company can be deleted
	 * @return bool
	 */
	function isCRMReducible($id) {
		$nbDocument = 0;

		$sql = 'SELECT COUNT(ID_CRMCONTACT) AS NB_DOCUMENT FROM TAB_CRMCONTACT WHERE ID_CRMSOCIETE = :id
				UNION ALL
				SELECT COUNT(ID_BONDECOMMANDE) AS NB_DOCUMENT FROM TAB_BONDECOMMANDE WHERE ID_CRMSOCIETE = :id
				UNION ALL
				SELECT COUNT(ID_GROUPECONFIG) AS NB_DOCUMENT FROM TAB_GROUPECONFIG WHERE GRPCONF_BDCIDCRMSOCIETE = :id';

		foreach($this->exec($sql,['id' => $id]) as $document)
			$nbDocument += $document['NB_DOCUMENT'];

		return $nbDocument == 0;

	}
}
