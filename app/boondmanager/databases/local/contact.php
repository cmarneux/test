<?php
/**
 * contact.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use BoondManager\Models\WebSocial;
use Wish\Models\Model;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Lib\SolR;
use Wish\Tools;
use BoondManager\Models;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

/**
 * Handle all database work related to resources
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Contact extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param \BoondManager\APIs\Contacts\Filters\SearchContacts $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchContacts(\BoondManager\APIs\Contacts\Filters\SearchContacts $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('CCON.ID_CRMCONTACT, CCON.ID_CRMSOCIETE, CCON.ID_POLE, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, CCON.ID_SOCIETE, CCON_CIVILITE, CCON_NOM,
		CCON_PRENOM, CCON_FONCTION, CCON_EMAIL, CCON_EMAIL2, CCON_EMAIL3, CCON_TEL1, CCON_TEL2, CCON_SERVICE, CCON_ADR, CCON_CP, CCON_VILLE, CCON_PAYS,
		CSOC_SOCIETE, CSOC_INTERVENTION, CSOC_METIERS, CSOC_ADR, CSOC_CP, CSOC_TEL, CSOC_VILLE, CSOC_PAYS, CSOC_BAREMESEXCEPTION, CCON_TYPE,
		CCON_COMMENTAIRE, CCON_DATEUPDATE');
		$query->from('TAB_CRMCONTACT CCON');
		$query->addJoin('LEFT JOIN TAB_CRMSOCIETE CSOC USING(ID_CRMSOCIETE)
							LEFT JOIN TAB_PROFIL ON CCON.ID_PROFIL=TAB_PROFIL.ID_PROFIL
							LEFT JOIN TAB_USER ON TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL
							LEFT JOIN TAB_INFLUENCER ON(ID_ITEM=CCON.ID_CRMCONTACT AND ITEM_TYPE='.BM::INFLUENCER_CRM_CONTACT.')');
		$query->groupBy('CCON.ID_CRMCONTACT');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'CCON.ID_SOCIETE',
			$colPole = 'CCON.ID_POLE',
			$colUser = 'CCON.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['CCON' => 'CCON.ID_CRMCONTACT', 'CSOC' => 'CSOC.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where();
			switch($filter->keywordsType->getValue()) {
				case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_LASTNAME:
					$where->and_('CCON.CCON_NOM LIKE ?', $keywords.'%');
					break;
				case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_FIRSTNAME:
					$where->and_('CCON.CCON_PRENOM LIKE ?', $keywords.'%');
					break;
				case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_FULLNAME:
					$tabKeywords = explode('#', $keywords);
					$where->and_('CCON.CCON_NOM LIKE ?', $tabKeywords[0].'%')
							->and_('CCON.CCON_PRENOM LIKE ?', $tabKeywords[1].'%');
					break;
				case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_COMPANYFULLNAME:
					$tabKeywords = explode('#', $keywords);
					$where->and_($this->getListIdSearch($tabKeywords[0], ['CSOC' => 'CSOC.ID_CRMSOCIETE']))
							->and_('CCON.CCON_NOM LIKE ?', $tabKeywords[1].'%')
							->and_('CCON.CCON_PRENOM LIKE ?', $tabKeywords[2].'%');
					break;
				case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_EMAILS:
					$tabEmail = explode(' ',$keywords);
					array_filter($tabEmail, function($value){
						return filter_var($value, FILTER_VALIDATE_EMAIL)!==false;
					});

					if(sizeof($tabEmail) > 0) {
						$where->and_((new Where('CCON.CCON_EMAIL LIKE ?', $likeSearch))
										->or_('CCON.CCON_EMAIL2 LIKE ?', $likeSearch)
										->or_('CCON.CCON_EMAIL3 LIKE ?', $likeSearch)
										);
					}
					break;
				case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_PHONES:
					$where->and_((new Where('CCON.CCON_TEL1 LIKE ?', $likeSearch))
									->or_('CCON.CCON_TEL2 LIKE ?', $likeSearch)
									);
					break;
				default:case \BoondManager\APIs\Contacts\Filters\SearchContacts::KEYWORD_TYPE_DEFAULT:
					$where->and_((new Where('MATCH(CCON_FONCTION,CCON.CCON_COMMENTAIRE) AGAINST(?)', $keywords))
									->or_('CCON.CCON_NOM LIKE ?', $likeSearch)
									->or_('CCON.CCON_PRENOM LIKE ?', $likeSearch)
									->or_('CSOC.CSOC_SOCIETE LIKE ?', $likeSearch)
									->or_('CCON.CCON_FONCTION LIKE ?', $likeSearch)
									->or_('CCON.CCON_SERVICE LIKE ?', $likeSearch)
									);
					$query->addColumns([ 'PERTINENCE'=> 'MATCH(CCON_FONCTION,CCON.CCON_COMMENTAIRE) AGAINST(' . $this->escape($keywords) .  ')' ]);
					$query->addOrderBy('PERTINENCE DESC');
			}// fin du switch tk
		}

		$where->and_( $this->getFilterSearch( $filter->states->getValue(), 'CCON_TYPE') )
			->and_( $this->getFilterSearch( $filter->origins->getValue(), 'CCON_TYPESOURCE') )
			->and_( $this->getFilterSearch( $filter->activityAreas->getValue(), 'CCON_APPLICATIONS', [], true) )
			->and_( $this->getFilterSearch( $filter->expertiseAreas->getValue(), 'CSOC_INTERVENTION', [], true) )
			->and_( $this->getFilterSearch( $filter->tools->getValue(), 'CCON_OUTILS', [], true) );

		if($filter->returnLastAction->getValue() == 1){
			$tabActionsCRM = Dictionary::getDictTranslatedValues(Dictionary::getActionKeyFromCategoryID(BM::CATEGORY_CRM_CONTACT));
			$query->addJoin('LEFT OUTER JOIN TAB_ACTION LASTACTION ON(LASTACTION.ID_ACTION = (SELECT ID_ACTION FROM TAB_ACTION WHERE ID_PARENT=CCON.ID_CRMCONTACT AND ACTION_TYPE IN("'.implode('","', $tabActionsCRM).'") ORDER BY ACTION_DATE DESC LIMIT 1))');
			$query->addColumns('LASTACTION.ID_ACTION,LASTACTION.ID_PARENT,LASTACTION.ACTION_TYPE,LASTACTION.ACTION_TEXTE,LASTACTION.ACTION_DATE');
		}

		switch($filter->period->getValue()){
			case \BoondManager\APIs\Contacts\Filters\SearchContacts::PERIOD_CREATED:
				$where->and_('DATE(CCON_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case \BoondManager\APIs\Contacts\Filters\SearchContacts::PERIOD_UPDATED:
				$where->and_('CCON_DATEUPDATE BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case \BoondManager\APIs\Contacts\Filters\SearchContacts::PERIOD_ACTIONS:
			//~ TODO : 18-04-2016: Tin : utiliser getCategoryFromType du service actions
				$tabActionsCRM = Dictionary::getDictTranslatedValues(Dictionary::getActionKeyFromCategoryID(BM::CATEGORY_CRM_CONTACT));
				$query->addJoin('LEFT JOIN TAB_ACTION ON(TAB_ACTION.ID_PARENT=CCON.ID_CRMCONTACT AND ACTION_TYPE IN("'.implode('","', $tabActionsCRM).'") AND DATE(ACTION_DATE) BETWEEN '.$this->escape($filter->startDate->getValue()).' AND '.$this->escape($filter->endDate->getValue()).')');
				$where->and_('ID_ACTION IS NULL');
				break;
			default:case BM::INDIFFERENT:break;
		}

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_CRM_CONTACT.' AND TAB_FLAG.ID_PARENT=CCON.ID_CRMCONTACT)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

        $this->f3->set('BMDATA.MYSQLSEARCH', true); // TODO: a quoi ca sert ?

		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_COMPANY_EXPERTISEAREA => 'CSOC_INTERVENTION',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_LASTNAME              => 'CCON_NOM',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_MAINMANAGER_LASTNAME  => 'PROFIL_NOM',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_UPDATE_DATE           => 'CCON_DATEUPDATE',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_TYPE                  => 'CCON_TYPE',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_TOWN                  => 'CCON_VILLE',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_FUNCTION              => 'CCON_FONCTION',
			\BoondManager\APIs\Contacts\Filters\SearchContacts::ORDERBY_COMPANY_NAME          => 'CSOC_SOCIETE'
		];

		if(!$column) $query->addOrderBy('CCON_DATEUPDATE DESC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('CCON.ID_CRMCONTACT DESC');
	}

	private function getAllInfluencers($idcrm) {

		$query = new Query();
		$query->select(['INF.ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'TAB_USER.ID_USER'])
			->from('TAB_INFLUENCER AS INF')
			->addJoin('INNER JOIN TAB_PROFIL ON INF.ID_PROFIL=TAB_PROFIL.ID_PROFIL')
			->addJoin('INNER JOIN TAB_USER ON TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL')
			->addWhere('ID_ITEM = :id AND ITEM_TYPE = :type AND USER_TYPE = :userType', [
				'id' => $idcrm,
				'type' => BM::CATEGORY_CRM_CONTACT,
				'userType' => BM::DB_TYPE_MANAGER
			]);

		return $this->exec($query);
	}

	/**
	 * retrieve the contact information
	 * @param int $idcrm contact ID
	 * @param int|string $tab current tab
	 * @return false|Model
	 * @throws \Exception
	 */
	public function getCRMData($idcrm, $tab=0)
	{

		$baseQuery = new Query();

		$baseQuery->select([
			'TAB_CRMCONTACT.ID_CRMCONTACT', 'TAB_CRMCONTACT.ID_PROFIL', 'CCON_NOM', 'CCON_PRENOM', 'TAB_CRMCONTACT.ID_CRMSOCIETE',
			'TAB_CRMCONTACT.ID_SOCIETE', 'TAB_CRMCONTACT.ID_POLE', 'CCON_CIVILITE', 'CCON_DATE', 'CCON_DATEUPDATE'
		]);
		$baseQuery->from('TAB_CRMCONTACT')
		          ->addWhere('TAB_CRMCONTACT.ID_CRMCONTACT = ?', $idcrm);

		switch($tab) {
			case Models\Contact::TAB_INFORMATION:
				$baseQuery->addColumns([
					'SOC.CSOC_SOCIETE', 'SOC.CSOC_TEL', 'SOC.CSOC_ADR', 'SOC.CSOC_CP',
					'SOC.CSOC_WEB', 'SOC.CSOC_SIREN', 'SOC.CSOC_NIC', 'SOC.CSOC_FAX', 'SOC.CSOC_VILLE', 'SOC.CSOC_PAYS', 'SOC.CSOC_TYPE',
					'SOC.CSOC_EFFECTIF', 'SOC.CSOC_METIERS', 'SOC.CSOC_SERVICES', 'CCON_TEL1',
					'CCON_TEL2', 'CCON_FAX', 'CCON_EMAIL', 'CCON_EMAIL2', 'CCON_EMAIL3', 'CCON_ADR', 'CCON_CP', 'CCON_VILLE',
					'CCON_PAYS', 'CCON_COMMENTAIRE', 'CCON_FONCTION', 'CCON_SERVICE', 'CCON_TYPE', 'CCON_APPLICATIONS',
					'CCON_OUTILS', 'CCON_TYPESOURCE', 'CCON_SOURCE', 'RESPSOC.CSOC_SOCIETE AS RESP_SOCIETE',
					'RESPSOC.ID_CRMSOCIETE AS ID_RESPSOC', 'SOC.CSOC_INTERVENTION'
				]);
				$baseQuery->addJoin('LEFT JOIN TAB_CRMSOCIETE SOC ON(SOC.ID_CRMSOCIETE=TAB_CRMCONTACT.ID_CRMSOCIETE)');
				$baseQuery->addJoin('LEFT JOIN TAB_CRMSOCIETE RESPSOC ON(RESPSOC.ID_CRMSOCIETE=SOC.ID_RESPSOC)');

				$result = $this->singleExec($baseQuery);
				if($result) {
					$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm);

					$sql = 'SELECT ID_WEBSOCIAL, WEBSOCIAL_NETWORK, WEBSOCIAL_URL FROM TAB_WEBSOCIAL WHERE WEBSOCIAL_TYPE=2 AND ID_PARENT= ? ';
					$result['WEBSOCIALS'] = $this->exec($sql, $result['ID_CRMCONTACT']);
				}

				return $result;
			case Models\Contact::TAB_ACTIONS:
				$result = $this->singleExec($baseQuery);
				if($result) {
					$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm);
				}
				return $result;
				break;
			case 2://Besoins
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_modulebesoins')) {
					$result = $this->db->query_first('SELECT TAB_CRMCONTACT.ID_CRMCONTACT, TAB_CRMCONTACT.ID_PROFIL, CCON_NOM, CCON_PRENOM, TAB_CRMCONTACT.ID_CRMSOCIETE, TAB_CRMCONTACT.ID_SOCIETE, TAB_CRMCONTACT.ID_POLE FROM TAB_CRMCONTACT WHERE TAB_CRMCONTACT.ID_CRMCONTACT="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Besoins($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallbesoins', 'login_clefwriteallbesoins', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallbesoins') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallbesoins'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallbesoins'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallbesoins'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CCON'.$result['ID_CRMCONTACT'];
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'date';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
						$result['NB_BESOINS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['BESOINS'] = ($search)?$bourseBDD->get('RESULTS'):array();
						$result['TOTAL_CA'] = ($search)?$bourseBDD->get('TOTAL_CA'):0;

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmcontact', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMCONTACT'].'&onglet=2', 'besoins_');
					}
				}
				break;
			case 3://Projets
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_moduleprojets')) {
					$result = $this->db->query_first('SELECT TAB_CRMCONTACT.ID_CRMCONTACT, TAB_CRMCONTACT.ID_PROFIL, CCON_NOM, CCON_PRENOM, TAB_CRMCONTACT.ID_CRMSOCIETE, TAB_CRMCONTACT.ID_SOCIETE, TAB_CRMCONTACT.ID_POLE FROM TAB_CRMCONTACT WHERE TAB_CRMCONTACT.ID_CRMCONTACT="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Projets($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallprojets', 'login_clefwriteallprojets', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallprojets') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallprojets'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallprojets'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallprojets'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CCON'.$result['ID_CRMCONTACT'];
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'fin';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
						$result['NB_PROJETS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['TOTAL_CA'] = ($search)?$bourseBDD->get('TOTAL_CA'):0;
						$result['TOTAL_MARGE'] = ($search)?$bourseBDD->get('TOTAL_MARGE'):0;
						$result['TOTAL_RENTA'] = ($search)?$bourseBDD->get('TOTAL_RENTA'):0;
						$result['PROJETS'] = ($search)?$bourseBDD->get('RESULTS'):array();

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmcontact', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMCONTACT'].'&onglet=3', 'projets_');
					}
				}
				break;
			case 4://Achats
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_moduleachats')) {
					$result = $this->db->query_first('SELECT TAB_CRMCONTACT.ID_CRMCONTACT, TAB_CRMCONTACT.ID_PROFIL, CCON_NOM, CCON_PRENOM, TAB_CRMCONTACT.ID_CRMSOCIETE, TAB_CRMCONTACT.ID_SOCIETE, TAB_CRMCONTACT.ID_POLE FROM TAB_CRMCONTACT WHERE TAB_CRMCONTACT.ID_CRMCONTACT="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);

						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Achats($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallachats', 'login_clefwriteallachats', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallachats') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallachats'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallachats'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallachats'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CCON'.$result['ID_CRMCONTACT'];
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'date';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
						$result['NB_ACHATS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['ACHATS'] = ($search)?$bourseBDD->get('RESULTS'):array();
						$result['TOTAL_MONTANT'] = ($search)?$bourseBDD->get('TOTAL_MONTANT'):0;
						$result['TOTAL_CONSOMMES'] = ($search)?$bourseBDD->get('TOTAL_CONSOMMES'):0;

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmcontact', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMCONTACT'].'&onglet=4', 'achats_');
					}
				}
				break;
			case 5://Facturation
				throw new \Exception('TODO'); //TODO
				if(Wish_Session::getInstance()->isRegistered('login_modulefacturation')) {
					$result = $this->db->query_first('SELECT TAB_CRMCONTACT.ID_CRMCONTACT, TAB_CRMCONTACT.ID_PROFIL, CCON_NOM, CCON_PRENOM, TAB_CRMCONTACT.ID_CRMSOCIETE, TAB_CRMCONTACT.ID_SOCIETE, TAB_CRMCONTACT.ID_POLE FROM TAB_CRMCONTACT WHERE TAB_CRMCONTACT.ID_CRMCONTACT="'.$this->db->escape($idcrm).'";');
					if($result) {
						$result['INFLUENCERS'] = $this->getAllInfluencers($idcrm, $type_fiche);
						//On récupère les commandes de ce projet
						$bourseBDD = new BoondManager_ObjectBDD_Recherche_Facturation($this->db);
						$bourseBDD->type_join = 0;
						$managerBDD = new BoondManager_ObjectBDD_Manager($this->db);
						$filters_array = $bourseBDD->getDefaultFilters(false);
						switch($managerBDD->getProfileAccess(array($result['ID_SOCIETE'], $result['ID_POLE'], $result['ID_PROFIL']), 'login_clefshowallfacturation', 'login_clefwriteallfacturation', true, 0, array(), $result['INFLUENCERS'])) {
							case 3:$filters_array['perimetre'] = '';break;
							case 2:case 1:
							if(Wish_Session::getInstance()->get('login_clefshowallcrm') == 2) $filters_array['perimetre'] = ''; else {
								$buBDD = new BoondManager_ObjectBDD_BusinessUnit($this->db);
								$buBDD->getMyBusinessUnits();//On construit la variable de session login_listebu (Pur être sûr qu'elle existe)
								$filters_array['mc']['perimetre'] = array(0);
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallfacturation'), array(1,3))) foreach(Wish_Session::getInstance()->get('login_tabsocietes') as $id_societe) $filters_array['mc']['perimetre'][] = -$id_societe;
								if(in_array(Wish_Session::getInstance()->get('login_clefshowallfacturation'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_listebu') as $bu) $filters_array['mc']['perimetre'][] = 'BU_'.$bu['ID_BUSINESSUNIT'];
								if(Wish_Session::getInstance()->isRegistered('login_tabpoles') && in_array(Wish_Session::getInstance()->get('login_clefshowallfacturation'), array(1,4))) foreach(Wish_Session::getInstance()->get('login_tabpoles') as $id_pole) $filters_array['mc']['perimetre'][] = 'POLE_'.$id_pole;
							}
							break;
						}
						$filters_array['keywords'] = 'CCON'.$result['ID_CRMCONTACT'];
						if(isset($tabFilters['type_document']) && in_array($tabFilters['type_document'], array(0,1))) $filters_array['type_document'] = $tabFilters['type_document']; else $filters_array['type_document'] = '0';
						$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
						$filters_array['order_colonne'] = 'date';
						$filters_array['order_type'] = 'desc';
						$search = $bourseBDD->recherche($filters_array);
						$result['NB_FACTURATION'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
						$result['FACTURATION'] = ($search)?$bourseBDD->get('RESULTS'):array();
						if($filters_array['type_document'] == 1) {
							$result['TOTAL_MONTANTBDC'] = ($search)?$bourseBDD->get('TOTAL_MONTANTBDC'):0;
							$result['TOTAL_CAFACTUREHT'] = ($search)?$bourseBDD->get('TOTAL_CAFACTUREHT'):0;
						} else {
							$result['TOTAL_CAFACTUREHT'] = ($search)?$bourseBDD->get('TOTAL_CAFACTUREHT'):0;
							$result['TOTAL_CAFACTURETTC'] = ($search)?$bourseBDD->get('TOTAL_CAFACTURETTC'):0;
						}

						//On initialise la navigation de la vue
						$bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-crmsociete', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_CRMSOCIETE'].'&onglet=6', 'facturation_');
						$tabFilters = $filters_array;
					}
				}
				break;
			default:
				return $this->singleExec($baseQuery);
		}
	}

	/**
	 * Create a new contact
	 * @param array $data tableau de données
	 * @param bool $solr trigger a SolR Sync
	 * @return int the contact ID
	 */
	public function newCRMData($data, $solr = true){
		if(isset($data['CONTACT'])) {

			if(!isset($data['CONTACT']['CCON_DATEUPDATE'])) $data['CONTACT']['CCON_DATEUPDATE'] = date('Y-m-d H:i:s');
			else $data['CONTACT']['CCON_DATEUPDATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['CONTACT']['CCON_DATEUPDATE'])->format('Y-m-d H:i:s');

			if(!isset($data['CONTACT']['CCON_DATE']))  $data['CONTACT']['CCON_DATE'] = date('Y-m-d H:i:s');
			else  $data['CONTACT']['CCON_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['CONTACT']['CCON_DATE'])->format('Y-m-d H:i:s');

			if(!isset($data['CONTACT']['CCON_APPLICATIONS'])) $data['CONTACT']['CCON_APPLICATIONS'] = '';
			if(!isset($data['CONTACT']['CCON_OUTILS'])) $data['CONTACT']['CCON_OUTILS'] = '';
			$idcontact=$this->insert('TAB_CRMCONTACT', $data['CONTACT']);

			// On ajoute les réseaux sociaux
			if(isset($data['WEBSOCIALS'])) foreach($data['WEBSOCIALS'] as $data_social){
				$data_social['ID_PARENT'] = $idcontact;
				$data_social['WEBSOCIAL_TYPE'] = 2;
				$this->insert('TAB_WEBSOCIAL', $data_social);
			}

			// On ajoute les influenceurs
			if(isset($data['INFLUENCERS'])) foreach($data['INFLUENCERS'] as $id_profil){
				$this->insert('TAB_INFLUENCER', [
					'ID_PROFIL' => $id_profil,
					'ID_ITEM' => $idcontact,
					'ITEM_TYPE' => 10
				]);
			}

			if($solr) SolR::instance()->Creation(BM::SOLR_MODULE_CRM_CONTACTS);

			return $idcontact;
		}

		return false;
	}

	/**
	 * delete a contact
	 * @param int $idcrm Contact ID
	 * @param bool $solr trigger a SolR sync
	 * @return true
	 */
	public function deleteCRMData($idcrm, $solr = true)
	{
		//On supprime toutes les actions de ce contact
		$dict = Services\Actions::getDictionaryForCategory(BM::CATEGORY_CRM_CONTACT);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id'));
		$where->and_('ID_PARENT = ?', $idcrm);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//On supprime tous les flags de cette action
		$flagsData = new AttachedFlag();
		$flagsData->removeAllAttachedFlagsFromEntity($idcrm, Models\AttachedFlag::TYPE_CONTACT);

		//On supprime les réseaux sociaux de la société
		$this->delete('TAB_WEBSOCIAL', 'ID_PARENT=? AND WEBSOCIAL_TYPE=?', [$idcrm, WebSocial::WEBSOCIAL_TYPE_CONTACT_CRM]);

		//On supprime les influenceurs
		$this->delete('TAB_INFLUENCER', 'ID_ITEM=? AND ITEM_TYPE=?', [$idcrm,BM::INFLUENCER_CRM_CONTACT]);

		//On supprime le contact
		$this->delete('TAB_CRMCONTACT', 'ID_CRMCONTACT=?', $idcrm);

		//On indique la suppression dans la table TAB_DELETED
		$this->insert('TAB_DELETED', [
			'ID_PARENT' => $idcrm,
			'DEL_TYPE' => BM::CATEGORY_CRM_CONTACT,
			'DEL_DATE' => date('Y-m-d H:i:s')
		]);

		if($solr) SolR::instance()->Suppression(BM::SOLR_MODULE_CRM_CONTACTS);

		return true;
	}

	/**
	 * Update a contact
	 * @param array $data data to update
	 * @param int $idcrm a contact ID
	 * @param bool $solr trigger a SolR sync
	 * @return true
	 */
	public function updateCRMData($data, $idcrm, $solr = true)
	{
		if(isset($data['SOCIETE'])) {
			$data['SOCIETE']['CSOC_DATEUPDATE'] = date('Y-m-d H:i:s');
			if(isset($data['SOCIETE']['CSOC_DATE'])) $data['SOCIETE']['CSOC_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['SOCIETE']['CSOC_DATE'])->format('Y-m-d H:i:s');
			$this->update('TAB_CRMSOCIETE', $data['SOCIETE'], 'ID_CRMSOCIETE=:id', ['id'=>$idcrm]);
		}

		if(isset($data['CONTACT'])) {
			$data['CONTACT']['CCON_DATEUPDATE'] = date('Y-m-d H:i:s');
			if(isset($data['CONTACT']['CCON_DATE'])) $data['CONTACT']['CCON_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['CONTACT']['CCON_DATE'])->format('Y-m-d H:i:s');
			$this->update('TAB_CRMCONTACT', $data['CONTACT'], 'ID_CRMCONTACT=:id', ['id'=>$idcrm]);
		}

		// on met à jour les réseaux sociaux
		//~ - passer un tableau vide pour supprimer tous les réseaux
		//~ - passer une url vide pour supprimer le réseau correspondant (même comportement que sur l'ui v6)
		//~ - passer le réseau et une url valide pour l'ajouter ou le modifier
		if(isset($data['WEBSOCIALS'])) {
			$existing_networks = Tools::getFieldsToArray(
				$this->exec('SELECT WEBSOCIAL_NETWORK FROM TAB_WEBSOCIAL WHERE ID_PARENT = ? AND WEBSOCIAL_TYPE = ?',
							[$idcrm, Models\WebSocial::WEBSOCIAL_TYPE_CONTACT_CRM])
				,'WEBSOCIAL_NETWORK');
			if($networks = array_column($data['WEBSOCIALS'], 'WEBSOCIAL_NETWORK')) {
				foreach($data['WEBSOCIALS'] as $data_social) {
					if(empty($data_social['WEBSOCIAL_URL'])){
						$this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ? AND WEBSOCIAL_NETWORK = ?',
						[$idcrm, Models\WebSocial::WEBSOCIAL_TYPE_CONTACT_CRM, $data_social['WEBSOCIAL_NETWORK']]);
					} else if(in_array($data_social['WEBSOCIAL_NETWORK'], $existing_networks)) {
						$this->update('TAB_WEBSOCIAL', $data_social,
							'ID_PARENT = :parent AND WEBSOCIAL_TYPE = :type AND WEBSOCIAL_NETWORK = :network',
							['parent' => $idcrm, 'type' => Models\WebSocial::WEBSOCIAL_TYPE_CONTACT_CRM, 'network' => $data_social['WEBSOCIAL_NETWORK']]);
					} else
						$this->insert('TAB_WEBSOCIAL', array_merge($data_social,[
							'WEBSOCIAL_TYPE' => Models\WebSocial::WEBSOCIAL_TYPE_CONTACT_CRM,
							'ID_PARENT' => $idcrm
						]));
				}
			} else $this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ?', [$idcrm, Models\WebSocial::WEBSOCIAL_TYPE_CANDIDATE]);
		}

		if(isset($data['INFLUENCERS']) && $data['INFLUENCERS']) {
			$inflResult = $this->exec('SELECT ID_PROFIL FROM TAB_INFLUENCER WHERE ID_ITEM=? AND ITEM_TYPE=10', $idcrm);
			$existingIDs = Tools::getFieldsToArray($inflResult, 'ID_PROFIL');
			$this->delete('TAB_INFLUENCER', 'ID_ITEM = ? AND ITEM_TYPE = 0 AND ID_PROFIL NOT IN (' . Where::prepareWhereIN($data['INFLUENCERS']) . ')', array_merge([$idcrm] , $data['INFLUENCERS']));

			foreach($data['INFLUENCERS'] as $id_profil) {
				if (!in_array($id_profil, $existingIDs))
					$this->insert('TAB_INFLUENCER', [
						'ID_PROFIL' => $id_profil,
						'ITEM_TYPE' => 10,
						'ID_ITEM'   => $idcrm
					]);
			}
		}
		if($solr) {
			SolR::instance()->Modification(BM::SOLR_MODULE_CRM_CONTACTS);
		}
		return true;
	}

	/**
	 * Check that a contact can be deleted
	 * @return true
	 */
	function isCRMReducible($id) {
		$nbDocument = 0;
		$sql = 'SELECT COUNT(ID_AO) AS NB_DOCUMENT FROM TAB_AO WHERE ID_CRMCONTACT = :id
				UNION ALL
				SELECT COUNT(ID_PROJET) AS NB_DOCUMENT FROM TAB_PROJET WHERE ID_CRMCONTACT= :id OR  ID_CRMTECHNIQUE = :id
				UNION ALL
				SELECT COUNT(ID_PROFIL) AS NB_DOCUMENT FROM TAB_PROFIL WHERE ID_CRMCONTACT = :id
				UNION ALL
				SELECT COUNT(ID_ACHAT) AS NB_DOCUMENT FROM TAB_ACHAT WHERE  ID_CRMCONTACT = :id';

		foreach($this->exec($sql, ['id'=>$id]) as $document) $nbDocument += $document['NB_DOCUMENT'];
		return ($nbDocument == 0);
	}
}
