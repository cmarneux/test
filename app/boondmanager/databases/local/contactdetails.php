<?php
/**
 * contactdetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\MySQL\Query;

/**
 * Class ContactDetails
 * @package BoondManager\Databases\Local
 */
class ContactDetails extends AbstractObject{

	/**
	 * retrieve the contactdetails information
	 * @param $id
	 * @return bool|\Wish\Models\Model
	 * @throws \Exception
	 */
	public function getContactDetails($id) {
		$query = new Query();

		$query->select()->from('TAB_COORDONNEES')->addWhere('ID_COORDONNEES = ?', $id);

		return $this->singleExec($query);
	}
}
