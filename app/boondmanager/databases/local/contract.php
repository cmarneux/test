<?php
/**
 * contracts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Contracts\Filters\SearchContracts;
use BoondManager\Services\BM;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\Model;
use Wish\Tools;
use Wish\MySQL\AbstractDb;
use BoondManager\OldModels\Filters;
use BoondManager\OldModels\MySQL\Mappers;

class Contract extends AbstractObject{

	/**
	 * update a contract
	 * @param array $data new data
	 * @param int $id contract id
	 * @return bool
	 * @throws \Exception
	 */
	public function updateContract($data, $id){
		if(isset($data['CONTRAT']))
			$this->update('TAB_CONTRAT', $data['CONTRAT'], 'ID_CONTRAT=:id', ['id'=>$id]);

		//On supprime les détails de frais qui n'existent plus
		if(isset($data['FRAISDETAILS'])) {
			$fraisDetails = $this->exec('SELECT ID_FRAISDETAILS FROM TAB_FRAISDETAILS WHERE ID_PARENT=? AND PARENT_TYPE=0', $id);

			$idsExists =Tools::getFieldsToArray($fraisDetails, 'ID_FRAISDETAILS');
			$idsInput = array_keys(Tools::useColumnAsKey('ID_FRAISDETAILS', $data['FRAISDETAILS']));

			$idIntersect = array_diff($idsExists, $idsInput);

			if($idIntersect) {
				$this->delete('TAB_FRAISDETAILS', 'ID_FRAISDETAILS IN ('.Where::prepareWhereIN($idIntersect).') AND ID_PARENT = ? AND PARENT_TYPE=0', array_merge($idIntersect,[$id]));
			}

			//On met à jour les détails de frais
			foreach($data['FRAISDETAILS'] as $data_frais) {
				if (isset($data_frais['ID_FRAISDETAILS'])){
					$this->update('TAB_FRAISDETAILS', $data_frais, 'ID_FRAISDETAILS=:idfr AND ID_PARENT=:id AND PARENT_TYPE=0', ['idfr'=>$data_frais['ID_FRAISDETAILS'], 'id'=>$id]);
				} else {//Ce détail de frais est nouveau
					unset($data_frais['ID_FRAISDETAILS']);
					$data_frais['ID_PARENT'] = $id;
					$data_frais['PARENT_TYPE'] = 0;
					$this->insert('TAB_FRAISDETAILS', $data_frais);
				}
			}
		}
		return true;
	}

	/**
	 * Delete a contract
	 * @param int $id contract id
	 * @return bool
	 * @throws \Exception
	 */
	public function deleteContract($id){
		$this->update('TAB_CONTRAT', ['ID_PARENT' => 0], 'ID_PARENT=:id', ['id' => $id]);
		$this->delete('TAB_CONTRAT', 'ID_CONTRAT=?', $id);
		$this->delete('TAB_AVANTAGE', 'ID_CONTRAT=?', $id);
		$this->delete('TAB_FRAISDETAILS', 'ID_PARENT=? AND PARENT_TYPE=0', $id);
		return true;
	}

	/**
	 * load a Contract
	 * @param int $id contract id
	 * @param \BoondManager\APIs\Advantages\Filters\SearchAdvantages $filter a search filter for advantages
	 * @return Model
	 * @throws \Exception
	 */
	public function getObject($id, $filter = null){
		//On récupère les informations RH et Paie de cet intervenant
		$result = $this->singleExec('SELECT CTR2.ID_CONTRAT AS ID_ENFANT, CTR1.ID_CONTRAT, CTR1.ID_SOCIETE, CTR1.ID_PROFIL, CTR1.ID_PARENT,
		                                     CTR1.CTR_SALAIREMENSUEL, CTR1.CTR_SALAIREHEURE, CTR1.CTR_FORCESALAIREHEURE, CTR1.CTR_CJMCONTRAT,
		                                     CTR1.CTR_CJMPRODUCTION, CTR1.CTR_FORCECJMPRODUCTION, CTR1.CTR_CHARGE, CTR1.CTR_FRSJOUR,
		                                     CTR1.CTR_FRSMENSUEL, CTR1.CTR_NBJRSOUVRE, CTR1.CTR_TYPE, CTR1.CTR_REGLESTEMPSEXCEPTION,
		                                     CTR1.CTR_DEBUT, CTR1.CTR_FIN, CTR1.CTR_DEVISE, CTR1.CTR_CHANGE, CTR1.CTR_DEVISEAGENCE,
		                                     CTR1.CTR_CHANGEAGENCE, CTR1.CTR_COMMENTAIRES, CTR1.CTR_AVANTAGES, CTR1.CTR_CLASSIFICATION,
		                                     CTR1.CTR_CATEGORIE, CTR1.CTR_TPSTRAVAIL, CTR1.CTR_DUREEHEBDOMADAIRE, CTR1.CTR_DATEPE1, CTR1.CTR_DATEPE2,
		                                     PROFIL_NOM, PROFIL_PRENOM, PROFIL_VISIBILITE, ID_RESPMANAGER, ID_RESPRH, PROFIL_TYPE,
		                                     TAB_PROFIL.ID_SOCIETE AS COMP_IDSOCIETE, USER_TYPE
		                              FROM TAB_CONTRAT CTR1 INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=CTR1.ID_PROFIL)
		                              LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL)
		                              LEFT JOIN TAB_CONTRAT CTR2 ON(CTR2.ID_PARENT=CTR1.ID_CONTRAT)
		                              WHERE CTR1.ID_CONTRAT=?', $id);

		/**
		 * @var \BoondManager\Models\Contract $result
		 */
		if(!$result)
			return false;

		//On récupère les détails des frais
		$sql = 'SELECT ID_FRAISDETAILS, FRSDETAILS_TYPE, FRSDETAILS_MONTANT, FRSDETAILS_TYPEFRSREF, TAB_FRAISDETAILS.ID_SOCIETE, TYPEFRS_NAME
		        FROM TAB_FRAISDETAILS
		        LEFT JOIN TAB_TYPEFRAIS ON(TYPEFRS_REF=FRSDETAILS_TYPEFRSREF AND TAB_TYPEFRAIS.ID_SOCIETE=TAB_FRAISDETAILS.ID_SOCIETE)
		        WHERE TAB_FRAISDETAILS.ID_PARENT=? AND PARENT_TYPE=0 ORDER BY ID_FRAISDETAILS ASC';

		$result['FRAISDETAILS'] = $this->exec($sql, $id);

		return $result;
	}

	/**
	 * @param $data
	 * @return bool|int
	 */
	public function newContratData($data)
	{
		if(isset($data['CONTRAT'])) {
			$idcontrat = $this->insert('TAB_CONTRAT', $data['CONTRAT']);

			if($data['FRAISDETAILS']) {
				foreach($data['FRAISDETAILS'] as $detail) {
					if(isset($detail['ID_FRAISDETAILS'])) unset($detail['ID_FRAISDETAILS']);
					$detail['ID_PARENT'] = $idcontrat;
					$detail['PARENT_TYPE'] = 0;
					$this->insert('TAB_FRAISDETAILS', $detail);
				}
			}
			return $idcontrat;
		}
		return false;
	}

	function isContratReducible($id) {
		$nbDocument = 0;
		$sql = 'SELECT COUNT(ID_MISSIONPROJET) AS NB_DOCUMENT FROM TAB_MISSIONPROJET WHERE ID_CONTRAT=:id
		        UNION ALL
		        SELECT COUNT(ID_AVANTAGE) AS NB_DOCUMENT FROM TAB_AVANTAGE WHERE ID_CONTRAT=:id';
		foreach($this->exec($sql, ['id'=>$id]) as $document) $nbDocument += $document['NB_DOCUMENT'];
		return ($nbDocument == 0);
	}

	/**
	 * perform a search on contract based on a given filter
	 * @param SearchContracts $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	public function search(SearchContracts $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->addWhere($this->getPerimeterSearch($filter->getSelectedPerimeter(), 'TAB_CONTRAT.ID_SOCIETE', 'TAB_CONTRAT.ID_POLE', ['USRESP.ID_PROFIL','USRH.ID_PROFIL']));

		//KEYWORDS
		if($keywords = $filter->keywords->getValue()) {
			$searchIds = $this->getListIdSearch($keywords, ['CTR'=>'TAB_CONTRAT.ID_CONTRAT', 'COMP'=>'TAB_PROFIL.ID_PROFIL']);
			if($searchIds->isEmpty()){
				$keywords2 = str_replace('*', '%', str_replace('%', '\%', $keywords));
				$searchIds = new Where('(PROFIL_NOM LIKE ? OR PROFIL_PRENOM LIKE ? OR PROFIL_REFERENCE LIKE ?)',[
					$keywords2.'%',
					$keywords2.'%',
					$keywords2
				]);
			}
			$query->addWhere($searchIds);
		}


		$query->addWhere( $this->getFilterSearch($filter->resourceStates->value, 'PROFIL_ETAT'));
		$query->addWhere( $this->getFilterSearch($filter->contractTypes->value, 'CTR_TYPE'));
		$query->addWhere( $this->getFilterSearch($filter->resourceTypes->value, 'PROFIL_TYPE'));

		//ON construit la liste des exclusions
		if($excludeTypes = $filter->excludeResourceTypes->getValue()){
			$query->addWhere('PROFIL_TYPE NOT IN (?)', $excludeTypes);
		}

		if($filter->period->getValue() == SearchContracts::PERIOD_OVERLAPS)
			$query->addWhere('DATEDIFF (?, CTR_DEBUT) >= 0 AND DATEDIFF(CTR_FIN,?) >= 0', [
				$filter->endDate->getValue(),
				$filter->startDate->getValue()
			]);

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());
		$query->setLimit(  $filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->from('TAB_CONTRAT
			INNER JOIN TAB_PROFIL USING(ID_PROFIL)
			LEFT JOIN TAB_USER USRESP ON(TAB_PROFIL.ID_RESPMANAGER = USRESP.ID_USER)
			LEFT JOIN TAB_USER USRH ON(TAB_PROFIL.ID_RESPRH = USRH.ID_USER)
			LEFT JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_CONTRAT.ID_SOCIETE)
			LEFT JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_CONTRAT.ID_SOCIETE)'
		);
		$query->groupBy('TAB_CONTRAT.ID_CONTRAT');
		$query->select(
			'TAB_PROFIL.ID_PROFIL, TAB_CONTRAT.ID_SOCIETE, SOCIETE_RAISON, PROFIL_NOM, PROFIL_PRENOM, PROFIL_REFERENCE, PROFIL_STATUT, 
			PROFIL_TYPE, TAB_CONTRAT.ID_CONTRAT, TAB_CONTRAT.ID_PARENT, CTR_TYPE, CTR_DEBUT, CTR_FIN, CTR_FRSJOUR, CTR_FRSMENSUEL, 
			CTR_DUREEHEBDOMADAIRE, CTR_SALAIREHEURE, CTR_SALAIREMENSUEL, CTR_CJMCONTRAT, CTR_CJMPRODUCTION, CTR_DATEPE1, CTR_DATEPE2, 
			CTR_CHARGE, CTR_NBJRSOUVRE, CTR_DEVISE, CTR_CHANGE, CTR_DEVISEAGENCE, CTR_CHANGEAGENCE, CTR_CLASSIFICATION, CTR_CATEGORIE, 
			CTR_TPSTRAVAIL, CTR_AVANTAGES, CTR_REGLESTEMPSEXCEPTION, GRPCONF_CALENDRIER'
		);

		return $this->launchSearch($query);
	}

	/**
	 * Add an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchContracts::ORDERBY_TYPE              => 'CTR_TYPE',
			SearchContracts::ORDERBY_RESOURCE_FUNCTION => 'TAB_PROFIL.PROFIL_STATUT',
			SearchContracts::ORDERBY_RESOURCE_LASTNAME => 'TAB_PROFIL.PROFIL_NOM',
			SearchContracts::ORDERBY_STARTDATE         => 'CTR_DEBUT',
			SearchContracts::ORDERBY_ENDDATE           => 'CTR_FIN',
		];

		if(!$column) $query->addOrderBy( 'TAB_PROFIL.ID_PROFIL ASC' );
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy( $mapping[$c].' '.$order);

		$query->addOrderBy( 'TAB_CONTRAT.ID_CONTRAT DESC');
	}
}
