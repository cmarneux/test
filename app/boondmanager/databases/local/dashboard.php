<?php
/**
 * dashboard.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use Wish\MySQL\AbstractDb;

/**
 * Handle all database work related to the dashboard
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Dashboard extends AbstractObject {

    /**
     * Retrieve all data for the dashboard
     * @TODO: adapter le code au lab (retour de la fonction, utilisation de l'utilisateur courant)
     */
	public function getObject() {
        throw new Exception("see TODO before use");
        //TOOD: gros nettoyage $this['OBJECT']
        switch($this->f3->get('SESSION.user.type')) {
            case 4:case 5://Administrateur/BoondManager
                if($this->f3->exists('SESSION.CLIENT'))
                    $sql = 'SELECT SUM(CASE WHEN USER_ABONNEMENT=3 THEN 1 ELSE 0 END) AS NB_COMPTESACTIF, SUM(CASE WHEN USER_ABONNEMENT=0 THEN 1 ELSE 0 END) AS NB_COMPTESINACTIF FROM TAB_USER WHERE USER_TYPE=2';
                else {
                    $sql = 'SELECT SUM(CASE WHEN AB_TYPE=3 THEN 1 ELSE 0 END) AS NB_CLIENTSACTIF, SUM(CASE WHEN AB_TYPE=3 THEN AB_NBMANAGERS ELSE 0 END) AS NB_MANAGERSACTIF,
                                   SUM(CASE WHEN AB_TYPE=1 THEN 1 ELSE 0 END) AS NB_CLIENTSDEMO, SUM(CASE WHEN AB_TYPE=1 THEN AB_NBMANAGERS ELSE 0 END) AS NB_MANAGERSDEMO
                            FROM TAB_ABONNEMENT';
                }
                $result = $this->exec($sql);
                if($result) {
                    $this['OBJECT'] = $result[0];

                    $login = $this->exec('SELECT USER_LOGIN, USER_SECURITYALERT, USER_SECURITYCOOKIE FROM TAB_USER WHERE ID_USER=?', $this->f3->get('SESSION.user.id'));
                    if(!$login) {
                        $login[0]['USER_LOGIN'] = '';
                        $login[0]['USER_SECURITYALERT'] = 0;
                        $login[0]['USER_SECURITYCOOKIE'] = 0;
                    }
                    $this['OBJECT'] = array_merge($this['OBJECT'], $login[0]);

                    //On récupère la liste des périphériques et des sessions
                    $deviceBDD = new Device();
                    $this['OBJECT']['DEVICES'] = $deviceBDD->getAllDevicesFromUser($this->f3->get('SESSION.user.id'))?$deviceBDD['DEVICES']:array();

                    if(!$this->f3->exists('SESSION.CLIENT')) {
                        //On récupère la durée min., max. et moyenne des abonnements
                        $duree = $this->exec('SELECT MIN(IF(AB_FIN>=AB_DEBUT,DATEDIFF(AB_FIN,AB_DEBUT)*12/365.25,0)) AS DUREE_MIN, MAX(IF(AB_FIN>=AB_DEBUT,DATEDIFF(AB_FIN,AB_DEBUT)*12/365.25,0)) AS DUREE_MAX, AVG(IF(AB_FIN>=AB_DEBUT,DATEDIFF(AB_FIN,AB_DEBUT)*12/365.25,0)) AS DUREE_AVG FROM TAB_ABONNEMENT WHERE AB_TYPE<>1');
                        if($duree) {
                            $duree[0]['DUREE_MIN'] = 0;
                            $duree[0]['DUREE_MAX'] = 0;
                            $duree[0]['DUREE_AVG'] = 0;
                        }
                        $this['OBJECT'] = array_merge($this['OBJECT'], $duree[0]);

                        //On récupère le CA total du mois en cours
                        $bourseBDD = new Reglements();
                        $bourseBDD->nb_max_per_pages = 1;
                        $this['OBJECT']['REGLEMENT_MOIS_EN_COURS'] = $bourseBDD->recherche(array('type_paiement' => -1))?$bourseBDD['TOTALHT']:0;
                    }
                    return true;
                }
                break;
            case 2://Manager
                //On récupère la page d'accueil du user
                $user = $this->exec('SELECT USER_HOMEPAGE FROM TAB_USER WHERE ID_USER=?', $this->f3->get('SESSION.user.id'));
                if($user) $this['OBJECT']['USER_HOMEPAGE'] = $user['USER_HOMEPAGE']; else $this->data['USER_HOMEPAGE'] = '';

                $version = $this->exec('SELECT BD_VERSION FROM TAB_BOONDMANAGER');
                if($version) $this['OBJECT']['BD_VERSION'] = $version['BD_VERSION']; else $this->data['BD_VERSION'] = '';

                //On récupère le nombre de candidats, ressources, besoins, projets, CRM et Actions de Rappels du manager + le CA, les coûts, la rentabilité et la marge de tous ses projets
                $candidatBourse = new Candidats();
                $candidatBourse->nb_max_per_pages = 1;
                $this['OBJECT']['NB_CANDIDATS'] = $candidatBourse->recherche(array('perimetre' => 0))?$candidatBourse['NB_ROWS']:0;

                $ressourceBourse = new Ressources();
                $ressourceBourse->nb_max_per_pages = 1;
                $this['OBJECT']['NB_RESSOURCES'] = $ressourceBourse->recherche(array('perimetre' => 0))?$ressourceBourse->getTotalRows():0;

                $besoinBourse = new Besoins();
                $besoinBourse->nb_max_per_pages = 1;
                $this['OBJECT']['NB_BESOINS'] = $besoinBourse->recherche(array('perimetre' => 0))?$besoinBourse['NB_ROWS']:0;

                $crmBourse = new CRM();
                $crmBourse->nb_max_per_pages = 1;
                $this['OBJECT']['NB_SOCIETES'] = $crmBourse->recherche(array('perimetre' => 0, 'type_fiche' => 0))?$crmBourse['NB_ROWS']:0;
                $this['OBJECT']['NB_CONTACTS'] = $crmBourse->recherche(array('perimetre' => 0, 'type_fiche' => 1))?$crmBourse['NB_ROWS']:0;

                $actionBourse = new Action();
                $actionBourse->nb_max_per_pages = 1;
                $this['OBJECT']['NB_RAPPELS'] = $actionBourse->recherche(array('perimetre' => 0))?$actionBourse['NB_ROWS']:0;

                $projetBourse = new Projets();
                $projetBourse->nb_max_per_pages = 1;
                $boolResult = $projetBourse->recherche(array('perimetre' => 0));
                $this['OBJECT']['NB_PROJETS'] = $boolResult?$projetBourse['NB_ROWS']:0;
                $this['OBJECT']['CA'] = $boolResult?$projetBourse['NB_ROWS']:0;
                $this['OBJECT']['COUT'] = $boolResult?$projetBourse['TOTAL_CA']:0;
                $this['OBJECT']['MARGE'] = $boolResult?$projetBourse['TOTAL_COUT']:0;
                $this['OBJECT']['RENTA'] = $boolResult?$projetBourse['TOTAL_MARGE']:0;
                $this['OBJECT']['NB_PROJETS'] = $boolResult?$projetBourse['TOTAL_RENTA']:0;
                return true;
                break;
            case 6://Ressource
                $result = $this->exec('SELECT USER_TPSFRSSTART, USER_HOMEPAGE, PROFIL_ALLOWCHANGE FROM TAB_USER INNER JOIN TAB_PROFIL USING(ID_PROFIL) WHERE TAB_USER.ID_PROFIL=?', $this->f3->get('SESSION.user.profil.id'));
                if($result) {
                    $this['OBJECT'] = $result[0];
                    $version = $this->exec('SELECT BD_VERSION FROM TAB_BOONDMANAGER');
                    $this['OBJECT']['BD_VERSION'] = $version?$version[0]['BD_VERSION']:'';
                    return true;
                }
                break;
        }
        return false;
    }
}
