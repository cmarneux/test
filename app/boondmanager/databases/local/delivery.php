<?php
/**
 * delivery.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Local;

use BoondManager\APIs\Contracts\Filters\SearchContracts;
use BoondManager\APIs\DeliveriesInactivitiesGroupments\Filters\SearchDeliveries;
use BoondManager\Lib\Currency;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use Wish\MySQL\Query;
use Wish\MySQL\Where;


/**
 * Handle all database work related to resources
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Delivery extends AbstractObject {
	/**
	 * Search deliveries
	 * @param SearchDeliveries $filter
	 * @return Models\SearchResults\Deliveries
	 * @throws \Exception
	 */
	public function searchDeliveries(SearchDeliveries $filter) {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		//Same columns as projects search
		$query->addColumns('PRJ.ID_PROJET, PRJ.ID_SOCIETE, PRJ.ID_POLE, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF, AO_TITLE, AO_APPLICATIONS, AO_INTERVENTION, PRJ_DEBUT,
								PRJ_FIN, PRJ_ADR, PRJ_CP, PRJ_VILLE, PRJ_PAYS, ID_PROFILCDP, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, PRJ_RESTEAFAIRE,
								PRJ_ETAT, PRJ_TARIFADDITIONNEL, PRJ_INVESTISSEMENT, TAB_CRMCONTACT.ID_CRMCONTACT, TAB_CRMCONTACT.ID_CRMSOCIETE, CCON_NOM,
								CCON_PRENOM, TAB_CRMSOCIETE.ID_CRMSOCIETE, CSOC_SOCIETE, PRJRESP.ID_PROFIL, PRJRESP.PROFIL_NOM, PRJRESP.PROFIL_PRENOM, ID_USER, ID_AO,
								SUM(IF(M1.ITEM_TYPE IN(0,1),1,0)) AS NB_PRESTATIONS');

		//Specific to deliveries search
		$query->addColumns('M1.ID_MISSIONPROJET, M1.ID_PARENT, M1.MP_NOM, M1.ID_ACHAT, M1.ID_MASTER, M1.MP_DEBUT, M1.MP_FIN, M1.ITEM_TYPE, M1.MP_CJM,
								M1.MP_TARIF, M1.MP_FRSJOUR, M1.MP_FRSMENSUEL, M1.MP_TARIFADDITIONNEL, M1.MP_INVESTISSEMENT, M1.MP_NBJRSFACTURE,
								M1.MP_NBJRSGRATUIT, M1.MP_TYPE, M1.MP_AVANTAGES, M1.MP_REGLESTEMPSEXCEPTION, M1.ID_CONTRAT, AT.PROFIL_NOM AS COMP_NOM,
								AT.PROFIL_PRENOM AS COMP_PRENOM, AT.ID_PROFIL AS COMP_IDPROFIL, AT.PROFIL_TYPE AS COMP_TYPE, TAB_PRODUIT.ID_PRODUIT, PRODUIT_NOM,
								PRODUIT_REF, PRODUIT_TYPE, PRODUIT_TAUXTVA, PRODUIT_TARIFHT, GRPCONF_CALENDRIER');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'PRJ.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$addData = $filter->sumAdditionalData->getValue();
		$notSmoothAddData = in_array($filter->period->getValue(),
			array(SearchDeliveries::PERIOD_PROJECT_RUNNING_AND_SUM,
				SearchDeliveries::PERIOD_PROJECT_RUNNING,
				SearchDeliveries::PERIOD_RUNNING_AND_SUM,
				SearchDeliveries::PERIOD_RUNNING)) && !Dictionary::getDict('specific.setting.smoothAdditionalData') ? true : false;

		$query->from('TAB_MISSIONPROJET M1');
		$query->addJoin('INNER JOIN TAB_PROJET PRJ ON(M1.ID_PROJET=PRJ.ID_PROJET)
							LEFT JOIN TAB_AO USING(ID_AO)
							LEFT JOIN TAB_PROFIL PRJRESP ON(PRJRESP.ID_PROFIL=PRJ.ID_PROFIL)
							LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=PRJRESP.ID_PROFIL)
							LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT)
							LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
							LEFT JOIN TAB_PROFIL AT ON(AT.ID_PROFIL=M1.ID_ITEM AND M1.ITEM_TYPE=0)
							LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=M1.ID_ITEM AND M1.ITEM_TYPE=1)
							LEFT JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=PRJ.ID_SOCIETE)');
		$query->groupBy('M1.ID_MISSIONPROJET');

		$currentUser =  CurrentUser::instance();
		$exchangeRate = $currentUser->getCurrency() != $currentUser->getCurrencyInDatabase() ? 'IF(PRJ_DEVISEAGENCE=' . $this->escape($currentUser->getCurrency()) . ',1/PRJ_CHANGEAGENCE,' . (1/$currentUser->getExchangeRate()) . ')*' : '';

		$columnsCACOUT = 'SUM(IF(M1.ID_PARENT=0,' . $exchangeRate . '(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL),0)) AS TOTAL_CASIMUHT'.
			', SUM(' . $exchangeRate . '(M1.MP_INVESTISSEMENT+M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT))) AS TOTAL_COUTSIMUHT';
		$columnsPRJNOTSMOOTHTOTAL = 'SUM(' . $exchangeRate . 'PRJ_TARIFADDITIONNEL)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
			', SUM(' . $exchangeRate . 'PRJ_INVESTISSEMENT)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJINVESTISSEMENT_PAR_PRESTATION';
		$columnsPRJ = 'SUM(' . $exchangeRate . 'PRJ_TARIFADDITIONNEL)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
			', SUM(' . $exchangeRate . 'PRJ_INVESTISSEMENT)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJINVESTISSEMENT_PAR_PRESTATION';
		$columnsMIS = 'SUM(' . $exchangeRate . 'PRJ_TARIFADDITIONNEL)/CASE WHEN COUNT(DISTINCT M3.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M3.ID_MISSIONPROJET)*COUNT(DISTINCT M3.ID_MISSIONPROJET) END AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
			', SUM(' . $exchangeRate . 'PRJ_INVESTISSEMENT)/CASE WHEN COUNT(DISTINCT M3.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M3.ID_MISSIONPROJET)*COUNT(DISTINCT M3.ID_MISSIONPROJET) END AS PRJINVESTISSEMENT_PAR_PRESTATION';

		$start = $this->escape($filter->startDate->getValue());
		$end = $this->escape($filter->endDate->getValue());

		$deliveryIntersectionPeriod = 'DATEDIFF(M1.MP_FIN,'.$start.')>=0 AND DATEDIFF('.$end.',M1.MP_DEBUT)>=0';
		$deliveryDetailsIntersectionPeriod = 'MD1.MISDETAILS_DATE BETWEEN '.$start.' AND '.$end;

		$projectIntersectionPeriod = 'DATEDIFF(PRJ_FIN,'.$start.')>=0 AND DATEDIFF('.$end.',PRJ_DEBUT)>=0';
		$projectDetailsIntersectionPeriod = 'MD2.MISDETAILS_DATE BETWEEN '.$start.' AND '.$end;

		$deliveryIntersectionNumberOfDays = '(DATEDIFF(LEAST(M1.MP_FIN,'.$end.'),GREATEST(M1.MP_DEBUT,'.$start.'))+1)';
		$projectIntersectionNumberOfDays = '(DATEDIFF(LEAST(PRJ_FIN,'.$end.'),GREATEST(PRJ_DEBUT,'.$start.'))+1)';

		$deliveryNumberOfDays = '(DATEDIFF(M1.MP_FIN,M1.MP_DEBUT)+1)';
		$projectNumberOfDays = '(DATEDIFF(PRJ_FIN,PRJ_DEBUT)+1)';

		$totalCount1 = '/(CASE WHEN COUNT(DISTINCT MD1.ID_MISSIONDETAILS)=0 THEN 1 ELSE COUNT(DISTINCT MD1.ID_MISSIONDETAILS) END * CASE WHEN COUNT(DISTINCT MD2.ID_MISSIONDETAILS)=0 THEN 1 ELSE COUNT(DISTINCT MD2.ID_MISSIONDETAILS) END)';
		$totalCount2 = '/CASE WHEN COUNT(DISTINCT MD2.ID_MISSIONDETAILS)=0 THEN 1 ELSE COUNT(DISTINCT MD2.ID_MISSIONDETAILS) END';
		$totalCount3 = '/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET)*COUNT(DISTINCT M1.ID_MISSIONPROJET) END';
		$totalCount4 = '/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END';
		$totalCount5 = '/CASE WHEN COUNT(DISTINCT M3.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M3.ID_MISSIONPROJET)*COUNT(DISTINCT M3.ID_MISSIONPROJET) END';

		switch($filter->period->getValue()){
			case SearchDeliveries::PERIOD_RUNNING_AND_SUM:case SearchDeliveries::PERIOD_RUNNING:
				$addData = $filter->period->getValue() == SearchDeliveries::PERIOD_RUNNING ? false : true;
				if($notSmoothAddData) {
					$query->addJoin('LEFT JOIN TAB_MISSIONDETAILS MD1 ON(MD1.PARENT_TYPE=1 AND MD1.ID_PARENT=M1.ID_MISSIONPROJET)'.
									' LEFT JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)');
					if($addData)
						$where->and_('('.$deliveryIntersectionPeriod.' OR '.$projectDetailsIntersectionPeriod.' AND (MD2.MISDETAILS_TARIF<>0 OR MD2.MISDETAILS_INVESTISSEMENT<>0) OR '.$deliveryDetailsIntersectionPeriod.')');
					else
						$where->and_('('.$deliveryIntersectionPeriod.' OR '.$deliveryDetailsIntersectionPeriod.')');
					$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'M1.MP_TARIF*M1.MP_NBJRSFACTURE/'.$deliveryNumberOfDays.',0))'.$totalCount1.
						'+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_TARIF,0))'.$totalCount2.' AS TOTAL_CASIMUHT, '.
						'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)/'.$deliveryNumberOfDays.',0))'.$totalCount1.
						'+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_INVESTISSEMENT,0))'.$totalCount2.' AS TOTAL_COUTSIMUHT';
					$columnsPRJ = 'SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount3.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
						', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount3.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
					$columnsPRJNOTSMOOTHTOTAL = 'SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
						', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
				} else {
					if($addData) {
						$where->and_('('.$deliveryIntersectionPeriod.' OR '.$projectIntersectionPeriod.' AND (PRJ_TARIFADDITIONNEL<>0 OR PRJ_INVESTISSEMENT<>0))');
						$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL)/'.$deliveryNumberOfDays.',0)) AS TOTAL_CASIMUHT'.
							', SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)+MP_INVESTISSEMENT)/'.$deliveryNumberOfDays.',0)) AS TOTAL_COUTSIMUHT';
					} else {
						$where->and_($deliveryIntersectionPeriod);
						$columnsCACOUT = 'SUM('.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL)/'.$deliveryNumberOfDays.') AS TOTAL_CASIMUHT'.
							', SUM('.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)+MP_INVESTISSEMENT)/'.$deliveryNumberOfDays.',0)) AS TOTAL_COUTSIMUHT';
					}
					$columnsMIS = 'SUM('.$projectIntersectionNumberOfDays.'*'.$exchangeRate.'PRJ_TARIFADDITIONNEL/'.$projectNumberOfDays.')'.$totalCount5.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
						', SUM('.$projectIntersectionNumberOfDays.'*'.$exchangeRate.'PRJ_INVESTISSEMENT/'.$projectNumberOfDays.')'.$totalCount5.' END AS PRJINVESTISSEMENT_PAR_PRESTATION';
				}
				break;
			case SearchDeliveries::PERIOD_PROJECT_RUNNING_AND_SUM:case SearchDeliveries::PERIOD_PROJECT_RUNNING:
				$addData = $filter->period->getValue() == SearchDeliveries::PERIOD_RUNNING ? false : true;
				if($notSmoothAddData) {
					$query->addJoin('LEFT JOIN TAB_MISSIONDETAILS MD1 ON(MD1.PARENT_TYPE=1 AND MD1.ID_PARENT=M1.ID_MISSIONPROJET)'.
									' LEFT JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)');
					if($addData)
						$where->and_('('.$projectIntersectionPeriod.' OR '.$projectDetailsIntersectionPeriod.' AND (MD2.MISDETAILS_TARIF<>0 OR MD2.MISDETAILS_INVESTISSEMENT<>0) OR '.$deliveryDetailsIntersectionPeriod.')');
					else
						$where->and_('('.$projectIntersectionPeriod.' OR '.$deliveryDetailsIntersectionPeriod.')');
					$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*(IF(M1.ID_PARENT=0,'.$exchangeRate.'M1.MP_TARIF*M1.MP_NBJRSFACTURE,0))/'.$deliveryNumberOfDays.',0))'.$totalCount1.
						'+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_TARIF,0))'.$totalCount2.' AS TOTAL_CASIMUHT, '.
						'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)/'.$deliveryNumberOfDays.',0))'.$totalCount1.
						'+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_INVESTISSEMENT,0))'.$totalCount2.' AS TOTAL_COUTSIMUHT';
					$columnsPRJ = ', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
						', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
					$columnsPRJNOTSMOOTHTOTAL = ', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
						', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
				} else {
					$where_expr[] = 'DATEDIFF(\''.$end.'\',PRJ_DEBUT)>=0 AND DATEDIFF(PRJ_FIN,\''.$start.'\')>=0';
					$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL)/'.$deliveryNumberOfDays.',0)) AS TOTAL_CASIMUHT, '.
						', SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)+MP_INVESTISSEMENT)/'.$deliveryNumberOfDays.',0)) AS TOTAL_COUTSIMUHT';
					$columnsPRJ = 'SUM('.$projectIntersectionNumberOfDays.'*'.$exchangeRate.'PRJ_TARIFADDITIONNEL/'.$projectNumberOfDays.')'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
						', SUM('.$projectIntersectionNumberOfDays.'*'.$exchangeRate.'PRJ_INVESTISSEMENT/'.$projectNumberOfDays.')'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
				}
				break;
			case SearchDeliveries::PERIOD_STARTED:
				$where->and_('PRJ_DEBUT BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchDeliveries::PERIOD_STOPPED:
				$where->and_('PRJ_FIN BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchDeliveries::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM:case SearchDeliveries::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE:
				$addData = $filter->period->getValue() == SearchDeliveries::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE;
				$query->addJoin('LEFT JOIN TAB_MISSIONDETAILS MD1 ON(MD1.PARENT_TYPE=1 AND MD1.ID_PARENT=M1.ID_MISSIONPROJET)'.
								' LEFT JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)'.
								' LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_PROJET=TAB_PROJET.ID_PROJET)');
				$where->and_('('.$projectIntersectionPeriod.' OR '.$deliveryDetailsIntersectionPeriod.' OR '.$projectDetailsIntersectionPeriod.' OR ACHAT_DATE BETWEEN \''.$start.'\' AND \''.$end.'\')');
				$columnsCACOUT = '0 AS TOTAL_CASIMUHT, 0 AS TOTAL_COUTSIMUHT';
				$columnsPRJ = '0 AS PRJTARIFADDITIONNEL_PAR_PRESTATION, 0 AS PRJINVESTISSEMENT_PAR_PRESTATION';
				break;
		}
		$query->addColumns($columnsCACOUT);
		$query->addCountColumns($columnsCACOUT);

		$keywordsMapping = ['MIS'=>'M1.ID_MISSIONPROJET', 'PRJ'=>'PRJ.ID_PROJET', 'COMP'=>'AT.ID_PROFIL', 'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE', 'AO'=>'TAB_AO.ID_AO', 'PROD'=>'TAB_PRODUIT.ID_PRODUIT',  'CTR'=>'M1.ID_CONTRAT'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if(!$whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('MATCH(AO_TITLE, AO_DESCRIPTION) AGAINST(?)', $keywords);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch)
				->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
				->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
				->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
				->or_('AT.PROFIL_PRENOM LIKE ?', $likeSearch)
				->or_('AT.PROFIL_NOM LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_($this->getFilterSearch($filter->projectStates->getValue(), 'PRJ_ETAT'))
			->and_($this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF'))
			->and_($this->getFilterSearch($filter->deliveryStates->getValue(), 'M1.MP_TYPE'))
			->and_($this->getFilterSearch($filter->activityAreas->getValue(), 'AO_APPLICATIONS'))
			->and_($this->getFilterSearch($filter->expertiseAreas->getValue(), 'AO_INTERVENTION'));

		if(!$filter->showGroupment->getValue()) $where->and_('M1.ITEM_TYPE IN('.Models\Delivery::TYPE_RESOURCE.','.Models\Delivery::TYPE_PRODUCT.')');

		if(!$filter->showInactivity->getValue()) $where->and_('PRJ_TYPE>0');

		if($filter->returnNbCorrelatedOrders->getValue()) {
			$query->addColumns('COUNT(DISTINCT ID_BONDECOMMANDE) AS NB_COR');
			$query->addJoin('LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=M1.ID_MISSIONPROJET AND CORBDC_TYPE=0)');
		}

		switch ($filter->transferType->getValue()) {
			case SearchDeliveries::TRANSFER_MASTER:
				$query->addJoin('LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)');
				$where->and_('M1.ID_MASTER=0 AND M2.ID_MISSIONPROJET IS NOT NULL');
				break;
			case SearchDeliveries::TRANSFER_SLAVE:
				$where->and_('M1.ID_MASTER<>0');
				break;
			case SearchDeliveries::TRANSFER_NONE:
				$query->addJoin('LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)');
				$where->and_('M1.ID_MASTER=0 AND M2.ID_MISSIONPROJET IS NULL');
				break;
			case SearchDeliveries::TRANSFER_NOT_SLAVE:
				$where->and_('M1.ID_MASTER=0');
				break;
			case SearchDeliveries::TRANSFER_SLAVE_INFO:
				$query->addJoin('LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)');
				$query->addColumns('M2.ID_MISSIONPROJET AS SLAVE_IDMISSIONPROJET');
				break;
			case SearchDeliveries::TRANSFER_NOT_MASTER:
				$query->addJoin('LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)');
				$where->and_('M2.ID_MISSIONPROJET IS NULL');
				break;
		}

		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PROJECT.' AND TAB_FLAG.ID_PARENT=PRJ.ID_PROJET)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		/** @var Models\SearchResults\Deliveries $result */
		$result = $this->launchSearch($query, Models\SearchResults\Deliveries::class);

		if($result->total > 0) {
			$currency = new Currency();
			if ($notSmoothAddData) {
				$result->turnoverSimulatedExcludingTax = 0;
				$result->costsSimulatedExcludingTax = 0;

				foreach ($result->rows as $i => $resultat) {
					$result->rows[$i]['TOTAL_CASIMUHT'] = 0;
					$result->rows[$i]['TOTAL_COUTSIMUHT'] = 0;
				}

				if ($addData) {
					$query->resetOrder()
						->replaceColumns('PRJ.ID_PROJET')
						->resetCountColumns()
						->addOrderBy('PRJ.ID_PROJET DESC');

					$nbPages = ceil($result->total / BM::getNumberMaxOfDatabaseResults());
					for ($k = 1; $k <= $nbPages; $k++) {
						$query->setLimit(BM::getNumberMaxOfDatabaseResults(), Query::getOffset($k, BM::getNumberMaxOfDatabaseResults()));
						$tabIDs = array();
						$allResults = $this->exec($query);
						foreach ($allResults as $resultat) $tabIDs[] = $resultat['ID_PROJET'];

						$queryTMP = new Query();
						$queryTMP->addColumns(['TAB_PROJET.ID_PROJET', $columnsPRJNOTSMOOTHTOTAL]);
						$queryTMP->from('TAB_PROJET');
						$queryTMP->addJoin('INNER JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)' .
							' LEFT JOIN TAB_MISSIONPROJET M1 ON(M1.ID_PROJET=TAB_PROJET.ID_PROJET)');
						$queryTMP->groupBy('TAB_PROJET.ID_PROJET');

						$whereTMP = new Where();
						$whereTMP->and_('TAB_PROJET.ID_PROJET IN(' . implode(',', $tabIDs) . ')');

						$queryTMP->addWhere($whereTMP);

						$projets = $this->exec($queryTMP);
						foreach ($projets as $ca) {
							$result->turnoverSimulatedExcludingTax += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
							$result->costsSimulatedExcludingTax += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
						}
					}

					$tabIDs = array();
					foreach ($result->rows as $i => $resultat) if (!in_array($resultat['ID_PROJET'], $tabIDs)) $tabIDs[] = $resultat['ID_PROJET'];

					$queryTMP = new Query();
					$queryTMP->addColumns(['TAB_PROJET.ID_PROJET', $columnsPRJ]);
					$queryTMP->from('TAB_PROJET');
					$queryTMP->addJoin('INNER JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)' .
						' LEFT JOIN TAB_MISSIONPROJET M1 ON(M1.ID_PROJET=TAB_PROJET.ID_PROJET)');
					$queryTMP->groupBy('TAB_PROJET.ID_PROJET');

					$whereTMP = new Where();
					$whereTMP->and_('TAB_PROJET.ID_PROJET IN(' . implode(',', $tabIDs) . ')');

					$queryTMP->addWhere($whereTMP);

					$details = $this->exec($queryTMP);
					foreach ($result->rows as $i => $resultat)
						foreach ($details as $ca) {
							if ($ca['ID_PROJET'] == $resultat['ID_PROJET']) {
								$result->rows[$i]['TOTAL_CASIMUHT'] += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
								$result->rows[$i]['TOTAL_COUTSIMUHT'] += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
								break;
							}
						}
				}

				$query->resetOrder()
					->replaceColumns(['M1.ID_MISSIONPROJET', 'M1.ID_PROJET', 'M1.ID_PARENT', $columnsCACOUT])
					->resetCountColumns()
					->groupBy('M1.ID_MISSIONPROJET');;

				$count = $this->singleExec($query->getCountQuery(), $query->getArgs());
				$nbPages = $count ? ceil($count['NB_ROWS'] / BM::getNumberMaxOfDatabaseResults()) : 0;
				for ($k = 1; $k <= $nbPages; $k++) {
					$query->setLimit(BM::getNumberMaxOfDatabaseResults(), Query::getOffset($k, BM::getNumberMaxOfDatabaseResults()));
					$missions = $this->exec($query);
					foreach ($missions as $ca) {
						$result->turnoverSimulatedExcludingTax += $ca['TOTAL_CASIMUHT'];
						$result->costsSimulatedExcludingTax += $ca['TOTAL_COUTSIMUHT'];
					}

					foreach ($result->rows as $i => $resultat) {
						foreach ($missions as $ca) {
							if ($ca['ID_PROJET'] == $resultat['ID_PROJET']) {
								if ($ca['ID_PARENT'] == 0) $result->rows[$i]['TOTAL_CASIMUHT'] += $ca['TOTAL_CASIMUHT'];
								$result->rows[$i]['TOTAL_COUTSIMUHT'] += $ca['TOTAL_COUTSIMUHT'];
							}
						}
					}
				}
			} else if ($addData) {
				$query->resetOrder()
					->replaceColumns($columnsMIS)
					->addJoin('LEFT JOIN TAB_MISSIONPROJET M3 ON(M3.ID_PROJET=PRJ.ID_PROJET)')
					->resetCountColumns()
					->groupBy('M1.ID_MISSIONPROJET');

				$count = $this->singleExec($query->getCountQuery(), $query->getArgs());
				$nbPages = $count ? ceil($count['NB_ROWS'] / BM::getNumberMaxOfDatabaseResults()) : 0;
				for ($k = 1; $k <= $nbPages; $k++) {
					$query->setLimit(BM::getNumberMaxOfDatabaseResults(), Query::getOffset($k, BM::getNumberMaxOfDatabaseResults()));
					$additionnel = $this->exec($query);
					foreach ($additionnel as $ca) {
						$result->turnoverSimulatedExcludingTax += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
						$result->costsSimulatedExcludingTax += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
					}
				}

				$tabIDs = array();
				foreach ($result->rows as $i => $resultat) if (!in_array($resultat['ID_MISSIONPROJET'], $tabIDs)) $tabIDs[] = $resultat['ID_MISSIONPROJET'];

				$where->and_('M1.ID_MISSIONPROJET IN(' . implode(',', $tabIDs) . ')');
				$query->resetWhere()
					->replaceColumns(['M1.ID_MISSIONPROJET', $columnsMIS])
					->addWhere($where);

				$tabAdditionnels = $this->exec($query);
				foreach ($result->rows as $i => $resultat) {
					foreach ($tabAdditionnels as $ca) {
						if ($ca['ID_MISSIONPROJET'] == $resultat['ID_MISSIONPROJET']) {
							$result->rows[$i]['TOTAL_CASIMUHT'] += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
							$result->rows[$i]['TOTAL_COUTSIMUHT'] += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
						}
					}
				}
			}

			if ($currentUser->getCurrency() != $currentUser->getCurrencyInDatabase()) {
				$result->turnoverSimulatedExcludingTax = $currency->getAmountForBDD($result->turnoverSimulatedExcludingTax);
				$result->costsSimulatedExcludingTax = $currency->getAmountForBDD($result->costsSimulatedExcludingTax);
			}

			$result->marginSimulatedExcludingTax = $result->turnoverSimulatedExcludingTax - $result->costsSimulatedExcludingTax;
			if(BM::isProfitabilityCalculatingBasedOnMarginRate())
				$result->profitabilitySimulated = $result->costsSimulatedExcludingTax == 0 ? 0 : 100 * $result->marginSimulatedExcludingTax / $result->costsSimulatedExcludingTax;
			else
				$result->profitabilitySimulated = $result->turnoverSimulatedExcludingTax == 0 ? 0 : 100 * $result->marginSimulatedExcludingTax / $result->turnoverSimulatedExcludingTax;

			foreach ($result->rows as $i => $resultat) {
				if ($currentUser->getCurrency() != $currentUser->getCurrencyInDatabase()) {
					$result->rows[$i]['TOTAL_CASIMUHT'] = $currency->getAmountForBDD($result->rows[$i]['TOTAL_CASIMUHT']);
					$result->rows[$i]['TOTAL_COUTSIMUHT'] = $currency->getAmountForBDD($result->rows[$i]['TOTAL_COUTSIMUHT']);
				}

				$result->rows[$i]['TOTAL_MARGESIMUHT'] = $result->rows[$i]['TOTAL_CASIMUHT'] - $result->rows[$i]['TOTAL_COUTSIMUHT'];
				$result->rows[$i]['TOTAL_RENTASIMU'] = $result->rows[$i]['TOTAL_CASIMUHT'] == 0 ? 0 : $result->rows[$i]['TOTAL_MARGESIMUHT'] / $result->rows[$i]['TOTAL_CASIMUHT'];
			}
		}
		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchDeliveries::ORDERBY_STARTDATE => 'M1.MP_DEBUT',
			SearchDeliveries::ORDERBY_ENDDATE => 'M1.MP_FIN',
			SearchDeliveries::ORDERBY_ID => 'M1.ID_MISSIONPROJET',
			SearchDeliveries::ORDERBY_PROJECT_REFERENCE => 'PRJ_REFERENCE',
			SearchDeliveries::ORDERBY_PROJECT_COMPANY_NAME => 'CSOC_SOCIETE',
			SearchDeliveries::ORDERBY_PROJECT_MAINMANAGER_LASTNAME => 'PRJRESP.PROFIL_NOM',
			SearchDeliveries::ORDERBY_GROUPMENT_ID => 'M1.ID_PARENT'
		];

		if(!$column) $query->addOrderBy('M1.MP_DEBUT DESC');

		$addDefaultOrder = true;

		foreach($column as $c)
			if(array_key_exists($c, $mapping)) {
				$query->addOrderBy($mapping[$c] . ' ' . $order);
				if($c == SearchDeliveries::ORDERBY_GROUPMENT_ID)
					$query->addOrderBy('M1.MP_FIN DESC');
				if($c == SearchDeliveries::ORDERBY_ID)
					$addDefaultOrder = false;
			}

		if($addDefaultOrder) $query->addOrderBy('M1.ID_MISSIONPROJET DESC');
	}


	/**
	 * Retrieve all employee's projects for a given period
	 * @param int $id
	 * @param string $startDate
	 * @param string $endDate
	 * @return false|int|\Wish\Models\Model[]
	 */
	public function searchDeliveriesAndBatchesForEmployeeOnPeriod($id, $startDate, $endDate)
	{
		$sql ='SELECT 
		            TAB_MISSIONPROJET.ID_MISSIONPROJET, TAB_PROJET.ID_PROJET, PRJ_REFERENCE, MP_NOM, 
		            TAB_PROJET.ID_CRMSOCIETE, CSOC_SOCIETE, ID_AO, MP_DEBUT, MP_FIN, ID_PROFILCDP, 
		            TAB_LOT.ID_LOT, LOT_TITRE
		       FROM TAB_MISSIONPROJET 
		       INNER JOIN TAB_PROJET USING(ID_PROJET) 
		       LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT) 
		       LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		       LEFT JOIN TAB_LOT USING(ID_PROJET) 
		       LEFT JOIN TAB_JALON ON(TAB_JALON.ID_LOT=TAB_LOT.ID_LOT AND TAB_JALON.ID_PROJET=TAB_PROJET.ID_PROJET)
		       WHERE 
		           TAB_MISSIONPROJET.ID_ITEM=:id AND ITEM_TYPE=0 AND ID_MASTER=0 AND MP_TYPE<>1 AND PRJ_TYPE>0 
		           AND DATEDIFF(:endDate, MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,:startDate)>=0 
		           AND (TAB_JALON.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM OR TAB_JALON.ID_PROFIL=0 OR TAB_LOT.ID_LOT IS NULL OR TAB_JALON.ID_JALON IS NULL) 
		       GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, TAB_LOT.ID_LOT 
		       ORDER BY TAB_PROJET.ID_PROJET ASC, TAB_MISSIONPROJET.ID_MISSIONPROJET ASC, TAB_LOT.ID_LOT';
		$result = $this->exec($sql, ['id' => $id, 'endDate' => $endDate, 'startDate' => $startDate]);
		return $result;
	}

	/**
	 * Delete a delivery
	 * @param $id
	 * @throws \Exception
	 */
	public function deleteDelivery($id) {
		$this->delete('TAB_AVANTAGE', 'ID_MISSIONPROJET=?', $id);
		$this->delete('TAB_MISSIONDETAILS', 'ID_PARENT=? AND PARENT_TYPE=1', $id);
		$this->delete('TAB_FRAISDETAILS', 'ID_PARENT=? AND PARENT_TYPE=1', $id);
		$this->delete('TAB_CORRELATIONBONDECOMMANDE', 'ID_ITEM=? AND CORBDC_TYPE=0', $id);
		$this->update('TAB_MISSIONPROJET', ['ID_MASTER' => 0],'ID_MASTER=:id', ['id' => $id]);	//On supprime la référence du maître sur la mission cédée
		$this->update('TAB_MISSIONPROJET', ['ID_PARENT' => 0],'ID_PARENT=:id', ['id' => $id]); //On supprime du groupement
		$this->delete('TAB_MISSIONPROJET', 'ID_MISSIONPROJET=?', $id);
	}

	/**
	 * Does delivery reducible ?
	 * @param int $id
	 * @return boolean
	 */
	function isDeliveryReducible($id) {
		$nbDocument = 0;
		$query = 'SELECT COUNT(ID_MISSIONPROJET) AS NB_DOCUMENT FROM TAB_MISSIONPROJET WHERE ID_MASTER=:id
				UNION ALL
				SELECT COUNT(ID_LIGNETEMPS) AS NB_DOCUMENT FROM TAB_LIGNETEMPS WHERE ID_MISSIONPROJET=:id
				UNION ALL
				SELECT COUNT(ID_LIGNEFRAIS) AS NB_DOCUMENT FROM TAB_LIGNEFRAIS WHERE ID_MISSIONPROJET=:id
				UNION ALL
				SELECT COUNT(ID_AVANTAGE) AS NB_DOCUMENT FROM TAB_AVANTAGE WHERE ID_MISSIONPROJET=:id
				UNION ALL
				SELECT COUNT(ID_FRAISREEL) AS NB_DOCUMENT FROM TAB_FRAISREEL WHERE ID_MISSIONPROJET=:id
				UNION ALL
				SELECT COUNT(ID_MISSIONDETAILS) AS NB_DOCUMENT FROM TAB_MISSIONDETAILS WHERE ID_PARENT=:id AND PARENT_TYPE=1 AND ID_ACHAT<>0
				UNION ALL
				SELECT COUNT(ID_TEMPSEXCEPTION) AS NB_DOCUMENT FROM TAB_TEMPSEXCEPTION WHERE ID_MISSIONPROJET=:id
				UNION ALL
				SELECT COUNT(ID_CORRELATIONBONDECOMMANDE) AS NB_DOCUMENT FROM TAB_CORRELATIONBONDECOMMANDE WHERE ID_ITEM=:id AND CORBDC_TYPE=0';
		foreach($this->exec($query, ['id'=>$id]) as $document) $nbDocument += $document['NB_DOCUMENT'];
		return ($nbDocument == 0);
	}


	/**
	 * Retrieve a delivery
	 * @param int $id delivery id
	 * @return bool|\Wish\Models\Model
	 */
	public function getDelivery($id)
	{
		$sql = 'SELECT TAB_MISSIONPROJET.ID_PROJET, ID_MISSIONPROJET, ID_MASTER, MP_CJM, MP_PRJM, MP_FRSJOUR, MP_FRSMENSUEL, 
		               MP_NBJRSFACTURE, MP_NBJRSGRATUIT, MP_NBJRSOUVRE, MP_COMMENTAIRES, ID_CRMTECHNIQUE, TAB_MISSIONPROJET.ID_ACHAT, 
		               TAB_MISSIONPROJET.ID_CONTRAT, TAB_MISSIONPROJET.ID_PARENT, MP_DEBUT, MP_FIN, MP_NOM, MP_TARIF, MP_TARIFADDITIONNEL, 
		               MP_INVESTISSEMENT, MP_TYPE, MP_AVANTAGES, MP_CONDITIONS, MP_TARIFHEURE, MP_FORCETARIFHEURE, MP_DUREEHEBDOMADAIRE, 
		               MP_REGLESTEMPSEXCEPTION, TAB_PROJET.ID_AO, PRJ_REFERENCE, PRJ_DEBUT, PRJ_FIN, PRJ_DEVISE, PRJ_CHANGE, PRJ_DEVISEAGENCE, 
		               PRJ_CHANGEAGENCE, TAB_MISSIONPROJET.ID_ITEM, ITEM_TYPE, PRJ_ADR, PRJ_CP, PRJ_VILLE, PRJ_PAYS, TAB_PROJET.ID_CRMCONTACT, 
		               TAB_PROJET.ID_PROFIL AS PRJ_IDPROFIL, TAB_AO.ID_PROFIL AS AO_IDPROFIL, AO_REF, TAB_PROJET.ID_CRMSOCIETE, CSOC.CSOC_SOCIETE, 
		               CSOC.CSOC_BAREMESEXCEPTION, CSOC.CSOC_ADR, CSOC.CSOC_CP, CSOC.CSOC_VILLE, CSOC.CSOC_PAYS, CCON.CCON_NOM, CCON.CCON_PRENOM, 
		               CCON.CCON_TEL1, CCON.CCON_TEL2, CCON.CCON_SERVICE, CSTECH.ID_CRMSOCIETE AS CSTECH_ID, CSTECH.CSOC_SOCIETE AS CSTECH_SOCIETE, CSTECH.CSOC_ADR AS CSTECH_ADR, 
		               CSTECH.CSOC_CP AS CSTECH_CP, CSTECH.CSOC_VILLE AS CSTECH_VILLE, CSTECH.CSOC_PAYS AS CSTECH_PAYS, CCTECH.CCON_NOM AS CCTECH_NOM, 
		               CCTECH.CCON_PRENOM AS CCTECH_PRENOM, CCTECH.CCON_TEL1 AS CCTECH_TEL1, CCTECH.CCON_TEL2 AS CCTECH_TEL2, 
		               CCTECH.CCON_SERVICE AS CCTECH_SERVICE, PROFIL_NOM, PROFIL_PRENOM, PROFIL_EMAIL, TAB_USER.ID_USER, TAB_USER.USER_TYPE, 
		               PROFIL_TYPE, PROFIL_VISIBILITE, TAB_PROFIL.ID_SOCIETE, TAB_PROFIL.ID_POLE, ID_RESPMANAGER, ID_RESPRH, AO_TITLE, 
		               TAB_PROJET.ID_SOCIETE AS PRJ_SOCIETE, TAB_PROJET.ID_POLE AS PRJ_POLE, TAB_AO.ID_SOCIETE AS AO_SOCIETE, 
		               TAB_AO.ID_POLE AS AO_POLE, PRODUIT_NOM, PRODUIT_REF, PRODUIT_TYPE, PRJ_TYPE, PRJ_TYPEREF,
		               UMANAGER.ID_PROFIL AS ID_PROFIL_RESPMANAGER, URH.ID_PROFIL AS ID_PROFIL_RESPRH
		        FROM TAB_MISSIONPROJET 
		        INNER JOIN TAB_PROJET USING(ID_PROJET) 
		        LEFT JOIN TAB_AO USING(ID_AO)
		        LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=0) 
		        LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL)
		        LEFT JOIN TAB_USER AS UMANAGER ON(UMANAGER.ID_USER = TAB_PROFIL.ID_RESPMANAGER)
		        LEFT JOIN TAB_USER AS URH ON(URH.ID_USER = TAB_PROFIL.ID_RESPRH)
		        LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=1)
		        LEFT JOIN TAB_CRMCONTACT CCON ON(CCON.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) 
		        LEFT JOIN TAB_CRMSOCIETE CSOC ON(CSOC.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		        LEFT JOIN TAB_CRMCONTACT CCTECH ON(CCTECH.ID_CRMCONTACT=TAB_PROJET.ID_CRMTECHNIQUE) 
		        LEFT JOIN TAB_CRMSOCIETE CSTECH ON(CSTECH.ID_CRMSOCIETE=CCTECH.ID_CRMSOCIETE)
		        WHERE ID_MISSIONPROJET=?';
		$result = $this->singleExec($sql, $id);
		if($result) {
			if($result['PRJ_TYPE'] > 0) {
				switch($result['ITEM_TYPE']) {
					case 5:case 6:case 7:
						$filter = new SearchDeliveries();
						$filter->disableMaxResultLimit();
						$filter->setIndifferentPerimeter();
						$filter->order->setValue(SearchDeliveries::ORDER_DESC);
						$filter->sort->setValue(SearchDeliveries::ORDERBY_ENDDATE);
						$filter->sumAdditionalData->setValue(false);
						$filter->returnNbCorrelatedOrders->setValue(false);
						$filter->transferType->setValue(SearchDeliveries::TRANSFER_SLAVE_INFO);
						$filter->keywords->setValue(Models\Project::buildReference($result['ID_PROJET']));
						$filter->projectTypes->setValue($result['PRJ_TYPEREF']);

						$resultSubDel = self::searchDeliveries($filter);

						$resultSubDel->rows = array_filter($resultSubDel->rows, function($mission) use ($result) {
							return ( ! ($mission['ID_PARENT'] != 0 && $mission['ID_PARENT'] != $result['ID_MISSIONPROJET']));
						});

						$result['MISSIONS'] = $resultSubDel;
					break;
				}

				//On récupère tous les détails de mission
				$result['MISSIONDETAILS'] = $this->exec('SELECT TAB_MISSIONDETAILS.ID_ACHAT, ID_MISSIONDETAILS, MISDETAILS_DATE, MISDETAILS_ETAT, MISDETAILS_TARIF, MISDETAILS_INVESTISSEMENT, MISDETAILS_TITRE, ACHAT_TYPE FROM TAB_MISSIONDETAILS LEFT JOIN TAB_ACHAT USING(ID_ACHAT) WHERE ID_PARENT=? AND PARENT_TYPE=1 ORDER BY ID_MISSIONDETAILS ASC', $result['ID_MISSIONPROJET']);
			}

			//On récupère tous les documents
			$sql = 'SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=7';
			$result['DOCUMENTS'] = $this->exec($sql, $result['ID_MISSIONPROJET']);

			//On va récupérer les informations de détails des frais
			$result['FRAISDETAILS'] = $this->exec(
				'SELECT ID_FRAISDETAILS, FRSDETAILS_TYPE, FRSDETAILS_MONTANT, FRSDETAILS_TYPEFRSREF, TAB_FRAISDETAILS.ID_SOCIETE, TYPEFRS_NAME 
				 FROM TAB_FRAISDETAILS 
				 LEFT JOIN TAB_TYPEFRAIS ON(TYPEFRS_REF=FRSDETAILS_TYPEFRSREF AND TAB_TYPEFRAIS.ID_SOCIETE=TAB_FRAISDETAILS.ID_SOCIETE) 
				 WHERE TAB_FRAISDETAILS.ID_PARENT=? AND PARENT_TYPE=1 
				 ORDER BY ID_FRAISDETAILS ASC
				',
				$result['ID_MISSIONPROJET']
			);

			//On récupère les contrats qui couvrent la période
			if($result['ITEM_TYPE'] == 0) {
				$contractFilter = new SearchContracts();
				$contractFilter->disableMaxResultLimit();
				$contractFilter->setIndifferentPerimeter();
				$contractFilter->keywords->setValue( Models\Employee::buildReference($result['ID_ITEM']));
				$contractFilter->period->setValue( SearchContracts::PERIOD_OVERLAPS );
				$contractFilter->startDate->setValue($result['MP_DEBUT']);
				$contractFilter->endDate->setValue($result['MP_FIN']);
				$contractFilter->order->setValue(SearchContracts::ORDER_DESC);
				$contractFilter->sort->setValue(SearchContracts::ORDERBY_ENDDATE);

				$result['CONTRATS'] = Contract::instance()->search($contractFilter);
			}

			if($result['ID_MASTER'] > 0) {//On cherche les données de la mission maître
				$result['MASTER'] = $this->singleExec(
					'SELECT ID_MISSIONPROJET, ID_ITEM, ITEM_TYPE, TAB_MISSIONPROJET.ID_PROJET, MP_CJM, MP_FRSJOUR, 
					        MP_FRSMENSUEL, MP_NBJRSOUVRE, MP_CJM, MP_NBJRSFACTURE, MP_NBJRSGRATUIT, MP_DEBUT, MP_FIN, 
					        PRJ_TYPE, PRJ_TYPEREF, PRJ_CHANGEAGENCE, PRJ_DEVISEAGENCE, PRJ_CHANGE,PRJ_DEVISE, 
					        TAB_PROJET.ID_PROFIL AS PRJ_IDPROFIL, PRJ_REFERENCE, :lastname AS PROFIL_NOM, :firstname AS PROFIL_PRENOM 
					 FROM TAB_MISSIONPROJET 
					 INNER JOIN TAB_PROJET USING(ID_PROJET) 
					 WHERE TAB_MISSIONPROJET.ID_MISSIONPROJET= :id',
					[
						'id' => $result['ID_MASTER'],
						'lastname' => $result['PROFIL_NOM'],
						'firstname' => $result['PROFIL_PRENOM']
					]
				);
				if(!$result['MASTER']) unset($result['MASTER']);
			} else {//On cherche les données d'une hypothétique mission fille
				$result['SLAVE'] = $this->singleExec(
					'SELECT ID_MISSIONPROJET, ID_ITEM, ITEM_TYPE, TAB_MISSIONPROJET.ID_PROJET, MP_CJM, MP_FRSJOUR, 
					        MP_FRSMENSUEL, MP_NBJRSOUVRE, MP_CJM, MP_NBJRSFACTURE, MP_NBJRSGRATUIT, MP_DEBUT, MP_FIN, 
					        PRJ_TYPE, PRJ_TYPEREF, PRJ_CHANGEAGENCE, PRJ_DEVISEAGENCE, PRJ_CHANGE,PRJ_DEVISE, 
					        TAB_PROJET.ID_PROFIL AS PRJ_IDPROFIL, PRJ_REFERENCE, :lastname AS PROFIL_NOM, :firstname AS PROFIL_PRENOM 
					 FROM TAB_MISSIONPROJET 
					 INNER JOIN TAB_PROJET USING(ID_PROJET) 
					 WHERE TAB_MISSIONPROJET.ID_MASTER= :id',
					[
						'id' => $result['ID_MISSIONPROJET'],
						'lastname' => $result['PROFIL_NOM'],
						'firstname' => $result['PROFIL_PRENOM']
					]
				);
				if(!$result['SLAVE']) unset($result['SLAVE']);
			}
			return $result;
		}
		return false;
	}


	/**
	 * create a delivery
	 * @param array $data
	 * @return bool|int the new id or false
	 */
	public function createDelivery($data)
	{
		if(isset($data['MISSION'])) {
			if(!isset($data['MISSION']['MP_COMMENTAIRES'])) $data['MISSION']['MP_COMMENTAIRES'] = '';
			if(!isset($data['MISSION']['MP_CONDITIONS'])) $data['MISSION']['MP_CONDITIONS'] = '';
			if(!isset($data['MISSION']['MP_AVANTAGES'])) $data['MISSION']['MP_AVANTAGES'] = '';
			if(!isset($data['MISSION']['MP_REGLESTEMPSEXCEPTION'])) $data['MISSION']['MP_REGLESTEMPSEXCEPTION'] = '';
			if(!isset($data['MISSION']['MP_DEBUT'])) $data['MISSION']['MP_DEBUT'] = date('Y-m-d', mktime());
			if(!isset($data['MISSION']['MP_FIN'])) $data['MISSION']['MP_FIN'] = date('Y-m-d', mktime());
			$idmissionprojet = $this->insert('TAB_MISSIONPROJET', $data['MISSION']);

			if($idmissionprojet && isset($data['FRAISDETAILS']) && is_array($data['FRAISDETAILS'])) {
				foreach($data['FRAISDETAILS'] as $detail) {
					if(isset($detail['ID_FRAISDETAILS'])) unset($detail['ID_FRAISDETAILS']);
					$detail['ID_PARENT'] = $idmissionprojet;
					$detail['PARENT_TYPE'] = 1;
					$this->insert('TAB_FRAISDETAILS', $detail);
				}
			}

			if($idmissionprojet && isset($data['MISSIONDETAILS'])) {
				//On met à jour les items
				foreach($data['MISSIONDETAILS'] as $dataItem) {
					$dataItem['ID_PARENT'] = $idmissionprojet;
					$dataItem['PARENT_TYPE'] = 1;
					$this->insert('TAB_MISSIONDETAILS', $dataItem);
				}
			}
			return $idmissionprojet;
		}
		return false;
	}

	/**
	 * update a delivery
	 * @param array $data new data
	 * @param int $id delivery id
	 * @return bool
	 */
	public function updateDelivery($data, $id)
	{
		if(isset($data['MISSION']))
			$this->update('TAB_MISSIONPROJET', $data['MISSION'], 'ID_MISSIONPROJET=:id', ['id' => $id]);

		//On supprime les détails de frais qui n'existent plus
		if(isset($data['FRAISDETAILS'])) {
			foreach($this->exec('SELECT ID_FRAISDETAILS FROM TAB_FRAISDETAILS WHERE ID_PARENT=:id AND PARENT_TYPE=1', ['id' => $id]) as $frais) {
				$etat = false;
				foreach($data['FRAISDETAILS'] as $data_frais) if($data_frais['ID_FRAISDETAILS'] == $frais['ID_FRAISDETAILS']) {$etat = true;break;}
				if(!$etat) $this->delete('TAB_FRAISDETAILS', 'ID_FRAISDETAILS=:id AND ID_PARENT = :idp', ['id' => $frais['ID_FRAISDETAILS'], 'idp' => $id]);
			}

			//On met à jour les détails de frais
			foreach($data['FRAISDETAILS'] as $data_frais) {
				if($data_frais['ID_FRAISDETAILS'] != 0)//Ce détail de frais existe déjà
					$this->update('TAB_FRAISDETAILS', $data_frais, 'ID_FRAISDETAILS=:id AND ID_PARENT=:idp AND PARENT_TYPE=1', ['id' => $data_frais['ID_FRAISDETAILS'], 'idp' => $id]);
				else {//Ce détail de frais est nouveau
					unset($data_frais['ID_FRAISDETAILS']);
					$data_frais['ID_PARENT'] = $id;
					$data_frais['PARENT_TYPE'] = 1;
					$this->insert('TAB_FRAISDETAILS', $data_frais);
				}
			}
		}

		if(isset($data['MISSIONDETAILS'])) {
			//On supprime les items qui n'existent plus
			foreach($this->exec('SELECT ID_MISSIONDETAILS FROM TAB_MISSIONDETAILS WHERE ID_PARENT=:id AND PARENT_TYPE=1 AND ID_ACHAT=0', ['id' => $id]) as $item) {
				$etat = false;
				foreach($data['MISSIONDETAILS'] as $data_item) if($data_item['ID_MISSIONDETAILS'] == $item['ID_MISSIONDETAILS']) {$etat = true;break;}
				if(!$etat) $this->delete('TAB_MISSIONDETAILS', 'ID_MISSIONDETAILS=:id AND ID_PARENT=:idp AND PARENT_TYPE=1', ['id' => $item['ID_MISSIONDETAILS'], 'idp' => $id]);
			}

			//On met à jour les items
			foreach($data['MISSIONDETAILS'] as $dataItem) {
				if(isset($dataItem['ID_MISSIONDETAILS']) && $dataItem['ID_MISSIONDETAILS'])//Cet item existe déjà
					$this->update('TAB_MISSIONDETAILS', $dataItem, 'ID_MISSIONDETAILS=:id AND ID_PARENT=:idp AND PARENT_TYPE=1', ['id' => $dataItem['ID_MISSIONDETAILS'], 'idp' => $id]);
				else {//Cet item est nouvelle
					$dataItem['ID_PARENT'] = $id;
					$dataItem['PARENT_TYPE'] = 1;
					$this->insert('TAB_MISSIONDETAILS', $dataItem);
				}
			}
		}

		if(isset($data['ENFANTS']) && $data['ENFANTS']) {
			// removing deliveries from the groupment
			$this->update('TAB_MISSIONPROJET', ['ID_PARENT' => 0], 'ID_MISSIONPROJET IN ('.Where::prepareNamedWhereIN('imp', $data['ENFANTS']).') AND ID_PARENT=:id', array_merge(Where::prepareNamedWhereINArgs('imp', $data['ENFANTS']), ['id' => $id]));
			// linking deliveries to the groupment
			$this->update('TAB_MISSIONPROJET', ['ID_PARENT' => $id], 'ID_MISSIONPROJET IN ('.Where::prepareNamedWhereIN('imp', $data['ENFANTS']).')', Where::prepareNamedWhereINArgs('imp', $data['ENFANTS']));
		}
		return true;
	}
}
