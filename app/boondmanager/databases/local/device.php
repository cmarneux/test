<?php
/**
 * device.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use Wish\Models\Model;

/**
 * Handle all database work related to Devices
 * @namespace BoondManager\Databases\Local
 */
class Device extends AbstractObject {

	/**
	 * Get all devices allowed by a user
	 * @param integer $id user id, cf. [TAB_DEVICEALLOWED.ID_USER](../../bddclient/classes/TAB_DEVICEALLOWED.html#property_ID_USER).
	 * @return Model[]|false
	 */
	public function getAllDevicesFromUser($id) {
		return $this->exec('SELECT ID_DEVICEALLOWED, DALL_SHA, DALL_NAVIGATEUR, DALL_SESSION, DALL_SERVER, DALL_NOM, DALL_DATE, DALL_LASTCONNEXION, DALL_IP, ID_USER FROM TAB_DEVICEALLOWED WHERE ID_USER=? ORDER BY ID_DEVICEALLOWED ASC', $id);
	}

	/**
	 * Update a device data
	 * @param array $data
	 * @param integer $id  Device ID to update, cf. [TAB_DEVICEALLOWED.ID_DEVICEALLOWED](../../bddclient/classes/TAB_DEVICEALLOWED.html#property_ID_DEVICEALLOWED).
	 * if none, a new device will be created
	 *
	 * @return boolean
	 */
	public function updateDevice($data, $id) {
		$this->update('TAB_DEVICEALLOWED', $data, 'ID_DEVICEALLOWED=:id', array(':id' => $id));
		return true;
	}

	/**
	 * Create a device data
	 * @param array $data
	 * @return integer Device's id
	 */
	public function createDevice($data) {
		if(!isset($data['DALL_NAVIGATEUR'])) $data['DALL_NAVIGATEUR'] = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
		if(!isset($data['DALL_IP'])) {
			if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $data['DALL_IP'] = ip2long($_SERVER['HTTP_X_FORWARDED_FOR']);
			elseif(isset($_SERVER['HTTP_CLIENT_IP'])) $data['DALL_IP'] = ip2long($_SERVER['HTTP_CLIENT_IP']);
			else $data['DALL_IP'] = ip2long($_SERVER['REMOTE_ADDR']);
		}
		if(!isset($data['DALL_SERVER'])) $data['DALL_SERVER'] = ip2long($_SERVER['SERVER_ADDR']);
		if(!isset($data['DALL_SESSION'])) $data['DALL_SESSION'] = session_id();
		if(!isset($data['DALL_LASTCONNEXION'])) $data['DALL_LASTCONNEXION'] = date('Y-m-d H:i:s');
		if(!isset($data['ID_USER'])) $data['ID_USER'] = CurrentUser::instance()->getUserId();
		if(!isset($data['DALL_DATE'])) $data['DALL_DATE'] = date('Y-m-d H:i:s');

		$browser = get_browser($data['DALL_NAVIGATEUR'], true);
		if(!isset($data['DALL_SHA'])) $data['DALL_SHA'] = sha1($data['ID_USER'].'*'.$browser['browser'].'*'.$browser['platform'].'*'.$data['DALL_DATE'].'*'.BM::getCustomerCode());

		return $this->insert('TAB_DEVICEALLOWED', $data);
	}

	/**
	 * Delete a device
	 * @param  integer  $id device id, cf. [TAB_DEVICEALLOWED.ID_DEVICEALLOWED](../../bddclient/classes/TAB_DEVICEALLOWED.html#property_ID_DEVICEALLOWED).
	 * @return int
	 */
	public function deleteDevice($id) {
		return $this->delete('TAB_DEVICEALLOWED', 'ID_DEVICEALLOWED=?', $id);
	}

	/**
	 * Retrieve all device data from an ID
	 * @param  integer  $id  Device id, cf. [TAB_DEVICEALLOWED.ID_DEVICEALLOWED](../../bddclient/classes/TAB_DEVICEALLOWED.html#property_ID_DEVICEALLOWED).
	 * @return Model|false
	 */
	public function getDevice($id) {
		return $this->singleExec('SELECT ID_USER, DALL_SHA, DALL_NAVIGATEUR, DALL_SESSION, DALL_SERVER, DALL_NOM, DALL_DATE, DALL_LASTCONNEXION, DALL_IP FROM TAB_DEVICEALLOWED WHERE ID_DEVICEALLOWED=?', $id);
	}

	/**
	 * Check if a device is new
	 * @param  integer  $id  User id, cf. [TAB_DEVICEALLOWED.ID_USER](../../bddclient/classes/TAB_DEVICEALLOWED.html#property_ID_USER).
	 * @param  array  $tabCookies  an array with the user Cookies containing the device
	 * the keys should be the concatenation of the db name with the device id
	 * @param  boolean  $deleteUnused  If true, all "empty" (last connection < 5days or session doesn't exists) devices will be deleted
	 * @return \Wish\Models\Model|false
	 */
	public function getDeviceFromUserAndCookie($id, $tabCookies = array(), $deleteUnused = false) {
		$societe = ($this->f3->exists('SESSION.CLIENT'))?$this->f3->get('SESSION.CLIENT.BMMYSQL.DATABASE'):'';
		if($tabDevices = $this->getAllDevicesFromUser($id)) {
			if($deleteUnused) {
				$dateExpiration = date('Y-m-d', mktime(0,0,0,date('n'),date('j')-5,date('Y')));
				foreach($tabDevices as $i => $device) {
					$tabDate = explode(' ', isset($device['DALL_LASTCONNEXION'])?$device['DALL_LASTCONNEXION']:'0000-00-00 00:00:00');
					if($device['DALL_NOM'] == '' &&
					   (($device['DALL_SESSION'] == '' && $tabDate[0] < $dateExpiration) ||
						($device['DALL_SESSION'] != '' && !file_exists('../sessions/sess_'.$device['DALL_SESSION'])))) {
						$this->deleteDevice($device['ID_DEVICEALLOWED']);
						unset($tabDevices[$i]);
					}
				}
			}

			if(is_array($tabCookies) && sizeof($tabCookies) > 0) {
				foreach($tabDevices as $device) {
					if(isset($tabCookies[$societe.$id]) && $tabCookies[$societe.$id] == $device['DALL_SHA']) {
						return $device;
					}
				}
			}
		}
		return false;
	}
}
