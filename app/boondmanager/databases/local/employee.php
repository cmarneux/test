<?php
/**
 * ressource.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Employees\Filters\SearchEmployees;
use Wish\Tools;
use Wish\MySQL\Where;
use BoondManager\Lib\SolR;
use Wish\Models\SearchResult;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\OldModels\Filters;
use BoondManager\OldModels\MySQL\Mappers;
use BoondManager\OldModels\MySQL\Mappers\SearchEmployee;
use BoondManager\Models;
use BoondManager\Services;
use Wish\MySQL\Query;

/**
 * Handle all database work related to resources
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Employee extends AbstractObject {

	/**
	 * Retrieve resource data from its ID
	 * @param  integer  $id  Resource ID, cf. [TAB_PROFIL.ID_PROFIL](../../bddclient/classes/TAB_PROFIL.html#property_ID_PROFIL).
	 * @param  integer $tab Subpart of the resource (Tab)
	 * @return Models\Employee|false
	 * @TODO finir la migration v6 -> lab
	 */
	public function getObject($id, $tab= Models\Employee::TAB_DEFAULT, $filterAdvantages = null) {
		// construction de la requete de base, sera enrichie en fonction de l'onglet
		$baseQuery = new Query();
		$baseQuery->addColumns([
			'AT.ID_PROFIL', 'AT.ID_RESPRH', 'ID_PROFIL_RESPMANAGER' => 'USRESP.ID_PROFIL', 'ID_PROFIL_RESPRH' => 'USRH.ID_PROFIL', 'AT.ID_RESPMANAGER',
			'AT.PROFIL_NOM', 'AT.PROFIL_PRENOM', 'AT.PROFIL_VISIBILITE', 'AT.PROFIL_ALLOWCHANGE', 'AT.PROFIL_TYPE', 'US.ID_USER', 'US.USER_TYPE',
			'US.USER_TPSFRSSTART', 'ID_CV', 'NB_CV'=>'COUNT(ID_CV)', 'AT.ID_SOCIETE', 'AT.ID_POLE', 'AT.PROFIL_CIVILITE', 'AT.PROFIL_DATEUPDATE', 'AT.PROFIL_DATE'
		])
		->from('TAB_PROFIL AS AT')
		->addJoin('LEFT JOIN TAB_USER AS US ON (US.ID_PROFIL=AT.ID_PROFIL)')
		->addJoin('LEFT JOIN TAB_USER AS USRESP ON USRESP.ID_USER=ID_RESPMANAGER')
		->addJoin('LEFT JOIN TAB_USER AS USRH ON USRH.ID_USER=ID_RESPRH')
		->addJoin('LEFT JOIN TAB_CV ON (TAB_CV.ID_PROFIL=AT.ID_PROFIL)')
		->addWhere('AT.ID_PROFIL = ?', $id)
		->addWhere('AT.PROFIL_TYPE <> ?', Models\Candidate::TYPE_CANDIDATE)
		->groupBy('AT.ID_PROFIL');

		switch($tab) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Employee::TAB_INFORMATION:
				// réecriture de la requete avec l'objet Query => possibilité de sortir des segments de requetes récurents aux differents profils et le centraliser avant le switch
				$baseQuery->addColumns([
						'AT.PROFIL_DATENAISSANCE', 'PROFIL_STATUT',
						'PROFIL_EMAIL', 'PROFIL_EMAIL2', 'PROFIL_EMAIL3', 'PROFIL_TEL1', 'PROFIL_TEL2', 'PROFIL_TEL3',
						'PROFIL_FAX', 'PROFIL_ADR', 'PROFIL_CP', 'PROFIL_VILLE', 'PROFIL_PAYS',
						'PROFIL_ETAT', 'ID_DT', 'US.USER_ABONNEMENT', 'US.USER_LOGIN', 'US.USER_SECURITYCOOKIE',
						'US.USER_SECURITYALERT', 'US.USER_CONNECTIONTYPE', 'PARAM_COMMENTAIRE', 'PARAM_TARIF1', 'PARAM_MOBILITE',
						'PARAM_TYPEDISPO', 'PARAM_DATEDISPO', 'PARAM_CHANGE', 'PARAM_DEVISE', 'PARAM_CHANGEAGENCE',
						'PARAM_DEVISEAGENCE', 'ID_CONTRAT'=>'MAX(ID_CONTRAT)'
					])
					->addJoin('LEFT JOIN TAB_DT USING(ID_DT)')
					->addJoin('LEFT JOIN TAB_CONTRAT ON TAB_CONTRAT.ID_PROFIL = AT.ID_PROFIL')
					->addOrderBy('ID_CONTRAT DESC');
				$result = $this->singleExec($baseQuery);
				if($result) {
					$result['CVs'] = $this->exec('SELECT ID_CV, CV_NAME FROM TAB_CV WHERE ID_PROFIL=? ORDER BY ID_CV ASC', $id); // Models\Resume::class
					$result['WEBSOCIALS'] = $this->exec('SELECT ID_WEBSOCIAL, WEBSOCIAL_NETWORK, WEBSOCIAL_URL FROM TAB_WEBSOCIAL WHERE WEBSOCIAL_TYPE=1 AND ID_PARENT=?', $id); // Models\WebSocial::class
				}
				return $result;
			case Models\Employee::TAB_ADMINISTRATIVE:
				$baseQuery->addColumns([
						'AT.PROFIL_DATENAISSANCE', 'AT.PROFIL_LIEUNAISSANCE', 'AT.PROFIL_NATIONALITE', 'AT.PROFIL_SITUATION',
						'AT.PROFIL_REFERENCE', 'AT.PROFIL_NUMSECU', 'AT.PARAM_COMMENTAIRE2', 'US.USER_LOGIN',
						'US.USER_SECURITYCOOKIE', 'US.USER_SECURITYALERT', 'US.USER_CONNECTIONTYPE', 'ID_PROFILCANDIDAT'=>'CAND.ID_PROFIL',
						'CAND_NOM'=>'CAND.PROFIL_NOM', 'CAND_PRENOM'=>'CAND.PROFIL_PRENOM',
						'TAB_CRMCONTACT.ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM', 'AT.ID_CRMSOCIETE', 'CSOC_SOCIETE', 'ID_CONTRAT'=>'MAX(ID_CONTRAT)'
					])
					->addJoin('LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=AT.ID_CRMCONTACT)')
					->addJoin('LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE = AT.ID_CRMSOCIETE)')
					->addJoin('LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=AT.ID_PROFIL)')
					->addJoin('LEFT JOIN TAB_PROFIL CAND ON(CAND.ID_EMBAUCHE=AT.ID_PROFIL)')
					->addOrderBy('ID_CONTRAT DESC');
				$result = $this->singleExec($baseQuery);

				if($result) {
					//On récupère tous les contrats
					$filterContract = new \BoondManager\APIs\Contracts\Filters\SearchContracts();
					$filterContract->disableMaxResultLimit();
					$filterContract->setIndifferentPerimeter();
					$filterContract->setData([
						//'user'=> $result['ID_PROFIL'],
						'keywords' => Models\Employee::buildReference($result['ID_PROFIL']),
						'sort' => \BoondManager\APIs\Contracts\Filters\SearchContracts::ORDERBY_ENDDATE,
						'order'=> \BoondManager\APIs\Contracts\Filters\SearchContracts::ORDER_DESC,
						'page' => 1
					]);
					$filterContract->isValid();

					$contractsDB = new Contract();
					$search = $contractsDB->search($filterContract);

					$tabContrats = array();
					if($search->total) {
						$tabIDs = array();
						foreach($search->rows as $contrat1) {
							if(!in_array($contrat1['ID_CONTRAT'], $tabIDs)) {
								$tabIDs[] = $contrat1['ID_CONTRAT'];
								$tabContrats[] = $contrat1;
								$idParent = $contrat1['ID_PARENT'];
								while($idParent > 0) {
									$parentExist = false;
									foreach($search->rows as $contrat2) {
										if($idParent == $contrat2['ID_CONTRAT']) {
											$tabIDs[] = $contrat2['ID_CONTRAT'];
											$tabContrats[] = $contrat2;
											$parentExist = true;
											break;
										}
									}
									$idParent = ($parentExist)?$contrat2['ID_PARENT']:0;
								}
							}
						}
					}
					$result['NB_CONTRATS'] = count($tabContrats);
					$result['CONTRATS'] = $tabContrats;

					/*
					$forbiddenFields = ['CTR_FRSJOUR', 'CTR_FRSMENSUEL', 'CTR_TPSTRAVAIL', 'CTR_CLASSIFICATION', 'resource'];
					if($result['CONTRATS'])
						foreach($result['CONTRATS'] as $contract)
							$contract->removeFields($forbiddenFields);
					*/

					//On récupère les pièces jointes
					$sql = 'SELECT ID_FILE, FILE_NAME FROM TAB_FILE WHERE ID_PROFIL=? ORDER BY ID_FILE ASC;';
					$result['FILES'] = $this->exec($sql, $id); //Models\File::class
				}
				return $result;
			case Models\Employee::TAB_TECHNICALDATA:
				$baseQuery->addColumns([
						'PROFIL_EMAIL', 'PROFIL_TEL1', 'PROFIL_DATENAISSANCE',  'PARAM_MOBILITE', 'PARAM_TYPEDISPO', 'PARAM_DATEDISPO',
						'PARAM_COMMENTAIRE', 'COMP_COMPETENCE', 'COMP_APPLICATIONS',
						'COMP_INTERVENTIONS', 'DT_LANGUES', 'DT_TITRE', 'DT_OUTILS', 'DT_LANGUES', 'DT_DIPLOMES', 'DT_EXPERIENCE',
						'DT_FORMATION', 'ID_DT', 'US.USER_LOGIN', 'US.USER_SECURITYCOOKIE', 'US.USER_SECURITYALERT',
						'US.USER_CONNECTIONTYPE'
					])
					->addJoin('LEFT JOIN TAB_DT USING(ID_DT)');
				$result = $this->singleExec($baseQuery);
				if($result){
					$sql = 'SELECT ID_REFERENCE, REF_TITRE, REF_DESCRIPTION FROM TAB_REFERENCE WHERE ID_DT=? ORDER BY ID_REFERENCE ASC';
					$result['REFERENCES'] = $this->exec($sql, $result->ID_DT);
				}
				return $result;
			case Models\Employee::TAB_ACTIONS:
				$baseQuery->addColumns(['US.USER_LOGIN', 'ID_PROFILCANDIDAT'=>'CAND.ID_PROFIL'])
						  ->addJoin('LEFT JOIN TAB_PROFIL CAND ON(CAND.ID_EMBAUCHE = AT.ID_PROFIL)');
				$result = $this->singleExec($baseQuery);
				return $result;
			case Models\Employee::TAB_POSITIONINGS:
				$baseQuery->addColumns(['US.USER_LOGIN', 'AT.ID_SOCIETE', 'ID_PROFILCANDIDAT'=>'CAND.ID_PROFIL'])
						  ->addJoin('LEFT JOIN TAB_PROFIL CAND ON(CAND.ID_EMBAUCHE = AT.ID_PROFIL)');
				$result = $this->singleExec($baseQuery);
				return $result;
			case Models\Employee::TAB_DELIVERIES://Activité
				$baseQuery->addColumns(['US.USER_LOGIN', 'AT.ID_SOCIETE', 'GRPCONF_CALENDRIER'])
				->addJoin('LEFT JOIN TAB_GROUPECONFIG USING(ID_SOCIETE)');
				$result = $this->singleExec($baseQuery);
				return $result;
			case Models\Employee::TAB_TIMESREPORTS:case Models\Employee::TAB_EXPENSESREPORTS:case Models\Employee::TAB_ABSENCESREPORTS://Temps, Frais, Absences
				$baseQuery->addColumns(['MIN_DEBUT_CONTRAT' => 'MIN(CTR_DEBUT)', 'MAX_FIN_CONTRAT' => 'MAX(CTR_FIN)'])
				->addJoin('LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=AT.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=AT.ID_SOCIETE)');
				$result = $this->singleExec($baseQuery);
				return $result;
				//~ if(Wish_Session::getInstance()->isRegistered('login_moduletpsfrs') || (Wish_Session::getInstance()->isRegistered('login_moduleintranet') && $id == Wish_Session::getInstance()->get('login_idprofil')) || $this->always_access) {
					//~ $sql ='SELECT TAB_PROFIL.ID_PROFIL, PROFIL_ALLOWCHANGE, PROFIL_VISIBILITE, ID_RESPMANAGER, ID_RESPRH, ID_USER, US.USER_TYPE, US.USER_TPSFRSSTART, US.USER_VALIDATIONTEMPS, US.USER_VALIDATIONFRAIS, US.USER_TPSFRSETAT, PROFIL_NOM, PROFIL_PRENOM, PROFIL_TYPE, PROFIL_EMAIL, TAB_PROFIL.ID_SOCIETE, ID_CV, COUNT(ID_CV) AS NB_CV, MIN(CTR_DEBUT) AS MIN_DEBUT, MAX(CTR_FIN) AS MAX_FIN
						   //~ FROM TAB_PROFIL LEFT JOIN TAB_USER USING(ID_PROFIL) LEFT JOIN TAB_CV USING(ID_PROFIL) LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_PROFIL.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_PROFIL.ID_SOCIETE)
						   //~ WHERE TAB_PROFIL.ID_PROFIL="'.$this->db->escape($id).'" AND PROFIL_TYPE<>'.PROFIL_CANDIDAT.' GROUP BY TAB_PROFIL.ID_PROFIL;';
					//~ $result = $this->db->query_first($sql);
				//~ }
				break;
			case Models\Employee::TAB_ABSENCESACCOUNTS://Décomptes
				//rien à ajouter par rapport à la requête de base
				$resource = $this->singleExec($baseQuery);
				return $resource;
			/*
				if($resource) {//~ À supprimer plus tard si appel via resource/id/absencesaccounts direct
					$activiteBDD = new TimesExpensesAbsences();
					$configGroupe = $activiteBDD->getConfigTpsFrsGroupe($resource->agency->getId());

					//On récupère la période actuelle par défaut des décomptes
					$nowDate = date('Y-m-d', time());
					$nowDate = explode('-', $nowDate);
					if ($nowDate[1] >= $configGroupe['GRPCONF_STARTDECOMPTE']) $nowAnnee = $nowDate[0]; else $nowAnnee = $nowDate[0] - 1;
					if ($configGroupe['GRPCONF_STARTDECOMPTE'] < 10) $monthDecompte = '0' . $configGroupe['GRPCONF_STARTDECOMPTE']; else $monthDecompte = $configGroupe['GRPCONF_STARTDECOMPTE'];
					$periodeDecompte = $nowAnnee . '-' . $monthDecompte . '-01';

					//On construit le tableau des périodes actuelles pour chaque type de décompte
					$liste_DPeriodes = array();
					if (isset($configGroupe['LISTE_DECOMPTE'])) {
						foreach ($configGroupe['LISTE_DECOMPTE'] as $decompte) {
							if ($nowDate[1] >= $decompte['TYPED_STARTDECOMPTE']) $decAnnee = $nowDate[0]; else $decAnnee = $nowDate[0] - 1;
							if ($decompte['TYPED_STARTDECOMPTE'] < 10) $decMonth = '0' . $decompte['TYPED_STARTDECOMPTE']; else $decMonth = $decompte['TYPED_STARTDECOMPTE'];
							$liste_DPeriodes[$decompte['TYPED_TYPEHREF']] = $decAnnee . '-' . $decMonth . '-01';
						}
					}

					//On construit la liste des types d'absences
					$liste_type = array();
					if (isset($configGroupe['LISTE_TYPEH'])) {
						foreach ($configGroupe['LISTE_TYPEH'] as $typeh) {
							if ($typeh['TYPEH_TYPEACTIVITE'] == 1) {
								if (isset($liste_DPeriodes[$typeh['TYPEH_REF']]))
									$liste_type[$typeh['TYPEH_REF']] = array($typeh['TYPEH_NAME'], $liste_DPeriodes[$typeh['TYPEH_REF']]);
								else
									$liste_type[$typeh['TYPEH_REF']] = array($typeh['TYPEH_NAME'], $periodeDecompte);
							}
						}
					}

					//On ajoute dans la liste des décomptes la somme des absences prises par société, par type et par période
					$liste_decomptes = array();
					$absences = (new Absence)->getListeAbsences($resource->getId());
					foreach ($absences as $absence) {
						//On récupère la période de cette absence
						$date = $absence['LISTETEMPS_DATE'];
						$date = explode('-', $date);

						if (isset($liste_DPeriodes[$absence['TYPEH_REF']])) {
							$decDate = explode('-', $liste_DPeriodes[$absence['TYPEH_REF']]);
							if ($date[1] >= $decDate[1]) $annee = $date[0]; else $annee = $date[0] - 1;
							$abs_periode = $annee . '-' . $decDate[1] . '-01';
						} else {
							if ($date[1] >= $configGroupe['GRPCONF_STARTDECOMPTE']) $annee = $date[0]; else $annee = $date[0] - 1;
							$abs_periode = $annee . '-' . $monthDecompte . '-01';
						}

						if (isset($liste_decomptes[$abs_periode][$absence['ID_SOCIETE']][$absence['TYPEH_REF']]['consomme']))
							$liste_decomptes[$abs_periode][$absence['ID_SOCIETE']][$absence['TYPEH_REF']]['consomme'] += $absence['TEMPS_DUREE'];
						else {
							$liste_decomptes[$abs_periode][$absence['ID_SOCIETE']][$absence['TYPEH_REF']]['consomme'] = $absence['TEMPS_DUREE'];
							$liste_decomptes[$abs_periode][$absence['ID_SOCIETE']][$absence['TYPEH_REF']]['agency'] = $absence['SOCIETE_RAISON'];
							if (isset($absence['TYPEH_NAME']))
								$liste_decomptes[$abs_periode][$absence['ID_SOCIETE']][$absence['TYPEH_REF']]['type'] = $absence['TYPEH_NAME'];
							else
								$liste_decomptes[$abs_periode][$absence['ID_SOCIETE']][$absence['TYPEH_REF']]['type'] = $absence['TYPEH_REF'];
						}
					}

					//On ajoute dans la liste des décomptes les quotas définis pour le consultant par société, par type et par période
					$quotas = (new Absence)->getAllQuotas($resource->getId());
					foreach ($quotas as $quota) {
						if (isset($quota['TYPEH_NAME']))
							$liste_decomptes[$quota['INTQUO_PERIODE']][$quota['ID_SOCIETE']][$quota['INTQUO_TYPEHREF']]['type'] = $quota['TYPEH_NAME'];
						else
							$liste_decomptes[$quota['INTQUO_PERIODE']][$quota['ID_SOCIETE']][$quota['INTQUO_TYPEHREF']]['type'] = $quota['INTQUO_TYPEHREF'];
						$liste_decomptes[$quota['INTQUO_PERIODE']][$quota['ID_SOCIETE']][$quota['INTQUO_TYPEHREF']]['quota'] = $quota['INTQUO_QUOTA'];
						$liste_decomptes[$quota['INTQUO_PERIODE']][$quota['ID_SOCIETE']][$quota['INTQUO_TYPEHREF']]['agency'] = $quota['SOCIETE_RAISON'];
					}

					//On ajoute dans la liste des décomptes les quotas par défaut pour la société actuelle du consultant, par type et pour la période actuelle
					if (isset($configGroupe['LISTE_DECOMPTE']) && $activiteBDD->getProfilTpsFrsData($resource->getId())) {
						foreach ($configGroupe['LISTE_DECOMPTE'] as $decompte) {
							if ($activiteBDD->get('USER_ALLOWABSENCESTYPEHREF') == '')
								$absAllow = true;
							else {
								$absAllow = false;
								$tabAllowAbsences = Tools::convertDoubleArray($activiteBDD->get('USER_ALLOWABSENCESTYPEHREF'), true, false);
								foreach ($tabAllowAbsences as $abs)
									if ($abs[0] == $decompte['TYPED_TYPEHREF'] && $abs[1] == $configGroupe['ID_SOCIETE']) {
										$absAllow = true;
										break;
									}
							}

							if ($absAllow && $decompte['TYPED_QUOTA'] != 0) {
								$liste_decomptes[$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]][$configGroupe['ID_SOCIETE']][$decompte['TYPED_TYPEHREF']]['agency'] = $configGroupe['SOCIETE_RAISON'];
								if (!isset($liste_decomptes[$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]][$configGroupe['ID_SOCIETE']][$decompte['TYPED_TYPEHREF']]['type'])) {
									if (isset($liste_type[$decompte['TYPED_TYPEHREF']]))
										$liste_decomptes[$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]][$configGroupe['ID_SOCIETE']][$decompte['TYPED_TYPEHREF']]['type'] = $liste_type[$decompte['TYPED_TYPEHREF']][0];
									else
										$liste_decomptes[$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]][$configGroupe['ID_SOCIETE']][$decompte['TYPED_TYPEHREF']]['type'] = $decompte['TYPED_TYPEHREF'];
								}
								if (!isset($liste_decomptes[$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]][$configGroupe['ID_SOCIETE']][$decompte['TYPED_TYPEHREF']]['quota']))
									$liste_decomptes[$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]][$configGroupe['ID_SOCIETE']][$decompte['TYPED_TYPEHREF']]['quota'] = $decompte['TYPED_QUOTA'];
							}
						}
					}

					$absencesAccounts = [];
					//On termine tous les calculs de décompte
					foreach ($liste_decomptes as $periode => $pDecompte) {
						foreach ($pDecompte as $idsociete => $sDecompte) {
							foreach ($sDecompte as $ref => $decompte) {
								if (!isset($decompte['quota'])) $liste_decomptes[$periode][$idsociete][$ref]['quota'] = 0;
								if (!isset($decompte['consomme'])) $liste_decomptes[$periode][$idsociete][$ref]['consomme'] = 0;
								$liste_decomptes[$periode][$idsociete][$ref]['restant'] = $liste_decomptes[$periode][$idsociete][$ref]['quota'] - $liste_decomptes[$periode][$idsociete][$ref]['consomme'];

								if ($liste_decomptes[$periode][$idsociete][$ref]['quota'] == 0 && $liste_decomptes[$periode][$idsociete][$ref]['consomme'] == 0 && $liste_decomptes[$periode][$idsociete][$ref]['restant'] == 0) {
									unset($liste_decomptes[$periode][$idsociete][$ref]);
									if (sizeof($liste_decomptes[$periode][$idsociete]) == 0) unset($liste_decomptes[$periode][$idsociete]);
									if (sizeof($liste_decomptes[$periode]) == 0) unset($liste_decomptes[$periode]);
								}
								$absencesAccounts[] = (new Models\AbsencesAccount)->fromArray([
									'ID_ABSENCESACCOUNT' => $periode.$idsociete.$ref,
									'INTQUO_PERIODE' => $periode,
									'INTQUO_QUOTA' => $liste_decomptes[$periode][$idsociete][$ref]['quota'],
									'INTQUO_USED' => $liste_decomptes[$periode][$idsociete][$ref]['consomme'],
									'INTQUO_DELTA' => $liste_decomptes[$periode][$idsociete][$ref]['restant'],
									'workUnitType' => (new Models\WorkUnitTypeOnlyReference)->fromArray([
										'TYPEH_REF' => $ref,
										'TYPEH_TYPEACTIVITE' => Models\WorkUnitTypeOnlyReference::ACTIVITY_ABSENCE,
										'TYPEH_NAME' => $liste_decomptes[$periode][$idsociete][$ref]['type'],
									]),
									'agency' => (new Models\Agency)->fromArray([
										'ID_SOCIETE' => $idsociete,
										'SOCIETE_RAISON' => $liste_decomptes[$periode][$idsociete][$ref]['agency'],
									]),
								]);
							}
						}
					}

					//Tri les périodes du plus grand au plus petit
					krsort($absencesAccounts);
					$resource->absencesAccounts = array_values($absencesAccounts);
					return $resource;
				}
				break;
			*/
			case Models\Employee::TAB_SETTING_ABSENCESACCOUNTS:
				$resource = $this->singleExec($baseQuery);
				if($resource){
					$activiteBDD = new TimesExpensesAbsences();
					$configGroupe = $activiteBDD->getConfigTpsFrsGroupe($resource->agency->getId());

					//On récupère la période actuelle par défaut des décomptes
					$nowDate = date('Y-m-d', time());
					$nowDate = explode('-', $nowDate);
					if($nowDate[1] >= $configGroupe['GRPCONF_STARTDECOMPTE']) $nowAnnee = $nowDate[0]; else $nowAnnee = $nowDate[0]-1;
					if($configGroupe['GRPCONF_STARTDECOMPTE'] < 10) $monthDecompte = '0'.$configGroupe['GRPCONF_STARTDECOMPTE']; else $monthDecompte = $configGroupe['GRPCONF_STARTDECOMPTE'];
					$periodeDecompte = $nowAnnee.'-'.$monthDecompte.'-01';

					//On construit le tableau des périodes actuelles pour chaque type de décompte
					$liste_DPeriodes = array();
					if(isset($configGroupe['LISTE_DECOMPTE'])) {
						foreach($configGroupe['LISTE_DECOMPTE'] as $decompte) {
							if($nowDate[1] >= $decompte['TYPED_STARTDECOMPTE']) $nowAnnee = $nowDate[0]; else $nowAnnee = $nowDate[0]-1;
							if($decompte['TYPED_STARTDECOMPTE'] < 10) $monthDecompte = '0'.$decompte['TYPED_STARTDECOMPTE']; else $monthDecompte = $decompte['TYPED_STARTDECOMPTE'];
							$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]['period'] = $nowAnnee.'-'.$monthDecompte.'-01';
							$liste_DPeriodes[$decompte['TYPED_TYPEHREF']]['quota'] = $decompte['TYPED_QUOTA'];
						}
					}

					//On construit la liste des types d'absences
					$absencesQuotas = [];
					if(isset($configGroupe['LISTE_TYPEH'])) {
						foreach($configGroupe['LISTE_TYPEH'] as $typeh) {
							if($typeh['TYPEH_TYPEACTIVITE'] == 1) {
								if(isset($liste_DPeriodes[$typeh['TYPEH_REF']]))
									$absencesQuotas[] = AbsencesQuota::fromSQL([
									'ID_INTQUO' => 0,
									'INTQUO_PERIODE' => $liste_type[$typeh['TYPEH_REF']],
									'INTQUO_QUOTA' => $liste_DPeriodes[$typeh['TYPEH_REF']]['quota'],
									'workUnitType' => WorkUnitType::fromSQL([
										'TYPEH_REF' => $typeh['TYPEH_REF'],
										'TYPEH_TYPEACTIVITE' => RowObject\WorkUnitType::ACTIVITY_ABSENCE,
										'TYPEH_NAME' => $typeh['TYPEH_NAME'],
									])
								]);
							}

						}
					}
					$resource->agency = Agency::fromSQL([
						'ID_SOCIETE' => $resource->agency->getId(),
						'defaultAbsencesAccountsPeriod' => $configGroupe['GRPCONF_STARTDECOMPTE'],
						'absencesQuotas' => $absencesQuotas,
					]);
var_dump($resource->agency);exit;
					$quotasV6 = (new Absence)->getAllQuotas($resource->getId());
					$quotasV7 = [];
					foreach ($quotasV6 as $quotaV6){
						$quotasV7[] = (new Models\AbsencesQuota)->fromArray([
							'ID_INTQUO' => $quotaV6->ID_INTQUO,
							'INTQUO_PERIODE' => $quotaV6->INTQUO_PERIODE,
							'INTQUO_QUOTA' => $quotaV6->INTQUO_QUOTA,
							'workUnitType' => (new Models\WorkUnitType)->fromArray([
								'TYPEH_REF' => $quotaV6->INTQUO_TYPEHREF,
								'TYPEH_TYPEACTIVITE' => Models\WorkUnitType::ACTIVITY_ABSENCE,
								'TYPEH_NAME' => $quotaV6->TYPEH_NAME,
							]),
							'agency' => (new Models\Agency)->fromArray([
								'ID_SOCIETE' => $quotaV6->ID_SOCIETE,
								'SOCIETE_RAISON' => $quotaV6->SOCIETE_RAISON,
							]),
						]);
					}
					$resource->absencesQuotas = $quotasV7;
					return $resource;
				}
				break;
			case Models\Employee::TAB_SETTING_INTRANET:
			case Models\Employee::TAB_SETTING_SECURITY:

				// same as TAB_INFORMATION
				$baseQuery->addColumns([
					'AT.PROFIL_DATENAISSANCE', 'PROFIL_STATUT',
					'PROFIL_EMAIL', 'PROFIL_EMAIL2', 'PROFIL_EMAIL3', 'PROFIL_TEL1', 'PROFIL_TEL2', 'PROFIL_TEL3',
					'PROFIL_FAX', 'PROFIL_ADR', 'PROFIL_CP', 'PROFIL_VILLE', 'PROFIL_PAYS',
					'PROFIL_ETAT', 'ID_DT', 'US.USER_ABONNEMENT', 'US.USER_LOGIN', 'US.USER_SECURITYCOOKIE',
					'US.USER_SECURITYALERT', 'US.USER_CONNECTIONTYPE', 'PARAM_COMMENTAIRE', 'PARAM_TARIF1', 'PARAM_MOBILITE',
					'PARAM_TYPEDISPO', 'PARAM_DATEDISPO', 'PARAM_CHANGE', 'PARAM_DEVISE', 'PARAM_CHANGEAGENCE',
					'PARAM_DEVISEAGENCE', 'ID_CONTRAT'=>'MAX(ID_CONTRAT)'
				])
					->addJoin('LEFT JOIN TAB_DT USING(ID_DT)')
					->addJoin('LEFT JOIN TAB_CONTRAT ON TAB_CONTRAT.ID_PROFIL = AT.ID_PROFIL')
					->addOrderBy('ID_CONTRAT DESC');
				$result = $this->singleExec($baseQuery);
				return $result;
			default:
				return $this->singleExec($baseQuery);
		}

		return false;
	}

	public function getProfilTpsFrsData($profilID) {
		$sql ='SELECT TAB_PROFIL.ID_PROFIL, ID_POLE, TAB_PROFIL.ID_RESPMANAGER, RESP.ID_PROFIL AS ID_PROFIL_RESPMANAGER, TAB_PROFIL.ID_RESPRH, 
		              HR.ID_PROFIL AS ID_PROFIL_RESPRH, TAB_USER.ID_USER, TAB_USER.USER_TYPE, TAB_USER.USER_LOGIN,
		              TAB_USER.USER_LANGUE, TAB_USER.USER_ABONNEMENT, TAB_USER.USER_ALLOWTEMPSEXCEPTION, TAB_USER.USER_DEFAULTSEARCH, TAB_USER.USER_JOINCATEGORY, 
		              TAB_USER.USER_HOMEPAGE, TAB_USER.USER_TPSFRSSTART, TAB_USER.USER_TAUXHORAIRE, TAB_USER.USER_VALIDATIONTEMPS, TAB_USER.USER_VALIDATIONFRAIS, 
		              TAB_USER.USER_VALIDATIONABSENCES, TAB_USER.USER_ALLOWABSENCESTYPEHREF, TAB_USER.USER_TPSFRSETAT, 
		              PROFIL_NOM, PROFIL_PRENOM, TAB_PROFIL.PROFIL_TYPE, PROFIL_EMAIL, TAB_PROFIL.ID_SOCIETE, 
		              CONFIG_MENUBAR, CONFIG_DASHBOARD, MIN(CTR_DEBUT) AS MIN_DEBUT, MAX(CTR_FIN) AS MAX_FIN
		       FROM TAB_PROFIL 
		       LEFT JOIN TAB_USER  USING(ID_PROFIL) 
		       LEFT JOIN TAB_USERCONFIG ON (TAB_USER.ID_USER = TAB_USERCONFIG.ID_USER) 
		       LEFT JOIN TAB_CONTRAT USING(ID_PROFIL)
		       LEFT JOIN TAB_USER AS RESP ON RESP.ID_USER = TAB_PROFIL.ID_RESPMANAGER
		       LEFT JOIN TAB_USER AS HR ON HR.ID_USER = TAB_PROFIL.ID_RESPRH
		       WHERE TAB_PROFIL.ID_PROFIL=? AND TAB_PROFIL.PROFIL_TYPE<> ? GROUP BY TAB_PROFIL.ID_PROFIL';

		return $this->singleExec($sql, [$profilID, Models\Candidate::TYPE_CANDIDATE]);
	}

	/**
	 * create a new Resource
	 * @param array $tabProfil new data group by category (PROFIL, DT, REFERENCES, NOTATIONS, CV)
	 * @param bool $copy duplicate or move files (resumes)
	 * @param bool $solr trigger a SolR Sync
	 * @return bool|int the new resource ID
	 */
	public function createEmployee($tabProfil, $copy = false, $solr = true)
	{
		if(isset($tabProfil['PROFIL'])) {

			if(isset($tabProfil['DT'])) {
				$defaultDT = $this->guessDefaultValue('TAB_DT');
				foreach($defaultDT as $field => $default)
					if(!isset($tabProfil['DT'][$field])) $tabProfil['DT'][$field] = $default;

				$iddt = $this->insert('TAB_DT', $tabProfil['DT']);
				$tabProfil['PROFIL']['ID_DT'] = $iddt;

				//On met à jour les références du DT
				foreach($tabProfil['REFERENCES'] as $reference) {
					$reference['ID_DT'] = $iddt;
					$this->insert('TAB_REFERENCE', $reference);
				}
			}

			$tabProfil['PROFIL']['PROFIL_TYPE'] = Models\Employee::TYPE_INTERNAL_RESOURCE;

			$defaultProfil = array_merge($this->guessDefaultValue('TAB_PROFIL'),[
				'PARAM_DEVISEAGENCE' => CurrentUser::instance()->getCurrency(),
				'PARAM_CHANGEAGENCE' => CurrentUser::instance()->getExchangeRate(),
				'PARAM_DEVISE' => CurrentUser::instance()->getCurrency()
			]);
			foreach($defaultProfil as $field => $default)
				if(!isset($tabProfil['PROFIL'][$field])) $tabProfil['PROFIL'][$field] = $default;

			// force some values & format
			$tabProfil['PROFIL']['PARAM_DATEDISPO'] = \DateTime::createFromFormat('Y-m-d', $tabProfil['PROFIL']['PARAM_DATEDISPO'])->format('Y-m-d');
			$tabProfil['PROFIL']['PROFIL_DATEUPDATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $tabProfil['PROFIL']['PROFIL_DATEUPDATE'])->format('Y-m-d H:i:s');
			$tabProfil['PROFIL']['PROFIL_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $tabProfil['PROFIL']['PROFIL_DATE'])->format('Y-m-d H:i:s');

			$idProfil = $this->insert('TAB_PROFIL', $tabProfil['PROFIL']);

			if($idProfil && isset($tabProfil['CVs']) && is_array($tabProfil['CVs'])) {
				$dbFile = new File();
				foreach($tabProfil['CVs'] as $cv)
					$dbFile->setObjectFromParent(File::TYPE_RESOURCE_RESUME, $cv['CV_FILE'], $cv['CV_NAME'], $cv['CV_TEXT'], $idProfil, $copy);
			}

			// On ajoute les réseaux sociaux
			if($idProfil && isset($tabProfil['WEBSOCIALS']) && is_array($tabProfil['WEBSOCIALS'])) {
				foreach($tabProfil['WEBSOCIALS'] as $data_social)
					$data_social['ID_PARENT'] = $idProfil;
					$data_social['WEBSOCIAL_TYPE'] = Models\WebSocial::WEBSOCIAL_TYPE_RESOURCE;
					$this->insert('TAB_WEBSOCIAL', $data_social);
			}

			if($solr)
				SolR::instance()->Creation(BM::SOLR_MODULE_RESOURCES);
			return $idProfil;
		}
		return false;
	}

	/**
	 * Update a resource
	 * @param array $data an array with the new values group by category (DT, NOTATIONS, REFERENCES, WEBSOCIALS)
	 * @param integer $id resource ID
	 * @param integer $iddt TD ID
	 * @param boolean $solrSync trigger a SolR Sync
	 */
	public function updateEmployee($data, $id, $iddt = 0, $solrSync = true)
	{
		if($solrSync) $solr = SolR::instance();

		if(isset($data['DT'])) {
			if($iddt)
				$this->update('TAB_DT', $data['DT'], 'ID_DT=:id', [':id'=>$iddt]);
			else {
				if(!isset($data['DT']['COMP_APPLICATIONS'])) $data['DT']['COMP_APPLICATIONS'] = '';
				if(!isset($data['DT']['COMP_INTERVENTIONS'])) $data['USDTER']['COMP_INTERVENTIONS'] = '';
				if(!isset($data['DT']['COMP_COMPETENCE'])) $data['DT']['COMP_COMPETENCE'] = '';
				if(!isset($data['DT']['DT_DIPLOMES'])) $data['DT']['DT_DIPLOMES'] = '';
				if(!isset($data['DT']['DT_OUTILS'])) $data['DT']['DT_OUTILS'] = '';
				if(!isset($data['DT']['DT_LANGUES'])) $data['DT']['DT_LANGUES'] = '';
				$iddt = $this->insert('TAB_DT', $data['DT']);
			}
			$data['PROFIL']['ID_DT'] = $iddt;
		}

		if(isset($data['REFERENCES']) && $iddt) {
			//On met à jour les références du DT
			$ids = array_column($data['REFERENCES'], 'ID_REFERENCE');
			if($ids) {
				$this->delete('TAB_REFERENCE', 'ID_DT = ?  AND ID_REFERENCE NOT IN (' . Where::prepareWhereIN($ids) . ')', array_merge([$iddt] , $ids));
			}else{
				$this->delete('TAB_REFERENEC', 'ID_DT = ?', $iddt);
			}

			foreach($data['REFERENCES'] as $reference)
				if(isset($reference['ID_REFERENCE'])) {
					$queryParams = [ ':id' => $reference['ID_REFERENCE'], ':iddt' => $iddt];
					$this->update('TAB_REFERENCE', $reference, 'ID_REFERENCE=:id AND ID_DT=:iddt', $queryParams);
				}else {
					$reference['ID_DT'] = $iddt;
					$this->insert('TAB_REFERENCE', $reference);
				}
		}

		// on met à jour les réseaux sociaux
		//~ - passer un tableau vide pour supprimer tous les réseaux
		//~ - passer une url vide pour supprimer le réseau correspondant (même comportement que sur l'ui v6)
		//~ - passer le réseau et une url valide pour l'ajouter ou le modifier
		if(isset($data['WEBSOCIALS'])) {
			$existing_networks = Tools::getFieldsToArray(
				$this->exec('SELECT WEBSOCIAL_NETWORK FROM TAB_WEBSOCIAL WHERE ID_PARENT = ? AND WEBSOCIAL_TYPE = ?',
							[$id, Models\WebSocial::WEBSOCIAL_TYPE_RESOURCE])
				,'WEBSOCIAL_NETWORK');
			if($networks = array_column($data['WEBSOCIALS'], 'WEBSOCIAL_NETWORK')) {
				foreach($data['WEBSOCIALS'] as $data_social) {
					if(empty($data_social['WEBSOCIAL_URL'])){
						$this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ? AND WEBSOCIAL_NETWORK = ?',
						[$id, Models\WebSocial::WEBSOCIAL_TYPE_RESOURCE, $data_social['WEBSOCIAL_NETWORK']]);
					} else if(in_array($data_social['WEBSOCIAL_NETWORK'], $existing_networks)) {
						$this->update('TAB_WEBSOCIAL', $data_social,
							'ID_PARENT = :parent AND WEBSOCIAL_TYPE = :type AND WEBSOCIAL_NETWORK = :network',
							['parent' => $id, 'type' => Models\WebSocial::WEBSOCIAL_TYPE_RESOURCE, 'network' => $data_social['WEBSOCIAL_NETWORK']]);
					} else
						$this->insert('TAB_WEBSOCIAL', array_merge($data_social,[
							'WEBSOCIAL_TYPE' => Models\WebSocial::WEBSOCIAL_TYPE_RESOURCE,
							'ID_PARENT' => $id
						]));
				}
			} else $this->delete('TAB_WEBSOCIAL', 'ID_PARENT = ? AND WEBSOCIAL_TYPE = ?', [$id, Models\WebSocial::WEBSOCIAL_TYPE_RESOURCE]);
		}

		$data['PROFIL']['PROFIL_DATEUPDATE'] = date('Y-m-d H:i:s');
		if(isset($data['PROFIL']['PROFIL_DATE'])) $data['PROFIL']['PROFIL_DATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['PROFIL']['PROFIL_DATE'])->format('Y-m-d H:i:s');
		$this->update('TAB_PROFIL', $data['PROFIL'], 'ID_PROFIL=:id', [':id' => $id]);

		if(isset($solr)) {
			$solr->Modification(BM::SOLR_MODULE_RESOURCES);
		}
	}

	/**
	 * delete all data related to a resource
	 * @param int $id resource ID
	 * @param int $iddt resource ID_DT
	 * @param bool $solr perform a SolR sync
	 * @throws \Exception
	 */
	public function deleteResourceData($id, $iddt = 0, $solr = true)
	{
		//On supprime les paramètres, le DT, la compétence et les références du profil
		$this->delete('TAB_DT', 'ID_DT=?', $iddt);
		$this->delete('TAB_REFERENCE', 'ID_DT=?', $iddt);

		// On supprime les ribs
		$this->delete('TAB_RIB', 'RIB_TYPE=1 AND ID_PARENT=?', $id);

		//On supprime les réseaux sociaux de la société
		$this->delete('TAB_WEBSOCIAL', 'ID_PARENT=? AND WEBSOCIAL_TYPE=0', $id);

		//On supprime les actions associés à ce profil
		$dict = Services\Actions::getDictionaryForCategory(BM::CATEGORY_RESOURCE);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id'));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		$this->delete('TAB_INTQUOTA', 'ID_PROFIL=?', $id);

		//On supprime tous les CV et les pièces jointes de cette ressource
		$dbFile = new File();
		$dbFile->deleteAllObjects( File::TYPE_RESOURCE_RESUME, $id);
		$dbFile->deleteAllObjects( File::TYPE_OTHER, $id);

		//On supprime tous les positionnements de la ressource
		$this->delete('TAB_POSITIONNEMENT', 'ID_RESPPROFIL=?',$id);

		//On supprime tous les flags de cette action
		$dbFlag = new Flag();
		$dbFlag->removeAllFlagsFromEntity($id, Models\Flag::TYPE_RESOURCE);

		//On supprime les liaisons entre candidats et consultants
		$this->update('TAB_PROFIL', ['ID_EMBAUCHE'=>0], 'ID_EMBAUCHE=:id', ['id'=>$id]);

		$opp = new Opportunity();
		$result = $this->exec('SELECT ID_POSITIONNEMENT FROM TAB_POSITIONNEMENT WHERE ID_ITEM=? AND ITEM_TYPE=0', $id);
		foreach($result as $row){
			$opp->deletePositioning($row['ID_POSITIONNEMENT']);
		}

		$this->delete('TAB_AVANTAGE', 'ID_PROFIL=?', $id);

		//TODO : contrats
		//On supprime les données RH et paie de l'intervenant (Tous les contrats)
		//$contratData = new BoondManager_ObjectBDD_Contrat($this->db);
		//foreach($this->db->fetch_all_array('SELECT ID_CONTRAT FROM TAB_CONTRAT WHERE ID_PROFIL="'.$this->db->escape($id).'"') as $contrat) $contratData->deleteContratData($contrat['ID_CONTRAT']);

		//TODO : projets
		//On supprime tous les projets en Recrutement de ce profil et les périodes d'inactivités
		//$projetData = new BoondManager_ObjectBDD_Projet($this->db);
		//foreach($this->db->fetch_all_array('SELECT ID_PROJET FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE TAB_MISSIONPROJET.ID_ITEM="'.$this->db->escape($id).'" AND ITEM_TYPE=0 AND (PRJ_TYPE<0 OR PRJ_TYPE='.AO_RECRUTEMENT.')') as $projet) $projetData->deleteProjetData($projet['ID_PROJET'], $solr);

		//TODO : mission
		//On supprime toutes les missions profils au forfait/at de ce profil
		//foreach($this->db->fetch_all_array('SELECT ID_MISSIONPROJET FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE TAB_MISSIONPROJET.ID_ITEM="'.$this->db->escape($id).'" AND ITEM_TYPE=0 AND PRJ_TYPE>0 AND PRJ_TYPE<>'.AO_RECRUTEMENT) as $mission) $projetData->deleteProjetMission($mission['ID_MISSIONPROJET']);

		//TODO : TpsFrsAbs
		//On supprime toutes les informations du module Temps&Frais&Absence
		//$activiteData = new BoondManager_ObjectBDD_TpsFrsAbs($this->db);
		//$activiteData->deleteAllDataTpsFrsAbsProfil($id);

		$user = $this->singleExec('SELECT ID_USER, USER_TYPE FROM TAB_USER WHERE ID_PROFIL=?', $id);
		if($user){
			$userDB = new Account();
			$userDB->deleteAccount($user->ID_USER);
		}

		//On supprime le profil
		$this->delete('TAB_PROFIL', 'ID_PROFIL=?', $id);

		//On indique la suppression dans la table TAB_DELETED
		$this->insert('TAB_DELETED', ['ID_PARENT' => $id, 'DEL_TYPE' => BM::CATEGORY_RESOURCE, 'DEL_DATE' => date('Y-m-d H:i:s')]);

		if($solr) SolR::instance()->Suppression(BM::SOLR_MODULE_RESOURCES);
	}

	/**
	 * create or update a resource
	 * @param array $data new data group by category (PROFIL, DT, REFERENCES, CV)
	 * @param int $id resource ID
	 * @param int $iddt TD ID
	 * @param bool $copy import resume (if new resource)
	 * @param bool $solr trigger a SolR sync
	 * @return int the resource ID
	 */
	public function setObject($data, $id = 0, $iddt = 0, $copy = false, $solr = true){
		if($id) $this->updateEmployee($data, $id, $iddt, $solr);
		else $id = $this->createEmployee($data, $copy, $solr);
		return $id;
	}

	/**
	 * @TODO faire la migration v6 -> lab
	 */
	public function getConfigResourceGroup($tabIds, $firstResult = false) {
		throw new \Exception('A MIGRER');
		return true;
	}

	/**
	 * Update and retrieve a resource's availability date
	 * @param int $id Resource ID
	 * @param bool $update if `true` Update the resource's availability date
	 * @param bool $force force the update
	 * @param bool $solr sync the solr database (in case of an update)
	 * @return \Wish\Models\Model|false
	 */
	public function updateResourceAvailability($id, $update=true, $force=false, $solr = true) {
		$resourceDB = new Employee();

		$query = new Query();
		$query->select('MAX(MP_FIN) AS DATE_DISPO, ID_RESPMANAGER, ID_RESPRH, PROFIL_NOM, PROFIL_PRENOM, PARAM_TYPEDISPO, PARAM_DATEDISPO')
			->from('TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=0 AND MP_TYPE<>1)')
			->addWhere('PRJ_TYPE>0 AND PRJ_TYPE<>3') // 3 = AO_RECRUTEMENT
			->addWhere('TAB_PROFIL.ID_PROFIL = ?', $id)
			->addWhere('PROFIL_TYPE <> 9'); // 9 = PROFIL_CANDIDAT

		if($force){
			$whereDisp = new Where('PARAM_TYPEDISPO=? OR PARAM_TYPEDISPO=?', [Models\Employee::DISPO_ASAP, Models\Employee::DISPO_DATE]);
			$query->addWhere($whereDisp);
		}

		//On recherche la plus grande date de fin de mission de ce profil
		$result = $this->singleExec($query);
		if($result && isset($result['DATE_DISPO']) && $update) {//On a trouvé la dernière date de disponibilité de ce profil
			$newData = [
				'PROFIL' => [
					'PARAM_TYPEDISPO' => Models\Employee::DISPO_DATE,
					'PARAM_DATEDISPO' => $result['DATE_DISPO']
				]
			];
			$resourceDB->setObject($newData, $id,0, $solr);
			$oldData = [
				'ID_RESPMANAGER' => $result['ID_RESPMANAGER'],
				'ID_RESPRH' => $result['ID_RESPRH'],
				'PROFIL_NOM' => $result['PROFIL_NOM'],
				'PROFIL_PRENOM' => $result['PROFIL_PRENOM'],
				'PARAM_TYPEDISPO' => $result['PARAM_TYPEDISPO'],
				'PARAM_DATEDISPO' => $result['PARAM_DATEDISPO']
			];
			Notification\Resource::getInstance($id, Models\Employee::TAB_INFORMATION, $oldData, $newData)->updateDispo();
		} else if($update) {//Si la mise à jour est demandée, on positionne le type de dispo à immédiat
			$query = new Query();
			$query->select('PARAM_TYPEDISPO, ID_RESPMANAGER, ID_RESPRH, PROFIL_NOM, PROFIL_PRENOM, PARAM_TYPEDISPO, PARAM_DATEDISPO')
				->from('TAB_PROFIL')
				->addWhere('ID_PROFIL=?', $id)
				->addWhere('PROFIL_TYPE <> 9'); //9 = PROFIL_CANDIDAT
			if(isset($whereDisp)) $query->addWhere($whereDisp);
			$subresult = $this->singleExec($query);
			if($subresult) {
				$newData = [
					'PROFIL' => [
						'PARAM_TYPEDISPO' => Models\Employee::DISPO_ASAP,
						'PARAM_DATEDISPO' => date('Y-m-d')
					]
				];
				$resourceDB->setObject($newData, $id,0, $solr);
				$oldData = [
					'ID_RESPMANAGER' => $result['ID_RESPMANAGER'],
					'ID_RESPRH' => $result['ID_RESPRH'],
					'PROFIL_NOM' => $result['PROFIL_NOM'],
					'PROFIL_PRENOM' => $result['PROFIL_PRENOM'],
					'PARAM_TYPEDISPO' => $result['PARAM_TYPEDISPO'],
					'PARAM_DATEDISPO' => $result['PARAM_DATEDISPO']
				];
				Notification\Resource::getInstance($id, Models\Employee::TAB_INFORMATION, $oldData, $newData)->updateDispo();
			}
		}
		return $result;
	}

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchEmployees $filter
	 * @return \Wish\Models\SearchResult
	 */
	public function searchEmployees(SearchEmployees $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		//récupération des parametres de bases (page, nb d'element par page)
		$query->setLimit(  $filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		//SI RECHERCHE SUR PERIODE AUTRE QUE DISPONIBILITE OU SELON UN PROJET/CONTACT CRM/SOCIETE CRM ALORS MODE=MYSQL
		$prjKeywords = $this->getListIdSearch($filter->keywords->getValue(), array('PRJ' => 'TAB_PROJET.ID_PROJET', 'CCON' => 'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC' => 'TAB_CRMCONTACT.ID_CRMSOCIETE'));
		$query->addWhere($prjKeywords);

		$aoKeywords = $this->getListIdSearch( $filter->keywords->getValue(), ['AO'=>'TAB_AO.ID_AO']);
		if(!$aoKeywords->isEmpty()){
			$query->addJoin('INNER JOIN TAB_POSITIONNEMENT POSB ON(POSB.ID_ITEM=AT.ID_PROFIL AND POSB.ITEM_TYPE=0) INNER JOIN TAB_AO ON(TAB_AO.ID_AO=POSB.ID_AO)');
			$query->addWhere($aoKeywords);
		}

		$query->groupBy('AT.ID_PROFIL');
		$query->addColumns('AT.ID_PROFIL, AT.PROFIL_NOM, AT.PROFIL_PRENOM, AT.PROFIL_REFERENCE, AT.PROFIL_SITUATION,
			AT.PROFIL_DATE, AT.PROFIL_DATEUPDATE, AT.PROFIL_DATENAISSANCE, AT.PROFIL_VISIBILITE, AT.PROFIL_NATIONALITE, AT.PROFIL_TEL1,
			AT.PROFIL_TEL2, AT.PROFIL_TEL3, AT.PROFIL_EMAIL, AT.PROFIL_EMAIL2, AT.PROFIL_EMAIL3, AT.PROFIL_TYPE, AT.PROFIL_ETAT,
			AT.PROFIL_ADR, AT.PROFIL_CP, AT.PROFIL_VILLE, AT.PROFIL_PAYS, AT.PROFIL_CIVILITE,
			AT.PARAM_TARIF1, AT.PARAM_DEVISEAGENCE, AT.PARAM_CHANGEAGENCE, AT.PARAM_DEVISE, AT.PARAM_CHANGE,
			AT.PARAM_TYPEDISPO, AT.PARAM_DATEDISPO, AT.PARAM_MOBILITE, AT.ID_RESPRH, AT.ID_RESPMANAGER, AT.ID_SOCIETE,
			COMP_COMPETENCE, COMP_APPLICATIONS,
			DT_TITRE, DT_FORMATION, DT_EXPERIENCE,
			RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM, RESP.ID_PROFIL AS RESP_ID,
			ID_CV, COUNT(DISTINCT ID_CV) AS NB_CV,
			USAT.ID_USER,
			0 AS CURRENT_UPDATE, 0 AS CURRENT_DELETE');

		$query->from('TAB_PROFIL AT');
		$query->addJoin('LEFT JOIN TAB_DT USING(ID_DT) LEFT JOIN TAB_REFERENCE USING(ID_DT)
			LEFT JOIN TAB_USER USAT ON(USAT.ID_PROFIL=AT.ID_PROFIL)
			LEFT JOIN TAB_CV ON TAB_CV.ID_PROFIL=AT.ID_PROFIL
			LEFT JOIN TAB_USER USRH ON(USRH.ID_USER=AT.ID_RESPRH)
			LEFT JOIN TAB_USER USRESP ON(USRESP.ID_USER=AT.ID_RESPMANAGER) LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=USRESP.ID_PROFIL)');

		// join with states
		$oppStates = Services\Dictionary::getActiveStatesIDs('specific.setting.state.opportunity');
		$posStates = Services\Dictionary::getActiveStatesIDs('specific.setting.state.positioning');

		$posQuery = (new Query())->select('COUNT(DISTINCT ID_POSITIONNEMENT)')
			->from('TAB_AO')
			->addJoin('INNER JOIN TAB_POSITIONNEMENT ON(TAB_POSITIONNEMENT.ID_AO=TAB_AO.ID_AO)')
			->addWhere('ID_ITEM=AT.ID_PROFIL AND ITEM_TYPE=0');

		if($posStates) $posQuery->addWhere('POS_ETAT NOT IN ('.implode(',', array_map(function($v){ return intval($v); }, $posStates)).')');
		if($oppStates) $posQuery->addWhere('AO_ETAT NOT IN ('.implode(',', array_map(function($v){ return intval($v); }, $oppStates)).')');

		//$query->addJoin('LEFT JOIN TAB_POSITIONNEMENT POSA ON(POSA.ID_POSITIONNEMENT=('.$posQuery->getQuery().'))');
		$query->addColumns('('.$posQuery->getQuery().') AS NB_POS');
		//COUNT(DISTINCT POSA.ID_POSITIONNEMENT) AS NB_POS,


		if($filter->returnHRManager->getValue() == 1) {
			$query->addColumns('RH.PROFIL_NOM AS RESPRH_NOM, RH.PROFIL_PRENOM AS RESPRH_PRENOM, RH.ID_PROFIL AS RESPRH_ID');
			$query->addJoin('LEFT JOIN TAB_PROFIL RH ON(RH.ID_PROFIL=USRH.ID_PROFIL)');
		}

		//EXCLUSION SELON PROFIL_TYPE
		if(sizeof($filter->excludeResourceTypes->getValue()) > 0)
			$query->addWhere('AT.PROFIL_TYPE NOT IN(9,'. Where::prepareWhereIN($filter->excludeResourceTypes->getValue()) .')', $filter->excludeResourceTypes->getValue());
		else
			$query->addWhere('AT.PROFIL_TYPE<>9');//Exclusion des candidats

		//UNIQUEMENT SI PAS DE COMPTE MANAGER
		if($filter->excludeManager->getValue()==1)
			$query->addWhere('(USAT.ID_USER IS NULL OR USAT.USER_TYPE=6)');

		$query->addWhere($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'AT.ID_SOCIETE',
			$colPole = 'AT.ID_POLE',
			$colUser = ['USRESP.ID_PROFIL','USRH.ID_PROFIL'],
			true,
			$filter->narrowPerimeter->getValue()
		));

		if($filter->availabilityType->getValue())
			$query->addWhere('(AT.PARAM_TYPEDISPO IN(1,3) OR (AT.PARAM_TYPEDISPO IN(2,4) AND AT.PARAM_DATEDISPO <= ?))', [date('Y-m-d',time()+3600*24*15)]);

		//PERIODE
		if($filter->getPeriodType() == SearchEmployees::PERIOD_WORKING || !$prjKeywords->isEmpty())
			$query->addJoin(' INNER JOIN TAB_MISSIONPROJET MP ON(MP.ID_ITEM=AT.ID_PROFIL AND MP.ITEM_TYPE=0)
						INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=MP.ID_PROJET AND PRJ_TYPE>0)
						  LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT)');

		switch($filter->getPeriodType()) {
			case SearchEmployees::PERIOD_WORKING:
				$query->addWhere('DATEDIFF(MP.MP_FIN,?)>=0 AND DATEDIFF(?,MP.MP_DEBUT)>=0', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_ABSENT:
				$query->addJoin(' INNER JOIN TAB_MISSIONPROJET MP ON(MP.ID_ITEM=AT.ID_PROFIL AND MP.ITEM_TYPE=0) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=MP.ID_PROJET AND PRJ_TYPE=-1)');
				$query->addWhere('DATEDIFF(MP.MP_FIN,?)>=0 AND DATEDIFF(?,MP.MP_DEBUT)>=0', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_IDLE:
				$query->addJoin(' INNER JOIN TAB_MISSIONPROJET MP ON(MP.ID_ITEM=AT.ID_PROFIL AND MP.ITEM_TYPE=0) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=MP.ID_PROJET AND PRJ_TYPE=-2)');
				$query->addWhere('DATEDIFF(MP.MP_FIN,?)>=0 AND DATEDIFF(?,MP.MP_DEBUT)>=0', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_HIRED:
				$query->addWhere('(SELECT MIN(CTR_DEBUT) AS CTR_DEBUT FROM TAB_CONTRAT WHERE TAB_CONTRAT.ID_PROFIL=AT.ID_PROFIL) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_LEFT:
				$query->addWhere('(SELECT MAX(CTR_FIN) AS CTR_FIN FROM TAB_CONTRAT WHERE TAB_CONTRAT.ID_PROFIL=AT.ID_PROFIL) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_EMPLOYED:
				$query->addJoin(' INNER JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=AT.ID_PROFIL)');
				$query->addWhere('DATEDIFF(CTR_FIN,?)>=0 AND DATEDIFF(?,CTR_DEBUT)>=0', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_AVAILABLE:
				$query->addWhere('AT.PARAM_DATEDISPO BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchEmployees::PERIOD_UNEMPLOYED:
				$this->exec('SET SQL_BIG_SELECTS=1');
				// TODO: normalement, le ON pourrait passer dans la clause where et on pourrait beneficier du binding pdo (a voir si ca ne change pas les perfs de la requete)
				$query->addJoin(' LEFT JOIN TAB_CONTRAT ON(ID_CONTRAT=(SELECT ID_CONTRAT
															  FROM TAB_CONTRAT
															  WHERE ID_PROFIL=AT.ID_PROFIL AND
																	DATEDIFF(\''.$this->escape($filter->endDate->getValue()).'\',CTR_DEBUT)>=0 AND
																	DATEDIFF(CTR_FIN,\''.$this->escape($filter->startDate->getValue()).'\')>=0
															  LIMIT 0,1))');
				$query->addWhere('ID_CONTRAT IS NULL');
				break;
			case SearchEmployees::PERIOD_ACTIONS:
				if(!is_null($filter->getPeriodAction())) {
					$query->addJoin(' INNER JOIN TAB_ACTION ON(ACTION_TYPE='.$this->escape($filter->getPeriodAction()).' AND TAB_ACTION.ID_PARENT=AT.ID_PROFIL)');
					$query->addWhere('DATE(ACTION_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				}
				break;
		}

		//filtres exacts
		$query->addWhere( $this->getFilterSearch($filter->resourceTypes->getValue(), 'AT.PROFIL_TYPE') )
			  ->addWhere( $this->getFilterSearch($filter->resourceStates->getValue(), 'AT.PROFIL_ETAT') )
			  ->addWhere( $this->getFilterSearch($filter->experiences->getValue(), 'DT_EXPERIENCE') )
			  ->addWhere( $this->getFilterSearch($filter->trainings->getValue(), 'DT_FORMATION') )
		// filtre aproximatifs (LIKE)
			  ->addWhere( $this->getFilterSearch($filter->activityAreas->getValue(), 'COMP_APPLICATIONS', [], true) )
			  ->addWhere( $this->getFilterSearch($filter->expertiseAreas->getValue(), 'COMP_INTERVENTIONS', [], true) )
			  ->addWhere( $this->getFilterSearch($filter->languages->getValue(), 'DT_LANGUES', [], true) )
			  ->addWhere( $this->getFilterSearch($filter->mobilityAreas->getValue(), 'AT.PARAM_MOBILITE', [], true) )
			  ->addWhere( $this->getFilterSearch($filter->tools->getValue(), 'DT_OUTILS', [], true) );

		//~ Show only if visible
		if($filter->visibleProfile->getValue() && !$this->always_access) $query->addWhere('AT.PROFIL_VISIBILITE=1');

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_RESOURCE.' AND TAB_FLAG.ID_PARENT=AT.ID_PROFIL)');
			$query->addWhere('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$keywords = str_replace('%', '\%', $filter->keywords->getValue());
		$keywords = str_replace('*', '%', $keywords);

		if($keywords) {
			$whereKeywords = $this->getListIdSearch($keywords, array('COMP' => 'AT.ID_PROFIL'));
			$query->addWhere($whereKeywords);

			if($prjKeywords->isEmpty() && $aoKeywords->isEmpty() && $whereKeywords->isEmpty()) {
				$likeSearch = $keywords.'%';

				switch($filter->keywordsType->getValue()) {
					case SearchEmployees::KEYWORD_TYPE_LASTNAME://Nom
						$query->addWhere('AT.PROFIL_NOM LIKE ?', $likeSearch);
						break;
					case SearchEmployees::KEYWORD_TYPE_FIRSTNAME://Prénom
						$query->addWhere('AT.PROFIL_PRENOM LIKE ?', $likeSearch);
						break;
					case SearchEmployees::KEYWORD_TYPE_FULLNAME://Nom#Prénom
						$tabKeywords = explode('#', $keywords);
						$query->addWhere('AT.PROFIL_NOM LIKE ?', $tabKeywords[0].'%');
						$query->addWhere('AT.PROFIL_PRENOM LIKE ?', $tabKeywords[1].'%');
						break;
					case SearchEmployees::KEYWORD_TYPE_EMAILS:
						$tabEmail = explode(' ',$keywords);
						array_filter($tabEmail, function($value){
							return filter_var($value, FILTER_VALIDATE_EMAIL)!==false;
						});

						if(sizeof($tabEmail) > 0) {
							$email = (new Where('AT.PROFIL_EMAIL IN ?', $tabEmail))
								->or_('AT.PROFIL_EMAIL2 IN ?', $tabEmail)
								->or_('AT.PROFIL_EMAIL3 IN ?', $tabEmail);
							$query->addWhere($email);
						}
						break;
					case SearchEmployees::KEYWORD_TYPE_TITLE:
						$query->addWhere('MATCH(DT_TITRE, DT_OUTILS) AGAINST (?)', $keywords);
						break;
					case SearchEmployees::KEYWORD_TYPE_PHONES:
						$query->addWhere('AT.PROFIL_TEL1 LIKE ? OR AT.PROFIL_TEL2 LIKE ? OR AT.PROFIL_TEL3 LIKE ?', [$likeSearch, $likeSearch, $likeSearch]);
						break;
					case SearchEmployees::KEYWORD_TYPE_NUMBER:
						$query->addWhere('AT.PROFIL_REFERENCE LIKE ?', $keywords);
						break;
					case SearchEmployees::KEYWORD_TYPE_RESUME:
						$query->addWhere('MATCH(CV_TEXT) AGAINST(?)', $keywords);
						$pertinence_expr = 'MATCH(CV_TEXT) AGAINST(' . $this->escape($keywords) . ')';
						$query->addColumns([ 'PERTINENCE'=> $pertinence_expr ]);
						$query->addOrderBy('PERTINENCE DESC');
						break;
					case SearchEmployees::KEYWORD_TYPE_TD:
						$query->addWhere('(MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(?)' .
							' OR MATCH(DT_TITRE, DT_OUTILS) AGAINST(?)' .
							' OR MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(?)' .
							' OR AT.PROFIL_NOM LIKE ?' .
							' OR AT.PROFIL_PRENOM LIKE ?)', array($keywords, $keywords, $keywords, $likeSearch, $likeSearch));
						$escapedKeywords = $this->escape($keywords);
						$pertinence_expr = 'MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(' . $escapedKeywords . ')' .
							' + MATCH(DT_TITRE, DT_OUTILS) AGAINST(' . $escapedKeywords . ')' .
							' + MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(' . $escapedKeywords .')';
						$query->addColumns([ 'PERTINENCE'=> $pertinence_expr ]);
						$query->addOrderBy('PERTINENCE DESC');
						break;
					default:case SearchEmployees::KEYWORD_TYPE_RESUMETD:
						$query->addWhere('(MATCH(CV_TEXT) AGAINST(?)' .
							' OR MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(?)' .
							' OR MATCH(DT_TITRE, DT_OUTILS) AGAINST(?)' .
							' OR MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(?)' .
							' OR AT.PROFIL_NOM LIKE ?' .
							' OR AT.PROFIL_PRENOM LIKE ?)', array($keywords, $keywords, $keywords, $keywords, $likeSearch, $likeSearch));
						$escapedKeywords = $this->escape($keywords);
						$pertinence_expr = 'MATCH(CV_TEXT) AGAINST(' . $escapedKeywords .  ')' .
							' + MATCH(COMP_COMPETENCE, COMP_INTERVENTIONS, COMP_APPLICATIONS) AGAINST(' . $escapedKeywords .  ')' .
							' + MATCH(DT_TITRE, DT_OUTILS) AGAINST(' . $escapedKeywords .  ')' .
							' + MATCH(REF_TITRE, REF_DESCRIPTION) AGAINST(' . $escapedKeywords .')';
						$query->addColumns([ 'PERTINENCE'=> $pertinence_expr ]);
						$query->addOrderBy('PERTINENCE DESC');
				}
			}
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY (après l'éventuel ORDER_BY PERTINENCE DESC)

		return $this->launchSearch($query);
	}

	/**
	 * Add an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchEmployees::ORDERBY_UPDATEDATE                    => 'AT.PROFIL_DATEUPDATE',
			SearchEmployees::ORDERBY_LASTNAME                      => 'AT.PROFIL_NOM',
			SearchEmployees::ORDERBY_TITLE                         => 'DT_TITRE',
			SearchEmployees::ORDERBY_AVAILABILITY                  => 'AT.PARAM_DATEDISPO',
			SearchEmployees::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS => 'NB_POS',
			SearchEmployees::ORDERBY_PRICEEXCLUDINGTAX             => 'AT.PARAM_TARIF1',
			SearchEmployees::ORDERBY_MAINMANAGER_LASTNAME          => 'RESP.PROFIL_NOM'
		];

		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy( $mapping[$c].' '.$order);

		$query->addOrderBy( 'AT.PROFIL_DATEUPDATE DESC');
	}

	/**
	 * indicate if the candidate can be deleted
	 * @return boolean
	 */
	function isResourceReducible($id) {
		$nbDocument = 0;
		$sql = 'SELECT COUNT(ID_POSITIONNEMENT) AS NB_DOCUMENT FROM TAB_POSITIONNEMENT WHERE ID_ITEM=:id AND ITEM_TYPE=0
				UNION ALL
				SELECT COUNT(ID_MISSIONPROJET) AS NB_DOCUMENT FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE PRJ_TYPE>0 AND ITEM_TYPE=0 AND ID_ITEM=:id
				UNION ALL
				SELECT COUNT(ID_LISTETEMPS) AS NB_DOCUMENT FROM TAB_LISTETEMPS WHERE ID_PROFIL=:id
				UNION ALL
				SELECT COUNT(ID_LISTEFRAIS) AS NB_DOCUMENT FROM TAB_LISTEFRAIS WHERE ID_PROFIL=:id
				UNION ALL
				SELECT COUNT(ID_LISTEABSENCES) AS NB_DOCUMENT FROM TAB_LISTEABSENCES WHERE ID_PROFIL=:id
				UNION ALL
				SELECT COUNT(ID_INTQUO) AS NB_DOCUMENT FROM TAB_INTQUOTA WHERE ID_PROFIL=:id
				UNION ALL
				SELECT COUNT(ID_AVANTAGE) AS NB_DOCUMENT FROM TAB_AVANTAGE WHERE ID_PROFIL=:id';
		foreach($this->exec($sql, [':id'=>$id]) as $document)
			$nbDocument += $document['NB_DOCUMENT'];
		if($nbDocument == 0) return true; else return false;
	}
}
