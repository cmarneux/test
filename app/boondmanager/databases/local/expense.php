<?php
/**
 * expenses.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\Model;
use Wish\Models\SearchResult;
use BoondManager\OldModels\Filters;
use Wish\MySQL\AbstractDb;

class Expense extends AbstractObject{

	/**
	 * perform a search
	 * @param Filters\Search\Expenses $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function search(Filters\Search\Expenses $filter)
	{
		if(!$filter->isValid()) throw new \Exception('invalid filter');

		$commonColumns = 'TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT, PROFIL_TYPE, ID_LISTEFRAIS, 
						  TAB_LISTEFRAIS.ID_SOCIETE, LISTEFRAIS_DATE, LISTEFRAIS_AVANCE, LISTEFRAIS_DEVISEAGENCE, LISTEFRAIS_CHANGEAGENCE, 
						  LISTEFRAIS_BKMVALUE, ID_PROFILCDP, TYPEFRS_NAME, TYPEFRS_TVA,
						  PRJ_REFERENCE, TAB_PROJET.ID_CRMCONTACT, TAB_CRMCONTACT.ID_CRMSOCIETE, CCON_NOM, CCON_PRENOM, 
						  CSOC_SOCIETE, LISTEFRAIS_ETAT AS VAL_STATE';

		$forfaitColumns = 'ID_LIGNEFRAIS, ID_FRAIS, TAB_LIGNEFRAIS.ID_PROJET, TAB_LIGNEFRAIS.ID_MISSIONPROJET, TAB_LIGNEFRAIS.ID_LOT, LOT_TITRE,
						  0 AS FRAIS_REFACTURE, \'\' AS FRAIS_INTITULE, FRAIS_DATE, FRAIS_VALUE AS FRAIS_MONTANT, 
						  1 AS FRAIS_CHANGE, LISTEFRAIS_DEVISEAGENCE AS FRAIS_DEVISE, LIGNEFRAIS_TYPEFRSREF AS FRAIS_TYPEREF, 
						  LIGNEFRAIS_TVA AS FRAIS_TVA,  1 AS FRAIS_TYPE, 0 AS FRAIS_NUM';

		$realColumns = 'NULL AS ID_LIGNEFRAIS, ID_FRAISREEL AS ID_FRAIS, TAB_FRAISREEL.ID_PROJET, TAB_FRAISREEL.ID_MISSIONPROJET, TAB_FRAISREEL.ID_LOT, LOT_TITRE,
						FRAISREEL_REFACTURE AS FRAIS_REFACTURE, FRAISREEL_INTITULE AS FRAIS_INTITULE, 
						FRAISREEL_DATE AS FRAIS_DATE, FRAISREEL_MONTANT AS FRAIS_MONTANT, FRAISREEL_CHANGE AS FRAIS_CHANGE, 
						FRAISREEL_DEVISE AS FRAIS_DEVISE, FRAISREEL_TYPEFRSREF AS FRAIS_TYPEREF, FRAISREEL_TVA AS FRAIS_TVA, 0 AS FRAIS_TYPE, FRAISREEL_NUM AS FRAIS_NUM';

		$forfaitQuery = new Query();
		$forfaitQuery->addColumns($commonColumns);
		$forfaitQuery->addColumns($forfaitColumns);
		$forfaitQuery->from('TAB_FRAIS 
							 INNER JOIN TAB_LIGNEFRAIS USING(ID_LIGNEFRAIS) 
							 INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS) 
							 INNER JOIN TAB_PROFIL USING(ID_PROFIL) 
							 LEFT JOIN TAB_USER US ON (TAB_PROFIL.ID_RESPMANAGER = US.ID_USER)
							 LEFT JOIN TAB_USER USRH ON (TAB_PROFIL.ID_RESPRH = USRH.ID_USER)
							 LEFT JOIN TAB_TYPEFRAIS ON(TYPEFRS_REF=LIGNEFRAIS_TYPEFRSREF AND TAB_TYPEFRAIS.ID_SOCIETE=TAB_LISTEFRAIS.ID_SOCIETE)
							 LEFT JOIN TAB_PROJET USING(ID_PROJET) 
							 LEFT JOIN TAB_LOT USING(ID_LOT)
							 LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) 
							 LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)')
					 ->groupBy('TAB_FRAIS.ID_FRAIS');

		$realQuery = new Query();
		$realQuery->addColumns($commonColumns);
		$realQuery->addColumns($realColumns);
		$realQuery->from('TAB_FRAISREEL 
						  INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS) 
						  INNER JOIN TAB_PROFIL USING(ID_PROFIL) 
						  LEFT JOIN TAB_USER US ON (TAB_PROFIL.ID_RESPMANAGER = US.ID_USER)
						  LEFT JOIN TAB_USER USRH ON (TAB_PROFIL.ID_RESPRH = USRH.ID_USER)
						  LEFT JOIN TAB_TYPEFRAIS ON(TYPEFRS_REF=FRAISREEL_TYPEFRSREF AND TAB_TYPEFRAIS.ID_SOCIETE=TAB_LISTEFRAIS.ID_SOCIETE)
						  LEFT JOIN TAB_PROJET USING(ID_PROJET) 
						  LEFT JOIN TAB_LOT USING(ID_LOT)
						  LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) 
						  LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)')
				  ->groupBy('TAB_FRAISREEL.ID_FRAISREEL');

		$commonWhere = new Where();

		$wherePerimeter = $this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			'TAB_LISTEFRAIS.ID_SOCIETE',
			'TAB_PROFIL.ID_POLE',
			['US.ID_PROFIL','USRH.ID_PROFIL']
		);

		$commonWhere->and_($wherePerimeter);

		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), [
			'COMP' =>'TAB_PROFIL.ID_PROFIL',
			'PRJ' => 'TAB_LIGNETEMPS.ID_PROJET',
			'CCON' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
			'CSOC' => 'TAB_CRMSOCIETE.ID_CRMSOCIETE',
			'RPT'  => 'ID_LISTEFRAIS',
		]);

		if($filter->keywords->getValue() && $whereKeywords->isEmpty()){
			$keywords = str_replace('*', '%', str_replace('%', '\%', $filter->keywords->getValue()));
			$whereKeywords = (new Where('PROFIL_NOM LIKE ?', $keywords.'%'))
						->or_(new Where('PROFIL_PRENOM LIKE ?', $keywords.'%'));
		}

		$commonWhere->and_($whereKeywords);

		if($filter->activityType->getValue()){
			switch ($filter->activityType->getValue()){
				case Filters\Search\Expenses::TYPE_PRODUCTION:
					$whereType = new Where('ID_PROJET > 0');
					break;
				case Filters\Search\Expenses::TYPE_ABSENCE:
					$whereType = new Where('ID_PROJET = -1');
					break;
				case Filters\Search\Expenses::TYPE_INTERNAL:
					$whereType = new Where('ID_PROJET = -2');
					break;
			}
			$commonWhere->and_($whereType);
		}

		$whereProfil = $this->getFilterSearch($filter->resourceTypes->getValue(), 'PROFIL_TYPE');
		$commonWhere->and_($whereProfil);

		if($filter->excludeResourceTypes->getValue()) {
			$whereNotProfil = new Where('PROFIL_TYPE NOT IN (?)', $filter->excludeResourceTypes->getValue());
			$commonWhere->and_($whereNotProfil);
		}

		if($filter->startDate->isDefined()){
			$realQuery->addWhere( new Where('FRAISREEL_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]) );
			$forfaitQuery->addWhere( new Where('FRAIS_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]) );
		}

		$realQuery->addWhere($commonWhere);
		$forfaitQuery->addWhere($commonWhere);

		$this->setOrderExpr($realQuery, $filter->sort->getValue(), $filter->order->getValue());
		$this->setOrderExpr($forfaitQuery, $filter->sort->getValue(), $filter->order->getValue());

		/** @var Query[] $queries */
		$queries = [];
		if($filter->category->isDefined()){
			if( \BoondManager\Models\Expense::CATEGORY_FIXED == $filter->category->getValue()){
				$queries[] = $forfaitQuery;
			}else if(\BoondManager\Models\Expense::CATEGORY_ACTUAL == $filter->category->getValue()){
				$queries[] = $realQuery;
			}
		}else{
			$queries = [$realQuery, $forfaitQuery];
		}

		$totalRows = 0;
		$subQueries = [];
		$argQueries = [];
		foreach($queries as $q){
			$result = $this->singleExec( $q->getCountQuery(), $q->getArgs() );
			$totalRows += $result->NB_ROWS;
			$subQueries[] = $q->getQuery();
			$argQueries = array_merge($argQueries, $q->getArgs());
		}

		if($totalRows){
			$sql_union = 'SELECT * FROM (('.implode(') UNION ALL (', $subQueries).')) AS FRAIS '.$queries[0]->buildOrderBy().' LIMIT '.Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()).', '.$filter->maxResults->getValue();
			$result = $this->exec($sql_union, $argQueries, \BoondManager\Models\Expense::class, new MySQL\Mappers\SearchExpense());
		}else{
			$result = [];
		}

		$searchResult = new SearchResult($result, $totalRows);

		return $searchResult;
	}


	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			Filters\Search\Expenses::ORDERBY_LASTNAME => 'PROFIL_NOM',
			Filters\Search\Expenses::ORDERBY_CATEGORY => 'ID_PROJET',
			Filters\Search\Expenses::ORDERBY_STARTDATE => 'FRAIS_DATE'
		];

		if(!$column) $query->addOrderBy('ID_PROFIL ASC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('FRAIS_TYPE DESC');
		$query->addOrderBy('ID_FRAIS DESC');
	}
}
