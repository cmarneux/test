<?php
/**
 * expensesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\ExpensesReports\Filters\SearchExpensesReports;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use BoondManager\Models;

class ExpensesReport extends AbstractObject{

	/**
	 * search for expenses
	 * @param SearchExpensesReports $filter
	 * @return \Wish\Models\SearchResult <type>
	 * @throws \Exception
	 */
	public function search(SearchExpensesReports $filter)
	{
		if(!$filter->isValid()) throw new \Exception('filter is not valid');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));
		$query->select('DOCUMENT.ID_LISTEFRAIS AS ID_DOCUMENT, DOCUMENT.LISTEFRAIS_DATE AS DOCUMENT_DATE, DOCUMENT.LISTEFRAIS_ETAT AS DOCUMENT_ETAT, 
						AT.ID_PROFIL, AT.PROFIL_NOM, AT.PROFIL_PRENOM, AT.PROFIL_REFERENCE, AT.PROFIL_TYPE, DOCUMENT.ID_SOCIETE, 
						DOCUMENT.LISTEFRAIS_REGLE, DOCUMENT.LISTEFRAIS_AVANCE, LISTEFRAIS_CLOTURE AS DOCUMENT_CLOTURE, ID_VALIDATION, VAL_DATE');
		$query->from('TAB_LISTEFRAIS AS DOCUMENT 
					  INNER JOIN TAB_PROFIL AT ON(AT.ID_PROFIL=DOCUMENT.ID_PROFIL)
					  LEFT JOIN TAB_USER AS US ON (US.ID_USER=AT.ID_RESPMANAGER) 
					  LEFT JOIN TAB_USER AS USRH ON (USRH.ID_USER=AT.ID_RESPRH)
					  LEFT JOIN TAB_VALIDATION ON(ID_PARENT=DOCUMENT.ID_LISTEFRAIS AND VAL_TYPE=1)');

		$stateWhere = new Where();

		if($filter->perimeterManagersType->getValue() == SearchExpensesReports::PERIMETER_TYPE_VALIDATOR){

			$query->select('VAL_ETAT AS VAL_STATE, RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM, RESP.ID_PROFIL AS RESP_ID_PROFIL');
			$query->addJoin('LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=TAB_VALIDATION.ID_PROFIL) LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)');

			$query->addWhere( $this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'RESP.ID_SOCIETE',
				'RESP.ID_POLE',
				'RESP.ID_PROFIL'
			));

			$this->setOrderExprForValidators($query, $filter->sort->getValue(), $filter->order->getValue());

			foreach($filter->validationStates->getValue() as $etat){
				switch($etat){
					case Models\Validation::VALIDATION_VALIDATED:
						$stateWhere->or_('VAL_ETAT=1'); break;
					case Models\Validation::VALIDATION_REJECTED:
						$stateWhere->or_('VAL_ETAT=3'); break;
					case Models\Validation::VALIDATION_WAITING:
						$stateWhere->or_('VAL_ETAT=2 OR VAL_ETAT IS NULL'); break;
				}
			}

			$query->setCountColumn('DOCUMENT.ID_LISTEFRAIS');
		}else {
			$query->select('DOCUMENT.LISTEFRAIS_ETAT AS VAL_STATE');

			$query->addWhere($this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'AT.ID_SOCIETE',
				'AT.ID_POLE',
				['US.ID_PROFIL', 'USRH.ID_PROFIL']
			));
			foreach($filter->validationStates->getValue() as $etat){
				switch($etat){
					case Models\Validation::VALIDATION_VALIDATED:
						$stateWhere->or_('LISTEFRAIS_ETAT=1'); break;
					case Models\Validation::VALIDATION_REJECTED:
						$stateWhere->or_('LISTEFRAIS_ETAT=3'); break;
					case Models\Validation::VALIDATION_WAITING:
						$stateWhere->or_('LISTEFRAIS_ETAT=2'); break;
				}
			}

			$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

			$query->groupBy('DOCUMENT.ID_LISTEFRAIS');
		}
		$query->addWhere($stateWhere);

		$query->addWhere('DOCUMENT.LISTEFRAIS_DATE BETWEEN ? AND ?', [$filter->startMonth->getValue(), $filter->endMonth->getValue()]);

		if($filter->keywords->isDefined()){
			$whereIds = $this->getListIdSearch($filter->keywords->getValue(), ['COMP'=>'AT.ID_PROFIL', 'DOC'=>'DOCUMENT.ID_LISTEFRAIS']);
			if(!$whereIds->isEmpty()){
				$query->addWhere($whereIds);
			}else{

				$keywords = str_replace('*', '%', str_replace('%', '\%', $filter->keywords->getValue()));
				$query->addWhere('AT.PROFIL_NOM LIKE ? OR AT.PROFIL_PRENOM LIKE ?', [$keywords.'%', $keywords.'%']);
			}
		}

		if($filter->resourceTypes->isDefined()){
			$query->addWhere('AT.PROFIL_TYPE IN (?)', $filter->resourceTypes->getValue());
		}

		if($filter->closed->isDefined()){
			$query->addWhere('LISTEFRAIS_CLOTURE = ?', $filter->closed->getValue());
		}

		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			SearchExpensesReports::ORDERBY_LASTNAME => 'AT.PROFIL_NOM',
			SearchExpensesReports::ORDERBY_TERM     => 'DOCUMENT.LISTEFRAIS_DATE',
			SearchExpensesReports::ORDERBY_STATE    => 'DOCUMENT.LISTEFRAIS_ETAT',
			SearchExpensesReports::ORDERBY_PAID     => 'DOCUMENT.LISTEFRAIS_REGLE'
		];

		if(!$column) $query->addOrderBy('DOCUMENT.LISTEFRAIS_DATE DESC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('DOCUMENT.ID_LISTEFRAIS DESC');
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExprForValidators(Query $query, $column, $order) {

		$mapping = [
			SearchExpensesReports::ORDERBY_LASTNAME           => 'AT.PROFIL_NOM',
			SearchExpensesReports::ORDERBY_STATE              => 'VAL_ETAT',
			SearchExpensesReports::ORDERBY_VALIDATOR_LASTNAME => 'RESP.PROFIL_NOM',
			SearchExpensesReports::ORDERBY_DATE               => 'VAL_DATE',
			SearchExpensesReports::ORDERBY_TERM               => 'LISTEFRAIS_REGLE'
		];

		if(!$column) $query->addOrderBy('DOCUMENT.LISTEFRAIS_DATE DESC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('DOCUMENT.ID_LISTEFRAIS DESC');
	}

	/**
	 * Retrieve all expenses for a resource
	 * @param int $idprofil resource ID
	 * @return RowObject;
	 */
	public function getAllFrais($idprofil) {
		$sql = 'SELECT ID_LISTEFRAIS, LISTEFRAIS_DATE, LISTEFRAIS_ETAT AS VAL_STATE, LISTEFRAIS_REGLE, LISTEFRAIS_CLOTURE, 
					   ID_VALIDATION FROM TAB_LISTEFRAIS 
				LEFT JOIN TAB_VALIDATION ON(ID_PARENT=ID_LISTEFRAIS AND VAL_TYPE=1) 
				WHERE TAB_LISTEFRAIS.ID_PROFIL = ?
				GROUP BY ID_LISTEFRAIS ORDER BY LISTEFRAIS_DATE DESC;';
		$result = $this->exec($sql, $idprofil);
		return $result;
	}

	/**
	 * Retrieve all data for an Expense
	 * @param int $idfrais Expense ID
	 * @return RowObject
	 */
	public function getFraisData($idfrais) {
		$sql = 'SELECT ID_LISTEFRAIS, TAB_LISTEFRAIS.ID_PROFIL, TAB_LISTEFRAIS.ID_SOCIETE, LISTEFRAIS_DATE, LISTEFRAIS_ETAT, 
					   LISTEFRAIS_COMMENTAIRES, LISTEFRAIS_DEVISEAGENCE, LISTEFRAIS_CHANGEAGENCE, LISTEFRAIS_AVANCE, 
					   LISTEFRAIS_REGLE, LISTEFRAIS_BKMREF, LISTEFRAIS_BKMVALUE, BKM_NAME, BKM_VALUE, ID_POLE, 
					   ID_RESPMANAGER, ID_RESPRH, USER_TYPE, LISTEFRAIS_CLOTURE
				FROM TAB_LISTEFRAIS 
				INNER JOIN TAB_PROFIL USING(ID_PROFIL) 
				LEFT JOIN TAB_USER USING(ID_PROFIL) 
				LEFT JOIN TAB_BAREMEKM ON(BKM_REF=LISTEFRAIS_BKMREF AND TAB_BAREMEKM.ID_SOCIETE=TAB_LISTEFRAIS.ID_SOCIETE)
				WHERE ID_LISTEFRAIS = ?';
		$result = $this->singleExec($sql, $idfrais);

		if(!$result) return false;

		$this->data = $result;

		//On récupère les frais réels
		$sql = 'SELECT TAB_FRAISREEL.ID_FRAISREEL, FRAISREEL_TYPEFRSREF, FRAISREEL_INTITULE, FRAISREEL_REFACTURE, 
					   TAB_FRAISREEL.ID_PROJET, TAB_FRAISREEL.ID_MISSIONPROJET, TAB_FRAISREEL.ID_LOT, PRJ_REFERENCE, 
					   MP_NOM, MP_DEBUT, MP_FIN, CSOC_SOCIETE, TYPEFRS_NAME, TYPEFRS_TVA, FRAISREEL_DATE, FRAISREEL_NUM, 
					   FRAISREEL_MONTANT, FRAISREEL_TVA, FRAISREEL_DEVISE, FRAISREEL_CHANGE, LOT_TITRE
				FROM TAB_FRAISREEL 
				LEFT JOIN TAB_TYPEFRAIS ON TYPEFRS_REF=FRAISREEL_TYPEFRSREF
				LEFT JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET)  
				LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_FRAISREEL.ID_PROJET) 
				LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT) 
				LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
				LEFT JOIN TAB_LOT USING(ID_LOT)
				WHERE ID_LISTEFRAIS = :idExpense AND TAB_TYPEFRAIS.ID_SOCIETE = :idCompany
				GROUP BY ID_FRAISREEL ORDER BY TAB_FRAISREEL.ID_FRAISREEL ASC';

		$result->LISTEFRAISREEL = $this->exec($sql, ['idExpense'=>$result->ID_LISTEFRAIS, 'idCompany'=>$result->ID_SOCIETE]);

		//On récupère les frais au forfait
		$sql = 'SELECT TAB_LIGNEFRAIS.ID_LIGNEFRAIS, LIGNEFRAIS_TYPEFRSREF, LIGNEFRAIS_TVA, TAB_LIGNEFRAIS.ID_PROJET, 
					   TAB_LIGNEFRAIS.ID_MISSIONPROJET, TAB_LIGNEFRAIS.ID_LOT, PRJ_REFERENCE, MP_NOM, MP_DEBUT, MP_FIN, 
					   CSOC_SOCIETE, TYPEFRS_NAME, TYPEFRS_TVA, LOT_TITRE
				FROM TAB_LIGNEFRAIS 
				LEFT JOIN TAB_TYPEFRAIS ON TYPEFRS_REF=LIGNEFRAIS_TYPEFRSREF 
				LEFT JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET) 
				LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_LIGNEFRAIS.ID_PROJET) 
				LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT) 
				LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
				LEFT JOIN TAB_LOT USING(ID_LOT)
				WHERE ID_LISTEFRAIS = :idExpense AND TAB_TYPEFRAIS.ID_SOCIETE = :idCompany
				GROUP BY ID_LIGNEFRAIS ORDER BY ID_LIGNEFRAIS ASC';

		$result->LISTEFRAISFORFAIT = $this->exec($sql, ['idExpense'=>$result->ID_LISTEFRAIS, 'idCompany'=>$result->ID_SOCIETE]);

		foreach($result->LISTEFRAISFORFAIT as $i => $ligne)
			$result->LISTEFRAISFORFAIT[$i]['LISTEJOURS'] = $this->exec('SELECT FRAIS_DATE, FRAIS_VALUE FROM TAB_FRAIS WHERE ID_LIGNEFRAIS=? ORDER BY FRAIS_DATE ASC', $ligne['ID_LIGNEFRAIS']);

		//On récupère tous les justifiatifs
		$result->JUSTIFICATIFS = $this->exec('SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF WHERE ID_PARENT= ?  AND JUSTIF_TYPE=0', $result['ID_LISTEFRAIS']);

		//On récupère toutes les validations
		$result->LISTEVALIDATIONS = $this->exec('SELECT ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, TAB_VALIDATION.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT FROM TAB_VALIDATION INNER JOIN TAB_PROFIL ON(ID_VALIDATEUR=TAB_PROFIL.ID_PROFIL) WHERE ID_PARENT=? AND VAL_TYPE=1 ORDER BY ID_VALIDATION ASC', $idfrais);

		$timeDB = new Times();
		$result->ID_LISTETEMPS = $timeDB->isTempsExist($result['LISTEFRAIS_DATE'], $result['ID_PROFIL']);
		if(!$result->ID_LISTETEMPS) $result->ID_LISTETEMPS = 0;

		return $result;
	}

	/**
	 * \brief Met à jour toutes les données d'une note de frais
	 * @param <type> $data tableau de données
	 * @param <type> $idfrais identifiant de la note de frais
	 * @return <type> identifiant de la note de frais
	 */
	public function updateFraisData($data, $idfrais=0) {
		throw new \Exception('TODO'); //TODO
		if(isset($data['FRAIS'])) {($idfrais > 0)?$this->db->query_update('LISTEFRAIS', $data['FRAIS'], 'ID_LISTEFRAIS="'.$this->db->escape($idfrais).'"'):$idfrais=$this->db->query_insert('LISTEFRAIS', $data['FRAIS']);}

		if(isset($data['LISTEFRAISREEL']) && $idfrais > 0) {
			//On supprime les frais réels qui n'existent plus
			foreach($this->db->fetch_all_array('SELECT ID_FRAISREEL FROM TAB_FRAISREEL WHERE ID_LISTEFRAIS="'.$this->db->escape($idfrais).'";') as $lignefrs) {
				$etat = false;
				foreach($data['LISTEFRAISREEL'] as $data_lignefrs) if($data_lignefrs[0] == $lignefrs['ID_FRAISREEL']) {$etat = true;break;}
				if(!$etat) $this->db->query_delete('FRAISREEL', 'ID_FRAISREEL="'.$lignefrs['ID_FRAISREEL'].'"');
			}

			//On met à jour les frais réels
			if(sizeof($data['LISTEFRAISREEL']) > 0) {
				foreach($data['LISTEFRAISREEL'] as $data_lignefrs) {
					$dataFrais = array();
					$dataFrais['FRAISREEL_NUM'] = $data_lignefrs[1];
					$dataFrais['FRAISREEL_DATE'] = $data_lignefrs[2];
					$dataFrais['ID_PROJET'] = $data_lignefrs[3];
					$dataFrais['ID_MISSIONPROJET'] = $data_lignefrs[4];
					$dataFrais['FRAISREEL_TYPEFRSREF'] = $data_lignefrs[5];
					if($data_lignefrs[5] != '') $dataFrais['FRAISREEL_TVA'] = $data_lignefrs[6];//Si TVA est défini, alors on modifie la TVA, sinon alors le TYPE n'existe plus on conserve la dernière valeur enregistrée
					$dataFrais['FRAISREEL_INTITULE'] = $data_lignefrs[7];
					$dataFrais['FRAISREEL_DEVISE'] = $data_lignefrs[8];
					$dataFrais['FRAISREEL_CHANGE'] = $data_lignefrs[9];
					$dataFrais['FRAISREEL_MONTANT'] = $data_lignefrs[10];
					$dataFrais['FRAISREEL_REFACTURE'] = ($data_lignefrs[11] == 1)?1:0;
					$dataFrais['ID_LOT'] = $data_lignefrs[12];
					if($data_lignefrs[0] != 0)//Ce frais existe déjà
						$this->db->query_update('FRAISREEL', $dataFrais, 'ID_FRAISREEL="'.$data_lignefrs[0].'"');
					else {//Ce frais est nouveau
						$dataFrais['ID_LISTEFRAIS'] = $idfrais;
						$this->db->query_insert('FRAISREEL', $dataFrais);
					}
				}
			}
		}

		if(isset($data['LISTEFRAISFORFAIT']) && $idfrais > 0) {
			//On supprime les frais au forfait n'existent plus
			foreach($this->db->fetch_all_array('SELECT ID_LIGNEFRAIS FROM TAB_LIGNEFRAIS WHERE ID_LISTEFRAIS="'.$this->db->escape($idfrais).'";') as $lignefrs) {
				$etat = false;
				foreach($data['LISTEFRAISFORFAIT'] as $data_lignefrs) if($data_lignefrs[0] == $lignefrs['ID_LIGNEFRAIS']) {$etat = true;break;}
				if(!$etat) {
					$this->db->query_delete('LIGNEFRAIS', 'ID_LIGNEFRAIS="'.$lignefrs['ID_LIGNEFRAIS'].'"');
					$this->db->query_delete('FRAIS', 'ID_LIGNEFRAIS="'.$lignefrs['ID_LIGNEFRAIS'].'"');
				}
			}

			//On met à jour les frais au forfait
			if(sizeof($data['LISTEFRAISFORFAIT']) > 0) {
				$tabIDs = array();foreach($data['LISTEFRAISFORFAIT'] as $data_lignefrs) if($data_lignefrs[0] != 0) $tabIDs[] = '"'.$this->db->escape($data_lignefrs[0]).'"';
				$listefrs = (sizeof($tabIDs) > 0)?$this->db->fetch_all_array('SELECT ID_FRAIS, ID_LIGNEFRAIS, FRAIS_DATE FROM TAB_FRAIS WHERE ID_LIGNEFRAIS IN('.implode(',', $tabIDs).')'):array();
				foreach($data['LISTEFRAISFORFAIT'] as $data_lignefrs) {
					$dataLigne = array();
					if($data_lignefrs[0] != 0) {//La ligne des frais existent déjà
						//On supprime les frais de la ligne qui n'existe plus
						foreach($listefrs as $frs) if($data_lignefrs[0] == $frs['ID_LIGNEFRAIS']) {
							$etat = false;
							foreach($data_lignefrs[5] as $data_frs) if($data_frs[0] == $frs['FRAIS_DATE']) {$etat = true;break;}
							if(!$etat) $this->db->query_delete('FRAIS', 'ID_FRAIS="'.$frs['ID_FRAIS'].'"');
						}

						//On met à jour les frais de la ligne
						foreach($data_lignefrs[5] as $data_frs) {
							$dataFrais = array();

							$etat = false;
							foreach($listefrs as $frs) if($data_lignefrs[0] == $frs['ID_LIGNEFRAIS'] && $data_frs[0] == $frs['FRAIS_DATE']) {$etat = true;break;}

							if($etat) {
								$dataFrais['FRAIS_VALUE'] = $data_frs[1];
								$this->db->query_update('FRAIS', $dataFrais, 'ID_FRAIS="'.$frs['ID_FRAIS'].'"');
							} else {
								$dataFrais['FRAIS_DATE'] = $data_frs[0];
								$dataFrais['FRAIS_VALUE'] = $data_frs[1];
								$dataFrais['ID_LIGNEFRAIS'] = $data_lignefrs[0];
								$this->db->query_insert('FRAIS', $dataFrais);
							}
						}
						$dataLigne['ID_PROJET'] = $data_lignefrs[1];
						$dataLigne['ID_MISSIONPROJET'] = $data_lignefrs[2];
						$dataLigne['LIGNEFRAIS_TYPEFRSREF'] = $data_lignefrs[3];
						if($data_lignefrs[4] != '') $dataLigne['LIGNEFRAIS_TVA'] = $data_lignefrs[4];//Si TVA est défini, alors on modifie la TVA, sinon alors le TYPE n'existe plus on conserve la dernière valeur enregistrée
						$dataLigne['ID_LOT'] = $data_lignefrs[6];
						$this->db->query_update('LIGNEFRAIS', $dataLigne, 'ID_LIGNEFRAIS="'.$data_lignefrs[0].'"');
					} else {//La ligne des frais est nouvelle, tout la ligne est à créer avec tous les frais
						$dataLigne = array();
						$dataLigne['ID_PROJET'] = $data_lignefrs[1];
						$dataLigne['ID_MISSIONPROJET'] = $data_lignefrs[2];
						$dataLigne['LIGNEFRAIS_TYPEFRSREF'] = $data_lignefrs[3];
						if($data_lignefrs[4] != '') $dataLigne['LIGNEFRAIS_TVA'] = $data_lignefrs[4];//Si TVA est défini, alors on modifie la TVA, sinon alors le TYPE n'existe plus on conserve la dernière valeur enregistrée
						$dataLigne['ID_LOT'] = $data_lignefrs[6];
						$dataLigne['ID_LISTEFRAIS'] = $idfrais;
						$idLigneFrs = $this->db->query_insert('LIGNEFRAIS', $dataLigne);
						foreach($data_lignefrs[5] as $frs) {
							$dataFrs = array();
							$dataFrs['ID_LIGNEFRAIS'] = $idLigneFrs;
							$dataFrs['FRAIS_DATE'] = $frs[0];
							$dataFrs['FRAIS_VALUE'] = $frs[1];
							$this->db->query_insert('FRAIS', $dataFrs);
						}
					}
				}
			}
		}
		return $idfrais;
	}

	/**
	 * Check that an expense exists for a given date
	 * @param string $date (Y-m-d format)
	 * @param int $idProfil Profil ID
	 * @return int Expense ID (0 if it doesn't exist)
	 */
	public function isFraisExist($date, $idProfil) {
		$sql = 'SELECT ID_LISTEFRAIS FROM TAB_LISTEFRAIS WHERE LISTEFRAIS_DATE= :date AND ID_PROFIL= :idProfil';
		$result = $this->singleExec($sql, [ 'date'=>$date, 'idProfil' => $idProfil]);
		if($result) return $result['ID_LISTEFRAIS'];
		else return 0;
	}

	/**
	 * Delete an expense
	 * @param int $idExpense expense ID
	 */
	public function deleteFraisData($idExpense) {

		$result = $this->exec('SELECT ID_LIGNEFRAIS FROM TAB_LIGNEFRAIS WHERE ID_LISTEFRAIS=?', $idExpense);
		foreach($result as $row)
			$this->delete('TAB_FRAIS', 'ID_LIGNEFRAIS=?', $row['ID_LIGNEFRAIS']);

		$this->delete('TAB_LIGNEFRAIS', 'ID_LISTEFRAIS=?', $idExpense);

		$this->delete('TAB_FRAISREEL', 'ID_LISTEFRAIS=?', $idExpense);

		$this->delete('TAB_VALIDATION', 'ID_PARENT = ? AND VAL_TYPE=1', $idExpense);
		$this->delete('TAB_LISTEFRAIS', 'ID_LISTEFRAIS=?' , $idExpense);

		//ON SUPPRIME LES JUSTIFICATIFS
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_JUSTIFICATIVE, $idExpense, 0);
	}

	/**
	 * Retrieve all warning on expenses not related to a project
	 * @param int $idlistefrais Expenes List ID
	 * @return array
	 */
	public function getFrsPrjWarnings($idlistefrais) {
		$warnings = [];
		$sql = 'SELECT 0 AS ID_PROJET, 0 AS ID_MISSIONPROJET, "" AS PRJ_REFERENCE, COUNT(DISTINCT TAB_CONTRAT.ID_CONTRAT) AS NB_MISSION
				FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL)
					LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTEFRAIS.ID_SOCIETE AND FRAISREEL_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
				WHERE TAB_LISTEFRAIS.ID_LISTEFRAIS= :id AND PROFIL_TYPE<>'.Employee::TYPE_EXTERNAL_RESOURCE.' GROUP BY ID_FRAISREEL HAVING NB_MISSION=0
				 UNION ALL
				SELECT 0 AS ID_PROJET, 0 AS ID_MISSIONPROJET, "" AS PRJ_REFERENCE, COUNT(DISTINCT TAB_CONTRAT.ID_CONTRAT) AS NB_MISSION
				FROM TAB_LIGNEFRAIS INNER JOIN TAB_FRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL)
					LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTEFRAIS.ID_SOCIETE AND FRAIS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
				WHERE TAB_LISTEFRAIS.ID_LISTEFRAIS=:id AND PROFIL_TYPE<>'.Employee::TYPE_EXTERNAL_RESOURCE.' GROUP BY ID_FRAIS HAVING NB_MISSION=0
				 UNION ALL
				SELECT TAB_PROJET.ID_PROJET, TAB_FRAISREEL.ID_MISSIONPROJET, PRJ_REFERENCE, COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) AS NB_MISSION
				FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_FRAISREEL.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_FRAISREEL.ID_MISSIONPROJET AND FRAISREEL_DATE BETWEEN MP_DEBUT AND MP_FIN)
				WHERE TAB_LISTEFRAIS.ID_LISTEFRAIS=:id GROUP BY ID_FRAISREEL HAVING NB_MISSION=0
				 UNION ALL
				SELECT TAB_PROJET.ID_PROJET, TAB_LIGNEFRAIS.ID_MISSIONPROJET, PRJ_REFERENCE, COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) AS NB_MISSION
				FROM TAB_LIGNEFRAIS INNER JOIN TAB_FRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_LIGNEFRAIS.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNEFRAIS.ID_MISSIONPROJET AND FRAIS_DATE BETWEEN MP_DEBUT AND MP_FIN)
				WHERE TAB_LISTEFRAIS.ID_LISTEFRAIS=:id GROUP BY ID_FRAIS HAVING NB_MISSION=0;';
		$result = $this->exec($sql, ['id'=>$idlistefrais]);

		$this->data['WARNING_FRS'] = array();
		foreach($result as $i => $row)
			if($row['NB_MISSION'] == 0)
				$warnings[ $row['ID_MISSIONPROJET'] ] = $row;

		return $warnings;
	}

	/**
	 * tell if the expense can be deleted
	 * @param int Expense ID
	 * @return bool
	 */
	function isFraisReducible($id) {
		$sql = 'SELECT COUNT(*) AS NB_DOCUMENT FROM TAB_LISTEFRAIS WHERE ID_LISTEFRAIS = ? AND LISTEFRAIS_CLOTURE = 1';
		$result = $this->singleExec($sql, $id);
		return $result->NB_DOCUMENT == 0;
	}

	/**
	 * Retrieves entity data from its ID
	 * @param  integer  $id  ExpensesReport ID.
	 * @param  integer $tab Subpart of the entity (Tab)
	 * @return \BoondManager\Models\ExpensesReport|false
	 */
	public function getObject($id)
	{
		$sql = 'SELECT ID_LISTEFRAIS, TAB_LISTEFRAIS.ID_PROFIL, TAB_LISTEFRAIS.ID_SOCIETE, LISTEFRAIS_DATE, LISTEFRAIS_ETAT, 
					   LISTEFRAIS_COMMENTAIRES, LISTEFRAIS_DEVISEAGENCE, LISTEFRAIS_CHANGEAGENCE, LISTEFRAIS_AVANCE, 
					   LISTEFRAIS_REGLE, LISTEFRAIS_BKMREF, LISTEFRAIS_BKMVALUE, BKM_NAME, BKM_VALUE, ID_POLE, PROFIL_NOM, PROFIL_PRENOM, PROFIL_TYPE, PROFIL_REFERENCE
					   ID_RESPMANAGER, ID_RESPRH, URH.ID_PROFIL AS ID_PROFIL_RH, UMANAGER.ID_PROFIL AS ID_PROFIL_MANAGER, TAB_USER.USER_TYPE, LISTEFRAIS_CLOTURE
				FROM TAB_LISTEFRAIS 
				INNER JOIN TAB_PROFIL USING(ID_PROFIL) 
				LEFT JOIN TAB_USER ON TAB_USER.ID_PROFIL = TAB_PROFIL.ID_PROFIL 
				LEFT JOIN TAB_USER AS URH ON URH.ID_USER = TAB_PROFIL.ID_RESPRH
				LEFT JOIN TAB_USER AS UMANAGER ON UMANAGER.ID_USER = TAB_PROFIL.ID_RESPMANAGER
				LEFT JOIN TAB_BAREMEKM ON(BKM_REF=LISTEFRAIS_BKMREF AND TAB_BAREMEKM.ID_SOCIETE=TAB_LISTEFRAIS.ID_SOCIETE)
				WHERE ID_LISTEFRAIS = ?';

		$result = $this->singleExec($sql, $id, Models\ExpensesReport::class);

		if(!$result) return false;

		$sql = 'SELECT TAB_FRAISREEL.ID_FRAISREEL, FRAISREEL_TYPEFRSREF, FRAISREEL_INTITULE, FRAISREEL_REFACTURE, 
		               TAB_FRAISREEL.ID_PROJET, TAB_FRAISREEL.ID_MISSIONPROJET, TAB_FRAISREEL.ID_LOT, PRJ_REFERENCE, 
		               MP_NOM, MP_DEBUT, MP_FIN, CSOC_SOCIETE, TAB_PROJET.ID_CRMSOCIETE, TYPEFRS_NAME, TYPEFRS_TVA,
		               FRAISREEL_DATE, FRAISREEL_NUM, FRAISREEL_MONTANT, FRAISREEL_TVA, FRAISREEL_DEVISE, FRAISREEL_CHANGE, LOT_TITRE
		        FROM TAB_FRAISREEL 
		        LEFT JOIN TAB_TYPEFRAIS ON(TYPEFRS_REF=FRAISREEL_TYPEFRSREF AND TAB_TYPEFRAIS.ID_SOCIETE= :agency)
		        LEFT JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET)  
		        LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_FRAISREEL.ID_PROJET)
		        LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		        LEFT JOIN TAB_LOT USING(ID_LOT)
		        WHERE ID_LISTEFRAIS= :id 
		        GROUP BY ID_FRAISREEL 
		        ORDER BY TAB_FRAISREEL.ID_FRAISREEL ASC';
		$result['LISTEFRAISREEL'] = $this->exec($sql, [ 'agency' => $result['ID_SOCIETE'], 'id' => $result['ID_LISTEFRAIS']]);

		//On récupère les frais au forfait
		$sql = 'SELECT TAB_LIGNEFRAIS.ID_LIGNEFRAIS, LIGNEFRAIS_TYPEFRSREF, LIGNEFRAIS_TVA, TAB_LIGNEFRAIS.ID_PROJET, 
		               TAB_LIGNEFRAIS.ID_MISSIONPROJET, TAB_LIGNEFRAIS.ID_LOT, PRJ_REFERENCE, MP_NOM, MP_DEBUT, MP_FIN, CSOC_SOCIETE, 
		               TYPEFRS_NAME, TYPEFRS_TVA, LOT_TITRE
		        FROM TAB_LIGNEFRAIS 
		        LEFT JOIN TAB_TYPEFRAIS ON(TYPEFRS_REF=LIGNEFRAIS_TYPEFRSREF AND TAB_TYPEFRAIS.ID_SOCIETE=:agency)
		        LEFT JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET) 
		        LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_LIGNEFRAIS.ID_PROJET) 
		        LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		        LEFT JOIN TAB_LOT USING(ID_LOT)
		        WHERE ID_LISTEFRAIS=:id
		        GROUP BY ID_LIGNEFRAIS 
		        ORDER BY ID_LIGNEFRAIS ASC;';
		$result['LISTEFRAISFORFAIT'] =  $this->exec($sql, [ 'agency' => $result['ID_SOCIETE'], 'id' => $result['ID_LISTEFRAIS']]);

		foreach($result['LISTEFRAISFORFAIT'] as $i => $ligne)
			$result['LISTEFRAISFORFAIT'][$i]['LISTEJOURS'] = $this->exec('SELECT ID_FRAIS, FRAIS_DATE, FRAIS_VALUE FROM TAB_FRAIS WHERE ID_LIGNEFRAIS=? ORDER BY FRAIS_DATE ASC', $ligne['ID_LIGNEFRAIS']);

		//On récupère tous les justificatifs
		$result['JUSTIFICATIFS'] = $this->exec(
			'SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF WHERE ID_PARENT= :id  AND JUSTIF_TYPE = :type',
			['id'=>$id, 'type'=> Models\Proof::TYPE_EXPENSESREPORT]
		);

		//On récupère toutes les validations
		$result['LISTEVALIDATIONS'] = $this->exec(
			'SELECT ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, TAB_VALIDATION.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT 
			 FROM TAB_VALIDATION 
			 INNER JOIN TAB_PROFIL ON(ID_VALIDATEUR=TAB_PROFIL.ID_PROFIL) 
			 WHERE ID_PARENT= ? AND VAL_TYPE=1 
			 ORDER BY ID_VALIDATION ASC', $id
		);

		$tpsBDD = Time::instance();
		$result['ID_LISTETEMPS'] = $tpsBDD->isTempsExist($result['LISTEFRAIS_DATE'], $result['ID_PROFIL']);
		if(!$result['ID_LISTETEMPS']) $result['ID_LISTETEMPS'] = 0;

		return $result;
	}

	/**
	 * Retrieve all timesheet for a resource
	 * @param int $idProfil Profil ID
	 * @return array
	 */
	public function getAllTemps($idProfil) {
		$sql = 'SELECT ID_LISTETEMPS, LISTETEMPS_DATE, LISTETEMPS_ETAT AS VAL_STATE, LISTETEMPS_CLOTURE, ID_VALIDATION 
				FROM TAB_LISTETEMPS LEFT JOIN TAB_VALIDATION ON(ID_PARENT=ID_LISTETEMPS AND VAL_TYPE=0) 
				WHERE TAB_LISTETEMPS.ID_PROFIL=? GROUP BY ID_LISTETEMPS ORDER BY LISTETEMPS_DATE DESC';
		$result = $this->exec($sql, $idProfil);

		return $result;
	}

	/**
	 * check a timesheet exist at a date for a profil
	 * @param int $date Y-m-d format
	 * @param int $idProfil Profil ID
	 * @return int|false the Timesheet ID
	 */
	public function isTempsExist($date, $idProfil) {
		$result = $this->singleExec('SELECT ID_LISTETEMPS FROM TAB_LISTETEMPS WHERE LISTETEMPS_DATE=:date  AND ID_PROFIL=:profil', ['date'=>$date, 'profil'=>$idProfil]);
		if($result) return $result['ID_LISTETEMPS'];
		return false;
	}

	/**
	 * Retrieve all warnings for time not allocated on a project
	 * @param int $idlistetemps Timesheet ID
	 * @return array with all warnings
	 */
	public function getTpsPrjWarnings($idlistetemps) {
		$warnings = [];

		$sql = 'SELECT 0 AS ID_PROJET, 0 AS ID_MISSIONPROJET, "" AS PRJ_REFERENCE, COUNT(DISTINCT TAB_CONTRAT.ID_CONTRAT) AS NB_MISSION
				FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
					LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND TEMPS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
				WHERE TAB_LISTETEMPS.ID_LISTETEMPS=:id AND PROFIL_TYPE<>'.Models\Employee::TYPE_EXTERNAL_RESOURCE.' GROUP BY ID_TEMPS HAVING NB_MISSION=0
				 UNION ALL
				SELECT TAB_PROJET.ID_PROJET, TAB_LIGNETEMPS.ID_MISSIONPROJET, PRJ_REFERENCE, COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) AS NB_MISSION
				FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_LIGNETEMPS.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET AND TEMPS_DATE BETWEEN MP_DEBUT AND MP_FIN)
				WHERE TAB_LISTETEMPS.ID_LISTETEMPS=:id GROUP BY ID_TEMPS HAVING NB_MISSION=0
				 UNION ALL
				SELECT TAB_PROJET.ID_PROJET, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET, PRJ_REFERENCE, COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) AS NB_MISSION
				FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_TEMPSEXCEPTION.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET AND DATE(TEMPSEXP_DEBUT) BETWEEN MP_DEBUT AND MP_FIN AND DATE(TEMPSEXP_FIN) BETWEEN MP_DEBUT AND MP_FIN)
				WHERE TAB_LISTETEMPS.ID_LISTETEMPS=:id GROUP BY ID_TEMPSEXCEPTION HAVING NB_MISSION=0;';
		$result = $this->exec($sql, ['id'=>$idlistetemps]);


		foreach($result as $row)
			if($row['NB_MISSION'] == 0)
				$warnings[ $row['ID_MISSIONPROJET'] ] = $row;

		return $warnings;
	}

	/**
	 * Set a timesreport
	 * @param array $data an array with the new values group by category (FRAIS, LISTEFRAISREEL, LISTEFRAISFORFAIT)
	 * @param integer $idExpense expensesReport ID
	 * @return int the object id
	 */
	public function setObject($data, $idExpense = 0)
	{
		if(isset($data['FRAIS'])) {
			($idExpense > 0)
				? $this->update('TAB_LISTEFRAIS', $data['FRAIS'], 'ID_LISTEFRAIS = :id', ['id'=>$idExpense])
				: $idExpense = $this->insert('TAB_LISTEFRAIS', $data['FRAIS']);
		}

		if(!$idExpense)
			return false;

		if( isset($data['LISTEFRAISREEL']) ) {
			//On supprime les frais réels qui n'existent plus
			$ids = array_column($data['LISTEFRAISREEL'], 'ID_FRAISREEL');
			if($ids) {
				$this->delete('TAB_FRAISREEL', 'ID_FRAISREEL NOT IN ('.Where::prepareWhereIN($ids).') AND ID_LISTEFRAIS= ?', array_merge($ids, [$idExpense]));
			} else {
				$this->delete('TAB_FRAISREEL', 'ID_LISTEFRAIS = ?', $idExpense);
			}

			//On met à jour les frais réels
			foreach($data['LISTEFRAISREEL'] as $dataFrais) {
				if(isset($dataFrais['ID_FRAISREEL'])) {
					$this->update('TAB_FRAISREEL', $dataFrais, 'ID_FRAISREEL=:idFrais AND ID_LISTEFRAIS=:idLigne', ['idFrais' => $dataFrais['ID_FRAISREEL'], 'idLigne' => $idExpense]);
				} else {//Ce frais est nouveau
					$dataFrais['ID_LISTEFRAIS'] = $idExpense;
					$this->insert('TAB_FRAISREEL', $dataFrais);
				}
			}
		}

		if( isset($data['LISTEFRAISFORFAIT']) ) {
			//On supprime les frais au forfait n'existent plus
			foreach($this->exec('SELECT ID_LIGNEFRAIS FROM TAB_LIGNEFRAIS WHERE ID_LISTEFRAIS=?',$idExpense) as $lignefrs) {
				$found = false;
				foreach($data['LISTEFRAISFORFAIT'] as $data_lignefrs) if($data_lignefrs['ID_LIGNEFRAIS'] == $lignefrs['ID_LIGNEFRAIS']) {$found = true;break;}
				if(!$found) {
					$this->delete('TAB_LIGNEFRAIS', 'ID_LIGNEFRAIS=?', $lignefrs['ID_LIGNEFRAIS']);
					$this->delete('TAB_FRAIS', 'ID_LIGNEFRAIS=?', $lignefrs['ID_LIGNEFRAIS']);
				}
			}

			//On met à jour les frais au forfait
			if($data['LISTEFRAISFORFAIT']) {
				$tabIDs = array_filter( array_column($data['LISTEFRAISFORFAIT'], 'ID_LIGNEFRAIS') );

				$listefrs =
					($tabIDs)
						? $this->exec('SELECT ID_FRAIS, ID_LIGNEFRAIS, FRAIS_DATE FROM TAB_FRAIS WHERE ID_LIGNEFRAIS IN ('.Where::prepareWhereIN($tabIDs).')', $tabIDs)
						: [];

				foreach($data['LISTEFRAISFORFAIT'] as $dataLigne) {
					if($dataLigne['ID_LIGNEFRAIS'] > 0) {//La ligne des frais existent déjà
						//On supprime les frais de la ligne qui n'existe plus
						foreach($listefrs as $frs) if($dataLigne['ID_LIGNEFRAIS'] == $frs['ID_LIGNEFRAIS']) {
							$found = false;
							foreach($dataLigne['LISTEJOURS'] as $dailyExpense)
								if($dailyExpense['FRAIS_DATE'] == $frs['FRAIS_DATE']) {$found = true;break;}
							if(!$found) $this->delete('TAB_FRAIS', 'ID_FRAIS=?', $frs['ID_FRAIS']);
						}

						//On met à jour les frais de la ligne
						foreach($dataLigne['LISTEJOURS'] as $dataFrais) {

							$found = false;
							foreach($listefrs as $frs)
								if($dataLigne['ID_LIGNEFRAIS'] == $frs['ID_LIGNEFRAIS'] && $dataFrais['FRAIS_DATE'] == $frs['FRAIS_DATE']) {
								$found = true;
								$this->update('TAB_FRAIS', $dataFrais, 'ID_LIGNEFRAIS = :idLigne AND ID_FRAIS = :idFrais ', [
									'idLigne' => $dataLigne['ID_LIGNEFRAIS'],
									'idFrais' => $frs['ID_FRAIS']
								]);
								break;
							}

							if(!$found) {
								$dataFrais['ID_LIGNEFRAIS'] = $dataLigne['ID_LIGNEFRAIS'];
								$this->insert('TAB_FRAIS', $dataFrais);
							}
						}
						$this->update('TAB_LIGNEFRAIS', $dataLigne, 'ID_LIGNEFRAIS=:id', ['id'=>$dataLigne['ID_LIGNEFRAIS']]);
					} else {//La ligne des frais est nouvelle, tout la ligne est à créer avec tous les frais
						unset($dataLigne['ID_LIGNEFRAIS']); // si jamais l'id etait négatif (truc de dev front)
						$dataLigne['ID_LISTEFRAIS'] = $idExpense;
						$idLigneFrs = $this->insert('TAB_LIGNEFRAIS', $dataLigne);
						foreach($dataLigne['LISTEJOURS'] as $dataFrs) {
							$dataFrs['ID_LIGNEFRAIS'] = $idLigneFrs;
							$this->insert('TAB_FRAIS', $dataFrs);
						}
					}
				}
			}
		}

		return $idExpense ? $idExpense:false;
	}

	/**
	 * Deletes an expensesreport
	 * @param int $id expensesreport ID
	 * @return bool
	 */
	public function deleteObject($id)
	{
		foreach($this->exec('SELECT ID_LIGNEFRAIS FROM TAB_LIGNEFRAIS WHERE ID_LISTEFRAIS=?', $id) as $expensesEntry)
			$this->delete('TAB_FRAIS', 'ID_LIGNEFRAIS=?', $expensesEntry['ID_LIGNEFRAIS']);

		$this->delete('TAB_LIGNEFRAIS', 'ID_LISTEFRAIS=?', $id);

		$this->delete('TAB_VALIDATION', 'ID_PARENT=? AND VAL_TYPE=0', $id);
		$this->delete('TAB_LISTEFRAIS', 'ID_LISTEFRAIS=?', $id);
		$this->delete('TAB_FRAISREEL', 'ID_LISTEFRAIS=?', $id);

		//ON SUPPRIME LES JUSTIFICATIFS
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_JUSTIFICATIVE, $id, Models\Proof::TYPE_EXPENSESREPORT);
		return true;
	}

	/**
	 * is a given ExpensesReport deletable
	 * @param int $id
	 * @return bool
	 */
	function canDeleteObject($id) {
		$sql = 'SELECT COUNT(*) AS NB_DOCUMENT FROM TAB_LISTEFRAIS WHERE ID_LISTEFRAIS = ? AND LISTEFRAIS_CLOTURE=1';
		$result = $this->singleExec($sql, $id);
		return $result['NB_DOCUMENT'] == 0;
	}
}
