<?php
/**
 * file.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\Lib\GED;
use BoondManager\Models;
use Wish\MySQL\Query;
use Wish\Models\Model;
use BoondManager\OldModels\Filters;

/**
 * handle all database work related to files
 * @namespace \BoondManager\Models\ObjectBDD\Fiches
 */
class File extends AbstractObject {

	/**#@+
	* @var int TYPE
	*/
	const
		TYPE_RESOURCE_RESUME = 0,
		TYPE_DOCUMENT = 1,
		TYPE_JUSTIFICATIVE = 2, // anciennement justificatif
		TYPE_CANDIDATE_RESUME = 3, // on est obligé de distingué les types de résumé (pour le moment) car ils sont stockés dans des dossiers différents
		TYPE_OTHER = 4,
		TYPE_DOWNLOADCENTER = 5;
	/**#@-*/

    /**
     * GED instance
     * @var GED
     */
    protected $GED;

    /**
     * Constructor
     * @param GED|null GED Instance
     */
    public function __construct(GED $GED = null) {
        parent::__construct();
        $this->GED = (isset($GED))?$GED:GED::instance();
    }

	/**
	* Retrieve all data of a file
	* @TODO nettoyer ces types (passages en constantes)
	* @param  integer  $typeFile Document TYPE:
	* - `0|3` : [TAB_CV](../../bddclient/classes/TAB_CV.html).
	* - `1` : [TAB_DOCUMENT](../../bddclient/classes/TAB_DOCUMENT.html).
	* - `2` : [TAB_JUSTIFICATIF](../../bddclient/classes/TAB_JUSTIFICATIF.html).
	* - `4` : [TAB_FILE](../../bddclient/classes/TAB_FILE.html).
	* @param  integer  $id Document ID
	* @return Model|false
	*/
	public function getObject($typeFile, $id) {
		switch($typeFile) {
			case self::TYPE_CANDIDATE_RESUME:
			case self::TYPE_RESOURCE_RESUME:
				$sql = 'SELECT ID_CV, CV_NAME, CV_TEXT, TAB_CV.ID_PROFIL AS ID_PARENT, TAB_PROFIL.ID_PROFIL, PROFIL_TYPE, PROFIL_NOM, PROFIL_PRENOM 
				        FROM TAB_CV 
				        LEFT JOIN TAB_PROFIL ON TAB_PROFIL.ID_PROFIL = TAB_CV.ID_PROFIL
				        WHERE ID_CV=?';
				break;
			case self::TYPE_JUSTIFICATIVE:
				$sql = 'SELECT ID_JUSTIFICATIF, FILE_NAME, JUSTIF_TYPE AS FILE_TYPE, ID_PARENT FROM TAB_JUSTIFICATIF WHERE ID_JUSTIFICATIF=?';
				break;
			case self::TYPE_DOCUMENT:
				$sql = 'SELECT ID_DOCUMENT, FILE_NAME, DOC_TYPE AS FILE_TYPE, ID_PARENT FROM TAB_DOCUMENT WHERE ID_DOCUMENT=?';
				break;
			default:
				$sql = 'SELECT ID_FILE, FILE_NAME, ID_PROFIL AS ID_PARENT ,
				               TAB_PROFIL.ID_PROFIL, PROFIL_TYPE, PROFIL_NOM, PROFIL_PRENOM
				        FROM TAB_FILE 
				        LEFT JOIN TAB_PROFIL ON TAB_PROFIL.ID_PROFIL = TAB_FILE.ID_PROFIL
				        WHERE ID_FILE=?';
				break;
		}
		return $this->singleExec($sql, $id);
	}

	/**
	 * Create/update a document using its parent
	 * @param  integer  $typeFile  Catégorie de document :
	 * - `0|3` : [TAB_CV](../../bddclient/classes/TAB_CV.html).
	 * - `1` : [TAB_DOCUMENT](../../bddclient/classes/TAB_DOCUMENT.html).
	 * - `2` : [TAB_JUSTIFICATIF](../../bddclient/classes/TAB_JUSTIFICATIF.html).
	 * - `4` : [TAB_FILE](../../bddclient/classes/TAB_FILE.html).
	 * @param  string  $filename file path.
	 * @param  string  $name file name.
	 * @param  string  $text Filtered document content, only if `$typeFile = 0|3`.
	 * @param  int  $id file id.
	 * @param  boolean $copy should we copy the file or just move it.
	 * @param  integer  $subType type of the parent , only if `$typeFile = 2|1`.
	 * @TODO utiliser des constantes
	 * @return integer Identifiant de l'utilisateur.
	 */
	public function setObjectFromParent($typeFile, $filename = '', $name = '', $text = '', $id = 0, $copy = false, $subType = null)
	{
		//DOCTYPE
		//0 = TAB_CRMSOCIETE
		//1 = TAB_AO
		//2 = TAB_PROJET
		//3 = TAB_BONDECOMMANDE
		//4 = TAB_PRODUIT
		//5 = TAB_ACHAT
		//6 = TAB_ACTION
		//9 = TAB_POSITIONNEMENT
		$data = [];
		switch ($typeFile) {
			case self::TYPE_CANDIDATE_RESUME:
			case self::TYPE_RESOURCE_RESUME:
				$data['CV_NAME']   = $name;
				$data['CV_TEXT']   = $text;
				$data['ID_PROFIL'] = $id;
				$table             = 'TAB_CV';
				break;
			case self::TYPE_JUSTIFICATIVE:
				$data['FILE_NAME']   = $name;
				$data['JUSTIF_TYPE'] = isset($subType) ? $subType : Models\Proof::TYPE_TIMESREPORT;
				$data['ID_PARENT']   = $id;
				$table               = 'TAB_JUSTIFICATIF';
				break;
			case self::TYPE_DOCUMENT:
				$data['FILE_NAME'] = $name;
				$data['DOC_TYPE']  = isset($subType) ? $subType : Models\Document::TYPE_COMPANY;
				$data['ID_PARENT'] = $id;
				$table             = 'TAB_DOCUMENT';
				break;
			default:
				$data['FILE_NAME'] = $name;
				$data['ID_PROFIL'] = $id;
				$table             = 'TAB_FILE';
				break;
		}
		$idfile = $this->insert($table, $data);
		return ($this->GED->uploadFile($typeFile, $idfile, $filename, $copy)) ? $idfile : false;
	}

	/**
	 * Delete a file
	 * @param integer $typeFile Catégorie de document :
	 * - `0|3` : [TAB_CV](../../bddclient/classes/TAB_CV.html).
	 * - `1` : [TAB_DOCUMENT](../../bddclient/classes/TAB_DOCUMENT.html).
	 * - `2` : [TAB_JUSTIFICATIF](../../bddclient/classes/TAB_JUSTIFICATIF.html).
	 * - `4` : [TAB_FILE](../../bddclient/classes/TAB_FILE.html).
	 * @param integer $id File ID.
	 * @return boolean
	 */
	public function deleteObject($typeFile, $id = 0)
	{
		switch ($typeFile) {
			case self::TYPE_CANDIDATE_RESUME:
			case self::TYPE_RESOURCE_RESUME:
				$this->delete('TAB_CV', 'ID_CV=?', $id);
				break;
			case self::TYPE_JUSTIFICATIVE:
				$this->delete('TAB_JUSTIFICATIF', 'ID_JUSTIFICATIF=?', $id);
				break;
			case self::TYPE_DOCUMENT:
				$this->delete('TAB_DOCUMENT', 'ID_DOCUMENT=?', $id);
				break;
			default:
				$this->delete('TAB_FILE', 'ID_FILE=?', $id);
		}
		$this->GED->deleteFile($typeFile, $id);
		return true;
	}

	/**
	* Delete all file for a parent
	* @param  integer  $typeFile Document type
	* - `0|3` : [TAB_CV](../../bddclient/classes/TAB_CV.html).
	* - `1` : [TAB_DOCUMENT](../../bddclient/classes/TAB_DOCUMENT.html).
	* - `2` : [TAB_JUSTIFICATIF](../../bddclient/classes/TAB_JUSTIFICATIF.html).
	* - `4` : [TAB_FILE](../../bddclient/classes/TAB_FILE.html).
	* @param  integer  $id  parent id
	* @param  integer  $subType parent type, _only if `$typeFile = 2|1`_.
	* @return boolean
	*/
	public function deleteAllObjects($typeFile, $id = 0, $subType = null) {
		switch($typeFile) {
			case self::TYPE_CANDIDATE_RESUME:
			case self::TYPE_RESOURCE_RESUME:
				$idColumn = 'ID_CV';
				$table = 'TAB_CV';
				$where = 'ID_PROFIL=:id';
				$args = [':id' => $id];
				break;
			case self::TYPE_JUSTIFICATIVE:
				$idColumn = 'ID_JUSTIFICATIF';
				$table = 'TAB_JUSTIFICATIF';
				$where = 'ID_PARENT=:id AND JUSTIF_TYPE=:type';
				$args = [':id' => $id, ':type' => isset($subType) ? $subType : Models\Proof::TYPE_TIMESREPORT];
				break;
			case self::TYPE_DOCUMENT:
				$idColumn = 'ID_DOCUMENT';
				$table = 'TAB_DOCUMENT';
				$where = 'ID_PARENT=:id AND DOC_TYPE=:type';
				$args = [':id' => $id, ':type' => isset($subType) ? $subType : Models\Document::TYPE_COMPANY];
				break;
			default:
				$idColumn = 'ID_FILE';
				$table = 'TAB_FILE';
				$where = 'ID_PROFIL=:id';
				$args = [':id' => $id];
				break;
		}
		//On récupère tous les fichiers appartenant à cette fiche
		foreach($this->exec('SELECT '.$idColumn. ' FROM '.$table.' WHERE '.$where, $args) as $file)
			$this->GED->deleteFile($typeFile, $file[$idColumn]);//On les supprime

		//On supprime les items de la table
		$this->delete($table, $where, $args);
		return true;
	}

	public function search(Filters\Search\Documents $filter) {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		switch($filter->category->getValue()) {
			case self::TYPE_CANDIDATE_RESUME:
			case self::TYPE_RESOURCE_RESUME:
				$query->select(['ID_CV', 'CV_NAME']);
				$query->from('TAB_CV');
				$query->addWhere('ID_PROFIL=?', $filter->parent->getValue());
				$query->setModelClass(\BoondManager\Models\Resume::class);
				break;
			case self::TYPE_JUSTIFICATIVE:
				$query->select(['ID_JUSTIFICATIF', 'FILE_NAME', 'FILE_TYPE'=>'JUSTIF_TYPE']);
				$query->from('TAB_JUSTIFICATIF');
				$query->addWhere('ID_PARENT=? AND_JUSTIF_TYPE=?', [$filter->parent->getValue(), $filter->type->getValue()]);
				$query->setModelClass(\BoondManager\Models\Proof::class);
				break;
			case self::TYPE_DOCUMENT:
				$query->select(['ID_DOCUMENT', 'FILE_NAME', 'FILE_TYPE'=>'DOC_TYPE']);
				$query->from('TAB_DOCUMENT');
				$query->addWhere('ID_PARENT=? AND DOC_TYPE=?', [$filter->parent->getValue(), $filter->type->getValue()]);
				$query->setModelClass(\BoondManager\Models\Document::class);
				break;
			default:
				$query->select(['ID_FILE', 'FILE_NAME']);
				$query->from('TAB_FILE');
				$query->addWhere('ID_PROFIL=?', $filter->parent->getValue());
				$query->setModelClass(\BoondManager\Models\File::class);
				break;
		}

		$query->setLimit(  $filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		return $this->launchSearch($query);
	}
}
