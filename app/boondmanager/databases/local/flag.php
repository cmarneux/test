<?php
/**
 * flag.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Flags\Filters\SearchFlags;
use Wish\Models\Model;
use Wish\Cache;
use Wish\Models\SearchResult;
use Wish\MySQL\Query;
use Wish\MySQL\Where;

/**
 * Handle the database work related to Flags
 * @@package BoondManager\Databases\Local;
 */
class Flag extends AbstractObject {
	/**
	 * Search flags
	 * @param SearchFlags $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchFlags(SearchFlags $filter) {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('TAB_USERFLAG.ID_USERFLAG, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, TAB_USER.ID_USER, FLAG_LIBELLE');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'TAB_PROFIL.ID_SOCIETE',
			$colPole = 'TAB_PROFIL.ID_POLE',
			$colUser = 'TAB_PROFIL.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$query->from('TAB_USERFLAG');
		$query->addJoin('INNER JOIN TAB_USER USING(ID_USER)
							INNER JOIN TAB_PROFIL USING(ID_PROFIL)');
		$query->groupBy('ID_USERFLAG');

		$keywordsMapping = ['FLAG'=>'ID_USERFLAG'];
        $whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);
		if(!$whereKeywords->isEmpty())
        	$where->and_($whereKeywords);

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

		$query->addWhere($where);

		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchFlags::ORDERBY_NAME            		=> 'FLAG_LIBELLE',
			SearchFlags::ORDERBY_MAINMANAGER_LASTNAME	=> 'PROFIL_NOM'
		];

		if(!$column) $query->addOrderBy('FLAG_LIBELLE ASC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping)) {
				$query->addOrderBy($mapping[$c] . ' ' . $order);
				if($c == SearchFlags::ORDERBY_MAINMANAGER_LASTNAME)
					$query->addOrderBy('FLAG_LIBELLE ASC');
			}

		$query->addOrderBy('ID_USERFLAG DESC');
	}

	/**
	 * Get a list of users flags from a list of user's id
	 * @param array|int $tabIds employee's id
	 * @return Model[]
	 */
	function getFlags($tabIds) {
		if(!is_array($tabIds)) $tabIds = [$tabIds];
		if(!$tabIds) return [];

		$cache = Cache::instance();
		$key = 'flags.profil.list.'.implode('_',$tabIds);
		if(!$cache->exists($key)) {
			$query = new Query();
			$query->select('ID_USERFLAG, FLAG_LIBELLE, TAB_USERFLAG.ID_USER, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM')
				->from('TAB_USERFLAG INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL)')
				->addWhere('TAB_USER.ID_USER IN (?)', $tabIds)
				->addOrderBy('TAB_USERFLAG.ID_USER ASC, TAB_USERFLAG.ID_USERFLAG DESC');
			$data = $this->exec($query);
			$cache->set($key, $data);
		}
		return $cache->get($key);
	}

	/**
	 * Get a list of users flags from a list of employee's id
	 * @param array $tabIds employee's id
	 * @return Model[]
	 */
	function getFlagsFromProfilIds($tabIds) {
		if(!is_array($tabIds)) $tabIds = [$tabIds];
		if(!$tabIds) return [];

		$cache = Cache::instance();
		$key = 'flags.profil.list.'.implode('_',$tabIds);
		if(!$cache->exists($key)) {
			$query = new Query();
			$query->select('ID_USERFLAG, FLAG_LIBELLE, TAB_USERFLAG.ID_USER, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM')
				->from('TAB_USERFLAG INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL)')
				->addWhere('TAB_USER.ID_PROFIL IN (?)', $tabIds)
				->addOrderBy('TAB_USERFLAG.ID_USER ASC, TAB_USERFLAG.ID_USERFLAG DESC');
			$data = $this->exec($query);
			$cache->set($key, $data);
		}
		return $cache->get($key);
	}

    /**
     * Create a user's flag
     * @param  array  $data
     * This array can contains the following keys:
     * - `USERFLAG` : all flags data, cf. [TAB_USERFLAG](../../bddclient/classes/TAB_USERFLAG.html)
     * @return integer flag id
     */
    public function createFlag($data) {
        if(isset($data['USERFLAG']))
            return $this->insert('TAB_USERFLAG', $data['USERFLAG']);
        return false;
    }

	/**
	 * Update a user's flag
	 * @param  integer  $id  flag id to update, cf. [TAB_USERFLAG.ID_USERFLAG](../../bddclient/classes/TAB_USERFLAG.html#property_ID_USERFLAG).
	 * @param  array  $data
	 * This array can contains the following keys:
	 * - `USERFLAG` : all flags data, cf. [TAB_USERFLAG](../../bddclient/classes/TAB_USERFLAG.html)
	 * @return bool
	 */
	public function updateFlag($id, $data) {
		if(isset($data['USERFLAG']))
			$this->update('TAB_USERFLAG', $data['USERFLAG'], 'ID_USERFLAG=:id', array(':id' => $id));
		return true;
	}

    /**
     * Get a user's flag
     * @param  integer  $id
     * @return Model
     */
    public function getFlag($id) {
        return $this->singleExec('SELECT ID_USERFLAG, ID_USER, FLAG_LIBELLE, TAB_PROFIL.ID_PROFIL, TAB_PROFIL.ID_SOCIETE, PROFIL_NOM, PROFIL_PRENOM
                                  FROM TAB_USERFLAG INNER JOIN TAB_USER USING(ID_USER) INNER JOIN TAB_PROFIL USING(ID_PROFIL)
                                  WHERE ID_USERFLAG=?', $id);
    }

    /**
     * Delete a user's flag
     * @param  integer  $id
     * @return int
     */
    public function deleteFlag($id) {
        $this->delete('TAB_FLAG', 'ID_USERFLAG=?', $id);
        $this->delete('TAB_USERFLAG', 'ID_USERFLAG=?', $id);
        return true;
    }

	/**
	 * indicate if the flag can be deleted
	 * @return boolean
	 */
	function isFlagReducible($id) {
		$nbDocument = 0;
		$sql = 'SELECT COUNT(ID_FLAG) AS NB_DOCUMENT FROM TAB_FLAG WHERE ID_USERFLAG=:id';
		foreach($this->exec($sql, [':id'=>$id]) as $document)
			$nbDocument += $document['NB_DOCUMENT'];
		if($nbDocument == 0) return true; else return false;
	}
}
