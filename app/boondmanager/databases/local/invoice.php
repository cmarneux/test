<?php
/**
 * invoice.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Databases\Local;

use BoondManager\APIs\Invoices\Filters\SearchInvoices;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Services\BM;
use BoondManager\Models;

/**
 * Handle all database work related to invoices
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Invoice extends AbstractObject {
	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchInvoices $filter
	 * @return SearchResult
	 */
	public function searchInvoices(SearchInvoices $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('TAB_BONDECOMMANDE.ID_BONDECOMMANDE, TAB_ECHEANCIER.ID_ECHEANCIER, ECH_DATE, ECH_DESCRIPTION, PRJ.ID_PROJET,
		PRJ.ID_SOCIETE, PRJ.ID_POLE, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF, ID_AO, AO_TITLE, TAB_USER.ID_PROFIL, BDC_DATE, BDC_REF, BDC_REFCLIENT,
		BDC_ACCORDCLIENT, ID_RIB, BDC_MONTANTHT AS TOTAL_MONTANTBDC, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE,
		TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, TAB_CRMSOCIETE.ID_CRMSOCIETE, PROFIL_NOM, PROFIL_PRENOM,
		SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)) AS TOTAL_CAFACTUREHT,
		SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)*(1+ITEM_TAUXTVA/100)) AS TOTAL_CAFACTURETTC,
		FACT.ID_FACTURATION, FACT_TYPEPAYMENT, FACT_AVOIR, FACT_REF, FACT_DATE, FACT_DATEREGLEMENTATTENDUE,
		FACT_DATEREGLEMENTRECUE, FACT_ETAT, FACT_DEBUT, FACT_FIN, FACT_DEVISEAGENCE, FACT_CHANGEAGENCE, FACT_CHANGE, FACT_DEVISE, FACT_CLOTURE');

		$query->from('TAB_FACTURATION FACT');

		$query->addJoin('INNER JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE)
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=TAB_BONDECOMMANDE.ID_PROJET)
			LEFT JOIN TAB_AO USING(ID_AO)
			LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=PRJ.ID_PROFIL)
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
			LEFT JOIN TAB_ITEMFACTURE ON(TAB_ITEMFACTURE.ID_FACTURATION=FACT.ID_FACTURATION)
			LEFT JOIN TAB_ECHEANCIER USING(ID_ECHEANCIER)');

		$query->groupBy('FACT.ID_FACTURATION');

		$where = new Where('PRJ.PRJ_TYPE>0');
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'PRJ.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['FACT'=>'FACT.ID_FACTURATION', 'BDC' => 'TAB_BONDECOMMANDE.ID_BONDECOMMANDE', 'PRJ'=>'PRJ.ID_PROJET' ,
		'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT' ,'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('BDC_REF LIKE ?', $likeSearch)
						  ->or_('BDC_REFCLIENT LIKE ?', $likeSearch)
						  ->or_('FACT_REF LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		if($filter->creditNote->isDefined()) $where->and_( 'FACT_AVOIR = ?', $filter->creditNote->getValue());

		if($filter->closing->isDefined()) $where->and_( 'FACT_CLOTURE = ?', $filter->closing->getValue());

		$where->and_( $this->getFilterSearch($filter->states->getValue(), 'FACT_ETAT') )
				->and_( $this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF') )
				->and_( $this->getFilterSearch($filter->paymentMethods->getValue(), 'FACT_TYPEPAYMENT') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_BILLING.' AND TAB_FLAG.ID_PARENT=FACT.ID_FACTURATION)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		switch ($filter->period->getValue()) {
			case SearchInvoices::PERIOD_CREATED:
				$query->addWhere('FACT_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			case SearchInvoices::PERIOD_EXPECTEDPAYMENT:
				$query->addWhere('FACT_DATEREGLEMENTATTENDUE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			case SearchInvoices::PERIOD_PERFORMEDPAYMENT:
				$query->addWhere('FACT_DATEREGLEMENTRECUE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			case SearchInvoices::PERIOD_PERIOD:
				$query->addWhere('DATEDIFF(FACT_FIN,?)>=0 && DATEDIFF(?,FACT_DEBUT)>=0', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			default:case BM::INDIFFERENT:break;
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		/** @var Models\SearchResults\Invoices $result */
		$result = $this->launchSearch($query, Models\SearchResults\Invoices::class);

		$result->turnoverExcludingTax = $result->turnoverIncludingTax  = 0;
		$states =  $filter->states->getListOfValidValues();
		foreach($states as $state){
			$result->setStateAmount($state, 0, 0);
		}
		if($result->total > 0){
			foreach($states as $state){
				$select_sum_by_state[] = 'SUM(CASE WHEN FACT_ETAT="'.$state.'" THEN ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100) ELSE 0 END) 
				AS TOTAL_CAFACTUREHT_ETAT'.$state.', SUM(CASE WHEN FACT_ETAT="'.$state.'" 
				THEN ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)*(1+ITEM_TAUXTVA/100) ELSE 0 END) AS TOTAL_CAFACTURETTC_ETAT'.$state;
			}
/*
			$sum_by_state = $this->singleExec('SELECT SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)) AS TOTAL_CAFACTUREHT,
			SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)*(1+ITEM_TAUXTVA/100)) AS TOTAL_CAFACTURETTC
			'.((sizeof($select_sum_by_state) > 0)?', '.implode(',',$select_sum_by_state):'').' FROM '.$query->getFrom().' '. $query->buildJoins().
			' '.$query->buildWhere(), $query->getArgs());
*/

			$sumQuery = new Query();
			$sumQuery->select(
				'SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)) AS TOTAL_CAFACTUREHT,
				SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100)*(1+ITEM_TAUXTVA/100)) AS TOTAL_CAFACTURETTC'
			);
			$sumQuery->addColumns($select_sum_by_state);
			$sumQuery->from( $query->getFrom() )->addJoin( $query->buildJoins() )->addWhere($query->getWhere() );
			$sum_by_state = $this->singleExec($sumQuery);

			if($sum_by_state){
				foreach($states as $state){
					$result->setStateAmount($state, $sum_by_state['TOTAL_CAFACTUREHT_ETAT'. $state], $sum_by_state['TOTAL_CAFACTURETTC_ETAT'.$state]);
				}
				$result->turnoverExcludingTax  += $sum_by_state->TOTAL_CAFACTUREHT;
				$result->turnoverIncludingTax  += $sum_by_state->TOTAL_CAFACTURETTC;
			}
		}
		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchInvoices::ORDERBY_TURNOVERINCLUDEDTAX => 'TOTAL_CAFACTUREHT',
			SearchInvoices::ORDERBY_TURNOVEREXCLUDEDTAX => 'TOTAL_CAFACTURETTC',
			SearchInvoices::ORDERBY_CREATIONDATE => 'FACT_DATE',
			SearchInvoices::ORDERBY_EXPECTEDPAYMENTDATE=> 'FACT_DATEREGLEMENTATTENDUE',
			SearchInvoices::ORDERBY_STATE => 'FACT_ETAT',
			SearchInvoices::ORDERBY_REFERENCE=> 'FACT_REF',
			SearchInvoices::ORDERBY_ORDER_REFERENCE=> 'BDC_REF',
			SearchInvoices::ORDERBY_ORDER_PROJECT_REFERENCE=> 'PRJ_REFERENCE',
			SearchInvoices::ORDERBY_ORDER_PROJECT_COMPANY_NAME=> 'CSOC_SOCIETE',
			SearchInvoices::ORDERBY_MAINMANAGER_LASTNAME=> 'PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy('FACT.FACT_DATE DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('FACT.ID_FACTURATION DESC');
	}

	/**
	 * Delete all data related to the given bill
	 * @param integer $id
	 */
	public function deleteInvoice($id) {
		//On supprime les actions associés à cette facture
		$dict = Actions::getDictionaryForCategory(BM::CATEGORY_BILLING);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id'));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//On supprime tous les flags de cette action
		$dbFlag = new AttachedFlag();
		$dbFlag->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_INVOICE);

		$this->delete('TAB_ITEMFACTURE', 'ID_FACTURATION=?', $id);
		$this->delete('TAB_FACTURATION', 'ID_FACTURATION=?', $id);
	}

	/**
	 * \brief Récupère la liste des intervenants ayant travaillés sur la période comprenant la date passée en paramètres
	 * @param int $projectId identifiant du projet
	 * @param string $startDate date de début de la période
	 * @param string $endDate date de fin de la période
	 * @param array $correlations tableau contenant la liste des commandes/missions corrélés afin de ne traiter que celles-là
	 * @param boolean $withException
	 * @return array tableau des intervenants de la facture
	 */
	public function getIntervenantsDataOnPeriod($projectId, $startDate, $endDate, $correlations = array(), $withException = false)
	{
		$result = ['LISTE_TEMPSNORMAUX' => [], 'LISTE_TEMPSEXCEPTION' => []];
		$deliveries = [];
		if(sizeof($correlations) > 0) {
			foreach($correlations as $correlation) if($correlation['CORBDC_TYPE'] == 0) $deliveries[] = $correlation['ID_ITEM'];
			if(sizeof($deliveries) > 0)
				$whereExpr = ' AND TAB_MISSIONPROJET.ID_MISSIONPROJET IN('.Where::prepareNamedWhereIN('delivery',$deliveries).')';
			else return $result;
		} else $whereExpr = '';

		$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_CJM, MP_DEBUT, MP_FIN, SUM(TEMPS_DUREE) AS NB_JRSOUVRES, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
				FROM TAB_TEMPS INNER JOIN TAB_LIGNETEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
					INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
					INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
					LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
				WHERE TEMPS_DATE BETWEEN :startDate AND :endDate AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE
				 UNION ALL
				SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_CJM, MP_DEBUT, MP_FIN, SUM(TEMPS_DUREE) AS NB_JRSOUVRES, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
				FROM TAB_TEMPS INNER JOIN TAB_LIGNETEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
					INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_LIGNETEMPS.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
					INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
					LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
				WHERE TEMPS_DATE BETWEEN :startDate AND :endDate AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, LIGNETEMPS_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE;';
		$result['LISTE_TEMPSNORMAUX'] = $this->exec($sql, array_merge(
			['projectId' => $projectId,'startDate' => $startDate,'endDate' => $endDate],
			Where::prepareNamedWhereINArgs('delivery', $deliveries)
		));

		if($withException) {
			$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, SUM(TEMPSEXP_TARIF) AS MP_TARIF, SUM(TEMPSEXP_COUT) AS MP_CJM, TEMPSEXP_DEBUT, TEMPSEXP_FIN, SUM(IF(TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1,TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN))) AS TEMPS_DUREE, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
					FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
						INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
						INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
						LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
					WHERE DATEDIFF(:endDate,TEMPSEXP_DEBUT)>=0 AND DATEDIFF(TEMPSEXP_FIN,:startDate)>=0 AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE
					 UNION ALL
					SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, SUM(TEMPSEXP_ACHAT) AS MP_TARIF, SUM(TEMPSEXP_COUT) AS MP_CJM, TEMPSEXP_DEBUT, TEMPSEXP_FIN, SUM(IF(TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1,TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN))) AS TEMPS_DUREE, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE
					FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS)
						INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTETEMPS.ID_PROFIL AND ITEM_TYPE=0)
						INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
						LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
					WHERE DATEDIFF(:endDate,TEMPSEXP_DEBUT)>=0 AND DATEDIFF(TEMPSEXP_FIN,:startDate)>=0 AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.' GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, TEMPSEXP_TYPEHREF, TAB_LISTETEMPS.ID_SOCIETE;';
			$result['LISTE_TEMPSEXCEPTION'] = $this->exec($sql, array_merge(
				['projectId' => $projectId,'startDate' => $startDate,'endDate' => $endDate],
				Where::prepareNamedWhereINArgs('delivery', $deliveries)
			));
		}

		return $result;
	}

	/**
	 * \brief Récupère la liste des frais à refacturer au client
	 * @param <type> $idprojet identifiant du projet
	 * @param <type> $debut date de début de la période
	 * @param <type> $fin date de fin de la période
	 * @return <type> tableau des frais à refacturer
	 */
	public function getFraisDataOnPeriod($projectId, $startDate, $endDate, $correlations = array(), $rebilledOnly = true)
	{
		$result = ['LISTE_FRAISREEL' => [],'LISTE_FRAISFORFAIT' => []];
		$deliveries = [];
		if(sizeof($correlations) > 0) {
			foreach($correlations as $correlation) if($correlation['CORBDC_TYPE'] == 0) $deliveries[] = $correlation['ID_ITEM'];
			if(sizeof($deliveries) > 0)
				$whereExpr = ' AND TAB_MISSIONPROJET.ID_MISSIONPROJET IN('.Where::prepareNamedWhereIN('delivery',$deliveries).')';
			else return $result;
		} else $whereExpr = '';

		if($rebilledOnly) $whereExpr .= ' AND FRAISREEL_REFACTURE=1';
		$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_CJM, MP_DEBUT, MP_FIN, FRAISREEL_TYPEFRSREF, FRAISREEL_INTITULE, FRAISREEL_TVA, FRAISREEL_MONTANT, FRAISREEL_NUM, LISTEFRAIS_BKMVALUE, LISTEFRAIS_CHANGEAGENCE, TAB_LISTEFRAIS.ID_SOCIETE FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
					 INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_FRAISREEL.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTEFRAIS.ID_PROFIL AND ITEM_TYPE=0)
					 INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL)
				WHERE FRAISREEL_DATE BETWEEN :startDate AND :endDate AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.'
				 UNION ALL
				SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_CJM, MP_DEBUT, MP_FIN, FRAISREEL_TYPEFRSREF, FRAISREEL_INTITULE, FRAISREEL_TVA, FRAISREEL_MONTANT, FRAISREEL_NUM, LISTEFRAIS_BKMVALUE, LISTEFRAIS_CHANGEAGENCE, TAB_LISTEFRAIS.ID_SOCIETE FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
					 INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_FRAISREEL.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTEFRAIS.ID_PROFIL AND ITEM_TYPE=0)
					 INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL)
				WHERE FRAISREEL_DATE BETWEEN :startDate AND :endDate AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.';';
		$result['LISTE_FRAISREEL'] = $this->exec($sql,array_merge(
			['projectId' => $projectId,'startDate' => $startDate,'endDate' => $endDate],
			Where::prepareNamedWhereINArgs('delivery', $deliveries)
		));
		if(!$rebilledOnly) {
			$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_CJM, MP_DEBUT, MP_FIN, LIGNEFRAIS_TYPEFRSREF, LIGNEFRAIS_TVA, FRAIS_VALUE, FRAIS_DATE, LISTEFRAIS_BKMVALUE, LISTEFRAIS_CHANGEAGENCE, TAB_LISTEFRAIS.ID_SOCIETE FROM TAB_FRAIS INNER JOIN TAB_LIGNEFRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
						 INNER JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNEFRAIS.ID_MISSIONPROJET AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTEFRAIS.ID_PROFIL AND ITEM_TYPE=0)
						 INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL)
					WHERE FRAIS_DATE BETWEEN :startDate AND :endDate AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.'
					 UNION ALL
					SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, MP_NOM, MP_TARIF, MP_CJM, MP_DEBUT, MP_FIN, LIGNEFRAIS_TYPEFRSREF, LIGNEFRAIS_TVA, FRAIS_VALUE, FRAIS_DATE, LISTEFRAIS_BKMVALUE, LISTEFRAIS_CHANGEAGENCE, TAB_LISTEFRAIS.ID_SOCIETE FROM TAB_FRAIS INNER JOIN TAB_LIGNEFRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
						 INNER JOIN TAB_MISSIONPROJET ON(ID_MASTER=TAB_LIGNEFRAIS.ID_MISSIONPROJET AND ID_MASTER<>0 AND TAB_MISSIONPROJET.ID_ITEM=TAB_LISTEFRAIS.ID_PROFIL AND ITEM_TYPE=0)
						 INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTEFRAIS.ID_PROFIL)
					WHERE FRAIS_DATE BETWEEN :startDate AND :endDate AND TAB_MISSIONPROJET.ID_PROJET=:projectId'.$whereExpr.';';
			$result['LISTE_FRAISFORFAIT'] = $this->exec($sql,array_merge(
				['projectId' => $projectId,'startDate' => $startDate,'endDate' => $endDate],
				Where::prepareNamedWhereINArgs('delivery', $deliveries)
			));
		}
		return $result;
	}
	/**
	 * \brief récupère les données d'une commande
	 * @param <type> $id identifiant de la commande
	 */
	public function getFactureData($id, $onglet = BM::TAB_DEFAULT)
	{
		$baseQuery = new Query();
		$baseQuery->select([
			'ID_FACTURATION', 'ID_PROJET', 'ID_RESPUSER', 'TAB_PROJET.ID_AO', 'PRJ_TYPE', 'PRJ_TYPEREF',
			'TAB_PROJET.ID_SOCIETE', 'TAB_PROJET.ID_POLE', 'TAB_BONDECOMMANDE.ID_BONDECOMMANDE', 'FACT_REF'
		])
			->from('TAB_FACTURATION')
			->addJoin('INNER JOIN TAB_BONDECOMMANDE USING(ID_BONDECOMMANDE)')
			->addJoin('INNER JOIN TAB_PROJET USING(ID_PROJET)')
			->addWhere('ID_FACTURATION = ?', $id);

		switch($onglet) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Invoice::TAB_INFORMATION:
				$baseQuery->select(
					'ECH_DESCRIPTION, ECH_DATE, TAB_FACTURATION.ID_ECHEANCIER, SOCIETE_GROUPE, PRJ_REFERENCE, SOCIETE_ADR, 
					SOCIETE_VILLE, SOCIETE_RAISON, SOCIETE_CP, SOCIETE_PAYS, SOCIETE_EMAIL, SOCIETE_TEL, SOCIETE_TVA, 
					SOCIETE_STATUT, SOCIETE_RCS, SOCIETE_SIREN, SOCIETE_NIC, SOCIETE_NAF, SOC.ID_CRMSOCIETE, SOC.CSOC_SOCIETE, 
					TAB_PROJET.ID_CRMCONTACT, AO_TITLE, AO_REF, CCON_NOM, CCON_PRENOM, 
					SOC.CSOC_TVA, SOC.CSOC_NUMERO, TAB_COORDONNEES.ID_COORDONNEES, COORD_NOM, COORD_CONTACT, COORD_EMAIL, COORD_EMAIL2, COORD_EMAIL3, 
					COORD_TEL, COORD_ADR1, COORD_ADR2, COORD_ADR3, COORD_CP, COORD_VILLE, COORD_PAYS, 
					PRJ_DEBUT, PRJ_FIN, PRJ_DEVISE, TAB_PROFIL.ID_PROFIL, PROFIL_EMAIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_TEL1,
					BDC_REF, BDC_REFCLIENT, BDC_DATE, BDC_TYPEREGLEMENT, BDC_CONDREGLEMENT, BDC_TAUXTVA, BDC_LANGUE, 
					BDC_COMMENTAIRE, BDC_SHOWRIB, BDC_SHOWPRJREFERENCE, BDC_SHOWFOOTER, 
					BDC_SHOWINTNAME, BDC_SHOWTARIFJRS, BDC_SHOWJRSOUVRES, BDC_COPYCOMMENTS, TAB_BONDECOMMANDE.ID_CRMSOCIETE AS ID_FACTOR, 
					BDC_SHOWFACTOR, BDC_SHOWNUMBER, BDC_SHOWTVAIC, BDC_MENTIONS, FACT_AVOIR, FACT_DATE, FACT_DATEREGLEMENTATTENDUE, 
					FACT_DATEREGLEMENTRECUE, FACT_TYPEPAYMENT, FACT_TAUXREMISE, FACT_COMMENTAIRE, FACT_DEVISEAGENCE, FACT_CHANGEAGENCE, 
					FACT_DEVISE, FACT_CHANGE, FACT_ETAT, FACT_DEBUT, FACT_FIN, FACT_SHOWCOMMENTAIRE, FACT_CLOTURE, 
					TAB_RIB.ID_RIB, RIB_DESCRIPTION, RIB_IBAN, RIB_BIC,
					FACTOR.CSOC_SOCIETE AS FACTOR_SOCIETE, FACTOR.CSOC_SIREN AS FACTOR_SIREN, FACTOR.CSOC_NIC AS FACTOR_NIC, 
					FACTOR.CSOC_STATUT AS FACTOR_STATUT, FACTOR.CSOC_RCS AS FACTOR_RCS, FACTOR.CSOC_TVA AS FACTOR_TVA, 
					FACTOR.CSOC_ADR AS FACTOR_ADR, FACTOR.CSOC_CP AS FACTOR_CP, FACTOR.CSOC_VILLE AS FACTOR_VILLE, FACTOR.CSOC_PAYS AS FACTOR_PAYS, 
					FACTOR.CSOC_NAF AS FACTOR_NAF'
				)
				->addJoin('LEFT JOIN TAB_AO USING(ID_AO)')
				->addJoin('LEFT JOIN TAB_ECHEANCIER ON(TAB_ECHEANCIER.ID_ECHEANCIER = TAB_FACTURATION.ID_ECHEANCIER)')
				->addJoin('LEFT JOIN TAB_COORDONNEES ON(TAB_COORDONNEES.ID_COORDONNEES = TAB_BONDECOMMANDE.ID_COORDONNEES)')
				->addJoin('LEFT JOIN TAB_RIB ON(TAB_RIB.ID_RIB = TAB_BONDECOMMANDE.ID_RIB)')
				->addJoin('LEFT JOIN TAB_CRMSOCIETE FACTOR ON(FACTOR.ID_CRMSOCIETE = TAB_BONDECOMMANDE.ID_CRMSOCIETE)')
				->addJoin('LEFT JOIN TAB_USER ON(TAB_USER.ID_USER = TAB_BONDECOMMANDE.ID_RESPUSER)')
				->addJoin('LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL = TAB_USER.ID_PROFIL)')
				->addJoin('LEFT JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE = TAB_PROJET.ID_SOCIETE)')
				->addJoin('LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT = TAB_PROJET.ID_CRMCONTACT')
				->addJoin('LEFT JOIN TAB_CRMSOCIETE SOC ON(SOC.ID_CRMSOCIETE = TAB_PROJET.ID_CRMSOCIETE)');

				$result = $this->singleExec($baseQuery);
				if($result) {
					//On récupère tous les items
					$sql = 'SELECT ID_ITEMFACTURE, ITEM_DESCRIPTION, ITEM_MONTANTHT, ITEM_TAUXTVA, ITEM_QUANTITE, ITEM_TYPEF, TYPEF_REF, TYPEF_NAME 
					        FROM TAB_ITEMFACTURE 
					        LEFT JOIN TAB_TYPEFACTURATION ON(TYPEF_REF=ITEM_TYPEF AND TAB_TYPEFACTURATION.ID_SOCIETE = :agency) 
					        WHERE ID_FACTURATION = :id
					        ORDER BY ID_ITEMFACTURE ASC';

					$result['ITEMS'] = $this->exec($sql, [
						'agency' => $result['ID_SOCIETE'],
						'id' => $id
					]);

					//On cherche toutes les missions/ventes/Achats corrélés à ce bon de commande
					$sql = 'SELECT ID_CORRELATIONBONDECOMMANDE, ID_ITEM, CORBDC_TYPE FROM TAB_CORRELATIONBONDECOMMANDE WHERE ID_BONDECOMMANDE = ? ORDER BY ID_ITEM ASC';
					$result['CORRELATIONS'] = $this->exec($sql, $result['ID_BONDECOMMANDE']);
				}
				break;
			default:
				$result = $this->singleExec($baseQuery);
		}

		return $result;
	}
}
