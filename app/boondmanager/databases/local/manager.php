<?php
/**
 * manager.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\MySQL\Query;
use BoondManager\Lib\Specifications\ManagerIsActive;
use Wish\Models\Model;
use Wish\Tools;
use BoondManager\Services\BM;
use Wish\Cache;

/**
 * Class Manager
 * @package BoondManager\Databases\Local
 */
class Manager extends AbstractObject {
	const FROM_PROFIL = 'ID_PROFIL';
	const FROM_USER = 'ID_USER';

	/**
	 * get the column name (ID_PROFIL or ID_USER ) depending on the type value (true or false)
	 * @param bool $type
	 * @return string
	 */
	public static function getRefIdColumn($type){
		if(!$type) return self::FROM_USER;
		else return self::FROM_PROFIL;
	}

	/**
	 * Check that the manager belongs to one of the user business unit
	 * @param integer $id Manager ID.
	 * @param boolean $type If `true` the check is made using [TAB_USER.ID_PROFIL](../../bddclient/classes/TAB_USER.html#property_ID_PROFIL), otherwise [TAB_USER.ID_USER](../../bddclient/classes/TAB_USER.html#property_ID_USER).
	 * @return boolean
	 */
	/**
	 * Check that the manager belongs to one of the user business unit
	 * @param int $id
	 * @param array $tabBUs list of business unit's id
	 * @param bool $type
	 * @return bool
	 */
	public function isManagerInBusinessUnits($id, $tabBUs, $type = false) {
	return $this->isManagerExists($id, $this->getManagersFromBusinessUnits($tabBUs), $type);
	}

	/**
	 * Retrieve managers
	 *
	 * @param boolean $only_active If `true` it retrieve only managers having an active account
	 * @param array $addManagers an array with more manager ID to retrieve
	 * @return \BoondManager\Models\Account[]
	 */
	public function getSpecificGroupManagers($only_active = true, $addManagers = []) {
		$tabManagersGroupe = $this->getAllGroupManagers();

		if($only_active) {
			$tabManagersGroupe = Tools::filterArrayWithSpec($tabManagersGroupe, new ManagerIsActive() );
		}

		$managersIDs = Tools::getFieldsToArray($tabManagersGroupe, 'ID_PROFIL');
		$addManagers = array_diff($addManagers, $managersIDs);

		if($addManagers){
			$managersToAppend = $this->getManagersFromEmployee($addManagers);
			$tabManagersGroupe = array_merge($tabManagersGroupe, $managersToAppend);
		}

		return $tabManagersGroupe;
	}

	/**
	 * build a basic mysql query for manager
	 * @return Query
	 */
	private function getBasicManagerQuery(){
		$query = new Query();
		$query->select('TAB_USER.ID_PROFIL, USER_ABONNEMENT, USER_LANGUE, ID_USER, PROFIL_NOM, PROFIL_PRENOM, PROFIL_EMAIL, PROFIL_STATUT, ID_RESPMANAGER')
			->from('TAB_USER INNER JOIN TAB_PROFIL USING(ID_PROFIL)');
		return $query;
	}

	/**
	 * get a list of manager/user from a list of ids
	 * @param array|int $tabIds
	 * @param string $type @see self::TYPE
	 * @return Model[]
	 * @throws \Exception
	 */
	public function getManagers($tabIds, $type = self::FROM_USER){
		if(!is_array($tabIds)) $tabIds = [$tabIds];
		if(!$tabIds) return [];

		if(!in_array($type, [self::FROM_USER, self::FROM_PROFIL])) throw new \Exception('wrong type');
		$query = $this->getBasicManagerQuery()
			->addWhere('TAB_USER.'.$type.' IN (?)', $tabIds)
			->addOrderBy('USER_ABONNEMENT DESC, PROFIL_NOM ASC');

		return $this->exec($query);
	}

	/**
	 * get a list of manager/user from a list of profil ids
	 * @param array|int $tabIds
	 * @return Model[]
	 */
	public function getManagersFromEmployee($tabIds){
		return $this->getManagers($tabIds, self::FROM_PROFIL);
	}

	/**
	 * get a list of manager/user from a list of user ids
	 * @param array|int $tabIds
	 * @return Model[]
	 * @deprecated
	 */
	public function getManagersFromUser($tabIds){
		return $this->getManagers($tabIds, self::FROM_USER);
	}

	/**
	 * get a list of all group managers
	 * @return Model[]
	 */
	public function getAllGroupManagers(){
		$cache = Cache::instance();
		$key = 'managers.group.list';
		if(!$cache->exists($key)){
			$query = $this->getBasicManagerQuery()
				->addWhere('USER_TYPE=?', BM::DB_TYPE_MANAGER)
				->addOrderBy('USER_ABONNEMENT DESC, PROFIL_NOM ASC');
			$data = $this->exec($query);
			$cache->set($key, $data);
		}
		return $cache->get($key);
	}

	/**
	 * Retrieve all manager belonging to a user
	 * @param integer $id
	 * @param array $addManagers
	 * @return Model[]
	 */
	public function getAllUserManagers($id, $addManagers = []) {
		//On construit la liste de tous les managers de la société
		$tabManagers = $this->getSpecificGroupManagers(true, $addManagers);
		$arbre = $this->getManagersTree($tabManagers, $id);//on construit l'arbre de leur dépendance

		$liste_result = array();
		foreach($tabManagers as $manager_societe) {
			if($manager_societe['ID_USER'] == $id) $liste_result[] = $manager_societe;
			foreach($arbre as $manager_sub) if($manager_sub['ID_USER'] == $manager_societe['ID_USER']) $liste_result[] = $manager_societe;
		}
		return $liste_result;
	}

	/**
	 * Build recursively a list of all managers in a hierarchy
	 * @param array $tabManagers
	 * @param integer $id
	 * @return array
	 */
	public function getManagersTree($tabManagers, $id) {
		$arbre = array();
		foreach($tabManagers as $manager) {
			if($manager['ID_RESPMANAGER'] == $id) {
				$arbre[] = $manager;
				$manager_arbre = $this->getManagersTree($tabManagers, $manager['ID_USER']);
				if(sizeof($manager_arbre) != 0) $arbre = array_merge($arbre, $manager_arbre);
			}
		}
		return $arbre;
	}

	/**
	 * Check if the manager exists in a given aray
	 *
	 * @param integer $id manager Id to search
	 * @param array $tabManagers array of managers to check
	 * @param boolean $type If `true` the check is made using [TAB_USER.ID_PROFIL](../../bddclient/classes/TAB_USER.html#property_ID_PROFIL), otherwise [TAB_USER.ID_USER](../../bddclient/classes/TAB_USER.html#property_ID_USER).
	 * @return boolean
	 */
	public static function isManagerExists($id, $tabManagers = array(), $type = false) {
		$item = self::getRefIdColumn($type);
		foreach($tabManagers as $manager) if($manager[$item] == $id) return true;
		return false;
	}
	/**
	 * Retrieve all managers from a list of business units
	 * @param array $tabIds business units' id
	 * @return Model[]
	 */
	public function getManagersFromBusinessUnits($tabIds) {
		if(!is_array($tabIds)) $tabIds = [$tabIds];
		if(!$tabIds) return [];

		$cache = Cache::instance();
		$key = 'managers.bu.list.'.implode('_',$tabIds);
		if(!$cache->exists($key)) {
			$query = new Query();
			$query->select('TAB_USER.ID_PROFIL, ID_USER, PROFIL_NOM, PROFIL_PRENOM, PROFIL_EMAIL, PROFIL_STATUT, ID_RESPMANAGER, ID_BUSINESSUNIT')
					->from('TAB_USER INNER JOIN TAB_PROFIL USING(ID_PROFIL) INNER JOIN TAB_INBU USING(ID_USER)')
					->addWhere('USER_ABONNEMENT=? AND USER_TYPE=?', [BM::DB_ACCESS_ACTIVE, BM::DB_TYPE_MANAGER])
					->addWhere('ID_BUSINESSUNIT IN (?)', $tabIds)
					->addOrderBy('PROFIL_NOM ASC');
			$data = $this->exec($query);
			$cache->set($key, $data);
		}
		return $cache->get($key);
	}

	/**
	 * @return \BoondManager\Models\Account[]
	 */
	public function getAllBasicManagers(){
		$cache = Cache::instance();
		$key = 'manager.basic.list';
		if(!$cache->exists($key)) {
			$managers    = $this->getAllGroupManagers();
			$mappedArray = [];
			foreach($managers as $manager){
				$mappedArray[$manager['ID_PROFIL']] = new Model([
					'ID_PROFIL' => $manager['ID_PROFIL'],
					'PROFIL_NOM' => $manager['PROFIL_NOM'],
					'PROFIL_PRENOM' => $manager['PROFIL_PRENOM'],
					'PROFIL_STATUT' => $managers['PROFIL_STATUT']
				]);
			}
			$cache->set($key, $mappedArray);
		}
		return $cache->get($key);
	}

	/**
	 * return a manager with minimum info (id & lastname & firstname)
	 * @param $id
	 * @return Model|null
	 */
	public function getBasicManager($id){
		$managers = $this->getAllBasicManagers();
		$manager = Tools::mapData($id, $managers);
		if(!$manager && $id){
			$manager = $this->singleExec('SELECT ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT FROM TAB_PROFIL WHERE ID_PROFIL = ?', $id);
		}
		return $manager;
	}

	public function updateManager($data, $id, $iddt = 0, $solrSync = true) {
		// 	clear cache to reload included (bug #BMLABS-2024)
		$cache = Cache::instance();
		$cache->clear('manager.basic.list');
		$cache->clear('managers.group.list');
		$cache->clear('bu.group.list');
		$dbEmployee = Employee::instance();
		return $dbEmployee->updateEmployee($data, $id, $iddt, $solrSync);
	}

}
