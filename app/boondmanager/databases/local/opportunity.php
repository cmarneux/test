<?php
/**
 * opportunity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Services\BM;
use BoondManager\Services;

use BoondManager\APIs\Opportunities\Filters\SearchOpportunities;
use BoondManager\Models;

/**
 * Handle all database work related to resources
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Opportunity extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchOpportunities $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
    public function searchOpportunities(SearchOpportunities $filter)
    {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

        $query = new Query();
        $query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

        $query->addColumns('OPP.ID_AO, AORESP.ID_PROFIL, OPP.ID_SOCIETE, OPP.ID_POLE, AORESP.PROFIL_NOM, AORESP.PROFIL_PRENOM, ID_USER, AO_APPLICATIONS, AO_INTERVENTION, AO_TYPESOURCE, AO_SOURCE,
                    AO_TITLE, AO_REF, AO_TYPE, AO_TYPEREF, AO_DEPOT, AO_TYPEDEBUT, AO_DEBUT, AO_LIEU, AO_DUREE, AO_ETAT, AO_CA, AO_PROBA, AO_DEVISE, AO_CHANGE, AO_DEVISEAGENCE, AO_CHANGEAGENCE, AO_VISIBILITE,
                    TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, TAB_CRMCONTACT.ID_PROFIL AS CRMRESP_IDPROFIL, TAB_CRMCONTACT.ID_SOCIETE AS CRMRESP_IDSOCIETE, TAB_CRMCONTACT.ID_POLE AS CRMRESP_IDPOLE,
                    TAB_CRMSOCIETE.ID_CRMSOCIETE, CSOC_SOCIETE, SUM(IF(POS_ETAT NOT IN(1,2),1,0)) AS NB_POS');
        $query->from('TAB_AO OPP');
        $query->addJoin('LEFT JOIN TAB_PROFIL AORESP ON(AORESP.ID_PROFIL=OPP.ID_PROFIL)
                            LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=AORESP.ID_PROFIL)
                            LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=OPP.ID_CRMCONTACT)
                            LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=OPP.ID_CRMSOCIETE)
                            LEFT JOIN TAB_POSITIONNEMENT ON(TAB_POSITIONNEMENT.ID_AO=OPP.ID_AO)');
        $query->groupBy('OPP.ID_AO');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'OPP.ID_SOCIETE',
			$colPole = 'OPP.ID_POLE',
			$colUser = 'OPP.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

        if($filter->onlyVisible->getValue()) $where->and_('AO_VISIBILITE=1');

        $keywordsMapping = ['AO'=>'OPP.ID_AO', 'PROD'=>'TAB_POSITIONNEMENT.ITEM_TYPE=1 AND TAB_POSITIONNEMENT.ID_ITEM', 'CAND'=>'TAB_POSITIONNEMENT.ITEM_TYPE=0 AND TAB_POSITIONNEMENT.ID_ITEM', 'COMP'=>'TAB_POSITIONNEMENT.ITEM_TYPE=0 AND TAB_POSITIONNEMENT.ID_ITEM', 'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
        $whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

        if (! $whereKeywords->isEmpty())
            $where->and_($whereKeywords);
        else if ($filter->keywords->getValue()) {
            $keywords = str_replace('%', '\%', $filter->keywords->getValue());
            $keywords = str_replace('*', '%', $keywords);
            $likeSearch = $keywords.'%';

            $whereKeywords = new Where('MATCH(AO_TITLE, AO_DESCRIPTION) AGAINST(?)', $keywords);
            $whereKeywords->or_('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch)
                          ->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
                          ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
                          ->or_('AO_REF LIKE ?', $likeSearch);
            $where->and_($whereKeywords);
            $query->addColumns([ 'PERTINENCE'=> 'MATCH(AO_TITLE, AO_DESCRIPTION) AGAINST(' . $this->escape($keywords) .  ')' ]);
            $query->addOrderBy('PERTINENCE DESC');
        }

        $where->and_( $this->getFilterSearch($filter->opportunityStates->getValue(), 'AO_ETAT') )
              ->and_( $this->getFilterSearch($filter->opportunityTypes->getValue(), 'AO_TYPEREF') )
              ->and_( $this->getFilterSearch($filter->durations->getValue(), 'AO_DUREE') )
              ->and_( $this->getFilterSearch($filter->origins->getValue(), 'AO_TYPESOURCE') )
              ->and_( $this->getFilterSearch($filter->activityAreas->getValue(), 'AO_APPLICATIONS', [], true) )
              ->and_( $this->getFilterSearch($filter->expertiseAreas->getValue(), 'AO_INTERVENTION', [], true) )
              ->and_( $this->getFilterSearch($filter->places->getValue(), 'AO_LIEU', [], true) )
              ->and_( $this->getFilterSearch($filter->tools->getValue(), 'AO_OUTILS', [], true) );

        $wherePosStates = $this->getFilterSearch($filter->positioningStates->getValue(), 'POS_ETAT', ['-2']);
        if(in_array('-2',$filter->positioningStates->getValue())) $wherePosStates->or_('ID_POSITIONNEMENT IS NULL');
        $where->and_( $wherePosStates );

        switch($filter->period->getValue()){
            case SearchOpportunities::PERIOD_CREATED:
                $where->and_('AO_DEPOT BETWEEN ? AND ?', $filter->getDatesPeriod());
                break;
            case SearchOpportunities::PERIOD_UPDATED_POSITIONING:
                $where->and_('DATE(POS_DATEUPDATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
                break;
            case SearchOpportunities::PERIOD_STARTED:
                $where->and_('AO_DEBUT BETWEEN ? AND ?', $filter->getDatesPeriod());
                break;
            case SearchOpportunities::PERIOD_UPDATED:
                $where->and_('AO_DATEUPDATE BETWEEN ? AND ?', $filter->getDatesPeriod());
                break;
            default:case BM::INDIFFERENT:break;
        }

        //FLAGS
        if(sizeof($filter->flags->getValue()) > 0) {
            $query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_OPPORTUNITY.' AND TAB_FLAG.ID_PARENT=OPP.ID_AO)');
            $where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
        }

        $this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

        $query->addWhere($where);

        $result = $this->launchSearch($query, Models\SearchResults\Opportunities::class);

        $result->turnoverWeightedExcludingTax = 0;
        if($result->total > 0){
            $ca = $this->singleExec('SELECT SUM(AO_CA*AO_PROBA/100) AS TOTAL_CA FROM (SELECT DISTINCT OPP.ID_AO, AO_CA, AO_PROBA FROM '.$query->getFrom().' '. $query->buildJoins().' '.$query->buildWhere().') OPP', $query->getArgs());
            if($ca) $result->turnoverWeightedExcludingTax += $ca->TOTAL_CA;
        }
        return $result;
    }

    /**
     * Adds an order by to the query
     * @param Query $query the query to modify
     * @param array $column the column to sort (front end value)
     * @param string $order ASC|DESC
     */
    private function setOrderExpr(Query $query, $column, $order) {

        $mapping = [
            SearchOpportunities::ORDERBY_CREATIONDATE => 'AO_DEPOT',
            SearchOpportunities::ORDERBY_TITLE => 'AO_TITLE',
            SearchOpportunities::ORDERBY_COMPANY_NAME => 'CSOC_SOCIETE',
            SearchOpportunities::ORDERBY_PLACE => 'AO_LIEU',
            SearchOpportunities::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS => 'NB_POS',
            SearchOpportunities::ORDERBY_STARTDATE => 'AO_DEBUT',
            SearchOpportunities::ORDERBY_DURATION => 'AO_DUREE',
            SearchOpportunities::ORDERBY_STATE => 'AO_ETAT',
            SearchOpportunities::ORDERBY_TOTAL_WEIGHTED_TURNOVER_EXCLUDED_TAX => 'TOTAL_CA',
            SearchOpportunities::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM'
        ];

        if(!$column) $query->addOrderBy('AO_DEPOT DESC, AO_REF DESC');

		foreach($column as $c)
        	if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

        if(sizeof($column) == 1 && in_array(SearchOpportunities::ORDERBY_CREATIONDATE, $column))
			$query->addOrderBy('AO_REF '.$order);

        $query->addOrderBy('OPP.ID_AO DESC');
    }


	/**
	 * Update an opportunity
	 * @param array $data new data
	 * @param int $id opportunity id to update
	 * @return boolean
	 */
	public function updateOpportunity($data, $id) {
		if(isset($data['POSITIONDETAILS'])) {
			//On supprime les items qui n'existent plus
			$ids = array_column($data['POSITIONDETAILS'],'ID_POSITIONDETAILS');
			$wIDp = new Where('ID_PARENT=? AND PARENT_TYPE=0', $id);
			if($ids) $wIDp->and_('ID_POSITIONDETAILS NOT IN (?)', $ids);
			$this->delete('TAB_POSITIONDETAILS', $wIDp->getWhere(), $wIDp->getArgs());

			//On met à jour les items
			foreach($data['POSITIONDETAILS'] as $dataItem) {
				if(isset($dataItem['ID_POSITIONDETAILS']) && $dataItem['ID_POSITIONDETAILS'])//Cet item existe déjà
					$this->update('TAB_POSITIONDETAILS', $dataItem, 'ID_POSITIONDETAILS=:id', ['id'=>$dataItem['ID_POSITIONDETAILS']]);
				else {//Cet item est nouvelle
					$dataItem['ID_PARENT'] = $id;
					$dataItem['PARENT_TYPE'] = 0;
					$this->insert('TAB_POSITIONDETAILS', $dataItem);
				}
			}
		}

		$data['AO']['AO_DATEUPDATE'] = date('Y-m-d H:i:s');
		$this->update('TAB_AO', $data['AO'], 'ID_AO=:id', ['id'=>$id]);
		return true;
	}

	/**
	 * create a new opportunity
	 * @param $data
	 * @return int|false the ID created or false if something went wrong
	 */
	public function newOpportunity($data){
		if(isset($data['AO'])) {
			if(!isset($data['AO']['AO_DATEUPDATE'])) $data['AO']['AO_DATEUPDATE'] = date('Y-m-d H:i:s');
			else $data['AO']['AO_DATEUPDATE'] = \DateTime::createFromFormat('Y-m-d H:i:s', $data['AO']['AO_DATEUPDATE'])->format('Y-m-d H:i:s');
			if(!isset($data['AO']['AO_DESCRIPTION'])) $data['AO']['AO_DESCRIPTION'] = '';
			if(!isset($data['AO']['AO_CRITERES'])) $data['AO']['AO_CRITERES'] = '';
			if(!isset($data['AO']['AO_OUTILS'])) $data['AO']['AO_OUTILS'] = '';
			$idao = $this->insert('TAB_AO', $data['AO']);

			if($idao && isset($data['POSITIONDETAILS'])) {
				//On met à jour les items
				foreach($data['POSITIONDETAILS'] as $dataItem) {
					$dataItem['ID_PARENT'] = $idao;
					$dataItem['PARENT_TYPE'] = 0;
					$this->insert('TAB_POSITIONDETAILS', $dataItem);
				}
			}
			return $idao;
		}
		return false;
	}

	/**
	 * retrieve an opportunity
	 * @param int $id opportunity ID
	 * @param int|string $tab retrieve info for the given tab
	 * @return false|Models\Opportunity
	 * @throws \Exception
	 */
	public function getOpportunity($id, $tab=0)
	{
		$baseQuery = new Query();
		$baseQuery->select(
				'TAB_AO.ID_AO, AO_TYPE, AO_TYPEREF, AO_TITLE, TAB_AO.ID_PROFIL, TAB_AO.ID_CRMCONTACT, TAB_AO.ID_CRMSOCIETE, 
				TAB_AO.ID_SOCIETE, TAB_AO.ID_POLE, AO_VISIBILITE, AO_DEPOT, AO_DATEUPDATE, AO_REF')
			->from('TAB_AO')
			->addWhere('TAB_AO.ID_AO = ?', $id);

		switch($tab) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Opportunity::TAB_INFORMATION://Informations
				$baseQuery->addColumns(
					'AO_TYPEDEBUT, AO_DEBUT, AO_BUDGET, AO_LIEU, AO_CORRELATIONPOS, 
					AO_DUREE, AO_CA, AO_PROBA, AO_DEVISEAGENCE, AO_CHANGEAGENCE, AO_DEVISE, AO_CHANGE, TAB_AO.ID_PROFIL, 
					AO_APPLICATIONS, AO_OUTILS, AO_INTERVENTION, AO_DESCRIPTION, AO_CRITERES, AO_VISIBILITE, AO_ETAT, 
					AO_TYPESOURCE, AO_SOURCE, AO_TARIFADDITIONNEL, AO_INVESTISSEMENT, 
					CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, CSOC_CP, CSOC_VILLE, CSOC_PAYS, 
					COUNT(DISTINCT TAB_PROJET.ID_PROJET) AS NB_PROJETS, 
					TAB_CRMCONTACT.ID_PROFIL AS CRMRESP_IDPROFIL, TAB_CRMCONTACT.ID_SOCIETE AS CRMRESP_IDSOCIETE, TAB_CRMCONTACT.ID_POLE AS CRMRESP_IDPOLE'
				);
				$baseQuery->addJoin('LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_AO.ID_CRMCONTACT)');
				$baseQuery->addJoin('LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_AO.ID_CRMSOCIETE)');
				$baseQuery->addJoin('LEFT JOIN TAB_PROJET USING(ID_AO)');

				$baseQuery->groupBy('TAB_AO.ID_AO');

				$result = $this->singleExec($baseQuery);
				if($result) {
					//On récupère tous les documents
					$result['DOCUMENTS'] = $this->exec('SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=1', $id);
				}

				return $result;
				break;
			case Models\Opportunity::TAB_SIMULATION:
				$baseQuery->addColumns('AO_TARIFADDITIONNEL, AO_INVESTISSEMENT, AO_CORRELATIONPOS');
				$baseQuery->addColumns(['AO_DEVISE', 'AO_CHANGE', 'AO_DEVISEAGENCE', 'AO_CHANGEAGENCE']);
				$result = $this->singleExec($baseQuery);
				if($result) {
					$result['POSITIONDETAILS'] = $this->exec('SELECT ID_POSITIONDETAILS, POSDETAILS_TARIF, POSDETAILS_INVESTISSEMENT, POSDETAILS_TITRE FROM TAB_POSITIONDETAILS WHERE ID_PARENT=? AND PARENT_TYPE=0 ORDER BY ID_POSITIONDETAILS ASC', $id);
				}
				return $result;
				break;
			case Models\Opportunity::TAB_POSITIONINGS://Positionnements Ressources/Candidats & Produits
				$baseQuery->addColumns(['AO_CA', 'AO_DEVISE', 'AO_CHANGE', 'AO_CORRELATIONPOS', 'AO_DEVISEAGENCE', 'AO_CHANGEAGENCE',  'AO_TARIFADDITIONNEL', 'AO_INVESTISSEMENT']);
				$result = $this->singleExec($baseQuery);
				if($result) {
					//On récupère tous les détails
					$result['POSITIONDETAILS'] = $this->exec('SELECT ID_POSITIONDETAILS, POSDETAILS_TARIF, POSDETAILS_INVESTISSEMENT, POSDETAILS_TITRE FROM TAB_POSITIONDETAILS WHERE ID_PARENT=? AND PARENT_TYPE=0 ORDER BY ID_POSITIONDETAILS ASC', $result['ID_AO']);
				}
				return $result;
			default:
				return $this->singleExec($baseQuery);
		}
	}

	/**
	 * @param int $id opportunity ID
	 * @param bool $solr trigger a SolR sync
	 * @return true
	 * @throws \Exception
	 */
	public function deleteOpportunity($id, $solr = true)
	{
		//On supprime les actions associés à ce besoin
		$dict = Services\Actions::getDictionaryForCategory(BM::CATEGORY_OPPORTUNITY);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id'));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//Il faut supprimer tous les positionnements de ce besoin
		$dbPositionings = new Positioning();
		$positionings = $this->exec(
			'SELECT ID_POSITIONNEMENT, TAB_POSITIONNEMENT.ID_ITEM, TAB_POSITIONNEMENT.ITEM_TYPE, PROFIL_TYPE 
			 FROM TAB_POSITIONNEMENT LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_POSITIONNEMENT.ID_ITEM AND TAB_POSITIONNEMENT.ITEM_TYPE=0)
			 WHERE ID_AO=?',
			$id
		);
		foreach($positionings as $pos)
			$dbPositionings->deleteObject($pos['ID_POSITIONNEMENT'], $pos['ID_ITEM'], $pos['ITEM_TYPE'], (isset($pos['PROFIL_TYPE']))?$pos['PROFIL_TYPE']:PROFIL_CANDIDAT, $solr);

		//Il faut supprimer tous les devis de ce besoin
		throw new \Exception('impossible de trouver la classe Devis'); // pas trouvé dans la v6
		$devisObject = new BoondManager_ObjectBDD_Devis($this->db);
		foreach($this->db->fetch_all_array('SELECT ID_DEVIS FROM TAB_DEVIS WHERE ID_AO="'.$this->db->escape($id).'"') as $devis) $devisObject->deleteDevisData($devis['ID_DEVIS']);

		//On supprime tous les documents de ce besoin
		$dbFiles = new File();
		$dbFiles->deleteAllObjects(File::TYPE_DOCUMENT, $id, Models\Document::TYPE_OPPORTUNITY);

		//On supprime tous les flags de ce besoin
		$dbFlags = new AttachedFlag();
		$dbFlags->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_OPPORTUNITY);

		$this->delete('TAB_POSITIONDETAILS', 'ID_PARENT=? AND PARENT_TYPE=0', $id);

		$this->delete('TAB_AO', 'ID_AO=?', $id);
		return true;
	}

	/**
	 * \brief Création d'un nouveau positionnement
	 * @param <type> $data tableau de données
	 * @return <type> identifiant du positionnement
	 */
	public function newPositionnementData($data, $iditem = 0, $itemtype = 0, $type_profil = PROFIL_CANDIDAT, $solr = true)
	{
		throw new \Exception('do migration'); //TODO
		if(isset($data['POSITIONNEMENT'])) {
			if(!isset($data['POSITIONNEMENT']['ID_RESPPROFIL'])) $data['POSITIONNEMENT']['ID_RESPPROFIL'] = Wish_Session::getInstance()->get('login_idprofil');
			if(!isset($data['POSITIONNEMENT']['POS_DATE'])) $data['POSITIONNEMENT']['POS_DATE'] = date('Y-m-d H:i:s', mktime());
			if(!isset($data['POSITIONNEMENT']['POS_DATEUPDATE'])) $data['POSITIONNEMENT']['POS_DATEUPDATE'] = date('Y-m-d H:i:s', mktime());
			if($iditem > 0) {
				$data['POSITIONNEMENT']['ID_ITEM'] = $iditem;
				$data['POSITIONNEMENT']['ITEM_TYPE'] = $itemtype;
			}
			$idpos = $this->db->query_insert('POSITIONNEMENT', $data['POSITIONNEMENT']);
			if($iditem > 0 && $itemtype == 0) {
				if($type_profil == PROFIL_CANDIDAT) {
					$candidatBDD = new BoondManager_ObjectBDD_Candidat($this->db);
					$candidatBDD->updateCandidatData(array(), $iditem, 0, $solr);
				} else {
					$ressourceBDD = new BoondManager_ObjectBDD_Ressource($this->db);
					$ressourceBDD->updateRessourceData(array(), $iditem, 0, $solr);
				}
			}

			if($idpos && isset($data['POSITIONDETAILS'])) {
				//On met à jour les items
				foreach($data['POSITIONDETAILS'] as $data_item) {
					$dataItem = array();
					$dataItem['POSDETAILS_TITRE'] = $data_item[1];
					$dataItem['POSDETAILS_TARIF'] = $data_item[2];
					$dataItem['POSDETAILS_INVESTISSEMENT'] = $data_item[3];
					$dataItem['ID_PARENT'] = $idpos;
					$dataItem['PARENT_TYPE'] = 1;
					$this->db->query_insert('POSITIONDETAILS', $dataItem);
				}
			}
			return $idpos;
		}
		return false;
	}

	/**
	 * @param int $idpos Positioning ID
	 * @param int $iditem Candidate/Resource ID
	 * @param int $itemtype
	 * @param int $type_profil Candidate/Resource Type
	 * @param bool $solr trigger a SolR Sync
	 * @return bool
	 */
	public function deletePositioning($idpos, $iditem = 0, $itemtype = 0, $type_profil = \BoondManager\Models\Candidate::TYPE_CANDIDATE, $solr = true)
	{
		//On supprime tous les documents de ce positionnement
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_DOCUMENT, $idpos, Models\Document::TYPE_POSITIONING);

		//On supprime tous les flags de cette action
		$dbFlags = new AttachedFlag();
		$dbFlags->removeAllAttachedFlagsFromEntity($idpos, Models\AttachedFlag::TYPE_POSITIONING);

		$this->delete('TAB_POSITIONDETAILS', 'ID_PARENT=? AND PARENT_TYPE=1', $idpos);

		$this->delete('TAB_POSITIONNEMENT', 'ID_POSITIONNEMENT=?', $idpos);
		if($iditem > 0 && $itemtype == 0) {
			if($type_profil == \BoondManager\Models\Candidate::TYPE_CANDIDATE) {
				$dbCandidates = new Candidate();
				$dbCandidates->updateObject([], $iditem, 0, $solr);
			} else {
				$dbResource = new Employee();
				$dbResource->updateEmployee([], $iditem, 0, $solr);
			}
		}
		return true;
	}

	/**
	 * \brief Récupère les données d'un positionnement
	 * @param <type> $id identifiant du positionnement
	 */
	public function getPositionnementData($id)
	{
		throw new \Exception('do migration'); //TODO
		$sql = 'SELECT ID_POSITIONNEMENT, TAB_POSITIONNEMENT.ID_ITEM, TAB_POSITIONNEMENT.ITEM_TYPE, ID_RESPPROFIL, POS_ETAT, POS_DATE, POS_DATEUPDATE, POS_COMMENTAIRE, POS_DEBUT, POS_FIN, POS_TARIF, POS_CJM, POS_NBJRSFACTURE, POS_NBJRSGRATUIT, POS_CORRELATION, POS_TARIFADDITIONNEL, POS_INVESTISSEMENT,
				TAB_AO.ID_AO, TAB_AO.ID_SOCIETE AS AO_IDSOCIETE, TAB_AO.ID_POLE AS AO_IDPOLE, TAB_AO.ID_PROFIL AS AO_IDPROFIL, AO_VISIBILITE, AO_CORRELATIONPOS, AO_CA, AO_TYPE, AO_TYPEREF, AO_TITLE, AO_REF, AO_DEVISE, AO_CHANGE, AO_DEVISEAGENCE, AO_CHANGEAGENCE, TAB_AO.ID_CRMCONTACT,
				TAB_PROJET.ID_PROJET, PRJ_REFERENCE,
				COMP.PROFIL_TYPE, COMP.PROFIL_NOM, COMP.PROFIL_PRENOM, COMP.ID_SOCIETE, COMP.ID_POLE,
				TAB_AO.ID_CRMSOCIETE, CSOC.CSOC_SOCIETE, CCON.CCON_NOM, CCON.CCON_PRENOM,
				PRODUIT_NOM, PRODUIT_REF, PRODUIT_TYPE, PRODUIT_TAUXTVA, PRODUIT_TARIFHT
				FROM TAB_POSITIONNEMENT
					INNER JOIN TAB_AO USING(ID_AO)
						LEFT JOIN TAB_PROFIL COMP ON(COMP.ID_PROFIL=TAB_POSITIONNEMENT.ID_ITEM AND TAB_POSITIONNEMENT.ITEM_TYPE=0)
						LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=TAB_POSITIONNEMENT.ID_ITEM AND TAB_POSITIONNEMENT.ITEM_TYPE=1)
					LEFT JOIN TAB_PROJET USING(ID_PROJET)
					LEFT JOIN TAB_CRMCONTACT CCON ON(CCON.ID_CRMCONTACT=TAB_AO.ID_CRMCONTACT) LEFT JOIN TAB_CRMSOCIETE CSOC ON(CSOC.ID_CRMSOCIETE=TAB_AO.ID_CRMSOCIETE)
				WHERE ID_POSITIONNEMENT="'.$this->db->escape($id).'";';
		$result = $this->db->query_first($sql);
		if($result) {
			$this->data = $result;

			//On récupère tous les détails
			$this->data['POSITIONDETAILS'] = $this->db->fetch_all_array('SELECT ID_POSITIONDETAILS, POSDETAILS_TARIF, POSDETAILS_INVESTISSEMENT, POSDETAILS_TITRE FROM TAB_POSITIONDETAILS WHERE ID_PARENT="'.$result['ID_POSITIONNEMENT'].'" AND PARENT_TYPE=1 ORDER BY ID_POSITIONDETAILS ASC');

			//On récupère tous les documents
			$this->data['DOCUMENTS'] = $this->db->fetch_all_array('SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT="'.$result['ID_POSITIONNEMENT'].'" AND DOC_TYPE=9;');
			return true;
		}
		return false;
	}

	/**
	 * \brief Met à jour un positionnement
	 * @param <type> $data tableau de données
	 * @param <type> $idpos identifiant du positionnement
	 */
	public function updatePositionnementData($data, $idpos, $iditem = 0, $itemtype = 0, $type_profil = PROFIL_CANDIDAT, $solr = true)
	{
		throw new \Exception('do migration'); //TODO
		if(isset($data['POSITIONNEMENT'])) {
			$data['POSITIONNEMENT']['POS_DATEUPDATE'] = date('Y-m-d H:i:s', mktime());
			if($iditem > 0) {
				$data['POSITIONNEMENT']['ID_ITEM'] = $iditem;
				$data['POSITIONNEMENT']['ITEM_TYPE'] = $itemtype;
			}
			$this->db->query_update('POSITIONNEMENT', $data['POSITIONNEMENT'], 'ID_POSITIONNEMENT="'.$this->db->escape($idpos).'"');
			if($iditem > 0 && $itemtype == 0) {
				if($type_profil == PROFIL_CANDIDAT) {
					$candidatBDD = new BoondManager_ObjectBDD_Candidat($this->db);
					$candidatBDD->updateCandidatData(array(), $iditem, 0, $solr);
				} else {
					$ressourceBDD = new BoondManager_ObjectBDD_Ressource($this->db);
					$ressourceBDD->updateRessourceData(array(), $iditem, 0, $solr);
				}
			}
		}

		if(isset($data['POSITIONDETAILS'])) {
			//On supprime les items qui n'existent plus
			foreach($this->db->fetch_all_array('SELECT ID_POSITIONDETAILS FROM TAB_POSITIONDETAILS WHERE ID_PARENT="'.$this->db->escape($idpos).'" AND PARENT_TYPE=1;') as $item) {
				$etat = false;
				foreach($data['POSITIONDETAILS'] as $data_item) if($data_item[0] == $item['ID_POSITIONDETAILS']) {$etat = true;break;}
				if(!$etat) $this->db->query_delete('POSITIONDETAILS', 'ID_POSITIONDETAILS="'.$item['ID_POSITIONDETAILS'].'"');
			}

			//On met à jour les items
			foreach($data['POSITIONDETAILS'] as $data_item) {
				$dataItem = array();
				$dataItem['POSDETAILS_TITRE'] = $data_item[1];
				$dataItem['POSDETAILS_TARIF'] = $data_item[2];
				$dataItem['POSDETAILS_INVESTISSEMENT'] = $data_item[3];

				if($data_item[0] != 0)//Cet item existe déjà
					$this->db->query_update('POSITIONDETAILS', $dataItem, 'ID_POSITIONDETAILS="'.$data_item[0].'"');
				else {//Cet item est nouvelle
					$dataItem['ID_PARENT'] = $idpos;
					$dataItem['PARENT_TYPE'] = 1;
					$this->db->query_insert('POSITIONDETAILS', $dataItem);
				}
			}
		}
		return true;
	}

	/**
	 * \brief Retourne la prochaine référence du besoin en tenant compte du masque
	 * @param string $mask
	 */
	function getNewReferenceBesoin($idao = 0, $id_ressource = 0, $id_crmcontact = 0, $mask = '', $onlymask = false, $forceDate = '') {
		throw new \Exception('do migration'); //TODO
		if(preg_match("/([a-zA-Z0-9\[\]_\-\/]{1,})/", $mask, $matchRef)) {
			if($forceDate != '' && preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $forceDate, $match)) {
				$forceDate = explode('-',$forceDate);
				$annee4 = $forceDate[0];
				$annee2 = substr($forceDate[0],2);
				$mois = $forceDate[1];
				$jour = $forceDate[2];
			} else {
				$actualTime = mktime();
				$annee4 = date('Y',$actualTime);
				$annee2 = date('y',$actualTime);
				$mois = date('m',$actualTime);
				$jour = date('d',$actualTime);
			}

			$crmBDD = new BoondManager_ObjectBDD_CRM($this->db);
			if($crmBDD->getCRMData($id_crmcontact, 1)) $trigramme_client = strtoupper(substr(suppr_accents($crmBDD->get('CSOC_SOCIETE')),0,3)); else $trigramme_client = '';

			$profilBDD = new BoondManager_ObjectBDD_Ressource($this->db);
			if($profilBDD->getRessourceData($id_ressource, 0)) $trigramme_manager = strtoupper(substr(suppr_accents($profilBDD->get('PROFIL_PRENOM')),0,1).substr(suppr_accents($profilBDD->get('PROFIL_NOM')),0,2)); else $trigramme_manager = '';

			$mask = str_replace(array('[AAAA]','[AA]','[MM]','[JJ]','[ID_AO]','[CCC]','[RRR]'), array($annee4,$annee2,$mois,$jour,$idao,$trigramme_client,$trigramme_manager), $matchRef[1]);
			$maskWhere = str_replace(array('[AAAA]','[AA]','[MM]','[JJ]','[ID_AO]','[CCC]','[RRR]','_','/','-'), array($annee4,$annee2,$mois,$jour,$idao,$trigramme_client,$trigramme_manager,'\_','\/','\-'), $matchRef[1]);//Masque de la clause Where
			$maskMax = str_replace(array('/', '-'), array('\/', '\-'), $mask);

			if(strpos($matchRef[1], '[ID_AO]') === false) {
				$result = $this->db->query_first('SELECT MAX(CAST(SUBSTRING(AO_REF,'.(strlen($mask)+2).') AS UNSIGNED)) AS MAX_REFERENCE FROM TAB_AO WHERE AO_REF LIKE \''.$this->db->escape($maskWhere).'%\' AND ID_AO<>'.$this->db->escape($idao).';');
				if($result) $maxReference = $mask.'_'.$result['MAX_REFERENCE']; else $maxReference = $mask;
			} else $maxReference = $mask;
			if(preg_match("/".$maskMax."_([0-9]{1,})/", $maxReference, $matchMax)) $startNumber = '_'.(intval($matchMax[1])+1); else $startNumber = '';
		} else {
			$mask = 'AO'.$idao;
			$maskMax = 'AO'.$idao;
			$startNumber = '';
		}

		if($onlymask) return $mask; else return $mask.$startNumber;
	}

	/**
	 * \brief On gagne un positionnement, on va créer les projets/missions, DOIT OBLIGATOIREMENT ETRE APPELE APRES AVOIR RECUPERE LES DONNEES DU BESOIN/POSITIONNEMENT
	 */
	public function winPositionnement($idprofil, $profilType, $selProject = '', $idPos = 0, $tabPositionnement = array(), $solr = true, $groupement = array())
	{
		throw new \Exception('do migration'); //TODO
		$modDevise = new BoondManager_Devise();
		$showok = false;

		$posDebut = '';
		$posFin = '';
		$posTarif = 0;
		$posNbJrsFactures = 0;
		$posNbJrsGratuit = 0;
		$posDetails = array();

		$tmpAO = new BoondManager_ObjectBDD_Besoin($this->db);
		if($idPos != 0 && $tmpAO->getPositionnementData($idPos)) $tabPositionnement = $tmpAO->getData();
		$tmpAO->setData();

		if(isset($tabPositionnement['POS_DEBUT']) && $tabPositionnement['POS_DEBUT'] != 'NULL') $posDebut = $tabPositionnement['POS_DEBUT'];
		if(isset($tabPositionnement['POS_FIN']) && $tabPositionnement['POS_FIN'] != 'NULL') $posFin = $tabPositionnement['POS_FIN'];
		if(isset($tabPositionnement['POS_TARIF'])) $posTarif = $tabPositionnement['POS_TARIF'];
		if(isset($tabPositionnement['POS_NBJRSFACTURE'])) $posNbJrsFactures = $tabPositionnement['POS_NBJRSFACTURE'];
		if(isset($tabPositionnement['POS_NBJRSGRATUIT'])) $posNbJrsGratuit = $tabPositionnement['POS_NBJRSGRATUIT'];
		if(isset($tabPositionnement['POSITIONDETAILS'])) $posDetails = $tabPositionnement['POSITIONDETAILS'];

		//On vérifie si le positionnement du profil est autorisé
		if($profilType == -1) {
			$produitBDD = new BoondManager_ObjectBDD_Produit($this->db);
			if($produitBDD->getProduitData($idprofil)) $showok = true;
		} else {
			$ressourceBDD = new BoondManager_ObjectBDD_Ressource($this->db);
			if($profilType == PROFIL_CANDIDAT) {
				$candidatBDD = new BoondManager_ObjectBDD_Candidat($this->db);
				if($candidatBDD->getCandidatData($idprofil)) {
					if($this->get('AO_TYPE') == AO_RECRUTEMENT) {$showok = true;$idprofil=intval($candidatBDD->get('ID_PROFIL'));}//Pour un recrutement, le projet peut être créé avec un candidat
					else if(intval($candidatBDD->get('ID_PROFILCONSULTANT')) > 0 && $ressourceBDD->getRessourceData($candidatBDD->get('ID_PROFILCONSULTANT'),1)) {$showok = true;$idprofil=intval($candidatBDD->get('ID_PROFILCONSULTANT'));} else return -1; //Le candidat n'est pas embauché
				}
			} else if($ressourceBDD->getRessourceData($idprofil,1)) $showok = true;
		}

		if($showok) {
			$newprojet = true;

			$societeBDD = new BoondManager_ObjectBDD_Societe($this->db);
			$projetBDD = new BoondManager_ObjectBDD_Projet($this->db);
			$projetliste = explode('_',$selProject);
			if(isset($projetliste[1])) {
				$idprojet = intval($projetliste[1]);
				if($idprojet && $projetBDD->getProjetData($idprojet) && $this->get('ID_AO') == $projetBDD->get('ID_AO')) {
					$dataProjet = $projetBDD->getData();
					$newprojet = false;
					if($this->get('AO_TYPE') == AO_RECRUTEMENT && $projetBDD->isRegistered('COMP_IDPROFIL')) return -2;//Le projet contient déjà un candidat gagné
				}
			}

			if($this->isRegistered('AO_IDSOCIETE')) $idAOSociete = $this->get('AO_IDSOCIETE'); else $idAOSociete = $this->get('ID_SOCIETE');
			if($this->isRegistered('AO_IDPOLE')) $idAOPole = $this->get('AO_IDPOLE'); else $idAOPole = $this->get('ID_POLE');
			if($this->isRegistered('AO_IDPROFIL')) $idAOProfil = $this->get('AO_IDPROFIL'); else $idAOProfil = $this->get('ID_PROFIL');
			$idPRJReference = '';
			if($newprojet) {
				$dataProjet['PROJET'] = array('ID_AO' => $this->get('ID_AO'),
				                              'ID_PROFIL' => $idAOProfil,
				                              'ID_SOCIETE' => $idAOSociete,
				                              'ID_POLE' => $idAOPole,
				                              'ID_CRMCONTACT' => $this->get('ID_CRMCONTACT'),
				                              'ID_CRMSOCIETE' => $this->get('ID_CRMSOCIETE'),
				                              'PRJ_TYPE' => $this->get('AO_TYPE'),
				                              'PRJ_TYPEREF' => $this->get('AO_TYPEREF'),
				                              'PRJ_DATE' => date('Y-m-d H:i:s', mktime()),
				                              'PRJ_TARIFADDITIONNEL' => 0,
				                              'PRJ_INVESTISSEMENT' => 0,
				                              'PRJ_DEVISE' => $modDevise->getDeviseAgence(),
				                              'PRJ_DEVISEAGENCE' => $modDevise->getDeviseAgence(),
				                              'PRJ_CHANGEAGENCE' => $modDevise->getTauxAgence());

				if($posTarif != 0 && $this->get('AO_TYPE') == AO_RECRUTEMENT) {
					$dataProjet['PROJET']['PRJ_TARIFADDITIONNEL'] = $posTarif;
					$dataProjet['MISSIONDETAILS'][] = array(0,date('Y-m-d', mktime()),1,'',$posTarif,0);
				}
				if($posDebut != '') $dataProjet['PROJET']['PRJ_DEBUT'] = $posDebut; else $dataProjet['PROJET']['PRJ_DEBUT'] = date('Y-m-d', mktime());
				if($posFin != '') $dataProjet['PROJET']['PRJ_FIN'] = $posFin; else $dataProjet['PROJET']['PRJ_FIN'] = date('Y-m-d', mktime());

				//On récupère les informations du client si c'est un besoin client
				$crmBDD = new BoondManager_ObjectBDD_CRM($this->db);
				if($crmBDD->getCRMData($this->get('ID_CRMSOCIETE'), 0)) {
					$dataProjet['PROJET']['PRJ_ADR'] = $crmBDD->get('CSOC_ADR');
					$dataProjet['PROJET']['PRJ_CP'] = $crmBDD->get('CSOC_CP');
					$dataProjet['PROJET']['PRJ_VILLE'] = $crmBDD->get('CSOC_VILLE');
					$dataProjet['PROJET']['PRJ_PAYS'] = $crmBDD->get('CSOC_PAYS');
					$this->set('CSOC_SOCIETE', $crmBDD->get('CSOC_SOCIETE'));
				} else $dataProjet['PROJET']['PRJ_PAYS'] = Wish_Session::getInstance()->isRegistered('login_pays') ? Wish_Session::getInstance()->get('login_pays') : getCountryFromCalendar(Wish_Session::getInstance()->get('login_calendrier'));

				//On ajoute les données additionnelles du besoin s'il y en a
				if(($this->get('AO_TARIFADDITIONNEL') != 0 || $this->get('AO_INVESTISSEMENT') != 0) && $tmpAO->getBesoinData($this->get('ID_AO'), 1)) {
					if($tmpAO->getBesoinData($this->get('ID_AO'), 1)) {
						foreach($tmpAO->get('POSITIONDETAILS') as $detail) {
							$dataProjet['PROJET']['PRJ_TARIFADDITIONNEL'] += $detail['POSDETAILS_TARIF'];
							$dataProjet['PROJET']['PRJ_INVESTISSEMENT'] += $detail['POSDETAILS_INVESTISSEMENT'];
							$dataProjet['MISSIONDETAILS'][] = array(0,$dataProjet['PROJET']['PRJ_DEBUT'],1,$detail['POSDETAILS_TITRE'],$detail['POSDETAILS_TARIF'],$detail['POSDETAILS_INVESTISSEMENT']);
						}
					}
				}
				$idprojet = $projetBDD->newProjetData($dataProjet);

				//On récupère le masque du projet
				if($societeBDD->getSocieteData($idAOSociete, 4)) {
					//On construit la référence du projet
					$tabMask = convertArray($societeBDD->get('GRPCONF_MASKREFPROJET'));
					if(!isset($tabMask[$this->get('AO_TYPE')-1])) $tabMask[$this->get('AO_TYPE')-1] = '';
					$tabPrjDate = explode(' ', $dataProjet['PROJET']['PRJ_DATE']);
					$idPRJReference = $projetBDD->getNewReferenceProjet($idprojet, $this->get('ID_AO'), $this->get('AO_REF'), $this->get('CSOC_SOCIETE'), $idAOProfil, $tabMask[$this->get('AO_TYPE')-1], false, $tabPrjDate[0]);
					$projetBDD->updateProjetData(array('PROJET' => array('PRJ_REFERENCE' => $idPRJReference)), $idprojet);
				}
				if($idprojet) BoondManager_Notification_Projet::getInstance($idprojet, 0, ($oldData = array('PRJ_TYPE' => $this->get('AO_TYPE'), 'ID_PROFIL' => $idAOProfil, 'PRJ_REFERENCE' => $idPRJReference)), $dataProjet, $this->db)->create();//Gestion des notifications
			} else {
				$dataMission['MISSION']['MP_DEBUT'] = $dataProjet['PRJ_DEBUT'];
				$dataMission['MISSION']['MP_FIN'] = $dataProjet['PRJ_FIN'];
				$idPRJReference = $dataProjet['PRJ_REFERENCE'];
				if(!$societeBDD->getSocieteData($idAOSociete)) $societeBDD->set('GRPCONF_NBJRSOUVRE', Wish_Session::getInstance()->get('login_nbjrsouvre'));
			}

			if($idprojet) {
				//On crée une mission profil
				$dataMission['MISSION']['ID_PROJET'] = $idprojet;
				$dataMission['MISSION']['ID_ITEM'] = $idprofil;
				$dataMission['MISSION']['ITEM_TYPE'] = ($profilType != -1) ? 0 : 1;
				$dataMission['MISSION']['MP_TARIFADDITIONNEL'] = 0;
				$dataMission['MISSION']['MP_INVESTISSEMENT'] = 0;
				$dataMission['MISSION']['MP_NBJRSOUVRE'] = $societeBDD->get('GRPCONF_NBJRSOUVRE');
				if($profilType != -1 && sizeof($ressourceBDD->isRegistered('CONTRATS')) > 0) {//Si des paramètres de paie ont été défini pour ce profil, on les initialise
					$dataMission['MISSION']['MP_PRJM'] = round($ressourceBDD->get('CTR_CJMCONTRAT',0,'CONTRATS'),2);
					$dataMission['MISSION']['MP_CJM'] = $dataMission['MISSION']['MP_PRJM'];
					$dataMission['MISSION']['MP_NBJRSOUVRE'] = $ressourceBDD->get('CTR_NBJRSOUVRE',0,'CONTRATS');
					$dataMission['MISSION']['MP_DUREEHEBDOMADAIRE'] = $ressourceBDD->get('CTR_DUREEHEBDOMADAIRE',0,'CONTRATS');
					$dataMission['MISSION']['ID_CONTRAT'] = $ressourceBDD->get('ID_CONTRAT',0,'CONTRATS');
				}

				//On met à jours les données de la mission si les données du positionnement sont configurés
				if($posDebut != '') $dataMission['MISSION']['MP_DEBUT'] = $posDebut; else if(isset($groupement['MP_DEBUT'])) $dataMission['MISSION']['MP_DEBUT'] = $groupement['MP_DEBUT'];
				if($posFin != '') $dataMission['MISSION']['MP_FIN'] = $posFin; else if(isset($groupement['MP_FIN'])) $dataMission['MISSION']['MP_FIN'] = $groupement['MP_FIN'];
				if($posTarif != 0 && $this->get('AO_TYPE') != AO_RECRUTEMENT) $dataMission['MISSION']['MP_TARIF'] = $posTarif;
				if($posNbJrsFactures != 0 && $this->get('AO_TYPE') != AO_RECRUTEMENT) $dataMission['MISSION']['MP_NBJRSFACTURE'] = $posNbJrsFactures;
				if($posNbJrsGratuit != 0 && $this->get('AO_TYPE') != AO_RECRUTEMENT) $dataMission['MISSION']['MP_NBJRSGRATUIT'] = $posNbJrsGratuit;

				//On ajoute les données additionnelles du besoin s'il y en a
				foreach($posDetails as $detail) {
					$dataMission['MISSION']['MP_TARIFADDITIONNEL'] += $detail['POSDETAILS_TARIF'];
					$dataMission['MISSION']['MP_INVESTISSEMENT'] += $detail['POSDETAILS_INVESTISSEMENT'];
					$dataMission['MISSIONDETAILS'][] = array(0,isset($dataMission['MISSION']['MP_DEBUT']) ? $dataMission['MISSION']['MP_DEBUT'] : date('Y-m-d', mktime()),1,$detail['POSDETAILS_TITRE'],$detail['POSDETAILS_TARIF'],$detail['POSDETAILS_INVESTISSEMENT']);
				}

				$idnewmissionprofil = $projetBDD->newProjetMission($dataMission);
				if($idnewmissionprofil) {
					if(sizeof($groupement) > 0 && isset($groupement['ID_PROJET']) && $groupement['ID_PROJET'] == $idprojet &&
						isset($groupement['PRJ_TYPE']) && in_array($groupement['PRJ_TYPE'], array(AO_AT,AO_FORFAIT)) &&
						isset($groupement['ITEM_TYPE']) && in_array($groupement['ITEM_TYPE'], array(5,6,7)))
						//On ajoute cette prestation au groupement
						$projetBDD->updateGroupement($groupement['ID_MISSIONPROJET'], $idnewmissionprofil, true);

					if($profilType != -1)
						BoondManager_Notification_Mission::getInstance($idnewmissionprofil, ($oldData = array('ID_PROJET' => $idprojet, 'PRJ_TYPE' => $this->get('AO_TYPE'), 'PRJ_IDPROFIL' => $idAOProfil, 'PRJ_REFERENCE' => $idPRJReference, 'ID_ITEM' => $ressourceBDD->get('ID_PROFIL'), 'ITEM_TYPE' => 0, 'PROFIL_NOM' => $ressourceBDD->get('PROFIL_NOM'), 'PROFIL_PRENOM' => $ressourceBDD->get('PROFIL_PRENOM'))), $dataMission, $this->db, array(false => array($ressourceBDD->get('ID_RESPMANAGER'))))->create();//Gestion des notifications
					else
						BoondManager_Notification_Mission::getInstance($idnewmissionprofil, ($oldData = array('ID_PROJET' => $idprojet, 'PRJ_TYPE' => $this->get('AO_TYPE'), 'PRJ_IDPROFIL' => $idAOProfil, 'PRJ_REFERENCE' => $idPRJReference, 'ID_ITEM' => $produitBDD->get('ID_PRODUIT'), 'ITEM_TYPE' => 1, 'PRODUIT_NOM' => $produitBDD->get('PRODUIT_NOM'))), $dataMission, $this->db, array(true => array($produitBDD->get('ID_PROFIL'))))->create();//Gestion des notifications
				}
				if($profilType != -1) $projetBDD->updateProfilDispo($idprofil, true, false, $solr);	//On met à jour la date de disponibilité de la ressource
				if(!$newprojet) $projetBDD->updateProjetDate($idprojet);	//Si on vient d'ajouter une mission à un projet existant alors on met à jour la date du projet
				if($this->get('AO_ETAT') != 1) {
					//On met à jour l'état du besoin à "Gagné"
					$dataAO = array('AO' => array('AO_ETAT' => 1));
					$this->updateBesoinData($dataAO, $this->get('ID_AO'));
					BoondManager_Notification_Besoin::getInstance($this->get('ID_AO'), 0, $this->getData(), $dataAO, $this->db)->update();//Gestion des notifications
				}
				return $idprojet;
			}
		}
		return false;
	}

	/**
	 *\brief Dupliquer un besoin
	 * @param <type> $data tableau des données du besoin à dupliquer
	 */
	function dupliquerBesoin($data, BoondManager_Devise $modDevise)
	{
		throw new \Exception('do migration'); //TODO
		$newData['AO']['AO_TITLE'] = $data['AO_TITLE'];
		$newData['AO']['AO_TYPE'] = $data['AO_TYPE'];
		$newData['AO']['AO_TYPEREF'] = $data['AO_TYPEREF'];
		$newData['AO']['AO_DEPOT'] = date('Y-m-d');
		$newData['AO']['AO_DEBUT'] = $newData['AO']['AO_DEPOT'];
		$newData['AO']['AO_TYPEDEBUT'] = $data['AO_TYPEDEBUT'];
		$newData['AO']['AO_BUDGET'] = $data['AO_BUDGET'];
		$newData['AO']['AO_LIEU'] = $data['AO_LIEU'];
		$newData['AO']['AO_CORRELATIONPOS'] = $data['AO_CORRELATIONPOS'];
		$newData['AO']['AO_DUREE'] = $data['AO_DUREE'];
		$newData['AO']['AO_CA'] = $data['AO_CA'];
		$newData['AO']['AO_PROBA'] = $data['AO_PROBA'];
		$newData['AO']['AO_APPLICATIONS'] = $data['AO_APPLICATIONS'];
		$newData['AO']['AO_OUTILS'] = $data['AO_OUTILS'];
		$newData['AO']['AO_INTERVENTION'] = $data['AO_INTERVENTION'];
		$newData['AO']['AO_DESCRIPTION'] = $data['AO_DESCRIPTION'];
		$newData['AO']['AO_CRITERES'] = $data['AO_CRITERES'];
		$newData['AO']['AO_VISIBILITE'] = $data['AO_VISIBILITE'];
		$newData['AO']['AO_TYPESOURCE'] = $data['AO_TYPESOURCE'];
		$newData['AO']['AO_SOURCE'] = $data['AO_SOURCE'];
		$newData['AO']['ID_CRMCONTACT'] = 0;
		$newData['AO']['ID_CRMSOCIETE'] = 0;
		$newData['AO']['ID_PROFIL'] = $data['ID_PROFIL'];
		$newData['AO']['ID_SOCIETE'] = $data['ID_SOCIETE'];
		$newData['AO']['ID_POLE'] = $data['ID_POLE'];
		$newData['AO']['AO_ETAT'] = 0;
		$newData['AO']['AO_DEVISE'] = $data['AO_DEVISE'];
		$newData['AO']['AO_CHANGE'] = $data['AO_CHANGE'];
		$newData['AO']['AO_DEVISEAGENCE'] = $modDevise->getDeviseAgence();
		$newData['AO']['AO_CHANGEAGENCE'] = $modDevise->getTauxAgence();

		$idao = $this->newBesoinData($newData);

		//On récupère le masque du besoin
		$societeBDD = new BoondManager_ObjectBDD_Societe($this->getDBinstance());
		if($idao && $societeBDD->getSocieteData($newData['AO']['ID_SOCIETE'], 3)) {
			$tabMask = convertArray($societeBDD->get('GRPCONF_MASKREFAO'));
			if(!isset($tabMask[$newData['AO']['AO_TYPE']-1])) $tabMask[$newData['AO']['AO_TYPE']-1] = '';
			$newData['AO']['AO_REF'] = $this->getNewReferenceBesoin($idao, $newData['AO']['ID_PROFIL'], 0, $tabMask[$newData['AO']['AO_TYPE']-1], false, $newData['AO']['AO_DEPOT']);
			$this->updateBesoinData(array('AO' => array('AO_REF' => $newData['AO']['AO_REF'])), $idao, 0);
		}
		return $idao;
	}
}
