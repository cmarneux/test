<?php
/**
 * order.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use BoondManager\APIs\Orders\Filters\SearchOrders;
use Wish\MySQL\Query;
use Wish\Models\SearchResult;
use Wish\MySQL\Where;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;
use BoondManager\OldModels\Filters;
use BoondManager\Services\Actions;

/**
 * Handle all database work related to orders
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Order extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchOrders $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchOrders(SearchOrders $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$invoiceStates =  Dictionary::getDictTranslatedValues('specific.setting.state.invoice');

		foreach($invoiceStates as $invoiceState)
			$query->addColumns(
				'SUM( CASE WHEN FACT_ETAT="'.$invoiceState.'" THEN ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100) ELSE 0 END)
					/ CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END 
					AS SUM_CA'.$invoiceState
			);

		$query->addColumns(
			'BDC.ID_BONDECOMMANDE, PRJ.ID_PROJET, PRJ.ID_SOCIETE, PRJ.ID_POLE, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF,
			ID_AO, AO_TITLE, TAB_USER.ID_PROFIL, BDC_DATE, BDC_REF, BDC_ETAT,  BDC_ACCORDCLIENT, BDC_REFCLIENT, BDC_MONTANTHT AS TOTAL_MONTANTBDC,
			PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, CSOC_SOCIETE,
			TAB_CRMSOCIETE.ID_CRMSOCIETE, PROFIL_NOM, PROFIL_PRENOM,
			CASE WHEN ID_CORRELATIONBONDECOMMANDE IS NULL THEN PRJ_DEBUT ELSE CASE WHEN TAB_ACHAT.ID_ACHAT IS NULL THEN MIN(MP_DEBUT) ELSE 
			CASE WHEN TAB_MISSIONPROJET.ID_MISSIONPROJET IS NULL THEN MIN(ACHAT_DEBUT) ELSE 
			CASE WHEN MIN(MP_DEBUT) < MIN(ACHAT_DEBUT) THEN MIN(MP_DEBUT) ELSE 
			MIN(ACHAT_DEBUT) END END END END AS BDC_DEBUT, CASE WHEN ID_CORRELATIONBONDECOMMANDE IS NULL THEN PRJ_FIN ELSE 
			CASE WHEN TAB_ACHAT.ID_ACHAT IS NULL THEN MAX(MP_FIN) ELSE CASE WHEN TAB_MISSIONPROJET.ID_MISSIONPROJET IS NULL THEN MAX(ACHAT_FIN) ELSE 
			CASE WHEN MAX(MP_FIN) > MAX(ACHAT_FIN) THEN MAX(MP_FIN) ELSE MAX(ACHAT_FIN) END END END END AS BDC_FIN, 
			SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100))/CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE 
			COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_CAFACTUREHT, 
			SUM(CASE WHEN ID_ITEMFACTURE IS NOT NULL THEN ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100) ELSE 0 END)/
			CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE 
			COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END-BDC_MONTANTHT AS DOCUMENT_DELTA'
		);

		$query->from('TAB_BONDECOMMANDE BDC');

		$query->addJoin('INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=BDC.ID_PROJET) 
		LEFT JOIN TAB_AO USING(ID_AO) 
		LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
		LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
		LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
		LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
		LEFT JOIN TAB_FACTURATION ON(TAB_FACTURATION.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)
		LEFT JOIN TAB_ITEMFACTURE ON(TAB_ITEMFACTURE.ID_FACTURATION=TAB_FACTURATION.ID_FACTURATION)
		LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)
		LEFT JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=0'
		.(($filter->period->getValue() == SearchOrders::PERIOD_PERIOD)?' AND DATEDIFF('.$this->escape($filter->endDate).',MP_DEBUT)>=0 AND 
		DATEDIFF(MP_FIN,'.$this->escape($filter->startDate).')>=0':'').')
		LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_ACHAT=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=1'
		.(($filter->period->getValue() == SearchOrders::PERIOD_PERIOD)?' AND DATEDIFF('.$this->escape($filter->endDate).',ACHAT_DEBUT)>=0 AND 
		DATEDIFF(ACHAT_FIN,'.$this->escape($filter->startDate).')>=0':'').')');

		$query->groupBy('BDC.ID_BONDECOMMANDE');

		$where = new Where('PRJ.PRJ_TYPE>0');
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'TAB_USER.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['BDC' => 'BDC.ID_BONDECOMMANDE','PRJ'=>'PRJ.ID_PROJET','CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT',
		'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('BDC_REF LIKE ?', $likeSearch)
						  ->or_('BDC_REFCLIENT LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_( $this->getFilterSearch($filter->states->getValue(), 'BDC_ETAT') )
				->and_( $this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF') )
				->and_( $this->getFilterSearch($filter->paymentMethods->getValue(), 'BDC_TYPEPAYMENT') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		switch($filter->period->getValue()) {
			case SearchOrders::PERIOD_CREATED:
				$query->addWhere('BDC_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			case SearchOrders::PERIOD_PERIOD:
				$query->addWhere('((ID_CORRELATIONBONDECOMMANDE IS NULL AND DATEDIFF(PRJ.PRJ_FIN,?)>=0 AND 
				DATEDIFF(?,PRJ.PRJ_DEBUT)>=0) OR (ID_CORRELATIONBONDECOMMANDE IS NOT NULL AND 
				(TAB_MISSIONPROJET.ID_MISSIONPROJET IS NOT NULL OR TAB_ACHAT.ID_ACHAT IS NOT NULL)))',
				[$filter->startDate->getValue(), $filter->endDate->getValue()]);
				break;
			default:case BM::INDIFFERENT:break;
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		$result = $this->launchSearch($query);

		$result->TOTAL_CAFACTUREHT = $result->TOTAL_MONTANTBDC = $result->DOCUMENT_DELTA = 0;
		if($result->total > 0){
			foreach($result->rows as $order) $result->DOCUMENT_DELTA += $order->DOCUMENT_DELTA;
			//On ajoute les CA des missions au TOTAL
			$caBDCQuery = (new Query())
			->addColumns('SUM(BDC_MONTANTHT)/CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE 
			COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_MONTANTBDC')
			->addJoin('TAB_BONDECOMMANDE BDC
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=BDC.ID_PROJET)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)
			LEFT JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=0'
			.(($filter->period->getValue() == SearchOrders::PERIOD_PERIOD)?' AND DATEDIFF('.$this->escape($filter->endDate).',MP_DEBUT)>=0 AND 
			DATEDIFF(MP_FIN,'.$this->escape($filter->startDate).')>=0':'').')
			LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_ACHAT=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=1'
			.(($filter->period->getValue() == SearchOrders::PERIOD_PERIOD)?' AND DATEDIFF('.$this->escape($filter->endDate).',ACHAT_DEBUT)>=0 AND 
			DATEDIFF(ACHAT_FIN,'.$this->escape($filter->startDate).')>=0':'').') 
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)')
			->addWhere($where)
			->groupBy('BDC.ID_BONDECOMMANDE');

			if(sizeof($filter->flags->getValue()) > 0) $caBDCQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			if($filter->period->getValue() != SearchOrders::PERIOD_PERIOD) $caBDCQuery->addWhere(new Where('(ID_CORRELATIONBONDECOMMANDE IS NULL OR 
			(ID_CORRELATIONBONDECOMMANDE IS NOT NULL AND (TAB_MISSIONPROJET.ID_MISSIONPROJET IS NOT NULL OR TAB_ACHAT.ID_ACHAT IS NOT NULL)))'));
			$caBDC = $this->exec($caBDCQuery);
			foreach($caBDC as $ca) $result->TOTAL_MONTANTBDC += $ca->TOTAL_MONTANTBDC;

			//On calcule le CA Facturé des projets (On distingue les projets corrélés aux missions des autres)
			$caPRJQuery = (new Query())
			->addColumns('SUM(ITEM_MONTANTHT*ITEM_QUANTITE*(1-FACT_TAUXREMISE/100))/CASE 
			WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_CAFACTUREHT')
			->addJoin('TAB_BONDECOMMANDE BDC
			INNER JOIN TAB_PROJET PRJ ON(PRJ.ID_PROJET=BDC.ID_PROJET)
			INNER JOIN TAB_FACTURATION ON(TAB_FACTURATION.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE) 
			INNER JOIN TAB_ITEMFACTURE ON(TAB_ITEMFACTURE.ID_FACTURATION=TAB_FACTURATION.ID_FACTURATION)
			LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=BDC.ID_BONDECOMMANDE)
			LEFT JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=0'
			.(($filter->period->getValue() == SearchOrders::PERIOD_PERIOD)?' AND DATEDIFF('.$this->escape($filter->endDate).',MP_DEBUT)>=0 AND 
			DATEDIFF(MP_FIN,'.$this->escape($filter->startDate).')>=0':'').')
			LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_ACHAT=TAB_CORRELATIONBONDECOMMANDE.ID_ITEM AND CORBDC_TYPE=1'
			.(($filter->period->getValue() == SearchOrders::PERIOD_PERIOD)?' AND DATEDIFF('.$this->escape($filter->endDate).',ACHAT_DEBUT)>=0 AND 
			DATEDIFF(ACHAT_FIN,'.$this->escape($filter->startDate).')>=0':'').') 
			LEFT JOIN TAB_USER ON(TAB_USER.ID_USER=BDC.ID_RESPUSER) 
			LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_USER.ID_PROFIL)
			LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT
			LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)')
			->addWhere($where)
			->groupBy('BDC.ID_BONDECOMMANDE');

			if(sizeof($filter->flags->getValue()) > 0) $caPRJQuery->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_ORDER.' AND 
			TAB_FLAG.ID_PARENT=BDC.ID_BONDECOMMANDE)');
			if($filter->period->getValue() != SearchOrders::PERIOD_PERIOD) $caPRJQuery->addWhere(new Where('(ID_CORRELATIONBONDECOMMANDE IS NULL OR 
			(ID_CORRELATIONBONDECOMMANDE IS NOT NULL AND (TAB_MISSIONPROJET.ID_MISSIONPROJET IS NOT NULL OR TAB_ACHAT.ID_ACHAT IS NOT NULL)))'));
			$caPRJ = $this->exec($caPRJQuery);
			foreach($caPRJ as $ca) $result->TOTAL_CAFACTUREHT += $ca->TOTAL_CAFACTUREHT;
		}

		$searchResult = new Models\SearchResults\Orders($result->rows, $result->total);
		$searchResult->turnoverInvoicedExcludingTax = $result->TOTAL_CAFACTUREHT;
		$searchResult->turnoverOrderedExcludingTax = $result->TOTAL_MONTANTBDC;
		$searchResult->deltaInvoicedExcludingTax = $result->TOTAL_CAFACTUREHT - $result->TOTAL_MONTANTBDC;

		return $searchResult;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchOrders::ORDERBY_CREATIONDATE                => 'BDC_DATE',
			SearchOrders::ORDERBY_REFERENCE                   => 'BDC_REF',
			SearchOrders::ORDERBY_STATE                       => 'BDC_ETAT',
			SearchOrders::ORDERBY_PROJECT_REFERENCE           => 'PRJ_REFERENCE',
			SearchOrders::ORDERBY_CUSTOMERAGREEMENT           => 'BDC_ACCORDCLIENT',
			SearchOrders::ORDERBY_TURNOVERINVOICEDINCLUDEDTAX => 'TOTAL_CAFACTUREHT',
			SearchOrders::ORDERBY_TURNOVERORDEREDEXCLUDEDTAX  => 'TOTAL_MONTANTBDC',
			SearchOrders::ORDERBY_DELTAINVOICEDEXCLUDEDTAX    => 'DOCUMENT_DELTA',
			SearchOrders::ORDERBY_PROJECT_COMPANY_NAME        => 'CSOC_SOCIETE',
			SearchOrders::ORDERBY_MAINMANAGER_LASTNAME        => 'PROFIL_NOM',
		];

		if(!$column) $query->addOrderBy('BDC.BDC_DATE DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('BDC.ID_BONDECOMMANDE DESC');
	}

	/**
	 * Search for all not archived mensual orders:
	 *  - associated with the list of projects available for a resource on period
	 *  - not yet correlated with the list of projects
	 *
	 * v6 > TpsFrsAbs->getBDCMensuelPrj
	 *
	 * @param $resourceId
	 * @param $projectIds
	 * @param $startDate
	 * @param $endDate
	 *
	 * @return Models\Order[]
	 */
	public function searchMensualOrdersForProjectsOfAResourceOnPeriod($resourceId, $projectIds, $startDate, $endDate){
		if(!$projectIds) return [];
		return $this->exec('
			SELECT TAB_BONDECOMMANDE.ID_RESPUSER, BDC_REF, PRJ_REFERENCE, TAB_PROJET.ID_PROJET, TAB_PROJET.ID_SOCIETE, TAB_PROJET.ID_POLE,
				TAB_BONDECOMMANDE.ID_BONDECOMMANDE, CSOC_SOCIETE, BDC_SEPARATETPSFRS, COUNT(ID_CORRELATIONBONDECOMMANDE) AS NB_CORRELATIONS
			FROM TAB_BONDECOMMANDE
				INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_BONDECOMMANDE.ID_PROJET)
				INNER JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=TAB_BONDECOMMANDE.ID_BONDECOMMANDE)
				INNER JOIN TAB_MISSIONPROJET ON(TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=ID_MISSIONPROJET AND CORBDC_TYPE=0)
				LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT)
				LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
			WHERE BDC_ETAT<>0 AND TAB_PROJET.ID_PROJET IN('.Where::prepareWhereIN($projectIds).') AND TAB_MISSIONPROJET.ID_ITEM=?
				AND ITEM_TYPE=0 AND DATEDIFF(?, MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,?)>=0
			GROUP BY TAB_BONDECOMMANDE.ID_BONDECOMMANDE
			UNION ALL
			SELECT TAB_BONDECOMMANDE.ID_RESPUSER, BDC_REF, PRJ_REFERENCE, TAB_PROJET.ID_PROJET, TAB_PROJET.ID_SOCIETE, TAB_PROJET.ID_POLE,
				TAB_BONDECOMMANDE.ID_BONDECOMMANDE, CSOC_SOCIETE, BDC_SEPARATETPSFRS, COUNT(ID_CORRELATIONBONDECOMMANDE) AS NB_CORRELATIONS
			FROM TAB_BONDECOMMANDE
				INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_BONDECOMMANDE.ID_PROJET)
				LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT)
				LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
				LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_BONDECOMMANDE=TAB_BONDECOMMANDE.ID_BONDECOMMANDE)
			WHERE BDC_ETAT<>0 AND TAB_PROJET.ID_PROJET IN('.Where::prepareWhereIN($projectIds).') AND ID_CORRELATIONBONDECOMMANDE IS NULL
			GROUP BY TAB_BONDECOMMANDE.ID_BONDECOMMANDE
			', array_merge($projectIds, [$resourceId, $startDate, $endDate], $projectIds));
	}

	/**
	 * delete All data related to orders
	 * @param int $id order ID
	 */
	public function deleteOrder($id) {
		//On supprime toutes les factures
		$billings = $this->exec('SELECT ID_FACTURATION FROM TAB_FACTURATION WHERE ID_BONDECOMMANDE=?', $id);
		$dbInvoice = new Invoice();
		foreach($billings as $bill) $dbInvoice->deleteInvoice($bill->ID_FACTURATION);

		//On supprime les actions associés à cette commande
		$dict = Actions::getDictionaryForCategory(BM::CATEGORY_ORDER);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id') );
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//On supprime tous les documents de cette commande
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_DOCUMENT, $id, Models\Document::TYPE_ORDER);

		//On supprime tous les flags de cette action
		$dbFlag = new AttachedFlag();
		$dbFlag->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_ORDER);

		//On supprime toutes les échéances
		$this->delete('TAB_ECHEANCIER', 'ID_BONDECOMMANDE=?', $id);

		//On supprime toutes les corrélations de cette commande
		$this->delete('TAB_CORRELATIONBONDECOMMANDE', 'ID_BONDECOMMANDE=?', $id);

		//On supprime toutes commande
		$this->delete('TAB_BONDECOMMANDE', 'ID_BONDECOMMANDE=?', $id);
	}

	public function getReferenceCommande($idcommande) {
		return 'BM'.(1000000000000+$idcommande);
	}

	/**
	 * Retrieve an order's data
	 * @param int $id Order's id
	 * @param string $onglet
	 */
	public function getOrder($id, $onglet = BM::TAB_DEFAULT, $activepage = 1, &$tabFilters = array(), $nbmaxresults = TABLEAU_NBRESULTATS_DEFAULT)
	{
		$baseQuery = new Query();

		$baseQuery->select([
			'ID_BONDECOMMANDE', 'ID_PROJET', 'ID_RESPUSER', 'TAB_PROJET.ID_SOCIETE', 'TAB_PROJET.ID_POLE', 'PRJ_TYPE', 'PRJ_TYPEREF',
			'PRJ_DEVISEAGENCE', 'PRJ_CHANGEAGENCE', 'PRJ_DEVISE', 'PRJ_CHANGE', 'PRJ_DEBUT', 'PRJ_FIN', 'BDC_REFCLIENT', 'BDC_REF',
			'BDC_TYPEREGLEMENT', 'BDC_SEPARATETPSFRS', 'BDC_MONTANTHT'
		])
			->from('TAB_BONDECOMMANDE')
			->addJoin('INNER JOIN TAB_PROJET USING (ID_PROJET)')
			->addWhere('ID_BONDECOMMANDE = ?', $id);

		switch($onglet) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Order::TAB_INFORMATION:

				$baseQuery->addColumns([
					'AO_TITLE', 'PRJ_REFERENCE', 'TAB_CRMCONTACT.ID_CRMSOCIETE', 'SOC.ID_CRMSOCIETE', 'SOC.CSOC_SOCIETE', 'SOC.CSOC_NUMERO',
					'TAB_PROJET.ID_CRMCONTACT', 'TAB_PROJET.ID_AO', 'AO_REF', 'AO_TITLE', 'CCON_NOM', 'CCON_PRENOM',
					'SOC.CSOC_TVA', 'ID_COORDONNEES', 'FACTOR_SOCIETE' => 'FACTOR.CSOC_SOCIETE' , 'ID_FACTOR' => 'TAB_BONDECOMMANDE.ID_CRMSOCIETE',
					'BDC_ETAT', 'BDC_DATE', 'BDC_TYPEPAYMENT', 'BDC_CONDREGLEMENT', 'BDC_TAUXTVA', 'BDC_COMMENTAIRE', 'BDC_LANGUE',
					'BDC_ACCORDCLIENT', 'BDC_GROUPMISSION', 'BDC_COPYCOMMENTS', 'ID_RIB', 'RIB_DESCRIPTION', 'RIB_IBAN', 'RIB_BIC',
					'BDC_SHOWINTNAME', 'BDC_SHOWTARIFJRS', 'BDC_SHOWRIB', 'BDC_SHOWFOOTER', 'BDC_SHOWCOMMENTS', 'BDC_SHOWPRJREFERENCE',
					'BDC_SHOWJRSOUVRES', 'BDC_AUTOCREATION', 'BDC_SHOWFACTOR', 'BDC_SHOWNUMBER', 'BDC_SEPARATETPSFRS', 'BDC_SHOWTVAIC', 'BDC_MENTIONS',
					'TAB_PROJET.ID_SOCIETE', 'SOCIETE_GROUPE', 'SOCIETE_ADR', 'SOCIETE_VILLE', 'SOCIETE_RAISON', 'SOCIETE_CP',
					'SOCIETE_PAYS', 'SOCIETE_TVA', 'SOCIETE_STATUT', 'SOCIETE_RCS'
				]);
				$baseQuery->addJoin('LEFT JOIN TAB_AO USING (ID_AO)')
					->addJoin('LEFT JOIN TAB_RIB USING (ID_RIB)')
					->addJoin('LEFT JOIN TAB_SOCIETE ON (TAB_SOCIETE.ID_SOCIETE=TAB_PROJET.ID_SOCIETE)')
					->addJoin('LEFT JOIN TAB_CRMSOCIETE AS FACTOR ON(FACTOR.ID_CRMSOCIETE=TAB_BONDECOMMANDE.ID_CRMSOCIETE)')
					->addJoin('LEFT JOIN TAB_CRMCONTACT ON TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT ')
					->addJoin('LEFT JOIN TAB_CRMSOCIETE SOC ON(SOC.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)');

				$result = $this->singleExec($baseQuery);
				if($result) {
					//On récupère tous les documents
					$sql = 'SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT= ? AND DOC_TYPE=3';
					$result['FILES'] = $this->exec($sql, $id);

					//On récupère tous les RIB de la société
					$sql = 'SELECT ID_RIB, RIB_DESCRIPTION, RIB_IBAN, RIB_BIC FROM TAB_RIB WHERE RIB_TYPE=0 AND ID_PARENT=?';
					$result['RIBS'] = $this->exec($sql, $result['ID_SOCIETE']);

					//On récupère toutes les coordonnées de facturation de la société
					if(isset($result['ID_CRMSOCIETE'])) {
						$sql = 'SELECT 
						            ID_COORDONNEES, COORD_NOM, COORD_CONTACT, COORD_EMAIL, COORD_EMAIL2, COORD_EMAIL3, COORD_TEL, 
						            COORD_ADR1, COORD_ADR2, COORD_ADR3, COORD_CP, COORD_VILLE, COORD_PAYS, COORD_ETAT 
						        FROM TAB_COORDONNEES 
						        WHERE ID_PARENT = ? AND COORD_TYPE=0 ORDER BY ID_COORDONNEES ASC';
						$result['FACT_COORDONNEES'] = $this->exec($sql, $result['ID_CRMSOCIETE']);
					} else {
						$result['FACT_COORDONNEES'] = [];
					}
				}
				break;
			case 1://Factures
				$sql = 'SELECT ID_BONDECOMMANDE, ID_PROJET, ID_RESPUSER, TAB_PROJET.ID_SOCIETE, TAB_PROJET.ID_POLE, PRJ_TYPE, PRJ_TYPEREF, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, PRJ_DEBUT, PRJ_FIN, BDC_REFCLIENT, BDC_REF, BDC_TYPEREGLEMENT, BDC_SEPARATETPSFRS, BDC_MONTANTHT
                        FROM TAB_BONDECOMMANDE INNER JOIN TAB_PROJET USING(ID_PROJET)
                        WHERE ID_BONDECOMMANDE="'.$this->db->escape($id).'"';
				$result = $this->db->query_first($sql);
				if($result) {
					$bourseBDD = new BoondManager_ObjectBDD_Recherche_Facturation($this->db);
					$filters_array = $bourseBDD->getDefaultFilters(false);
					$filters_array['perimetre'] = '';
					$filters_array['keywords'] = 'BDC'.$result['ID_BONDECOMMANDE'];
					$filters_array['type_document'] = '0';
					$filters_array['order_colonne'] = 'date';
					$filters_array['order_type'] = 'desc';
					$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
					$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
					$result['NB_FACTURES'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
					$result['FACTURES'] = ($search)?$bourseBDD->get('RESULTS'):array();
					$result['TOTAL_CAFACTUREHT'] = ($search)?$bourseBDD->get('TOTAL_CAFACTUREHT'):0;
					$result['TOTAL_CAFACTURETTC'] = ($search)?$bourseBDD->get('TOTAL_CAFACTURETTC'):0;
					foreach(Wish_Session::getInstance()->get('login_clefetatsfacture')+array('10' => 'ProForma') as $id => $etat)
						if($etat != '') {
							$result['TOTAL_CAFACTUREHT_ETAT'.$id] = ($search)?$bourseBDD->get('TOTAL_CAFACTUREHT_ETAT'.$id):0;
							$result['TOTAL_CAFACTURETTC_ETAT'.$id] = ($search)?$bourseBDD->get('TOTAL_CAFACTURETTC_ETAT'.$id):0;
						}

					//On initialise la navigation de la vue
					if($search) $bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-commande', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_BONDECOMMANDE'].'&onglet=1', 'factures_');
					$tabFilters = $filters_array;
				}
				break;
			case 2://Actions
				$sql = 'SELECT ID_BONDECOMMANDE, ID_PROJET, ID_RESPUSER, TAB_PROJET.ID_SOCIETE, TAB_PROJET.ID_POLE, PRJ_TYPE, PRJ_TYPEREF, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, PRJ_DEBUT, PRJ_FIN, BDC_REFCLIENT, BDC_REF, BDC_TYPEREGLEMENT, BDC_SEPARATETPSFRS, BDC_MONTANTHT
                        FROM TAB_BONDECOMMANDE INNER JOIN TAB_PROJET USING(ID_PROJET)
                        WHERE ID_BONDECOMMANDE="'.$this->db->escape($id).'"';
				$result = $this->db->query_first($sql);
				if($result) {
					$bourseBDD = new BoondManager_ObjectBDD_Recherche_Actions($this->db);
					$filters_array = $bourseBDD->getDefaultFilters(false);
					$filters_array['perimetre'] = '';
					$filters_array['keywords'] = 'BDC'.$result['ID_BONDECOMMANDE'];
					$filters_array['order_colonne'] = 'date';
					$filters_array['order_type'] = 'desc';
					$filters_array['page_active'] = ($activepage != 0)?$activepage:1;
					$filters_array['type_rappel'] = '-1';//On recherche les actions des commandes ET factures
					if(isset($tabFilters['mc']['liste_rappel']) && is_array($tabFilters['mc']['liste_rappel'])) $filters_array['mc']['liste_rappel'] = $tabFilters['mc']['liste_rappel']; else foreach((Wish_Session::getInstance()->get('login_clefactionscommande')+Wish_Session::getInstance()->get('login_clefactionsfacture')) as $idaction => $action) $filters_array['mc']['liste_rappel'][] = $idaction;
					$search = $bourseBDD->recherche($filters_array, $nbmaxresults);
					$result['NB_ACTIONS'] = ($search)?$bourseBDD->get('NB_ROWS'):0;
					$result['ACTIONS'] = ($search)?$bourseBDD->get('RESULTS'):array();

					//On initialise la navigation de la vue
					if($search) $bourseBDD->updateNavLinks($filters_array['page_active'], 'tableau-de-bord/fiche-commande', '', $bourseBDD->get('NB_ROWS'), TABLEAU_NBRESULTATS_DEFAULT, '&id='.$result['ID_BONDECOMMANDE'].'&onglet=2&'.setURLMultiCriteria('lr', $filters_array['mc']['liste_rappel']), 'actions_');
					$tabFilters = $filters_array;
				}
				break;
			default :
				$result = $this->singleExec($baseQuery);
		}

		if(!$result) return false;

		//On récupère toutes les échéances
		$sql = 'SELECT ID_ECHEANCIER, ECH_DATE, ECH_DESCRIPTION, ECH_MONTANTHT, COUNT(DISTINCT ID_FACTURATION) AS NB_FACTURE, ECH_QUOTA 
		        FROM TAB_ECHEANCIER 
		        LEFT JOIN TAB_FACTURATION USING(ID_ECHEANCIER) 
		        WHERE TAB_ECHEANCIER.ID_BONDECOMMANDE = ? 
		        GROUP BY TAB_ECHEANCIER.ID_ECHEANCIER 
		        ORDER BY TAB_ECHEANCIER.ID_ECHEANCIER ASC';
		$result['ECHEANCIER'] = $this->exec($sql, $id);

		/*
		 * Requete scindée en deux
		 *
		 *
		//On récupère toutes les missions du projet
		$sql = 'SELECT
		            0 AS ITEM_TYPE, CONCAT(\'MIS\',ID_MISSIONPROJET) AS ITEM_REF, ID_MISSIONPROJET AS ID_ITEM,
		            CASE WHEN ITEM_TYPE=0 THEN CONCAT(PROFIL_NOM,\' \',PROFIL_PRENOM) ELSE PRODUIT_NOM END AS ITEM_TITRE,
		            MP_DEBUT AS ITEM_DEBUT, MP_FIN AS ITEM_FIN
                FROM TAB_MISSIONPROJET
                LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=0)
                LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=1)
                WHERE ID_PROJET = :idprojet AND ITEM_TYPE IN(0,1)
                UNION ALL
                SELECT
                    1 AS ITEM_TYPE, CONCAT(\'ACH\',ID_ACHAT) AS ITEM_REF, ID_ACHAT AS ID_ITEM,
                    ACHAT_TITLE AS ITEM_TITRE, ACHAT_DEBUT AS ITEM_DEBUT, ACHAT_FIN AS ITEM_FIN
                FROM TAB_ACHAT
                WHERE ID_PROJET = :idprojet
                ORDER BY ITEM_TYPE ASC, ID_ITEM DESC';
		$result['MISSIONSVENTESACHATS'] = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);
		*/

		//On récupère toutes les missions du projet
		$sql = 'SELECT ID_MISSIONPROJET, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, ID_PRODUIT, PRODUIT_NOM, MP_DEBUT, MP_FIN, MP_NOM
                FROM TAB_MISSIONPROJET
                LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=0)
                LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=1)
                WHERE ID_PROJET = :idprojet AND ITEM_TYPE IN(0,1)
                ORDER BY ID_MISSIONPROJET DESC';
		$result['MISSIONS'] = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);

		//On récupère toutes les missions du projet
		$sql = 'SELECT ID_ACHAT, ACHAT_TITLE, ACHAT_DEBUT, ACHAT_FIN, ACHAT_REF FROM TAB_ACHAT WHERE ID_PROJET = :idprojet ORDER BY ID_ACHAT DESC';
		$result['VENTESACHATS'] = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);

		//On cherche toutes les missions/ventes/Achats corrélés à ce bon de commande
		$sql = 'SELECT ID_CORRELATIONBONDECOMMANDE, ID_ITEM, CORBDC_TYPE FROM TAB_CORRELATIONBONDECOMMANDE WHERE ID_BONDECOMMANDE = ? ORDER BY ID_ITEM ASC';
		$result['CORRELATIONS'] = $this->exec($sql, $id);
		return $result;

	}
}
