<?php
/**
 * payment.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Payments\Filters\SearchPayments;
use Wish\MySQL\Where;
use BoondManager\Services\BM;
use Wish\MySQL\Query;
use BoondManager\Models;

/**
 * Handle all database work related to payments
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Payment extends AbstractObject {

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchPayments $filter
	 * @return Models\SearchResults\Payments
	 */
	public function searchPayments(SearchPayments $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('PRC.ID_ACHAT, RESP.ID_PROFIL, RESP.PROFIL_NOM, RESP.PROFIL_PRENOM, ID_USER, PRJ_REFERENCE, PRC.ID_PROJET, PRC.ID_SOCIETE, PRC.ID_POLE,
		ACHAT_MONTANTHT*ACHAT_QUANTITE AS TOTAL_MONTANT, ACHAT_REFACTURE, ACHAT_TARIFFACTURE, ACHAT_TITLE, ACHAT_REF, ACHAT_REFFOURNISSEUR,
		ACHAT_ETAT, ACHAT_TYPE, ACHAT_CATEGORIE, ACHAT_DATE, ACHAT_MONTANTHT, ACHAT_QUANTITE, ACHAT_TAUXTVA, ACHAT_CONDREGLEMENT, ACHAT_DEVISE,
		ACHAT_CHANGE, ACHAT_DEVISEAGENCE, ACHAT_CHANGEAGENCE, ACHAT_DEBUT, ACHAT_FIN, AT.ID_PROFIL AS EXT_ID, AT.PROFIL_NOM AS EXT_NOM,
		AT.PROFIL_PRENOM AS EXT_PRENOM, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, TAB_CRMSOCIETE.ID_CRMSOCIETE, CSOC_SOCIETE,
		COUNT(DISTINCT ID_BONDECOMMANDE) AS NB_COR, ID_JUSTIFICATIF, COUNT(ID_JUSTIFICATIF) AS NB_JUSTIFICATIF, PMT_MONTANTHT, PMT_ETAT, ID_PAIEMENT,
		PMT_COMMENTAIRES, PMT_DATE, PMT_DEBUT, PMT_FIN, PMT_DATEPAIEMENTATTENDU, PMT_DATEPAIEMENTEFFECTUE, PMT_TAUXTVA, PMT_REFPROVIDER,
		PMT_MONTANTHT*(1+PMT_TAUXTVA/100) AS PMT_MONTANTTTC');
		$query->from('TAB_ACHAT PRC');
		$query->addJoin('INNER JOIN TAB_PAIEMENT PMT ON(PMT.ID_ACHAT=PRC.ID_ACHAT)
						LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=PRC.ID_PROFIL)
						LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)
						LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=PRC.ID_CRMCONTACT)
						LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRC.ID_CRMSOCIETE)
						LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=PRC.ID_PROJET)
						LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=PRC.ID_ACHAT AND CORBDC_TYPE=1)
						LEFT JOIN TAB_MISSIONPROJET MP ON(MP.ID_ACHAT=PRC.ID_ACHAT AND MP.ID_PROJET=TAB_PROJET.ID_PROJET)
						LEFT JOIN TAB_PROFIL AT ON(AT.ID_PROFIL=MP.ID_ITEM AND MP.ITEM_TYPE=0)
						LEFT JOIN TAB_JUSTIFICATIF ON(TAB_JUSTIFICATIF.ID_PARENT=ID_PAIEMENT AND JUSTIF_TYPE=3)');
		$query->groupBy('PMT.ID_PAIEMENT');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRC.ID_SOCIETE',
			$colPole = 'PRC.ID_POLE',
			$colUser = 'PRC.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['ACH'=>'PRC.ID_ACHAT', 'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE', 'PRJ'=>'PRC.ID_PROJET', 'COMP'=>'AT.ID_PROFIL'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('MATCH(ACHAT_TITLE) AGAINST(?)', $keywords);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('ACHAT_REF LIKE ?', $likeSearch)
						  ->or_('ACHAT_REFFOURNISSEUR LIKE ?', $likeSearch)
						  ->or_('PMT_REFPROVIDER LIKE ?', $likeSearch);

			$where->and_($whereKeywords);
		}

		$where->and_( $this->getFilterSearch($filter->paymentStates->getValue(), 'PMT_ETAT') )
				->and_( $this->getFilterSearch($filter->subscriptionTypes->getValue(), 'ACHAT_TYPE') )
				->and_( $this->getFilterSearch($filter->purchaseTypes->getValue(), 'ACHAT_CATEGORIE') )
				->and_( $this->getFilterSearch($filter->paymentMethods->getValue(), 'ACHAT_TYPEPAYMENT') );

        switch ($filter->period->getValue()) {
            case SearchPayments::PERIOD_CREATED_PURCHASE:
                $query->addWhere('ACHAT_DATE BETWEEN ? AND ?', $filter->getDatesPeriod());
                break;
            case SearchPayments::PERIOD_SUBSCRIPTION:
                $query->addWhere('DATEDIFF(ACHAT_FIN,?)>=0 && DATEDIFF(?,ACHAT_DEBUT)>=0', $filter->getDatesPeriod());
                break;
            case SearchPayments::PERIOD_CREATED:
                $query->addWhere('PMT_DATE BETWEEN ? AND ?', $filter->getDatesPeriod());
                break;
            default:case BM::INDIFFERENT:break;
        }

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PURCHASE.' AND TAB_FLAG.ID_PARENT=PRC.ID_ACHAT)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		/**
		 * @var Models\SearchResults\Payments $result
		 */
        $result = $this->launchSearch($query, Models\SearchResults\Payments::class);

		$result->TOTAL_PAIEMENTSHT = $result->TOTAL_PAIEMENTSTTC = 0;
		$paymentStates =  $filter->paymentStates->getListOfValidValues();
		foreach($paymentStates as $state){
			$result->__set('TOTAL_PMTHT'.$state, 0);
			$result->__set('TOTAL_PMTTTC'.$state, 0);
		}
        if($result->total > 0){
			$sumPmt = [];
            foreach($paymentStates as $state){
				$sumPmt[] = 'SUM(CASE WHEN PMT_ETAT="'.$state.'" THEN PMT_MONTANTHT ELSE 0 END) AS TOTAL_PMTHT'.$state.', SUM(CASE WHEN PMT_ETAT="'.$state.'" THEN PMT_MONTANTHT*(1+PMT_TAUXTVA/100) ELSE 0 END) AS TOTAL_PMTTTC'.$state;
			}
            $total_paiements = $this->singleExec('SELECT SUM(PMT_MONTANTHT) AS TOTAL_PAIEMENTSHT, SUM(PMT_MONTANTHT*(1+PMT_TAUXTVA/100)) AS TOTAL_PAIEMENTSTTC'.((sizeof($sumPmt) > 0)?', '.implode(',',$sumPmt):'').' FROM (SELECT DISTINCT ID_PAIEMENT, PMT_DATE, PMT_MONTANTHT, PMT_COMMENTAIRES, PMT_ETAT, PMT_TAUXTVA FROM '.$query->getFrom().' '. $query->buildJoins().' '.$query->buildWhere().') TAB_PAIEMENT', $query->getArgs());
            if($total_paiements){
				foreach($paymentStates as $state){
					$result->addStateAmount($state, $total_paiements['TOTAL_PMTHT'.$state], $total_paiements['TOTAL_PMTTTC'.$state]);
				}
				$result->TOTAL_PAIEMENTSHT += $total_paiements->TOTAL_PAIEMENTSHT;//La valeur n'est pas castée en float si on n'utilise pas +=
				$result->TOTAL_PAIEMENTSTTC += $total_paiements->TOTAL_PAIEMENTSTTC;
			}
        }

        return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order)
	{
		$mapping = [
			SearchPayments::ORDERBY_CREATIONDATE         => 'PMT_DATE',
			SearchPayments::ORDERBY_STATE                => 'PMT_ETAT',
			SearchPayments::ORDERBY_EXPECTEDDATE         => 'PMT_DATEPAIEMENTATTENDU',
			SearchPayments::ORDERBY_PERFORMEDDATE        => 'PMT_DATEPAIEMENTEFFECTUE',
			SearchPayments::ORDERBY_PURCHASE_TITLE       => 'ACHAT_TITLE',
			SearchPayments::ORDERBY_PROJECT_REFERENCE    => 'PRJ_REFERENCE',
			SearchPayments::ORDERBY_COMPANY_NAME         => 'CSOC_SOCIETE',
			SearchPayments::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM'
		];

		if(!$column) $query->addOrderBy('PMT_DATE DESC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('ID_PAIEMENT DESC');
	}

	/**
	 * Retrieves entity data from its ID
	 * @param  integer  $id  Payment ID.
	 * @return Models\Payment|false
	 */
	public function getObject($id)
	{
		$sql = 'SELECT ID_PAIEMENT, PMT_DATE, PMT_DEBUT, PMT_FIN, PMT_DATEPAIEMENTATTENDU, PMT_DATEPAIEMENTEFFECTUE, PMT_REFPROVIDER,
					PMT_MONTANTHT, PMT_TAUXTVA, PMT_MONTANTHT*(1+PMT_TAUXTVA/100) AS PMT_MONTANTTTC, PMT_TYPEPAYMENT, PMT_ETAT, PMT_COMMENTAIRES,
					TAB_ACHAT.ID_ACHAT, TAB_ACHAT.ID_PROJET, PRJ_REFERENCE, ACHAT_TITLE, ACHAT_DATE, ACHAT_REF, ACHAT_REFFOURNISSEUR,
					ACHAT_TYPE, ACHAT_ETAT, ACHAT_MONTANTHT, ACHAT_QUANTITE, ACHAT_DEVISE, ACHAT_CHANGE, ACHAT_DEVISEAGENCE, ACHAT_CHANGEAGENCE,
					ACHAT_TAUXTVA, ACHAT_CONDREGLEMENT,	ACHAT_DEBUT, ACHAT_FIN, ACHAT_COMMENTAIRE, TAB_ACHAT.ID_CRMCONTACT, CCON_NOM,
					CCON_PRENOM, CSOC_SOCIETE, TAB_ACHAT.ID_CRMSOCIETE, TAB_ACHAT.ID_PROFIL, TAB_ACHAT.ID_SOCIETE, TAB_ACHAT.ID_POLE,
					TAB_MISSIONPROJET.ID_MISSIONPROJET, MP_NOM
				FROM TAB_PAIEMENT
				INNER JOIN TAB_ACHAT USING(ID_ACHAT)
				LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_ACHAT.ID_CRMCONTACT)
				LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_ACHAT.ID_CRMSOCIETE)
				LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_ACHAT.ID_PROFIL)
				LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_ACHAT.ID_PROJET)
				LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_ACHAT=TAB_ACHAT.ID_ACHAT)
				WHERE ID_PAIEMENT=:id';
		$result = $this->singleExec($sql, ['id' => $id]);

		if(!$result) return false;

		//On récupère tous les justificatifs
		$result->FILE = $this->exec('SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF
									  WHERE ID_PARENT= :id  AND JUSTIF_TYPE=:type',
			['id'=>$id, 'type'=> Models\Proof::TYPE_PAYMENT]);

		return $result;
	}

	/**
	 * Sets a payment
	 * @param array $data an array with the new values
	 * @param integer $id payment ID
	 * @return bool|int the new or updated payment ID
	 */
	public function setObject($data, $id = null)
	{
		if($data) {
			if($id > 0) $this->update('TAB_PAIEMENT', $data, 'ID_PAIEMENT=:id', [':id' => $id]);
			else $id = $this->insert('TAB_PAIEMENT', $data);
		}
		return $id ? $id:false;
	}

	/**
	 * Delete a payment
	 * @param int $id payment ID
	 */
	public function deleteObject($id)
	{
		$this->delete('TAB_PAIEMENT', 'ID_PAIEMENT=?', $id);

		//ON SUPPRIME LES JUSTIFICATIFS
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_JUSTIFICATIVE, $id, Models\Proof::TYPE_PAYMENT);
		return true;
	}

	/**
	 * is a given Payment deletable
	 * @param int $id
	 * @return bool
	 */
	function canDeleteObject($id) {
		return true;
	}
}
