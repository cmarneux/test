<?php
/**
 * pole.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\Cache;
use Wish\Models\Model;
use Wish\Tools;
use BoondManager\Models;

class Pole extends AbstractObject{
	/**
	 * Get all poles
	 * @return Model[]
	 */
	function getAllPoles() {
		$cache = Cache::instance();
		$key = 'poles.list';
		if(!$cache->exists($key)){
			$query = 'SELECT ID_POLE, POLE_NAME, POLE_ETAT FROM TAB_POLE ORDER BY POLE_NAME ASC';
			$result = $this->exec($query);
			$cache->set($key, $result);
		}
		return $cache->get($key);
	}

	/**
	 * Get a list of poles from a list of pole's id
	 * @param array|int $tabIds poles to load
	 * @return Model[]
	 */
	public function getPoles($tabIds){
		if(!is_array($tabIds)) $tabIds = [$tabIds];
		if(!$tabIds) return [];

		$poles = $this->getAllPoles();
		$selectedPoles = [];
		foreach($poles as $p){
			if(in_array($p['ID_POLE'], $tabIds)) $selectedPoles[] = $p;
		}
		return $selectedPoles;
	}

	/**
	 * Get a pole
	 * @param int $id pole ID
	 * @return Model|false
	 */
	public function getPole($id) {
		$sql = 'SELECT ID_POLE, POLE_NAME, POLE_ETAT FROM TAB_POLE WHERE ID_POLE=?';
		return $this->singleExec($sql, $id);
	}

	/**
	 * Create a pole
	 * @param array $data
	 * @return int pole id
	 */
	public function createPole($data) {
		return $this->insert('TAB_POLE', $data);
	}

	/**
	 * Update a pole
	 * @param int $id pole id
	 * @param array $data
	 * @return bool
	 */
	public function updatePole($id, $data) {
		$this->update('TAB_POLE', $data, 'ID_POLE=:id', ['id' => $id]);
		return true;
	}

	/**
	 * Delete a pole
	 * @param int $id pole ID
	 * @return int number of pole deleted
	 */
	public function deletePole($id) {
		return $this->delete('TAB_POLE', 'ID_POLE=?', $id);
	}

	/**
	 * Does pole reducible ?
	 * @param $id
	 * @return bool
	 * @throws \Exception
	 */
	function isPoleReducible($id) {
		$nbDocument = 0;
		$query = 'SELECT COUNT(ID_PROFIL) AS NB_DOCUMENT FROM TAB_PROFIL WHERE ID_POLE=:id
				UNION ALL
				SELECT COUNT(ID_AO) AS NB_DOCUMENT FROM TAB_AO WHERE ID_POLE=:id
				UNION ALL
				SELECT COUNT(ID_PROJET) AS NB_DOCUMENT FROM TAB_PROJET WHERE ID_POLE=:id
				UNION ALL
				SELECT COUNT(ID_PRODUIT) AS NB_DOCUMENT FROM TAB_PRODUIT WHERE ID_POLE=:id
				UNION ALL
				SELECT COUNT(ID_ACHAT) AS NB_DOCUMENT FROM TAB_ACHAT WHERE ID_POLE=:id
				UNION ALL
				SELECT COUNT(ID_CRMSOCIETE) AS NB_DOCUMENT FROM TAB_CRMSOCIETE WHERE ID_POLE=:id
				UNION ALL
				SELECT COUNT(ID_CRMCONTACT) AS NB_DOCUMENT FROM TAB_CRMCONTACT WHERE ID_POLE=:id';
		$result = $this->exec($query, ['id'=>$id]);
		foreach($result as $row)$nbDocument += $row['NB_DOCUMENT'];
		return ($nbDocument == 0);
	}

	/**
	 * Get a pole with minimum data
	 * @param $id
	 * @return Model|null
	 */
	public function getBasicPole($id){
		$poles = $this->getAllBasicPoles();
		return Tools::mapData($id, $poles);
	}

	/**
	 * Get a list of all poles with minimum data
	 * @return Model[]
	 */
	public function getAllBasicPoles(){
		$cache = Cache::instance();
		$key = 'poles.basic.list';
		if(!$cache->exists($key)) {
			$poles    = $this->getAllPoles();
			$mappedArray = [];
			foreach($poles as $pole){
				$mappedArray[$pole['ID_POLE']] = new Models\Pole([
					'ID_POLE' => $pole['ID_POLE'],
					'POLE_NAME' => $pole['POLE_NAME']
				]);
			}
			$cache->set($key, $mappedArray);
		}
		return $cache->get($key);
	}
}
