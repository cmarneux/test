<?php
/**
 * positioning.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Positionings\Filters\SearchPositionings;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Handle all database work related to positionings
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Positioning extends AbstractObject {

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchPositionings $filter
	 * @return SearchResult
	 */
	public function searchPositionings(SearchPositionings $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->groupBy('ID_POSITIONNEMENT');
		$query->addColumns('ID_POSITIONNEMENT, POS.ID_ITEM, POS.ITEM_TYPE, POS.ID_AO, POS.ID_RESPPROFIL, POS_DATE, POS_ETAT, POS_DATEUPDATE, POS_TARIF, POS_CJM, POS_NBJRSFACTURE, POS_DEBUT, POS_FIN, POS_CORRELATION, POS_COMMENTAIRE, POS_NBJRSGRATUIT, POS_TARIFADDITIONNEL, POS_INVESTISSEMENT,
					TAB_PROJET.ID_PROJET, TAB_PROJET.ID_PROFILCDP, TAB_PROJET.ID_SOCIETE AS PRJ_IDSOCIETE, TAB_PROJET.ID_PROFIL AS PRJ_IDPROFIL,
					RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM,
					AO_TITLE, AO_REF, AO_ETAT, AO_TYPE, AO_TYPEREF, AO_VISIBILITE, AO_DEVISEAGENCE, AO_CHANGEAGENCE, AO_DEVISE, AO_CHANGE, TAB_AO.ID_SOCIETE AS AO_IDSOCIETE, TAB_AO.ID_PROFIL AS AO_IDPROFIL,
					TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM,
					TAB_CRMCONTACT.ID_PROFIL AS CRMRESP_IDPROFIL, TAB_CRMCONTACT.ID_SOCIETE AS CRMRESP_IDSOCIETE, TAB_CRMCONTACT.ID_POLE AS CRMRESP_IDPOLE,
					TAB_CRMSOCIETE.ID_CRMSOCIETE, CSOC_SOCIETE, RESPUS.ID_USER');
		$query->from('TAB_POSITIONNEMENT POS');
		$query->addJoin('INNER JOIN TAB_AO USING(ID_AO)
					LEFT JOIN TAB_PROJET USING(ID_PROJET)
					LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_AO.ID_CRMCONTACT)
					LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_AO.ID_CRMSOCIETE)');

		if($filter->positioningType->getValue() == Models\Positioning::ENTITY_PRODUCT){
			$query->addColumns('TAB_PRODUIT.ID_PRODUIT, PRODUIT_NOM, PRODUIT_REF, PRODUIT_TYPE, PRODUIT_TAUXTVA, PRODUIT_TARIFHT');
			$query->addJoin('LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=POS.ID_ITEM AND POS.ITEM_TYPE='. Models\Positioning::ENTITY_PRODUCT_KEY.')');

			if($filter->returnMainManager->getValue() == 1) {
				$query->addColumns('RESP.PROFIL_NOM AS COMP_RESPMANAGER_NOM, RESP.PROFIL_PRENOM AS COMP_RESPMANAGER_PRENOM, RESP.ID_PROFIL AS COMP_RESPMANAGER_ID');
			}
			$query->addWhere('POS.ITEM_TYPE = '. Models\Positioning::ENTITY_PRODUCT_KEY);
		} else {
			$query->addColumns('COMP.ID_PROFIL, COMP.ID_SOCIETE, COMP.PROFIL_TYPE AS COMP_TYPE, COMP.PROFIL_NOM AS COMP_NOM, COMP.PROFIL_PRENOM AS COMP_PRENOM, COMP.PROFIL_VISIBILITE AS COMP_VISIBILITE, COMP.ID_RESPMANAGER, COMP.ID_RESPRH, COMPUS.USER_TYPE');
			$query->addJoin('LEFT JOIN TAB_PROFIL COMP ON(COMP.ID_PROFIL=POS.ID_ITEM AND POS.ITEM_TYPE='. Models\Positioning::ENTITY_PROFILE_KEY.')
						LEFT JOIN TAB_USER COMPUS ON(COMPUS.ID_PROFIL=COMP.ID_PROFIL)');

			if($filter->returnHRManager->getValue() == 1) {
				$query->addColumns('COMP_RESPRH_PROFIL.PROFIL_NOM AS COMP_RESPRH_NOM, COMP_RESPRH_PROFIL.PROFIL_PRENOM AS COMP_RESPRH_PRENOM, COMP_RESPRH_PROFIL.ID_PROFIL AS COMP_RESPRH_ID');
				$query->addJoin('LEFT JOIN TAB_USER COMP_RESPRH_USER ON(COMP_RESPRH_USER.ID_USER=COMP.ID_RESPRH)
							LEFT JOIN TAB_PROFIL COMP_RESPRH_PROFIL ON(COMP_RESPRH_PROFIL.ID_PROFIL=COMP_RESPRH_USER.ID_PROFIL)');
			}

			if($filter->returnMainManager->getValue() == 1) {
				if($filter->perimeterManagersType->getValue() == SearchPositionings::PERIMETER_PROFILES){
					$query->addColumns('RESP.PROFIL_NOM AS COMP_RESPMANAGER_NOM, RESP.PROFIL_PRENOM AS COMP_RESPMANAGER_PRENOM, RESP.ID_PROFIL AS COMP_RESPMANAGER_ID');
				} else {
					$query->addColumns('COMP_RESPMANAGER_PROFIL.PROFIL_NOM AS COMP_RESPMANAGER_NOM, COMP_RESPMANAGER_PROFIL.PROFIL_PRENOM AS COMP_RESPMANAGER_PRENOM, COMP_RESPMANAGER_PROFIL.ID_PROFIL AS COMP_RESPMANAGER_ID');
					$query->addJoin('LEFT JOIN TAB_USER COMP_RESPMANAGER_USER ON(COMP_RESPMANAGER_USER.ID_USER=COMP.ID_RESPMANAGER)
								LEFT JOIN TAB_PROFIL COMP_RESPMANAGER_PROFIL ON(COMP_RESPMANAGER_PROFIL.ID_PROFIL=COMP_RESPMANAGER_USER.ID_PROFIL)');
				}
			}
			$query->addWhere('POS.ITEM_TYPE = '. Models\Positioning::ENTITY_PROFILE_KEY);
		}

		switch($filter->perimeterManagersType->getValue()){
			case SearchPositionings::PERIMETER_PROFILES://perimeter of profiles
				$query->addJoin('LEFT JOIN TAB_USER RESPUS ON(RESPUS.ID_USER=COMP.ID_RESPMANAGER)
							LEFT JOIN TAB_PROFIL RESP ON(RESPUS.ID_PROFIL=RESP.ID_PROFIL)');
				break;
			case SearchPositionings::PERIMETER_POSITIONINGS://perimeter of positionings
				$query->addJoin('LEFT  JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=POS.ID_RESPPROFIL)
							LEFT JOIN TAB_USER RESPUS ON(RESPUS.ID_PROFIL=RESP.ID_PROFIL)');
				break;
			case SearchPositionings::PERIMETER_PRODUCTS://perimeter of products
				$query->addJoin('LEFT  JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=TAB_PRODUIT.ID_PROFIL)
							LEFT JOIN TAB_USER RESPUS ON(RESPUS.ID_PROFIL=RESP.ID_PROFIL)');
				break;
			case SearchPositionings::PERIMETER_OPPORTUNITIES://perimeter of opportunities
			default:
				$query->addJoin('LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=TAB_AO.ID_PROFIL)
							LEFT JOIN TAB_USER RESPUS ON(RESPUS.ID_PROFIL=RESP.ID_PROFIL)');
				break;
		}

		$query->addWhere($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'RESP.ID_SOCIETE',
			$colPole = 'RESP.ID_POLE',
			$colUser = 'RESP.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['AO' => 'TAB_AO.ID_AO', 'CCON' => 'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC' => 'TAB_CRMSOCIETE.ID_CRMSOCIETE', 'POS' => 'ID_POSITIONNEMENT']+
						($filter->positioningType->getValue() == Models\Positioning::ENTITY_PRODUCT ? ['PROD' => 'TAB_PRODUIT.ID_PRODUIT']:['CAND' => 'COMP.ID_PROFIL', 'COMP' => 'COMP.ID_PROFIL']);
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$query->addWhere($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('MATCH(AO_TITLE, AO_DESCRIPTION) AGAINST(?)', $keywords);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('RESP.PROFIL_PRENOM LIKE ?', $likeSearch)
						  ->or_('RESP.PROFIL_NOM LIKE ?', $likeSearch)
						  ->or_('AO_REF LIKE ?', $likeSearch);
			if($filter->positioningType->getValue() == Models\Positioning::ENTITY_PRODUCT){
				$whereKeywords->or_('TAB_PRODUIT.PRODUIT_NOM LIKE ?', $likeSearch)
							  ->or_('TAB_PRODUIT.PRODUIT_REF LIKE ?', $likeSearch);
			} else {
				$whereKeywords->or_('COMP.PROFIL_PRENOM LIKE ?', $likeSearch)
							  ->or_('COMP.PROFIL_NOM LIKE ?', $likeSearch);
			}

			$query->addWhere($whereKeywords);
		}

		$query->addWhere( $this->getFilterSearch($filter->entityTypes->getValue(),
							$filter->positioningType->getValue() == Models\Positioning::ENTITY_PRODUCT ? 'PRODUIT_TYPE' : 'COMP.PROFIL_TYPE') );

		//OPPORTUNITY TYPE
		$query->addWhere( $this->getFilterSearch($filter->opportunityTypes->getValue(), 'AO_TYPEREF') )
			  ->addWhere( $this->getFilterSearch($filter->positioningStates->getValue(), 'POS_ETAT') )
			  ->addWhere( $this->getFilterSearch($filter->opportunityStates->getValue(), 'AO_ETAT') );

		switch ($filter->period->getValue()) {
			case SearchPositionings::PERIOD_UPDATED:
				$query->addWhere('DATE(POS_DATEUPDATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchPositionings::PERIOD_CREATED:
				$query->addWhere('DATE(POS_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			default:case BM::INDIFFERENT:break;
		}

		//~ Show only if opportunity is visible
		if($filter->visibleOpportunity->getValue()) $query->addWhere('AO_VISIBILITE=1');

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin(' INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_POSITIONING.' AND TAB_FLAG.ID_PARENT=ID_POSITIONNEMENT)');
			$query->addWhere('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

		$result = $this->launchSearch($query);

		$totalQuery = new Query();
		$totalQuery->select('COUNT(DISTINCT ID_POSITIONNEMENT) AS NB_ROWS, SUM(CASE WHEN POS_CORRELATION=1 THEN POS_TARIF*POS_NBJRSFACTURE+POS_TARIFADDITIONNEL ELSE 0 END) AS TOTAL_CA, SUM(CASE WHEN POS_CORRELATION=1 THEN POS_CJM*(POS_NBJRSFACTURE+POS_NBJRSGRATUIT)+POS_INVESTISSEMENT ELSE 0 END) AS TOTAL_COUT');
		$totalQuery->from( $query->getFrom() );
		$totalQuery->addJoin( $query->buildJoins() );
		$totalQuery->addWhere( $query->getWhere() );

		$totalResult = $this->singleExec($totalQuery);

		$result->turnoverForecastExcludingTax = $result->total_ca = $totalResult->TOTAL_CA;
		$result->costsForecastExcludingTax = $result->total_costs = $totalResult->TOTAL_COUT;
		$result->marginForecastExcludingTax = $result->turnoverForecastExcludingTax - $result->costsForecastExcludingTax;
		$result->profitabilityForecast = ($result->turnoverForecastExcludingTax) ? 100 * $result->marginForecastExcludingTax / $result->turnoverForecastExcludingTax : 0;

		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchPositionings::ORDERBY_UPDATE_DATE                      => 'POS_DATEUPDATE',
			SearchPositionings::ORDERBY_RESOURCE_LASTNAME                => 'COMP_NOM',
			SearchPositionings::ORDERBY_CANDIDATE_LASTNAME               => 'COMP_NOM',
			SearchPositionings::ORDERBY_PRODUCT_NAME                     => 'PRODUIT_NOM',
			SearchPositionings::ORDERBY_OPPORTUNITY_TITLE                => 'AO_TITLE',
			SearchPositionings::ORDERBY_STATE                            => 'POS_ETAT',
			SearchPositionings::ORDERBY_OPPORTUNITY_COMPANY_NAME         => 'CSOC_SOCIETE',
			SearchPositionings::ORDERBY_RESOURCE_MAINMANAGER_LASTNAME    => 'RESP_NOM',
			SearchPositionings::ORDERBY_CANDIDATE_MAINMANAGER_LASTNAME   => 'RESP_NOM',
			SearchPositionings::ORDERBY_PRODUCT_MAINMANAGER_LASTNAME     => 'RESP_NOM',
			SearchPositionings::ORDERBY_OPPORTUNITY_MAINMANAGER_LASTNAME => 'RESP_NOM',
			SearchPositionings::ORDERBY_MAINMANAGER_LASTNAME             => 'RESP_NOM',
		];

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('ID_POSITIONNEMENT DESC');
	}

	/**
	 * Retrieve positioning data from its ID
	 * @param  integer  $id  Positioning ID, cf. [TAB_POSITIONNEMENT.ID_POSITIONNEMENT](../../bddclient/classes/TAB_POSITIONNEMENT.html#property_ID_POSITIONNEMENT).
	 * @return \BoondManager\Models\Positioning|false
	 */
	public function getObject($id) {
		// construction de la requete de base
		$baseQuery = new Query();
		$baseQuery->addColumns([
			'ID_POSITIONNEMENT', 'POS.ID_ITEM', 'POS.ITEM_TYPE', 'ID_RESPPROFIL', 'POS_ETAT', 'POS_DATE', 'POS_DATEUPDATE',
			'POS_COMMENTAIRE', 'POS_DEBUT', 'POS_FIN', 'POS_TARIF', 'POS_CJM', 'POS_NBJRSFACTURE', 'POS_NBJRSGRATUIT', 'POS_CORRELATION',
			'POS_TARIFADDITIONNEL', 'POS_INVESTISSEMENT', 'TAB_AO.ID_AO', 'AO_IDSOCIETE' => 'TAB_AO.ID_SOCIETE', 'AO_IDPOLE' => 'TAB_AO.ID_POLE',
			'AO_IDPROFIL' => 'TAB_AO.ID_PROFIL', 'AO_VISIBILITE', 'AO_CORRELATIONPOS', 'AO_CA', 'AO_TYPE', 'AO_TYPEREF', 'AO_TITLE', 'AO_REF', 'AO_DEVISE',
			'AO_CHANGE', 'AO_DEVISEAGENCE', 'AO_CHANGEAGENCE', 'TAB_AO.ID_CRMCONTACT', 'TAB_PROJET.ID_PROJET', 'PRJ_REFERENCE', 'COMP.PROFIL_TYPE',
			'COMP.ID_PROFIL', 'COMP.PROFIL_NOM', 'COMP.PROFIL_PRENOM', 'COMP.ID_SOCIETE', 'COMP.ID_POLE', 'TAB_AO.ID_CRMSOCIETE', 'CSOC.CSOC_SOCIETE',
			'CCON.CCON_NOM', 'CCON.CCON_PRENOM', 'ID_PRODUIT', 'PRODUIT_NOM', 'PRODUIT_REF', 'PRODUIT_TYPE', 'PRODUIT_TAUXTVA', 'PRODUIT_TARIFHT',
			'ID_RESP' => 'COALESCE(UMANAGER.ID_PROFIL, TAB_PRODUIT.ID_PROFIL)', 'ID_RESPRH' => 'URH.ID_PROFIL'
		])
		->from('TAB_POSITIONNEMENT AS POS')
		->addJoin('INNER JOIN TAB_AO USING(ID_AO)
					LEFT JOIN TAB_PROFIL COMP ON(COMP.ID_PROFIL=POS.ID_ITEM AND POS.ITEM_TYPE=0)
					LEFT JOIN TAB_USER URH ON (URH.ID_USER = COMP.ID_RESPRH)
					LEFT JOIN TAB_USER UMANAGER ON (UMANAGER.ID_USER = COMP.ID_RESPMANAGER)
					LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=POS.ID_ITEM AND POS.ITEM_TYPE=1)
					LEFT JOIN TAB_PROJET USING(ID_PROJET)
					LEFT JOIN TAB_CRMCONTACT CCON ON(CCON.ID_CRMCONTACT=TAB_AO.ID_CRMCONTACT)
					LEFT JOIN TAB_CRMSOCIETE CSOC ON(CSOC.ID_CRMSOCIETE=TAB_AO.ID_CRMSOCIETE)')
		->addWhere('ID_POSITIONNEMENT = ?', $id)
		->groupBy('ID_POSITIONNEMENT');

		$result = $this->singleExec($baseQuery);

		if($result) {
			// TODO passer le parent_type = 1 en parent_type = constante
			$result['POSITIONDETAILS'] = $this->exec('SELECT ID_POSITIONDETAILS, POSDETAILS_TARIF, POSDETAILS_INVESTISSEMENT, POSDETAILS_TITRE FROM TAB_POSITIONDETAILS WHERE ID_PARENT=? AND PARENT_TYPE=1 ORDER BY ID_POSITIONDETAILS ASC', [$id]);
			$result['DOCUMENTS'] = $this->exec( 'SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=?', [$id, BM::CATEGORY_POSITIONING]);
		}
		return $result;
	}

	/**
	 * \brief Création d'un nouveau positionnement
	 * @param <type> $data tableau de données
	 * @return <type> identifiant du positionnement
	 */
	//~ v6 public function postObject($data, $iditem = 0, $itemtype = 0, $type_profil = PROFIL_CANDIDAT, $solr = true)
	public function postObject($data)
	{
		if(isset($data['POSITIONNEMENT'])) {
			if(!isset($data['POSITIONNEMENT']['ID_RESPPROFIL'])) $data['POSITIONNEMENT']['ID_RESPPROFIL'] = CurrentUser::instance()->getEmployeeId(); // TODO : Tin ceci doit être fait dans Service\Positionings->getNew()
			if(!isset($data['POSITIONNEMENT']['POS_DATE'])) $data['POSITIONNEMENT']['POS_DATE'] = date('Y-m-d H:i:s', time()); // TODO : Tin : idem
			if(!isset($data['POSITIONNEMENT']['POS_DATE'])) $data['POSITIONNEMENT']['POS_DATEUPDATE'] = date('Y-m-d H:i:s');
			//~ if($iditem > 0) { //TODO : Tin : À quoi ça sert ? pourquoi ne pas passer directement $data['POSITIONNEMENT']['ID_ITEM'] et $data['POSITIONNEMENT']['ITEM_TYPE'] ?
				//~ $data['POSITIONNEMENT']['ID_ITEM'] = $iditem;
				//~ $data['POSITIONNEMENT']['ITEM_TYPE'] = $itemtype;
			//~ }
			$id = $this->insert('TAB_POSITIONNEMENT', $data['POSITIONNEMENT']);
			//~ if($iditem > 0 && $itemtype == 0) { //Tous ça pour lancer une simple notification ? pas très bon si ?
				//~ if($type_profil == PROFIL_CANDIDAT) {
					//~ (new Candidate())->updateObject([], $iditem, 0, $solr);
				//~ } else {
					//~ (new Resource())->setObject([], $iditem, 0, $solr);
				//~ }
			//~ }

			if($id && isset($data['POSITIONDETAILS'])) foreach($data['POSITIONDETAILS'] as $detail){
				unset($detail['ID_POSITIONDETAILS']);//~ avoids duplicate primary key
				$this->insert('TAB_POSITIONDETAILS', $detail+['ID_PARENT' => $id, 'PARENT_TYPE' => Models\PositioningDetail::PARENT_POSITIONING_KEY]);
			}

			return $id;
		}
		return false;
	}

	/**
	 * Update a positioning
	 * @param array $data an array with the new values group by category (POSITIONDETAILS)
	 * @param integer $id positioning ID
	 * @param boolean $solr trigger a SolR Sync
	 */
	//~ v6 public function putObject($data, $id, $iditem = 0, $itemtype = 0, $type_profil = PROFIL_CANDIDAT, $solr = true)
	public function putObject($data, $id)
	{
		if(isset($data['POSITIONDETAILS'])) {
			//On met à jour les détails
			$ids = array_column($data['POSITIONDETAILS'], 'ID_POSITIONDETAILS');
			if($ids) {
				$this->delete('TAB_POSITIONDETAILS', 'ID_PARENT=? AND PARENT_TYPE=? AND ID_POSITIONDETAILS NOT IN (' . Where::prepareWhereIN($ids) . ')'
				, array_merge([$id, Models\PositioningDetail::PARENT_POSITIONING_KEY], $ids));
			}

			foreach($data['POSITIONDETAILS'] as $detail)
				if(isset($detail['ID_POSITIONDETAILS'])) {
					$this->update('TAB_POSITIONDETAILS', $detail, 'ID_POSITIONDETAILS=:id', [ ':id' => $detail['ID_POSITIONDETAILS']]);
				}else {
					$this->insert('TAB_POSITIONDETAILS', $detail+['ID_PARENT' => $id, 'PARENT_TYPE' => Models\PositioningDetail::PARENT_POSITIONING_KEY]);
				}
		}

		$data['POSITIONNEMENT']['POS_DATEUPDATE'] = date('Y-m-d H:i:s');
		//~ if($iditem > 0) { //TODO : Tin : À quoi ça sert ? pourquoi ne pas passer directement $data['POSITIONNEMENT']['ID_ITEM'] et $data['POSITIONNEMENT']['ITEM_TYPE'] ?
			//~ $data['POSITIONNEMENT']['ID_ITEM'] = $iditem;
			//~ $data['POSITIONNEMENT']['ITEM_TYPE'] = $itemtype;
		//~ }
		$this->update('TAB_POSITIONNEMENT', $data['POSITIONNEMENT'], 'ID_POSITIONNEMENT=:id', [':id' => $id]);
		//~ if($iditem > 0 && $itemtype == 0) { //~ Ceci est geré dans le service
			//~ if($type_profil == PROFIL_CANDIDAT) {
				//~ (new Candidate())->updateObject([], $iditem, 0, $solr);
			//~ } else {
				//~ (new Resource())->setObject([], $iditem, 0, $solr);
			//~ }
		//~ }
	}

	/**
	 * Delete a positioning
	 * @param int $id positioning ID
	 */
	public function deleteObject($id, $iditem = 0, $itemtype = 0, $type_profil = Models\Candidate::TYPE_CANDIDATE, $solr = true)
	{
		//On supprime tous les documents de ce positionnement
		$fileDB = new File();
		$fileDB->deleteAllObjects(File::TYPE_DOCUMENT, $id, Models\Document::TYPE_POSITIONING);

		//On supprime tous les flags de cette action
		$flagsDB = new AttachedFlag();
		$flagsDB->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_POSITIONING);

		$this->delete('TAB_POSITIONDETAILS', 'ID_PARENT=? AND PARENT_TYPE=?', [$id, Models\PositioningDetail::PARENT_POSITIONING_KEY]);

		$this->delete('TAB_POSITIONNEMENT', 'ID_POSITIONNEMENT=?', $id);
		if($iditem > 0 && $itemtype == 0) {
			if($type_profil == Models\Candidate::TYPE_CANDIDATE) {
				$candidateDB = new Candidate();
				$candidateDB->updateObject([], $iditem, 0, $solr);
			} else {
				$ressourceBDD = new Employee();
				$ressourceBDD->setObject([], $iditem, 0, $solr);
			}
		}
		return true;
	}
}
