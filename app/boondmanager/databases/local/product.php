<?php
/**
 * product.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Products\Filters\SearchProducts;
use Wish\Models\SearchResult;
use Wish\MySQL\Where;
use BoondManager\Services\BM;
use Wish\MySQL\Query;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;

/**
 * Handle all database work related to products
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Product extends AbstractObject {

	/**
	 * Retrieve product data from its ID
	 * @param  integer  $id  Product ID, cf. [TAB_PRODUIT.ID_PRODUIT](../../bddclient/classes/TAB_PRODUIT.html#property_ID_PRODUIT).
	 * @param  integer $tab Subpart of the product (Tab)
	 * @return Models\Product|false
	 */
	public function getObject($id, $tab = 0) {
		// construction de la requete de base, sera enrichie en fonction de l'onglet
		$baseQuery = new Query();
		$baseQuery->addColumns(['ID_PRODUIT', 'PRODUIT_NOM', 'PRODUIT_REF', 'ID_PROFIL', 'ID_SOCIETE', 'PRODUIT_DATEUPDATE'])
			->from('TAB_PRODUIT AS PRD')
			->addWhere('ID_PRODUIT = ?', $id)
			->groupBy('ID_PRODUIT')
			->setModelClass(Models\Product::class);

		switch($tab) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Product::TAB_INFORMATION:
				$baseQuery->addColumns([
						'PRODUIT_TYPE', 'PRODUIT_ETAT', 'PRODUIT_DESCRIPTION', 'PRODUIT_TARIFHT', 'PRODUIT_TAUXTVA', 'PRODUIT_DEVISE',
						'PRODUIT_CHANGE', 'PRODUIT_DEVISEAGENCE', 'PRODUIT_CHANGEAGENCE', 'ID_POLE',
					]);
				$baseQuery->setMapper( new Mappers\GetProduct());
				$result = $this->singleExec($baseQuery);
				if($result) {
					$result->DOCUMENTS = $this->exec(
						'SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=?',
						[$id, BM::CATEGORY_PRODUCT],
						Models\Document::class
					);
				}
				return $result;
			default:
				//basic sql
				return $this->singleExec($baseQuery);
		}

		return false;
	}

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchProducts $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchProducts(SearchProducts $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('PRD.ID_PRODUIT, TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, ID_USER,
		                    PRODUIT_REF, PRODUIT_TYPE, PRODUIT_ETAT, PRODUIT_NOM, PRODUIT_TARIFHT, PRODUIT_DEVISE, 
		                    PRODUIT_CHANGE, PRODUIT_DEVISEAGENCE, PRODUIT_CHANGEAGENCE');
		$query->from('TAB_PRODUIT PRD');
		$query->addJoin('LEFT JOIN TAB_PROFIL USING(ID_PROFIL)
		                 LEFT JOIN TAB_USER USING(ID_PROFIL)');
		$query->groupBy('PRD.ID_PRODUIT');

		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRD.ID_SOCIETE',
			$colPole = 'PRD.ID_POLE',
			$colUser = 'PRD.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['PROD'=>'PRD.ID_PRODUIT'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('MATCH(PRODUIT_DESCRIPTION) AGAINST(?)', $keywords);
			$whereKeywords->or_('PRD.PRODUIT_NOM LIKE ?', $likeSearch)
			              ->or_('PRD.PRODUIT_REF LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
			$query->addColumns([ 'PERTINENCE'=> 'MATCH(PRODUIT_DESCRIPTION) AGAINST(' . $this->escape($keywords) .  ')' ]);
			$query->addOrderBy('PERTINENCE DESC');
		}

		$where->and_( $this->getFilterSearch($filter->productStates->getValue(), 'PRODUIT_ETAT') )
				->and_( $this->getFilterSearch($filter->productTypes->getValue(), 'PRODUIT_TYPE') );

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PRODUCT.' AND TAB_FLAG.ID_PARENT=ID_PRODUIT)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		return $this->launchSearch($query, Models\SearchResults\Products::class);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			SearchProducts::ORDERBY_REFERENCE => 'PRODUIT_REF',
			SearchProducts::ORDERBY_NAME => 'PRODUIT_NOM',
			SearchProducts::ORDERBY_TYPE => 'PRODUIT_TYPE',
			SearchProducts::ORDERBY_PRICEEXCLUDINGTAX => 'PRODUIT_TARIFHT',
			SearchProducts::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM'
		];

		if(!$column) $query->addOrderBy('PRODUIT_REF ASC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('ID_PRODUIT DESC');
	}

	/**
	 * Update a product
	 * @param array $data tableau de données
	 * @param int $id product ID
	 * @return true
	 */
	public function updateObject($data, $id)
	{
		$data['PRODUIT_DATEUPDATE'] = date('Y-m-d H:i:s');
		$this->update('TAB_PRODUIT', $data['PRODUIT'], 'ID_PRODUIT=:id', ['id'=>$id]);
		return true;
	}

	/**
	 * Create a new product
	 * @param array $data
	 * @return int the product ID
	 */
	public function newObject($data)
	{
		if(isset($data['PRODUIT'])) {
			$data = $data['PRODUIT'];
			if (!isset($data['PRODUIT_DATEUPDATE'])) $data['PRODUIT_DATEUPDATE'] = date('Y-m-d H:i:s');
			return $this->insert('TAB_PRODUIT', $data);
		}else return false;
	}

	/**
	 * Delete a product
	 * @param int $id product id
	 * @return bool
	 */
	public function deleteObject($id) {
		//On supprime tous les documents de ce produit
		$dbFile = File::instance();
		$dbFile->deleteAllObjects(File::TYPE_DOCUMENT, $id, Models\Document::TYPE_PRODUCT);

		//On supprime tous les positionnements de ce produit
		$positioningData = Positioning::instance();
		foreach($this->exec('SELECT ID_POSITIONNEMENT FROM TAB_POSITIONNEMENT WHERE ID_ITEM=? AND ITEM_TYPE=1', $id) as $pos)
			$positioningData->deleteObject($pos['ID_POSITIONNEMENT']);

		//On supprime toutes les missions profils de ce produit
		$projetData = Delivery::instance();
		foreach($this->exec('SELECT ID_MISSIONPROJET FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE TAB_MISSIONPROJET.ID_ITEM=? AND ITEM_TYPE=1 AND PRJ_TYPE>0', $id) as $mission)
			$projetData->deleteDelivery($mission['ID_MISSIONPROJET']);

		//On supprime tous les flags de cette action
		$flagsData = AttachedFlag::instance();
		$flagsData->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_PRODUCT);

		$this->delete('TAB_PRODUIT', 'ID_PRODUIT=?', $id);
		return true;
	}

	/**
	 * check if a product can be deleted
	 * @param int $id product id
	 * @return bool
	 */
	function isProductReducible($id) {
		$nbDocument = 0;
		$sql = 'SELECT COUNT(ID_POSITIONNEMENT) AS NB_DOCUMENT FROM TAB_POSITIONNEMENT WHERE ID_ITEM= :id AND ITEM_TYPE=1
		        UNION ALL
		        SELECT COUNT(ID_MISSIONPROJET) AS NB_DOCUMENT FROM TAB_MISSIONPROJET WHERE ID_ITEM= :id AND ITEM_TYPE=1';

		foreach($this->exec($sql, ['id'=>$id]) as $document)
			$nbDocument += $document['NB_DOCUMENT'];

		return $nbDocument == 0;
	}

}
