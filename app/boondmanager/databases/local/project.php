<?php
/**
 * project.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Local;

use BoondManager\APIs\Projects\Filters\SearchProjects;
use BoondManager\Lib\Currency;
use BoondManager\Services\CurrentUser;
use Wish\Models\Model;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use BoondManager\Services\Actions;

/**
 * Class Project
 * @package BoondManager\Databases\Local
 */
class Project extends AbstractObject {
	/**
	 * Search projects
	 * @param SearchProjects $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchProjects(SearchProjects $filter) {
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('PRJ.ID_PROJET, PRJ.ID_SOCIETE, PRJ.ID_POLE, PRJ_REFERENCE, PRJ_TYPE, PRJ_TYPEREF, AO_TITLE, AO_APPLICATIONS, AO_INTERVENTION, PRJ_DEBUT,
								PRJ_FIN, PRJ_ADR, PRJ_CP, PRJ_VILLE, PRJ_PAYS, ID_PROFILCDP, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE, PRJ_RESTEAFAIRE,
								PRJ_ETAT, PRJ_TARIFADDITIONNEL, PRJ_INVESTISSEMENT, TAB_CRMCONTACT.ID_CRMCONTACT, TAB_CRMCONTACT.ID_CRMSOCIETE, CCON_NOM,
								CCON_PRENOM, TAB_CRMSOCIETE.ID_CRMSOCIETE, CSOC_SOCIETE, PRJRESP.ID_PROFIL, PRJRESP.PROFIL_NOM, PRJRESP.PROFIL_PRENOM, ID_AO,
								SUM(IF(M1.ITEM_TYPE IN(0,1),1,0)) AS NB_PRESTATIONS');

		$where = new Where('PRJ_TYPE>0');
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRJ.ID_SOCIETE',
			$colPole = 'PRJ.ID_POLE',
			$colUser = 'PRJ.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$addData = $filter->sumAdditionalData->getValue();
		$notSmoothAddData = in_array($filter->period->getValue(),
				array(SearchProjects::PERIOD_DELIVERIES_RUNNING_AND_SUM,
					SearchProjects::PERIOD_DELIVERIES_RUNNING,
					SearchProjects::PERIOD_RUNNING_AND_SUM,
					SearchProjects::PERIOD_RUNNING)) && !Dictionary::getDict('specific.setting.smoothAdditionalData') ? true : false;

		$query->from('TAB_PROJET PRJ');
		$query->addJoin('LEFT JOIN TAB_AO USING(ID_AO)
							LEFT JOIN TAB_PROFIL PRJRESP ON(PRJRESP.ID_PROFIL=PRJ.ID_PROFIL)
							LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=PRJ.ID_CRMCONTACT)
							LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRJ.ID_CRMSOCIETE)
							LEFT JOIN TAB_MISSIONPROJET M1 ON(M1.ID_PROJET=PRJ.ID_PROJET)
							LEFT JOIN TAB_PROFIL AT ON(AT.ID_PROFIL=M1.ID_ITEM AND M1.ITEM_TYPE=0)
							LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=M1.ID_ITEM AND M1.ITEM_TYPE=1)');
		$query->groupBy('PRJ.ID_PROJET');

		$currentUser =  CurrentUser::instance();
		$exchangeRate = $currentUser->getCurrency() != $currentUser->getCurrencyInDatabase() ? 'IF(PRJ_DEVISEAGENCE=' . $this->escape($currentUser->getCurrency()) . ',1/PRJ_CHANGEAGENCE,' . (1/$currentUser->getExchangeRate()) . ')*' : '';

		$columnsCACOUT = 'SUM(IF(M1.ID_PARENT=0,' . $exchangeRate . '(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL),0)) AS TOTAL_CASIMUHT'.
						 ', SUM(' . $exchangeRate . '(M1.MP_INVESTISSEMENT+M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT))) AS TOTAL_COUTSIMUHT';
		$columnsPRJNOTSMOOTHTOTAL = 'SUM(' . $exchangeRate . 'PRJ_TARIFADDITIONNEL)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
									', SUM(' . $exchangeRate . 'PRJ_INVESTISSEMENT)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJINVESTISSEMENT_PAR_PRESTATION';
		$columnsPRJ = 'SUM(' . $exchangeRate . 'PRJ_TARIFADDITIONNEL)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
					  ', SUM(' . $exchangeRate . 'PRJ_INVESTISSEMENT)/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END AS PRJINVESTISSEMENT_PAR_PRESTATION';

		$start = $this->escape($filter->startDate->getValue());
		$end = $this->escape($filter->endDate->getValue());

		$deliveryIntersectionPeriod = 'DATEDIFF(M1.MP_FIN,'.$start.')>=0 AND DATEDIFF('.$end.',M1.MP_DEBUT)>=0';
		$deliveryDetailsIntersectionPeriod = 'MD1.MISDETAILS_DATE BETWEEN '.$start.' AND '.$end;

		$projectIntersectionPeriod = 'DATEDIFF(PRJ_FIN,'.$start.')>=0 AND DATEDIFF('.$end.',PRJ_DEBUT)>=0';
		$projectDetailsIntersectionPeriod = 'MD2.MISDETAILS_DATE BETWEEN '.$start.' AND '.$end;

		$deliveryIntersectionNumberOfDays = '(DATEDIFF(LEAST(M1.MP_FIN,'.$end.'),GREATEST(M1.MP_DEBUT,'.$start.'))+1)';
		$projectIntersectionNumberOfDays = '(DATEDIFF(LEAST(PRJ_FIN,'.$end.'),GREATEST(PRJ_DEBUT,'.$start.'))+1)';

		$deliveryNumberOfDays = '(DATEDIFF(M1.MP_FIN,M1.MP_DEBUT)+1)';
		$projectNumberOfDays = '(DATEDIFF(PRJ_FIN,PRJ_DEBUT)+1)';

		$totalCount1 = '/(CASE WHEN COUNT(DISTINCT MD1.ID_MISSIONDETAILS)=0 THEN 1 ELSE COUNT(DISTINCT MD1.ID_MISSIONDETAILS) END * CASE WHEN COUNT(DISTINCT MD2.ID_MISSIONDETAILS)=0 THEN 1 ELSE COUNT(DISTINCT MD2.ID_MISSIONDETAILS) END)';
		$totalCount2 = '/CASE WHEN COUNT(DISTINCT MD2.ID_MISSIONDETAILS)=0 THEN 1 ELSE COUNT(DISTINCT MD2.ID_MISSIONDETAILS) END';
		$totalCount3 = '/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET)*COUNT(DISTINCT M1.ID_MISSIONPROJET) END';
		$totalCount4 = '/CASE WHEN COUNT(DISTINCT M1.ID_MISSIONPROJET)=0 THEN 1 ELSE COUNT(DISTINCT M1.ID_MISSIONPROJET) END';

		switch($filter->period->getValue()){
			case SearchProjects::PERIOD_DELIVERIES_RUNNING_AND_SUM:case SearchProjects::PERIOD_DELIVERIES_RUNNING:
				$addData = $filter->period->getValue() == SearchProjects::PERIOD_DELIVERIES_RUNNING ? false : true;
				if($notSmoothAddData) {
					$query->addJoin('LEFT JOIN TAB_MISSIONDETAILS MD1 ON(MD1.PARENT_TYPE=1 AND MD1.ID_PARENT=M1.ID_MISSIONPROJET)'.
									' LEFT JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)');
					if($addData)
						$where->and_('('.$deliveryIntersectionPeriod.' OR '.$projectDetailsIntersectionPeriod.' AND (MD2.MISDETAILS_TARIF<>0 OR MD2.MISDETAILS_INVESTISSEMENT<>0) OR '.$deliveryDetailsIntersectionPeriod.')');
					else
						$where->and_('('.$deliveryIntersectionPeriod.' OR '.$deliveryDetailsIntersectionPeriod.')');
					$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'M1.MP_TARIF*M1.MP_NBJRSFACTURE/'.$deliveryNumberOfDays.',0))'.$totalCount1.
									 '+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_TARIF,0))'.$totalCount2.' AS TOTAL_CASIMUHT, '.
									 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)/'.$deliveryNumberOfDays.',0))'.$totalCount1.
									 '+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_INVESTISSEMENT,0))'.$totalCount2.' AS TOTAL_COUTSIMUHT';
					$columnsPRJ = 'SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount3.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
								  ', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount3.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
					$columnsPRJNOTSMOOTHTOTAL = 'SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
												', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
				} else {
					if($addData) {
						$where->and_('('.$deliveryIntersectionPeriod.' OR '.$projectIntersectionPeriod.' AND (PRJ_TARIFADDITIONNEL<>0 OR PRJ_INVESTISSEMENT<>0))');
						$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL)/'.$deliveryNumberOfDays.',0)) AS TOTAL_CASIMUHT'.
										 ', SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)+MP_INVESTISSEMENT)/'.$deliveryNumberOfDays.',0)) AS TOTAL_COUTSIMUHT';
					} else {
						$where->and_($deliveryIntersectionPeriod);
						$columnsCACOUT = 'SUM('.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL)/'.$deliveryNumberOfDays.') AS TOTAL_CASIMUHT'.
										 ', SUM('.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)+MP_INVESTISSEMENT)/'.$deliveryNumberOfDays.',0)) AS TOTAL_COUTSIMUHT';
					}
				}
				break;
			case SearchProjects::PERIOD_RUNNING_AND_SUM:case SearchProjects::PERIOD_RUNNING:
				$addData = $filter->period->getValue() == SearchProjects::PERIOD_RUNNING ? false : true;
				if($notSmoothAddData) {
					$query->addJoin('LEFT JOIN TAB_MISSIONDETAILS MD1 ON(MD1.PARENT_TYPE=1 AND MD1.ID_PARENT=M1.ID_MISSIONPROJET)'.
									' LEFT JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)');
					if($addData)
						$where->and_('('.$projectIntersectionPeriod.' OR '.$projectDetailsIntersectionPeriod.' AND (MD2.MISDETAILS_TARIF<>0 OR MD2.MISDETAILS_INVESTISSEMENT<>0) OR '.$deliveryDetailsIntersectionPeriod.')');
					else
						$where->and_('('.$projectIntersectionPeriod.' OR '.$deliveryDetailsIntersectionPeriod.')');
					$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*(IF(M1.ID_PARENT=0,'.$exchangeRate.'M1.MP_TARIF*M1.MP_NBJRSFACTURE,0))/'.$deliveryNumberOfDays.',0))'.$totalCount1.
									 '+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_TARIF,0))'.$totalCount2.' AS TOTAL_CASIMUHT, '.
									 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)/'.$deliveryNumberOfDays.',0))'.$totalCount1.
									 '+ SUM(IF('.$deliveryDetailsIntersectionPeriod.','.$exchangeRate.'MD1.MISDETAILS_INVESTISSEMENT,0))'.$totalCount2.' AS TOTAL_COUTSIMUHT';
					$columnsPRJ = ', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
								  ', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
					$columnsPRJNOTSMOOTHTOTAL = ', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_TARIF,0))'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
												', SUM(IF('.$projectDetailsIntersectionPeriod.','.$exchangeRate.'MD2.MISDETAILS_INVESTISSEMENT,0))'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
				} else {
					$where_expr[] = 'DATEDIFF(\''.$end.'\',PRJ_DEBUT)>=0 AND DATEDIFF(PRJ_FIN,\''.$start.'\')>=0';
					$columnsCACOUT = 'SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_TARIF*M1.MP_NBJRSFACTURE+M1.MP_TARIFADDITIONNEL)/'.$deliveryNumberOfDays.',0)) AS TOTAL_CASIMUHT, '.
									 ', SUM(IF('.$deliveryIntersectionPeriod.','.$deliveryIntersectionNumberOfDays.'*'.$exchangeRate.'(M1.MP_CJM*(M1.MP_NBJRSFACTURE+M1.MP_NBJRSGRATUIT)+MP_INVESTISSEMENT)/'.$deliveryNumberOfDays.',0)) AS TOTAL_COUTSIMUHT';
					$columnsPRJ = 'SUM('.$projectIntersectionNumberOfDays.'*'.$exchangeRate.'PRJ_TARIFADDITIONNEL/'.$projectNumberOfDays.')'.$totalCount4.' AS PRJTARIFADDITIONNEL_PAR_PRESTATION'.
								  ', SUM('.$projectIntersectionNumberOfDays.'*'.$exchangeRate.'PRJ_INVESTISSEMENT/'.$projectNumberOfDays.')'.$totalCount4.' AS PRJINVESTISSEMENT_PAR_PRESTATION';
				}
				break;
			case SearchProjects::PERIOD_CREATED:
				$where->and_('DATE(PRJ_DATE) BETWEEN ? AND ?', $filter->getDatesPeriod());
				//if($filters_array['type_document'] != '1') $where_expr[] = 'DATE(PRJ_DATE) BETWEEN \''.$start.'\' AND \''.$end.'\'';
				break;
			case SearchProjects::PERIOD_CLOSED:
				$where->and_('DATEDIFF(PRJ_FIN,?)<=0 OR DATEDIFF(?,PRJ_DEBUT)<=0', $filter->getDatesPeriod());
				break;
			case SearchProjects::PERIOD_STARTED:
				$where->and_('PRJ_DEBUT BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchProjects::PERIOD_STOPPED:
				$where->and_('PRJ_FIN BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchProjects::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM:case SearchProjects::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE:
				$addData = $filter->period->getValue() == SearchProjects::PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE;
				$query->addJoin('LEFT JOIN TAB_MISSIONDETAILS MD1 ON(MD1.PARENT_TYPE=1 AND MD1.ID_PARENT=M1.ID_MISSIONPROJET)'.
								' LEFT JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)'.
								' LEFT JOIN TAB_ACHAT ON(TAB_ACHAT.ID_PROJET=TAB_PROJET.ID_PROJET)');
				$where->and_('('.$projectIntersectionPeriod.' OR '.$deliveryDetailsIntersectionPeriod.' OR '.$projectDetailsIntersectionPeriod.' OR ACHAT_DATE BETWEEN \''.$start.'\' AND \''.$end.'\')');
				$columnsCACOUT = '0 AS TOTAL_CASIMUHT, 0 AS TOTAL_COUTSIMUHT';
				$columnsPRJ = '0 AS PRJTARIFADDITIONNEL_PAR_PRESTATION, 0 AS PRJINVESTISSEMENT_PAR_PRESTATION';
				break;
		}
		$query->addColumns($columnsCACOUT);
		$query->addCountColumns($columnsCACOUT);

		$keywordsMapping = ['MIS'=>'M1.ID_MISSIONPROJET', 'PRJ'=>'PRJ.ID_PROJET', 'COMP'=>'AT.ID_PROFIL', 'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE', 'AO'=>'TAB_AO.ID_AO', 'PROD'=>'TAB_PRODUIT.ID_PRODUIT',  'CTR'=>'M1.ID_CONTRAT'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if(!$whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('MATCH(AO_TITLE, AO_DESCRIPTION) AGAINST(?)', $keywords);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('AT.PROFIL_PRENOM LIKE ?', $likeSearch)
						  ->or_('AT.PROFIL_NOM LIKE ?', $likeSearch);
			$where->and_($whereKeywords);
		}

		$where->and_($this->getFilterSearch($filter->projectStates->getValue(), 'PRJ_ETAT'))
			->and_($this->getFilterSearch($filter->projectTypes->getValue(), 'PRJ_TYPEREF'))
			->and_($this->getFilterSearch($filter->activityAreas->getValue(), 'AO_APPLICATIONS'))
			->and_($this->getFilterSearch($filter->expertiseAreas->getValue(), 'AO_INTERVENTION'));

		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PROJECT.' AND TAB_FLAG.ID_PARENT=PRJ.ID_PROJET)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());

		$query->addWhere($where);

		/**
		 * @var Models\SearchResults\Projects $result
		 */
		$result = $this->launchSearch($query, Models\SearchResults\Projects::class);

		if($result->total > 0) {
			$currency = new Currency();
			if($notSmoothAddData) {
				$result->turnoverSimulatedExcludingTax = 0;
				$result->costsSimulatedExcludingTax = 0;

				foreach($result->rows as $i => $resultat) {
					$result->rows[$i]['TOTAL_CASIMUHT'] = 0;
					$result->rows[$i]['TOTAL_COUTSIMUHT'] = 0;
				}

				if($addData) {
					$query->resetOrder()
						->replaceColumns('PRJ.ID_PROJET')
						->resetCountColumns()
						->addOrderBy('PRJ.ID_PROJET DESC');

					$nbPages = ceil($result->total / BM::getNumberMaxOfDatabaseResults());
					for($k = 1; $k <= $nbPages; $k++) {
						$query->setLimit(BM::getNumberMaxOfDatabaseResults(), Query::getOffset($k, BM::getNumberMaxOfDatabaseResults()));
						$tabIDs = array();
						$allResults = $this->exec($query);
						foreach($allResults as $resultat) $tabIDs[] = $resultat['ID_PROJET'];

						$queryTMP = new Query();
						$queryTMP->addColumns(['TAB_PROJET.ID_PROJET', $columnsPRJNOTSMOOTHTOTAL]);
						$queryTMP->from('TAB_PROJET');
						$queryTMP->addJoin('INNER JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)'.
										' LEFT JOIN TAB_MISSIONPROJET M1 ON(M1.ID_PROJET=TAB_PROJET.ID_PROJET)');
						$queryTMP->groupBy('TAB_PROJET.ID_PROJET');

						$whereTMP = new Where();
						$whereTMP->and_('TAB_PROJET.ID_PROJET IN('.implode(',',$tabIDs).')');

						$queryTMP->addWhere($whereTMP);

						$projets = $this->exec($queryTMP);
						foreach($projets as $ca) {
							$result->turnoverSimulatedExcludingTax += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
							$result->costsSimulatedExcludingTax += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
						}
					}

					$tabIDs = array();
					foreach($result->rows as $i => $resultat) if(!in_array($resultat['ID_PROJET'], $tabIDs)) $tabIDs[] = $resultat['ID_PROJET'];

					$queryTMP = new Query();
					$queryTMP->addColumns(['TAB_PROJET.ID_PROJET', $columnsPRJ]);
					$queryTMP->from('TAB_PROJET');
					$queryTMP->addJoin('INNER JOIN TAB_MISSIONDETAILS MD2 ON(MD2.PARENT_TYPE=0 AND MD2.ID_PARENT=TAB_PROJET.ID_PROJET)'.
						' LEFT JOIN TAB_MISSIONPROJET M1 ON(M1.ID_PROJET=TAB_PROJET.ID_PROJET)');
					$queryTMP->groupBy('TAB_PROJET.ID_PROJET');

					$whereTMP = new Where();
					$whereTMP->and_('TAB_PROJET.ID_PROJET IN('.implode(',',$tabIDs).')');

					$queryTMP->addWhere($whereTMP);

					$details = $this->exec($queryTMP);
					foreach($result->rows as $i => $resultat)
						foreach($details as $ca) {
							if($ca['ID_PROJET'] == $resultat['ID_PROJET']) {
								$result->rows[$i]['TOTAL_CASIMUHT'] += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
								$result->rows[$i]['TOTAL_COUTSIMUHT'] += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
								break;
							}
						}
				}

				$query->resetOrder()
					->replaceColumns(['M1.ID_MISSIONPROJET', 'M1.ID_PROJET', 'M1.ID_PARENT', $columnsCACOUT])
					->resetCountColumns()
					->groupBy('M1.ID_MISSIONPROJET');;

				$count = $this->singleExec($query->getCountQuery(), $query->getArgs());
				$nbPages = $count ? ceil($count['NB_ROWS'] / BM::getNumberMaxOfDatabaseResults()) : 0;
				for($k = 1; $k <= $nbPages; $k++) {
					$query->setLimit(BM::getNumberMaxOfDatabaseResults(), Query::getOffset($k, BM::getNumberMaxOfDatabaseResults()));
					$missions = $this->exec($query);
					foreach($missions as $ca) {
						$result->turnoverSimulatedExcludingTax += $ca['TOTAL_CASIMUHT'];
						$result->costsSimulatedExcludingTax += $ca['TOTAL_COUTSIMUHT'];
					}

					foreach($result->rows as $i => $resultat) {
						foreach($missions as $ca) {
							if($ca['ID_PROJET'] == $resultat['ID_PROJET']) {
								if($ca['ID_PARENT'] == 0) $result->rows[$i]['TOTAL_CASIMUHT'] += $ca['TOTAL_CASIMUHT'];
								$result->rows[$i]['TOTAL_COUTSIMUHT'] += $ca['TOTAL_COUTSIMUHT'];
							}
						}
					}
				}
			} else if($addData) {
				$query->resetOrder()
					->replaceColumns($columnsPRJ)
					->resetCountColumns()
					->addOrderBy('PRJ.ID_PROJET DESC');

				$nbPages = ceil($result->total / BM::getNumberMaxOfDatabaseResults());
				for($k = 1; $k <= $nbPages; $k++) {
					$query->setLimit(BM::getNumberMaxOfDatabaseResults(), Query::getOffset($k, BM::getNumberMaxOfDatabaseResults()));
					$additionnel = $this->exec($query);
					foreach($additionnel as $ca) {
						$result->turnoverSimulatedExcludingTax += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
						$result->costsSimulatedExcludingTax += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
					}
				}

				$tabIDs = array();
				foreach($result->rows as $i => $resultat) if(!in_array($resultat['ID_PROJET'], $tabIDs)) $tabIDs[] = $resultat['ID_PROJET'];

				$where->and_('PRJ.ID_PROJET IN('.implode(',',$tabIDs).')');
				$query->resetWhere()
					->replaceColumns(['PRJ.ID_PROJET', $columnsPRJ])
					->addWhere($where);

				$tabAdditionnels = $this->exec($query);
				foreach($result->rows as $i => $resultat) {
					foreach($tabAdditionnels as $ca) {
						if($ca['ID_PROJET'] == $resultat['ID_PROJET']) {
							$result->rows[$i]['TOTAL_CASIMUHT'] += $ca['PRJTARIFADDITIONNEL_PAR_PRESTATION'];
							$result->rows[$i]['TOTAL_COUTSIMUHT'] += $ca['PRJINVESTISSEMENT_PAR_PRESTATION'];
						}
					}
				}
			}

			if($currentUser->getCurrency() != $currentUser->getCurrencyInDatabase()) {
				$result->turnoverSimulatedExcludingTax = $currency->getAmountForBDD($result->turnoverSimulatedExcludingTax);
				$result->costsSimulatedExcludingTax = $currency->getAmountForBDD($result->costsSimulatedExcludingTax);
			}

			$result->marginSimulatedExcludingTax = $result->turnoverSimulatedExcludingTax - $result->costsSimulatedExcludingTax;
			if(BM::isProfitabilityCalculatingBasedOnMarginRate())
				$result->profitabilitySimulated = $result->costsSimulatedExcludingTax == 0 ? 0 : 100 * $result->marginSimulatedExcludingTax / $result->costsSimulatedExcludingTax;
			else
				$result->profitabilitySimulated = $result->turnoverSimulatedExcludingTax == 0 ? 0 : 100 * $result->marginSimulatedExcludingTax / $result->turnoverSimulatedExcludingTax;

			foreach($result->rows as $i => $resultat) {
				if($currentUser->getCurrency() != $currentUser->getCurrencyInDatabase()) {
					$result->rows[$i]['TOTAL_CASIMUHT'] = $currency->getAmountForBDD($result->rows[$i]['TOTAL_CASIMUHT']);
					$result->rows[$i]['TOTAL_COUTSIMUHT'] = $currency->getAmountForBDD($result->rows[$i]['TOTAL_COUTSIMUHT']);
				}

				$result->rows[$i]['TOTAL_MARGESIMUHT'] = $result->rows[$i]['TOTAL_CASIMUHT'] - $result->rows[$i]['TOTAL_COUTSIMUHT'];
				$result->rows[$i]['TOTAL_RENTASIMU'] = $result->rows[$i]['TOTAL_CASIMUHT'] == 0 ? 0 : $result->rows[$i]['TOTAL_MARGESIMUHT'] / $result->rows[$i]['TOTAL_CASIMUHT'];
			}
		}
		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {
		$mapping = [
			SearchProjects::ORDERBY_STARTDATE            => 'PRJ.PRJ_DEBUT',
			SearchProjects::ORDERBY_ENDDATE              => 'PRJ.PRJ_FIN',
			SearchProjects::ORDERBY_REFERENCE            => 'PRJ.PRJ_REFERENCE',
			SearchProjects::ORDERBY_COMPANY_NAME         => 'CSOC_SOCIETE',
			SearchProjects::ORDERBY_MAINMANAGER_LASTNAME => 'PRJRESP.PROFIL_NOM'
		];

		if(!$column) $query->addOrderBy('PRJ.PRJ_DATE DESC');
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('PRJ.ID_PROJET DESC');
	}

	/**
	 * Get project
	 * @param int $id
	 * @param string $tab
	 * @return Model|false
	 * @throws \Exception
	 */
	public function getProject($id, $tab = BM::TAB_DEFAULT) {
		$baseQuery = new Query();
		$baseQuery->select([
			'ID_PROJET', 'PRJ_REFERENCE', 'PRJ_DATE', 'PRJ_DATEUPDATE', 'TAB_PROJET.ID_PROFIL', 'TAB_PROJET.ID_SOCIETE',
			'TAB_PROJET.ID_POLE', 'PRJ_LOTSTAB', 'PRJ_TYPE', 'PRJ_TYPEREF', 'PRJ_DEVISE', 'PRJ_CHANGE', 'PRJ_DEVISEAGENCE',
			'PRJ_CHANGEAGENCE', 'ID_PROFILCDP'
		])
		->from('TAB_PROJET')
		->addWhere('PRJ_TYPE>0')
		->addWhere('ID_PROJET=?', $id);

		switch($tab) {
			default:
				$result = $this->singleExec($baseQuery);
				if($result) {
					//On récupère le nombre d'avantages
					$nbAdvantages = $this->exec('SELECT COUNT(DISTINCT ID_AVANTAGE) AS NB_AVANTAGES FROM TAB_AVANTAGE WHERE ID_PROJET=?', $id);
					$result->NB_AVANTAGES = $nbAdvantages[0]->NB_AVANTAGES;

					//On récupère le nombre d'achats
					$nbPurchases = $this->exec('SELECT COUNT(DISTINCT ID_ACHAT) AS NB_ACHATS FROM TAB_ACHAT WHERE ID_PROJET=?', $id);
					$result->NB_ACHATS = $nbPurchases[0]->NB_ACHATS;
				}
				break;
			case Models\Project::TAB_INFORMATION:
				$baseQuery->addColumns(
					'PRJ_COMMENTAIRES, PRJ_ETAT, PRJ_DEBUT, PRJ_FIN, PRJ_ADR, PRJ_CP, PRJ_VILLE, PRJ_PAYS, TAB_PROJET.ID_AO, TAB_PROJET.ID_CRMTECHNIQUE AS CCTECH_IDCRMCONTACT,
					AO_TITLE, AO_REF, TAB_PROJET.ID_CRMCONTACT, PRJ_TARIFADDITIONNEL, PRJ_INVESTISSEMENT, TAB_PROJET.ID_CRMCONTACT, TAB_PROJET.ID_CRMSOCIETE,
					CSOC.CSOC_SOCIETE, CSOC.CSOC_ADR, CSOC.CSOC_CP, CSOC.CSOC_VILLE, CSOC.CSOC_PAYS, CCON.CCON_NOM, CCON.CCON_PRENOM,
					CSTECH.CSOC_SOCIETE AS CSTECH_SOCIETE, CCTECH.ID_CRMSOCIETE AS CCTECH_IDCRMSOCIETE, CCTECH.CCON_NOM AS CCTECH_NOM, CCTECH.CCON_PRENOM AS CCTECH_PRENOM'
				)->addJoin('LEFT JOIN TAB_AO USING(ID_AO)
					LEFT JOIN TAB_CRMCONTACT CCON ON(CCON.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT)
					LEFT JOIN TAB_CRMSOCIETE CSOC ON(CSOC.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
					LEFT JOIN TAB_CRMCONTACT CCTECH ON(CCTECH.ID_CRMCONTACT=TAB_PROJET.ID_CRMTECHNIQUE)
					LEFT JOIN TAB_CRMSOCIETE CSTECH ON(CSTECH.ID_CRMSOCIETE=CCTECH.ID_CRMSOCIETE)');

				$result = $this->singleExec($baseQuery);
				if($result) {
					if($result['PRJ_TYPE'] == BM::PROJECT_TYPE_RECRUITMENT) //On récupère les données du candidat
						$result->CANDIDATE = $this->singleExec('SELECT ID_PROFIL , PROFIL_TYPE, PROFIL_NOM, PROFIL_PRENOM, ID_MISSIONPROJET FROM TAB_MISSIONPROJET INNER JOIN TAB_PROFIL ON(ID_PROFIL=ID_ITEM AND ITEM_TYPE=0) WHERE ID_PROJET=?', $id);

					//On récupère tous les documents
					$result->FILES = $this->exec('SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=2', $id);
				}
				break;
			case Models\Project::TAB_BATCHES_MARKERS://Jalons
				$baseQuery->addColumns('PRJ_DATELOTUPDATE, PRJ_RESTEAFAIRE, PRJ_LOTVIEW, PRJ_CORRELATIONJALON, PRJ_TARIFADDITIONNEL, PRJ_INVESTISSEMENT, GRPCONF_PRJALLOWJALONS')
				->addJoin(' LEFT JOIN TAB_GROUPECONFIG USING(ID_SOCIETE)');

				$result = $this->singleExec($baseQuery);
				if($result) {
					//Get all batches
					$result->LOTS = $this->exec('SELECT ID_LOT, LOT_TITRE, LOT_VALUE FROM TAB_LOT WHERE ID_PROJET=? ORDER BY ID_LOT ASC;', $id);

					//Get all markers
					$result->JALONS = $this->exec('SELECT ID_JALON, JALON_DATE, JALON_TITRE, JALON_VALUE, JALON_DUREE, ID_LOT, ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM FROM TAB_JALON LEFT JOIN TAB_PROFIL USING(ID_PROFIL) WHERE ID_PROJET=? ORDER BY ID_LOT ASC, ID_JALON ASC;', $id);

					//Get all deliveries resources
					$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, SUM(CASE WHEN MP_TYPE<>'.Models\Delivery::STATE_FORECAST.' THEN MP_TARIF*MP_NBJRSFACTURE+MP_TARIFADDITIONNEL ELSE 0 END) AS CASIGNEDHT
							FROM TAB_PROFIL INNER JOIN TAB_MISSIONPROJET ON(ID_ITEM=ID_PROFIL AND ITEM_TYPE=0)
							WHERE ID_PROJET=? GROUP BY TAB_PROFIL.ID_PROFIL ORDER BY TAB_PROFIL.ID_PROFIL ASC;';
					$result->RESOURCES = $this->exec($sql, $id);

					//Calculate sum of working days filled in timesheets
					//Calculate production turnover
					$sql = 'SELECT SUM(TEMPS_DUREE) AS DUREE, TAB_LISTETEMPS.ID_PROFIL, TAB_LIGNETEMPS.ID_LOT
							FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS ON(TAB_LISTETEMPS.ID_LISTETEMPS=TAB_LIGNETEMPS.ID_LISTETEMPS) INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
								INNER JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET)
							WHERE TAB_LIGNETEMPS.ID_LOT<>0 AND TAB_MISSIONPROJET.ID_PROJET=? GROUP BY TAB_LIGNETEMPS.ID_LOT, TAB_LISTETEMPS.ID_PROFIL;';
					$result->SUMTPS = $this->exec($sql, $id);
				}
				break;
			case Models\Project::TAB_SIMULATION:
				$baseQuery->addColumns('PRJ_CONSOMMATIONVIEW');
				$result = $this->singleExec($baseQuery);
				if($result) {
					$sql = 'SELECT TAB_MISSIONDETAILS.ID_ACHAT, ID_MISSIONDETAILS, MISDETAILS_DATE, MISDETAILS_ETAT, MISDETAILS_TARIF, MISDETAILS_INVESTISSEMENT, MISDETAILS_TITRE, ACHAT_TYPE FROM TAB_MISSIONDETAILS LEFT JOIN TAB_ACHAT USING(ID_ACHAT) WHERE ID_PARENT=? AND PARENT_TYPE=0 ORDER BY ID_MISSIONDETAILS ASC';
					$result->MISSIONSDETAILS = $this->exec($sql, $id);

					//On récupère toutes les ressources appartenant au projet
					$sql = 'SELECT TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM
							FROM TAB_PROFIL INNER JOIN TAB_MISSIONPROJET ON(ID_ITEM=ID_PROFIL AND ITEM_TYPE=0)
							WHERE ID_PROJET=? GROUP BY TAB_PROFIL.ID_PROFIL ORDER BY TAB_PROFIL.ID_PROFIL ASC;';
					$result->RESOURCES = $this->exec($sql, $id);
				}
				break;
			case Models\Project::TAB_DELIVERIES://Missions
				$baseQuery->addColumns('PRJ_DEBUT, PRJ_FIN');
				$result = $this->singleExec($baseQuery);
				break;
			case Models\Project::TAB_PRODUCTIVITY://Consommation
				$baseQuery->addColumns('PRJ_TARIFADDITIONNEL, PRJ_INVESTISSEMENT');
				$result = $this->singleExec($baseQuery);
				if($result) {
					//COUT DE PRODUCTION (Salaire Pointé + Frais + Achats + Avantages + Investissements)
					//CA DE PRODUCTION (Tarif Pointé + Frais Refacturés + Achats Refacturés
					$sql = 'SELECT SUM(TEMPS_DUREE) AS NBTEMPS, TAB_LISTETEMPS.ID_PROFIL, TAB_LIGNETEMPS.ID_MISSIONPROJET,
									   SUM(TEMPS_DUREE*IF(M1.ID_ACHAT=0,IF(M2.ID_MISSIONPROJET IS NULL AND PROFIL_TYPE<>' . Models\Employee::TYPE_EXTERNAL_RESOURCE . ',CTR_CJMPRODUCTION,M1.MP_PRJM),0)) AS COUTTEMPS,
									   SUM(TEMPS_DUREE*M1.MP_TARIF) AS CATEMPS, '.Models\Time::ACTIVITY_REGULAR_PRODUCTION.' AS TYPEH_TYPEACTIVITE
								 FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
									 INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
									 LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_PROFIL.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND PROFIL_TYPE<>' . Models\Employee::TYPE_EXTERNAL_RESOURCE . ' AND TEMPS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
								 WHERE M1.ID_PROJET=? GROUP BY TAB_LIGNETEMPS.ID_MISSIONPROJET, TAB_LISTETEMPS.ID_PROFIL
								  UNION ALL
								SELECT SUM(IF(TAB_TYPEHEURE.TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1,TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN))) AS NBTEMPS, TAB_LISTETEMPS.ID_PROFIL, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET,
									   0 AS COUTTEMPS,
									   SUM(TEMPSEXP_TARIF) AS CATEMPS, TYPEH_TYPEACTIVITE
								 FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
									 INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
									 LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
								 WHERE M1.ID_PROJET=? GROUP BY TAB_TYPEHEURE.TYPEH_TYPEACTIVITE, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET, TAB_LISTETEMPS.ID_PROFIL';
					$result->SUMTPS = $this->exec($sql, [$id, $id]);

					//Manage transfer delivery
					$sql = 'SELECT SUM(TEMPS_DUREE) AS NBTEMPS, TAB_LISTETEMPS.ID_PROFIL, TAB_LIGNETEMPS.ID_MISSIONPROJET,
									   SUM(TEMPS_DUREE*IF(M2.ID_ACHAT=0,IF(PROFIL_TYPE<>' . Models\Employee::TYPE_EXTERNAL_RESOURCE . ',CTR_CJMPRODUCTION,M2.MP_PRJM),0)) AS COUTTEMPS,
									   SUM(TEMPS_DUREE*M2.MP_TARIF) AS CATEMPS, '.Models\Time::ACTIVITY_REGULAR_PRODUCTION.' AS TYPEH_TYPEACTIVITE
								 FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
									 INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_LIGNETEMPS.ID_MISSIONPROJET)
									 LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_PROFIL.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND PROFIL_TYPE<>' . Models\Employee::TYPE_EXTERNAL_RESOURCE . ' AND TEMPS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
								 WHERE M2.ID_PROJET=? AND M2.ID_MASTER<>0 GROUP BY TAB_LIGNETEMPS.ID_MISSIONPROJET, TAB_LISTETEMPS.ID_PROFIL
								  UNION ALL
								 SELECT SUM(IF(TAB_TYPEHEURE.TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1,TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN))) AS NBTEMPS, TAB_LISTETEMPS.ID_PROFIL, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET,
									   0 AS COUTTEMPS,
									   SUM(TEMPSEXP_ACHAT) AS CATEMPS, TYPEH_TYPEACTIVITE
								 FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
									 INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET)
									  LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
								 WHERE M2.ID_PROJET=? AND M2.ID_MASTER<>0 GROUP BY TAB_TYPEHEURE.TYPEH_TYPEACTIVITE, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET, TAB_LISTETEMPS.ID_PROFIL';
					$sumTps2 = $this->exec($sql, [$id, $id]);
					foreach ($sumTps2 as $sum1) {
						$sumExist = -1;
						foreach($result->SUMTPS as $i => $sum2) if ($sum1['ID_PROFIL'] == $sum2['ID_PROFIL'] && $sum1['ID_MISSIONPROJET'] == $sum2['ID_MISSIONPROJET'] && ($sum1['TYPEH_TYPEACTIVITE'] == $sum2['TYPEH_TYPEACTIVITE'] || !isset($sum1['TYPEH_TYPEACTIVITE']) && !isset($sum2['TYPEH_TYPEACTIVITE']))) {
							$sumExist = $i;
							break;
						}

						if($sumExist >= 0) {
							$result->SUMTPS[$sumExist]['NBTEMPS'] += $sum1['NBTEMPS'];
							$result->SUMTPS[$sumExist]['COUTTEMPS'] += $sum1['COUTTEMPS'];
							$result->SUMTPS[$sumExist]['CATEMPS'] += $sum1['CATEMPS'];
						} else $result->SUMTPS[] = $sum1;
					}

					$sql = 'SELECT SUM(IF(M2.ID_MISSIONPROJET IS NULL OR FRAISREEL_REFACTURE=1,IF(FRAISREEL_TYPEFRSREF=0,FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAISREEL_MONTANT),0)) AS COUTFRAIS,
									   SUM(IF(FRAISREEL_REFACTURE=1,IF(FRAISREEL_TYPEFRSREF=0,FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAISREEL_MONTANT),0)) AS CAFRAIS,
									   TAB_LISTEFRAIS.ID_PROFIL, TAB_FRAISREEL.ID_MISSIONPROJET
								 FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
									INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_FRAISREEL.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
								 WHERE M1.ID_PROJET=? GROUP BY TAB_FRAISREEL.ID_MISSIONPROJET, TAB_LISTEFRAIS.ID_PROFIL
								UNION ALL
								SELECT SUM(IF(M2.ID_MISSIONPROJET IS NULL,IF(LIGNEFRAIS_TYPEFRSREF=0,FRAIS_VALUE*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAIS_VALUE),0)) AS COUTFRAIS,
									   0 AS CAFRAIS,
									   TAB_LISTEFRAIS.ID_PROFIL, TAB_LIGNEFRAIS.ID_MISSIONPROJET
								 FROM TAB_LIGNEFRAIS INNER JOIN TAB_FRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
									INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_LIGNEFRAIS.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
								 WHERE M1.ID_PROJET=? GROUP BY TAB_LIGNEFRAIS.ID_MISSIONPROJET, TAB_LISTEFRAIS.ID_PROFIL';
					$result->SUMFRS = $this->exec($sql, [$id, $id]);

					//Manage transfer delivery
					$sql = 'SELECT SUM(IF(FRAISREEL_TYPEFRSREF=0,FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAISREEL_MONTANT)) AS COUTFRAIS,
									   SUM(IF(FRAISREEL_REFACTURE=1,IF(FRAISREEL_TYPEFRSREF=0,FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAISREEL_MONTANT),0)) AS CAFRAIS,
									   TAB_LISTEFRAIS.ID_PROFIL, TAB_FRAISREEL.ID_MISSIONPROJET
								 FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
									INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_FRAISREEL.ID_MISSIONPROJET)
								 WHERE M2.ID_PROJET=? AND M2.ID_MASTER<>0 GROUP BY TAB_FRAISREEL.ID_MISSIONPROJET, TAB_LISTEFRAIS.ID_PROFIL
								UNION ALL
								SELECT SUM(IF(LIGNEFRAIS_TYPEFRSREF=0,FRAIS_VALUE*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAIS_VALUE)) AS COUTFRAIS,
									   0 AS CAFRAIS,
									   TAB_LISTEFRAIS.ID_PROFIL, TAB_LIGNEFRAIS.ID_MISSIONPROJET
								 FROM TAB_LIGNEFRAIS INNER JOIN TAB_FRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
									INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_LIGNEFRAIS.ID_MISSIONPROJET)
								 WHERE M2.ID_PROJET=? AND M2.ID_MASTER<>0 GROUP BY TAB_LIGNEFRAIS.ID_MISSIONPROJET, TAB_LISTEFRAIS.ID_PROFIL';
					$sumFrs2 = $this->exec($sql, [$id, $id]);
					foreach ($sumFrs2 as $sum1) {
						$sumExist = -1;
						foreach ($result->SUMFRS as $i => $sum2) if ($sum1['ID_PROFIL'] == $sum2['ID_PROFIL']) {
							$sumExist = $i;
							break;
						}
						if($sumExist >= 0) {
							$result->SUMFRS[$sumExist]['COUTFRAIS'] += $sum1['COUTFRAIS'];
							$result->SUMFRS[$sumExist]['CAFRAIS'] += $sum1['CAFRAIS'];
						} else $result->SUMFRS[] = $sum1;
					}

					$result->COUTAVANTAGES = $this->exec('SELECT SUM(AVG_COUT) AS COUTAVANTAGES, TAB_AVANTAGE.ID_PROFIL FROM TAB_AVANTAGE  WHERE TAB_AVANTAGE.ID_PROJET=? GROUP BY TAB_AVANTAGE.ID_PROFIL', $id);

					$sql = 'SELECT SUM(CASE WHEN PMT_ETAT<>'.Models\Payment::STATE_PLANNED.' THEN PMT_MONTANTHT ELSE 0 END) AS VALUE FROM TAB_ACHAT INNER JOIN TAB_PAIEMENT USING(ID_ACHAT) WHERE TAB_ACHAT.ID_PROJET=? AND ACHAT_ETAT<>'.Models\Purchase::STATE_PLANNED
							.' UNION ALL
							SELECT SUM(CASE WHEN ACHAT_REFACTURE=1 THEN ACHAT_TARIFFACTURE ELSE 0 END) AS VALUE FROM TAB_ACHAT WHERE TAB_ACHAT.ID_PROJET=? AND ACHAT_ETAT<>'.Models\Purchase::STATE_PLANNED;
					$sumAchats = $this->exec($sql, [$id, $id]);
					$result->CAACHATS = (isset($sumAchats[1]['VALUE'])) ? $sumAchats[1]['VALUE'] : 0;
					$result->COUTACHATS = (isset($sumAchats[0]['VALUE'])) ? $sumAchats[0]['VALUE'] : 0;

					//On récupère les CA & Investissements additionnels de production
					$sql = 'SELECT SUM(MISDETAILS_TARIF) AS CAADDITIONNEL, SUM(MISDETAILS_INVESTISSEMENT) AS COUTADDITIONNEL
								 FROM TAB_MISSIONDETAILS INNER JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_MISSIONDETAILS.ID_PARENT AND PARENT_TYPE='.Models\DeliveryDetail::PARENT_DELIVERY_KEY.')
								 WHERE TAB_MISSIONDETAILS.ID_ACHAT=0 AND MISDETAILS_ETAT='.Models\DeliveryDetail::STATE_ON.' AND TAB_MISSIONPROJET.ID_PROJET=?
								UNION ALL
								SELECT SUM(MISDETAILS_TARIF) AS CAADDITIONNEL, SUM(MISDETAILS_INVESTISSEMENT) AS COUTADDITIONNEL
								 FROM TAB_MISSIONDETAILS
								 WHERE TAB_MISSIONDETAILS.ID_ACHAT=0 AND MISDETAILS_ETAT='.Models\DeliveryDetail::STATE_ON.' AND ID_PARENT=? AND PARENT_TYPE='.Models\DeliveryDetail::PARENT_PROJECT_KEY;
					$sumAdditionnel = $this->exec($sql, [$id, $id]);
					$result->CAADDITIONNEL = ((isset($sumAdditionnel[0]['CAADDITIONNEL'])) ? $sumAdditionnel[0]['CAADDITIONNEL'] : 0) + ((isset($sumAdditionnel[1]['CAADDITIONNEL'])) ? $sumAdditionnel[1]['CAADDITIONNEL'] : 0);
					$result->COUTADDITIONNEL = ((isset($sumAdditionnel[0]['COUTADDITIONNEL'])) ? $sumAdditionnel[0]['COUTADDITIONNEL'] : 0) + ((isset($sumAdditionnel[1]['COUTADDITIONNEL'])) ? $sumAdditionnel[1]['COUTADDITIONNEL'] : 0);
				}
				break;
			case Models\Project::TAB_ORDERS://Facturation
				$baseQuery->addColumns([
					'PRJ_TARIFADDITIONNEL', 'PRJ_INVESTISSEMENT', 'PRJ_DEBUT', 'PRJ_FIN',
					'TAB_PROJET.ID_CRMCONTACT', 'TAB_PROJET.ID_CRMSOCIETE', 'CSOC.CSOC_SOCIETE', 'CSOC.CSOC_TVA', 'CCON.CCON_NOM', 'CCON.CCON_PRENOM',
					'TAB_PROJET.ID_AO', 'AO_TITLE'
				]);
				$baseQuery->addJoin('LEFT JOIN TAB_AO USING (ID_AO)');
				$baseQuery->addJoin('LEFT JOIN TAB_CRMCONTACT CCON ON(CCON.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) LEFT JOIN TAB_CRMSOCIETE CSOC ON(CSOC.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)');

				$result = $this->singleExec($baseQuery);

				if(!$result) break;

				$cession = false;
				$sumMissions = $this->singleExec('SELECT SUM(MP_TARIFADDITIONNEL) AS CA1, SUM(IF(ID_PARENT=0,MP_TARIF*MP_NBJRSFACTURE,0)) AS CA2, SUM(MP_INVESTISSEMENT) AS COUT1, SUM(MP_CJM*(MP_NBJRSFACTURE+MP_NBJRSGRATUIT)) AS COUT2, SUM(ID_MASTER) AS SUM_IDMASTER FROM TAB_MISSIONPROJET WHERE ID_PROJET= ? AND MP_TYPE<>1', $result['ID_PROJET']);
				if($sumMissions) {
					$result['CASIGNE'] = $sumMissions['CA1']+$sumMissions['CA2'];
					$result['COUTSIGNE'] = $sumMissions['COUT1']+$sumMissions['COUT2'];
					if($result['PRJ_TYPE'] == BM::PROJECT_TYPE_PRODUCT)
						$result['CAPRODUCTION'] += $sumMissions['CA2'];
					if($sumMissions['SUM_IDMASTER'] != 0)
						$cession = true;
				}


				$sql = "SELECT SUM(TEMPS_DUREE*IF(M1.ID_ACHAT=0,IF(M2.ID_MISSIONPROJET IS NULL AND PROFIL_TYPE<>".Models\Employee::TYPE_EXTERNAL_RESOURCE.",CTR_CJMPRODUCTION,M1.MP_PRJM),0)) AS COUTTEMPS,
									   SUM(TEMPS_DUREE*M1.MP_TARIF) AS CATEMPS
								 FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
									 INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
									 LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_PROFIL.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND PROFIL_TYPE<>".Models\Employee::TYPE_EXTERNAL_RESOURCE." AND TEMPS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
								 WHERE M1.ID_PROJET = :idprojet
								  UNION ALL
								SELECT 0 AS COUTTEMPS,
									   SUM(TEMPSEXP_TARIF) AS CATEMPS
								 FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
									 INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
								 WHERE M1.ID_PROJET = :idprojet ";

				$sumTps = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);

				$result['CAPRODUCTION'] += ((isset($sumTps[0]['CATEMPS']))?$sumTps[0]['CATEMPS']:0) + ((isset($sumTps[1]['CATEMPS']))?$sumTps[1]['CATEMPS']:0);
				$result['TOTAL_COUT'] += ((isset($sumTps[0]['COUTTEMPS']))?$sumTps[0]['COUTTEMPS']:0) + ((isset($sumTps[1]['COUTTEMPS']))?$sumTps[1]['COUTTEMPS']:0);

				if($cession) {//Uniquement si il y a au moins une cession
					$sql = "SELECT SUM(TEMPS_DUREE*IF(M2.ID_ACHAT=0,IF(PROFIL_TYPE<>".Models\Employee::TYPE_EXTERNAL_RESOURCE.",CTR_CJMPRODUCTION,M2.MP_PRJM),0)) AS COUTTEMPS,
										   SUM(TEMPS_DUREE*M2.MP_TARIF) AS CATEMPS
									 FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
										 INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_LIGNETEMPS.ID_MISSIONPROJET)
										 LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_PROFIL.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND PROFIL_TYPE<>".Models\Employee::TYPE_EXTERNAL_RESOURCE." AND TEMPS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
									 WHERE M2.ID_PROJET= :idprojet AND M2.ID_MASTER<>0
									  UNION ALL
									 SELECT 0 AS COUTTEMPS,
										   SUM(TEMPSEXP_ACHAT) AS CATEMPS
									 FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
										 INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET)
									 WHERE M2.ID_PROJET= :idprojet AND M2.ID_MASTER<>0;";

					$sumTps = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);

					$result['CAPRODUCTION'] += ((isset($sumTps[0]['CATEMPS']))?$sumTps[0]['CATEMPS']:0) + ((isset($sumTps[1]['CATEMPS']))?$sumTps[1]['CATEMPS']:0);
					$result['TOTAL_COUT'] += ((isset($sumTps[0]['COUTTEMPS']))?$sumTps[0]['COUTTEMPS']:0) + ((isset($sumTps[1]['COUTTEMPS']))?$sumTps[1]['COUTTEMPS']:0);
				}

				$sql = 'SELECT SUM(IF(M2.ID_MISSIONPROJET IS NULL OR FRAISREEL_REFACTURE=1,CASE WHEN FRAISREEL_TYPEFRSREF=0 THEN FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE ELSE FRAISREEL_MONTANT END,0)) AS COUTFRAIS,
									   SUM(IF(FRAISREEL_REFACTURE=1,IF(FRAISREEL_TYPEFRSREF=0,FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAISREEL_MONTANT),0)) AS CAFRAIS
								 FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
									INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_FRAISREEL.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
								 WHERE M1.ID_PROJET= :idprojet
								UNION ALL
								SELECT SUM(IF(M2.ID_MISSIONPROJET IS NULL,IF(LIGNEFRAIS_TYPEFRSREF=0,FRAIS_VALUE*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAIS_VALUE),0)) AS COUTFRAIS,
									   0 AS CAFRAIS
								 FROM TAB_LIGNEFRAIS INNER JOIN TAB_FRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
									INNER JOIN TAB_MISSIONPROJET M1 ON(M1.ID_MISSIONPROJET=TAB_LIGNEFRAIS.ID_MISSIONPROJET) LEFT JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=M1.ID_MISSIONPROJET)
								 WHERE M1.ID_PROJET= :idprojet';

				$sumFrs = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);

				$result['CAPRODUCTION'] += ((isset($sumFrs[0]['CAFRAIS']))?$sumFrs[0]['CAFRAIS']:0) + ((isset($sumFrs[1]['CAFRAIS']))?$sumFrs[1]['CAFRAIS']:0);
				$result['TOTAL_COUT'] += ((isset($sumFrs[0]['COUTFRAIS']))?$sumFrs[0]['COUTFRAIS']:0) + ((isset($sumFrs[1]['COUTFRAIS']))?$sumFrs[1]['COUTFRAIS']:0);

				if($cession) {//Uniquement si il y a au moins une cession
					$sql = 'SELECT SUM(CASE WHEN FRAISREEL_TYPEFRSREF=0 THEN FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE ELSE FRAISREEL_MONTANT END) AS COUTFRAIS,
										   SUM(IF(FRAISREEL_REFACTURE=1,IF(FRAISREEL_TYPEFRSREF=0,FRAISREEL_MONTANT*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAISREEL_MONTANT),0)) AS CAFRAIS
									 FROM TAB_FRAISREEL INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
										INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_FRAISREEL.ID_MISSIONPROJET)
									 WHERE M2.ID_PROJET= :idprojet AND M2.ID_MASTER<>0
									UNION ALL
									SELECT SUM(IF(LIGNEFRAIS_TYPEFRSREF=0,FRAIS_VALUE*LISTEFRAIS_BKMVALUE*LISTEFRAIS_CHANGEAGENCE,FRAIS_VALUE)) AS COUTFRAIS,
										   0 AS CAFRAIS
									 FROM TAB_LIGNEFRAIS INNER JOIN TAB_FRAIS USING(ID_LIGNEFRAIS) INNER JOIN TAB_LISTEFRAIS USING(ID_LISTEFRAIS)
										INNER JOIN TAB_MISSIONPROJET M2 ON(M2.ID_MASTER=TAB_LIGNEFRAIS.ID_MISSIONPROJET)
									 WHERE M2.ID_PROJET = :idprojet AND M2.ID_MASTER<>0';

					$sumFrs = $this->exec($sql, ['idprojet' => $result['ID_PROJET']]);

					$result['CAPRODUCTION'] += ((isset($sumFrs[0]['CAFRAIS']))?$sumFrs[0]['CAFRAIS']:0) + ((isset($sumFrs[1]['CAFRAIS']))?$sumFrs[1]['CAFRAIS']:0);
					$result['TOTAL_COUT'] += ((isset($sumFrs[0]['COUTFRAIS']))?$sumFrs[0]['COUTFRAIS']:0) + ((isset($sumFrs[1]['COUTFRAIS']))?$sumFrs[1]['COUTFRAIS']:0);
				}

				$sumAvantages = $this->exec('SELECT SUM(AVG_COUT) AS COUTAVANTAGES FROM TAB_AVANTAGE WHERE TAB_AVANTAGE.ID_PROJET=?', $result['ID_PROJET']);

				$result['TOTAL_COUT'] += ($sumAvantages)?$sumAvantages['COUTAVANTAGES']:0;

				$sql = 'SELECT SUM(CASE WHEN PMT_ETAT<>0 THEN PMT_MONTANTHT ELSE 0 END) AS VALUE FROM TAB_ACHAT INNER JOIN TAB_PAIEMENT USING(ID_ACHAT) WHERE TAB_ACHAT.ID_PROJET=:idprojet AND ACHAT_ETAT<>9
								 UNION ALL
								SELECT SUM(CASE WHEN ACHAT_REFACTURE=1 THEN ACHAT_TARIFFACTURE ELSE 0 END) AS VALUE FROM TAB_ACHAT WHERE TAB_ACHAT.ID_PROJET=:idprojet AND ACHAT_ETAT<>9;';
				$sumAchats = $this->exec($sql, ['idprojet'=>$result['ID_PROJET']]);
				$result['CAPRODUCTION'] += ((isset($sumAchats[1]['VALUE']))?$sumAchats[1]['VALUE']:0);
				$result['TOTAL_COUT'] += ((isset($sumAchats[0]['VALUE']))?$sumAchats[0]['VALUE']:0);

				//On récupère les CA & Investissements additionnels de production
				$sql = 'SELECT SUM(MISDETAILS_TARIF) AS CAADDITIONNEL, SUM(MISDETAILS_INVESTISSEMENT) AS COUTADDITIONNEL
								 FROM TAB_MISSIONDETAILS INNER JOIN TAB_MISSIONPROJET ON(ID_MISSIONPROJET=TAB_MISSIONDETAILS.ID_PARENT AND PARENT_TYPE=1)
								 WHERE TAB_MISSIONDETAILS.ID_ACHAT=0 AND MISDETAILS_ETAT=1 AND TAB_MISSIONPROJET.ID_PROJET=:idprojet
								UNION ALL
								SELECT SUM(MISDETAILS_TARIF) AS CAADDITIONNEL, SUM(MISDETAILS_INVESTISSEMENT) AS COUTADDITIONNEL
								 FROM TAB_MISSIONDETAILS
								 WHERE TAB_MISSIONDETAILS.ID_ACHAT=0 AND MISDETAILS_ETAT=1 AND ID_PARENT=:idprojet AND PARENT_TYPE=0';

				$sumAdditionnel = $this->exec($sql, ['idprojet'=>$result['ID_PROJET']]);

				$result['CAPRODUCTION'] += ((isset($sumAdditionnel[0]['CAADDITIONNEL']))?$sumAdditionnel[0]['CAADDITIONNEL']:0) + ((isset($sumAdditionnel[1]['CAADDITIONNEL']))?$sumAdditionnel[1]['CAADDITIONNEL']:0);
				$result['TOTAL_COUT'] += ((isset($sumAdditionnel[0]['COUTADDITIONNEL']))?$sumAdditionnel[0]['COUTADDITIONNEL']:0) + ((isset($sumAdditionnel[1]['COUTADDITIONNEL']))?$sumAdditionnel[1]['COUTADDITIONNEL']:0);

				break;
		}
		return $result;
	}

	/**
	 * Create a project
	 * @param array $data
	 * @return int
	 */
	public function createProject($data) {
		if(!isset($data['PROJET']['PRJ_DATE'])) $data['PROJET']['PRJ_DATE'] = date('Y-m-d H:i:s');
		if(!isset($data['PROJET']['PRJ_DEBUT'])) $data['PROJET']['PRJ_DEBUT'] = date('Y-m-d');
		if(!isset($data['PROJET']['PRJ_FIN'])) $data['PROJET']['PRJ_FIN'] = date('Y-m-d');
		$data['PROJET']['PRJ_DATEUPDATE'] = date('Y-m-d H:i:s', time());
		$id = $this->insert('TAB_PROJET', $data['PROJET']);

		if($id && isset($data['MISSIONDETAILS'])) {
			foreach($data['MISSIONDETAILS'] as $detail) {
				$detail['ID_PARENT'] = $id;
				$detail['PARENT_TYPE'] = 0;
				$this->insert('TAB_MISSIONDETAILS', $detail);
			}
		}
		return $id;
	}

	/**
	 * Update project
	 * @param int $id
	 * @param array $data
	 * @return boolean
	 */
	public function updateProject($id, $data) {
		$data['PROJET']['PRJ_DATEUPDATE'] = date('Y-m-d H:i:s', time());
		$this->update('TAB_PROJET', $data['PROJET'], 'ID_PROJET=:id', ['id' => $id]);

		if(isset($data['MISSIONDETAILS'])) $this->deleteAndCreateItemsFromEntity($data['MISSIONDETAILS'], 'TAB_MISSIONDETAILS', 'ID_MISSIONDETAILS', ['PARENT_TYPE' => 0, 'ID_PARENT' => $id, 'ID_ACHAT' => 0]);

		if(isset($data['JALONSUNIQUE'])) $this->deleteAndCreateItemsFromEntity($data['JALONSUNIQUE'], 'TAB_JALON', 'ID_JALON', ['ID_PROJET' => $id]);

		if(isset($data['LOTSJALONS'])) {
			$tabBatches = $this->deleteAndCreateItemsFromEntity($data['LOTSJALONS'], 'TAB_LOT', 'ID_LOT', ['ID_PROJET' => $id]);
			foreach($tabBatches as $batch) {
				if(isset($batch['JALONS']))
					$this->deleteAndCreateItemsFromEntity($batch['JALONS'], 'TAB_JALON', 'ID_JALON', ['ID_PROJET' => $id, 'ID_LOT' => $batch['ID_LOT']]);
			}
		}
		return true;
	}

	/**
	 * Delete project
	 * @param int $id
	 * @param boolean $solr
	 * @return boolean
	 */
	public function deleteProject($id, $solr = true) {
		//Delete all deliveries
		$dbResource = new Employee();
		$dbDelivery = new Delivery();
		$sql = 'SELECT ID_MISSIONPROJET, ID_ITEM, ITEM_TYPE FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE TAB_PROJET.ID_PROJET=?';
		foreach($this->exec($sql, $id) as $mission) {
			$dbDelivery->deleteDelivery($mission['ID_MISSIONPROJET']);
			//Update availability employee
			if($mission['ITEM_TYPE'] == 0) $dbResource->updateResourceAvailability($mission['ID_ITEM'], true, false, $solr);
		}

		//Delete all actions
		$dict1 = Actions::getDictionaryForCategory(BM::CATEGORY_PROJECT);
		$dict2 = Dictionary::getDict('specific.setting.collaborative.project');
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict1, 'id') + array_column($dict2, 'id'));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//Delete all files
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_DOCUMENT, $id, Models\Document::TYPE_PROJECT);

		//Delete all flags
		$dbFlag = new AttachedFlag();
		$dbFlag->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_PROJECT);

		$this->delete('TAB_AVANTAGE', 'ID_PROJET=?', $id);
		$this->delete('TAB_JALON', 'ID_PROJET=?', $id);
		$this->delete('TAB_LOT', 'ID_PROJET=?', $id);
		$this->delete('TAB_MISSIONDETAILS', 'ID_PARENT=? AND PARENT_TYPE=0', $id);

		//Delete all orders
		$commands = $this->exec('SELECT ID_BONDECOMMANDE FROM TAB_BONDECOMMANDE WHERE ID_PROJET=?', $id);
		$dbOrder = new Order();
		foreach($commands as $command)
			$dbOrder->deleteOrder($command->ID_BONDECOMMANDE);

		//Delete project
		$this->delete('TAB_PROJET', 'ID_PROJET=?', $id);
		return true;
	}

	/**
	 * Does project reducible ?
	 * @param int $id
	 * @return boolean
	 */
	function isProjectReducible($id) {
		$nbDocument = 0;
		$query = 'SELECT COUNT(ID_MISSIONPROJET) AS NB_DOCUMENT FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET) WHERE PRJ_TYPE<>'.BM::PROJECT_TYPE_RECRUITMENT.' AND ID_PROJET=:id
				UNION ALL
				SELECT COUNT(ID_ACHAT) AS NB_DOCUMENT FROM TAB_ACHAT WHERE ID_PROJET=:id
				UNION ALL
				SELECT COUNT(ID_BONDECOMMANDE) AS NB_DOCUMENT FROM TAB_BONDECOMMANDE WHERE ID_PROJET=:id
				UNION ALL
				SELECT COUNT(ID_AVANTAGE) AS NB_DOCUMENT FROM TAB_AVANTAGE WHERE ID_PROJET=:id';
		foreach($this->exec($query, ['id'=>$id]) as $document) $nbDocument += $document['NB_DOCUMENT'];
		return ($nbDocument == 0);
	}

	/**
	 * @param $maskSelect
	 * @param $maskWhere
	 * @param $id
	 * @return string
	 */
	function calculateReferenceMax($maskSelect, $maskWhere, $id) {
		$result = $this->singleExec('SELECT MAX(CAST(SUBSTRING(PRJ_REFERENCE,'.(strlen($maskSelect)+2).') AS UNSIGNED)) AS MAX_REFERENCE FROM TAB_PROJET WHERE PRJ_REFERENCE LIKE \''.$this->escape($maskWhere).'%\' AND ID_PROJET<>'.$this->escape($id).';');
		return $result ? $maxReference = $maskSelect.'_'.$result['MAX_REFERENCE'] : $maxReference = $maskSelect;
	}
}
