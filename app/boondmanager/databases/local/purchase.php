<?php
/**
 * purchase.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\Purchases\Filters\SearchPurchases;
use BoondManager\Services\BM;
use BoondManager\Models;
use BoondManager\Services;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;

/**
 * Handle all database work related to purchases
 * @namespace \BoondManager\Models\MySQL\Local
 */
class Purchase extends AbstractObject {

	/**
	 * Do a search based on the given filter
	 *
	 * @param SearchPurchases $filter
	 * @return SearchResult
	 * @throws \Exception
	 */
	public function searchPurchases(SearchPurchases $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));

		$query->addColumns('PRC.ID_ACHAT, RESP.ID_PROFIL, RESP.PROFIL_NOM, RESP.PROFIL_PRENOM, ID_USER, PRJ_REFERENCE, PRC.ID_PROJET, PRC.ID_SOCIETE, PRC.ID_POLE,
		ACHAT_MONTANTHT*ACHAT_QUANTITE AS TOTAL_MONTANT, ACHAT_REFACTURE, ACHAT_TARIFFACTURE, ACHAT_TITLE, ACHAT_REF, ACHAT_REFFOURNISSEUR,
		ACHAT_ETAT, ACHAT_TYPE, ACHAT_CATEGORIE, ACHAT_DATE, ACHAT_MONTANTHT, ACHAT_QUANTITE, ACHAT_TAUXTVA, ACHAT_CONDREGLEMENT, ACHAT_DEVISE,
		ACHAT_CHANGE, ACHAT_DEVISEAGENCE, ACHAT_CHANGEAGENCE, ACHAT_DEBUT, ACHAT_FIN, AT.ID_PROFIL AS EXT_ID, AT.PROFIL_NOM AS EXT_NOM,
		AT.PROFIL_PRENOM AS EXT_PRENOM, TAB_CRMCONTACT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, TAB_CRMSOCIETE.ID_CRMSOCIETE, CSOC_SOCIETE,
		COUNT(DISTINCT ID_BONDECOMMANDE) AS NB_COR, SUM(PMT_MONTANTHT)/CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE
		COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_PAIEMENTS,
		SUM(CASE WHEN PMT_ETAT<>0 THEN PMT_MONTANTHT ELSE 0 END)/CASE WHEN COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE)=0 THEN 1 ELSE
		COUNT(DISTINCT ID_CORRELATIONBONDECOMMANDE) END AS TOTAL_CONSOMMES');
		$query->from('TAB_ACHAT PRC');
		$query->addJoin('LEFT JOIN TAB_PAIEMENT ON(TAB_PAIEMENT.ID_ACHAT=PRC.ID_ACHAT)
						LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=PRC.ID_PROFIL)
						LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)
						LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=PRC.ID_CRMCONTACT)
						LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=PRC.ID_CRMSOCIETE)
						LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=PRC.ID_PROJET)
						LEFT JOIN TAB_CORRELATIONBONDECOMMANDE ON(TAB_CORRELATIONBONDECOMMANDE.ID_ITEM=PRC.ID_ACHAT AND CORBDC_TYPE=1)
						LEFT JOIN TAB_MISSIONPROJET MP ON(MP.ID_ACHAT=PRC.ID_ACHAT AND MP.ID_PROJET=TAB_PROJET.ID_PROJET)
						LEFT JOIN TAB_PROFIL AT ON(AT.ID_PROFIL=MP.ID_ITEM AND MP.ITEM_TYPE=0)');
		$query->groupBy('PRC.ID_ACHAT');


		$where = new Where();
		$where->and_($this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'PRC.ID_SOCIETE',
			$colPole = 'PRC.ID_POLE',
			$colUser = 'PRC.ID_PROFIL',
			true,
			$filter->narrowPerimeter->getValue()
		));

		$keywordsMapping = ['ACH'=>'PRC.ID_ACHAT', 'CCON'=>'TAB_CRMCONTACT.ID_CRMCONTACT', 'CSOC'=>'TAB_CRMSOCIETE.ID_CRMSOCIETE', 'PRJ'=>'PRC.ID_PROJET', 'COMP'=>'AT.ID_PROFIL'];
		$whereKeywords = $this->getListIdSearch($filter->keywords->getValue(), $keywordsMapping);

		if (! $whereKeywords->isEmpty())
			$where->and_($whereKeywords);
		else if ($filter->keywords->getValue()) {
			$keywords = str_replace('%', '\%', $filter->keywords->getValue());
			$keywords = str_replace('*', '%', $keywords);
			$likeSearch = $keywords.'%';

			$whereKeywords = new Where('MATCH(ACHAT_TITLE) AGAINST(?)', $keywords);
			$whereKeywords->or_('TAB_CRMCONTACT.CCON_NOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMCONTACT.CCON_PRENOM LIKE ?', $likeSearch)
						  ->or_('TAB_CRMSOCIETE.CSOC_SOCIETE LIKE ?', $likeSearch)
						  ->or_('PRJ_REFERENCE LIKE ?', $likeSearch)
						  ->or_('ACHAT_REF LIKE ?', $likeSearch)
						  ->or_('ACHAT_REFFOURNISSEUR LIKE ?', $likeSearch)
						  ->or_('PMT_REFPROVIDER LIKE ?', $likeSearch);

			$where->and_($whereKeywords);
		}

		$where->and_( $this->getFilterSearch($filter->purchaseStates->getValue(), 'ACHAT_ETAT') )
				->and_( $this->getFilterSearch($filter->subscriptionTypes->getValue(), 'ACHAT_TYPE') )
				->and_( $this->getFilterSearch($filter->purchaseTypes->getValue(), 'ACHAT_CATEGORIE') )
				->and_( $this->getFilterSearch($filter->paymentMethods->getValue(), 'ACHAT_TYPEPAYMENT') );

		switch ($filter->period->getValue()) {
			case SearchPurchases::PERIOD_CREATED:
				$query->addWhere('ACHAT_DATE BETWEEN ? AND ?', $filter->getDatesPeriod());
				break;
			case SearchPurchases::PERIOD_SUBSCRIPTION:
				$query->addWhere('DATEDIFF(ACHAT_FIN,?)>=0 && DATEDIFF(?,ACHAT_DEBUT)>=0', $filter->getDatesPeriod());
				break;
			default:case BM::INDIFFERENT:break;
		}

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0) {
			$query->addJoin('INNER JOIN TAB_FLAG ON(FLAG_TYPE='.BM::CATEGORY_PURCHASE.' AND TAB_FLAG.ID_PARENT=PRC.ID_ACHAT)');
			$where->and_('TAB_FLAG.ID_USERFLAG IN (?)', $filter->flags->getValue());
		}

		$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		$query->addWhere($where);

		$result = $this->launchSearch($query);
		$result->TOTAL_MONTANT = $result->TOTAL_PAIEMENTS = $result->TOTAL_CONSOMMES = 0;

		if($result->total > 0){
			$montant = $this->singleExec('SELECT SUM(ACHAT_MONTANTHT*ACHAT_QUANTITE) AS TOTAL_MONTANT FROM (SELECT DISTINCT PRC.ID_ACHAT, ACHAT_TYPE, ACHAT_DEBUT, ACHAT_FIN, ACHAT_MONTANTHT, ACHAT_QUANTITE FROM '.$query->getFrom().' '. $query->buildJoins().' '.$query->buildWhere().') PRC', $query->getArgs());
			if($montant) $result->TOTAL_MONTANT += $montant->TOTAL_MONTANT;

			$total_paiements = $this->singleExec('SELECT SUM(PMT_MONTANTHT) AS TOTAL_PAIEMENTS, SUM(CASE WHEN PMT_ETAT<>0 THEN PMT_MONTANTHT ELSE 0 END) AS TOTAL_CONSOMMES FROM (SELECT DISTINCT ID_PAIEMENT, PMT_DATE, PMT_MONTANTHT, PMT_COMMENTAIRES, PMT_ETAT FROM '.$query->getFrom().' '. $query->buildJoins().' '.$query->buildWhere().') TAB_PAIEMENT', $query->getArgs());
			if($total_paiements){
				$result->TOTAL_PAIEMENTS += $total_paiements->TOTAL_PAIEMENTS;
				$result->TOTAL_CONSOMMES += $total_paiements->TOTAL_CONSOMMES;
			}
		}
		$result->deltaExcludingTax = $result->TOTAL_CONSOMMES - $result->TOTAL_MONTANT;
		$result->engagedPaymentsAmountExcludingTax = $result->TOTAL_CONSOMMES;
		$result->totalAmountExcludingTax = $result->TOTAL_MONTANT;

		return $result;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order)
	{
		$mapping = [
			SearchPurchases::ORDERBY_CREATIONDATE         => 'ACHAT_DATE',
			SearchPurchases::ORDERBY_STATE                => 'ACHAT_ETAT',
			SearchPurchases::ORDERBY_TITLE                => 'ACHAT_TITLE',
			SearchPurchases::ORDERBY_PROJECT_REFERENCE    => 'PRJ_REFERENCE',
			SearchPurchases::ORDERBY_COMPANY_NAME         => 'CSOC_SOCIETE',
			SearchPurchases::ORDERBY_MAINMANAGER_LASTNAME => 'PROFIL_NOM'
		];

		if(!$column) $query->addOrderBy('ACHAT_DATE DESC');
		foreach ($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('ID_ACHAT DESC');
	}

	/**
	 * Update a purchase
	 * @param array $data data to rewrite
	 * @param int $idachat Purchase ID
	 * @return boolean
	 */
	public function updatePurchase($data, $idachat)
	{
		if(isset($data['ACHAT'])) {
			$this->update('TAB_ACHAT', $data['ACHAT'], 'ID_ACHAT = :id', ['id'=>$idachat]);
		}
		if(isset($data['MISSIONDETAILS'])) {
			if(isset($data['MISSIONDETAILS']['ID_MISSIONDETAILS'])) {
				$this->update('TAB_MISSIONDETAILS', $data['MISSIONDETAILS'], 'ID_MISSIONDETAILS=:id AND ID_ACHAT=:idachat', [
					'id' => $data['MISSIONDETAILS']['ID_MISSIONDETAILS'],
					'idachat' => $idachat
				]);
			} else {
				$data['MISSIONDETAILS']['ID_ACHAT'] = $idachat;
				$this->insert('TAB_MISSIONDETAILS', $data['MISSIONDETAILS']);
			}
		}
		return true;
	}

	/**
	 * Create a new purchase
	 * @param array $data tableau de données
	 * @return int $iddetail Detail ID
	 */
	public function newPurchase($data)
	{
		$idachat = false;
		if(isset($data['ACHAT'])) {
			if(!isset($data['ACHAT']['ACHAT_TAUXREFACTURATION'])) $data['ACHAT']['ACHAT_TAUXREFACTURATION'] = 'NULL';
			$idachat = $this->insert('TAB_ACHAT', $data['ACHAT']);
		}

		if($idachat && isset($data['MISSIONDETAILS'])) {
			$data['MISSIONDETAILS']['ID_ACHAT'] = $idachat;
			if($data['MISSIONDETAILS']['ID_MISSIONDETAILS'] > 0) {
				$this->update('TAB_MISSIONDETAILS', $data['MISSIONDETAILS'], 'ID_MISSIONDETAILS= :id AND ID_ACHAT = :idachat', [
					'id' => $data['MISSIONDETAILS']['ID_MISSIONDETAILS'] ,
					'idachat' => $idachat
				]);
			} else {
				$this->insert('TAB_MISSIONDETAILS', $data['MISSIONDETAILS']);
			}
		}
		return $idachat;
	}

	/**
	 * Retrieve a purchase
	 * @param int $id identifiant du besoin
	 * @param mixed $onglet tab id
	 * @return Models\Purchase
	 * @throws \Exception
	 */
	public function getPurchase($id, $onglet=0){
		$baseQuery = new Query();
		$baseQuery->select([
			'TAB_ACHAT.ID_ACHAT', 'TAB_ACHAT.ID_PROJET', 'ACHAT_TITLE', 'ACHAT_MONTANTHT', 'ACHAT_QUANTITE', 'TAB_ACHAT.ID_SOCIETE',
			'TAB_ACHAT.ID_POLE', 'TAB_ACHAT.ID_PROFIL', 'TAB_ACHAT.ID_CRMCONTACT', 'TAB_ACHAT.ID_CRMSOCIETE', 'ACHAT_TAUXTVA'
		])
			->from('TAB_ACHAT')
			->addWhere('TAB_ACHAT.ID_ACHAT=?', $id)
			->setModelClass(Models\Purchase::class)
			/*->setMapper( new Mappers\Purchase() )*/;

		switch($onglet) {//On récupère + de détails suivant l'onglet à afficher
			case Models\Purchase::TAB_INFORMATION:

				$baseQuery->addColumns([
					'ID_COORDONNEES', 'PRJ_REFERENCE', 'ACHAT_CATEGORIE', 'ACHAT_TYPEPAYMENT', 'ACHAT_DATE', 'ACHAT_REF', 'ACHAT_REFFOURNISSEUR',
					'ACHAT_TYPE', 'ACHAT_ETAT', 'ACHAT_DEVISE', 'ACHAT_CHANGE', 'ACHAT_DEVISEAGENCE', 'ACHAT_CHANGEAGENCE',
					'ACHAT_CONDREGLEMENT', 'ACHAT_DEBUT', 'ACHAT_FIN', 'ACHAT_COMMENTAIRE', 'ACHAT_REFACTURE', 'ACHAT_TARIFFACTURE',
					'ACHAT_TAUXREFACTURATION', 'ACHAT_SHOWCOMMENTAIRE', 'CCON_NOM', 'CCON_PRENOM', 'CSOC_SOCIETE',
					'CSOC_TVA', 'ID_MISSIONDETAILS', 'TAB_MISSIONDETAILS.ID_PARENT', 'PARENT_TYPE', 'TAB_MISSIONPROJET.ID_MISSIONPROJET',
					'MP_NOM'
				])
				->addJoin('LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_ACHAT.ID_CRMCONTACT)')
				->addJoin('LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_ACHAT.ID_CRMSOCIETE) ')
				->addJoin('LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_ACHAT.ID_PROFIL)')
				->addJoin('LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_ACHAT.ID_PROJET)')
				->addJoin('LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_ACHAT=TAB_ACHAT.ID_ACHAT)')
				->addJoin('LEFT JOIN TAB_MISSIONDETAILS ON(TAB_MISSIONDETAILS.ID_ACHAT=TAB_ACHAT.ID_ACHAT)')
				->groupBy('TAB_ACHAT.ID_ACHAT');

				$result = $this->singleExec($baseQuery);

				if($result) {
					//On récupère tous les documents
					$sql = 'SELECT ID_DOCUMENT, FILE_NAME FROM TAB_DOCUMENT WHERE ID_PARENT=? AND DOC_TYPE=5;';
					$result['DOCUMENTS'] = $this->exec($sql, $result['ID_ACHAT']);

					//On récupère toutes les coordonnées de facturation de la société
					if($result['ID_CRMSOCIETE'])
						$result['FACT_COORDONNEES'] = $this->exec(
							'SELECT ID_COORDONNEES, COORD_NOM, COORD_CONTACT, COORD_EMAIL, COORD_EMAIL2, COORD_EMAIL3, COORD_TEL, COORD_ADR1, COORD_ADR2, COORD_ADR3, COORD_CP, COORD_VILLE, COORD_PAYS FROM TAB_COORDONNEES WHERE ID_PARENT=? AND COORD_TYPE=0 ORDER BY ID_COORDONNEES ASC',
							$result['ID_CRMSOCIETE']
						);
				}

				return $result;
			default:
				return $this->singleExec($baseQuery);
		}
	}

	/**
	 * Delete a purchase
	 * @param int $id Purchase ID
	 * @return bool
	 */
	public function deleteAchatData($id) {
		//On supprime les actions associés à cet achat
		$dict = Services\Actions::getDictionaryForCategory(BM::CATEGORY_PURCHASE);
		$where = new Where('ACTION_TYPE IN (?)', array_column($dict, 'id'));
		$where->and_('ID_PARENT = ?', $id);
		$this->delete('TAB_ACTION', $where->getWhere(), $where->getArgs());

		//Il faut supprimer tous les paiements de cet achat
		foreach($this->exec('SELECT ID_PAIEMENT FROM TAB_PAIEMENT WHERE ID_ACHAT= ?', $id) as $paiement) {
			$this->deletePaiementData($paiement['ID_PAIEMENT']);
		}

		//Il faut supprimer le détail de cet achat
		$detail = $this->singleExec('SELECT ID_MISSIONDETAILS, ID_PARENT, PARENT_TYPE FROM TAB_MISSIONDETAILS WHERE ID_ACHAT = ?', $id);
		if($detail) {
			$this->delete('TAB_MISSIONDETAILS', 'ID_MISSIONDETAILS= ?', $detail['ID_MISSIONDETAILS']);
			$this->updateMissionDetails($detail['ID_PARENT'], $detail['PARENT_TYPE']);
		}

		//Il faut supprimer l'achat de prestation de cet achat
		$this->update('TAB_MISSIONPROJET', ['ID_ACHAT' => 0], 'ID_ACHAT= :id ', ['id'=> $id]);

		//On supprime tous les documents de ce besoin
		$dbFile = File::instance();
		$dbFile->deleteAllObjects( File::TYPE_DOCUMENT, $id, BM::CATEGORY_PURCHASE);

		//On supprime tous les flags de cette action
		$dbFlag = AttachedFlag::instance();
		$dbFlag->removeAllAttachedFlagsFromEntity($id, Models\AttachedFlag::TYPE_PURCHASE);

		//ON supprime
		$this->delete('TAB_CORRELATIONBONDECOMMANDE', 'ID_ITEM=? AND CORBDC_TYPE=1', $id);
		$this->delete('TAB_ACHAT', 'ID_ACHAT=?', $id);

		return true;
	}

	/**
	 * \brief Retourne la prochaine référence de l'achat en tenant compte du masque
	 * @param string $mask
	 */
	function getNewReferenceAchat($idachat = 0, $id_ressource = 0, $id_crmcontact = 0, $mask = '', $onlymask = false, $forceDate = '') {
		throw new \Exception('todo migration'); //TODO
		if(preg_match("/([a-zA-Z0-9\[\]_\-\/]{1,})/", $mask, $matchRef)) {
			if($forceDate != '' && preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $forceDate, $match)) {
				$forceDate = explode('-',$forceDate);
				$annee4 = $forceDate[0];
				$annee2 = substr($forceDate[0],2);
				$mois = $forceDate[1];
				$jour = $forceDate[2];
			} else {
				$actualTime = mktime();
				$annee4 = date('Y',$actualTime);
				$annee2 = date('y',$actualTime);
				$mois = date('m',$actualTime);
				$jour = date('d',$actualTime);
			}

			$crmBDD = new BoondManager_ObjectBDD_CRM($this->db);
			if($crmBDD->getCRMData($id_crmcontact, 1)) $trigramme_client = strtoupper(substr(suppr_accents($crmBDD->get('CSOC_SOCIETE')),0,3)); else $trigramme_client = '';

			$profilBDD = new BoondManager_ObjectBDD_Ressource($this->db);
			if($profilBDD->getRessourceData($id_ressource, 0)) $trigramme_manager = strtoupper(substr(suppr_accents($profilBDD->get('PROFIL_PRENOM')),0,1).substr(suppr_accents($profilBDD->get('PROFIL_NOM')),0,2)); else $trigramme_manager = '';

			$mask = str_replace(array('[AAAA]','[AA]','[MM]','[JJ]','[ID_ACHAT]','[CCC]','[RRR]'), array($annee4,$annee2,$mois,$jour,$idachat,$trigramme_client,$trigramme_manager), $matchRef[1]);
			$maskWhere = str_replace(array('[AAAA]','[AA]','[MM]','[JJ]','[ID_ACHAT]','[CCC]','[RRR]','_','/','-'), array($annee4,$annee2,$mois,$jour,$idachat,$trigramme_client,$trigramme_manager,'\_','\/','\-'), $matchRef[1]);//Masque de la clause Where
			$maskMax = str_replace(array('/', '-'), array('\/', '\-'), $mask);

			if(strpos($matchRef[1], '[ID_ACHAT]') === false) {
				$result = $this->db->query_first('SELECT MAX(CAST(SUBSTRING(ACHAT_REF,'.(strlen($mask)+2).') AS UNSIGNED)) AS MAX_REFERENCE FROM TAB_ACHAT WHERE ACHAT_REF LIKE \''.$this->db->escape($maskWhere).'%\' AND ID_ACHAT<>'.$this->db->escape($idachat).';');
				if($result) $maxReference = $mask.'_'.$result['MAX_REFERENCE']; else $maxReference = $mask;
			} else $maxReference = $mask;
			if(preg_match("/".$maskMax."_([0-9]{1,})/", $maxReference, $matchMax)) $startNumber = '_'.(intval($matchMax[1])+1); else $startNumber = '';
		} else {
			$mask = 'ACH'.$idachat;
			$maskMax = 'ACH'.$idachat;
			$startNumber = '';
		}


		if($onlymask) return $mask; else return $mask.$startNumber;
	}

	/**
	 * \brief Test si un détail de mission existe et appartient bien à la mission indiquée
	 */
	public function getMissionDetails($iddetail) {
		throw new \Exception('todo migration'); //TODO
		$result = $this->db->query_first('SELECT ID_MISSIONDETAILS, MISDETAILS_DATE, MISDETAILS_ETAT, MISDETAILS_TITRE, MISDETAILS_TARIF, MISDETAILS_INVESTISSEMENT, ID_PARENT, PARENT_TYPE FROM TAB_MISSIONDETAILS WHERE ID_MISSIONDETAILS="'.$this->db->escape($iddetail).'";');
		if($result) {
			$this->data = $result;
			return true;
		}
		return false;
	}

	/**
	 * \brief Met à jour et récupère les dates de début et de fin du projet
	 * @param <type> $idprojet identifiant du projet
	 */
	public function updateMissionDetails($idparent, $typeparent)
	{
		throw new \Exception('todo migration'); //TODO
		if($typeparent == 1)
			$sql = 'SELECT TAB_MISSIONDETAILS.ID_ACHAT, ID_MISSIONDETAILS, MISDETAILS_TARIF, MISDETAILS_INVESTISSEMENT, ID_MISSIONPROJET AS ID_PARENT, 1 AS PARENT_TYPE FROM TAB_MISSIONPROJET LEFT JOIN TAB_MISSIONDETAILS ON(TAB_MISSIONDETAILS.ID_PARENT=ID_MISSIONPROJET AND PARENT_TYPE=1) WHERE ID_MISSIONPROJET="'.$idparent.'"';
		else
			$sql = 'SELECT TAB_MISSIONDETAILS.ID_ACHAT, ID_MISSIONDETAILS, MISDETAILS_TARIF, MISDETAILS_INVESTISSEMENT, ID_PROJET AS ID_PARENT, 0 AS PARENT_TYPE FROM TAB_PROJET LEFT JOIN TAB_MISSIONDETAILS ON(TAB_MISSIONDETAILS.ID_PARENT=ID_PROJET AND PARENT_TYPE=0) WHERE ID_PROJET="'.$idparent.'"';
		$tabDetails = $this->db->fetch_all_array($sql);
		if(sizeof($tabDetails)>0) {
			if($typeparent == 1) $newData = array('MP_TARIFADDITIONNEL' => 0, 'MP_INVESTISSEMENT' => 0); else $newData = array('PROJET' => array('PRJ_TARIFADDITIONNEL' => 0, 'PRJ_INVESTISSEMENT' => 0));
			foreach($tabDetails as $detail) {
				if($typeparent == 1) {
					$newData['MP_TARIFADDITIONNEL'] += $detail['MISDETAILS_TARIF'];
					$newData['MP_INVESTISSEMENT'] += $detail['MISDETAILS_INVESTISSEMENT'];
				} else {
					$newData['PROJET']['PRJ_TARIFADDITIONNEL'] += $detail['MISDETAILS_TARIF'];
					$newData['PROJET']['PRJ_INVESTISSEMENT'] += $detail['MISDETAILS_INVESTISSEMENT'];
				}
			}
			$projetBDD = new BoondManager_ObjectBDD_Projet($this->db);
			if($typeparent == 1) {
				$sql = 'SELECT TAB_MISSIONPROJET.ID_PROJET, MP_TARIFADDITIONNEL, MP_INVESTISSEMENT, PRJ_REFERENCE, TAB_MISSIONPROJET.ID_ITEM, TAB_MISSIONPROJET.ITEM_TYPE, TAB_PROJET.ID_PROFIL AS PRJ_IDPROFIL, PRJ_TYPE, PROFIL_NOM, PROFIL_PRENOM, PRODUIT_NOM, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE
						FROM TAB_MISSIONPROJET INNER JOIN TAB_PROJET USING(ID_PROJET)
							LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=0)
							LEFT JOIN TAB_PRODUIT ON(TAB_PRODUIT.ID_PRODUIT=TAB_MISSIONPROJET.ID_ITEM AND ITEM_TYPE=1)
						WHERE ID_MISSIONPROJET="'.$this->db->escape($idparent).'";';
				$result = $this->db->query_first($sql);
				if($result) {
					$projetBDD->updateProjetMission($newData, $idparent);
					BoondManager_Notification_Mission::getInstance($idparent, ($oldData = array('PRJ_TYPE' => $result['PRJ_TYPE'], 'ID_PROJET' => $result['ID_PROJET'], 'PRJ_DEVISEAGENCE' => $result['PRJ_DEVISEAGENCE'], 'PRJ_CHANGEAGENCE' => $result['PRJ_CHANGEAGENCE'], 'PRJ_DEVISE' => $result['PRJ_DEVISE'], 'PRJ_CHANGE' => $result['PRJ_CHANGE'], 'ID_ITEM' => $result['ID_ITEM'], 'ITEM_TYPE' => $result['ITEM_TYPE'], 'PROFIL_NOM' => getArrayValue($result,'PROFIL_NOM'), 'PROFIL_PRENOM' => getArrayValue($result,'PROFIL_PRENOM'), 'PRODUIT_NOM' => getArrayValue($result,'PRODUIT_NOM'), 'PRJ_IDPROFIL' => $result['PRJ_IDPROFIL'], 'MP_TARIFADDITIONNEL' => $result['MP_TARIFADDITIONNEL'], 'MP_INVESTISSEMENT' => $result['MP_INVESTISSEMENT'], 'PRJ_REFERENCE' => $result['PRJ_REFERENCE'])), $newData, $this->db)->update();//Gestion des notifications
				}
			} else {
				$sql = 'SELECT TAB_PROJET.ID_PROJET, TAB_PROJET.ID_PROFIL, PRJ_TARIFADDITIONNEL, PRJ_INVESTISSEMENT, PRJ_TYPE, PRJ_REFERENCE, PRJ_DEVISEAGENCE, PRJ_CHANGEAGENCE, PRJ_DEVISE, PRJ_CHANGE
						FROM TAB_PROJET WHERE ID_PROJET="'.$this->db->escape($idparent).'";';
				$result = $this->db->query_first($sql);
				if($result) {
					$projetBDD->updateProjetData($newData, $idparent);
					BoondManager_Notification_Projet::getInstance($idparent, 0, ($oldData = array('PRJ_TYPE' => $result['PRJ_TYPE'], 'PRJ_DEVISEAGENCE' => $result['PRJ_DEVISEAGENCE'], 'PRJ_CHANGEAGENCE' => $result['PRJ_CHANGEAGENCE'], 'PRJ_DEVISE' => $result['PRJ_DEVISE'], 'PRJ_CHANGE' => $result['PRJ_CHANGE'], 'ID_PROFIL' => $result['ID_PROFIL'], 'PRJ_TARIFADDITIONNEL' => $result['PRJ_TARIFADDITIONNEL'], 'PRJ_INVESTISSEMENT' => $result['PRJ_INVESTISSEMENT'], 'PRJ_REFERENCE' => $result['PRJ_REFERENCE'])), $newData, $this->db)->update();//Gestion des notifications
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * \brief Récupère les données d'un paiement
	 * @param <type> $id identifiant du besoin
	 */
	public function getPaiementData($id)
	{
		throw new \Exception('todo migration'); //TODO
		$sql = 'SELECT ID_PAIEMENT, PMT_DATE, PMT_DEBUT, PMT_FIN, PMT_DATEPAIEMENTATTENDU, PMT_DATEPAIEMENTEFFECTUE, PMT_REFPROVIDER, PMT_MONTANTHT, PMT_TAUXTVA, PMT_TYPEPAYMENT, PMT_ETAT, PMT_COMMENTAIRES, TAB_ACHAT.ID_ACHAT, TAB_ACHAT.ID_PROJET, PRJ_REFERENCE, ACHAT_TITLE, ACHAT_DATE, ACHAT_REF, ACHAT_REFFOURNISSEUR, ACHAT_TYPE, ACHAT_ETAT, ACHAT_MONTANTHT, ACHAT_QUANTITE, ACHAT_DEVISE, ACHAT_CHANGE, ACHAT_DEVISEAGENCE, ACHAT_CHANGEAGENCE, ACHAT_TAUXTVA, ACHAT_CONDREGLEMENT, ACHAT_DEBUT, ACHAT_FIN, ACHAT_COMMENTAIRE,
					   TAB_ACHAT.ID_CRMCONTACT, CCON_NOM, CCON_PRENOM, CSOC_SOCIETE, TAB_ACHAT.ID_CRMSOCIETE, TAB_ACHAT.ID_PROFIL, TAB_ACHAT.ID_SOCIETE, TAB_ACHAT.ID_POLE, TAB_MISSIONPROJET.ID_MISSIONPROJET
				FROM TAB_PAIEMENT INNER JOIN TAB_ACHAT USING(ID_ACHAT) LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_ACHAT.ID_CRMCONTACT) LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_ACHAT.ID_CRMSOCIETE)
					LEFT JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_ACHAT.ID_PROFIL) LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_ACHAT.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_ACHAT=TAB_ACHAT.ID_ACHAT)
				WHERE ID_PAIEMENT="'.$this->db->escape($id).'";';
		$result = $this->db->query_first($sql);
		if($result) {
			$this->data = $result;
			//On récupère tous les documents
			$result['JUSTIFICATIFS'] = 'SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF WHERE ID_PARENT="'.$result['ID_PAIEMENT'].'" AND JUSTIF_TYPE=3;';
			$this->data['JUSTIFICATIFS'] = $this->db->fetch_all_array('SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF WHERE ID_PARENT="'.$result['ID_PAIEMENT'].'" AND JUSTIF_TYPE=3;');
			return true;
		}
		return false;
	}

	/**
	 *\brief Création d'un paiement
	 * @param <type> $id identifiant du paiement
	 */
	public function newPaiementData($data) {
		throw new \Exception('todo migration'); //TODO
		if(!isset($data['PMT_DATE'])) $data['PMT_DATE'] = date('Y-m-d', mktime());
		$idpaiement = $this->db->query_insert('PAIEMENT', $data);
		return $idpaiement;
	}

	/**
	 *\brief Update d'un paiement
	 * @param <type> $id identifiant du paiement
	 */
	public function updatePaiementData($data, $id) {
		throw new \Exception('todo migration'); //TODO
		$this->db->query_update('PAIEMENT', $data, 'ID_PAIEMENT="'.$this->db->escape($id).'"');
		return true;
	}

	/**
	 *\brief Suppression d'un paiement
	 * @param <type> $id identifiant du paiement
	 */
	public function deletePaiementData($id) {
		throw new \Exception('todo migration'); //TODO
		//On supprime tous les documents de ce besoin
		$fileData = new BoondManager_ObjectBDD_File($this->db);
		$fileData->deleteAllFiles(FILE_JUSTIFICATIFS, $id, 3);
		$this->db->query_delete('PAIEMENT', 'ID_PAIEMENT="'.$this->db->escape($id).'"');
		return true;
	}

	/**
	 * \brief Crée une liste de paiement
	 * @param string $mask
	 */
	function createPaymentsList($idachat, $debut, $fin, $montant, $quantite, $type, $etat = 0, $condReglement = 0, $tva = 0, $modReglement = -1) {
		throw new \Exception('todo migration'); //TODO
		$xmlParam = new BoondManager_XML('/common/configuration/parametres.xml');
		$factData = new BoondManager_Facturation();
		if($type == 0) {
			if($montant*$quantite != 0) $this->db->query_insert('PAIEMENT', array('ID_ACHAT' => $idachat, 'PMT_ETAT' => $etat, 'PMT_MONTANTHT' => $montant*$quantite, 'PMT_DATE' => $debut, 'PMT_DEBUT' => $debut, 'PMT_FIN' => $debut, 'PMT_TAUXTVA' => $tva, 'PMT_TYPEPAYMENT' => $modReglement, 'PMT_DATEPAIEMENTATTENDU' => $factData->getDateReglement($debut, $condReglement, $xmlParam->getTagListe('/configuration/reglement/conditions'))));
		} else {
			$tabDebut = explode('-', $debut);
			for($i = 0; $i < $quantite; $i++) {
				$nbMonths = $factData->getNbMensualiteAbonnement($type);
				$start = date('Y-m-d', mktime(0,0,0,$tabDebut[1] + $i*$nbMonths, 1, $tabDebut[0]));
				$end = date('Y-m-d', mktime(0,0,0,$tabDebut[1] + ($i+1)*$nbMonths, 0, $tabDebut[0]));
				if($montant != 0) $this->db->query_insert('PAIEMENT', array('ID_ACHAT' => $idachat, 'PMT_ETAT' => $etat, 'PMT_MONTANTHT' => $montant, 'PMT_DATE' => $end, 'PMT_DEBUT' => $start, 'PMT_FIN' => $end, 'PMT_TAUXTVA' => $tva, 'PMT_TYPEPAYMENT' => $modReglement, 'PMT_DATEPAIEMENTATTENDU' => $factData->getDateReglement($end, $condReglement, $xmlParam->getTagListe('/configuration/reglement/conditions'))));
			}
		}
	}

	/**
	 * \brief Met à jour et récupère les dates de début et de fin du projet
	 * @param <type> $idprojet identifiant du projet
	 */
	/*public function updateMissionPrestation($idachat, $montantht, $debut, $fin) {
		throw new \Exception('TODO'); //TODO
		$achat = $this->db->query_first('SELECT ID_ACHAT, ID_PROFIL, ACHAT_TITLE, ACHAT_DEVISEAGENCE, ACHAT_CHANGEAGENCE, ACHAT_CHANGE, ACHAT_DEVISE, ACHAT_MONTANTHT, ACHAT_QUANTITE, ACHAT_DATE, ACHAT_DEBUT, ACHAT_FIN FROM TAB_ACHAT WHERE ID_ACHAT="'.$idachat.'"');
		if($achat) {
			$factData = new BoondManager_Facturation();
			$achatBDD = new BoondManager_ObjectBDD_Achat($this->db);
			$quantite = $factData->getPeriodeAbonnement($debut, $fin, 1);
			$newData = array('ACHAT' => array('ACHAT_MONTANTHT' => ($quantite == 0)?0:round($montantht/$quantite,2), 'ACHAT_QUANTITE' => $quantite, 'ACHAT_DEBUT' => $debut, 'ACHAT_FIN' => $fin));
			$achatBDD->updateAchatData($newData, $achat['ID_ACHAT']);
			BoondManager_Notification_Achat::getInstance($achat['ID_ACHAT'], 0, ($oldData = array('ID_PROFIL' => $achat['ID_PROFIL'], 'ACHAT_TITLE' => $achat['ACHAT_TITLE'], 'ACHAT_DATE' => $achat['ACHAT_DATE'], 'ACHAT_DEBUT' => $achat['ACHAT_DEBUT'], 'ACHAT_FIN' => $achat['ACHAT_FIN'], 'ACHAT_DEVISEAGENCE' => $achat['ACHAT_DEVISEAGENCE'], 'ACHAT_CHANGEAGENCE' => $achat['ACHAT_CHANGEAGENCE'], 'ACHAT_CHANGE' => $achat['ACHAT_CHANGE'], 'ACHAT_DEVISE' => $achat['ACHAT_DEVISE'], 'ACHAT_MONTANTHT' => $achat['ACHAT_MONTANTHT'], 'ACHAT_QUANTITE' => $achat['ACHAT_QUANTITE'])), $newData, $this->db)->update();//Gestion des notifications
			return true;
		}
		return false;
	}*/

	/**
	 * @param $id
	 * @return bool
	 */
	function isAchatReducible($id) {
		$nbDocument = 0;
		foreach($this->exec('SELECT COUNT(ID_CORRELATIONBONDECOMMANDE) AS NB_DOCUMENT FROM TAB_CORRELATIONBONDECOMMANDE WHERE ID_ITEM= ? AND CORBDC_TYPE=1', $id) as $document) {
			$nbDocument += $document['NB_DOCUMENT'];
		}
		return $nbDocument == 0;
	}

	/**
	 * Delete a payment
	 * @param int $id payment ID
	 */
	public function deleteObject($id)
	{
		$this->deleteAchatData($id);
	}

	/**
	 * is a given Payment deletable
	 * @param int $id
	 * @return bool
	 */
	function canDeleteObject($id) {
		$this->isAchatReducible($id);
	}
}
