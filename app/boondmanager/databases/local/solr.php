<?php
/**
 * solr.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use Wish\Models\Model;
use Wish\MySQL\AbstractDb;

/**
 * Handle all database work related to SolR
 * @namespace \BoondManager\Models\MySQL\Local
 */
class SolR extends \BoondManager\Databases\Local\AbstractObject {

	/**
	 * Retrieve all SolR data related to a given module
     * @param integer $module Module, cf. [TAB_SOLR.SOLR_MODULE](../../bddclient/classes/TAB_SOLR.html#property_SOLR_MODULE).
     * @return Model
     */
    public function getObject($module) {
		$result = $this->singleExec('SELECT ID_SOLR, SOLR_LASTIMPORT FROM TAB_SOLR WHERE SOLR_MODULE=?', $module);
		if($result) {
			return $result; // $this['OBJECT']
		} else {
			$tabImport = array('SOLR_LASTIMPORT' => '1900-01-01 00:00:00', 'SOLR_MODULE' => $module);
			$tabImport['ID_SOLR'] = $this->insert('TAB_SOLR', $tabImport);
			if($tabImport['ID_SOLR']) {
				//$this['OBJECT'] = $tabImport;
				return new Model($tabImport);
			}
		}
		return false;
    }

    /**
     * Create/update SolR data
     * @param array $data
	 * the array can contains the following keys:
     * - `SOLR` : cf. [TAB_SOLR](../../bddclient/classes/TAB_SOLR.html)
	 *
     * @param integer $id SolR database ID to update, cf. [TAB_SOLR.ID_SOLR](../../bddclient/classes/TAB_SOLR.html#property_ID_SOLR).
     * if none a new entry will be created
     * @return integer id
     */
    public function setObject($data, $id = 0) {
    	if($id) {
	    	if(isset($data['SOLR'])) $this->update('TAB_SOLR', $data['SOLR'], 'ID_SOLR=:id', [':id' => $id]);
	    } else {
	    	if(!isset($data['SOLR']['SOLR_LASTIMPORT'])) $data['SOLR']['SOLR_LASTIMPORT'] = date('Y-m-d H:i:s', time());
	    	$id = $this->insert('TAB_SOLR', $data['SOLR']);
	    }
	    return $id;
    }
}
