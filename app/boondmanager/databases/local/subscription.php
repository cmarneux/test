<?php
/**
 * subscription.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;
use Wish\MySQL\Query;
use BoondManager\Databases\Local\AbstractObject;

/**
 * Handle all database operations related to subscriptions
 * @package BoondManager\Models\MySQL\Local
 */
class Subscription extends AbstractObject{
    /**
     * get the configuration for a customer
     * @return \Wish\Models\Model|false
     */
    public function getGroupSubscription() {
        $query = new Query();
        $query->select([
            'AB_DEBUT', 'AB_FIN', 'AB_NBMANAGERS', 'AB_MAXRESSOURCESPERMANAGER', 'AB_MAXSTORAGE',
            'AB_MAXRESSOURCESINTRANET', 'ID_ABONNEMENT'
        ])->from('TAB_ABONNEMENT');

        $result = $this->singleExec($query);

        return $result;
    }

    /**
     * Update a customer configuration
     * @param array $data row data
     * @return int number of rows updated
     */
    public function updateGroupSubscription($data) {
        return $this->update('TAB_ABONNEMENT', $data);
    }
}
