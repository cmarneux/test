<?php
/**
 * target.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

/**
 * Handle all database work related to targets
 */
class Target extends AbstractObject
{
	/**
	 * @param $idmanager
	 * @return false|int|\Wish\Models\Model[]
	 */
	function getAllObjectifs($idmanager)
	{
		$sql = 'SELECT ID_OBJECTIF, OBJ_CATEGORIE, OBJ_INDICATEUR, OBJ_PERIODE, OBJ_DATE, OBJ_VALUE, OBJ_OPERATEUR FROM TAB_OBJECTIF WHERE ID_USER = ? ORDER BY ID_OBJECTIF DESC';
		return $this->exec($sql, $idmanager);
	}
}
