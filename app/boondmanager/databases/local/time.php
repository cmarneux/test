<?php
/**
 * times.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\SearchResult;
use BoondManager\OldModels\Filters;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;

class Time extends AbstractObject{

	/**
	 * perform a search
	 * @param Filters\Search\Times $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	public function search(Filters\Search\Times $filter){

		if(!$filter->isValid()) throw new \Exception('invalid filter');


		$commonColumns = 'TAB_PROFIL.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT, PROFIL_TYPE, TAB_SOCIETE.SOCIETE_RAISON, 
						  ID_LISTETEMPS, TAB_LISTETEMPS.ID_SOCIETE, LISTETEMPS_DATE, ID_PROFILCDP, GRPCONF_TAUXHORAIRE, TAB_USER.USER_TAUXHORAIRE, 
						  TYPEH_NAME, TYPEH_TYPEACTIVITE, PRJ_REFERENCE, TAB_PROJET.ID_CRMCONTACT, TAB_CRMCONTACT.ID_CRMSOCIETE, CCON_NOM, CCON_PRENOM, 
						  CSOC_SOCIETE, LISTETEMPS_ETAT AS VAL_STATE';

		$normalColumns = 'ID_LIGNETEMPS, TAB_LIGNETEMPS.ID_PROJET, TAB_LIGNETEMPS.ID_MISSIONPROJET, TAB_LIGNETEMPS.ID_LOT, LOT_TITRE, ID_TEMPS,
							TEMPS_DATE, TEMPS_DUREE, LIGNETEMPS_TYPEHREF AS TYPEH_REF, 0 AS TEMPS_TYPE';

		$exepColumns = 'NULL AS ID_LIGNETEMPS, TAB_TEMPSEXCEPTION.ID_PROJET, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET, TAB_TEMPSEXCEPTION.ID_LOT, LOT_TITRE,
						ID_TEMPSEXCEPTION AS ID_TEMPS, TEMPSEXP_DEBUT AS TEMPS_DATE, IF(TYPEH_TYPEACTIVITE=4,DATEDIFF(TEMPSEXP_FIN, TEMPSEXP_DEBUT)+1, 
						TIMESTAMPDIFF(SECOND, TEMPSEXP_DEBUT, TEMPSEXP_FIN)) AS TEMPS_DUREE, TEMPSEXP_TYPEHREF AS TYPEH_REF, 
						IF(TYPEH_TYPEACTIVITE IS NULL OR TYPEH_TYPEACTIVITE=3,1,2) AS TEMPS_TYPE';


		$normalQuery = new Query();
		$normalQuery->addColumns($commonColumns);
		$normalQuery->addColumns($normalColumns);
		$normalQuery->from('TAB_TEMPS 
							INNER JOIN TAB_LIGNETEMPS USING(ID_LIGNETEMPS) 
							INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) 
							INNER JOIN TAB_PROFIL USING(ID_PROFIL) 
							INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE) 
							INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE) 
							LEFT JOIN TAB_USER US ON TAB_PROFIL.ID_RESPMANAGER = US.ID_USER
							LEFT JOIN TAB_USER USRH ON TAB_PROFIL.ID_RESPRH = USRH.ID_USER
							LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
							LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL)
							LEFT JOIN TAB_PROJET USING(ID_PROJET) 
							LEFT JOIN TAB_LOT USING(ID_LOT)
							LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET) 
							LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) 
							LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)')
					->groupBy('TAB_TEMPS.ID_TEMPS');

		$excepQuery = new Query();
		$excepQuery->addColumns($commonColumns);
		$excepQuery->addColumns($exepColumns);
		$excepQuery->from('TAB_TEMPSEXCEPTION 
						  INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) 
						  INNER JOIN TAB_PROFIL USING(ID_PROFIL) 
						  INNER JOIN TAB_GROUPECONFIG ON(TAB_GROUPECONFIG.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE) 
						  INNER JOIN TAB_SOCIETE ON(TAB_SOCIETE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE) 
						  LEFT JOIN TAB_USER US ON TAB_PROFIL.ID_RESPMANAGER = US.ID_USER
						  LEFT JOIN TAB_USER USRH ON TAB_PROFIL.ID_RESPRH = USRH.ID_USER
						  LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE)
						  LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=TAB_PROFIL.ID_PROFIL) 
						  LEFT JOIN TAB_PROJET USING(ID_PROJET) 
						  LEFT JOIN TAB_LOT USING(ID_LOT)
						  LEFT JOIN TAB_CRMCONTACT ON(TAB_CRMCONTACT.ID_CRMCONTACT=TAB_PROJET.ID_CRMCONTACT) 
						  LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)')
				  ->groupBy('TAB_TEMPSEXCEPTION.ID_TEMPSEXCEPTION');

		if($filter->returnDetailedExceptionalTimes->getValue()){
			$normalQuery->addColumns('NULL AS TEMPSEXP_FIN, NULL AS TEMPSEXP_RECUPERATION, NULL AS TEMPSEXP_DESCRIPTION, NULL AS TEMPSEXP_TARIF, NULL AS TEMPSEXP_REGLES')
						->addColumns('NULL AS TEMPSEXP_ACHAT, NULL AS TEMPSEXP_COUT, NULL AS TEMPSEXP_MODECALCUL, NULL AS TEMPSEXP_MANAGE');

			$excepQuery->addColumns('TEMPSEXP_FIN, TEMPSEXP_RECUPERATION, TEMPSEXP_DESCRIPTION, TEMPSEXP_TARIF, TEMPSEXP_REGLES, TEMPSEXP_ACHAT, TEMPSEXP_COUT, TEMPSEXP_MODECALCUL, TEMPSEXP_MANAGE');
		}

		$commonWhere = $this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			'TAB_LISTETEMPS.ID_SOCIETE',
			'TAB_PROFIL.ID_POLE',
			['US.ID_PROFIL','USRH.ID_PROFIL']
		);

		$normalWhereKeywords = $this->getListIdSearch($filter->keywords->getValue(), [
			'COMP' => 'TAB_PROFIL.ID_PROFIL',
			'PRJ'  => 'TAB_LIGNETEMPS.ID_PROJET',
			'CCON' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
			'CSOC' => 'TAB_CRMSOCIETE.ID_CRMSOCIETE',
			'TPS'  => 'ID_TEMPS',
			'RPT'  => 'ID_LISTETEMPS',
		]);
		$normalQuery->addWhere($normalWhereKeywords);

		$exepWhereKeywords = $this->getListIdSearch($filter->keywords->getValue(), [
			'COMP' => 'TAB_PROFIL.ID_PROFIL',
			'PRJ'  => 'TAB_TEMPSEXCEPTION.ID_PROJET',
			'CCON' => 'TAB_CRMCONTACT.ID_CRMCONTACT',
			'CSOC' => 'TAB_CRMSOCIETE.ID_CRMSOCIETE',
			'TPS'  => 'ID_TEMPSEXCEPTION',
			'RPT'  => 'ID_LISTETEMPS',
		]);
		$excepQuery->addWhere($exepWhereKeywords);

		if($filter->keywords->getValue() && $normalWhereKeywords->isEmpty() && $exepWhereKeywords->isEmpty()){
			$keywords = str_replace('*', '%', str_replace('%', '\%', $filter->keywords->getValue()));
			$commonWhere->and_( (new Where('PROFIL_NOM LIKE ?', $keywords.'%'))->or_(new Where('PROFIL_PRENOM LIKE ?', $keywords.'%')) );
		}

		if($filter->activityType->getValue()){
			switch ($filter->activityType->getValue()){
				case Filters\Search\Times::TYPE_PRODUCTION:
					$normalQuery->addWhere('TAB_LIGNETEMPS.ID_PROJET > 0');
					$excepQuery->addWhere('TAB_TEMPSEXCEPTION.ID_PROJET > 0');
					break;
				case Filters\Search\Times::TYPE_ABSENCE:
					$normalQuery->addWhere('TAB_LIGNETEMPS.ID_PROJET  = -1');
					$excepQuery->addWhere('TAB_TEMPSEXCEPTION.ID_PROJET = -1');
					break;
				case Filters\Search\Times::TYPE_INTERNAL:
					$normalQuery->addWhere('TAB_LIGNETEMPS.ID_PROJET  = -2');
					$excepQuery->addWhere('TAB_TEMPSEXCEPTION.ID_PROJET = -2');
					break;
			}
		}

		if($filter->resourceTypes->isDefined())
			$commonWhere->and_( $this->getFilterSearch($filter->resourceTypes->getValue(), 'PROFIL_TYPE') );

		if($filter->excludeResourceTypes->getValue())
			$commonWhere->and_( new Where('PROFIL_TYPE NOT IN (?)', $filter->excludeResourceTypes->getValue()) );

		if($filter->startDate->isDefined()){
			$normalQuery->addWhere( new Where('TEMPS_DATE BETWEEN ? AND ?', [$filter->startDate->getValue(), $filter->endDate->getValue()]) );
			$excepQuery->addWhere( new Where('DATEDIFF(TEMPSEXP_FIN, ?)>=0 AND DATEDIFF( ? , TEMPSEXP_DEBUT)>=0 ', [$filter->startDate->getValue(), $filter->endDate->getValue()]) );
		}

		$normalQuery->addWhere($commonWhere);
		$excepQuery->addWhere($commonWhere);

		$this->setOrderExpr($normalQuery, $filter->sort->getValue(), $filter->order->getValue());
		$this->setOrderExpr($excepQuery, $filter->sort->getValue(), $filter->order->getValue());

		/** @var Query[] $queries */
		$queries = [];
		if($filter->category->isDefined()){
			if( Models\Time::TYPE_TIME_REGULAR == $filter->category->getValue()){
				$queries[] = $normalQuery;
			}else if( Models\Time::TYPE_TIME_EXCEPTIONAL == $filter->category->getValue()){
				$queries[] = $excepQuery;
			}
		}else{
			$queries = [$normalQuery, $excepQuery];
		}

		$totalRows = 0;
		$subQueries = [];
		$argQueries = [];
		foreach($queries as $q){
			$result = $this->singleExec( $q->getCountQuery(), $q->getArgs() );
			$totalRows += $result->NB_ROWS;
			$subQueries[] = $q->getQuery();
			$argQueries = array_merge($argQueries, $q->getArgs());
		}

		if($totalRows){
			$sql_union = 'SELECT * FROM (('.implode(') UNION ALL (', $subQueries).')) AS TEMPS '.$queries[0]->buildOrderBy().' LIMIT '.Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()).', '.$filter->maxResults->getValue();
			$result = $this->exec($sql_union, $argQueries, Models\Time::class, new Mappers\SearchTime());
		}else{
			$result = [];
		}

		$searchResult = new SearchResult($result, $totalRows);

		return $searchResult;
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			Filters\Search\Times::ORDERBY_LASTNAME => 'PROFIL_NOM',
			Filters\Search\Times::ORDERBY_CATEGORY => 'ID_PROJET',
			Filters\Search\Times::ORDERBY_STARTDATE => 'TEMPS_DATE',
			Filters\Search\Times::ORDERBY_REFERENCE => 'TYPEH_REF'
		];

		if(!$column) $query->addOrderBy('ID_PROFIL ASC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('TEMPS_TYPE ASC');
		$query->addOrderBy('ID_TEMPS DESC');
	}

	/**
	 * \brief Vérifie si la timesheet existe en fonction d'une date (Y-m-d)
	 * @param <type> $date $date de test
	 * @param <type> $idprofil identifiant du profil
	 * @return <type>
	 */
	public function isTempsExist($date, $idprofil) {
		$result = $this->singleExec('SELECT ID_LISTETEMPS FROM TAB_LISTETEMPS WHERE LISTETEMPS_DATE= :date AND ID_PROFIL= :id', ['date'=>$date, 'id' => $idprofil]);
		if($result) return $result['ID_LISTETEMPS'];
		return false;
	}
}
