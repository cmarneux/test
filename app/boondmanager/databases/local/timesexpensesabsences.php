<?php
/**
 * timesexpensesabsences.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\OldModels\MySQL\Mappers\ProfilUserConfig;
use BoondManager\Models;

class TimesExpensesAbsences extends AbstractObject{

	/**
	 * @param $profilID
	 * @return Models\Employee|false
	 *
	 * Retourne les données de l'intervenant avec son salaire de référence
	 * @deprecated
	 * @TODO à migrer vers employee
	 */
	public function getProfilTpsFrsData($profilID) {
		$sql ='SELECT TAB_PROFIL.ID_PROFIL, ID_POLE, TAB_PROFIL.ID_RESPMANAGER, RESP.ID_PROFIL AS ID_PROFIL_RESPMANAGER, TAB_PROFIL.ID_RESPRH, 
		              HR.ID_PROFIL AS ID_PROFIL_RESPRH, TAB_USER.ID_USER, TAB_USER.USER_TYPE, TAB_USER.USER_LOGIN, 
		              TAB_USER.USER_LANGUE, TAB_USER.USER_ABONNEMENT, TAB_USER.USER_ALLOWTEMPSEXCEPTION, TAB_USER.USER_DEFAULTSEARCH, TAB_USER.USER_JOINCATEGORY, 
		              TAB_USER.USER_HOMEPAGE, TAB_USER.USER_TPSFRSSTART, TAB_USER.USER_TAUXHORAIRE, TAB_USER.USER_VALIDATIONTEMPS, TAB_USER.USER_VALIDATIONFRAIS, 
		              TAB_USER.USER_VALIDATIONABSENCES, TAB_USER.USER_ALLOWABSENCESTYPEHREF, TAB_USER.USER_TPSFRSETAT, 
		              PROFIL_NOM, PROFIL_PRENOM, TAB_PROFIL.PROFIL_TYPE, PROFIL_EMAIL, TAB_PROFIL.ID_SOCIETE, 
		              CONFIG_MENUBAR, CONFIG_DASHBOARD, MIN(CTR_DEBUT) AS MIN_DEBUT, MAX(CTR_FIN) AS MAX_FIN
		       FROM TAB_PROFIL 
		       LEFT JOIN TAB_USER  USING(ID_PROFIL) 
		       LEFT JOIN TAB_USERCONFIG ON (TAB_USER.ID_USER = TAB_USERCONFIG.ID_USER) 
		       LEFT JOIN TAB_CONTRAT USING(ID_PROFIL)
		       LEFT JOIN TAB_USER AS RESP ON RESP.ID_USER = TAB_PROFIL.ID_RESPMANAGER
		       LEFT JOIN TAB_USER AS HR ON HR.ID_USER = TAB_PROFIL.ID_RESPRH
		       WHERE TAB_PROFIL.ID_PROFIL=? AND TAB_PROFIL.PROFIL_TYPE<> ? GROUP BY TAB_PROFIL.ID_PROFIL';

		return $this->singleExec($sql, [$profilID, Models\Candidate::TYPE_CANDIDATE]);
	}

	/**
	 * @param $idsociete
	 * @return \BoondManager\Lib\Agency
	 * @deprecated regarder pour utiliser les getAgency($id, $tab=activity)
	 */
	public function getConfigTpsFrsGroupe($idsociete) {
		$sql = 'SELECT ID_SOCIETE, SOCIETE_RAISON, GRPCONF_TAUXHORAIRE, GRPCONF_VALIDATIONTEMPS, GRPCONF_VALIDATIONFRAIS, 
		               GRPCONF_VALIDATIONABSENCES, GRPCONF_MENTIONSTEMPS, GRPCONF_MENTIONSFRAIS, GRPCONF_MENTIONSABSENCES, 
		               GRPCONF_ALERTDECOMPTE, GRPCONF_DEVISE, GRPCONF_CHANGE, GRPCONF_CALENDRIER, GRPCONF_DATEMAILTEMPS, 
		               GRPCONF_DATEMAILFRAIS, GRPCONF_STARTDECOMPTE, GRPCONF_SHOWDECOMPTE, GRPCONF_CTRAUTOFILLEXPENSES
		        FROM TAB_GROUPECONFIG INNER JOIN TAB_SOCIETE USING(ID_SOCIETE)
		        WHERE ID_SOCIETE=?';
		$result = $this->singleExec($sql, $idsociete);
		if($result) {
			$confs = Agency::instance()->getActivityExpensesConfig($idsociete);
			foreach($confs as $key=>$list)
				$result[$key] = $list;
		}

		return $result;
	}


	/**
	 * Retrieve all employee's projects for a given period
	 * @param int $id
	 * @param string $startDate
	 * @param string $endDate
	 * @return false|int|\Wish\Models\Model[]
	 */
	public function getProfilMisPrjPeriode($id, $startDate, $endDate)
	{
		$sql ='SELECT 
		            TAB_MISSIONPROJET.ID_MISSIONPROJET, TAB_PROJET.ID_PROJET, PRJ_REFERENCE, MP_NOM, CSOC_SOCIETE, ID_AO, 
		            MP_DEBUT, MP_FIN, ID_PROFILCDP, TAB_LOT.ID_LOT, LOT_TITRE
		       FROM TAB_MISSIONPROJET 
		       INNER JOIN TAB_PROJET USING(ID_PROJET) 
		       LEFT JOIN TAB_CRMCONTACT USING(ID_CRMCONTACT) 
		       LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		       LEFT JOIN TAB_LOT USING(ID_PROJET) 
		       LEFT JOIN TAB_JALON ON(TAB_JALON.ID_LOT=TAB_LOT.ID_LOT AND TAB_JALON.ID_PROJET=TAB_PROJET.ID_PROJET)
		       WHERE 
		           TAB_MISSIONPROJET.ID_ITEM=:id AND ITEM_TYPE=0 AND ID_MASTER=0 AND MP_TYPE<>1 AND PRJ_TYPE>0 
		           AND DATEDIFF(:endDate, MP_DEBUT)>=0 AND DATEDIFF(MP_FIN,:startDate)>=0 
		           AND (TAB_JALON.ID_PROFIL=TAB_MISSIONPROJET.ID_ITEM OR TAB_JALON.ID_PROFIL=0 OR TAB_LOT.ID_LOT IS NULL OR TAB_JALON.ID_JALON IS NULL) 
		       GROUP BY TAB_MISSIONPROJET.ID_MISSIONPROJET, TAB_LOT.ID_LOT 
		       ORDER BY TAB_PROJET.ID_PROJET ASC, TAB_MISSIONPROJET.ID_MISSIONPROJET ASC, TAB_LOT.ID_LOT';
		$result = $this->exec($sql, ['id' => $id, 'endDate' => $endDate, 'startDate' => $startDate]);
		return $result;
	}
}
