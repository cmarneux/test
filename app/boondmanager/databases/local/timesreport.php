<?php
/**
 * timesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use BoondManager\APIs\TimesReports\Filters\SearchTimesReports;
use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Tools;
use BoondManager\Models;

class TimesReport extends AbstractObject{

	/**
	 * search for expenses
	 * @param SearchTimesReports $filter
	 * @return \Wish\Models\SearchResult <type>
	 * @throws \Exception
	 */
	public function search(SearchTimesReports $filter)
	{
		if(!$filter->isValid()) throw new \Exception('filter is not valid');

		$query = new Query();
		$query->setLimit($filter->maxResults->getValue(), Query::getOffset($filter->page->getValue(), $filter->maxResults->getValue()));
		$query->setMapper(new \BoondManager\Databases\Mapper\TimesReport());
		$query->select('DOCUMENT.ID_LISTETEMPS AS ID_DOCUMENT, DOCUMENT.LISTETEMPS_DATE AS DOCUMENT_DATE, DOCUMENT.LISTETEMPS_ETAT AS DOCUMENT_ETAT, 
						AT.ID_PROFIL, AT.PROFIL_NOM, AT.PROFIL_PRENOM, AT.PROFIL_REFERENCE, AT.PROFIL_TYPE, DOCUMENT.ID_SOCIETE, 
						ID_VALIDATION, VAL_DATE, LISTETEMPS_CLOTURE AS DOCUMENT_CLOTURE');
		$query->from('TAB_LISTETEMPS AS DOCUMENT 
					  INNER JOIN TAB_PROFIL AT ON (AT.ID_PROFIL=DOCUMENT.ID_PROFIL)
					  LEFT JOIN TAB_USER AS US ON (US.ID_USER=AT.ID_RESPMANAGER) 
					  LEFT JOIN TAB_USER AS USRH ON (USRH.ID_USER=AT.ID_RESPRH)
					  LEFT JOIN TAB_VALIDATION ON (ID_PARENT=DOCUMENT.ID_LISTETEMPS AND VAL_TYPE=0)');
		$stateWhere = new Where();

		if($filter->perimeterManagersType->getValue() == SearchTimesReports::PERIMETER_TYPE_VALIDATOR){

			$query->select('VAL_ETAT AS VAL_STATE, RESP.PROFIL_NOM AS RESP_NOM, RESP.PROFIL_PRENOM AS RESP_PRENOM, RESP.ID_PROFIL AS RESP_ID_PROFIL');
			$query->addJoin('LEFT JOIN TAB_PROFIL RESP ON(RESP.ID_PROFIL=TAB_VALIDATION.ID_PROFIL) LEFT JOIN TAB_USER ON(TAB_USER.ID_PROFIL=RESP.ID_PROFIL)');

			$query->addWhere( $this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'RESP.ID_SOCIETE',
				'RESP.ID_POLE',
				'RESP.ID_PROFIL'
			));

			$this->setOrderExprForValidators($query, $filter->sort->getValue(), $filter->order->getValue());

			foreach($filter->validationStates->getValue() as $etat){
				switch($etat){
					case Models\Validation::VALIDATION_VALIDATED:
						$stateWhere->or_('VAL_ETAT=1'); break;
					case Models\Validation::VALIDATION_REJECTED:
						$stateWhere->or_('VAL_ETAT=3'); break;
					case Models\Validation::VALIDATION_WAITING:
						$stateWhere->or_('VAL_ETAT=2 OR VAL_ETAT IS NULL'); break;
				}
			}

			$query->setCountColumn('DOCUMENT.ID_LISTETEMPS');
		}else {
			$query->select('DOCUMENT.LISTETEMPS_ETAT AS VAL_STATE');

			$query->addWhere($this->getPerimeterSearch(
				$filter->getSelectedPerimeter(),
				'AT.ID_SOCIETE',
				'AT.ID_POLE',
				['US.ID_PROFIL', 'USRH.ID_PROFIL']
			));

			foreach($filter->validationStates->getValue() as $etat){
				switch($etat){
					case Models\Validation::VALIDATION_VALIDATED:
						$stateWhere->or_('LISTETEMPS_ETAT=1'); break;
					case Models\Validation::VALIDATION_REJECTED:
						$stateWhere->or_('LISTETEMPS_ETAT=3'); break;
					case Models\Validation::VALIDATION_WAITING:
						$stateWhere->or_('LISTETEMPS_ETAT=2'); break;
				}
			}

			$query->groupBy('DOCUMENT.ID_LISTETEMPS');
			$this->setOrderExpr($query, $filter->sort->getValue(), $filter->order->getValue());
		}

		$query->addWhere($stateWhere);

		$query->addWhere('DOCUMENT.LISTETEMPS_DATE BETWEEN ? AND ?', [$filter->startMonth->getValue(), $filter->endMonth->getValue()]);

		if($filter->keywords->isDefined()){
			$whereIds = $this->getListIdSearch($filter->keywords->getValue(), ['COMP'=>'AT.ID_PROFIL', 'DOC'=>'DOCUMENT.ID_LISTETEMPS']);
			if(!$whereIds->isEmpty()){
				$query->addWhere($whereIds);
			}else{

				$keywords = str_replace('*', '%', str_replace('%', '\%', $filter->keywords->getValue()));
				$query->addWhere('AT.PROFIL_NOM LIKE ? OR AT.PROFIL_PRENOM LIKE ?', [$keywords.'%', $keywords.'%']);
			}
		}

		if($filter->resourceTypes->isDefined() && $types = $filter->resourceTypes->getValue()){
			$query->addWhere('AT.PROFIL_TYPE IN (?)', $types);
		}

		if($filter->closed->isDefined()){
			$query->addWhere('LISTETEMPS_CLOTURE = ?', $filter->closed->getValue());
		}

		return $this->launchSearch($query);
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExpr(Query $query, $column, $order) {

		$mapping = [
			SearchTimesReports::ORDERBY_LASTNAME => 'AT.PROFIL_NOM',
			SearchTimesReports::ORDERBY_TERM     => 'DOCUMENT.LISTETEMPS_DATE',
			SearchTimesReports::ORDERBY_STATE    => 'DOCUMENT.LISTETEMPS_ETAT',
			SearchTimesReports::ORDERBY_DATE     => 'VAL_DATE'
		];

		if(!$column) $query->addOrderBy('DOCUMENT.LISTETEMPS_DATE DESC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('DOCUMENT.ID_LISTETEMPS DESC');
	}

	/**
	 * Adds an order by to the query
	 * @param Query $query the query to modify
	 * @param array $column the column to sort (front end value)
	 * @param string $order ASC|DESC
	 */
	private function setOrderExprForValidators(Query $query, $column, $order) {

		$mapping = [
			SearchTimesReports::ORDERBY_LASTNAME           => 'AT.PROFIL_NOM',
			SearchTimesReports::ORDERBY_STATE              => 'VAL_ETAT',
			SearchTimesReports::ORDERBY_VALIDATOR_LASTNAME => 'RESP.PROFIL_NOM',
			SearchTimesReports::ORDERBY_DATE               => 'VAL_DATE'
		];

		if(!$column) $query->addOrderBy('DOCUMENT.LISTETEMPS_DATE DESC');

		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$query->addOrderBy($mapping[$c].' '.$order);

		$query->addOrderBy('DOCUMENT.ID_LISTETEMPS DESC');
	}

	/**
	 * Retrieves entity data from its ID
	 * @param  integer  $id  TimesReport ID.
	 * @return \BoondManager\Models\TimesReport|false
	 */
	public function getObject($id)
	{
		$sql = 'SELECT ID_LISTETEMPS, TAB_LISTETEMPS.ID_PROFIL, TAB_LISTETEMPS.ID_SOCIETE, LISTETEMPS_DATE, 
					LISTETEMPS_ETAT, LISTETEMPS_COMMENTAIRES, ID_POLE, ID_RESPMANAGER, UMANAGER.ID_PROFIL AS ID_PROFIL_MANAGER, 
					ID_RESPRH, URH.ID_PROFIL AS ID_PROFIL_RH, TAB_USER.USER_TYPE, LISTETEMPS_CLOTURE, PROFIL_NOM, PROFIL_PRENOM
				FROM TAB_LISTETEMPS 
				INNER JOIN TAB_PROFIL ON TAB_PROFIL.ID_PROFIL = TAB_LISTETEMPS.ID_PROFIL
				LEFT JOIN TAB_USER ON TAB_USER.ID_PROFIL = TAB_LISTETEMPS.ID_PROFIL 
				LEFT JOIN TAB_USER AS URH ON URH.ID_USER = ID_RESPRH
				LEFT JOIN TAB_USER AS UMANAGER ON UMANAGER.ID_USER = ID_RESPMANAGER
				WHERE ID_LISTETEMPS = ?';

		$result = $this->singleExec($sql, $id);

		if(!$result) return false;

		$sql = 'SELECT TAB_LIGNETEMPS.ID_LIGNETEMPS, LIGNETEMPS_TYPEHREF, TAB_LIGNETEMPS.ID_PROJET, TAB_LIGNETEMPS.ID_MISSIONPROJET, 
		               TAB_LIGNETEMPS.ID_LOT, PRJ_REFERENCE, MP_NOM, MP_DEBUT, MP_FIN, CSOC_SOCIETE, TAB_PROJET.ID_CRMSOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE, LOT_TITRE
		        FROM TAB_LIGNETEMPS 
		        LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=LIGNETEMPS_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE= :agency)
		        LEFT JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET) 
		        LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_LIGNETEMPS.ID_PROJET) 
		        LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		        LEFT JOIN TAB_LOT USING(ID_LOT)
		        WHERE ID_LISTETEMPS= :id 
		        GROUP BY ID_LIGNETEMPS 
		        ORDER BY ID_LIGNETEMPS ASC';
		$result['LISTETEMPSNORMAUX'] = $this->exec($sql, ['agency' => $result['ID_SOCIETE'], 'id' => $result['ID_LISTETEMPS']]);
		foreach($result['LISTETEMPSNORMAUX'] as $i => $ligne)
			$result['LISTETEMPSNORMAUX'][$i]['LISTEJOURS'] = $this->exec('SELECT ID_TEMPS, TEMPS_DATE, TEMPS_DUREE, ID_LIGNETEMPS FROM TAB_TEMPS WHERE ID_LIGNETEMPS= ? ORDER BY TEMPS_DATE ASC', $ligne['ID_LIGNETEMPS']);

		$sql = 'SELECT ID_TEMPSEXCEPTION, TEMPSEXP_TYPEHREF, TEMPSEXP_RECUPERATION, TEMPSEXP_DEBUT, TEMPSEXP_FIN, TEMPSEXP_DESCRIPTION, 
		               TAB_TEMPSEXCEPTION.ID_PROJET, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET, TAB_TEMPSEXCEPTION.ID_LOT, PRJ_REFERENCE, 
		               MP_NOM, MP_DEBUT, MP_FIN, CSOC_SOCIETE, TYPEH_NAME, TYPEH_TYPEACTIVITE, LOT_TITRE
		        FROM TAB_TEMPSEXCEPTION 
		        LEFT JOIN TAB_TYPEHEURE ON(TYPEH_REF=TEMPSEXP_TYPEHREF AND TAB_TYPEHEURE.ID_SOCIETE = :agency)
		        LEFT JOIN TAB_MISSIONPROJET USING(ID_MISSIONPROJET) 
		        LEFT JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_TEMPSEXCEPTION.ID_PROJET) 
		        LEFT JOIN TAB_CRMSOCIETE ON(TAB_CRMSOCIETE.ID_CRMSOCIETE=TAB_PROJET.ID_CRMSOCIETE)
		        LEFT JOIN TAB_LOT USING(ID_LOT)
		        WHERE ID_LISTETEMPS = :id 
		        GROUP BY ID_TEMPSEXCEPTION ORDER BY ID_TEMPSEXCEPTION ASC';
		$result['LISTETEMPSEXCEPTION'] = $this->exec($sql, ['agency' => $result['ID_SOCIETE'], 'id' => $result['ID_LISTETEMPS']]);

		//On récupère tous les justificatifs
		$result['JUSTIFICATIFS'] = $this->exec(
			'SELECT ID_JUSTIFICATIF, FILE_NAME FROM TAB_JUSTIFICATIF WHERE ID_PARENT= :id  AND JUSTIF_TYPE=:type',
			['id'=>$id, 'type'=> Models\Proof::TYPE_TIMESREPORT]
		);

		//On récupère toutes les validations
		$result['LISTEVALIDATIONS'] = $this->exec('SELECT ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, TAB_VALIDATION.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT FROM TAB_VALIDATION INNER JOIN TAB_PROFIL ON(ID_VALIDATEUR=TAB_PROFIL.ID_PROFIL) WHERE ID_PARENT= ? AND VAL_TYPE=0 ORDER BY ID_VALIDATION ASC', $id);

		$expenseDB = ExpensesReport::instance();
		$result['ID_LISTEFRAIS'] = $expenseDB->isFraisExist($result['LISTETEMPS_DATE'], $result['ID_PROFIL']);

		return $result;
	}

	/**
	 * Retrieve all timesheet for a resource
	 * @param int $idProfil Profil ID
	 * @return array
	 */
	public function getAllTemps($idProfil) {
		$sql = 'SELECT ID_LISTETEMPS, LISTETEMPS_DATE, LISTETEMPS_ETAT AS VAL_STATE, LISTETEMPS_CLOTURE, ID_VALIDATION 
				FROM TAB_LISTETEMPS LEFT JOIN TAB_VALIDATION ON(ID_PARENT=ID_LISTETEMPS AND VAL_TYPE=0) 
				WHERE TAB_LISTETEMPS.ID_PROFIL=? GROUP BY ID_LISTETEMPS ORDER BY LISTETEMPS_DATE DESC';
		$result = $this->exec($sql, $idProfil);

		return $result;
	}

	/**
	 * check a timesheet exist at a date for a profil
	 * @param int $date Y-m-d format
	 * @param int $idProfil Profil ID
	 * @return int|false the Timesheet ID
	 */
	public function isTempsExist($date, $idProfil) {
		$result = $this->singleExec('SELECT ID_LISTETEMPS FROM TAB_LISTETEMPS WHERE LISTETEMPS_DATE=:date  AND ID_PROFIL=:profil', ['date'=>$date, 'profil'=>$idProfil]);
		if($result) return $result['ID_LISTETEMPS'];
		return false;
	}

	/**
	 * Retrieve all warnings for time not allocated on a project
	 * @param int $idlistetemps Timesheet ID
	 * @return array with all warnings
	 */
	public function getTpsPrjWarnings($idlistetemps) {
		$warnings = [];

		$sql = 'SELECT 0 AS ID_PROJET, 0 AS ID_MISSIONPROJET, "" AS PRJ_REFERENCE, COUNT(DISTINCT TAB_CONTRAT.ID_CONTRAT) AS NB_MISSION
				FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROFIL ON(TAB_PROFIL.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL)
					LEFT JOIN TAB_CONTRAT ON(TAB_CONTRAT.ID_PROFIL=TAB_LISTETEMPS.ID_PROFIL AND TAB_CONTRAT.ID_SOCIETE=TAB_LISTETEMPS.ID_SOCIETE AND TEMPS_DATE BETWEEN CTR_DEBUT AND CTR_FIN)
				WHERE TAB_LISTETEMPS.ID_LISTETEMPS=:id AND PROFIL_TYPE<>'.Models\Employee::TYPE_EXTERNAL_RESOURCE.' GROUP BY ID_TEMPS HAVING NB_MISSION=0
				 UNION ALL
				SELECT TAB_PROJET.ID_PROJET, TAB_LIGNETEMPS.ID_MISSIONPROJET, PRJ_REFERENCE, COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) AS NB_MISSION
				FROM TAB_LIGNETEMPS INNER JOIN TAB_TEMPS USING(ID_LIGNETEMPS) INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_LIGNETEMPS.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_LIGNETEMPS.ID_MISSIONPROJET AND TEMPS_DATE BETWEEN MP_DEBUT AND MP_FIN)
				WHERE TAB_LISTETEMPS.ID_LISTETEMPS=:id GROUP BY ID_TEMPS HAVING NB_MISSION=0
				 UNION ALL
				SELECT TAB_PROJET.ID_PROJET, TAB_TEMPSEXCEPTION.ID_MISSIONPROJET, PRJ_REFERENCE, COUNT(DISTINCT TAB_MISSIONPROJET.ID_MISSIONPROJET) AS NB_MISSION
				FROM TAB_TEMPSEXCEPTION INNER JOIN TAB_LISTETEMPS USING(ID_LISTETEMPS) INNER JOIN TAB_PROJET ON(TAB_PROJET.ID_PROJET=TAB_TEMPSEXCEPTION.ID_PROJET)
					LEFT JOIN TAB_MISSIONPROJET ON(TAB_MISSIONPROJET.ID_MISSIONPROJET=TAB_TEMPSEXCEPTION.ID_MISSIONPROJET AND DATE(TEMPSEXP_DEBUT) BETWEEN MP_DEBUT AND MP_FIN AND DATE(TEMPSEXP_FIN) BETWEEN MP_DEBUT AND MP_FIN)
				WHERE TAB_LISTETEMPS.ID_LISTETEMPS=:id GROUP BY ID_TEMPSEXCEPTION HAVING NB_MISSION=0;';
		$result = $this->exec($sql, ['id'=>$idlistetemps]);


		foreach($result as $row)
			if($row['NB_MISSION'] == 0)
				$warnings[ $row['ID_MISSIONPROJET'] ] = $row;

		return $warnings;
	}

	/**
	 * Set a timesreport
	 * @param array $data an array with the new values group by category (TIMESHEET, LISTETEMPSNORMAUX, LISTETEMPSEXCEPTION)
	 * @param integer $id timesreport ID
	 */
	public function setObject($data, $id = null)
	{
		if(isset($data['TIMESHEET'])) {
			if($id > 0)
				$this->update('TAB_LISTETEMPS', $data['TIMESHEET'], 'ID_LISTETEMPS=:id', [':id' => $id]);
			else
				$id = $this->insert('TAB_LISTETEMPS', $data['TIMESHEET']);
		}

		if(isset($data['LISTETEMPSNORMAUX']) && $id > 0) {

			//On supprime les lignes de temps et les temps qui n'existent plus (cad celle qui n'ont plus aucun temps associé cf. service timesreports)
			$ids = array_column($data['LISTETEMPSNORMAUX'], 'ID_LIGNETEMPS');
			if($ids) {
				$deletedTimesEntries = $this->exec('SELECT ID_LIGNETEMPS FROM TAB_LIGNETEMPS WHERE ID_LISTETEMPS=? AND ID_LIGNETEMPS NOT IN (' . Where::prepareWhereIN($ids) . ')'
					, array_merge([$id], $ids));
				if($deletedTimesEntries) {
					$deletedTimesEntriesIds = Tools::getFieldsToArray($deletedTimesEntries, 'ID_LIGNETEMPS');

					$this->delete('TAB_LIGNETEMPS', 'ID_LIGNETEMPS IN (' . Where::prepareWhereIN($deletedTimesEntriesIds) . ')', $deletedTimesEntriesIds);
					$this->delete('TAB_TEMPS', 'ID_LIGNETEMPS IN (' . Where::prepareWhereIN($deletedTimesEntriesIds) . ')', $deletedTimesEntriesIds);
				}
			}

			//On met à jour les lignes de temps existantes
			if(sizeof($data['LISTETEMPSNORMAUX']) > 0) {
				$tabIDs = array_filter(array_column($data['LISTETEMPSNORMAUX'], 'ID_LIGNETEMPS'), function ($id_lignetemps){return $id_lignetemps;});
				$listetps = (sizeof($tabIDs) > 0)?$this->exec('SELECT ID_TEMPS, ID_LIGNETEMPS, TEMPS_DATE FROM TAB_TEMPS WHERE ID_LIGNETEMPS IN(' . Where::prepareWhereIN($tabIDs) . ')', $tabIDs):array();
				foreach($data['LISTETEMPSNORMAUX'] as $timesEntry) {
					$dataLigne = array();
					if($timesEntry['ID_LIGNETEMPS'] > 0) {//La ligne des temps existent déjà
						//On supprime les temps qui n'existent plus de la ligne
						foreach($listetps as $tps) if($timesEntry['ID_LIGNETEMPS'] == $tps['ID_LIGNETEMPS']) {
							$etat = false;
							foreach($timesEntry['LISTEJOURS'] as $data_tps) if($data_tps['TEMPS_DATE'] == $tps['TEMPS_DATE']) {$etat = true;break;}
							if(!$etat) $this->delete('TAB_TEMPS', 'ID_TEMPS=:id', ['id' => $tps['ID_TEMPS']]);
						}

						//On met à jour les temps de la ligne
						foreach($timesEntry['LISTEJOURS'] as $data_tps) {
							$dataTemps = array();

							$etat = false;
							foreach($listetps as $tps) if($timesEntry['ID_LIGNETEMPS'] == $tps['ID_LIGNETEMPS'] && $data_tps['TEMPS_DATE'] == $tps['TEMPS_DATE']) {$etat = true;break;}

							if($etat) {
								$dataTemps['TEMPS_DUREE'] = $data_tps['TEMPS_DUREE'];
//                                $this->db->query_update('TEMPS', $dataTemps, 'ID_TEMPS="'.$tps['ID_TEMPS'].'"');
								$this->update('TAB_TEMPS', $dataTemps, 'ID_TEMPS=:id', ['id' => $tps['ID_TEMPS']]);
							} else {
								$dataTemps['TEMPS_DATE'] = $data_tps['TEMPS_DATE'];
								$dataTemps['TEMPS_DUREE'] = $data_tps['TEMPS_DUREE'];
								$dataTemps['ID_LIGNETEMPS'] = $timesEntry['ID_LIGNETEMPS'];
//                                $this->db->query_insert('TEMPS', $dataTemps);
								$this->insert('TAB_TEMPS', $dataTemps);
							}
						}
						$dataLigne['ID_PROJET'] = $timesEntry['ID_PROJET'];
						$dataLigne['ID_MISSIONPROJET'] = $timesEntry['ID_MISSIONPROJET'];
						$dataLigne['LIGNETEMPS_TYPEHREF'] = $timesEntry['LIGNETEMPS_TYPEHREF'];
						$dataLigne['ID_LOT'] = $timesEntry['ID_LOT'];
//                        $this->db->query_update('LIGNETEMPS', $dataLigne, 'ID_LIGNETEMPS="'.$timesEntry[0].'"');
						$this->update('TAB_LIGNETEMPS', $dataLigne, 'ID_LIGNETEMPS=:id', ['id' => $timesEntry['ID_LIGNETEMPS']]);
					} else {//La ligne des temps est nouvelle, toute la ligne est à créer avec tous les temps
						$dataLigne = array();
						$dataLigne['ID_LISTETEMPS'] = $id;
						$dataLigne['ID_PROJET'] = $timesEntry['ID_PROJET'];
						$dataLigne['ID_MISSIONPROJET'] = $timesEntry['ID_MISSIONPROJET'];
						$dataLigne['LIGNETEMPS_TYPEHREF'] = $timesEntry['LIGNETEMPS_TYPEHREF'];
						$dataLigne['ID_LOT'] = $timesEntry['ID_LOT'];
						$idLigneTps = $this->insert('TAB_LIGNETEMPS', $dataLigne);
						foreach($timesEntry['LISTEJOURS'] as $tps) {
							$dataTps = array();
							$dataTps['ID_LIGNETEMPS'] = $idLigneTps;
							$dataTps['TEMPS_DATE'] = $tps['TEMPS_DATE'];
							$dataTps['TEMPS_DUREE'] = $tps['TEMPS_DUREE'];
							$this->insert('TAB_TEMPS', $dataTps);
						}
					}
				}
			}
		}

		//On met à jour l'activité exceptionnelle
		if(isset($data['LISTETEMPSEXCEPTION']) && $id > 0) {
			//On supprime les temps exceptionels qui n'existent plus
			$ids = array_column($data['LISTETEMPSEXCEPTION'], 'ID_TEMPSEXCEPTION');
			if($ids){
				$this->delete('TAB_TEMPSEXCEPTION', 'ID_TEMPSEXCEPTION NOT IN ('.Where::prepareWhereIN($ids).') AND ID_LISTETEMPS= ?', array_merge($ids, [$id]));
			}

			//On met à jour les temps exceptionels
			foreach($data['LISTETEMPSEXCEPTION'] as $exceptionalTime) {
				$existingId = $exceptionalTime['ID_TEMPSEXCEPTION'];
				unset($exceptionalTime['ID_TEMPSEXCEPTION']);
				if($existingId) {//Ce temps existe déjà
					$this->update('TAB_TEMPSEXCEPTION', $exceptionalTime, 'ID_TEMPSEXCEPTION=:id', ['id' => $existingId]);
				} else {//Ce temps est nouveau
					$exceptionalTime['ID_LISTETEMPS'] = $id;
					$this->insert('TAB_TEMPSEXCEPTION', $exceptionalTime);
				}
			}
		}
		return $id ? $id:false;
	}

	/**
	 * Delete a timesreport
	 * @param int $id timesreport ID
	 */
	public function deleteObject($id)
	{
		foreach($this->exec('SELECT ID_LIGNETEMPS FROM TAB_LIGNETEMPS WHERE ID_LISTETEMPS=?', $id) as $timesEntry)
			$this->delete('TAB_TEMPS', 'ID_LIGNETEMPS=?', $timesEntry['ID_LIGNETEMPS']);

		$this->delete('TAB_LIGNETEMPS', 'ID_LISTETEMPS=?', $id);

		$this->delete('TAB_VALIDATION', 'ID_PARENT=? AND VAL_TYPE=0', $id);
		$this->delete('TAB_LISTETEMPS', 'ID_LISTETEMPS=?', $id);
		$this->delete('TAB_TEMPSEXCEPTION', 'ID_LISTETEMPS=?', $id);

		//ON SUPPRIME LES JUSTIFICATIFS
		$dbFile = new File();
		$dbFile->deleteAllObjects(File::TYPE_JUSTIFICATIVE, $id, Models\Proof::TYPE_TIMESREPORT);
		return true;
	}

	/**
	 * is a given TimesReport deletable
	 * @param int $id
	 * @return bool
	 */
	function canDeleteObject($id) {
		$sql = 'SELECT COUNT(*) AS NB_DOCUMENT FROM TAB_LISTETEMPS WHERE ID_LISTETEMPS = ? AND LISTETEMPS_CLOTURE=1';
		$result = $this->singleExec($sql, $id);
		return $result['NB_DOCUMENT'] == 0;
	}
}
