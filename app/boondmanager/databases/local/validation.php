<?php
/**
 * validation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Local;

use Wish\MySQL\Query;
use BoondManager\OldModels\Filters;
use BoondManager\Models;
use BoondManager\OldModels\MySQL\Mappers;

class Validation extends AbstractObject{

	/**
	 * search through document requiring a validation
	 * @param Filters\Search\Validations $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	public function search(Filters\Search\Validations $filter)
	{
		if(!$filter->isValid()) throw new \Exception('filter is not valid');

		$query = new Query();
		$query->select('ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, TAB_VALIDATION.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT')
		->from('TAB_VALIDATION
		INNER JOIN TAB_PROFIL ON(ID_VALIDATEUR=TAB_PROFIL.ID_PROFIL)')//IL Y A UN PROBLEME POUR RECUPERER LES NOMS ET PRENOMS
		->addOrderBy('ID_VALIDATION ASC')
		->setModelClass(Models\Validation::class)
		->setMapper(new Mappers\SearchValidation);

		if($filter->type->isDefined()) $query->addWhere('VAL_TYPE=?', Models\Validation::bddFromPublic('TYPE', $filter->type->getValue()));

		if($filter->keywords->isDefined()){
			$whereIds = $this->getListIdSearch($filter->keywords->getValue(), ['DOC'=>'ID_PARENT']);
			if(!$whereIds->isEmpty()){
				$query->addWhere($whereIds);
			}
		}

		return $this->launchSearch($query);
	}

	/**
	 * @param $data
	 * @param null $idProfil
	 * @param null $idParent
	 * @param null $parentType
	 * @param bool $forceInsert
	 * @return bool|int
	 * @throws \Exception
	 */
	public function updateValidationData($data, $idProfil = null, $idParent = null, $parentType = null, $forceInsert = true) {
		if($idProfil === null) {
			if(isset($data['ID_PROFIL'])) $idProfil = $data['ID_PROFIL'];
			else throw new \Exception('missing id profil');
		}

		if($idParent === null) {
			if(isset($data['ID_PARENT'])) $idParent = $data['ID_PARENT'];
			else throw new \Exception('missing id parent');
		}

		if($parentType === null) {
			if(isset($data['VAL_TYPE'])) $parentType = $data['VAL_TYPE'];
			else throw new \Exception('missing parent type');
		}

		// TODO : un jour, faire un INSERT ... ON DUPLICATE KEY UPDATE ... (et utiliser le triplé comme clé unique)
		$result = $this->singleExec('SELECT ID_VALIDATION FROM TAB_VALIDATION WHERE ID_PROFIL= :idProfil AND ID_PARENT = :idParent AND VAL_TYPE = :parentType', ['idProfil' => $idProfil, 'idParent' => $idParent, 'parentType' => $parentType]);
		if($result) {
			if($forceInsert) $data['VAL_DATE'] = date('Y-m-d');
			$this->update('TAB_VALIDATION', $data, 'ID_VALIDATION=:id', [
				'id' => $result['ID_VALIDATION']
			]);
			$this->updateValidationDocument($idParent, $parentType);
			return $result['ID_VALIDATION'];
		} else {
			if($forceInsert) {
				$data['ID_PROFIL'] = $idProfil;
				$data['ID_PARENT'] = $idParent;
				$data['VAL_TYPE'] = $parentType;
				$data['VAL_DATE'] = date('Y-m-d');
				$data['VAL_MOTIF'] = '';
				$id = $this->insert('TAB_VALIDATION', $data);
				$this->updateValidationDocument($idParent, $parentType);
				return $id;
			}
		}
		return false;
	}

	public function getValidation($id) {
		return $this->singleExec('SELECT ID_VALIDATION, ID_PROFIL, ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, VAL_TYPE, ID_PARENT FROM TAB_VALIDATION WHERE ID_VALIDATION = ?', $id);
	}

	/**
	 * Update the validation state
	 * @param $id
	 * @param int $parentType
	 * @return bool
	 */
	public function updateValidationDocument($id, $parentType = 0) {
		$state = $this->singleExec('SELECT MAX(VAL_ETAT) AS VAL_STATE FROM TAB_VALIDATION WHERE ID_PARENT= :idParent AND VAL_TYPE = :parentType', [
			'idParent' => $id,
			'parentType' => $parentType
		]);

		if(!$state || !isset($state['VAL_STATE'])) $state['VAL_STATE'] = 2;

		switch($parentType) {
			case 1:
				$this->update('TAB_LISTEFRAIS', [ 'LISTEFRAIS_ETAT' => $state['VAL_STATE'] ], 'ID_LISTEFRAIS= :id', ['id' => $id]);
				break;
			case 2:
				$this->update('TAB_LISTEABSENCES', [ 'LISTEABSENCES_ETAT' => $state['VAL_STATE'] ], 'ID_LISTEABSENCES= :id', ['id' => $id]);
				break;
			default:
				$this->update('TAB_LISTETEMPS', [ 'LISTETEMPS_ETAT' => $state['VAL_STATE'] ], 'ID_LISTETEMPS= :id', ['id' => $id]);
				break;
		}

		return true;
	}

	/**
	 * \brief Supprime un validateur
	 */
	public function deleteValidationData($id) {
		$val = $this->singleExec('SELECT ID_PARENT, VAL_TYPE FROM TAB_VALIDATION WHERE ID_VALIDATION= ?', $id);
		if($val) {
			$this->delete('TAB_VALIDATION', 'ID_VALIDATION=?', $id);
			$this->updateValidationDocument($val['ID_PARENT'], $val['VAL_TYPE']);
			return true;
		} else
			return false;
	}
}
