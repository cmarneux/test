<?php
/**
 * absence.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use Wish\Models\Model;

class Absence extends Mapper{
	/**
	 * @param Model[] $data
	 * @return Models\AbsencesQuota[]
	 */
	public static function fromAllQuotas($data){
		$quotas = [];
		foreach($data as $row){
			/** @var Models\AbsencesQuota $quota */
			$quotas[] = $quota = self::createObject(Models\AbsencesQuota::class, $row);
			$quota->agency = self::createObject(Models\Agency::class, $row);

			$quota->workUnitType = self::createObject(Models\WorkUnitType::class, $row, $noId = true);
			$quota->period = self::getPeriodFromINTQUO_PERIODE($row['INTQUO_PERIODE']);
			$quota->year = self::getYearFROMINTQUO_PERIODE($row['INTQUO_PERIODE']);
		}
		return $quotas;
	}

	public static function getPeriodFromINTQUO_PERIODE($period) {
		$exploded = explode('-', $period);
		return Models\AbsencesQuota::ACCOUNTSPERIOD[intval($exploded[1])];
	}

	public static function getYearFROMINTQUO_PERIODE($period) {
		$exploded = explode('-', $period);
		return intval($exploded[0]);
	}

	/**
	 * @param array $data
	 * @return Models\Absence[]
	 */
	public static function fromListeAbsencesPoses($data)
	{
		$abs = [];
		foreach ($data as $row){
			/** @var Models\Absence $a */
			$abs[] = $a = self::createObject(Models\Absence::class, $row);
			$a->workUnitType = self::createObject(Models\WorkUnitType::class, $row, $noId = true);
			$a->absencesReport = self::createObject(Models\AbsencesReport::class, $row);
			$a->absencesReport->agency = self::createObject(Models\Agency::class, $row);
		}
		return $abs;
	}

	/**
	 * @param array $data
	 * @return Models\Time[]
	 */
	public static function fromListeAbsencesPrises($data)
	{
		$abs = [];
		foreach ($data as $row){
			/** @var Models\Time $t */
			$abs[] = $t = self::createObject(Models\Time::class, $row);

			$t->workUnitType = self::createObject(Models\WorkUnitType::class, $row, $noId = true);

			/** @var TimesReport $t */
			$t->timesReport = $tr = self::createObject(Models\TimesReport::class, $row);

			$tr->agency = self::createObject(Models\Agency::class, $row);
		}
		return $abs;
	}
}
