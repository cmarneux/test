<?php
/**
 * absencesquota.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Exceptions;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;

/**
 * Class AbsencesQuota
 * @package BoondManager\Databases\Mapper
 */
class AbsencesQuota extends Mapper {
    /**
     * Convert data from SQL to camelCase
     * @param Model $data
     * @return mixed
     * @throws Exceptions\DatabaseIntegrity
     */
    public static function fromSQL($data) {
        /** @var Models\AbsencesQuota $absencesquota */
        $absencesquota = self::createObject(Models\AbsencesQuota::class, $data);
        $absencesquota->workUnitType = self::createObject(Models\WorkUnitType::class, $data);
        return $absencesquota;
    }

    /**
     * Convert entity's data from camelCase to SQL
     * @param Models\AbsencesQuota $absencesquota
     * @return array
     */
    public static function toSQL(Models\AbsencesQuota $absencesquota) {
        $data = self::modelToDatabaseArray($absencesquota);
        $data['TYPED_TYPEHREF'] = $absencesquota->workUnitType->reference;
        return $data;
    }
}
