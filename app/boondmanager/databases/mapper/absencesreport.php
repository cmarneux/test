<?php
/**
 * searchabsencesreport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Models\SearchResult;

class AbsencesReport extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public static function fromRow($data)
	{
		/** @var Models\AbsencesReport $absencesReport */
		$absencesReport = self::createObject(Models\AbsencesReport::class, self::extractData($data, [
			'ID_DOCUMENT'   => 'ID_LISTEABSENCES',
			'DOCUMENT_DATE' => 'LISTEABSENCES_DATE',
			'DOCUMENT_ETAT' => 'LISTEABSENCES_ETAT',
			'LISTEABSENCES_TITRE',
			'LISTEABSENCES_COMMENTAIRES',
		]));

		$absencesReport->resource = self::createObject(Models\Employee::class, self::extractData($data, [
			'ID_PROFIL',
			'PROFIL_NOM',
			'PROFIL_PRENOM',
			'PROFIL_TYPE',
			'PROFIL_REFERENCE'
		]));
		$absencesReport->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		if($data['LISTE_PABSENCES']) {
			$absencesList = [];
			foreach($data['LISTE_PABSENCES'] as $absenceData) {
				/** @var Models\Absence $absence */
				$absence = self::createObject(Models\Absence::class, $absenceData);
				$absence->workUnitType = self::createObject(Models\WorkUnitType::class, self::extractData($absenceData, [
					'PABS_TYPEHREF' => 'TYPEH_REF',
					'TYPEH_TYPEACTIVITE',
					'TYPEH_NAME'
				]), $noId = true);
				$absencesList[] = $absence;
			}
			$absencesReport->absencesPeriods = $absencesList;
		}

		return $absencesReport;
	}

	/**
	 * @param $searchResult
	 * @return SearchResult
	 */
	public static function fromSearchResult($searchResult)
	{
		$reports = [];
		foreach($searchResult->rows as $data){
			// on extrait d'abord les données relatives au validateur s'il existe
			if(isset($data['RESP_ID_PROFIL']) && $data['RESP_ID_PROFIL']){
				/** @var Employee $expectedValidator */
				$expectedValidator = self::createObject(Models\Employee::class, self::extractData($data, [
					'RESP_ID_PROFIL' => 'ID_PROFIL',
					'RESP_NOM'       => 'PROFIL_NOM',
					'RESP_PRENOM'    => 'PROFIL_PRENOM'
				]));

				/** @var Models\Validation $validation */
				$validation = self::createObject(Models\Validation::class, self::extractData($data, [
					'ID_VALIDATION',
					'VAL_DATE',
					'VAL_STATE' => 'VAL_ETAT'
				]));

				$validation->expectedValidator = $expectedValidator;
			}

			// on s'assure de travailler avec un tableau associatif (cle = chaine) pour maintenir l'ordre du search)
			$id = strval($data['ID_DOCUMENT']);

			if(!array_key_exists($id, $reports)){
				unset($validationsList);
				$report = self::fromRow($data);
				$reports[$id] = $report;
			}else{
				$report = $reports[$id];
			}

			if(isset($validation)){
				if(!isset($validationsList)) $validationsList = [];
				$validationsList[] = $validation;
				$report->validations = $validationsList;
				unset($validation);
			}
		}

		$searchResult->rows = array_values($reports);
		return $searchResult;
	}

	/**
	 * @param \Wish\Models\Model $object
	 * @return Models\AbsencesReport
	 */
	public static function fromSQL($object)
	{
		/** @var Models\AbsencesReport $entity */
		$entity = self::createObject(Models\AbsencesReport::class, $object);

		$entity->agency = Services\Agencies::getBasic($object['ID_SOCIETE']);
		$entity->resource = self::createObject(Models\Employee::class, self::extractData($object, [
			'ID_PROFIL'
		]));
		$entity->resource->mainManager = Services\Managers::getBasic($object['ID_PROFIL_RESPMANAGER']);
		$entity->resource->hrManager = Services\Managers::getBasic($object['ID_PROFIL_RESPRH']);
		$entity->resource->pole = Services\Poles::getBasic($object['ID_POLE']);

		$proofs = [];
		foreach($object['PROOFS'] as $pr){
			$proofs[] = self::createObject(Models\Proof::class, $pr);
		}
		$entity->files = $proofs;

		$absences = [];
		foreach($object['LISTEABSENCES'] as $row){
			/** @var Models\Absence $abs */
			$absences[] = $abs = self::createObject(Models\Absence::class, $row);
			$abs->workUnitType = self::createObject(Models\WorkUnitType::class, self::extractData($row, [
				'PABS_TYPEHREF' => 'TYPEH_REF',
				'TYPEH_TYPEACTIVITE',
				'TYPEH_NAME'
			]), $noId = true);
		}
		$entity->absencesPeriods = $absences;

		$validations = [];
		foreach ($object['VALIDATIONS'] as $row){
			/** @var Models\Validation $val */
			$validations[] = $val = self::createObject(Models\Validation::class, $row);
			$val->realValidator = Services\Managers::getBasic($row['ID_VALIDATEUR']);
			$val->expectedValidator = Services\Managers::getBasic($row['ID_PROFIL']);
		}
		$entity->validations = $validations;

		return $entity;
	}

	/**
	 * @param Models\AbsencesReport $entity
	 * @return array
	 */
	public static function toSQL(Models\AbsencesReport $entity)
	{
		$sqlData = [];
		$sqlData['DEMANDEABSENCES'] = self::modelToDatabaseArray($entity);
		$sqlData['DEMANDEABSENCES']['ID_PROFIL'] = $entity->resource->id;
		$sqlData['DEMANDEABSENCES']['ID_SOCIETE'] = $entity->agency->id;

		$sqlData['LISTEABSENCES'] = [];
		foreach($entity->absencesPeriods as $absence) {
			$sqlAbsence = self::modelToDatabaseArray($absence);
			$sqlAbsence['PABS_TYPEHREF'] = $absence->workUnitType->reference;
			$sqlData['LISTEABSENCES'][] = $sqlAbsence;
		}

		return $sqlData;
	}
}
