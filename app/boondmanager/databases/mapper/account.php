<?php
/**
 * account.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Services\BM;
use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Models\Model;
use Wish\Tools;

class Account extends Mapper {
	/**
	 * Convert searchresult's row from SQL to camelCase
	 * @param Model $data
	 * @return Models\Account
	 */
	public static function fromRow($data) {
		$account = self::createObject(Models\Account::class, $data);

		if(BM::isCustomerInterfaceActive() && ($account->isManager() || $account->isEmployee())) {
			$account->mainManager = Services\Managers::getBasic($data['ID_PROFIL_RESPMANAGER']);
			$account->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
			$account->pole = Services\Poles::getBasic($data['ID_POLE']);

			$account->role = self::createObject(Models\Role::class, $data);
		}

		return $account;
	}

	/**
	 * @param Model $data
	 * @return Models\Account
	 */
	public static function fromSQL($data) {
		/**
		 * @var Models\Account $account
		 */
		if(!isset($data['ID_PROFIL']) || $data['ID_PROFIL'] == 0)
			$data['ID_PROFIL'] = Models\Account::unserializeAccount($data['USER_TYPE']).'_'.$data['ID_USER'];

		$account = self::createObject(Models\Account::class, $data);

		if(BM::isCustomerInterfaceActive()) {
			if ($account->isManager() || $account->isEmployee()) {
				$account->mainManager = Services\Managers::getBasic($data['ID_PROFIL_RESPMANAGER']);
				$account->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
				$account->pole = Services\Poles::getBasic($data['ID_POLE']);

				$account->role = self::createObject(Models\Role::class, $data);
			}

			if (isset($data['CONFIGUSER'])) {
				if ($account->isManager() || $account->isAdministrator())
					$account->modules = self::fromModulesConfig($data['CONFIGUSER']);
				else if($account->isEmployee()) {
					$account->modules = new Models\AccountModules([
						BM::MODULE_PROJECTS => true,
					]);
				}

				if ($account->isManager()) {
					$otherSocietes = [];
					if($data['CONFIGUSER']['CONFIG_SOCIETES'] != '' && $account->agency) {
						$tabSocietes = Tools::unserializeArray($data['CONFIGUSER']['CONFIG_SOCIETES']);
						foreach($tabSocietes as $idsociete) {
							if($idsociete != $account->agency->id)
								$otherSocietes[] = Services\Agencies::getBasic($idsociete);
						}
					}
					$account->otherAgencies = $otherSocietes;

					$otherPoles = [];
					if($data['CONFIGUSER']['CONFIG_POLES'] != '' && $account->pole) {
						$tabPoles = Tools::unserializeArray($data['CONFIGUSER']['CONFIG_POLES']);
						foreach($tabPoles as $idpole) {
							if($idpole != $account->pole->id)
								$otherPoles[] = Services\Poles::getBasic($idpole);
						}
					}
					$account->otherPoles = $otherPoles;

					$account->rights = self::fromRightsConfig($data['CONFIGUSER']);

					list(
						$account->dashboardGraphsAvailable,
						$account->dashboardGraphsPeriod,
						$account->dashboardGraphsPerimeter
						) = self::fromDashboardConfig($data['CONFIGUSER']['CONFIG_DASHBOARD']);
				}

				if ($account->isManager() || $account->isEmployee()) {
					list(
						$account->apps,
						$account->advancedAppCalendar,
						$account->advancedAppMail,
						$account->advancedAppEmailing,
						$account->advancedAppViewer
						) = self::fromAppsConfig($data['CONFIGUSER']);


					$account->navigationBar = Models\Employee::unserializeNavigation($data['CONFIGUSER']['CONFIG_MENUBAR']);
				}
			}

			/*devices*/
		}

		return $account;
	}

	/**
	 * @param Models\Account $account user level
	 * @param Model $data
	 * @return Models\AccountModules
	 */
	private static function fromModulesConfig($data){
		return self::createObject(Models\AccountModules::class, $data);
	}

	/**
	 * @param Model $data
	 * @return Models\AccountRights
	 */
	private static function fromRightsConfig($data){
		$userRights = new Models\AccountRights();
		$userRights->main = Mapper::createObject(Models\AccountRights\Main::class, $data);
		$userRights->resources = Mapper::createObject(Models\AccountRights\Resources::class, $data);
		$userRights->candidates = Mapper::createObject(Models\AccountRights\Candidates::class, $data);
		$userRights->flags = Mapper::createObject(Models\AccountRights\Flags::class, $data);
		$userRights->opportunities = Mapper::createObject(Models\AccountRights\Opportunities::class, $data);
		$userRights->crm = Mapper::createObject(Models\AccountRights\CRM::class, $data);
		$userRights->products = Mapper::createObject(Models\AccountRights\Products::class, $data);
		$userRights->projects = Mapper::createObject(Models\AccountRights\Projects::class, $data);
		$userRights->billing = Mapper::createObject(Models\AccountRights\Billing::class, $data);
		$userRights->reporting = Mapper::createObject(Models\AccountRights\Reporting::class, $data);
		$userRights->actions = Mapper::createObject(Models\AccountRights\Actions::class, $data);
		$userRights->activityExpenses = Mapper::createObject(Models\AccountRights\ActivityExpenses::class, $data);
		$userRights->reporting = Mapper::createObject(Models\AccountRights\Reporting::class, $data);
		$userRights->purchases = Mapper::createObject(Models\AccountRights\Purchases::class, $data);

		return $userRights;
	}

	/**
	 * Return an array with the following attributes for Account or Role :
	 * 0 => dashboardGraphsAvailable
	 * 1 => dashboardGraphsPeriod
	 * 2 => dashboardGraphsPerimeter
	 *
	 * @param array|string $tabDashboard
	 * @return array
	 */
	public static function fromDashboardConfig($tabDashboard) {
		if(!$tabDashboard) $tabDashboard = [];
		if(is_string($tabDashboard)) $tabDashboard = explode(' ', $tabDashboard);

		//$tabDashboard = $data['CONFIG_DASHBOARD'] != '' ? explode(' ', $data['CONFIG_DASHBOARD']) : [];

		$dashboardGraphsPeriod = Models\Employee::DASHBOARDPERIOD_THISYEAR;

		$dashboardGraphsPerimeter = [
			'agencies' => [],
			'businessUnits' => [],
			'managers' => [],
			'poles' => []
		];
		if(!isset($tabDashboard[0]) || $tabDashboard[0] == '') {
			if(Services\CurrentUser::instance()->isManager())
				$dashboardGraphsPerimeter['managers'][] = ['id' => Services\CurrentUser::instance()->getEmployeeId()];
		} else {
			$tabFilters = null;
			parse_str($tabDashboard[0], $tabFilters);
			foreach($tabFilters as $filter => $value) {
				switch($filter) {
					case 'id':
						if(!is_array($value)) $value = [$value];
						foreach($value as $v) {
							if ($v < 0) {
								$dashboardGraphsPerimeter['agencies'][] = Services\Agencies::getBasic(abs($v));
							} else if (substr($v, 0, 2) == 'BU') {
								$dashboardGraphsPerimeter['businessUnits'][] = Services\BusinessUnits::getBasic(substr($v, 3));
							} else if (substr($v, 0, 4) == 'POLE') {
								$dashboardGraphsPerimeter['poles'][] = Services\Poles::getBasic(substr($v, 5));
							} else {
								$dashboardGraphsPerimeter['managers'][] = Services\Managers::getBasic(intval($v));
							}
						}
						break;
					case 'ps':
						$dashboardGraphsPeriod = Tools::mapData($value, Models\Employee::DASHBOARDPERIODS_MAPPER);
						break;
				}
			}
		}

		if (!isset($tabDashboard[1]) || $tabDashboard[1] == '')
			$dashboardGraphsAvailable = [Models\Employee::DASHBOARDGRAPH_SUMMARY];
		else
			$dashboardGraphsAvailable = Tools::mapData(explode('|',$tabDashboard[1]), (array)Models\Employee::DASHBOARDGRAPHS_MAPPER);

		return [$dashboardGraphsAvailable, $dashboardGraphsPeriod, $dashboardGraphsPerimeter];
	}

	/**
	 * Return an array with the following attributes for Account or Role :
	 * 0 => apps
	 * 1 => advancedAppCalendar
	 * 2 => advancedAppMail
	 * 3 => advancedAppEmailing
	 * 4 => advancedAppViewer
	 *
	 * @param \Wish\Models\Model $data
	 * @param Models\Account $account
	 * @return array
	 */
	public static function fromAppsConfig($data, Models\Account $account = null) {
		$installedApps = Services\Apps::getAllApps($fullInfos = true);
		$allowedApps = explode('_', $data['CONFIG_APIS']);

		$tabApps = [];
		$advancedAppCalendar = null;
		$advancedAppEmailing = null;
		$advancedAppMail = null;
		$advancedAppViewer = null;
		foreach ($installedApps as $app) {
			$addApp = false;
			switch($app->visibility) {
				case Models\App::VISIBILITY_ALLMANAGERS:
					if($account && $account->isManager()) $addApp = true;
					break;
				case Models\App::VISIBILITY_ALLOWEDMANAGERSANDACTIVESRESOURCES:
					if($account && $account->isEmployee()) $addApp = true;
					break;
				case Models\App::VISIBILITY_ALLMANAGERSANDRESOURCES:
					$addApp = true;
					break;
				default:
					if (in_array($app->id, $allowedApps)) $addApp = true;
					break;
			}

			if ($addApp) {
				$tabApps[] = $app;

				switch ($app->category) {
					case Models\App::CATEGORY_CALENDAR:
						if ($app->id == $data['CONFIG_APICALENDAR'])
							$advancedAppCalendar = $app;
						break;
					case Models\App::CATEGORY_EMAILING:
						if ($app->id == $data['CONFIG_APIEMAILING'])
							$advancedAppEmailing = $app;
						break;
					case Models\App::CATEGORY_MAIL:
						if ($app->id == $data['CONFIG_APIMAIL'])
							$advancedAppMail = $app;
						break;
					case Models\App::CATEGORY_MAILCALENDAR:
						if ($app->id == $data['CONFIG_APICALENDAR'])
							$advancedAppCalendar = $app;
						if ($app->id == $data['CONFIG_APIMAIL'])
							$advancedAppMail = $app;
						break;
					case Models\App::CATEGORY_VIEWER:
						if ($app->id == $data['CONFIG_APIVIEWER'])
							$advancedAppViewer = $app;
						break;
				}
			}
		}
		return [$tabApps, $advancedAppCalendar, $advancedAppMail, $advancedAppEmailing, $advancedAppViewer];
	}
}
