<?php
/**
 * action.php
 * @author Darras Louis <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Services\BM;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;
use BoondManager\Models;
use BoondManager\Services;

/**
 * Class Action
 * @package BoondManager\Databases\Mapper
 */
class Action extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Action
	 */
	public static function fromRow($data){
		$contextId = Tools::getContext()->startTiming('action mapping', $group = true);

		/** @var Models\Action $action */
		$action = self::createObject(Models\Action::class, $data);

		$data = $data->toArray();

		switch ($action->category){

			case BM::CATEGORY_CRM_CONTACT:
				$action->dependsOn = self::createObject(Models\Contact::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_CRMCONTACT',
					'RAPPEL_NOM' => 'CCON_NOM',
					'RAPPEL_PRENOM' => 'CCON_PRENOM',
					'RAPPEL_TEL1' => 'CCON_TEL1',
					'RAPPEL_TEL2' => 'CCON_TEL2',
					'RAPPEL_TYPE' => 'CCON_TYPE',
					'RAPPEL_EMAIL' => 'CCON_EMAIL',
				]));
				$action->dependsOn->company = self::createObject(Models\Company::class, self::extractData($data, [
					'RAPPEL_IDPARENT' => 'ID_CRMSOCIETE',
					'RAPPEL_SOCIETE' => 'CSOC_SOCIETE'
				]));
				break;
			case BM::CATEGORY_CRM_COMPANY:
				$action->dependsOn =  self::createObject(Models\Company::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_CRMSOCIETE',
					'RAPPEL_TEL1' => 'CSOC_TEL',
					'RAPPEL_TYPE' => 'CSOC_TYPE',
					'RAPPEL_SOCIETE' => 'CSOC_SOCIETE'
				]));
				break;
			case BM::CATEGORY_CANDIDATE:
				$action->dependsOn = self::createObject(Models\Candidate::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_PROFIL',
					'RAPPEL_NOM' => 'PROFIL_NOM',
					'RAPPEL_PRENOM' => 'PROFIL_PRENOM',
					'RAPPEL_TEL1' => 'PROFIL_TEL1',
					'RAPPEL_TEL2' => 'PROFIL_TEL2',
					'RAPPEL_TYPE' => 'PROFIL_TYPE',
					'RAPPEL_EMAIL' => 'PROFIL_EMAIL',
					'RAPPEL_SOCIETE' => 'PROFIL_STATUT'
				]));
				break;
			case BM::CATEGORY_RESOURCE:
				$action->dependsOn = self::createObject(Models\Employee::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_PROFIL',
					'RAPPEL_NOM' => 'PROFIL_NOM',
					'RAPPEL_PRENOM' => 'PROFIL_PRENOM',
					'RAPPEL_TEL1' => 'PROFIL_TEL1',
					'RAPPEL_TEL2' => 'PROFIL_TEL2',
					'RAPPEL_TYPE' => 'PROFIL_TYPE',
					'RAPPEL_EMAIL' => 'PROFIL_EMAIL',
					'RAPPEL_SOCIETE' => 'PROFIL_STATUT'
				]));
				break;
			case BM::CATEGORY_PRODUCT:
				$action->dependsOn = self::createObject(Models\Product::class, self::extractData($data,[
					'RAPPEL_ID' => 'ID_PRODUIT',
					'RAPPEL_NOM' => 'PRODUIT_NOM',
					'RAPPEL_TYPE' => 'PRODUIT_REF',
				]));
				break;
			case BM::CATEGORY_OPPORTUNITY:
				$action->dependsOn = self::createObject(Models\Opportunity::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_AO',
					'RAPPEL_TYPE' => 'AO_TITLE',
				]));
				self::extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_PROJECT:
				$action->dependsOn = self::createObject(Models\Project::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_PROJET',
					'RAPPEL_TYPE' => 'PRJ_REFERENCE',
				]));
				self::extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_BILLING:
				$action->dependsOn = self::createObject(Models\Invoice::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_FACTURATION',
					'RAPPEL_TYPE' => 'FACT_REF',
				]));
				self::extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_ORDER:
				$action->dependsOn = self::createObject(Models\Order::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_BONDECOMMANDE',
					'RAPPEL_TYPE' => 'BDC_REFCLIENT',
				]));
				self::extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_PURCHASE:
				$action->dependsOn = self::createObject(Models\Purchase::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_ACHAT',
					'RAPPEL_TYPE' => 'ACHAT_TITLE',
				]));
				self::extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_APP:
				$action->dependsOn = self::createObject(Models\App::class, self::extractData($data, [
					'RAPPEL_ID' => 'ID_MAINAPI'
				]));
				break;
		}

		if($data['RESP_ID']) $action->mainManager = Services\Managers::getBasic($data['RESP_ID']);

		Tools::getContext()->endTiming($contextId);

		return $action;
	}

	public static function fromSQL($object)
	{
		/** @var Models\Action $action */
		$action = self::createObject(Models\Action::class, $object);

		if($object['ID_PROFIL_RESPUSER']) $action->mainManager = Services\Managers::getBasic($object['ID_PROFIL_RESPUSER']);

		switch ($action->category){
			case BM::CATEGORY_CRM_CONTACT: $action->dependsOn = new Models\Contact(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_CRM_COMPANY: $action->dependsOn = new Models\Company(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_CANDIDATE: $action->dependsOn = new Models\Candidate(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_RESOURCE: $action->dependsOn = new Models\Employee(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_PRODUCT: $action->dependsOn = new Models\Product(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_OPPORTUNITY: $action->dependsOn = new Models\Opportunity(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_PROJECT: $action->dependsOn = new Models\Project(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_BILLING: $action->dependsOn = new Models\Invoice(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_ORDER: $action->dependsOn = new Models\Order(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_PURCHASE: $action->dependsOn = new Models\Purchase(['id' => $object['ID_PARENT']]); break;
			case BM::CATEGORY_APP: $action->dependsOn = new Models\App(['id' => $object['ID_PARENT']]); break;
		}

		return $action;
	}

	private static function extractContactAndCompany(Model $object, &$data){
		if($data['RAPPEL_IDPARENT'])
			$object->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'RAPPEL_IDPARENT' => 'ID_CRMCONTACT',
				'RAPPEL_NOM'      => 'CCON_NOM',
				'RAPPEL_PRENOM'   => 'CCON_PRENOM',
				'RAPPEL_TEL1'     => 'CCON_TEL1',
				'RAPPEL_TEL2'     => 'CCON_TEL2',
				'RAPPEL_EMAIL'    => 'CCON_EMAIL',
			]));
		if($data['RAPPEL_IDPARENT2'])
			$object->company = self::createObject(Models\Company::class, self::extractData($data, [
				'RAPPEL_IDPARENT2' => 'ID_CRMSOCIETE',
				'RAPPEL_SOCIETE'   => 'CSOC_SOCIETE'
			]));
	}

	/**
	 * @param Models\Action $action
	 * @return array
	 */
	public static function toSQL($action)
	{
		$data = self::modelToDatabaseArray($action);
		if(!isset($data['ACTION_DATE'])) $data['ACTION_DATE'] = date('Y-m-d H:i:s');
		// $data['ID_OTHER'] = ?; //TODO
		$data['ID_PARENT'] = $action->dependsOn->id;
		$data['ID_RESPUSER'] = Services\Managers::getUserIdFromEmployeeId($action->mainManager->id);
		return $data;
	}
}
