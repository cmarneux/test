<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;
use BoondManager\Models;
use BoondManager\Services;

/**
 * Class Advantages
 * @package BoondManager\Databases\Mapper
 */
class Advantages extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Advantage
	 */
	public static function fromRow($data){
		$contextId = Tools::getContext()->startTiming('advantage mapping', $group = true);

		$data = $data->toArray();

		/** @var Models\Advantage $advantage */
		$advantage = self::createObject(Models\Advantage::class, $data);
		$advantage->agency = self::createObject(Models\Agency::class, $data);


		$advantage->resource = self::createObject(Models\Employee::class, self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_VISIBILITE', 'PROFIL_TYPE',
			'USER_TYPE'
		]));

		$advantage->resource->agency = Services\Agencies::getBasic($data['COMP_IDSOCIETE']);

		if($data['ID_PROFIL_RESPMANAGER'])
			$advantage->resource->mainManager = Services\Managers::getBasic($data['ID_PROFIL_RESPMANAGER']);

		if($data['ID_PROFIL_RESPRH'])
			$advantage->resource->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RESPRH']);

		if($data['ID_PROJET']){
			$advantage->project = self::createObject(Models\Project::class, self::extractData($data, [
				'ID_PROJET', 'PRJ_REFERENCE'
			]));
		}

		$advantage->advantageType = self::createObject(Models\AdvantageType::class, self::extractData($data, [
			'TYPEA_NAME',
			'AVG_TYPEAREF'           => 'TYPEA_REF',
			'AVG_TYPE'               => 'TYPEA_TYPE',
			'AVG_CATEGORIE'          => 'TYPEA_CATEGORIE',
			'AVG_QUOTAPARTICIPATION' => 'TYPEA_QUOTAPARTICIPATION',
			'AVG_QUOTASOCIETE'       => 'TYPEA_QUOTASOCIETE',
			'AVG_QUOTARESSOURCE'     => 'TYPEA_QUOTARESSOURCE'
		]), $noId = true);

		$advantage->calculateCostPaid();

		Tools::getContext()->endTiming($contextId);

		return $advantage;
	}

	public static function fromAdvantagesTypes($data){
		$types = [];
		foreach($data as $row){
			$types[] = self::createObject(Models\AdvantageType::class, $row, $noId = true);
		}
		return $types;
	}

	/**
	 * @param Model $data
	 * @return Models\Advantage
	 */
	public static function fromSQL($data){
		/** @var Models\Advantage $advantage */
		$advantage = self::createObject(Models\Advantage::class, $data);

		$advantage->resource = self::createObject(Models\Employee::class, $data);
		$advantage->resource->mainManager = Services\Managers::getBasic($data['ID_PROFIL_MANAGER']);
		$advantage->resource->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RH']);
		$advantage->resource->agency = Services\Agencies::getBasic($data['COMP_IDSOCIETE']);
		$advantage->resource->pole = Services\Poles::getBasic($data['ID_POLE']);

		$advantage->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		$advantage->project = self::createObject(Models\Project::class, $data);

		$advantage->delivery = self::createObject(Models\Delivery::class, $data);

		$advantage->advantageType = self::createObject(Models\AdvantageType::class, self::extractData($data, [
			'TYPEA_NAME',
			'AVG_TYPEAREF' => 'TYPEA_REF',
			'AVG_TYPE' => 'TYPEA_TYPE',
			'AVG_CATEGORIE' => 'TYPEA_CATEGORIE',
			'AVG_QUOTAPARTICIPATION' => 'TYPEA_QUOTAPARTICIPATION',
			'AVG_QUOTASOCIETE' => 'TYPEA_QUOTASOCIETE',
			'AVG_QUOTARESSOURCE' => 'TYPEA_QUOTARESSOURCE'
		]), $noId = true);

		return $advantage;
	}

	/**
	 * @param Models\Advantage $advantage
	 * @return array
	 */
	public static function toSQL($advantage)
	{

		$sqlData = self::modelToDatabaseArray($advantage);
		$sqlData['ID_PROFIL'] = $advantage->resource->id;
		$sqlData['ID_SOCIETE'] = $advantage->agency->id;
		if($advantage->project) $sqlData['ID_PROJET'] = $advantage->project->id;
		if($advantage->contract) $sqlData['ID_CONTRAT'] = $advantage->contract->id;

		$sqlData['AVG_TYPE']      = Tools::reverseMapData($advantage->advantageType->frequency, Models\AdvantageType::MAPPER_FREQUENCY);
		$sqlData['AVG_CATEGORIE'] = Tools::reverseMapData($advantage->advantageType->category, Models\AdvantageType::MAPPER_CATEGORY);

		if(!$advantage->id){
			if($advantage->project){
				// FIXME : gros doute, à checker
				switch ($advantage->advantageType->frequency){
					case Models\AdvantageType::FREQUENCY_PUNCTUAL:
						$sqlData['AVG_MONTANTRESSOURCE'] = $advantage->advantageType->employeeQuota;
						$sqlData['AVG_MONTANTSOCIETE'] = $advantage->advantageType->employeeQuota *($advantage->advantageType->agencyQuota - 1);
						break;
					case Models\AdvantageType::FREQUENCY_DAILY:
						$sqlData['AVG_MONTANTRESSOURCE'] = $advantage->advantageType->employeeQuota * $advantage->contract->monthlySalary / 100;
						$sqlData['AVG_MONTANTSOCIETE'] = $advantage->advantageType->agencyQuota * $advantage->contract->monthlySalary / 100;
						$sqlData['AVG_MONTANTPARTICIPATION'] = $advantage->advantageType->participationQuota * $advantage->contract->monthlySalary / 100;
						break;
					case Models\AdvantageType::FREQUENCY_MONTHLY:
						$sqlData['AVG_MONTANTRESSOURCE'] = $advantage->advantageType->employeeQuota;
						$sqlData['AVG_MONTANTSOCIETE'] = $advantage->advantageType->agencyQuota;
						$sqlData['AVG_MONTANTPARTICIPATION'] = $advantage->advantageType->participationQuota;
						break;
				}
			}
		}

		switch($advantage->advantageType->category ){
			case Models\AdvantageType::CATEGORY_FIXEDAMOUNT: //montant fixe
				$sqlData['AVG_MONTANTRESSOURCE'] = $advantage->advantageType->employeeQuota;
				$sqlData['AVG_MONTANTSOCIETE'] = $sqlData['AVG_MONTANTRESSOURCE']*($sqlData['AVG_QUOTASOCIETE']-1);
				break;
			case Models\AdvantageType::CATEGORY_PACKAGE: //Forfait
				$sqlData['AVG_MONTANTRESSOURCE']     = $advantage->advantageType->employeeQuota;
				$sqlData['AVG_MONTANTPARTICIPATION'] = $advantage->advantageType->participationQuota;
				$sqlData['AVG_MONTANTSOCIETE']       = $advantage->advantageType->agencyQuota;
				break;
		}

		$sqlData['AVG_COUT'] = $sqlData['AVG_QUANTITE']*($sqlData['AVG_MONTANTRESSOURCE'] + $sqlData['AVG_MONTANTSOCIETE'] - $sqlData['AVG_MONTANTPARTICIPATION']);

		return $sqlData;
	}
}
