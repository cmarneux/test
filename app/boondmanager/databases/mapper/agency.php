<?php
/**
 * agency.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Services\Agencies;
use Wish\Exceptions;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Services\BM;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class Agency
 * @package BoondManager\Databases\Mapper
 */
class Agency extends Mapper {
	/**
	 * Convert data from SQL to camelCase
	 * @param Model $data
	 * @param string $tab
	 * @return mixed
	 * @throws Exceptions\DatabaseIntegrity
	 */
	public static function fromSQL($data, $tab = BM::TAB_DEFAULT) {
		/** @var Models\Agency $agency */
		$agency = self::fromDefault($data);

		switch($tab) {
			case Models\Agency::TAB_INFORMATION:
				self::fromInformation($agency, $data);
				break;
			case Models\Agency::TAB_ACTIVITYEXPENSES:
				self::fromActivityExpenses($agency, $data);
				break;
			case Models\Agency::TAB_BILLING:
				self::fromBilling($agency, $data);
				break;
			case Models\Agency::TAB_OPPORTUNITIES:
				self::fromOpportunities($agency, $data);
				break;
			case Models\Agency::TAB_PROJECTS:
				self::fromProjects($agency, $data);
				break;
			case Models\Agency::TAB_PURCHASES:
				break;
			case Models\Agency::TAB_RESOURCES:
				self::fromEmployees($agency, $data);
				break;
		}
		$agency->calculateData();
		return $agency;
	}

	/**
	 * @param Model $data
	 * @return Models\Agency
	 */
	public static function fromDefault($data) {
		/** @var Models\Agency $agency */
		$agency = self::createObject(Models\Agency::class, $data);
		if($data->exists('SOCIETE_NIC') && $data->exists('SOCIETE_SIREN')) {
			$agency->registrationNumber = $data['SOCIETE_SIREN'] . $data['SOCIETE_NIC'];
		}
		return $agency;
	}

	/**
	 * @param Models\Agency $agency
	 * @param $data
	 */
	private static function fromInformation(Models\Agency $agency, $data){
	}

	/**
	 * @param Models\Agency $agency
	 * @param $data
	 */
	private static function fromActivityExpenses(Models\Agency $agency, $data){
		$imgLogo = Agencies::getLogoOnActivityExpensesPath($agency['ID_SOCIETE']);
		$agency->activityExpensesLogo = $imgLogo ? BM::getBoondManagerUrl().'/'.$imgLogo : '';

		$wut= [];
		foreach($data['CONFIGTPSFRS']['LISTE_TYPEHEURE'] as $row)
			$wut[] = WorkUnitType::fromSQL($row);
		$agency->workUnitTypes = $wut;

		$tf= [];
		foreach($data['CONFIGTPSFRS']['LISTE_TYPEFRAIS'] as $row)
			$tf[] = self::createObject(Models\ExpenseType::class, $row);
		$agency->expenseTypes = $tf;

		$baremes= [];
		foreach($data['CONFIGTPSFRS']['LISTE_BAREMEKM'] as $row)
			$baremes[] = self::createObject(Models\RatePerKilometerType::class, $row);
		$agency->ratePerKilometerTypes = $baremes;

		$decomptes= [];
		foreach($data['CONFIGTPSFRS']['LISTE_TYPEDECOMPTE'] as $row)
            $decomptes[] = AbsencesQuota::fromSQL($row);
		$agency->absencesQuotas = $decomptes;
	}

	/**
	 * @param Models\Agency $agency
	 * @param $data
	 */
	private static function fromBilling(Models\Agency $agency, $data){
		$imgLogo = Agencies::getLogoOnInvoicesPath($agency['ID_SOCIETE']);
		$agency->invoicesLogo = $imgLogo ? BM::getBoondManagerUrl().'/'.$imgLogo : '';

		if(is_null($data['GRPCONF_MAILFACTURATION'])) {
			$agency->invoicesMailSenderType = Models\Agency::MAILSENDERTYPE_CONTACT;
			$agency->invoicesMailSenderCustomized = '';
		} else if($data['GRPCONF_MAILFACTURATION'] == '') {
			$agency->invoicesMailSenderType = Models\Agency::MAILSENDERTYPE_MAINMANAGER;
			$agency->invoicesMailSenderCustomized = '';
		} else {
			$agency->invoicesMailSenderType = Models\Agency::MAILSENDERTYPE_CUSTOMIZED;
			$agency->invoicesMailSenderCustomized = $data['GRPCONF_MAILFACTURATION'];
		}

		$tabMask = explode(' ',$data['GRPCONF_MASKREFFACTURATION']);
		$agency->invoicesReferenceMask = sizeof($tabMask) == 2 ? $tabMask[0] : 'F';
		$agency->invoicesStartingNumber = sizeof($tabMask) == 2 ? $tabMask[1] : 1000000000000;

		$irt= [];
		foreach($data['CONFIGFACTURATION']['LISTE_TYPEFACTURATION'] as $row)
			$irt[] = InvoiceRecordType::fromSQL($row);
		$agency->invoiceRecordTypes = $irt;

		$bd= [];
		foreach($data['CONFIGFACTURATION']['LISTE_RIBS'] as $row)
			$bd[] = self::createObject(Models\BankDetails::class, $row);
		$agency->banksDetails = $bd;

		$agency->bankDetails = self::createObject(Models\BankDetails::class, ['ID_RIB' => $data['ID_RIB']]);

		$agency->factor = self::createObject(Models\Company::class, [
			'ID_CRMSOCIETE' => $data['ID_FACTOR'],
			'CSOC_SOCIETE' => $data['FACTOR_SOCIETE']
		]);
	}

	/**
	 * @param Models\Agency $agency
	 * @param $data
	 */
	private static function fromOpportunities(Models\Agency $agency, $data){
		list($agency->opportunitiesTechnicalAssistanceReferenceMask,
			$agency->opportunitiesPackageReferenceMask,
			$agency->opportunitiesRecruitmentReferenceMask,
			$agency->opportunitiesProductReferenceMask) = Tools::unserializeArray($data['GRPCONF_MASKREFAO']);

		$tabMask = explode(' ',$data['GRPCONF_MASKREFDEVIS']);
		$agency->quotationsReferenceMask = sizeof($tabMask) == 2 ? $tabMask[0] : 'Q';
		$agency->quotationsStartingNumber = sizeof($tabMask) == 2 ? $tabMask[1] : 1000000000000;
	}

	/**
	 * @param Models\Agency $agency
	 * @param $data
	 */
	private static function fromProjects(Models\Agency $agency, $data){
		list($agency->projectsTechnicalAssistanceReferenceMask,
			$agency->projectsPackageReferenceMask,
			$agency->projectsRecruitmentReferenceMask,
			$agency->projectsProductReferenceMask) = Tools::unserializeArray($data['GRPCONF_MASKREFPROJET']);
	}

	private static function fromEmployees(Models\Agency $agency, $data)
	{
		$advantages = [];
		foreach ($data['CONFIGRESSOURCES']['LISTE_TYPEAVANTAGE'] as $row)
			$advantages[] = self::createObject(Models\AdvantageType::class, $row);
		$agency->advantageTypes = $advantages;

		foreach ($agency->exceptionalScaleTypes as $est) {
			$rules = [];
			if($data['CONFIGRESSOURCES']['LISTE_REGLEEXCEPTION']) foreach($data['CONFIGRESSOURCES']['LISTE_REGLEEXCEPTION'] as $row){
				if($est->reference == $row['REGLE_BAREMEREF'])
					$rules[] = self::createObject(Models\ExceptionalRule::class, $row);
			}
			$est->exceptionalRuleTypes = $rules;
		}
	}

	/**
	 * Convert entity's data from camelCase to SQL
	 * @param Models\Agency $agency
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL(Models\Agency $agency, $tab = BM::TAB_DEFAULT) {
	    $tabModelToDatabase = self::modelToDatabaseArray($agency);
        $data = [
            'SOCIETE' => $tabModelToDatabase,
            'GROUPECONFIG' => $tabModelToDatabase
        ];

		switch($tab) {
			case Models\Agency::TAB_INFORMATION:
				self::toInformation($agency, $data);
				break;
			case Models\Agency::TAB_BILLING:
				self::toBilling($agency, $data);
				break;
			case Models\Agency::TAB_OPPORTUNITIES:
				self::toOpportunities($agency, $data);
				break;
			case Models\Agency::TAB_PROJECTS:
				self::toProjects($agency, $data);
				break;
            case Models\Agency::TAB_ACTIVITYEXPENSES:
                self::toActivityExpenses($agency, $data);
                break;
            case Models\Agency::TAB_RESOURCES:
                self::toEmployees($agency, $data);
                break;
		}

		return $data;
	}

	/**
	 * @param Models\Agency $agency
	 * @param array $data
	 */
	private static function toInformation(Models\Agency $agency, &$data) {
		$data['SOCIETE']['SOCIETE_SIREN'] = substr($agency->registrationNumber,0,-5);
		$data['SOCIETE']['SOCIETE_NIC'] = substr($agency->registrationNumber,-5);
	}

    /**
     * @param Models\Agency $agency
     * @param $data
     */
    private static function toActivityExpenses(Models\Agency $agency, &$data){
        $data['CONFIGTPSFRS']['LISTE_TYPEHEURE'] = [];
        foreach($agency->workUnitTypes as $wut)
            $data['CONFIGTPSFRS']['LISTE_TYPEHEURE'][] = WorkUnitType::toSQL($wut);

        $data['CONFIGTPSFRS']['LISTE_TYPEFRAIS'] = [];
        foreach($agency->expenseTypes as $et)
            $data['CONFIGTPSFRS']['LISTE_TYPEFRAIS'][] = Mapper::modelToDatabaseArray($et);

        $data['CONFIGTPSFRS']['LISTE_BAREMEKM'] = [];
        foreach($agency->ratePerKilometerTypes as $rpkt)
            $data['CONFIGTPSFRS']['LISTE_BAREMEKM'][] = Mapper::modelToDatabaseArray($rpkt);

        $data['CONFIGTPSFRS']['LISTE_TYPEDECOMPTE'] = [];
        foreach($agency->absencesQuotas as $aq)
            $data['CONFIGTPSFRS']['LISTE_TYPEDECOMPTE'][] = AbsencesQuota::toSQL($aq);
    }

	/**
	 * @param Models\Agency $agency
	 * @param array $data
	 */
	private static function toBilling(Models\Agency $agency, &$data) {
		$data['GROUPECONFIG']['GRPCONF_MASKREFFACTURATION'] = $agency->invoicesReferenceMask.' '.$agency->invoicesStartingNumber;

        $data['GROUPECONFIG']['GRPCONF_BDCIDRIB'] = is_null($agency->bankDetails) ? 0 : $agency->bankDetails->id;
        $data['GROUPECONFIG']['GRPCONF_BDCIDCRMSOCIETE'] = is_null($agency->factor) ? 0 : $agency->factor->id;

		switch($agency->invoicesMailSenderType) {
            case Models\Agency::MAILSENDERTYPE_CONTACT:
                $data['GROUPECONFIG']['GRPCONF_MAILFACTURATION'] = null;
                break;
            case Models\Agency::MAILSENDERTYPE_MAINMANAGER:
                $data['GROUPECONFIG']['GRPCONF_MAILFACTURATION'] = '';
                break;
            case Models\Agency::MAILSENDERTYPE_CUSTOMIZED:
                $data['GROUPECONFIG']['GRPCONF_MAILFACTURATION'] = $agency->invoicesMailSenderCustomized;
                break;
        }

        $data['CONFIGFACTURATION']['LISTE_TYPEFACTURATION'] = [];
        foreach($agency->invoiceRecordTypes as $irt)
            $data['CONFIGFACTURATION']['LISTE_TYPEFACTURATION'][] = InvoiceRecordType::toSQL($irt, $agency);

        $data['CONFIGFACTURATION']['LISTE_RIBS'] = [];
        foreach($agency->banksDetails as $bd)
            $data['CONFIGFACTURATION']['LISTE_RIBS'][] = Mapper::modelToDatabaseArray($bd);
	}

    /**
     * @param Models\Agency $agency
     * @param $data
     */
    private static function toEmployees(Models\Agency $agency, &$data){
        $data['CONFIGRESSOURCES']['LISTE_TYPEAVANTAGE'] = [];
        foreach($agency->advantageTypes as $at)
            $data['CONFIGRESSOURCES']['LISTE_TYPEAVANTAGE'][] = Mapper::modelToDatabaseArray($at);

        $data['CONFIGRESSOURCES']['LISTE_REGLEEXCEPTION'] = [];
        foreach($agency->exceptionalScaleTypes as $est)
            foreach($est->exceptionalRuleTypes as $er) {
                $tmpData = Mapper::modelToDatabaseArray($er);
                $tmpData['REGLE_BAREMEREF'] = $est->reference;
                $data['CONFIGRESSOURCES']['LISTE_REGLEEXCEPTION'][] = $tmpData;
            }
    }

	/**
	 * @param Models\Agency $agency
	 * @param array $data
	 */
	private static function toOpportunities(Models\Agency $agency, &$data) {
		$data['GROUPECONFIG']['GRPCONF_MASKREFAO'] = Tools::serializeArray([
			$agency->opportunitiesTechnicalAssistanceReferenceMask,
			$agency->opportunitiesPackageReferenceMask,
			$agency->opportunitiesRecruitmentReferenceMask,
			$agency->opportunitiesProductReferenceMask
		]);

		$data['GROUPECONFIG']['GRPCONF_MASKREFDEVIS'] = $agency->quotationsReferenceMask.' '.$agency->quotationsStartingNumber;
	}

	/**
	 * @param Models\Agency $agency
	 * @param array $data
	 */
	private static function toProjects(Models\Agency $agency, &$data) {
		$data['GROUPECONFIG']['GRPCONF_MASKREFPROJET'] = Tools::serializeArray([
			$agency->projectsTechnicalAssistanceReferenceMask,
			$agency->projectsPackageReferenceMask,
			$agency->projectsRecruitmentReferenceMask,
			$agency->projectsProductReferenceMask
		]);
	}
}
