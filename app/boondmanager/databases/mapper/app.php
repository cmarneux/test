<?php
/**
 * app.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Exceptions;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class App
 * @package BoondManager\Databases\Mapper
 */
class App extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\App
	 */
	public static function fromRow($data) {
		$app = self::createObject(Models\App::class, $data);
		return $app;
	}

	/**
	 * Convert entity's data from SQL to camelCase
	 * @param Model $data
	 * @return mixed
	 * @throws Exceptions\DatabaseIntegrity
	 */
	public static function fromSQL($data) {
		$app = self::createObject(Models\App::class, $data);

		if(isset($data['MAINAPI_VERSION'])) $app->currentAPI = intval($data['MAINAPI_VERSION']) < 1 ? false : true;
		if(isset($data['MAINAPI_HOSTSALLOWED'])) $app->hostsAllowed = Tools::unserializeArray($data['MAINAPI_HOSTSALLOWED']);
		if(isset($data['MAINAPI_APIALLOWED'])) $app->apiAllowed = Tools::unserializeArray($data['MAINAPI_APIALLOWED']);

		return $app;
	}
}
