<?php
/**
 * attachedflag.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Services;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class AttachedFlag
 * @package BoondManager\Databases\Mapper
 */
class AttachedFlag extends Mapper {
	/**
	 * Convert data from SQL to camelCase
	 * @param Model $data
	 * @return Models\AttachedFlag
	 */
	public static function fromSQL($data) {
		$attachedFlag = self::createObject(Models\AttachedFlag::class, $data);

		$attachedFlag->flag = self::createObject(Models\Flag::class, $data);
		$attachedFlag->flag->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		switch($data['FLAG_TYPE']) {
			case Models\AttachedFlag::TYPE_COMPANY: $attachedFlag->dependsOn = self::createObject(Models\Company::class, self::extractData($data, ['ID_PARENT' => 'ID_CRMSOCIETE']));break;
			case Models\AttachedFlag::TYPE_OPPORTUNITY: $attachedFlag->dependsOn = self::createObject(Models\Opportunity::class, self::extractData($data, ['ID_PARENT' => 'ID_AO']));break;
			case Models\AttachedFlag::TYPE_PROJECT: $attachedFlag->dependsOn = self::createObject(Models\Project::class, self::extractData($data, ['ID_PARENT' => 'ID_PROJET']));break;
			case Models\AttachedFlag::TYPE_ORDER: $attachedFlag->dependsOn = self::createObject(Models\Order::class, self::extractData($data, ['ID_PARENT' => 'ID_BONDECOMMANDE']));break;
			case Models\AttachedFlag::TYPE_PRODUCT: $attachedFlag->dependsOn = self::createObject(Models\Product::class, self::extractData($data, ['ID_PARENT' => 'ID_PRODUIT']));break;
			case Models\AttachedFlag::TYPE_PURCHASE: $attachedFlag->dependsOn = self::createObject(Models\Purchase::class, self::extractData($data, ['ID_PARENT' => 'ID_ACHAT']));break;
			case Models\AttachedFlag::TYPE_ACTION: $attachedFlag->dependsOn = self::createObject(Models\Action::class, self::extractData($data, ['ID_PARENT' => 'ID_ACTION']));break;
			case Models\AttachedFlag::TYPE_RESOURCE: $attachedFlag->dependsOn = self::createObject(Models\Employee::class, self::extractData($data, ['ID_PARENT' => 'ID_PROFIL']));break;
			case Models\AttachedFlag::TYPE_CANDIDATE: $attachedFlag->dependsOn = self::createObject(Models\Candidate::class, self::extractData($data, ['ID_PARENT' => 'ID_PROFIL']));break;
			case Models\AttachedFlag::TYPE_POSITIONING: $attachedFlag->dependsOn = self::createObject(Models\Positioning::class, self::extractData($data, ['ID_PARENT' => 'ID_POSITIONNEMENT']));break;
			case Models\AttachedFlag::TYPE_CONTACT: $attachedFlag->dependsOn = self::createObject(Models\Contact::class, self::extractData($data, ['ID_PARENT' => 'ID_CRMCONTACT']));break;break;
			case Models\AttachedFlag::TYPE_INVOICE: $attachedFlag->dependsOn = self::createObject(Models\Invoice::class, self::extractData($data, ['ID_PARENT' => 'ID_FACTURATION']));break;break;
		}

		return $attachedFlag;
	}

	/**
	 * Convert entity's data from camelCase to SQL
	 * @param Models\AttachedFlag $attachedFlag
	 * @return array
	 */
	public static function toSQL(Models\AttachedFlag $attachedFlag) {
		$data = Tools::reversePublicFieldsToPrivate($attachedFlag->getPublicFieldsMapping(), $attachedFlag->toArray());

		$data['ID_USERFLAG'] = $attachedFlag->flag->id;

		$data['ID_PARENT'] = $attachedFlag->dependsOn->id;
		switch(get_class($attachedFlag->dependsOn)) {
			case Models\Company::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_COMPANY;break;
			case Models\Opportunity::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_OPPORTUNITY;break;
			case Models\Project::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_PROJECT;break;
			case Models\Order::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_ORDER;break;
			case Models\Product::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_PRODUCT;break;
			case Models\Purchase::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_PURCHASE;break;
			case Models\Action::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_ACTION;break;
			case Models\Employee::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_RESOURCE;break;
			case Models\Candidate::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_CANDIDATE;break;
			case Models\Positioning::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_POSITIONING;break;
			case Models\Contact::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_CONTACT;break;
			case Models\Invoice::class: $data['FLAG_TYPE'] = Models\AttachedFlag::TYPE_INVOICE;break;
		}
		return $data;
	}
}
