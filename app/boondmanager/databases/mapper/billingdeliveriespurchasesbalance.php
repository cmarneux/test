<?php
/**
 * billingdeliveriespurchasesbalance.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;

/**
 * Class BillingDeliveriesPurchasesBalance
 * @package BoondManager\Databases\Mapper
 */
class BillingDeliveriesPurchasesBalance extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Delivery|Models\Purchase
	 */
	public static function fromRow($data){
		if($data['DLVPRC_TYPE'] == 0){
			/** @var Models\Delivery $entity */
			$entity = self::createObject(Models\Delivery::class, self::extractData($data, [
				'ID_ITEM'       => 'ID_MISSIONPROJET',
				'ITEM_DEBUT'    => 'MP_DEBUT',
				'ITEM_FIN'      => 'MP_FIN',
				'ITEM_INTITULE' => 'MP_NOM',
				'NB_BDC',
				'NB_COR'
			]));

			if($data['COMP_IDPROFIL']){
				$entity->dependsOn = self::createObject(Models\Employee::class, self::extractData($data, [
					'COMP_IDPROFIL' => 'ID_PROFIL',
					'COMP_NOM'      => 'PROFIL_NOM',
					'COMP_PRENOM'   => 'PROFIL_PRENOM'
				]));
			}else if($data['ID_PRODUIT']){
				$entity->dependsOn = self::createObject(Models\Product::class, self::extractData($data, [
					'ID_PRODUIT',
					'ITEM_TITRE' => 'PRODUIT_NOM'
				]));
			}
		}else{ // 1
			/** @var Models\Purchase $entity */
			$entity = self::createObject(Models\Purchase::class, self::extractData($data, [
				'ID_ITEM'    => 'ID_ACHAT',
				'ITEM_DEBUT' => 'ACHAT_DEBUT',
				'ITEM_FIN'   => 'ACHAT_FIN',
				'ITEM_TITRE' => 'ACHAT_TITLE',
				'NB_BDC',
				'NB_COR',
			]));
		}

		$entity->project = self::createObject(Models\Project::class, self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'PRJ_TYPEREF',
			'PRJ_TYPE',
			'PRJ_DEVISE',
			'PRJ_CHANGE',
			'PRJ_DEVISEAGENCE',
			'PRJ_CHANGEAGENCE',
		]));

		$entity->project->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
			'ID_PROFIL',
			'PROFIL_NOM',
			'PROFIL_PRENOM',
		]));
		$entity->project->opportunity = self::createObject(Models\Opportunity::class, self::extractData($data, [
			'ID_AO',
			'AO_TITLE',
		]));
		$entity->project->contact = self::createObject(Models\Contact::class, self::extractData($data, [
			'ID_CRMCONTACT',
			'CCON_PRENOM',
			'CCON_NOM',
		]));
		$entity->project->company = self::createObject(Models\Company::class, self::extractData($data, [
			'ID_CRMSOCIETE',
			'CSOC_SOCIETE',
		]));

		$entity->order = self::createObject(Models\Order::class, self::extractData($data, [
			'ID_BONDECOMMANDE',
			'BDC_REF',
			'BDC_REFCLIENT'
		]));

		return $entity;
	}
}
