<?php
/**
 * billingprojectsbalance.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;

/**
 * Class BillingProjectsBalance
 * @package BoondManager\Databases\Mapper
 */
class BillingProjectsBalance extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Order
	 */
	public static function fromRow($data){
		$entity = self::createObject(Models\Project::class, $data);

		if($entity) {
			if(!$entity->turnoverOrderedExcludingTax) $entity->turnoverOrderedExcludingTax = 0;
			$entity->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));
			$entity->opportunity = self::createObject(Models\Opportunity::class, self::extractData($data, [
				'ID_AO',
				'AO_TITLE',
			]));
			$entity->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT',
				'CCON_PRENOM',
				'CCON_NOM',
			]));
			$entity->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE',
				'CSOC_SOCIETE',
			]));
		}

		return $entity;
	}
}
