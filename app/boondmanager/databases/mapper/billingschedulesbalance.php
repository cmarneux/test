<?php
/**
 * billingschedulesbalance.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;

/**
 * Class BillingSchedulesBalance
 * @package BoondManager\Databases\Mapper
 */
class BillingSchedulesBalance extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Order
	 */
	public static function fromRow($data){
		$entity = self::createObject(Models\Schedule::class, $data);

		if($entity) {
			if(!$entity->turnoverInvoicedExcludingTax) $entity->turnoverInvoicedExcludingTax = 0;
			$entity->order = self::createObject(Models\Order::class, self::extractData($data, [
				'ID_BONDECOMMANDE',
				'BDC_REF',
				'BDC_REFCLIENT',
				'NB_FACTURE',
			]));
			$entity->order->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));
			$entity->order->project = self::createObject(Models\Project::class, self::extractData($data, [
				'ID_PROJET',
				'PRJ_REFERENCE',
				'PRJ_TYPEREF',
				'PRJ_TYPE',
				'PRJ_DEVISE',
				'PRJ_CHANGE',
				'PRJ_DEVISEAGENCE',
				'PRJ_CHANGEAGENCE',
			]));
			$entity->order->project->opportunity = self::createObject(Models\Opportunity::class, self::extractData($data, [
				'ID_AO',
				'AO_TITLE',
			]));
			$entity->order->project->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT',
				'CCON_PRENOM',
				'CCON_NOM',
			]));
			$entity->order->project->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE',
				'CSOC_SOCIETE',
			]));
		}

		return $entity;
	}
}
