<?php
/**
 * allGroupBUs.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Tools;

/**
 * Class BusinessUnit
 * @package BoondManager\Databases\Mapper
 */
class BusinessUnit extends Mapper{

	/**
	 * @param array|\ArrayAccess $object
	 * @return Models\BusinessUnit
	 */
	public static function fromSQL($object)
	{
		$bu = self::createObject(Models\BusinessUnit::class, $object);

		$excludedManagersInSearch = [];
		$managers        = [];

		if($object['MANAGERS']) {
			foreach ($object['MANAGERS'] as $row) {
				if (isset($row['ID_PROFIL'])) {
					$manager = Services\Managers::getBasic($row['ID_PROFIL']);

					$managers[] = $manager;
					if ($row['INBU_EXCLUDE']) $excludedManagersInSearch[] = $manager;
				}
			}
		}

		$bu->excludedManagersInSearch = $excludedManagersInSearch;
		$bu->managers        = $managers;
		return $bu;
	}

	/**
	 * Convert entity's data from camelCase to SQL
	 * @param Models\BusinessUnit $bu
	 * @return array
	 */
	public static function toSQL(Models\BusinessUnit $bu) {
		$data = ['BUSINESSUNIT' => Tools::reversePublicFieldsToPrivate($bu->getPublicFieldsMapping(), $bu->toArray())];

		$data['MANAGERS'] = [];
		foreach($bu->managers as $manager) {
			$data['MANAGERS'][] = [
				'ID_USER' => Services\Managers::getUserIdFromEmployeeId($manager->id),
				'ID_BUSINESSUNIT' => $bu->id,
				'INBU_EXCLUDE' => false
			];
		}

		foreach($bu->excludedManagersInSearch as $excludeManager) {
			foreach($data['MANAGERS'] as $i => $manager) {
				if($manager['ID_USER'] == Services\Managers::getUserIdFromEmployeeId($excludeManager->id)) {
					$data['MANAGERS'][$i]['INBU_EXCLUDE'] = true;
				}
			}
		}

		return $data;
	}
}
