<?php
/**
 * candidate.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;
use BoondManager\Models;
use BoondManager\Services;

/**
 * Class Candidate
 * @package BoondManager\Databases\Mapper
 */
class Candidate extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Candidate
	 */
	public static function fromRow($data){
		$contextId = Tools::getContext()->startTiming('candidate mapping', $group = true);

		/** @var Models\Candidate $candidate */
		$candidate = self::createObject(Models\Candidate::class, $data);

		$candidate->availability = ($data['PARAM_TYPEDISPO'] == 9) ? $data['PARAM_DATEDISPO'] : intval($data['PARAM_TYPEDISPO']);

		if($data['RESP_ID']) $candidate->mainManager = Services\Managers::getBasic($data['RESP_ID']);
		if($data['RESPRH_ID']) $candidate->hrManager = Services\Managers::getBasic($data['RESPRH_ID']);

		if($data['ID_CV'])
			$candidate->resume = self::createObject(Models\Resume::class, $data);

		Tools::getContext()->endTiming($contextId);

		return $candidate;
	}

	/**
	 * @param Model|array $data
	 * @return Models\Candidate
	 */
	public static function fromBasic($data) {
		$candidate = self::createObject(Models\Candidate::class, self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'
		]));

		return $candidate ? $candidate : null;
	}

	public static function fromSQL($data, $tab = Models\Candidate::TAB_DEFAULT){
		$candidate = self::createObject(Models\Candidate::class, $data);

		if($data['ID_PROFIL_RESPMANAGER']) $candidate->mainManager = Services\Managers::getBasic($data['ID_PROFIL_RESPMANAGER']);
		if($data['ID_PROFIL_RESPRH']) $candidate->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RESPRH']);
		if($data['ID_SOCIETE']) $candidate->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		if($data['ID_POLE']) $candidate->pole = Services\Poles::getBasic($data['ID_POLE']);

		switch ($tab){
			case Models\Candidate::TAB_INFORMATION:
				self::fromInformation($candidate, $data);
				break;
			case Models\Candidate::TAB_ADMINISTRATIVE:
				self::fromAdministrative($candidate, $data);
				break;
			case Models\Candidate::TAB_TD:
				self::fromTechnicalData($candidate, $data);
				break;
		}

		return $candidate;
	}

	public static function fromInformation(Models\Candidate $candidate, Model $data) {

		$candidate->availability = ($data['PARAM_TYPEDISPO'] == 9) ? $data['PARAM_DATEDISPO'] : intval($data['PARAM_TYPEDISPO']);

		$cvs = [];
		if($data['CVs']) foreach($data['CVs'] as $cv){
			$cvs[] = self::createObject(Models\Resume::class, $cv);
		}
		$candidate->resumes = $cvs;

		$networks = [];
		if($data['WEBSOCIALS']) foreach($data['WEBSOCIALS'] as $net){
			$networks[] = self::createObject(Models\WebSocial::class, $net);
		}
		$candidate->socialNetworks = $networks;

		$notations = [];
		if($data['NOTATIONS']) foreach($data['NOTATIONS'] as $not){
			$notations[] = $n = self::createObject(Models\Notation::class, $not);
			$manager = Services\Managers::getBasic($not['ID_PROFIL']);
			$n->lastName = $manager->lastName;
			$n->firstName = $manager->firstName;
		}
		$candidate->evaluations = $notations;

		$candidate->source = new Models\OriginOrSource();
		$candidate->source->detail = $data['PARAM_SOURCE'];
		$candidate->source->typeOf = $data['PARAM_TYPESOURCE'];
	}

	public static function fromAdministrative(Models\Candidate $candidate, Model $data) {

		if($data['ID_PROFILCONSULTANT']) {
			$candidate->resource = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFILCONSULTANT' => 'ID_PROFIL',
			]));
		}

		if($data['ID_CRMCONTACT']) {
			$candidate->providerContact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',
			]));
		}

		if($data['ID_CRMSOCIETE']) {
			$candidate->providerCompany = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT', 'CSOC_SOCIETE',
			]));
		}

		$files = [];
		if($data['FILES']) {
			foreach ($data['FILES'] as $fData){
				/** @var Models\Contract $c */
				$files[] = $f = self::createObject(Models\File::class, $fData);
			}
		}
		$candidate->files = $files;

		if($data['ID_CONTRAT']) {
			$candidate->contract = self::createObject(Models\Contract::class, $data);
		}

		$candidate->desiredSalary = new Models\DesiredSalary($data['PARAM_TARIF2'], $data['PARAM_TARIF3']);

	}

	public static function fromTechnicalData(Models\Candidate $candidate, Model $data)
	{
		$refs = [];
		foreach($data['REFERENCES'] as $row){
			$refs[] = self::createObject(Models\Reference::class, $row);
		}
		$candidate->references = $refs;
	}

	/**
	 * @param Models\Candidate $profil
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL($profil, $tab)
	{
		$data = [];
		$data['PROFIL'] = self::modelToDatabaseArray($profil);

		switch($tab) {
			case Models\Candidate::TAB_INFORMATION:
				self::toInformation($profil, $data);
				break;
			case Models\Candidate::TAB_ADMINISTRATIVE:
				self::toAdministrative($profil, $data);
				break;
			case Models\Candidate::TAB_TD:
				self::toTD($profil, $data);
				break;
		}

		return $data;
	}

	/**
	 * @param Models\Candidate $profil
	 * @param array $data
	 */
	private static function toInformation(Models\Candidate $profil, &$data) {

		if($profil->exists('socialNetworks')) {
			$data['WEBSOCIALS'] = [];
			foreach($profil->socialNetworks as $network) {
				$data['WEBSOCIALS'][] = self::modelToDatabaseArray($network);
			}
		}

		if($profil->exists('evaluations')) {
			$data['NOTATIONS'] = [];
			foreach($profil->evaluations as $network) {
				$notation = self::modelToDatabaseArray($network);
				$notation['ID_RESPUSER'] = Services\CurrentUser::instance()->getUserId();
				$data['NOTATIONS'][] = $notation;
			}
		}

		if($profil->source) {
			$data['PROFIL']['PARAM_TYPESOURCE'] = $profil->source->typeOf;
			$data['PROFIL']['PARAM_SOURCE']     = $profil->source->detail;
		}

		if($profil->mainManager)
			$data['PROFIL']['ID_RESPMANAGER'] = Services\Managers::getUserIdFromEmployeeId($profil->mainManager->id);

		if($profil->hrManager)
			$data['PROFIL']['ID_RESPRH'] = Services\Managers::getUserIdFromEmployeeId($profil->hrManager->id);

		if($profil->pole)
			$data['PROFIL']['ID_POLE'] = $profil->pole->id;

		if($profil->agency)
			$data['PROFIL']['ID_SOCIETE'] = $profil->agency->id;

	}

	/**
	 * @param Models\Candidate $profil
	 * @param array $data
	 */
	private static function toAdministrative(Models\Candidate $profil, &$data) {

		if($profil->resource)
			$data['PROFIL']['ID_PROFILCONSULTANT'] = $profil->resource->id;

		if($profil->providerCompany)
			$data['PROFIL']['ID_CRMSOCIETE'] = $profil->providerCompany->id;

		if($profil->providerContact)
			$data['PROFIL']['ID_CRMCONTACT'] = $profil->providerContact->id;

		if($profil->contract)
			$data['PROFIL']['ID_CONTRAT'] = $profil->contract->id;

		$data['PROFIL']['PARAM_TARIF2'] = $profil->desiredSalary->min;
		$data['PROFIL']['PARAM_TARIF3'] = $profil->desiredSalary->max;
	}

	/**
	 * @param Models\Candidate $profil
	 * @param array $data
	 */
	private static function toTD(Models\Candidate $profil, &$data) {

		if($profil->exists('references')) {
			$data['REFERENCES'] = [];
			foreach($profil->references as $ref) {
				$data['REFERENCES'][] = self::modelToDatabaseArray($ref);;
			}
		}

		$data['DT'] = self::extractData($data['PROFIL'], [
			'DT_LANGUES', 'DT_OUTILS', 'DT_DIPLOMES', 'DT_EXPERIENCE', 'DT_FORMATION', 'DT_TITRE', 'COMP_APPLICATIONS', 'COMP_INTERVENTIONS', 'COMP_COMPETENCE'
		]);
	}
}
