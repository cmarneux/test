<?php
/**
 * company.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Company
 * @package BoondManager\Databases\Mapper
 */
class Company extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Company
	 */
	public static function fromRow($data){
		$contextId = Tools::getContext()->startTiming('company mapping', $group = true);

		/** @var Models\Company $company */
		$company = self::createObject(Models\Company::class, $data);

		if($data['ID_PROFIL']) $company->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		Tools::getContext()->endTiming($contextId);

		return $company;
	}

	/**
	 * @param Model $data
	 * @param string|int $tab
	 * @return Models\Company|mixed|Model
	 */
	public static function fromSQL($data, $tab = Models\Company::TAB_DEFAULT){
		/** @var Models\Company $company */
		$company = self::createObject(Models\Company::class, $data);

		$company->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);
		$company->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		$company->pole = Services\Poles::getBasic($data['ID_POLE']);

		switch ($tab){
			case Models\Company::TAB_INFORMATION:
				self::fromInformation($company, $data);
				break;
		}

		return $company;
	}

	private static function fromInformation(Models\Company $company, $data){
		if($data['ID_RESPSOC']) $company->parentCompany = self::createObject(Models\Company::class, self::extractData($data, [
			'ID_RESPSOC'   => 'ID_CRMSOCIETE',
			'RESP_SOCIETE' => 'CSOC_SOCIETE'
		]));

		$company->origin = self::createObject(Models\OriginOrSource::class, self::extractData($data, [
			'CSOC_TYPESOURCE' => 'TYPE_SOURCE',
			'CSOC_SOURCE' => 'SOURCE'
		]));

		$influencers = [];
		foreach ($data['INFLUENCERS'] as $row){
			/** @var Models\Employee $i */
			$influencers[] = $i = self::createObject(Models\Account::class, $row);
		}
		$company->influencers = $influencers;

		$documents = [];
		foreach($data['DOCUMENTS'] as $row){
			$documents[] = self::createObject(Models\Document::class, $row);
		}
		$company->files = $documents;

		$filiales = [];
		foreach($data['SOCIETEFILLES'] as $row){
			$filiales[] = self::createObject(Models\Company::class, $row);
		}
		$company->subsidiaries = $filiales;

		$contacts = [];
		foreach($data['FACT_COORDONNEES'] as $row){
			$contacts[] = self::createObject(Models\ContactDetails::class, $row);
		}
		$company->billingDetails = $contacts;

		$networks = [];
		foreach($data['WEBSOCIALS'] as $row){
			$networks[] = self::createObject(Models\WebSocial::class, $row);
		}
		$company->socialNetworks = $networks;
	}

	/**
	 * @param Models\Company $entity
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL(Models\Company $entity) {

		$data = [];
		$data['SOCIETE'] = self::modelToDatabaseArray($entity);

		if($entity->exists('socialNetworks')) {
			$data['WEBSOCIALS'] = [];
			foreach($entity->socialNetworks as $network) {
				$data['WEBSOCIALS'][] = self::modelToDatabaseArray($network);
			}
		}

		if($entity->origin) {
			$data['SOCIETE']['CSOC_TYPESOURCE'] = $entity->origin->typeOf;
			$data['SOCIETE']['CSOC_SOURCE']     = $entity->origin->detail;
		}

		if($entity->mainManager)
			$data['SOCIETE']['ID_PROFIL'] = $entity->mainManager->id;

		if($entity->agency)
			$data['SOCIETE']['ID_SOCIETE'] = $entity->agency->id;

		$data['SOCIETE']['ID_POLE'] = $entity->pole ? $entity->pole->id : 0;

		if($entity->exists('billingDetails')) {
			$data['FACT_COORDONNEES'] = [];
			foreach($entity->billingDetails as $e) {
				$data['FACT_COORDONNEES'][] = self::modelToDatabaseArray($e);
			}
		}

		if($entity->exists('influencers')) {
			$data['INFLUENCERS'] = [];
			foreach($entity->influencers as $inf) {
				$data['INFLUENCERS'][] = $inf->id;
			}
		}

		return $data;
	}
}
