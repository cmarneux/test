<?php
/**
 * contact.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Contact
 * @package BoondManager\Databases\Mapper
 */
class Contact extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Contact
	 */
	public static function fromRow($data){
		$contextId = Tools::getContext()->startTiming('contact mapping', $group = true);

		$data = $data->toArray();

		/** @var Models\Contact $contact */
		$contact = self::createObject(Models\Contact::class, $data);

		if($data['ID_PROFIL'])
			$contact->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		if($data['ID_POLE'])
			$contact->pole = Services\Poles::getBasic($data['ID_POLE']);

		if($data['ID_CRMSOCIETE'])
			$contact->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_INTERVENTION', 'CSOC_METIERS', 'CSOC_ADR', 'CSOC_CP', 'CSOC_TEL',
				'CSOC_VILLE', 'CSOC_PAYS', 'CSOC_BAREMESEXCEPTION'
		]));
		if($data['ID_ACTION']) $contact->lastAction = self::createObject(Models\Action::class, $data);

		Tools::getContext()->endTiming($contextId);

		return $contact;
	}

	/**
	 * @param Model $data
	 * @param string $tab
	 * @return mixed
	 */
	public static function fromSQL($data, $tab = Models\Contact::TAB_DEFAULT){
		/** @var Models\Contact $contact */
		$contact = self::createObject(Models\Contact::class, $data);

		$contact->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);
		$contact->pole = Services\Poles::getBasic($data['ID_POLE']);
		$contact->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		$contact->company = self::createObject(Models\Company::class, $data);

		switch ($tab){
			case Models\Contact::TAB_INFORMATION:
				self::fromInformation($contact, $data);
				break;
			case Models\Contact::TAB_ACTIONS:
				self::fromActions($contact, $data);
				break;
		}

		return $contact;
	}

	private static function fromInformation(Models\Contact $contact, $data){
		if($data['ID_RESPSOC']) $contact->company->parentCompany = self::createObject(Models\Company::class, self::extractData($data, [
			'ID_RESPSOC' => 'ID_CRMSOCIETE',
			'RESP_SOCIETE' => 'CSOC_SOCIETE'
		]));

		$contact->origin = self::createObject(Models\OriginOrSource::class, self::extractData($data, [
			'CCON_TYPESOURCE' => 'TYPE_SOURCE',
			'CCON_SOURCE' => 'SOURCE'
		]));

		$influencers = [];

		foreach($data['INFLUENCERS'] as $row){
			$influencers[] = self::createObject(Models\Account::class, $row);
		}
		$contact->influencers = $influencers;

		$networks = [];
		foreach($data['WEBSOCIALS'] as $row){
			$networks[] = self::createObject(Models\WebSocial::class, $row);
		}
		$contact->socialNetworks = $networks;

	}

	private static function fromActions(Models\Contact $contact, $data){
		$influencers = [];
		foreach($data['INFLUENCERS'] as $row){
			$influencers[] = self::createObject(Models\Manager::class, $row);
		}
		$contact->influencers = $influencers;
	}

	public static function toSQL(Models\Contact $entity) {

		$data = [];
		$data['CONTACT'] = self::modelToDatabaseArray($entity);

		if($entity->origin) {
			$data['CONTACT']['CCON_TYPESOURCE'] = $entity->origin->typeOf;
			$data['CONTACT']['CCON_SOURCE'] = $entity->origin->detail;
		}

		if($entity->exists('socialNetworks')) {
			$data['WEBSOCIALS'] = [];
			foreach($entity->socialNetworks as $network) {
				$data['WEBSOCIALS'][] = self::modelToDatabaseArray($network);
			}
		}

		if($entity->exists('influencers')) {
			$data['INFLUENCERS'] = [];
			foreach($entity->influencers as $inf) {
				$data['INFLUENCERS'][] = $inf->id;
			}
		}

		if($entity->mainManager)
			$data['CONTACT']['ID_PROFIL'] = $entity->mainManager->id;

		if($entity->agency)
			$data['CONTACT']['ID_SOCIETE'] = $entity->agency->id;

		$data['CONTACT']['ID_POLE'] = $entity->pole ? $entity->pole->id : 0;

		return $data;
	}
}
