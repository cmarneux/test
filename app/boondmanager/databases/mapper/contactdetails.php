<?php
/**
 * contactdetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use Wish\Mapper;

class ContactDetails extends Mapper{
	public static function fromSQL($data) {
		/** @var Models\ContactDetails $details */
		$details = self::createObject(Models\ContactDetails::class, $data);
		return $details;
	}
}
