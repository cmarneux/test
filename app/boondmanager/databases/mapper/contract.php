<?php
/**
 * contracts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use BoondManager\Services;
use BoondManager\Databases\Local;

/**
 * Class Customer
 * @package BoondManager\Databases\Mapper
 */
class Contract extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Contract
	 */
	public static function fromRow($data) {
		/** @var Models\Contract $contract */
		$contract = self::createObject(Models\Contract::class, $data);

		$contract->resource = self::createObject(Models\Employee::class, $data);

		/*
		if($tab = 'resources/{id}/administrative'){//~ FIXME: ne devrait être fait que dans ce cas ... (==)
			//$agenciesDB = Services\Agencies::getAgencyConfigActivity();
			//$data['advantageTypes'] = $agenciesDB->getAgency($data['ID_SOCIETE'],6)->toArray()['advantageTypes'];
			//~ TODO: il faudrait filtrer les champs de chaque type d'avantage, qui ne doivent pas être retournés par resources/{id}/administrative
			//~ TODO: peut-être ajouter un paramètre $tab au service de recherche des contrats pour savoir quel champs des contrats retourner en fonction de lónglet d'appel ...
		}
		*/
		$contract->agency = Services\Agencies::getAgencyWithConfig($data['ID_SOCIETE']);
		//$contract->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE', 'SOCIETE_RAISON', 'advantageTypes'])));

		return $contract;
	}

	/**
	 * @param Model $data
	 * @return Models\Contract
	 */
	public static function fromSQL($data){
		/** @var Models\Contract $contract */
		$contract = self::createObject(Models\Contract::class, $data);

		$expensesDetails = [];
		if($data['FRAISDETAILS']) {
			foreach($data['FRAISDETAILS'] as $detail){
				$expensesDetails[] = $e = self::createObject(Models\ExpenseDetail::class, $detail);
				$e->agency = Services\Agencies::getBasic($detail['ID_SOCIETE']);
			}
		}
		$contract->expensesDetails = $expensesDetails;

		if($data['ID_PROFIL']){
			if($data['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE){
				$contract->dependsOn = self::createObject(Models\Candidate::class, $data);
			}else{
				$contract->dependsOn = self::createObject(Models\Employee::class, $data);
			}
			if($data['ID_PROFIL_RESPMANAGER']) $contract->dependsOn->mainManager = Services\Managers::getBasic($data['ID_PROFIL_RESPMANAGER']);
			if($data['ID_PROFIL_RESPRH']) $contract->dependsOn->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RESPRH']);
			if($data['COMP_IDSOCIETE']) $contract->dependsOn->agency = Services\Agencies::getBasic($data['COMP_IDSOCIETE']);
		}

		$contract->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		if($data['ID_PARENT']) $contract->relatedContract = new Models\Contract(['id' => $data['ID_PARENT']]);
		if($data['ID_ENFANT']) $contract->childContract = new Models\Contract(['id' =>$data['ID_ENFANT']]);

		return $contract;
	}

	/**
	 * @param Models\Contract $contract
	 * @return array
	 */
	public static function toSQL(Models\Contract $contract)
	{
		$data = [];

		$data['CONTRAT'] = self::modelToDatabaseArray($contract);

		if($contract->agency) $data['CONTRAT']['ID_SOCIETE'] = $contract->agency->id;
		if($contract->relatedContract) $data['CONTRAT']['ID_PARENT'] = $contract->relatedContract->id;
		if($contract->childContract) $data['CONTRAT']['ID_ENFANT'] = $contract->childContract->id;
		if($contract->dependsOn) $data['CONTRAT']['ID_PROFIL'] = $contract->dependsOn->id;


		$data['FRAISDETAILS'] = [];
		foreach($contract->expensesDetails as $expense){
			$data['FRAISDETAILS'][]  = self::modelToDatabaseArray($expense);
		}

		return $data;
	}
}
