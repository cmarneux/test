<?php
/**
 * customer.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Services\BM;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Tools;

/**
 * Class Customer
 * @package BoondManager\Databases\Mapper
 */
class Customer extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Customer
	 */
	public static function fromRow($data) {
		$customer = self::createObject(Models\Customer::class, $data);

		$customer->subscription = self::createObject(Models\Subscription::class, $data);
		$customer->subscription->state = Tools::mapData($data['AB_TYPE'], BM::USER_ACCESS_MAPPING);
		if(is_null($data['AB_FIN'])) $customer->subscription->endDate = '';

		if($data['ID_USER']) $customer->mainSupport = Services\Managers::getBasicSupport($data['ID_USER']);

		$customer->company = self::createObject(Models\Company::class, $data);

		return $customer;
	}

	/**
	 * Convert data from SQL to camelCase
	 * @param Model $data
	 * @return mixed
	 */
	public static function fromSQL($data) {
		$customer = self::createObject(Models\Customer::class, $data);

		$customer->clientToken = Services\Customers::buildToken($data['CLIENT_WEB']);

		$customer->subscription = self::createObject(Models\Subscription::class, $data);
		$customer->subscription->state = Tools::mapData($data['AB_TYPE'], BM::USER_ACCESS_MAPPING);

		if($data['ID_USER']) $customer->mainSupport = Services\Managers::getBasicSupport($data['ID_USER']);

		$customer->company = self::createObject(Models\Company::class, $data);

		$customer->administrator = self::createObject(Models\User::class, $data['ROOT']);
		$customer->administrator->modules = self::createObject(Models\AccountModules::class, $data['ROOT']);

		$customer->databaseServer = Services\Architecture::getBasicDatabaseServer($data['MYSQLSERVER']['BDD_URL']);
		$customer->nodeServer = Services\Architecture::getBasicNodeServer($data['NODESERVER']['NODE_SERVER']);
		$customer->gedServer = Services\Architecture::getBasicGEDServer($data['GEDSERVER']['GED_PATH']);

		return $customer;
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Customer $customer
	 * @return array
	 */
	public static function toSQL(Models\Customer $customer) {
		$tabCustomer = $customer->toArray();
		$data = ['CLIENT' => Tools::reversePublicFieldsToPrivate($customer->getPublicFieldsMapping(), $tabCustomer)];

		$data['CONFIGGROUPE'] = Tools::reversePublicFieldsToPrivate($customer->getPublicFieldsMapping(), $tabCustomer);
		if($customer->clientKey == '') $data['CONFIGGROUPE']['GRPCONF_HASH'] = Services\Customers::buildKey($customer->code);

		if($customer->subscription) {
			$data['ABONNEMENT'] = Tools::reversePublicFieldsToPrivate($customer->subscription->getPublicFieldsMapping(), $customer->subscription->toArray());
			$data['ABONNEMENT']['AB_TYPE'] = Tools::reverseMapData($customer->subscription->state, BM::USER_ACCESS_MAPPING);
			if($customer->subscription->endDate === '') $data['ABONNEMENT']['AB_FIN'] = null;
		}

		if($customer->administrator) {
			$data['ROOT'] = Tools::reversePublicFieldsToPrivate($customer->administrator->getPublicFieldsMapping(), $customer->administrator->toArray());
			if ($customer->administrator->modules)
				$data['CONFIGROOT'] = Tools::reversePublicFieldsToPrivate($customer->administrator->modules->getPublicFieldsMapping(), $customer->administrator->modules->toArray());
		}

		if($customer->databaseServer)
			$data['MYSQLSERVER'] = Tools::reversePublicFieldsToPrivate($customer->databaseServer->getPublicFieldsMapping(), $customer->databaseServer->toArray());

		if($customer->nodeServer)
			$data['NODESERVER'] = Tools::reversePublicFieldsToPrivate($customer->nodeServer->getPublicFieldsMapping(), $customer->nodeServer->toArray());

		if($customer->gedServer)
			$data['GEDSERVER'] = Tools::reversePublicFieldsToPrivate($customer->gedServer->getPublicFieldsMapping(), $customer->gedServer->toArray());
		return $data;
	}
}
