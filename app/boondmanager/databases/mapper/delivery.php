<?php
/**
 * delivery.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Services\CurrentUser;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Models\SearchResult;
use Wish\Tools;

/**
 * Class Delivery
 * @package BoondManager\Databases\Mapper
 */
class Delivery extends Mapper {
	static $numberOfWorkingDays = [];

	private static function getNumberOfWorkingDays($data) {
		if(!isset($data['GRPCONF_CALENDRIER'])) $data['GRPCONF_CALENDRIER'] = CurrentUser::instance()->getCalendar();

		$calendar = $data['GRPCONF_CALENDRIER'];
		$start = $data['MP_DEBUT'];
		$end = $data['MP_FIN'];

		if(!isset(self::$numberOfWorkingDays[$calendar][$start][$end]))
			self::$numberOfWorkingDays[$calendar][$start][$end] = Tools::getNumberOfWorkingDays($start, $end, $calendar);

		return self::$numberOfWorkingDays[$calendar][$start][$end];
	}

	private static function setOccupationRate($data) {
		$nbJrsOuvres = self::getNumberOfWorkingDays($data);
		return $nbJrsOuvres != 0 ? round(($data['MP_NBJRSFACTURE']+$data['MP_NBJRSGRATUIT']) * 100 / $nbJrsOuvres, 0) : 0;
	}

	/**
	 * @param Model $data
	 * @return Models\Delivery
	 */
	public static function fromRow($data) {
		/** @var Models\Delivery $delivery */
		$delivery = self::createObject(Models\Delivery::class, $data);

		$delivery->occupationRate = self::setOccupationRate($data);

		if($data['ITEM_TYPE'] == Models\Delivery::TYPE_RESOURCE) {
			$delivery->dependsOn = self::createObject(Models\Employee::class, self::extractData($data, [
				'COMP_IDPROFIL' => 'ID_PROFIL',
				'COMP_NOM'      => 'PROFIL_NOM',
				'COMP_PRENOM'   => 'PROFIL_PRENOM',
				'COMP_TYPE'     => 'PROFIL_TYPE'
			]));
		} else {
			$delivery->dependsOn = self::createObject(Models\Product::class, $data);
		}

		$delivery->project = self::createObject(Models\Project::class, $data);

		$delivery->project->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		$delivery->project->opportunity = self::createObject(Models\Opportunity::class, $data);
		$delivery->project->contact = self::createObject(Models\Contact::class, $data);
		$delivery->project->company = self::createObject(Models\Company::class, $data);

		$delivery->contract = self::createObject(Models\Contract::class, $data);

		if($data['ID_PARENT'])
			$delivery->groupment = self::createObject(Models\Delivery::class, self::extractData($data, [
				'ID_PARENT' => 'ID_MISSIONPROJET'
			]));

		if($data['ID_MASTER'])
			$delivery->master = self::createObject(Models\Delivery::class, self::extractData($data, [
				'ID_MASTER' => 'ID_MISSIONPROJET'
			]));

		if($data['SLAVE_IDMISSIONPROJET'])
			$delivery->slave = self::createObject(Models\Delivery::class, self::extractData($data, [
				'SLAVE_IDMISSIONPROJET' => 'ID_MISSIONPROJET'
			]));

		return $delivery;
	}

	/**
	 * @param Model $object
	 * @return Models\Delivery
	 */
	public static function fromSQL($object)
	{
		/** @var Models\Delivery $delivery */
		$delivery = self::createObject(Models\Delivery::class, $object);

		$delivery->project              = $project = self::createObject(Models\Project::class, $object);
		$delivery->project->mainManager = Services\Managers::getBasic($object['PRJ_IDPROFIL']);
		$delivery->project->agency      = Services\Agencies::getBasic($object['PRJ_SOCIETE']);
		$delivery->project->pole        = Services\Poles::getBasic($object['PRJ_POLE']);

		if ($object['ID_AO']) {
			$delivery->project->opportunity              = self::createObject(Models\Opportunity::class, $object);
			$delivery->project->opportunity->mainManager = Services\Managers::getBasic($object['AO_IDPROFIL']);
			$delivery->project->opportunity->agency      = Services\Agencies::getBasic($object['AO_SOCIETE']);
			$delivery->project->opportunity->pole        = Services\Poles::getBasic($object['AO_POLE']);
		}

		$delivery->project->contact = self::createObject(Models\Contact::class, $object);
		$delivery->project->company = self::createObject(Models\Company::class, $object);

		if($object['ID_CRMTECHNIQUE']) {
			$delivery->project->technical = self::createObject(Models\Contact::class, self::extractData($object, [
				'ID_CRMTECHNIQUE' => 'ID_CRMCONTACT',
				'CCTECH_PRENOM'   => 'CCON_PRENOM',
				'CCTECH_NOM'      => 'CCON_NOM',
				'CCTECH_TEL1'     => 'CCON_TEL1',
				'CCTECH_TEL2'     => 'CCON_TEL2',
				'CCTECH_SERVICE'  => 'CCON_SERVICE'
			]));
			$delivery->project->technical->company = self::createObject(Models\Company::class, self::extractData($object, [
				'CSTECH_ID'      => 'ID_CRMSOCIETE',
				'CSTECH_SOCIETE' => 'CSOC_SOCIETE',
				'CSTECH_ADR'     => 'CSOC_ADR',
				'CSTECH_CP'      => 'CSOC_CP',
				'CSTECH_VILLE'   => 'CSOC_VILLE',
				'CSTECH_PAYS'    => 'CSOC_PAYS'
			]));
		}

		switch($object['ITEM_TYPE']) {
			case Models\Delivery::TYPE_RESOURCE:
				/** @var Models\Employee $employee*/
				$delivery->dependsOn = $employee = self::createObject(Models\Employee::class, self::extractData($object, [
					'ID_ITEM' => 'ID_PROFIL',
					'PROFIL_NOM',
					'PROFIL_PRENOM',
					'PROFIL_EMAIL',
					'PROFIL_TYPE',
					'PROFIL_VISIBILITY',
					'ID_USER',
					'USER_TYPE'
				]));
				$employee->mainManager = Services\Managers::getBasic($object['ID_PROFIL_RESPMANAGER']);
				$employee->hrManager = Services\Managers::getBasic($object['ID_PROFIL_RESPRH']);
				$employee->agency = Services\Agencies::getBasic($object['ID_SOCIETE']); // should be extented with full config
				$employee->pole = Services\Poles::getBasic($object['ID_POLE']);

				$delivery->contracts = Contract::fromSearchResult($object['CONTRATS'])->rows;

				break;
			case Models\Delivery::TYPE_PRODUCT:
				$delivery->dependsOn = $product = self::createObject(Models\Product::class, self::extractData($object, [
					'ID_ITEM' => 'ID_PRODUIT',
					'PRODUIT_NOM',
					'PRODUIT_REF',
					'PRODUIT_TYPE'
				]));
				break;
		}

		$documents = [];
		foreach($object['DOCUMENTS'] as $row){
			$documents[] = self::createObject(Models\Document::class, $row);
		}
		$delivery->files = $documents;

		$expenses = [];
		foreach($object['FRAISDETAILS'] as $row) {
			/** @var Models\ExpenseDetail $e */
			$expenses[] = $e = self::createObject(Models\ExpenseDetail::class, $row);
			$e->resetRelationships();
			$e->expenseType = new Models\ExpenseType([
				'reference' => $row['FRSDETAILS_TYPEFRSREF'],
				'name' => $row['TYPEFRS_NAME']
			]);
			$e->agency = Services\Agencies::getBasic($row['ID_SOCIETE']);
		}
		$delivery->expensesDetails = $expenses;

		if($object['MASTER']) {
			/** @var Models\Delivery $master */
			$master = self::createObject(Models\Delivery::class, $object['MASTER']);
			$master->project = self::createObject(Models\Project::class, $object['MASTER']);
			$master->project->mainManager = Services\Managers::getBasic($object['MASTER']['PRJ_IDPROFIL']);
			$delivery->master = $master;
		} else if ($object['SLAVE']) {
			/** @var Models\Delivery $slave */
			$slave = self::createObject(Models\Delivery::class, $object['SLAVE']);
			$slave->project = self::createObject(Models\Project::class, $object['SLAVE']);
			$slave->project->mainManager = Services\Managers::getBasic($object['SLAVE']['PRJ_IDPROFIL']);
			$delivery->slave = $slave;
		}

		$additionalTurnoverAndCosts = [];
		foreach($object['MISSIONDETAILS'] as $deliveryDetail) {
			$addData = self::createObject(Models\DeliveryDetail::class, $deliveryDetail);
			if($deliveryDetail['ID_ACHAT']) $addData->purchase = self::createObject(Models\Purchase::class, $deliveryDetail);
			$additionalTurnoverAndCosts[] = $addData;
		}
		$delivery->additionalTurnoverAndCosts = $additionalTurnoverAndCosts;

		$delivery->costsSimulatedExcludingTax = ($delivery->numberOfDaysInvoicedOrQuantity + $delivery->numberOfDaysFree) * $delivery->averageDailyCost + $delivery->additionalCostsExcludingTax;

		$delivery->turnoverSimulatedExcludingTax = $delivery->numberOfDaysInvoicedOrQuantity * $delivery->averageDailyPriceExcludingTax + $delivery->additionalTurnoverExcludingTax;

		$delivery->marginSimulatedExcludingTax = $delivery->turnoverSimulatedExcludingTax - $delivery->costsSimulatedExcludingTax;

		if(Services\BM::isProfitabilityCalculatingBasedOnMarginRate()) {
			$delivery->profitabilitySimulated = ($delivery->costsSimulatedExcludingTax) ? 100 * $delivery->marginSimulatedExcludingTax / $delivery->costsSimulatedExcludingTax : 0;
		} else {
			$delivery->profitabilitySimulated = ($delivery->turnoverSimulatedExcludingTax) ? 100 * $delivery->marginSimulatedExcludingTax / $delivery->turnoverSimulatedExcludingTax : 0;
		}
		return $delivery;
	}

	/**
	 * @param Model[] $data
	 * @return Models\Project[]
	 */
	public static function fromSearchDeliveriesAndBatchesForEmployeeOnPeriod($data)
	{
		$projects = [];
		$deliveries = [];
		$batches = [];
		foreach($data as $row){
			/** @var Models\Project $project */
			if(!isset($projects[$row['ID_PROJET']])) {
				$project = self::createObject(Models\Project::class, $row);
				$projects[$project->id] = $project ;
				$deliveries[$project->id] = [];
				$batches[$project->id] = [];
				$project->company = self::createObject(Models\Company::class, $row);
			} else {
				$project = $projects[$row['ID_PROJET']];
			}

			if($d = self::createObject(Models\Delivery::class, $row)) $deliveries[$project->id][$d->id] = $d;
			if($b = self::createObject(Models\Batch::class, $row)) $batches[$project->id][$b->id] = $b;
		}

		foreach ($projects as $project){
			$project->deliveries = array_values($deliveries[$project->id]);
			$project->batches = array_values($batches[$project->id]);
		}

		return array_values($projects);
	}

	/**
	 * @param Models\Delivery $delivery
	 * @return array
	 */
	public static function toSQL($delivery) {

		$data = [
			'MISSION' => self::modelToDatabaseArray($delivery)
		];

		$data['MISSION']['ID_ITEM'] = $delivery->dependsOn->id;
		$data['MISSION']['ITEM_TYPE'] = ($delivery->dependsOn instanceof Models\Employee) ? Models\Delivery::TYPE_RESOURCE : Models\Delivery::TYPE_PRODUCT;
		$data['MISSION']['ID_PROJET'] = $delivery->project ? $delivery->project->id : 0;
		$data['MISSION']['ID_MASTER'] = $delivery->master ? $delivery->master->id : 0;

		$data['FRAISDETAILS'] = [];
		foreach($delivery->expensesDetails as $expense) {
			$detail = self::modelToDatabaseArray($expense);
			$detail['FRSDETAILS_TYPEFRSREF'] = $expense->expenseType->reference;
			$detail['ID_SOCIETE'] = $expense->agency->id;
			$data['FRAISDETAILS'][] = $detail;
		}

		$data['MISSIONDETAILS'] = [];
		foreach ($delivery->additionalTurnoverAndCosts as $detail) {
			$row = self::modelToDatabaseArray($detail);
			$row['ID_ACHAT'] = ($detail->purchase) ? $detail->purchase->id : 0;
			$data['MISSIONDETAILS'][] = $row;
		}

		$data['ENFANTS'] = [];
		//TODO (maj)

		return $data;
	}
}
