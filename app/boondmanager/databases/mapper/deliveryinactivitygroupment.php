<?php
/**
 * deliveryinactivitygroupment.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;

/**
 * Class Delivery
 * @package BoondManager\Databases\Mapper
 */
class DeliveryInactivityGroupment extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Delivery|Models\Inactivity|Models\Groupment
	 */
	public static function fromRow($data) {

		if($data['PRJ_TYPE'] < 0) {
			return Inactivity::fromRow($data);
		}else if (in_array($data['ITEM_TYPE'], [Models\Delivery::TYPE_RESOURCE, Models\Delivery::TYPE_PRODUCT]) ){
			return Delivery::fromRow($data);
		}else {
			return Groupment::fromRow($data);
		}
	}
}
