<?php
/**
 * device.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use Wish\Models\Model;

class Device extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Device
	 */
	public static function fromSQL($data) {
		$device = self::createObject(Models\Device::class, $data);

		$device->isManaged = $data['DALL_NOM'] != '' ? true : false;
		$device->isSessionExists = $data['DALL_SESSION'] != '' ? true : false;

		return $device;
	}
}
