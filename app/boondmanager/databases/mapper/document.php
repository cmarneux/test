<?php
/**
 * file.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;

class Document extends Mapper{
	/**
	 * @param \Wish\Models\Model $data
	 * @return Models\Document
	 */
	public static function fromSQL($data) {
		/** @var Models\Document $file */
		$file = self::createObject(Models\Document::class, $data);

		switch ($data['DOC_TYPE']) {
			case Models\Document::TYPE_COMPANY:
				$file->dependsOn = new Models\Company(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_OPPORTUNITY:
				$file->dependsOn = new Models\Opportunity(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_PROJECT:
				$file->dependsOn = new Models\Project(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_ORDER:
				$file->dependsOn = new Models\Order(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_PRODUCT:
				$file->dependsOn = new Models\Product(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_PURCHASE:
				$file->dependsOn = new Models\Purchase(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_ACTION:
				$file->dependsOn = new Models\Action(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_DELIVERY:
				$file->dependsOn = new Models\Delivery(['id' => $data['ID_PARENT']]);
				break;
			case Models\Document::TYPE_POSITIONING:
				$file->dependsOn = new Models\Positioning(['id' => $data['ID_PARENT']]);
				break;
		}

		return $file;
	}
}
