<?php
/**
 * employee.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;
use BoondManager\Models;
use BoondManager\Services;

class Employee extends Mapper {
	/**
	 * @param Model $data
	 * @return Models\Employee
	 */
	public static function fromRow($data){
		$contextId = Tools::getContext()->startTiming('employee mapping', $group = true);

		/** @var Models\Employee $employee */
		$employee = self::createObject(Models\Employee::class, $data);

		$employee->availability = in_array($data['PARAM_TYPEDISPO'], [1, 3]) ? 'immediate' : $data['PARAM_DATEDISPO'];
		$employee->forceAvailability = in_array($data['PARAM_TYPEDISPO'], [3, 4]);

		if($data['ID_CV'])
			$employee->resume = self::createObject(Models\Resume::class, $data);

		$employee->mainManager = Services\Managers::getBasic($data['RESP_ID']);
		$employee->hrManager = Services\Managers::getBasic($data['RESPRH_ID']);
		$employee->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		$employee->pole = Services\Poles::getBasic($data['ID_POLE']);

		Tools::getContext()->endTiming($contextId);
		return $employee;
	}

	/**
	 * @param $data
	 * @return Models\Employee
	 */
	private static function buildEmployee($data) {

		/** @var Models\Employee $employee */
		$employee = self::createObject(Models\Employee::class, $data);

		if($data['ID_PROFIL_RESPMANAGER']) $employee->mainManager = Services\Managers::getBasic($data['ID_PROFIL_RESPMANAGER']);
		if($data['ID_PROFIL_RESPRH']) $employee->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RESPRH']);
		if($data['ID_SOCIETE']) $employee->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		if($data['ID_POLE']) $employee->pole = Services\Poles::getBasic($data['ID_POLE']);

		if($data['ID_CV']) $employee->resume = new Models\Resume(['id' => $data['ID_CV']]);

		if($data['USER_TPSFRSSTART']) {
			$employee->activityExpensesStartDate = ($data['USER_TPSFRSSTART'] != '3000-01-01') ? $data['USER_TPSFRSSTART'] : Models\Account::INACTIVE;
		}else {
			$employee->activityExpensesStartDate = Models\Account::HIRING_DATE;
		}

		$employee->workUnitRate = isset($data['USER_TAUXHORAIRE']) ? $data['USER_TAUXHORAIRE'] : 'notUsed';

		list(
			$employee->dashboardGraphsAvailable,
			$employee->dashboardGraphsPeriod,
			$employee->dashboardGraphsPerimeter
			) = Account::fromDashboardConfig($data['CONFIG_DASHBOARD']);

		return $employee;
	}

	/**
	 * @param Model $data
	 * @param int $tab
	 * @return Models\Employee
	 */
	public static function fromSQL($data, $tab = Models\Employee::TAB_DEFAULT)
	{
		$employee = self::buildEmployee($data);

		switch ($tab){
			case Models\Employee::TAB_INFORMATION:
				self::fromInformation($employee, $data);
				break;
			case Models\Employee::TAB_ADMINISTRATIVE:
				self::fromAdministrative($employee, $data);
				break;
			case Models\Employee::TAB_TECHNICALDATA:
				self::fromTechnicalData($employee, $data);
				break;
			case Models\Employee::TAB_POSITIONINGS:
				self::fromPositionings($employee, $data);
				break;
			case Models\Employee::TAB_DELIVERIES:
				self::fromDeliveries($employee, $data);
				break;
			case Models\Employee::TAB_ABSENCESACCOUNTS:
				self::fromAbsencesAccounts($employee, $data);
				break;
		}

		return $employee;
	}

	private static function fromInformation(Models\Employee $employee, Model $data) {

		$employee->availability = in_array($data['PARAM_TYPEDISPO'], [1, 3]) ? 'immediate' : $data['PARAM_DATEDISPO'];
		$employee->forceAvailability = in_array($data['PARAM_TYPEDISPO'], [3, 4]);

		$cvs = [];
		if($data['CVs']) foreach($data['CVs'] as $cv){
			$cvs[] = self::createObject(Models\Resume::class, $cv);
		}

		$employee->resumes = $cvs;

		$networks = [];
		if($data['WEBSOCIALS']) foreach($data['WEBSOCIALS'] as $net){
			$networks[] = self::createObject(Models\WebSocial::class, $net);
		}
		$employee->socialNetworks = $networks;

		if($data['ID_CONTRAT']) {
			$employee->contract = self::createObject(Models\Contract::class, $data);
		}
	}

	private static function fromAdministrative(Models\Employee $employee, Model $data) {

		if($data['ID_PROFILCANDIDAT']) {
			$employee->candidate = self::createObject(Models\Candidate::class, self::extractData($data, [
				'ID_PROFILCANDIDAT' => 'ID_PROFIL',
				'CAND_NOM'          => 'PROFIL_NOM',
				'CAND_PRENOM'       => 'PROFIL_PRENOM',
			]));
		}

		if($data['ID_CRMCONTACT']) {
			$employee->providerContact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',
			]));
		}

		if($data['ID_CRMSOCIETE']) {
			$employee->providerCompany = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT', 'CSOC_SOCIETE',
			]));
		}

		$contrats = [];
		if($data['CONTRATS']) {
			foreach ($data['CONTRATS'] as $ctrData){
				/** @var Models\Contract $c */
				$c = self::createObject(Models\Contract::class, $ctrData);
				if(intval($ctrData['ID_PARENT'])) $c->relatedContract = new Models\Contract(['id' => $ctrData['ID_PARENT']]);

				$c->agency = self::createObject(Models\Agency::class, $ctrData);
				$c->agency->advantageTypes = Services\Advantages::getAdvantagesTypes($c->agency->id);
				$contrats[] = $c;
			}
		}
		$employee->contracts = $contrats;

		$files = [];
		if($data['FILES']) {
			foreach ($data['FILES'] as $fData){
				/** @var Models\Contract $c */
				$files[] = $f = self::createObject(Models\File::class, $fData);
			}
		}
		$employee->files = $files;

		if($data['ID_CONTRAT']) {
			$employee->contract = self::createObject(Models\Contract::class, $data);
		}
	}

	private static function fromTechnicalData(Models\Employee $employee, Model $data)
	{
		$employee->languages = array_map(function($value){
				list($language, $level) = explode('|', $value);
				return new Models\Language([
					'language' => $language,
					'level' => $level
				]);
			},explode('#', $data['DT_LANGUES']));

		$employee->tools = array_map(function($value){
			list($tool, $level) = explode('|', $value);
			return new Models\Tool([
				'tool' => $tool,
				'level' => $level
			]);
		},explode('#', $data['DT_OUTILS']));

		$refs = [];
		foreach($data['REFERENCES'] as $row){
			$refs[] = self::createObject(Models\Reference::class, $row);
		}
		$employee->references = $refs;
	}

	private static function fromPositionings(Models\Employee $employee, Model $data) {
		if($data['ID_PROFILCANDIDAT']) {
			$employee->candidate = self::createObject(Models\Candidate::class, self::extractData($data, [
				'ID_PROFILCANDIDAT' => 'ID_PROFIL',
				'CAND_NOM'          => 'PROFIL_NOM',
				'CAND_PRENOM'       => 'PROFIL_PRENOM',
			]));
		}
	}

	private static function fromDeliveries(Models\Employee $employee, Model $data) {
		if($data['ID_PROFILCANDIDAT']) {
			$employee->candidate = self::createObject(Models\Candidate::class, self::extractData($data, [
				'ID_PROFILCANDIDAT' => 'ID_PROFIL',
				'CAND_NOM'          => 'PROFIL_NOM',
				'CAND_PRENOM'       => 'PROFIL_PRENOM',
			]));
		}
	}

	private static function fromAbsencesAccounts(Models\Employee $employee, Model $data)
	{
	}

	/**
	 * @param $data
	 * @return Models\Employee
	 */
	public static function fromConfiguration($data){
		return self::buildEmployee($data);
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Employee $employee
	 * @return array
	 */
	public static function toSQL(Models\Employee $employee, $tab) {

		$data['PROFIL'] = self::modelToDatabaseArray($employee);


		switch($tab) {
			case Models\Employee::TAB_INFORMATION:
				self::toInformation($employee, $data);
				break;
			case Models\Employee::TAB_ADMINISTRATIVE:
				self::toAdministrative($employee, $data);
				break;
			case Models\Employee::TAB_TECHNICALDATA:
				self::toTD($employee, $data);
				break;
		}

		return $data;
	}
	/**
	 * @param Models\Employee $employee
	 * @param array $data
	 */
	private static function toInformation(Models\Employee $employee, &$data) {

		if($employee->exists('socialNetworks')) {
			$data['WEBSOCIALS'] = [];
			foreach($employee->socialNetworks as $network) {
				$data['WEBSOCIALS'][] = self::modelToDatabaseArray($network);
			}
		}

		if($employee->exists('availability') && $employee->exists('forceAvailability')) {
			if (preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/', $employee->availability)) {
				if($employee->forceAvailability) $data['PROFIL']['PARAM_DATEDISPO'] = $employee->availability;
				$data['PROFIL']['PARAM_TYPEDISPO'] = $employee->forceAvailability ? 4 : 2;
			} else {
				$data['PROFIL']['PARAM_TYPEDISPO'] = $employee->forceAvailability ? 3 : 1;
			}
		}

		$data['PROFIL']['ID_SOCIETE'] = $employee->agency->id;
		$data['PROFIL']['ID_RESPMANAGER'] = is_null($employee->mainManager) ? 0 : Services\Managers::getUserIdFromEmployeeId($employee->mainManager->id);
		$data['PROFIL']['ID_RESPRH'] = is_null($employee->hrManager) ? 0 : Services\Managers::getUserIdFromEmployeeId($employee->hrManager->id);
		$data['PROFIL']['ID_POLE'] = is_null($employee->pole) ? 0 : $employee->pole->id;
	}

	/**
	 * @param Models\Employee $employee
	 * @param array $data
	 */
	private static function toAdministrative(Models\Employee $employee, &$data) {

		if($employee->exists('providerContact')) {
			$data['PROFIL']['ID_CRMCONTACT'] = $employee->providerContact ? $employee->providerContact->id : 0;
		}

		if($employee->exists('providerCompany')) {
			$data['PROFIL']['ID_CRMSOCIETE'] = $employee->providerCompany ? $employee->providerCompany->id : 0;
		}

	}

	/**
	 * @param Models\Employee $employee
	 * @param array $data
	 */
	private static function toTD(Models\Employee $employee, &$data) {

		if($employee->exists('references')) {
			$data['REFERENCES'] = [];
			foreach($employee->references as $ref) {
				$data['REFERENCES'][] = self::modelToDatabaseArray($ref);;
			}
		}

		$data['DT'] = self::extractData($data['PROFIL'], [
			'DT_LANGUES', 'DT_OUTILS', 'DT_DIPLOMES', 'DT_EXPERIENCE', 'DT_FORMATION', 'DT_TITRE', 'COMP_APPLICATIONS', 'COMP_INTERVENTIONS', 'COMP_COMPETENCE'
		]);
	}
}
