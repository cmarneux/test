<?php
/**
 * searchexpensesreport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Models\SearchResult;

class ExpensesReport extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public static function fromRow($data)
	{
		/** @var Models\ExpensesReport $expensesReport */
		$expensesReport = self::createObject(Models\ExpensesReport::class, self::extractData($data, [
			'ID_DOCUMENT'   => 'ID_LISTEFRAIS',
			'DOCUMENT_DATE' => 'LISTEFRAIS_DATE',
			'DOCUMENT_ETAT' => 'LISTEFRAIS_ETAT',
			'LISTEFRAIS_BKMVALUE',
			'LISTEFRAIS_AVANCE',
			'LISTEFRAIS_REGLE',
		]));

		$expensesReport->resource = self::createObject(Models\Employee::class, self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_REFERENCE'
		]));
		$expensesReport->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		return $expensesReport;
	}

	/**
	 * @param SearchResult $searchResult
	 * @return SearchResult
	 */
	public static function fromSearchResult($searchResult)
	{
		$reports = [];
		foreach($searchResult->rows as $data){
			// on extrait d'abord les données relatives au validateur s'il existe
			if(isset($data['RESP_ID_PROFIL']) && $data['RESP_ID_PROFIL']){
				$expectedValidator = self::createObject(Models\Employee::class, self::extractData($data, [
					'RESP_ID_PROFIL' => 'ID_PROFIL',
					'RESP_NOM'       => 'PROFIL_NOM',
					'RESP_PRENOM'    => 'PROFIL_PRENOM'
				]));

				/** @var Models\Validation $validation */
				$validation = self::createObject(Models\Validation::class, self::extractData($data, [
					'ID_VALIDATION',
					'VAL_DATE',
					'VAL_STATE' => 'VAL_ETAT'
				]));

				$validation->expectedValidator = $expectedValidator;
			}

			// on s'assure de travailler avec un tableau associatif (cle = chaine) pour maintenir l'ordre du search)
			$id = strval($data['ID_DOCUMENT']);

			if(!array_key_exists($id, $reports)){
				unset($validationsList);
				$report = self::fromRow($data);
				$reports[$id] = $report;
			}else{
				$report = $reports[$id];
			}

			if(isset($validation)){
				if(!isset($validationsList)) $validationsList = [$validation];
				else $validationsList[] = $validation;
				$report->validations = $validationsList;
				unset($validation);
			}
		}

		$searchResult->rows = array_values($reports);

		return $searchResult;
	}

	/**
	 * @param $data
	 * @return Models\ExpensesReport
	 */
	public static function fromSQL($data)
	{
		/** @var Models\ExpensesReport $expensesReport */
		$expensesReport = self::createObject(Models\ExpensesReport::class, self::extractData($data, [
			'ID_LISTEFRAIS',
			'LISTEFRAIS_DATE',
			'LISTEFRAIS_ETAT',
			'LISTEFRAIS_AVANCE',
			'LISTEFRAIS_REGLE',
			'LISTEFRAIS_COMMENTAIRES',
			'LISTEFRAIS_CLOTURE',
			'LISTEFRAIS_DEVISEAGENCE',
			'LISTEFRAIS_CHANGEAGENCE'
		]));

		$expensesReport->ratePerKilometerType = self::createObject(Models\RatePerKilometerType::class, self::extractData($data, [
			'LISTEFRAIS_BKMREF' => 'BKM_REF',
			'BKM_NAME',
			'LISTEFRAIS_BKMVALUE' => 'BKM_VALUE'
		]), $noID = true);

		$expensesReport->agency = self::createObject(Models\Agency::class, $data);
		$expensesReport->resource = self::createObject(Models\Employee::class, self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_REFERENCE'
		]));
		$expensesReport->resource->pole = Services\Poles::getBasic($data['ID_POLE']);
		$expensesReport->resource->mainManager = Services\Managers::getBasic($data['ID_PROFIL_MANAGER']);
		$expensesReport->resource->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RH']);

		$realExpenses = [];
		foreach ($data['LISTEFRAISREEL'] as $row){
			/** @var Models\Expense $expense */
			$realExpenses[] = $expense = self::createObject(Models\Expense::class, self::extractData($row, [
				'ID_FRAISREEL'        => 'ID_FRAIS',
				'FRAISREEL_INTITULE'  => 'FRAIS_INTITULE',
				'FRAISREEL_REFACTURE' => 'FRAIS_REFACTURE',
				'FRAISREEL_NUM'       => 'FRAIS_NUM',
				'FRAISREEL_DATE'      => 'FRAIS_DATE',
				'FRAISREEL_DEVISE'    => 'FRAIS_DEVISE',
				'FRAISREEL_CHANGE'    => 'FRAIS_CHANGE',
				'FRAISREEL_TVA'       => 'FRAIS_TVA'
			]));
			$expense->resetRelationships();

			if($row['FRAISREEL_TYPEFRSREF']) {
				$expense->expenseType = self::createObject(Models\ExpenseType::class, self::extractData($row, [
					'FRAISREEL_TYPEFRSREF' => 'TYPEFRS_REF',
					'TYPEFRS_TVA',
					'TYPEFRS_NAME'
				]), $allowNoId = true);
				$expense->isKilometricExpense = false;
				$expense->amountIncludingTax = $row['FRAISREEL_MONTANT'];
			} else {
				$expense->isKilometricExpense = true;
				$expense->numberOfKilometers = $row['FRAISREEL_MONTANT'];
				$expense->amountIncludingTax = $row['FRAISREEL_MONTANT'] * $expensesReport->ratePerKilometerType->amount;
			}

			if($row['ID_PROJET'] == Models\Expense::DB_TYPE_ABSENCE) {
				$expense->activityType = Models\Expense::TYPE_ABSENCE;
			} else if($row['ID_PROJET'] == Models\Expense::DB_TYPE_INTERNAL) {
				$expense->activityType = Models\Expense::TYPE_INTERNAL;
			} else if($expense->project = self::createObject(Models\Project::class, $row) ){
				$expense->activityType = Models\Expense::TYPE_PRODUCTION;
				$expense->project->company = self::createObject(Models\Company::class, $row);
				$expense->delivery = self::createObject(Models\Delivery::class, $row);
				$expense->batch = self::createObject(Models\Batch::class, $row);
			}
		}

		$expensesReport->actualExpenses = $realExpenses;

		$fixedExpenses = [];
		foreach ($data['LISTEFRAISFORFAIT'] as $row){
			foreach($row['LISTEJOURS'] as $dailyExpense) {
				/** @var Models\Expense $expense */
				$fixedExpenses[] = $expense = self::createObject(Models\Expense::class, $dailyExpense);
				$expense->resetRelationships();
				$expense->row = $row['ID_LIGNEFRAIS'];

				if($row['LIGNEFRAIS_TYPEFRSREF']) {
					$expense->expenseType         = self::createObject(Models\ExpenseType::class, self::extractData($row, [
						'LIGNEFRAIS_TYPEFRSREF' => 'TYPEFRS_REF',
						'TYPEFRS_TVA',
						'TYPEFRS_NAME'
					]), $allowNoId = true);
					$expense->isKilometricExpense = false;
					$expense->amountIncludingTax = $dailyExpense['FRAIS_VALUE'];
				} else {
					$expense->isKilometricExpense = true;
					$expense->numberOfKilometers = $dailyExpense['FRAIS_VALUE'];
					$expense->amountIncludingTax = $dailyExpense['FRAIS_VALUE'] * $expensesReport->ratePerKilometerType->amount;
				}

				if($row['ID_PROJET'] == Models\Expense::DB_TYPE_ABSENCE) {
					$expense->activityType = Models\Expense::TYPE_ABSENCE;
				} else if($row['ID_PROJET'] == Models\Expense::DB_TYPE_INTERNAL) {
					$expense->activityType = Models\Expense::TYPE_INTERNAL;
				} else if($expense->project = self::createObject(Models\Project::class, $row) ) {
					$expense->activityType = Models\Expense::TYPE_PRODUCTION;
					$expense->project->company = self::createObject(Models\Company::class, $row);
					$expense->delivery = self::createObject(Models\Delivery::class, $row);
					$expense->batch = self::createObject(Models\Batch::class, $row);
				}
			}
		}

		$expensesReport->fixedExpenses = $fixedExpenses;

		$validations = [];
		foreach($data['LISTEVALIDATIONS'] as $row) {
			/** @var Models\Validation $vals */
			$validations[] = $vals = self::createObject(Models\Validation::class, $row);
			$vals->realValidator = Services\Managers::getBasic($row['ID_VALIDATEUR']);
			$vals->expectedValidator = Services\Managers::getBasic($row['ID_PROFIL']);
		}
		$expensesReport->validations = $validations;

		$files = [];
		foreach($data['JUSTIFICATIFS'] as $row){
			$files[] = self::createObject(Models\Proof::class, $row);
		}
		$expensesReport->files = $files;

		if($data['ID_LISTETEMPS']) $expensesReport->timesReport = new Models\TimesReport(['id' => $data['ID_LISTETEMPS']]);

		return $expensesReport;
	}

	/**
	 * @param Models\ExpensesReport $entity
	 * @return array
	 */
	public static function toSQL($entity) {
		$data = [];

		$data['FRAIS'] = self::modelToDatabaseArray($entity);
		$data['FRAIS']['LISTEFRAIS_BKMVALUE'] = $entity->ratePerKilometerType->amount;
		$data['FRAIS']['LISTEFRAIS_BKMREF'] = $entity->ratePerKilometerType->reference;

		$data['FRAIS']['ID_PROFIL'] = $entity->resource->id;
		$data['FRAIS']['ID_SOCIETE'] = $entity->agency->id;

		if($entity->exists('actualExpenses')) {
			$data['LISTEFRAISREEL'] = [];
			foreach ($entity->actualExpenses as $expense) {
				$expData                         = self::modelToDatabaseArray($expense);
				$expData['ID_PROJET']            = $expense->project ? intval($expense->project->id) : 0;
				$expData['ID_MISSIONPROJET']     = $expense->delivery ? intval($expense->delivery->id) : 0;
				$expData['ID_LOT']               = $expense->batch ? intval($expense->batch->id) : 0;
				$expData['FRAISREEL_MONTANT']    = $expense->isKilometricExpense ? $expense->numberOfKilometers : $expense->amountIncludingTax;
				$expData['FRAISREEL_TYPEFRSREF'] = $expense->expenseType && $expense->expenseType->reference ? $expense->expenseType->reference : 0;
				$expData['FRAISREEL_TVA']        = $expense->tax;
				$expData['FRAISREEL_INTITULE']   = $expense->title;
				$expData['FRAISREEL_DEVISE']     = $expense->currency;
				$expData['FRAISREEL_CHANGE']     = $expense->exchangeRate;
				$expData['FRAISREEL_DATE']       = $expense->startDate;
				$expData['FRAISREEL_NUM']        = $expense->number;
				$expData['FRAISREEL_REFACTURE']  = $expense->reinvoiced;

				$data['LISTEFRAISREEL'][] = $expData;
			}
		}

		if($entity->exists('fixedExpenses')) {
			$data['LISTEFRAISFORFAIT'] = [];
			foreach ($entity->fixedExpenses as $expense) {
				if(!isset($data['LISTEFRAISFORFAIT'][$expense->row])) {
					$expData = [];
					$expData['ID_LIGNEFRAIS']        = $expense->row;
					$expData['ID_PROJET']            = $expense->project ? intval($expense->project->id) : 0;
					$expData['ID_MISSIONPROJET']     = $expense->delivery ? intval($expense->delivery->id) : 0;
					$expData['ID_LOT']               = $expense->batch ? intval($expense->batch->id) : 0;

					if(isset($expense->expenseType)) {
						$expData['LIGNEFRAIS_TYPEFRSREF'] = $expense->expenseType->reference;
						$expData['LIGNEFRAIS_TVA'] = $expense->expenseType->taxRate;
					} else {
						$expData['LIGNEFRAIS_TYPEFRSREF'] = 0;
						$expData['LIGNEFRAIS_TVA'] = 0;
					}

					$expData['LISTEJOURS'] = [];
				} else {
					$expData = $data['LISTEFRAISFORFAIT'][$expense->row];
				}

				$expData['LISTEJOURS'][] = [
					'ID_FRAIS' => $expense->id,
					'FRAIS_DATE' => $expense->startDate,
					'FRAIS_VALUE' => $expense->isKilometricExpense ? $expense->numberOfKilometers : $expense->amountIncludingTax
				];

				$data['LISTEFRAISFORFAIT'][ $expense->row ] = $expData;
			}
		}

		return $data;
	}
}
