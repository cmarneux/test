<?php
/**
 * file.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use Wish\Mapper;

class File extends Mapper{
	/**
	 * @param \Wish\Models\Model $data
	 * @return Models\File
	 */
	public static function fromSQL($data) {
		/** @var Models\File $file */
		$file = self::createObject(Models\File::class, $data);
		if($data['ID_PROFIL']) {
			if($data['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE) {
				$file->dependsOn = self::createObject(Models\Candidate::class, $data);
			} else {
				$file->dependsOn = self::createObject(Models\Employee::class, $data);
			}
		}
		return $file;
	}
}
