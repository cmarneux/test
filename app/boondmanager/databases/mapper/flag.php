<?php
/**
 * flag.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Services;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class Flag
 * @package BoondManager\Databases\Mapper
 */
class Flag extends Mapper {
	/**
	 * Convert searchresult's row from SQL to camelCase
	 * @param Model $data
	 * @return Models\Flag
	 */
	public static function fromRow($data) {
		$flag = self::createObject(Models\Flag::class, $data);

		$flag->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		return $flag;
	}

	/**
	 * Convert data from SQL to camelCase
	 * @param Model $data
	 * @return Models\Flag
	 */
	public static function fromSQL($data) {
		$flag = self::createObject(Models\Flag::class, $data);

		$flag->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		return $flag;
	}

	/**
	 * Convert entity's data from camelCase to SQL
	 * @param Models\Flag $flag
	 * @return array
	 */
	public static function toSQL(Models\Flag $flag) {
		$data = ['USERFLAG' => Tools::reversePublicFieldsToPrivate($flag->getPublicFieldsMapping(), $flag->toArray())];

		$data['USERFLAG']['ID_USER'] = Services\Managers::getUserIdFromEmployeeId($flag->mainManager->id);
		return $data;
	}
}
