<?php
/**
 * groupment.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Tools;

/**
 * Class Groupment
 * @package BoondManager\Databases\Mapper
 */
class Groupment extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Groupment
	 */
	public static function fromRow($data) {
		/** @var Models\Groupment $delivery */
		$delivery = self::createObject(Models\Groupment::class, $data);

		$delivery->project = self::createObject(Models\Project::class, $data);

		$delivery->project->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		$delivery->project->opportunity = self::createObject(Models\Opportunity::class, $data);
		$delivery->project->contact = self::createObject(Models\Contact::class, $data);
		$delivery->project->company = self::createObject(Models\Company::class, $data);

		return $delivery;
	}

	/**
	 * @param Model $object
	 * @return Models\Groupment
	 */
	public static function fromSQL($object)
	{
		/** @var Models\Groupment $groupment */
		$groupment = self::createObject(Models\Groupment::class, $object);

		$groupment->project              = $project = self::createObject(Models\Project::class, $object);
		$groupment->project->mainManager = Services\Managers::getBasic($object['PRJ_IDPROFIL']);
		$groupment->project->agency      = Services\Agencies::getBasic($object['PRJ_SOCIETE']);
		$groupment->project->pole        = Services\Poles::getBasic($object['PRJ_POLE']);

		if ($object['ID_AO']) {
			$groupment->project->opportunity              = self::createObject(Models\Opportunity::class, $object);
			$groupment->project->opportunity->mainManager = Services\Managers::getBasic($object['AO_IDPROFIL']);
			$groupment->project->opportunity->agency      = Services\Agencies::getBasic($object['AO_SOCIETE']);
			$groupment->project->opportunity->pole        = Services\Poles::getBasic($object['AO_POLE']);
		}

		$groupment->project->contact = self::createObject(Models\Contact::class, $object);
		$groupment->project->company = self::createObject(Models\Company::class, $object);

		if($object['ID_CRMTECHNIQUE']) {
			$groupment->project->technical = self::createObject(Models\Contact::class, self::extractData($object, [
				'ID_CRMTECHNIQUE' => 'ID_CRMCONTACT',
				'CCTECH_PRENOM'   => 'CCON_PRENOM',
				'CCTECH_NOM'      => 'CCON_NOM',
				'CCTECH_TEL1'     => 'CCON_TEL1',
				'CCTECH_TEL2'     => 'CCON_TEL2',
				'CCTECH_SERVICE'  => 'CCON_SERVICE'
			]));
			$groupment->project->technical->company = self::createObject(Models\Company::class, self::extractData($object, [
				'CSTECH_ID'      => 'ID_CRMSOCIETE',
				'CSTECH_SOCIETE' => 'CSOC_SOCIETE',
				'CSTECH_ADR'     => 'CSOC_ADR',
				'CSTECH_CP'      => 'CSOC_CP',
				'CSTECH_VILLE'   => 'CSOC_VILLE',
				'CSTECH_PAYS'    => 'CSOC_PAYS'
			]));
		}

		$deliveries = Tools::useColumnAsKey('id', DeliveryInactivityGroupment::fromSearchResult($object['MISSIONS'])->rows );
		foreach($groupment->deliveries as $deliveryPonderation){
			$deliveryPonderation->delivery = clone($deliveries[ $deliveryPonderation->delivery->id ]);
		}
		$groupment->project->deliveries = array_values($deliveries);

		$documents = [];
		foreach($object['DOCUMENTS'] as $row){
			$documents[] = self::createObject(Models\Document::class, $row);
		}
		$groupment->files = $documents;

		$groupment->costsSimulatedExcludingTax = 0;
		$charge = $groupment->numberOfDaysInvoicedOrQuantity;
		foreach($groupment->project->deliveries as $del) {
			/** @var Models\Delivery $del */
			$groupment->costsSimulatedExcludingTax += $del->costsSimulatedExcludingTax;
			$charge -= $del->numberOfDaysInvoicedOrQuantity;
		}
		if($charge < 0) $charge = 0;
		$groupment->costsSimulatedExcludingTax += $charge*$groupment->averageDailyCost;

		$groupment->turnoverSimulatedExcludingTax = ($groupment->numberOfDaysInvoicedOrQuantity + $groupment->numberOfDaysFree) * $groupment->averageDailyPriceExcludingTax + $groupment->additionalTurnoverExcludingTax;
		$groupment->marginSimulatedExcludingTax = $groupment->turnoverSimulatedExcludingTax - $groupment->costsSimulatedExcludingTax;

		if(Services\BM::isProfitabilityCalculatingBasedOnMarginRate()) {
			$groupment->profitabilitySimulated = ($groupment->costsSimulatedExcludingTax) ? 100 * $groupment->marginSimulatedExcludingTax / $groupment->costsSimulatedExcludingTax : 0;
		} else {
			$groupment->profitabilitySimulated = ($groupment->turnoverSimulatedExcludingTax) ? 100 * $groupment->marginSimulatedExcludingTax / $groupment->turnoverSimulatedExcludingTax : 0;
		}

		return $groupment;
	}

	/**
	 * @param Models\Groupment $delivery
	 * @return array
	 */
	public static function toSQL($delivery) {

		$data = [
			'MISSION' => self::modelToDatabaseArray($delivery)
		];

		if($delivery->project) $data['MISSION']['ID_PROJET'] = $delivery->project->id;

		$ids = [];
		foreach($delivery->deliveries as $deliveryPonderation){
			$ids[] = $deliveryPonderation->delivery->id;
		}

		$data['ENFANTS'] = $ids;

		return $data;
	}
}
