<?php
/**
 * inactivity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Services\CurrentUser;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Tools;

/**
 * Class Inactivity
 * @package BoondManager\Databases\Mapper
 */
class Inactivity extends Mapper {

	static $numberOfWorkingDays = [];

	private static function getNumberOfWorkingDays($data) {
		if(!isset($data['GRPCONF_CALENDRIER'])) $data['GRPCONF_CALENDRIER'] = CurrentUser::instance()->getCalendar();

		$calendar = $data['GRPCONF_CALENDRIER'];
		$start = $data['MP_DEBUT'];
		$end = $data['MP_FIN'];

		if(!isset(self::$numberOfWorkingDays[$calendar][$start][$end]))
			self::$numberOfWorkingDays[$calendar][$start][$end] = Tools::getNumberOfWorkingDays($start, $end, $calendar);

		return self::$numberOfWorkingDays[$calendar][$start][$end];
	}

	private static function setOccupationRate($data) {
		$nbJrsOuvres = self::getNumberOfWorkingDays($data);
		return $nbJrsOuvres != 0 ? round(($data['MP_NBJRSFACTURE']) * 100 / $nbJrsOuvres, 0) : 0;
	}

	/**
	 * @param Model $data
	 * @return Models\Inactivity
	 */
	public static function fromRow($data) {
		/** @var Models\Inactivity $delivery */
		$delivery = self::createObject(Models\Delivery::class, $data);
		$delivery->occupationRate = self::setOccupationRate($data);

		$delivery->inactivityType = Tools::mapData($data['PRJ_TYPE'], Models\Inactivity::INACTIVITY_MAPPER);

		if($data['ITEM_TYPE'] == Models\Delivery::TYPE_RESOURCE) {
			$delivery->resource = self::createObject(Models\Employee::class, self::extractData($data, [
				'COMP_IDPROFIL' => 'ID_PROFIL',
				'COMP_NOM'      => 'PROFIL_NOM',
				'COMP_PRENOM'   => 'PROFIL_PRENOM',
				'COMP_TYPE'     => 'PROFIL_TYPE'
			]));
		}

		return $delivery;
	}

	/**
	 * @param Model $object
	 * @return Models\Inactivity
	 */
	public static function fromSQL($object)
	{
		/** @var Models\Inactivity $delivery */
		$delivery = self::createObject(Models\Inactivity::class, $object);

		$delivery->inactivityType = Tools::mapData($object['PRJ_TYPE'], Models\Inactivity::INACTIVITY_MAPPER);;

		/** @var Models\Employee $employee*/
		$delivery->resource = $employee = self::createObject(Models\Employee::class, self::extractData($object, [
			'ID_ITEM' => 'ID_PROFIL',
			'PROFIL_NOM',
			'PROFIL_PRENOM',
			'PROFIL_EMAIL',
			'PROFIL_TYPE',
			'PROFIL_VISIBILITY',
			'ID_USER',
			'USER_TYPE'
		]));
		$employee->mainManager = Services\Managers::getBasic($object['ID_PROFIL_RESPMANAGER']);
		$employee->hrManager = Services\Managers::getBasic($object['ID_PROFIL_RESPRH']);
		$employee->agency = Services\Agencies::getBasic($object['ID_SOCIETE']); // should be extented with full config
		$employee->pole = Services\Poles::getBasic($object['ID_POLE']);

		$delivery->contracts = Contract::fromSearchResult($object['CONTRATS'])->rows;

		$documents = [];
		foreach($object['DOCUMENTS'] as $row){
			$documents[] = self::createObject(Models\Document::class, $row);
		}
		$delivery->files = $documents;

		$expenses = [];
		foreach($object['FRAISDETAILS'] as $row) {
			/** @var Models\ExpenseDetail $e */
			$expenses[] = $e = self::createObject(Models\ExpenseDetail::class, $row);
			$e->resetRelationships();
			$e->expenseType = new Models\ExpenseType([
				'reference' => $row['FRSDETAILS_TYPEFRSREF'],
				'name' => $row['TYPEFRS_NAME']
			]);
			$e->agency = Services\Agencies::getBasic($row['ID_SOCIETE']);
		}
		$delivery->expensesDetails = $expenses;

		$delivery->costsSimulatedExcludingTax = $delivery->numberOfDaysInvoicedOrQuantity * $delivery->averageDailyCost + $delivery->additionalCostsExcludingTax;

		return $delivery;
	}

	/**
	 * @param Models\Inactivity $delivery
	 * @return array
	 */
	public static function toSQL($delivery) {

		$data = [
			'MISSION' => self::modelToDatabaseArray($delivery)
		];

		$data['MISSION']['ID_ITEM'] = $delivery->resource->id;
		$data['MISSION']['ITEM_TYPE'] = Models\Delivery::TYPE_RESOURCE;
		$data['MISSION']['ID_PROJET'] = $delivery->project ? $delivery->project->id : 0;

		$data['FRAISDETAILS'] = [];
		foreach($delivery->expensesDetails as $expense) {
			$detail = self::modelToDatabaseArray($expense);
			$detail['FRSDETAILS_TYPEFRSREF'] = $expense->expenseType->reference;
			$detail['ID_SOCIETE'] = $expense->agency->id;
			$data['FRAISDETAILS'][] = $detail;
		}

		return $data;
	}
}
