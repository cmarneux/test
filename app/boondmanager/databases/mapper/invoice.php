<?php
/**
 * invoice.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Invoice
 * @package BoondManager\Databases\Mapper
 */
class Invoice extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Invoice
	 */
	public static function fromRow($data){
		/** @var Models\Invoice $entity */
		$entity = self::createObject(Models\Invoice::class, $data);

		if($entity) {
			if(!$entity->turnoverInvoicedExcludingTax) $entity->turnoverInvoicedExcludingTax = $entity->turnoverInvoicedIncludingTax = 0;
			$entity->order = self::createObject(Models\Order::class,self::extractData($data, [
				'ID_BONDECOMMANDE',
				'BDC_REFCLIENT',
				'BDC_REF',
			]));
			$entity->order->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));
			$entity->order->project = self::createObject(Models\Project::class, self::extractData($data, [
				'ID_PROJET',
				'PRJ_REFERENCE',
				'PRJ_TYPEREF',
				'PRJ_TYPE',
			]));
			$entity->order->project->opportunity = self::createObject(Models\Opportunity::class, self::extractData($data, [
				'ID_AO',
				'AO_REF',
				'AO_TITLE'
			]));
			$entity->order->project->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT',
				'CCON_PRENOM',
				'CCON_NOM',
			]));
			$entity->order->project->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE',
				'CSOC_SOCIETE',
			]));
			$entity->termOfPayment = self::createObject(Models\Schedule::class, self::extractData($data, [
				'ID_ECHEANCIER',
				'ECH_DATE',
				'ECH_DESCRIPTION',
			]));
		}

		return $entity;
	}

	/**
	 * Convert data from SQL to camelCase
	 * @param array|\ArrayAccess $data
	 * @param string $tab
	 * @return Models\Invoice
	 */
	public static function fromSQL($data, $tab = null) {
		/** @var Models\Invoice $invoice */
		$invoice = self::createObject(Models\Invoice::class, $data);

		/** @var Models\Order $order */
		$invoice->order = $order = self::createObject(Models\Order::class, $data);

		$order->mainManager = Services\Managers::getBasic($data['ID_RESPUSER']);

		/** @var Models\Project project */
		$order->project = $project = self::createObject(Models\Project::class, $data);

		$project->pole = Services\Poles::getBasic($data['ID_POLE']);
		$project->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		$project->opportunity = self::createObject(Models\Opportunity::class, $data);

		if($tab == Models\Invoice::TAB_INFORMATION)
			self::fromInformation($invoice, $data);

		return $invoice;
	}

	private static function fromInformation(Models\Invoice $invoice, $data) {
		$invoice->order->project->agency = self::createObject(Models\Agency::class, $data);
		$invoice->order->mainManager = self::createObject(Models\Employee::class, $data);

		$invoice->termOfPayment = self::createObject(Models\Schedule::class, $data);

		$invoice->order->project->contact = self::createObject(Models\Contact::class, $data);
		$invoice->order->project->company = self::createObject(Models\Company::class, $data);

		$invoice->order->bankDetail = self::createObject(Models\BankDetails::class, $data);
		$invoice->order->billingDetail = self::createObject(Models\ContactDetails::class, $data);

		$invoice->order->factor = self::createObject(Models\Company::class, self::extractData($data, [
			'ID_FACTOR' => 'ID_CRMSOCIETE',
			'FACTOR_SOCIETE' => 'CSOC_SOCIETE',
			'FACTOR_SIREN' => 'CSOC_SIREN',
			'FACTOR_NIC' => 'CSOC_NIC',
			'FACTOR_STATUT' => 'CSOC_STATUT',
			'FACTOR_RCS' => 'CSOC_RCS',
			'FACTOR_TVA' => 'CSOC_TVA',
			'FACTOR_ADR' => 'CSOC_ADR',
			'FACTOR_CP' => 'CSOC_CP',
			'FACTOR_VILLE' => 'CSOC_VILLE',
			'FACTOR_PAYS' => 'CSOC_PAYS',
			'FACTOR_NAF' => 'CSOC_NAF'
		]));

		$records = [];
		foreach($data['ITEMS'] as $item) {
			/** @var Models\InvoiceRecord $r */
			$records[] = $r = self::createObject(Models\InvoiceRecord::class, $item);
			$r->invoiceRecordType = self::createObject(Models\InvoiceRecordType::class, $item, $noId = true);
			$r->calculateTurnovers();
		}
		$invoice->invoiceRecords = $records;

		$corels = [];
		foreach($data['CORRELATIONS'] as $row) {
			if($row['CORBDC_TYPE'] == Models\OrderCorrelation::DBTYPE_DELIVERY) $corels[] = new Models\Delivery(['id' => $row['ID_ITEM']]);
			else $corels[] = new Models\Purchase(['id' => $row['ID_ITEM']]);
		}
		$invoice->order->deliveriesPurchases = $corels;
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Invoice $invoice
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL(Models\Invoice $invoice, $tab = null) {
		$data = ['PRODUIT' => Tools::reversePublicFieldsToPrivate($invoice->getPublicFieldsMapping(), $invoice->toArray())];
		$data['PRODUIT'] = array_merge($data['PRODUIT'], Tools::reversePublicFieldsToPrivate($invoice->getRelationshipMapping(), $invoice->getRelationshipsValues()));

		return $data;
	}
}
