<?php
/**
 * invoicerecordtype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Exceptions;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class InvoiceRecordType
 * @package BoondManager\Databases\Mapper
 */
class InvoiceRecordType extends Mapper {
    /**
     * Convert data from SQL to camelCase
     * @param Model $data
     * @return mixed
     * @throws Exceptions\DatabaseIntegrity
     */
    public static function fromSQL($data) {
        /** @var Models\InvoiceRecordType $invoicerecordtype */
        $invoicerecordtype = self::createObject(Models\InvoiceRecordType::class, $data);

        $wut = [];
        $et = [];
        $invoicerecordtype->schedule = false;
        $invoicerecordtype->reinvoicedPurchase = false;
        $invoicerecordtype->product = false;
        $tabTypeF = Tools::unserializeArray($data['TYPEF_CATEGORIES']);
        foreach($tabTypeF as $type) {
            list($typeItem, $reference, $agencyId) = explode('_', $type);
            switch($typeItem) {
                case 0:$wut[] = new Models\WorkUnitType(['reference' => $reference]);break;
                case 1:$et[] = new Models\ExpenseType(['reference' => $reference]);break;
                case 2:$invoicerecordtype->schedule = true;break;
                case 3:$invoicerecordtype->reinvoicedPurchase = true;break;
                case 4:$invoicerecordtype->product = true;break;
            }
        }
        $invoicerecordtype->workUnitTypes = $wut;
        $invoicerecordtype->expenseTypes = $et;
        return $invoicerecordtype;
    }

    /**
     * Convert entity's data from camelCase to SQL
     * @param Models\InvoiceRecordType $invoicerecordtype
     * @param Models\Agency $agency
     * @return array
     */
    public static function toSQL(Models\InvoiceRecordType $invoicerecordtype, Models\Agency $agency) {
        $data = self::modelToDatabaseArray($invoicerecordtype);

        $data['TYPEF_CATEGORIES'] = [];
        foreach($invoicerecordtype->workUnitTypes as $wut)
            $data['TYPEF_CATEGORIES'][] = implode('_', [
                0,
                $wut->reference,
                $agency->id
            ]);

        foreach($invoicerecordtype->expenseTypes as $et)
            $data['TYPEF_CATEGORIES'][] = implode('_', [
                1,
                $et->reference,
                $agency->id
            ]);

        if($invoicerecordtype->schedule) $data['TYPEF_CATEGORIES'][] = 2;
        if($invoicerecordtype->reinvoicedPurchase) $data['TYPEF_CATEGORIES'][] = 3;
        if($invoicerecordtype->product) $data['TYPEF_CATEGORIES'][] = 4;

        return $data;
    }
}
