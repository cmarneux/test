<?php
/**
 * manager.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services;

class Manager extends Mapper{
	/**
	 * @param array|\ArrayAccess $object
	 * @return Models\Account
	 */
	public static function fromSQL($object)
	{

		/** @var Models\Account $manager */
		$manager = self::createObject(Models\Account::class, $object);

		if(isset($object['ID_RESPMANAGER'])){
			$manager->mainManager = Services\Managers::getBasicFromUser($object['ID_RESPMANAGER']);
		}

		return $manager;
	}
}
