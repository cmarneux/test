<?php
/**
 * opportunity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\Model;

/**
 * Class Opportunity
 * @package BoondManager\Databases\Mapper
 */
class Opportunity extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Opportunity
	 */
	public static function fromRow($data){
		/** @var Models\Opportunity $entity */
		$entity = self::createObject(Models\Opportunity::class, $data);

		$data = $data->toArray();

		if($entity) {
			if($entity->startASAP) $entity->startDate = date('Y-m-d');

			$entity->calculateTurnoverWeightedExcludingTax();

			$entity->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT',
				'CCON_NOM',
				'CCON_PRENOM',
			]));
			$entity->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE',
				'CSOC_SOCIETE',
			]));

			$entity->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));

			$entity->contact->mainManager = Services\Managers::getBasic($data['CRMRESP_IDPROFIL']);
			$entity->contact->agency = Services\Agencies::getBasic($data['CRMRESP_IDSOCIETE']);
			$entity->contact->pole = Services\Poles::getBasic($data['CRMRESP_IDPOLE']);
		}

		return $entity;
	}

	/**
	 * @param Model $data
	 * @param string $tab
	 * @return Models\Opportunity
	 */
	public static function fromSQL($data, $tab = Models\Opportunity::TAB_DEFAULT) {
		/** @var Models\Opportunity $opportunity */
		$opportunity = self::createObject(Models\Opportunity::class, $data);

		$opportunity->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);
		$opportunity->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		$opportunity->pole = Services\Poles::getBasic($data['ID_POLE']);

		$opportunity->contact = self::createObject(Models\Contact::class, $data);
		$opportunity->company = self::createObject(Models\Company::class, $data);

		switch ($tab){
			case Models\Opportunity::TAB_INFORMATION:
				self::fromInformation($opportunity, $data);
				break;
			case Models\Opportunity::TAB_POSITIONINGS:
				self::fromPositionings($opportunity, $data);
				break;
		}

		return $opportunity;
	}

	private static function fromInformation(Models\Opportunity $opportunity, $data) {
		$documents = [];

		$opportunity->calculateTurnoverWeightedExcludingTax();

		$opportunity->origin = new Models\OriginOrSource([
			'typeOf' => $data['AO_TYPESOURCE'],
			'detail' => $data['AO_SOURCE']
		]);

		if($data['CRMRESP_IDPROFIL']) {
			$opportunity->contact->mainManager = Services\Managers::getBasic($data['CRMRESP_IDPROFIL']);
			$opportunity->contact->pole = Services\Poles::getBasic($data['CRMRESP_IDPOLE']);
			$opportunity->contact->agency = Services\Poles::getBasic($data['CRMRESP_IDSOCIETE']);;
		}

		foreach($data['DOCUMENTS'] as $rows) {
			$documents[] = self::createObject(Models\Document::class, $rows);
		}

		$opportunity->files = $documents;
	}

	private static function fromPositionings(Models\Opportunity $opportunity, $data){
		$details = [];
		foreach($data['POSITIONDETAILS'] as $row) {
			$details[] = self::createObject(Models\PositioningDetail::class, $row);
		}
		$opportunity->additionalTurnoverAndCosts = $details;
	}
}
