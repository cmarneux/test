<?php
/**
 * order.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Databases\Local;
use BoondManager\Models;
use BoondManager\Services;
use BoondManager\Services\Dictionary;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Order
 * @package BoondManager\Databases\Mapper
 */
class Order extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Order
	 */
	public static function fromRow($data){

		/** @var Models\Order $entity */
		$entity = self::createObject(Models\Order::class, $data);

		$entity->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		$entity->project = $project =  self::createObject(Models\Project::class, self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'PRJ_TYPEREF',
			'PRJ_TYPE',
			'PRJ_DEVISEAGENCE',
			'PRJ_CHANGEAGENCE',
			'PRJ_DEVISE',
			'PRJ_CHANGE',
		]));
		$project->opportunity = self::createObject(Models\Opportunity::class, self::extractData($data, [
			'ID_AO',
			'AO_TITLE'
		]));
		$project->contact       = self::createObject(Models\Contact::class, self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM']));
		$project->company       = self::createObject(Models\Company::class, self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE']));

		$invoiceStates =  Dictionary::getDictTranslatedValues('specific.setting.state.invoice');
		$states = [];
		foreach($invoiceStates as $invoiceID){
			$states[] = new Models\TurnoverState([
				'id' => $invoiceID,
				'turnoverExcludingTax' => $data['SUM_CA'.$invoiceID]
			]);
		}
		$entity->turnoverStates = $states;

		return $entity;
	}

	/**
	 * @param $data
	 * @return Models\Order[]
	 */
	public static function fromSearchMensualOrdersForProjectsOfAReourceOnPeriod($data)
	{
		$orders = [];
		foreach($data as $row){
			/** @var Models\Order $order */
			$orders[] = $order = self::createObject(Models\Order::class, $row);
			$order->mainManager = Services\Managers::getBasicFromUser($row['ID_RESPUSER']);
			$order->project = self::createObject(Models\Project::class, $row);
			$order->project->pole = Services\Poles::getBasic($row['ID_POLE']);
			$order->project->agency = Services\Agencies::getBasic($row['ID_SOCIETE']);
			$order->project->company = self::createObject(Models\Company::class, $row);
		}
		return $orders;
	}

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$order = new Models\Order();

		$order->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));

		$project =  new Models\Project(self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'PRJ_TYPEREF',
			'PRJ_TYPE',
			'PRJ_DEVISEAGENCE',
			'PRJ_CHANGEAGENCE',
			'PRJ_DEVISE',
			'PRJ_CHANGE',
		]));
		$project->groupFieldsAs('opportunity', new Models\Opportunity(self::extractData($data, ['ID_AO', 'AO_TITLE'])));
		$project->groupFieldsAs('contact', new Models\Contact(self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$project->groupFieldsAs('company', new Models\Company(self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));
		$order->groupFieldsAs('project', $project);

		$order->fromArray($data);

		return $order;
	}

	/**
	 * Convert data from SQL to camelCase
	 * @param array|\ArrayAccess $data
	 * @param string $tab
	 * @return Models\Order
	 */
	public static function fromSQL($data, $tab = null) {
		/** @var Models\Order $entity */
		$entity = self::createObject(Models\Order::class, $data);

		$entity->mainManager = Services\Managers::getBasic($data['ID_RESPUSER']);
		$entity->turnoverOrderedExcludingTax = $data['BDC_MONTANTHT'];

		$entity->project = self::createObject(Models\Project::class, $data);
		$entity->project->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		$entity->project->pole = Services\Poles::getBasic($data['ID_POLE']);

		switch ($tab) {
			case Models\Order::TAB_INFORMATION:
				self::fromInformation($entity, $data);
				break;
		}

		return $entity;
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function fromInformation(Models\Order $order, $data) {
		$order->bankDetail = self::createObject(Models\BankDetails::class, $data);
		$order->files = self::createObjectArray(Models\Document::class, $data['FILES']);
		$order->billingDetail = self::createObject(Models\ContactDetails::class, $data);

		$order->project->agency = self::createObject(Models\Agency::class, $data);
		$order->project->opportunity = self::createObject(Models\Opportunity::class, $data);

		$order->project->contact = self::createObject(Models\Contact::class, $data);
		$order->project->company = self::createObject(Models\Company::class, $data);

		if($order->project->company) {
			$details = [];
			foreach($data['FACT_COORDONNEES'] as $row) {
				$details[] = self::createObject(Models\ContactDetails::class, $row);
			}
			$order->project->company->billingDetails = $details;
		}

		$ribs = [];
		foreach ($data['RIBS'] as $row) {
			$ribs[] = self::createObject(Models\BankDetails::class, $row);
		}
		$order->project->agency->banksDetails = $ribs;

		$schedules = [];
		foreach($data['ECHEANCIER'] as $row) {
			$schedules[] = self::createObject(Models\Schedule::class, $row);
		}
		$order->schedules = $schedules;

		$purchases = [];
		foreach($data['VENTESACHATS'] as $row) {
			$p = self::createObject(Models\Purchase::class, $row);
			$purchases[$p->id] = $p;
		}
		$order->project->purchases = array_values($purchases);

		$deliveries = [];
		foreach ($data['MISSIONS'] as $row) {
			/** @var Models\Delivery $d */
			$d = self::createObject(Models\Delivery::class, $row);
			$deliveries[$d->id] = $d;
			if($row['ID_PROFIL']) $d->dependsOn = self::createObject(Models\Employee::class, $row);
			else $d->dependsOn = self::createObject(Models\Product::class, $row);
		}
		$order->project->deliveries = array_values($deliveries);

		// TODO $data['CORRELATIONS']
		$corels = [];
		foreach($data['CORRELATIONS'] as $row) {
			if($row['CORBDC_TYPE'] == Models\OrderCorrelation::DBTYPE_DELIVERY) $corels[] = $deliveries[$row['ID_ITEM']];
			else $corels[] = $purchases[$row['ID_ITEM']];
		}
		$order->deliveriesPurchases = $corels;
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function fromActions(Models\Order $order, $data) {
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function fromOpportunities(Models\Order $order, $data) {
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function fromProjects(Models\Order $order, $data) {
	}
	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Order $order
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL(Models\Order $order, $tab = null) {
		$data = ['PRODUIT' => Tools::reversePublicFieldsToPrivate($order->getPublicFieldsMapping(), $order->toArray())];
		$data['PRODUIT'] = array_merge($data['PRODUIT'], Tools::reversePublicFieldsToPrivate($order->getRelationshipMapping(), $order->getRelationshipsValues()));

		switch($tab) {
			case Models\Order::TAB_INFORMATION:
				self::toInformation($order, $data);
				break;
			case Models\Order::TAB_ACTIONS:
				self::toActions($order, $data);
				break;
			case Models\Order::TAB_OPPORTUNITIES:
				self::toOpportunities($order, $data);
				break;
			case Models\Order::TAB_PROJECTS:
				self::toProjects($order, $data);
				break;
		}

		return $data;
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function toInformation(Models\Order $order, &$data) {
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function toActions(Models\Order $order, &$data) {
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function toOpportunities(Models\Order $order, &$data) {
	}

	/**
	 * @param Models\Order $order
	 * @param $data
	 */
	private static function toProjects(Models\Order $order, &$data) {
	}

}
