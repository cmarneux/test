<?php
/**
 * payment.php
 * @author Tanguy Lambert <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Payment
 * @package BoondManager\Databases\Mapper
 */
class Payment extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Payment
	 */
	public static function fromRow($data, $tab = null) {
		/**
		 * @var Models\Payment $entity
		 */
		$entity = self::createObject(Models\Payment::class, $data);

		if ($entity) {
			$entity->purchase = self::createObject(Models\Purchase::class, $data);

			$entity->purchase->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));
			$entity->purchase->project = self::createObject(Models\Project::class, self::extractData($data, [
				'ID_PROJET',
				'PRJ_REFERENCE',
			]));
			$entity->purchase->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT',
				'CCON_NOM',
				'CCON_PRENOM',
			]));
			$entity->purchase->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE',
				'CSOC_SOCIETE',
			]));
		}

		return $entity;
	}

	/**
	 * @param Model $data
	 * @return Models\Payment
	 */
	public static function fromSQL($data) {

		/** @var Models\Payment $entity */
		$entity = self::createObject(Models\Payment::class, $data);

		if($data['FILES']){
			$files = [];
			foreach($data['FILES'] as $data){
				$files[] = new Models\File([
					'id'   => $data['ID_JUSTIFICATIF'],
					'name' => $data['FILE_NAME']
				]);
			}
			$entity->files = $files;
		}

		$entity->purchase = self::createObject(Models\Purchase::class, $data);

		$entity->purchase->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);
		$entity->purchase->pole        = Services\Poles::getBasic($data['ID_POLE']);
		$entity->purchase->agency      = Services\Agencies::getBasic($data['ID_SOCIETE']);

		$entity->purchase->project = self::createObject(Models\Project::class, self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
		]));

		$entity->purchase->delivery = self::createObject(Models\Delivery::class, self::extractData($data, [
			'ID_MISSIONPROJET',
		]));

		$entity->purchase->contact = self::createObject(Models\Contact::class, self::extractData($data, [
			'ID_CRMCONTACT',
			'CCON_NOM',
			'CCON_PRENOM',
		]));
		$entity->purchase->company = self::createObject(Models\Company::class, self::extractData($data, [
			'ID_CRMSOCIETE',
			'CSOC_SOCIETE',
		]));

		if(isset($data['activityDetails'])){
			$entity->activityDetails = $data['activityDetails'];
		}

		return $entity;
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Payment $payment
	 * @return array
	 */
	public static function toSQL(Models\Payment $payment) {
		$data = Tools::reversePublicFieldsToPrivate($payment->getPublicFieldsMapping(), $payment->toArray());
		$data = array_merge($data, Tools::reversePublicFieldsToPrivate($payment->getRelationshipMapping(), $payment->getRelationshipsValues()));

		return $data;
	}
}
