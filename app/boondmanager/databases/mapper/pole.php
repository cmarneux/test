<?php
/**
 * pole.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class Pole
 * @package BoondManager\Databases\Mapper
 */
class Pole extends Mapper {
	/**
	 * Convert data from SQL to camelCase
	 * @param Model $object
	 * @return Models\Pole
	 */
	public static function fromSQL($object) {
		/** @var Models\Pole $pole */
		$pole = self::createObject(Models\Pole::class, $object);
		return $pole;
	}

	/**
	 * Convert entity's data from camelCase to SQL
	 * @param Models\Pole $pole
	 * @return array
	 */
	public static function toSQL(Models\Pole $pole) {
		return Tools::reversePublicFieldsToPrivate($pole->getPublicFieldsMapping(), $pole->toArray());
	}

}
