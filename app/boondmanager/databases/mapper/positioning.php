<?php
/**
 * searchpositioning.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Services;
use BoondManager\Models;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

class Positioning extends Mapper{
	/**
	 * @param Model $data
	 * @return Models\Positioning
	 */
	public static function fromRow($data) {
		$contextId = Tools::getContext()->startTiming('positioning mapping', $group = true);

		$data = $data->toArray();

		/** @var Models\Positioning $positioning */
		$positioning = self::createObject(Models\Positioning::class, $data);

		if($data['ID_RESPPROFIL'])
			$positioning->mainManager = Services\Managers::getBasic($data['ID_RESPPROFIL']);

		/** @var Models\Opportunity $opportunity */
		$opportunity = self::createObject(Models\Opportunity::class , self::extractData($data, [
			'ID_AO',
			'AO_TITLE',
			'AO_REF',
			'AO_TYPE',
			'AO_TYPEREF',
			'AO_VISIBILITE',
			'AO_ETAT'
		]));

		if($data['AO_IDPROFIL'])
			$opportunity->mainManager = Services\Managers::getBasic($data['AO_IDPROFIL']);

		if($data['ID_CRMCONTACT']) {
			$opportunity->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'
			]));
			if($data['CRMRESP_IDPROFIL']) $opportunity->contact->mainManager = new Models\Account(['id' => $data['CRMRESP_IDPROFIL']]);
			if($data['CRMRESP_IDSOCIETE']) $opportunity->contact->agency = new Models\Agency(['id' => $data['CRMRESP_IDSOCIETE']]);
			if($data['CRMRESP_IDPOLE']) $opportunity->contact->pole = new Models\Pole(['id' => $data['CRMRESP_IDPOLE']]);
		}

		if($data['ID_CRMSOCIETE']) {
			$opportunity->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE', 'CSOC_SOCIETE'
			]));
		}

		$positioning->opportunity = $opportunity;

		if($data['ID_PRODUIT']) {
			$positioning->dependsOn = self::createObject(Models\Product::class, self::extractData($data, [
				'ID_ITEM' => 'ID_PRODUIT',
				'PRODUIT_NOM',
				'PRODUIT_REF',
				'PRODUIT_TYPE'
			]));
		} else {
			$entityData = self::extractData($data, [
				'ID_ITEM'     => 'ID_PROFIL',
				'COMP_NOM'    => 'PROFIL_NOM',
				'COMP_PRENOM' => 'PROFIL_PRENOM',
				'COMP_TYPE'   => 'PROFIL_TYPE',
			]);
			$positioning->dependsOn = self::createObject(
				$entityData['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE ? Models\Candidate::class : Models\Employee::class,
				$entityData
			);
		}

		if($data['COMP_RESPMANAGER_ID']) {
			$entityManager = self::createObject(Models\Account::class, self::extractData($data, [
				'COMP_RESPMANAGER_ID'     => 'ID_PROFIL',
				'COMP_RESPMANAGER_NOM'    => 'PROFIL_NOM',
				'COMP_RESPMANAGER_PRENOM' => 'PROFIL_PRENOM',
			]));
			$positioning->dependsOn->mainManager = $entityManager;
		}
		if($data['COMP_RESPRH_ID']) {
			$entityManager = self::createObject(Models\Account::class, self::extractData($data, [
				'COMP_RESPRH_ID'     => 'ID_PROFIL',
				'COMP_RESPRH_NOM'    => 'PROFIL_NOM',
				'COMP_RESPRH_PRENOM' => 'PROFIL_PRENOM',
			]));
			$positioning->dependsOn->hrManager = $entityManager;
		}

		Tools::getContext()->endTiming($contextId);

		return $positioning;
	}

	/**
	 * @param Model $data
	 * @return Models\Positioning
	 */
	public static function fromSQL($data)
	{
		//var_dump($data->toArray());
		/** @var Models\Positioning $positioning */
		$positioning = self::createObject(Models\Positioning::class, $data);

		$positioning->mainManager = Services\Managers::getBasic($data['ID_RESPPROFIL']);
		$positioning->project = self::createObject(Models\Project::class, $data);

		/** @var Models\Opportunity $opportunity */
		$positioning->opportunity = $opportunity = self::createObject(Models\Opportunity::class , $data);

		$opportunity->mainManager = Services\Managers::getBasic($data['AO_IDPROFIL']);
		$opportunity->agency = Services\Agencies::getBasic($data['AO_IDSOCIETE']);
		$opportunity->pole = Services\Poles::getBasic($data['AO_IDPOLE']);


		if($data['ID_CRMCONTACT']) {
			$opportunity->contact = self::createObject(Models\Contact::class, self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'
			]));
		}

		if($data['ID_CRMSOCIETE']) {
			$opportunity->company = self::createObject(Models\Company::class, self::extractData($data, [
				'ID_CRMSOCIETE', 'CSOC_SOCIETE'
			]));
		}

		if($data['ID_PRODUIT']) {
			$positioning->dependsOn = self::createObject(Models\Product::class, self::extractData($data, [
				'ID_ITEM' => 'ID_PRODUIT',
				'PRODUIT_NOM',
				'PRODUIT_REF',
				'PRODUIT_TYPE',
				'PRODUIT_TAUXTVA',
				'PRODUIT_TARIFHT'
			]));
		} else {
			$entityData = self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
				'PROFIL_TYPE',
			]);
			$positioning->dependsOn = self::createObject(
				$entityData['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE ? Models\Candidate::class : Models\Employee::class,
				$entityData
			);

			$positioning->dependsOn->mainManager = Services\Managers::getBasic($data['ID_RESP']);
			$positioning->dependsOn->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
			$positioning->dependsOn->pole = Services\Poles::getBasic($data['ID_POLE']);

			if($positioning->dependsOn instanceof Models\Employee) {
				$positioning->dependsOn->hrManager = Services\Managers::getBasic($data['ID_RESPRH']);
			}
		}

		$files = [];
		foreach($data['DOCUMENTS'] as $doc) {
			$files[] = self::createObject(Models\Document::class, $doc);
		}
		$positioning->files = $files;

		$details = [];
		foreach($data['POSITIONDETAILS'] as $pos) {
			$details[] = self::createObject(Models\PositioningDetail::class, $pos);
		}
		$positioning->additionalTurnoverAndCosts = $details;

		return $positioning;
	}
}
