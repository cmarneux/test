<?php
/**
 * product.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Databases\Local;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class Product
 * @package BoondManager\Databases\Mapper
 */
class Product extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Product
	 */
	public static function fromRow($data){
		$entity = self::createObject(Models\Product::class, $data);

		if($entity) {
			$entity->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));
		}

		return $entity;
	}

	/**
	 * Convert data from SQL to camelCase
	 * @param array|\ArrayAccess $data
	 * @param string $tab
	 * @return Models\Product
	 */
	public static function fromSQL($data, $tab = null) {
		$entity = self::createObject(Models\Product::class, $data);

		if($entity) {
			$mainManager = (new Local\Manager())->getBasicManager($data['ID_PROFIL']);
			if ($mainManager) $entity->mainManager = self::createObject(Models\Employee::class, $mainManager);

			//On ajout aux données de l'usage celles fournis par data
			$agency = (new Local\Agency())->getBasicAgency($data['ID_SOCIETE']);
			if ($agency) $entity->agency = self::createObject(Models\Agency::class, $agency);

			$pole = (new Local\Pole())->getBasicPole($data['ID_POLE']);
			if ($pole) $entity->pole = self::createObject(Models\Pole::class, $pole);

			switch ($tab) {
				case Models\Product::TAB_INFORMATION:
					self::fromInformation($entity, $data);
					break;
				case Models\Product::TAB_ACTIONS:
					self::fromActions($entity, $data);
					break;
				case Models\Product::TAB_OPPORTUNITIES:
					self::fromOpportunities($entity, $data);
					break;
				case Models\Product::TAB_PROJECTS:
					self::fromProjects($entity, $data);
					break;
				case Models\Product::TAB_DEFAULT:
					self::fromDefault($entity, $data);
					break;
			}
		}
		return $entity;
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function fromInformation(Models\Product $product, $data) {
		$product->files = self::createObjectArray(Models\Document::class, $data['DOCUMENTS']);
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function fromActions(Models\Product $product, $data) {
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function fromOpportunities(Models\Product $product, $data) {
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function fromProjects(Models\Product $product, $data) {
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function fromDefault(Models\Product $product, $data) {
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Product $product
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL(Models\Product $product, $tab = null) {
		$data = ['PRODUIT' => self::modelToDatabaseArray($product)];

		$data['PRODUIT']['ID_PROFIL'] = $product->mainManager->id;
		$data['PRODUIT']['ID_SOCIETE'] = $product->agency->id;
		$data['PRODUIT']['ID_POLE'] = $product->pole ? $product->pole->id : 0;

		switch($tab) {
			case Models\Product::TAB_INFORMATION:
				self::toInformation($product, $data);
				break;
			case Models\Product::TAB_ACTIONS:
				self::toActions($product, $data);
				break;
			case Models\Product::TAB_OPPORTUNITIES:
				self::toOpportunities($product, $data);
				break;
			case Models\Product::TAB_PROJECTS:
				self::toProjects($product, $data);
				break;
		}

		return $data;
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function toInformation(Models\Product $product, &$data) {
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function toActions(Models\Product $product, &$data) {
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function toOpportunities(Models\Product $product, &$data) {
	}

	/**
	 * @param Models\Product $product
	 * @param $data
	 */
	private static function toProjects(Models\Product $product, &$data) {
	}

}
