<?php
/**
 * project.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Services;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Models;
use BoondManager\Databases\Local;

/**
 * Class Project
 * @package BoondManager\Databases\Mapper
 */
class Project extends Mapper {
	/**
	 * Convert searchresult's row from SQL to camelCase
	 * @param Model $data
	 * @return Models\Project
	 */
	public static function fromRow($data) {
		$project = self::createObject(Models\Project::class, $data);

		$project->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);

		$project->opportunity = self::createObject(Models\Opportunity::class, $data);
		$project->contact = self::createObject(Models\Contact::class, $data);
		$project->company = self::createObject(Models\Company::class, $data);

		return $project;
	}

	/**
	 * Convert entity's data from SQL to camelCase
	 * @param Model $data
	 * @param string $tab
	 * @return Models\Project
	 */
	public static function fromSQL($data, $tab = BM::TAB_DEFAULT) {
		$project = self::fromDefault($data);

		switch($tab) {
			case Models\Project::TAB_INFORMATION:
				self::fromInformation($project, $data);
				break;
			case Models\Project::TAB_BATCHES_MARKERS:
				self::fromBatchesMarkers($project, $data);
				break;
			case Models\Project::TAB_SIMULATION:
				self::fromSimulation($project, $data);
				break;
			case Models\Project::TAB_PRODUCTIVITY:
				self::fromProductivity($project, $data);
				break;
			case Models\Project::TAB_ORDERS:
				self::fromOrders($project, $data);
				break;
		}

		return $project;
	}

	/**
	 * @param Model $data
	 * @return Models\Project
	 */
	public static function fromDefault($data) {
		$project = self::createObject(Models\Project::class, $data);

		$project->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);
		$project->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
		$project->pole = Services\Poles::getBasic($data['ID_POLE']);

		$projectManagers = [];
		foreach(Tools::unserializeArray($data['ID_PROFILCDP']) as $managerId) $projectManagers[] = self::createObject(Models\Employee::class, ['ID_PROFIL' => $managerId]);
		$project->projectManagers = $projectManagers;

		return $project;
	}

	/**
	 * @param Models\Project $project
	 * @param Model $data
	 */
	private static function fromInformation(Models\Project $project, $data) {
		if($project->mode == BM::PROJECT_TYPE_RECRUITMENT) $project->clear('endDate');
		if(!in_array($project->mode, [BM::PROJECT_TYPE_PRODUCT, BM::PROJECT_TYPE_PACKAGE])) $project->clear('showBatchesMarkersTab');

		$project->opportunity = self::createObject(Models\Opportunity::class, $data);

		$project->contact = self::createObject(Models\Contact::class, $data);
		$project->company = self::createObject(Models\Company::class, $data);

		if($data['CCTECH_IDCRMCONTACT']) {
			$project->technical = self::createObject(Models\Contact::class, self::extractData($data, [
				'CCTECH_IDCRMCONTACT' => 'ID_CRMCONTACT',
				'CCTECH_NOM' => 'CCON_NOM',
				'CCTECH_PRENOM' => 'CCON_PRENOM'
			]));
			if($data['CCTECH_IDCRMSOCIETE'])
				$project->technical->company = self::createObject(Models\Company::class, self::extractData($data, [
					'CCTECH_IDCRMSOCIETE' => 'ID_CRMSOCIETE',
					'CSTECH_SOCIETE' => 'CSOC_SOCIETE'
				]));
		}

		if($data['CANDIDATE']) $project->candidate = Candidate::fromBasic($data['CANDIDATE']);

		$project->files = self::createObjectArray(Models\DocumentProject::class, $data['FILES']);
	}

	/**
	 * @param Models\Project $project
	 * @param Model $data
	 */
	private static function fromBatchesMarkers(Models\Project $project, $data) {
		if($project->mode == BM::PROJECT_TYPE_PRODUCT)
			$project->removeFields([
				'synchronizeRemainsToBeDone', 'allowCreateMarkersOnBatches', 'updateProductivityDate', 'remainsToBeDone'
			]);
		else {
			$project->agency->allowAlonesMarkers = $data['GRPCONF_PRJALLOWJALONS'];

			$project->resources = self::createObjectArray(Models\Employee::class, $data['RESOURCES']);

			if(is_null($data['PRJ_DATELOTUPDATE'])) $project->removeFields(['updateProductivityDate']);

			$turnoverSignedExcludingTax = $data['PRJ_TARIFADDITIONNEL'];
			foreach($data['RESOURCES'] as $resource) $turnoverSignedExcludingTax += $resource['CASIGNEDHT'];

			//batchesMarkers & aloneMarkers => cf. LOTS, JALONS, SUMTPS
			$aloneMarkers = [];
			foreach($data['JALONS'] as $marker) {
				if($marker->ID_LOT == 0) {
					$newMarker = self::createObject(Models\AloneMarker::class, [
						'ID_JALON' => $marker->ID_JALON,
						'JALON_TITRE' => $marker->JALON_TITRE,
						'JALON_VALUE' => $marker->JALON_VALUE,
						'JALON_DATE' => $marker->JALON_DATE
					]);
					$aloneMarkers[] = $newMarker;
				}
			}
			$project->aloneMarkers = $aloneMarkers;

			$batchesMarkers = [];
			foreach($data['LOTS'] as $batch) {
				$durationForecast = 0;
				$progressInDays = 0;
				$timesProduction = 0;

				$tabMarkers= [];
				foreach($data['JALONS'] as $marker) {
					if($marker->ID_LOT == $batch->ID_LOT) {
						$durationForecast += $marker->JALON_DUREE;
						$progressInDays += $marker->JALON_VALUE;

						/** @var Models\Marker $newMarker */
						$newMarker = self::createObject(Models\Marker::class, [
							'ID_JALON' => $marker->ID_JALON,
							'JALON_TITRE' => $marker->JALON_TITRE,
							'JALON_DATE' => $marker->JALON_DATE,
							'JALON_DUREE' => $marker->JALON_DUREE,
							'REMAINSTOBEDONE' => $marker->JALON_DUREE - $marker->JALON_VALUE,
							'PROGRESSRATE' => $marker->JALON_DUREE > 0 ? 100 * $marker->JALON_VALUE / $marker->JALON_DUREE : 0
						]);
						if($marker->ID_PROFIL) $newMarker->resource = self::createObject(Models\Employee::class, ['ID_PROFIL' => $marker->ID_PROFIL]);
						$tabMarkers[] = $newMarker;
					}
				}

				$tabResources= [];
				foreach($data['SUMTPS'] as $tps) {
					if($tps->ID_LOT == $batch->ID_LOT) {
						$timesProduction += $tps['DUREE'];
						$newResource = self::createObject(Models\Employee::class, [
							'ID_PROFIL' => $tps['ID_PROFIL'],
							'TIMESPRODUCTION' => $tps['DUREE']
						]);
						$tabResources[] = $newResource;
					}
				}

				$balance = $durationForecast - $timesProduction;
				$remainsToBeDone = $durationForecast - $progressInDays;
				$progressRate = 100;
				if(($timesProduction + $durationForecast - $progressInDays) > 0) {
					$progressRate = 100 * $timesProduction / ($timesProduction + $durationForecast - $progressInDays);
				}
				$turnoverProgressDistributedExcludingTax = $batch->LOT_VALUE / 100 * $turnoverSignedExcludingTax * $progressRate / 100;

				$newBatch = self::createObject(Models\BatchMarkers::class, [
					'ID_LOT' => $batch->ID_LOT,
					'LOT_TITRE' => $batch->LOT_TITRE,
					'LOT_VALUE' => $batch->LOT_VALUE,
					'TIMESPRODUCTION' => $timesProduction,
					'BALANCE' => $balance,
					'REMAINSTOBEDONE' => $remainsToBeDone,
					'DIFFERENCE' => $balance - $remainsToBeDone,
					'DURATIONFORECAST' => $durationForecast,
					'PROGRESSRATE' => $progressRate,
					'TURNOVERSIGNEDDISTRIBUTEDEXCLUDINGTAX' => $batch->LOT_VALUE / 100 * $turnoverSignedExcludingTax,
					'TURNOVERPROGRESSDISTRIBUTEDEXCLUDINGTAX' => $turnoverProgressDistributedExcludingTax,
					'MARKERS' => $tabMarkers,
					'RESOURCES' => $tabResources
				]);

				$batchesMarkers[] = $newBatch;
			}
			$project->batchesMarkers = $batchesMarkers;
		}
	}

	/**
	 * @param Models\Project $project
	 * @param Model $data
	 */
	private static function fromSimulation(Models\Project $project, $data) {
		$additionalTurnoverAndCosts = [];
		foreach($project->additionalTurnoverAndCosts as $deliveryDetail) {
			$addData = self::createObject(Models\DeliveryDetail::class, $deliveryDetail);
			if($deliveryDetail['ID_ACHAT']) $addData->purchase = self::createObject(Models\Purchase::class, $deliveryDetail);
			$additionalTurnoverAndCosts[] = $addData;
		}
		$project->additionalTurnoverAndCosts = $additionalTurnoverAndCosts;

		$project->resources = self::createObjectArray(Models\Employee::class, $data['RESOURCES']);
	}

	/**
	 * @param Models\Project $project
	 * @param Model $data
	 */
	private static function fromProductivity(Models\Project $project, $data) {
		$project->turnoverPurchasesAndAdditionnalExcludingTax = $data['CAACHATS'] + $data['CAADDITIONNEL'];
		$project->costsPurchasesAndAdditionnalExcludingTax = $data['COUTACHATS'] + $data['COUTADDITIONNEL'];

		$defaultProductivity = [
			'regularTimesProduction' => 0,
			'expensesProduction' => 0,
			'exceptionalTimesProduction' => 0,
			'exceptionalCalendarsProduction' => 0,
			'costsProductionExcludingTax' => 0,
			'turnoverProductionExcludingTax' => 0,
		];

		$tabEmployees = [];
		$tabProductivity = [];
		foreach(['SUMTPS', 'SUMFRS'] as $typeData) {
			foreach($data[$typeData] as $item) {
				if (!isset($tabProductivity[$item['ID_MISSIONPROJET']]))
					$tabProductivity[$item['ID_MISSIONPROJET']] = $defaultProductivity;

				if (!isset($tabEmployees[$item['ID_PROFIL']]))
					$tabEmployees[$item['ID_PROFIL']] = [];

				if (!in_array($item['ID_MISSIONPROJET'], $tabEmployees[$item['ID_PROFIL']]))
					$tabEmployees[$item['ID_PROFIL']][] = $item['ID_MISSIONPROJET'];

				$tabProductivity[$item['ID_MISSIONPROJET']]['id'] = $item['ID_MISSIONPROJET'];
				$tabProductivity[$item['ID_MISSIONPROJET']]['dependsOn'] = [
					'id' => $item['ID_PROFIL']
				];

				switch($typeData) {
					case 'SUMTPS':
						if(isset($item['TYPEH_TYPEACTIVITE']) && $item['TYPEH_TYPEACTIVITE'] == Models\Time::ACTIVITY_REGULAR_PRODUCTION)
							$tabProductivity[$item['ID_MISSIONPROJET']]['regularTimesProduction'] += $item['NBTEMPS'];

						if(!isset($item['TYPEH_TYPEACTIVITE']) || $item['TYPEH_TYPEACTIVITE'] == Models\Time::ACTIVITY_EXCEPTIONAL_TIME)
							$tabProductivity[$item['ID_MISSIONPROJET']]['exceptionalTimesProduction'] += $item['NBTEMPS']/3600;
						else if($item['TYPEH_TYPEACTIVITE'] == Models\Time::ACTIVITY_EXCEPTIONAL_CALENDAR)
							$tabProductivity[$item['ID_MISSIONPROJET']]['exceptionalCalendarsProduction'] += $item['NBTEMPS'];

						$tabProductivity[$item['ID_MISSIONPROJET']]['costsProductionExcludingTax'] += $item['COUTTEMPS'];
						$tabProductivity[$item['ID_MISSIONPROJET']]['turnoverProductionExcludingTax'] += $item['CATEMPS'];
						break;
					case 'SUMFRS':
						$tabProductivity[$item['ID_MISSIONPROJET']]['expensesProduction'] += $item['COUTFRAIS'];
						$tabProductivity[$item['ID_MISSIONPROJET']]['costsProductionExcludingTax'] += $item['COUTFRAIS'];
						$tabProductivity[$item['ID_MISSIONPROJET']]['turnoverProductionExcludingTax'] += $item['CAFRAIS'];
						break;
				}
			}
		}

		$tabDeliveries = [];
		foreach($tabProductivity as $id => $productivity) {
			foreach($data['COUTAVANTAGES'] as $costs) {
				if($productivity['dependsOn']['id'] == $costs['ID_PROFIL'])
					$tabProductivity[$id]['costsProductionExcludingTax'] += $costs['COUTAVANTAGES'] / sizeof($tabEmployees[$costs['ID_PROFIL']]);
			}
			$tabDeliveries[] = new Models\Delivery($tabProductivity[$id]);
		}
		$project->deliveries = $tabDeliveries;
	}

	/**
	 * @param Models\Project $project
	 * @param $data
	 */
	private static function fromOrders(Models\Project $project, $data) {

		$project->turnoverSignedExcludingTax = $data['CASIGNE'];
		$project->costsSignedExcludingTax = $data['COUTSIGNE'];
		$project->turnoverProductionExcludingTax = $data['CAPRODUCTION'];
		$project->costsProductionExcludingTax = $data['TOTAL_COUT'];

		$project->marginSignedExcludingTax = $project->turnoverSignedExcludingTax - $project->costsSignedExcludingTax;
		$project->profitabilitySigned = ($project->turnoverSignedExcludingTax) ? 100 * $project->marginSignedExcludingTax / $project->turnoverSignedExcludingTax : 0;
		$project->marginProductionExcludingTax = $project->turnoverProductionExcludingTax - $project->costsProductionExcludingTax;
		$project->profitabilityProduction = ($project->turnoverProductionExcludingTax) ? 100 * $project->marginProductionExcludingTax / $project->turnoverProductionExcludingTax : 0;

	}

	/**
	 * Convert entity's data from camelCase to SQL
	 * @param Models\Project $project
	 * @param string $tab
	 * @return array
	 */
	public static function toSQL(Models\Project $project, $tab = BM::TAB_DEFAULT) {
		$data = ['PROJET' => Tools::reversePublicFieldsToPrivate($project->getPublicFieldsMapping(), $project->toArray())];

		switch($tab) {
			case Models\Project::TAB_INFORMATION:
				self::toInformation($project, $data);
				break;
			case Models\Project::TAB_BATCHES_MARKERS:
				self::toBatchesMarkers($project, $data);
				break;
			case Models\Project::TAB_SIMULATION:
				self::toSimulation($project, $data);
				break;
		}

		return $data;
	}

	/**
	 * @param Models\Project $project
	 * @param array $data
	 */
	private static function toInformation(Models\Project $project, &$data) {
		$data['PROJET']['ID_PROFIL'] = $project->mainManager->id;
		$data['PROJET']['ID_SOCIETE'] = $project->agency->id;

		$data['PROJET']['ID_POLE'] = is_null($project->pole) ? 0 : $project->pole->id;
		if($project->opportunity) $data['PROJET']['ID_AO'] = $project->opportunity->id;
		$data['PROJET']['ID_CRMCONTACT'] = is_null($project->contact) ? 0 : $project->contact->id;
		$data['PROJET']['ID_CRMSOCIETE'] = is_null($project->company) ? 0 : $project->company->id;
		$data['PROJET']['ID_CRMTECHNIQUE'] = is_null($project->technical) ? 0 : $project->technical->id;

		$data['PROJET']['PRJ_TYPE'] = Dictionary::lookup('specific.setting.typeOf.project', $project->typeOf, null, 'id', 'mode');

		//Appeler buildNewReference si la référence est mis à jour à une chaîne vide
		if($project->reference == '') $data['PROJET']['PRJ_REFERENCE'] = self::buildNewReference($project, false, isset($data['PROJET']['PRJ_DATE']) ? $data['PROJET']['PRJ_DATE'] : null);
	}

	/**
	 * @param Models\Project $project
	 * @param array $data
	 */
	private static function toBatchesMarkers(Models\Project $project, &$data) {
		$data['JALONSUNIQUE'] = [];
		foreach($project->aloneMarkers as $marker) {
			$newMarker = [];
			if(isset($marker['id'])) $newMarker['ID_JALON'] = $marker['id'];
			$newMarker['JALON_TITRE'] = $marker['title'];
			$newMarker['JALON_DATE'] = $marker['date'];
			$newMarker['JALON_DUREE'] = 0;
			$newMarker['JALON_VALUE'] = $marker['progressRate'];
			$newMarker['ID_PROFIL'] = 0;
			$newMarker['ID_LOT'] = 0;
			$data['JALONSUNIQUE'][] = $newMarker;
		}

		$data['LOTSJALONS'] = [];
		foreach($project->batchesMarkers as $batch) {
			$newBatch = [];
			if(isset($batch['id'])) $newBatch['ID_LOT'] = $batch['id'];
			if(isset($batch['title'])) $newBatch['LOT_TITRE'] = $batch['title'];
			if(isset($batch['distributionRate'])) $newBatch['LOT_VALUE'] = $batch['distributionRate'];

			$newBatch['JALONS'] = [];
			foreach($batch['markers'] as $marker) {
				$newMarker = [];
				if(isset($marker['id'])) $newMarker['ID_JALON'] = $marker['id'];
				$newMarker['JALON_TITRE'] = $marker['title'];
				$newMarker['JALON_DATE'] = $marker['date'];
				$newMarker['JALON_DUREE'] = $marker['durationForecast'];
				$newMarker['JALON_VALUE'] = $marker['durationForecast'] - $marker['remainsToBeDone'];
				$newMarker['ID_PROFIL'] = isset($marker['resource']) ? $marker['resource']['id'] : 0;

				$newBatch['JALONS'][] = $newMarker;
			}
			$data['LOTSJALONS'][] = $newBatch;
		}
	}

	/**
	 * @param Models\Project $project
	 * @param array $data
	 */
	private static function toSimulation(Models\Project $project, &$data) {
		$data['PROJET']['ID_PROFILCDP'] = [];
		foreach($project->projectManagers as $manager) $data['PROJET']['ID_PROFILCDP'][] = $manager->id;
		$data['PROJET']['ID_PROFILCDP'] = Tools::serializeArray($data['PROJET']['ID_PROFILCDP']);

		$data['MISSIONDETAILS'] = [];
		foreach($project->additionalTurnoverAndCosts as $additional) {
			$newAdditional = [];
			if(isset($additional['id'])) $newAdditional['ID_MISSIONDETAILS'] = $additional['id'];
			$newAdditional['MISDETAILS_DATE'] = $additional['date'];
			$newAdditional['MISDETAILS_ETAT'] = $additional['state'];
			$newAdditional['MISDETAILS_TITRE'] = $additional['title'];
			$newAdditional['MISDETAILS_TARIF'] = $additional['turnoverExcludingTax'];
			$newAdditional['MISDETAILS_INVESTISSEMENT'] = $additional['costsExcludingTax'];
			$data['MISSIONDETAILS'][] = $newAdditional;
		}
	}

	/**
	 * Construct a new reference for a project
	 * @param Models\Project $project
	 * @param boolean $onlymask if true then returns only reference's mask else concatanes reference's mask and start's number
	 * @param string $forceDate if defined then uses this date to build reference's mask else uses now date
	 * @return string
	 */
	public static function buildNewReference(Models\Project $project, $onlymask = false, $forceDate = null) {
		if(preg_match('/([a-zA-Z0-9\[\]_\-\/]{1,})/', $project->agency->projectsReferenceMask, $matchRef)) {
			if($forceDate != '' && preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $forceDate, $match)) {
				$forceDate = explode('-',$forceDate);
				$annee4 = $forceDate[0];
				$annee2 = substr($forceDate[0],2);
				$mois = $forceDate[1];
				$jour = $forceDate[2];
			} else {
				$annee4 = date('Y');
				$annee2 = date('y');
				$mois = date('m');
				$jour = date('d');
			}

			$trigramme_manager = Tools::buildTrigramFromContact($project->mainManager->firstName, $project->mainManager->lastName);

			$tabFind = array('[AAAA]','[AA]','[MM]','[JJ]','[ID_AO]','[ID_PRJ]','[REF_AO]','[CCC]','[RRR]');
			$tabReplace = array($annee4,$annee2,$mois,$jour,$project->opportunity->getID(),$project->getID(),$project->opportunity->reference,strtoupper(substr(Tools::suppr_accents($project->company->name),0,3)),$trigramme_manager);

			$maskSelect = str_replace($tabFind, $tabReplace, $matchRef[1]);

			$tabFind[] = '_';
			$tabFind[] = '/';
			$tabFind[] = '-';

			$tabReplace[] = '\_';
			$tabReplace[] = '\/';
			$tabReplace[] = '\-';

			$maskWhere = str_replace($tabFind, $tabReplace, $matchRef[1]);//Masque de la clause Where

			$maskMax = str_replace(array('/', '-'), array('\/', '\-'), $maskSelect);

			if(strpos($matchRef[1], '[ID_PRJ]') === false) {
				$dbProject = new Local\Project();
				$maxReference = $dbProject->calculateReferenceMax($maskSelect, $maskWhere, $project->getID());
			} else
				$maxReference = $maskSelect;

			if(preg_match('/'.$maskMax.'_([0-9]{1,})/', $maxReference, $matchMax))
				$startNumber = '_'.(intval($matchMax[1])+1); else $startNumber = '';
		} else {
			$maskSelect = $maskMax = $project->getReference();
			$startNumber = '';
		}

		if($onlymask) return $maskSelect; else return $maskSelect.$startNumber;
	}
}
