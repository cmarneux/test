<?php
/**
 * file.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;

class Proof extends Mapper{
	/**
	 * @param \Wish\Models\Model $data
	 * @return Models\Proof
	 */
	public static function fromSQL($data) {
		/** @var Models\Proof $file */
		$file = self::createObject(Models\Proof::class, $data);
		return $file;
	}
}
