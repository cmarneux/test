<?php
/**
 * purchase.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use BoondManager\Services;
use BoondManager\Models;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Purchase
 * @package BoondManager\Databases\Mapper
 */
class Purchase extends Mapper {

	/**
	 * @param Model $data
	 * @return Models\Purchase
	 */
	public static function fromRow($data){
		/** @var Models\Purchase $entity */
		$entity = self::createObject(Models\Purchase::class, $data);

		if($entity) {
			$entity->mainManager = self::createObject(Models\Employee::class, self::extractData($data, [
				'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]));

			if($data['ID_PROJET'])
				$entity->project = self::createObject(Models\Project::class, self::extractData($data, [
					'ID_PROJET',
					'PRJ_REFERENCE',
				]));

			if($data['ID_CRMCONTACT'])
				$entity->contact = self::createObject(Models\Contact::class, self::extractData($data, [
					'ID_CRMCONTACT',
					'CCON_NOM',
					'CCON_PRENOM',
				]));

			if($data['ID_CRMSOCIETE'])
				$entity->company = self::createObject(Models\Company::class, self::extractData($data, [
					'ID_CRMSOCIETE',
					'CSOC_SOCIETE',
				]));

			$entity->calculateData();
		}

		return $entity;
	}

	/**
	 * Convert data from SQL to camelCase
	 * @param array|\ArrayAccess $data
	 * @param string $tab
	 * @return Models\Purchase
	 */
	public static function fromSQL($data, $tab = null) {
		/** @var Models\Purchase $entity */
		$entity = self::createObject(Models\Purchase::class, $data);

		if($entity) {

			$entity->mainManager = Services\Managers::getBasic($data['ID_PROFIL']);
			$entity->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);
			$entity->pole = Services\Poles::getBasic($data['ID_POLE']);

			if ($data['ID_PROJET']) {
				$entity->project = self::createObject(Models\Project::class, $data);
			}

			if ($data['ID_CRMCONTACT']) {
				$entity->contact = self::createObject(Models\Contact::class, $data);
			}

			if ($data['ID_CRMSOCIETE']) {
				$entity->company = self::createObject(Models\Company::class, $data);
			}

			switch ($tab) {
				case Models\Purchase::TAB_INFORMATION:
					self::fromInformation($entity, $data);
					break;
				case Models\Purchase::TAB_ACTIONS:
					self::fromActions($entity, $data);
					break;
				case Models\Purchase::TAB_PAYMENTS:
					self::fromPayments($entity, $data);
					break;
				case Models\Purchase::TAB_DEFAULT:
					self::fromDefault($entity, $data);
					break;
			}

			$entity->calculateData();
		}
		return $entity;
	}

	/**
	 * @param Models\Purchase $purchase
	 * @param $data
	 */
	private static function fromInformation(Models\Purchase $purchase, $data) {

		if($data['ID_MISSIONPROJET']) $purchase->delivery = self::createObject(Models\Delivery::class, $data);


		if($data['FACT_COORDONNEES']){
			$purchase->company->billingDetails = self::createObject(Models\ContactDetails::class, $data);
		}

		$purchase->files = self::createObjectArray(Models\Document::class, $data['DOCUMENTS']);
	}

	/**
	 * @param Models\Purchase $purchase
	 * @param $data
	 */
	private static function fromActions(Models\Purchase $purchase, $data) {
	}

	/**
	 * @param Models\Purchase $purchase
	 * @param $data
	 */
	private static function fromPayments(Models\Purchase $purchase, $data) {
	}

	/**
	 * @param Models\Purchase $purchase
	 * @param $data
	 */
	private static function fromDefault(Models\Purchase $purchase, $data) {
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\Purchase $purchase
	 * @return array
	 */
	public static function toSQL(Models\Purchase $purchase) {
		$data = [
			'ACHAT' => self::modelToDatabaseArray($purchase)
		];

		$data['ACHAT']['ID_PROFIL'] = $purchase->mainManager->id;
		$data['ACHAT']['ID_SOCIETE'] = $purchase->agency->id;
		$data['ACHAT']['ID_POLE'] = $purchase->pole ? $ $purchase->pole->id : 0;

		if($purchase->project) $data['ACHAT']['ID_PROJET'] = $purchase->project->id;
		if($purchase->company) $data['ACHAT']['ID_CRMCONTACT'] = $purchase->company->id;
		if($purchase->contact) $data['ACHAT']['ID_CRMSOCIETE'] = $purchase->contact->id;

		//$data['ACHAT']['ID_COORDONNEES'] = TODO

		if($purchase->delivery) {
			$data['MISSIONDETAILS'] = [
				'ID_MISSIONDETAILS' => $purchase->delivery->id,
				'MISDETAILS_DATE' => $data['ACHAT']['ACHAT_DATE'],
				'MISDETAILS_ETAT' => ($data['ACHAT']['ACHAT_ETAT'] == Models\Purchase::STATE_PLANNED) ? Models\Delivery::STATE_SIGNED : Models\Delivery::STATE_FORECAST, // TODO vérifier le sens du test
				'MISDETAILS_TITRE' => $data['ACHAT']['ACHAT_TITLE'],
				'MISDETAILS_TARIF' => $data['ACHAT']['ACHAT_TARIFFACTURE'],
				'MISDETAILS_INVESTISSEMENT' => $data['ACHAT']['ACHAT_MONTANTHT'] * $data['ACHAT']['ACHAT_QUANTITE']
			];
		}

		return $data;
	}
}
