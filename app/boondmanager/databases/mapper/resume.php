<?php
/**
 * file.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;

class Resume extends Mapper{
	/**
	 * @param \Wish\Models\Model $data
	 * @return Models\Resume
	 */
	public static function fromSQL($data) {
		/** @var Models\Resume $file */
		$file = self::createObject(Models\Resume::class, $data);
		if($data['ID_PROFIL']) {
			if($data['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE) {
				$file->dependsOn = self::createObject(Models\Candidate::class, $data);
			} else {
				$file->dependsOn = self::createObject(Models\Employee::class, $data);
			}
		}
		return $file;
	}
}
