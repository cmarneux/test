<?php
/**
 * target.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use Wish\Mapper;
use BoondManager\Models;
use Wish\Models\Model;
use Wish\Tools;

class Target extends Mapper{
	/**
	 * @param Model[] $data
	 * @return Models\Target[]
	 */
	public static function getAllObjectifs($data){
		$targets = [];
		foreach($data as $row){
			$targets[] = self::buildTarget($row);
		}
		return $targets;
	}

	/**
	 * @param $data
	 * @return Models\Target
	 */
	private static function buildTarget($data){

		/** @var Models\Target $target */
		$target = self::createObject(Models\Target::class, $data);

		list($period, $year) = explode(' ', $data['OBJ_DATE']);
		if(!$period) $target->periodNumber = Models\Target::ALL_DATES;
		else if(!$year) {
			if(preg_match('#^[0-9]{4}$#', $period)) $target->periodYear = $period;
			else $target->periodNumber = substr($period, 1);
		}else {
			$target->periodYear = $year;
			$target->periodNumber = substr($period, 1);
		}

		list($indic, $sub) = explode('_', $data['OBJ_INDICATEUR']);
		switch ($target->category) {
			case Models\Target::CATEGORY_COMMERCIAL:
				$scoreCard = [
					'reference' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_COMMERCIAL_MAPPER, 'id', 'name')),
					'typeOf' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_COMMERCIAL_MAPPER, 'id', 'typeOf')),
				];
				break;
			case Models\Target::CATEGORY_ACTIVITY:
				$scoreCard = [
					'reference' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_ACTIVITY_MAPPER, 'id', 'name')),
					'typeOf' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_ACTIVITY_MAPPER, 'id', 'typeOf')),
				];
				break;
			case Models\Target::CATEGORY_BILLING:
				$scoreCard = [
					'reference' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_BILLING_MAPPER, 'id', 'name')),
					'typeOf' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_BILLING_MAPPER, 'id', 'typeOf')),
				];
				break;
			case Models\Target::CATEGORY_GLOBAL:
				$scoreCard = [
					'reference' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_GLOBAL_MAPPER, 'id', 'name')),
					'typeOf' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_GLOBAL_MAPPER, 'id', 'typeOf')),
				];
				break;
			case Models\Target::CATEGORY_RECRUITMENT:
				$scoreCard = [
					'reference' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_RECRUITMENT_MAPPER, 'id', 'name')),
					'typeOf' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_RECRUITMENT_MAPPER, 'id', 'typeOf')),
				];
				break;
			case Models\Target::CATEGORY_HR:
				$scoreCard = [
					'reference' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_HR_MAPPER, 'id', 'name')),
					'typeOf' => Tools::mapData($indic, Tools::buildMaping(Models\Target::INDICATOR_HR_MAPPER, 'id', 'typeOf')),
				];
				break;
			default:
				$scoreCard = [];
		}

		if($sub) {
			$scoreCard['subId'] = intval($sub);
		}

		$target->scorecard = $scoreCard;
		return $target;
	}

}
