<?php

namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\Model;

class TimesExpensesAbsences extends Mapper{

	public static function fromConfigTpsFrsGroupe($data)
	{
		$agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		$wut= [];
		foreach($data['LISTE_TYPEH'] as $row){
			$wut[] = self::createObject(Models\WorkUnitType::class, $row);
		}
		$agency->workUnitTypes = $wut;

		$tf= [];
		foreach($data['LISTE_TYPEF'] as $row){
			$tf[] = self::createObject(Models\ExpenseType::class, $row);
		}
		$agency->expenseTypes = $tf;

		$baremes= [];
		foreach($data['LISTE_BARKM'] as $row){
			$baremes[] = self::createObject(Models\RatePerKilometerType::class, $row);
		}
		$agency->ratePerKilometerTypes = $baremes;

		$decomptes= [];
		foreach($data['LISTE_DECOMPTE'] as $row){
			$decomptes[] = self::createObject(Models\AbsencesAccount::class, $row);
		}
		$agency->absencesQuotas = $decomptes;

		return $agency;
	}
}
