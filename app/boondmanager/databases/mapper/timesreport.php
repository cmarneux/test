<?php
/**
 * searchtimesreport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Models;
use BoondManager\Services;
use Wish\Mapper;
use Wish\Models\SearchResult;

class TimesReport extends Mapper{

	/**
	 * @param $data
	 * @return  Models\TimesReport
	 */
	public static function fromRow($data)
	{
		$timesReport = self::createObject(Models\TimesReport::class, self::extractData($data, [
			'ID_DOCUMENT' => 'ID_LISTETEMPS',
			'DOCUMENT_DATE' => 'LISTETEMPS_DATE',
			'DOCUMENT_ETAT' => 'LISTETEMPS_ETAT',
		]));

		$timesReport->resource = self::createObject(Models\Employee::class, self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_REFERENCE'
		]));

		$timesReport->agency = Services\Agencies::getBasic($data['ID_SOCIETE']);

		return $timesReport;
	}

	/**
	 * @param SearchResult $searchResult
	 * @return SearchResult
	 */
	public static function fromSearchResult($searchResult)
	{
		$reports = [];
		foreach($searchResult->rows as $data){
			// on extrait d'abord les données relatives au validateur s'il existe
			if(isset($data['RESP_ID_PROFIL']) && $data['RESP_ID_PROFIL']){
				$expectedValidator = self::createObject(Models\Employee::class, self::extractData($data, [
					'RESP_ID_PROFIL' => 'ID_PROFIL',
					'RESP_NOM'       => 'PROFIL_NOM',
					'RESP_PRENOM'    => 'PROFIL_PRENOM'
				]));

				/** @var Models\Validation $validation */
				$validation = self::createObject(Models\Validation::class, self::extractData($data, [
					'ID_VALIDATION',
					'VAL_DATE',
					'VAL_STATE' => 'VAL_ETAT'
				]));

				$validation->expectedValidator = $expectedValidator;
			}

			// on s'assure de travailler avec un tableau associatif (cle = chaine) pour maintenir l'ordre du search)
			$id = strval($data['ID_DOCUMENT']);

			if(!array_key_exists($id, $reports)){
				unset($validationsList);//~ Sinon les validations du dernier document sont ajoutée à tort à celui-ci
				$report = self::fromRow($data);
				$reports[$id] = $report;
			}else{
				$report = $reports[$id];
			}

			if(isset($validation)){
				if(!isset($validationsList)) $validationsList = [$validation];
				else $validationsList[] = $validation;
				$report->validations = $validationsList; //~ XXX : Tin : /!\ $report->validations[] = $validation ne fonctionne pas !!!
				unset($validation); //~ Sinon la dernière validation trouvée est ajoutée à tort sur le document suivant ...
			}
		}

		$searchResult->rows = array_values($reports);

		return $searchResult;
	}

	/**
	 * @param \Wish\Models\Model $data
	 * @return Models\TimesReport
	 */
	public static function fromSQL($data) {
		/** @var Models\TimesReport $timesReport */
		$timesReport = self::createObject(Models\TimesReport::class, $data);

		$timesReport->agency = self::createObject(Models\Agency::class, $data);
		$timesReport->resource = self::createObject(Models\Employee::class, $data);
		$timesReport->resource->pole = Services\Poles::getBasic($data['ID_POLE']);
		$timesReport->resource->mainManager = Services\Managers::getBasic($data['ID_PROFIL_MANAGER']);
		$timesReport->resource->hrManager = Services\Managers::getBasic($data['ID_PROFIL_RH']);

		$regularTimes = [];
		foreach ($data['LISTETEMPSNORMAUX'] as $row) {
			foreach($row['LISTEJOURS'] as $dailyEntry) {
				/** @var Models\Time $time */
				$regularTimes[] = $time = self::createObject(Models\Time::class, $dailyEntry);
				$time->resetRelationships();
				$time->workUnitType = self::createObject(Models\WorkUnitType::class, self::extractData($row, [
					'LIGNETEMPS_TYPEHREF' => 'TYPEH_REF',
					'TYPEH_NAME',
					'TYPEH_TYPEACTIVITE'
				]), $ignoreMissingId = true);
				$time->delivery = self::createObject(Models\Delivery::class, $row);
				$time->batch = self::createObject(Models\Batch::class, $row);
				if($time->project = self::createObject(Models\Project::class, $row) ) {
					$time->project->company = self::createObject(Models\Company::class, $row);
				}
			}
		}
		$timesReport->regularTimes = $regularTimes;

		$exceptionalTimes = [];
		foreach ( $data['LISTETEMPSEXCEPTION'] as $row) {
			/** @var Models\TimeExceptional $time */
			$exceptionalTimes[] = $time = self::createObject(Models\TimeExceptional::class, $row);
			$time->resetRelationships();
			$time->duration = date_diff( new \DateTime($time->startDate), new \DateTime($time->endDate), true)->days;
			$time->workUnitType = self::createObject(Models\WorkUnitType::class, self::extractData($row, [
				'TEMPSEXP_TYPEHREF' => 'TYPEH_REF',
				'TYPEH_NAME',
				'TYPEH_TYPEACTIVITE'
			]), $ignoreMissingId = true);
			$time->delivery = self::createObject(Models\Delivery::class, $row);
			$time->batch = self::createObject(Models\Batch::class, $row);
			if($time->project = self::createObject(Models\Project::class, $row) ) {
				$time->project->company = self::createObject(Models\Company::class, $row);
			}
		}
		$timesReport->exceptionalTimes = $exceptionalTimes;

		$validations = [];
		foreach($data['LISTEVALIDATIONS'] as $row) {
			/** @var Models\Validation $vals */
			$validations[] = $vals = self::createObject(Models\Validation::class, $row);
			$vals->realValidator = Services\Managers::getBasic($row['ID_VALIDATEUR']);
			$vals->expectedValidator = Services\Managers::getBasic($row['ID_PROFIL']);
		}
		$timesReport->validations = $validations;

		$files = [];
		foreach($data['JUSTIFICATIFS'] as $row){
			$files[] = self::createObject(Models\Proof::class, $row);
		}
		$timesReport->files = $files;

		if($data['ID_LISTEFRAIS']) $timesReport->expensesReport = new Models\ExpensesReport(['id' => $data['ID_LISTEFRAIS']]);

		return $timesReport;
	}

	/**
	 * @param Models\TimesReport $entity
	 * @return array
	 */
	public static function toSQL($entity)
	{
		$sqlData = [];
		$sqlData['TIMESHEET'] = self::modelToDatabaseArray($entity);

		$entity['TIMESHEET']['ID_PROFIL'] = $entity->resource->id;
		$entity['TIMESHEET']['ID_SOCIETE'] = $entity->agency->id;

		if($entity->exists('regularTimes')) {
			$timesEntries = [];
			foreach($entity->regularTimes as $time){
				if( ! ($timesEntry = $timesEntries[ $time['row'] ]) ){
					$timesEntry = [
						'ID_LIGNETEMPS'       => $time['row'],
						'LIGNETEMPS_TYPEHREF' => $time['workUnitType']['reference'],
						'ID_PROJET'           => $time['project']['id'],
						'ID_MISSIONPROJET'    => $time['delivery']['id'],
						'ID_LOT'              => $time['batch']['id'],
					];
				}

				$timesEntry['LISTEJOURS'][] = [ 'TEMPS_DATE' => $time['startDate'], 'TEMPS_DUREE' => $time['duration'] ];
				$timesEntries[ $time['row'] ] = $timesEntry;
			}
			$sqlData['LISTETEMPSNORMAUX'] = $timesEntries;
		}
		if($entity->exists('exceptionalTimes')) {
			$sqlData['LISTETEMPSEXCEPTION'] = [];
			foreach($entity->exceptionalTimes as $time){
				$timesEntry = [
					'ID_TEMPSEXCEPTION'     => $time['id'],
					'TEMPSEXP_TYPEHREF'     => $time['workUnitType']['reference'],
					'TEMPSEXP_DEBUT'        => $time['startDate'],
					'TEMPSEXP_FIN'          => $time['endDate'],
					'TEMPSEXP_DESCRIPTION'  => $time['description'],
					'TEMPSEXP_RECUPERATION' => $time['recovering'],
					'ID_PROJET'             => $time['project']['id'],
					'ID_MISSIONPROJET'      => $time['delivery']['id'],
					'ID_LOT'                => $time['batch']['id'],
				];
				$sqlData['LISTETEMPSEXCEPTION'][] = $timesEntry;
			}
		}

		return $sqlData;
	}
}
