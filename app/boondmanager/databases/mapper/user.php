<?php
/**
 * user.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Services\BM;
use Wish\Mapper;
use BoondManager\Models;
use Wish\Tools;

class User extends Mapper {
	/**
	 * @param \Wish\Models\Model $data
	 * @return Models\User
	 */
	public static function fromSQL($data)
	{
		/** @var Models\User $user */
		$user = self::createObject(Models\User::class, $data);

		if(isset($data['ID_PROFIL']) && $data['ID_PROFIL'] > 0) {
			$user->resource = self::createObject(Models\Employee::class, $data);
			$user->resource->agency = self::createObject(Models\Agency::class, $data);
		}

		$user->state = Tools::mapData($data['USER_ABONNEMENT'], BM::USER_ACCESS_MAPPING);

		$user->account = Tools::mapData($data['USER_TYPE'], BM::USER_TYPE_MAPPING);
		if($user->account == BM::USER_TYPE_ADMINISTRATOR && !BM::isCustomerInterfaceActive())
			$user->account = BM::USER_TYPE_ROOT;
		return $user;
	}

	/**
	 * Convert data from camelCase to SQL
	 * @param Models\User $user
	 * @return array
	 */
	public static function toSQL(Models\User $user) {
		$data = ['USER' => Tools::reversePublicFieldsToPrivate($user->getPublicFieldsMapping(), $user->toArray())];

		$user['USER']['USER_ABONNEMENT'] = Tools::reverseMapData($user->state, BM::USER_ACCESS_MAPPING);
		$user['USER']['USER_TYPE'] = Tools::reverseMapData($user->account, BM::USER_TYPE_MAPPING);

		$data['USER']['ID_ROLE'] = is_null($user->role->id) ? 0 : $user->role->id;
		$data['USER']['ID_PROFIL'] = $user->resource->id;

		if($user->resource) {
			$data['PROFIL'] = Tools::reversePublicFieldsToPrivate($user->resource->getPublicFieldsMapping(), $user->resource->toArray());
			if($user->resource->agency)
				$data['PROFIL']['ID_SOCIETE'] = $user->resource->agency->id;
		}

		$tabModules = [];
		if($user->modules)
			$tabModules = Tools::reversePublicFieldsToPrivate($user->modules->getPublicFieldsMapping(), $user->modules->toArray());

		$tabRights = [];
		if($user->rights) {
			if($user->rights->main)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->main->getPublicFieldsMapping(), $user->rights->main->toArray()));

			if($user->rights->resources)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->resources->getPublicFieldsMapping(), $user->rights->resources->toArray()));

			if($user->rights->opportunities)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->opportunities->getPublicFieldsMapping(), $user->rights->opportunities->toArray()));

			if($user->rights->projects)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->projects->getPublicFieldsMapping(), $user->rights->projects->toArray()));

			if($user->rights->candidates)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->candidates->getPublicFieldsMapping(), $user->rights->candidates->toArray()));

			if($user->rights->crm)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->crm->getPublicFieldsMapping(), $user->rights->crm->toArray()));

			if($user->rights->activityExpenses)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->activityExpenses->getPublicFieldsMapping(), $user->rights->activityExpenses->toArray()));

			if($user->rights->billing)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->billing->getPublicFieldsMapping(), $user->rights->billing->toArray()));

			if($user->rights->products)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->products->getPublicFieldsMapping(), $user->rights->products->toArray()));

			if($user->rights->purchases)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->purchases->getPublicFieldsMapping(), $user->rights->purchases->toArray()));

			if($user->rights->reporting)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->reporting->getPublicFieldsMapping(), $user->rights->reporting->toArray()));

			if($user->rights->actions)
				$tabRights = array_merge($tabRights, Tools::reversePublicFieldsToPrivate($user->rights->actions->getPublicFieldsMapping(), $user->rights->actions->toArray()));
		}

		if($tabModules || $tabRights)
			$data['CONFIGUSER'] = array_merge($tabModules, $tabRights);

		return $data;
	}
}
