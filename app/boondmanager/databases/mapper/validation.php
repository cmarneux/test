<?php
/**
 * validation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Databases\Mapper;

use BoondManager\Services\Managers;
use Wish\Mapper;
use BoondManager\Models;

class Validation extends Mapper{

	/**
	 * @param \Wish\Models\Model $object
	 * @return Models\Validation
	 */
	public static function fromSQL($object)
	{
		/** @var Models\Validation $validation*/
		$validation = self::createObject(Models\Validation::class, $object);
		$validation->realValidator = Managers::getBasic($object['ID_VALIDATEUR']);
		$validation->expectedValidator = Managers::getBasic($object['ID_PROFIL']);

		switch ($object['VAL_TYPE']) {
			case Models\Validation::TYPE_TIMESREPORT_BDD:
				$validation->dependsOn = new Models\TimesReport(['id' => $object['ID_PARENT']]);
				break;
			case Models\Validation::TYPE_EXPENSESREPORT_BDD:
				$validation->dependsOn = new Models\ExpensesReport(['id' => $object['ID_PARENT']]);
				break;
			case Models\Validation::TYPE_ABSENCESREPORT_BDD:
				$validation->dependsOn = new Models\AbsencesReport(['id' => $object['ID_PARENT']]);
				break;
		}

		return $validation;
	}

	/**
	 * @param Models\Validation $object
	 * @return array
	 * @throws \Exception
	 */
	public static function toSQL($object) {
		$data = self::modelToDatabaseArray($object);

		if($object->realValidator) $data['ID_VALIDATEUR'] = $object->realValidator->id;
		if($object->expectedValidator) $data['ID_PROFIL'] = $object->expectedValidator->id;

		if($object->dependsOn) {
			switch (get_class($object->dependsOn )) {
				case Models\TimesReport::class: $state = Models\Validation::TYPE_TIMESREPORT_BDD; break;
				case Models\ExpensesReport::class: $state = Models\Validation::TYPE_EXPENSESREPORT_BDD; break;
				case Models\AbsencesReport::class: $state = Models\Validation::TYPE_ABSENCESREPORT_BDD; break;
				default: throw new \Exception('dependsOn not handled');
			}
			$data['ID_PARENT'] = $object->dependsOn->id;
			$data['VAL_ETAT'] = $state;
		}

		return $data;
	}
}
