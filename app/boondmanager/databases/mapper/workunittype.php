<?php
/**
 * workunittype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Databases\Mapper;

use Wish\Exceptions;
use Wish\Mapper;
use Wish\Models\Model;
use BoondManager\Models;

/**
 * Class WorkUnitType
 * @package BoondManager\Databases\Mapper
 */
class WorkUnitType extends Mapper {
    /**
     * Convert data from SQL to camelCase
     * @param Model $data
     * @return mixed
     * @throws Exceptions\DatabaseIntegrity
     */
    public static function fromSQL($data) {
        /** @var Models\WorkUnitType $workunittype */
        $workunittype = self::createObject(Models\WorkUnitType::class, $data);

        switch($data['TYPEH_ETAT']) {
            case 0:
                $workunittype->state = false;
                $workunittype->warning = $workunittype->activityType == Models\WorkUnitType::ACTIVITY_ABSENCE ? true : false;
                break;
            case 1:
                $workunittype->state = true;
                $workunittype->warning = $workunittype->activityType == Models\WorkUnitType::ACTIVITY_ABSENCE ? true : false;
                break;
            case 2:
                $workunittype->state = false;
                $workunittype->warning = false;
                break;
            case 3:
                $workunittype->state = true;
                $workunittype->warning = false;
                break;
        }
        return $workunittype;
    }

    /**
     * Convert entity's data from camelCase to SQL
     * @param Models\WorkUnitType $workunittype
     * @return array
     */
    public static function toSQL(Models\WorkUnitType $workunittype) {
        $data = self::modelToDatabaseArray($workunittype);

        if($workunittype->activityType == Models\WorkUnitType::ACTIVITY_ABSENCE) {
            if($workunittype->warning)
                $data['TYPEH_ETAT'] = $workunittype->state ? 1 : 0;
            else
                $data['TYPEH_ETAT'] = $workunittype->state ? 3 : 2;
        } else
            $data['TYPEH_ETAT'] = $workunittype->state ? 1 : 0;

        return $data;
    }
}
