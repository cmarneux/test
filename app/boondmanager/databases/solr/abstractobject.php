<?php
/**
 * abstractobject.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Databases\SolR;

use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Managers;
use BoondManager\Services\BM;

/**
 * Class AbstractObject
 * @package BoondManager\Databases\SolR
 */
class AbstractObject extends \Prefab {
	/**
	 * parse the keywords and add a where clauses if a keyword has a known prefix
	 *
	 * @param array|string $keywords if it is a string, it will be transform to an array by performing an explode on space
	 * @param  array $liste_ref array containing the mapping PREFIX=>DATABASE_FIELD
	 * Example : `$liste_ref = ["COMP" => "TAB_PROFIL.ID_PROFIL"];`
	 *
	 * @return array of where clauses (format string)
	 */
	public function getListIdSearch($keywords, $liste_ref = array()) {
		$items = (is_array($keywords))?$keywords:explode(' ', $keywords);//On sépare la variable mot clefs de chaque espace
		$where = [];
		$result = [];
		foreach($items as $item) {
			foreach($liste_ref as $ref => $id_table) {
				if(preg_match("#^".$ref."([0-9]{1,})$#",strtoupper($item), $id_reference)) {
					$result[$ref]['solr'][] = $id_table.':'.$id_reference[1];
					break;
				}
			}
		}

		foreach($result as $ref)
			$where[] = '('.implode(' OR ', $ref['solr']).')';

		return $where;
	}

	/**
	 * associate a field and a value with the solr format
	 * //TODO ceci pourrait justifier la creation de l'objet Query Pour SolR
	 * @param string $value
	 * @param string $field
	 * @param boolean $not should it be prefixed by NOT. ex: "NOT ID_PROFIL:1"
	 * @return string
	 */
	public function addFieldValue($value, $field, $not = false) {return (($not)?'NOT ':'').$field.':'.$value;}

	/**
	 * associate a field with values.
	 *
	 * each item will be build with {@see self::addFieldValue() addFieldValue}.
	 *
	 * @param array $array values to associate with the field
	 * @param string $field SolR's field
	 * @param boolean $not
	 * @return array
	 */
	public function addMultiFieldsValue($array, $field, $not = false) {
		foreach($array as $i => $value) $array[$i] = $this->addFieldValue($value, $field, $not);
		return $array;
	}

	/**
	 * Build some where clauses based on the selected perimeter
	 *
	 * @param Perimeter $selectedPerimeter
	 * @param  string $colSociete specify the table field for the agency
	 * @param  string|array $colUser specify the table field(s) which can match for a user
	 * @param  boolean $allowMyAgency  tell if we can filter according to "My Perimeter":
	 * @param bool $narrowPerimeter if true all fields must match, if false any fields can match
	 * - `true` : If the filter "Perimeter" contain `0` the it will be filtered based on "My Perimeter" (All manager in the user hierarchy)
	 * @return string
	 */
	public function getPerimeterSearch(Perimeter $selectedPerimeter, $colSociete, $colPole, $colUser, $allowMyAgency=true, $narrowPerimeter=null) {
		//TODO: voir pour utiliser la classe Manager plutot que de refaire appel à la session
		//TODO: voir pour mettre le cas "buildWhere" dans une autre fonction
		//TODO: voir pour passer la méthode en protected
		//TODO: voir pour enlever $allowMyAgency => possibilite de le gerer des la construction du filtre ?

		$tabSocietes = [];
		$tabManagers = [];
		$tabPoles    = [];

		// TODO: voir pour le passer en param ?
		$user = CurrentUser::instance();

		if(count($selectedPerimeter))
		{
			//On construit le filtre de la société
			foreach($selectedPerimeter->getBUs() as $data) {
				foreach ($data->getSearchableManagers() as $manager) {
					$idManager = Managers::getUserIdFromEmployeeId($manager->id);
					$tabManagers[ strval($idManager) ] = $idManager;
				}
			}
			foreach ($selectedPerimeter->getPoles() as $data) {
				$tabPoles[] = $data->getID();
			}
			foreach ($selectedPerimeter->getAgencies() as $data) {
				if ($data->getID() == 0 && $allowMyAgency) {//Mon agence
					if (sizeof($user->getManagers()) == 0) {
						$idManager = Managers::getUserIdFromEmployeeId($user->getEmployeeId());
						$tabManagers[ strval($idManager) ] = $idManager;
					}else
						foreach ($user->getManagers() as $manager){
							$idManager = Managers::getUserIdFromEmployeeId($manager->id);
							$tabManagers[ strval($idManager) ] = $idManager;
						}
				} else {//Société
					$tabSocietes[] = $data->getID();
				}
			}
			foreach ($selectedPerimeter->getManagers() as $data) {
				$idManager = Managers::getUserIdFromEmployeeId($data->id);
				$tabManagers[ strval($idManager) ] = $idManager;
			}
		}

		$colWhere = array();
		if(sizeof($tabSocietes) > 0 && $colSociete != '') {
			$colWhere[] = $this->addOperatorValues(array_map(array($this,'addFieldValue'), $tabSocietes, array_fill(0,sizeof($tabSocietes),'ID_SOCIETE')));
		}
		if(sizeof($tabPoles) > 0 && $colPole != '') {
			$colWhere[] = $this->addOperatorValues(array_map(array($this,'addFieldValue'), $tabPoles, array_fill(0,sizeof($tabPoles),'ID_POLE')));
		}
		if(sizeof($tabManagers) > 0) {
			if(!is_array($colUser)) $colUser = [$colUser];
			foreach($colUser as $i => $tabUser) {
				if($tabUser=='') continue;
				$colWhere[] = $this->addOperatorValues(array_map(array($this,'addFieldValue'), $tabManagers, array_fill(0,sizeof($tabManagers),$tabUser)));
			}
		}
		if(sizeof($colWhere) > 0) return ($narrowPerimeter == 1 || $user->getNarrowPerimeter() == 1 && $narrowPerimeter != 0)?implode(' AND ', $colWhere):'('.implode(' OR ', $colWhere).')';
	}

	/**
	 * perform an implode on the array and wrap it with parenthesis if there is more than 1 element
	 * @param array  $tabArray
	 * @param string $operator Glue field
	 * @return  string
	 *
	 * @deprecated
	 *
	 * Example :
	 *
	 *     echo addOperatorValues(array(3,4,8));
	 *     (3 OR 4 OR 8)
	 */
	public function addOperatorValues($tabArray = array(), $operator = ' OR ') {return (sizeof($tabArray) > 1)?'('.implode($operator, $tabArray).')':$tabArray[0];}

	/**
	 * Build a where clause in the query for the given values and filters
	 *
	 * @param  mixed $data (string, array, int, ...) field value
	 * @param  string $colBDD field name in the database
	 * @param  array $exceptionArray ignore some values
	 * @param  boolean $escape if true, the value will be escaped with double quotes
	 * @param mixed $indifferent
	 * @return array
	 */
	public function getFilterSearch($data, $colBDD, $exceptionArray = array(), $escape = false, $indifferent = BM::INDIFFERENT) {
		if(!is_array($indifferent)) $indifferent = [$indifferent];
		$where = [];
		if($data && !in_array($indifferent, $data) && !in_array($data, $exceptionArray)){
			if (is_array($data)) {
				$data = array_diff($data,$exceptionArray);

				$tabSolr = array();
				foreach($data as $mcItem){
					if($escape || $mcItem !== '' && $mcItem !== null )
						$tabSolr[] = $colBDD.':'.($escape?'"'.$mcItem.'"':$mcItem);
				}

				if($tabSolr)
					$where[] = '('.implode(' OR ',$tabSolr).')';
			} else {
				if($escape || $data )
					$where[] = $colBDD.':'.($escape?'"'.$data.'"':$data);
			}
		}
		return $where;
	}

	/**
	 * calculate the offset
	 *
	 * @param int $page
	 * @param int $elementsPerPage
	 * @return int
	 */
	protected function getStartOffset($page, $elementsPerPage){
		return ($page-1)*$elementsPerPage;
	}
}
