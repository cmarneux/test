<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Databases\SolR;

use Wish\Mapper;
use Wish\Models\SearchResult;
use BoondManager\Lib\SolR;
use BoondManager\Services\BM;
use BoondManager\APIs\Candidates\Filters\SearchCandidates;

/**
 * Class Candidates
 * @package BoondManager\Databases\SolR
 */
class Candidates extends AbstractObject{
	/**
	 * perform a research on solr
	 *
	 * @param SearchCandidates $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception on invalid filter
	 */
	public function search(SearchCandidates $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$where = [];
		$where_cpt_expr = [];

		$order = $this->setOrderExpr($filter->sort->getValue(), $filter->order->getValue());

		switch($filter->perimeterManagersType->getValue()){
			case SearchCandidates::MANAGER_TYPE_HR:
				$where_cpt_expr[] = $this->getPerimeterSearch( $filter->getSelectedPerimeter(), 'ID_SOCIETE', 'ID_POLE', ['ID_RESPRH'], true, $filter->narrowPerimeter->getValue());
				break;
			case SearchCandidates::MANAGER_TYPE_RESP:
				$where_cpt_expr[] = $this->getPerimeterSearch( $filter->getSelectedPerimeter(), 'ID_SOCIETE', 'ID_POLE', ['ID_RESPMANAGER'], true, $filter->narrowPerimeter->getValue());
				break;
			default:case BM::INDIFFERENT:
				$where_cpt_expr[] = $this->getPerimeterSearch( $filter->getSelectedPerimeter(), 'ID_SOCIETE', 'ID_POLE', ['ID_RESPMANAGER','ID_RESPRH'], true, $filter->narrowPerimeter->getValue());
				break;
		}


		switch($filter->getPeriodType()){
			case SearchCandidates::PERIOD_CREATED:
				$where_cpt_expr[] = 'PROFIL_DATE:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
			case SearchCandidates::PERIOD_AVAILABLE:
				$where_cpt_expr[] = 'PARAM_DATEDISPO:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
			case SearchCandidates::PERIOD_UPDATED:
				$where_cpt_expr[] = 'PROFIL_DATEUPDATE:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
		}

		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->candidateStates->getValue(), 'PROFIL_ETAT'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->availabilityTypes->getValue(), 'PARAM_TYPEDISPO'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->contractTypes->getValue(), 'PARAM_CONTRAT'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->sources->getValue(), 'PARAM_TYPESOURCE'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->evaluations->getValue(), 'PROFIL_STATUT'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->trainings->getValue(), 'DT_FORMATION'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->experiences->getValue(), 'DT_EXPERIENCE'));


		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->activityAreas->getValue(), 'COMP_APPLICATIONS', [], true));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->expertiseAreas->getValue(), 'COMP_INTERVENTIONS', [], true));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->languages->getValue(), 'DT_LANGUES', [], true));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->mobilityAreas->getValue(), 'PARAM_MOBILITE', [], true));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->tools->getValue(), 'DT_OUTILS', [], true));

		if($filter->visibleProfile->getValue() && !$this->always_access)
			$where_cpt_expr[] = 'PROFIL_VISIBILITE:1';

		if(sizeof($filter->flags->getValue()) > 0)
			$where_cpt_expr[] = '('.implode(' OR ', $this->addMultiFieldsValue($filter->flags->getValue(), 'ID_FLAGS')).')';

		$keywords = SolR::escape($filter->keywords->getValue());
		if($keywords) {
			$searchIds = $this->getListIdSearch($keywords, ['CAND' => 'ID_PROFIL']);
			if($searchIds)
				$where = array_merge($where, $searchIds);
			else{
				SolR::parseStemming($keywords, $solrRaw, $solrStemming, $solrWildCard);

				switch($filter->keywordsType->getValue()) {
					case SearchCandidates::KEYWORD_TYPE_LASTNAME:
						if($solrWildCard) $where[] = 'grPROFIL_NOM:('.$solrWildCard.')';
						if($solrRaw) $where[] = 'rwPROFIL_NOM:('.$solrRaw.')';
						break;
					case SearchCandidates::KEYWORD_TYPE_FIRSTNAME:
						if($solrWildCard) $where[] = 'grPROFIL_PRENOM:('.$solrWildCard.')';
						if($solrRaw) $where[] = 'rwPROFIL_PRENOM:('.$solrRaw.')';
						break;
					case SearchCandidates::KEYWORD_TYPE_FULLNAME:
						$tabKeywords = explode('#', $filter['keywords']);
						if($tabKeywords[0]) {
							SolR::parseStemming($tabKeywords[0], $nomRaw, $nomStemming, $nomWildCard);
							$tabNom = [];
							if ($nomWildCard) $tabNom[] = 'grPROFIL_NOM:(' . $nomWildCard . ')';
							if ($nomRaw) $tabNom[] = 'rwPROFIL_NOM:(' . $nomRaw . ')';
							if ($tabNom) $where_cpt_expr[] = '(' . implode(' OR ', $tabNom) . ')';
						}
						if(isset($tabKeywords[1]) && $tabKeywords[1]) {
							$nomRaw = $nomStemming = $nomWildCard = '';
							SolR::parseStemming($tabKeywords[0], $nomRaw, $nomStemming, $nomWildCard);
							$tabNom = [];
							if($nomWildCard) $tabNom[] = 'grPROFIL_PRENOM:('.$nomWildCard.')';
							if($nomRaw) $tabNom[] = 'rwPROFIL_PRENOM:('.$nomRaw.')';
							if($tabNom) $where_cpt_expr[] = '('.implode(' OR ', $tabNom).')';
						}
						break;
					case SearchCandidates::KEYWORD_TYPE_EMAILS:
						$where[] = 'PROFIL_EMAILS:('.$keywords.')';
						break;
					case SearchCandidates::KEYWORD_TYPE_TITLE:
						if($solrStemming ) $where[] = 'stDT_TITRE:('.$solrStemming.')';
						if($solrRaw ) $where[] = 'rwDT_TITRE:('.$solrRaw.')';
						$order[] = 'score desc';
						break;
					case SearchCandidates::KEYWORD_TYPE_PHONES:
						$sPhones = preg_replace('/[^0-9\*]/', '', $keywords);
						if($sPhones != '') $where[] = 'PROFIL_TELS:('.$sPhones.')';
						break;
					case SearchCandidates::KEYWORD_TYPE_RESUME:
						if($solrStemming ) $where[] = 'stcv:('.$solrStemming.')';
						if($solrRaw ) $where[] = 'rwcv:('.$solrRaw.')';
						$order[] = 'score desc';
						break;
					case SearchCandidates::KEYWORD_TYPE_TD:
						if($solrWildCard) {
							$where[] = 'grPROFIL_NOM:('.$solrWildCard.')';
							$where[] = 'grPROFIL_PRENOM:('.$solrWildCard.')';
						}
						if($solrStemming )
							$where[] = 'stdt:('.$solrStemming.')';
						if($solrRaw ) {
							$where[] = 'rwPROFIL_NOM:('.$solrRaw.')';
							$where[] = 'rwPROFIL_PRENOM:('.$solrRaw.')';
							$where[] = 'rwdt:('.$solrRaw.')';
						}
						$order[] = 'score desc';
						break;
					default:case SearchCandidates::KEYWORD_TYPE_RESUMETD:
						if($solrWildCard ) {
							$where[] = 'grPROFIL_NOM:('.$solrWildCard.')';
							$where[] = 'grPROFIL_PRENOM:('.$solrWildCard.')';
						}
						if($solrStemming ) {
							$where[] = 'stdt:('.$solrStemming.')';
							$where[] = 'stcv:('.$solrStemming.')';
						}
						if($solrRaw ) {
							$where[] = 'rwPROFIL_NOM:('.$solrRaw.')';
							$where[] = 'rwPROFIL_PRENOM:('.$solrRaw.')';
							$where[] = 'rwdt:('.$solrRaw.')';
							$where[] = 'rwcv:('.$solrRaw.')';
						}
						$order[] = 'score desc';
				}
			}
		}

		return $this->solrSearch( $where, $where_cpt_expr, $order, $this->getStartOffset($filter->page->getValue(), $filter->maxResults->getValue()), $filter->maxResults->getValue());
	}

	/**
	 * @param array $column
	 * @param string $order
	 * @return null|string
	 */
	private function setOrderExpr($column, $order) {

		$mapping = [
			SearchCandidates::ORDERBY_LASTNAME => 'srPROFIL_NOM',
			SearchCandidates::ORDERBY_TITLE => 'srDT_TITRE',
			SearchCandidates::ORDERBY_AVAILABILITY => 'PARAM_DATEDISPO',
			SearchCandidates::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS => 'NB_POS',
			SearchCandidates::ORDERBY_MAINMANAGER_LASTNAME => 'srRESP_NOM',
			SearchCandidates::ORDERBY_UPDATEDATE => 'PROFIL_DATEUPDATE',
			SearchCandidates::ORDERBY_STATE => 'PROFIL_ETAT',
			SearchCandidates::ORDERBY_EXPERIENCE => 'DT_EXPERIENCE',
			SearchCandidates::ORDERBY_CREATIONDATE => 'PROFIL_DATE',
			SearchCandidates::ORDERBY_EVALUATION => 'PROFIL_STATUT',
			SearchCandidates::ORDERBY_HRMANAGER_LASTNAME => 'srRH_NOM',
			SearchCandidates::ORDERBY_SOURCE => 'PARAM_TYPESOURCE',
		];

		$sort = [];
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$sort[] = $mapping[$c].' '.$order;

		$sort[] = 'PROFIL_DATEUPDATE desc';
		return $sort;
	}

	/**
	 * Execute the search
	 *
	 * @param array $where_cpt_expr an array with some complementary WHERE clauses (build based on keywards)
	 * @param array $where
	 * @param array $order
	 * @param int $offsetStart
	 * @param int $rows
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	private function solrSearch($where_cpt_expr, $where, $order, $offsetStart, $rows) {
		$tabResult = SolR::instance()->search(BM::SOLR_MODULE_CANDIDATES, $where_cpt_expr, $where, $order, $offsetStart, $rows);
		return new SearchResult($tabResult['docs'], $tabResult['numFound']);
	}
}
