<?php
/**
 * companies.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Databases\SolR;

use Wish\Mapper;
use Wish\Models\SearchResult;
use BoondManager\Lib\SolR;
use BoondManager\Services\BM;
use BoondManager\APIs\Companies\Filters\SearchCompanies;

/**
 * Class Companies
 * @package BoondManager\Databases\SolR
 */
class Companies extends AbstractObject
{
	public function search(SearchCompanies $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$where = [];
		$where_cpt_expr = [];

		$order = $this->setOrderExpr($filter->sort->getValue(), $filter->order->getValue());

		$where_cpt_expr[] = $this->getPerimeterSearch($filter->getSelectedPerimeter(), $colSociete = 'ID_SOCIETE', 'ID_POLE', $colUser = 'ID_USER', true, $filter->narrowPerimeter->getValue());

		switch($filter->period->getValue()){
			case SearchCompanies::PERIOD_CREATED:
				$where_cpt_expr[] = 'CSOC_DATE:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
			case SearchCompanies::PERIOD_UPDATED:
				$where_cpt_expr[] = 'CSOC_DATEUPDATE:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
		}

		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->states->getValue(), 'CSOC_TYPE'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->origins->getValue(), 'CSOC_TYPESOURCE'));

		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->expertiseAreas->getValue(), 'CSOC_INTERVENTION', [], true));

		if(sizeof($filter->flags->getValue()) > 0)
			$where_cpt_expr[] = '('.implode(' OR ', $this->addMultiFieldsValue($filter->flags->getValue(), 'ID_FLAGS')).')';

		$keywords = SolR::escape($filter->keywords->getValue());
		if($keywords) {
			$searchIds = $this->getListIdSearch($keywords, ['CSOC' => 'ID_CRMSOCIETE']);
			if($searchIds)
				$where = array_merge($where, $searchIds);
			else{
				SolR::parseStemming($keywords, $solrRaw, $solrStemming, $solrWildCard);

				switch($filter->keywordsType->getValue()) {
					case SearchCompanies::KEYWORD_TYPE_NAME:
						if($solrWildCard) $where[] = 'grCSOC_SOCIETE:('.$solrWildCard.')';
						if($solrRaw) $where[] = 'rwCSOC_SOCIETE:('.$solrRaw.')';
						break;
					case SearchCompanies::KEYWORD_TYPE_PHONES:
						$sPhones = preg_replace('/[^0-9\*]/', '', $keywords);
						if($sPhones != '') $where[] = 'CSOC_TEL:('.$sPhones.')';
						break;
					default:
						if($solrWildCard ) {
							$where[] = 'stCSOC_SOCIETE:('.$solrWildCard.')';
						}
						if($solrStemming ) {
							$where[] = 'stdata:('.$solrStemming.')';
						}
						if($solrRaw ) {
							$where[] = 'rwCSOC_SOCIETE:('.$solrRaw.')';
							$where[] = 'rwdata:('.$solrRaw.')';
						}
						$order[] = 'score desc';
				}// fin du switch tk
			}
		}

		return $this->solrSearch( $where, $where_cpt_expr, $order, $this->getStartOffset($filter->page->getValue(), $filter->maxResults->getValue()), $filter->maxResults->getValue());
	}

	/**
	 * @param array $column
	 * @param string $order
	 * @return null|string
	 */
	private function setOrderExpr($column, $order)
	{
		$mapping = [
			SearchCompanies::ORDERBY_INFORMATION => 'srCSOC_METIERS',
			SearchCompanies::ORDERBY_EXPERTISEAREA => 'srCSOC_INTERVENTION',
			SearchCompanies::ORDERBY_MAINMANAGER_LASTNAME => 'srPROFIL_NOM',
			SearchCompanies::ORDERBY_UPDATE_DATE => 'CSOC_DATEUPDATE',
			SearchCompanies::ORDERBY_TYPE => 'CSOC_TYPE',
			SearchCompanies::ORDERBY_TOWN => 'srCSOC_VILLE',
			SearchCompanies::ORDERBY_NAME => 'srCSOC_SOCIETE'
		];

        $sort = [];
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$sort[] = $mapping[$c].' '.$order;

		$sort[] = 'CSOC_DATEUPDATE desc';
		return $sort;
	}

	/**
	 * Execute the search
	 *
	 * @param array $where_cpt_expr an array with some complementary WHERE clauses (build based on keywards)
	 * @param array $where
	 * @param array $order
	 * @param int $offsetStart
	 * @param int $rows
	 * @return SearchResult
	 * @throws \Exception
	 */
	private function solrSearch($where_cpt_expr, $where, $order, $offsetStart, $rows) {
		$tabResult = SolR::instance()->search(BM::SOLR_MODULE_CRM_COMPANIES, $where_cpt_expr, $where, $order, $offsetStart, $rows);
		$totalRows = $tabResult['numFound'];
		$mapper = new Mapper(\BoondManager\Models\Company::class);
		$rows = $mapper->mapArray($tabResult['docs']);
		return new SearchResult($rows, $totalRows);
	}
}
