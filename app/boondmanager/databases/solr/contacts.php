<?php
/**
 * contacts.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Databases\SolR;

use Wish\Mapper;
use Wish\Models\SearchResult;
use BoondManager\Lib\SolR;
use BoondManager\Services\BM;
use BoondManager\APIs\Contacts\Filters\SearchContacts;

/**
 * Class Contacts
 * @package BoondManager\Databases\SolR
 */
class Contacts extends AbstractObject
{
	public function search(SearchContacts $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		$where = [];
		$where_cpt_expr = [];

		$order = $this->setOrderExpr($filter->sort->getValue(), $filter->order->getValue());

		$where_cpt_expr[] = $this->getPerimeterSearch($filter->getSelectedPerimeter(), $colSociete = 'ID_SOCIETE', 'ID_POLE', $colUser = 'ID_USER', true, $filter->narrowPerimeter->getValue());

		switch($filter->period->getValue()){
			case SearchContacts::PERIOD_CREATED:
				$where_cpt_expr[] = 'CCON_DATE:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
			case SearchContacts::PERIOD_UPDATED:
				$where_cpt_expr[] = 'CCON_DATEUPDATE:['.$filter->startDate->getValue().'T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z]';
				break;
		}

		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->states->getValue(), 'CCON_TYPE'));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->origins->getValue(), 'CCON_TYPESOURCE'));

		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->activityAreas->getValue(), 'CCON_APPLICATIONS', [], true));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->expertiseAreas->getValue(), 'CSOC_INTERVENTION', [], true));
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch( $filter->tools->getValue(), 'CCON_OUTILS', [], true));

		if(sizeof($filter->flags->getValue()) > 0)
			$where_cpt_expr[] = '('.implode(' OR ', $this->addMultiFieldsValue($filter->flags->getValue(), 'ID_FLAGS')).')';

		$keywords = SolR::escape($filter->keywords->getValue());
		if($keywords) {
			$searchIds = $this->getListIdSearch($keywords, ['CCON' => 'ID_CRMCONTACT', 'CSOC' => 'ID_CRMSOCIETE']);
			if($searchIds)
				$where = array_merge($where, $searchIds);
			else{
				SolR::parseStemming($keywords, $solrRaw, $solrStemming, $solrWildCard);

				switch($filter->keywordsType->getValue()) {
					case SearchContacts::KEYWORD_TYPE_LASTNAME:
						if($solrWildCard) $where[] = 'grCCON_NOM:('.$solrWildCard.')';
						if($solrRaw) $where[] = 'rwCCON_NOM:('.$solrRaw.')';
						break;
					case SearchContacts::KEYWORD_TYPE_FIRSTNAME:
						if($solrWildCard) $where[] = 'grCCON_PRENOM:('.$solrWildCard.')';
						if($solrRaw) $where[] = 'rwCCON_PRENOM:('.$solrRaw.')';
						break;
					case SearchContacts::KEYWORD_TYPE_FULLNAME:
						$tabKeywords = explode('#', $filter['keywords']);
						if($tabKeywords[0]) {
							SolR::parseStemming($tabKeywords[0], $nomRaw, $nomStemming, $nomWildCard);
							$tabNom = [];
							if ($nomWildCard) $tabNom[] = 'grCCON_NOM:(' . $nomWildCard . ')';
							if ($nomRaw) $tabNom[] = 'rwCCON_NOM:(' . $nomRaw . ')';
							if ($tabNom) $where_cpt_expr[] = '(' . implode(' OR ', $tabNom) . ')';
						}
						if(isset($tabKeywords[1]) && $tabKeywords[1]) {
							$nomRaw = $nomStemming = $nomWildCard = '';
							SolR::parseStemming($tabKeywords[0], $nomRaw, $nomStemming, $nomWildCard);
							$tabNom = [];
							if($nomWildCard) $tabNom[] = 'grCCON_PRENOM:('.$nomWildCard.')';
							if($nomRaw) $tabNom[] = 'rwCCON_PRENOM:('.$nomRaw.')';
							if($tabNom) $where_cpt_expr[] = '('.implode(' OR ', $tabNom).')';
						}
						break;
					case SearchContacts::KEYWORD_TYPE_COMPANYFULLNAME:
						$tabKeywords = explode('#', $filter['keywords']);
						if($tabKeywords[0]) {
							$searchCRMSociete = $this->getListIdSearch($tabKeywords[0], ['CSOC' => 'ID_CRMSOCIETE']);
							if($searchCRMSociete) $where_cpt_expr[] = $searchCRMSociete;
						}
						if(isset($tabKeywords[1]) && $tabKeywords[1]) {
							SolR::parseStemming($tabKeywords[0], $nomRaw, $nomStemming, $nomWildCard);
							$tabNom = [];
							if ($nomWildCard) $tabNom[] = 'grCCON_NOM:(' . $nomWildCard . ')';
							if ($nomRaw) $tabNom[] = 'rwCCON_NOM:(' . $nomRaw . ')';
							if ($tabNom) $where_cpt_expr[] = '(' . implode(' OR ', $tabNom) . ')';
						}
						if(isset($tabKeywords[2]) && $tabKeywords[2]) {
							$nomRaw = $nomStemming = $nomWildCard = '';
							SolR::parseStemming($tabKeywords[0], $nomRaw, $nomStemming, $nomWildCard);
							$tabNom = [];
							if($nomWildCard) $tabNom[] = 'grCCON_PRENOM:('.$nomWildCard.')';
							if($nomRaw) $tabNom[] = 'rwCCON_PRENOM:('.$nomRaw.')';
							if($tabNom) $where_cpt_expr[] = '('.implode(' OR ', $tabNom).')';
						}
						break;
					case SearchContacts::KEYWORD_TYPE_EMAILS:
						$where[] = 'CCON_EMAILS:('.$keywords.')';
						break;
					case SearchContacts::KEYWORD_TYPE_PHONES:
						$sPhones = preg_replace('/[^0-9\*]/', '', $keywords);
						if($sPhones != '') $where[] = 'CCON_TELS:('.$sPhones.')';
						break;
					default:
						if($solrWildCard ) {
							$where[] = 'stCCON_NOM:('.$solrWildCard.')';
							$where[] = 'stCCON_PRENOM:('.$solrWildCard.')';
							$where[] = 'stCSOC_SOCIETE:('.$solrWildCard.')';
						}
						if($solrStemming ) {
							$where[] = 'stdata:('.$solrStemming.')';
						}
						if($solrRaw ) {
							$where[] = 'rwCCON_NOM:('.$solrRaw.')';
							$where[] = 'rwCCON_PRENOM:('.$solrRaw.')';
							$where[] = 'rwCSOC_SOCIETE:('.$solrRaw.')';
							$where[] = 'rwdata:('.$solrRaw.')';
						}
						$order[] = 'score desc';
				}// fin du switch tk
			}
		}

		return $this->solrSearch( $where, $where_cpt_expr, $order, $this->getStartOffset($filter->page->getValue(), $filter->maxResults->getValue()), $filter->maxResults->getValue());
	}

	/**
	 * @param array $column
	 * @param string $order
	 * @return null|string
	 */
	private function setOrderExpr($column, $order)
	{
		$mapping = [
			SearchContacts::ORDERBY_COMPANY_EXPERTISEAREA => 'srCSOC_INTERVENTION',
			SearchContacts::ORDERBY_LASTNAME => 'srCCON_NOM',
			SearchContacts::ORDERBY_MAINMANAGER_LASTNAME => 'srPROFIL_NOM',
			SearchContacts::ORDERBY_UPDATE_DATE => 'CCON_DATEUPDATE',
			SearchContacts::ORDERBY_TYPE => 'CCON_TYPE',
			SearchContacts::ORDERBY_TOWN => 'srCCON_VILLE',
			SearchContacts::ORDERBY_FUNCTION => 'srCCON_FONCTION',
			SearchContacts::ORDERBY_COMPANY_NAME => 'srCSOC_SOCIETE'
		];

        $sort = [];
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$sort[] = $mapping[$c].' '.$order;

		$sort[] = 'CCON_DATEUPDATE DESC';
		return $sort;
	}

	/**
	 * Execute the search
	 *
	 * @param array $where_cpt_expr an array with some complementary WHERE clauses (build based on keywards)
	 * @param array $where
	 * @param array $order
	 * @param int $offsetStart
	 * @param int $rows
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	private function solrSearch($where_cpt_expr, $where, $order, $offsetStart, $rows) {
		$tabResult = SolR::instance()->search(BM::SOLR_MODULE_CRM_CONTACTS, $where_cpt_expr, $where, $order, $offsetStart, $rows);
		$totalRows = $tabResult['numFound'];
		$mapper = new Mapper(\BoondManager\Models\Contact::class);
		$rows = $mapper->mapArray($tabResult['docs']);
		return new SearchResult($rows, $totalRows);
	}
}
