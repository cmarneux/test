<?php
/**
 * resource.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Databases\SolR;

use Wish\Mapper;
use Wish\Models\SearchResult;
use BoondManager\Lib\SolR;
use BoondManager\Services\BM;
use BoondManager\APIs\Employees\Filters\SearchEmployees;

/**
 * Class Resource
 * @package BoondManager\Databases\SolR
 */
class Resources extends AbstractObject{

	/**
	 * Do a search based on the given filter
	 *
	 * @param  SearchEmployees $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception on invalid filter
	 */
	public function search(SearchEmployees $filter)
	{
		if(!$filter->isValid()) throw new \Exception('Invalid filter');

		// construction du where
		$where_cpt_expr = [];
		$where = [];

		//EXCLUSION SELON PROFIL_TYPE
		if(sizeof($filter->excludeResourceTypes->getValue()) > 0) {
			$excludes = array_filter($filter->excludeResourceTypes->getValue());
			if($excludes)
				$where_cpt_expr[] = '(' . implode(' OR ', $this->addMultiFieldsValue($excludes, 'PROFIL_TYPE', true)) . ')';
		}

		//UNIQUEMENT SI PAS DE COMPTE MANAGER
		if($filter->excludeManager->getValue() == 1)
			$where_cpt_expr[] = '(ID_USER:0 OR USER_TYPE:6)'; // 6 = consultant

		$where_cpt_expr[] = $this->getPerimeterSearch(
			$filter->getSelectedPerimeter(),
			$colSociete = 'ID_SOCIETE',
			$colPole = 'ID_POLE',
			$colUser = ['ID_RESPMANAGER','ID_RESPRH'],
			$allowMyAgency = true,
			$filter->narrowPerimeter->getValue()
		);

		if($filter->availabilityType->getValue())
			$where_cpt_expr[] = '(PARAM_TYPEDISPO:1 OR PARAM_TYPEDISPO:3 OR ((PARAM_TYPEDISPO:2 OR PARAM_TYPEDISPO:4) AND PARAM_DATEDISPO:['.date('Y-m-d',time()-3600*24*15).'T00:00:00Z TO '.date('Y-m-d',time()+3600*24*15).'T23:59:59Z]))';

		switch($filter->getPeriodType()) {
			case SearchEmployees::PERIOD_AVAILABLE:
					$where_cpt_expr[] = '((PARAM_TYPEDISPO:2 OR PARAM_TYPEDISPO:4) AND PARAM_DATEDISPO:[00-00-0000T00:00:00Z TO '.$filter->endDate->getValue().'T23:59:59Z])';
				break;
		}

		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->resourceTypes->getValue(),'PROFIL_TYPE') );
		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->resourceStates->getValue(),'PROFIL_ETAT') );
		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->experiences->getValue(),'DT_EXPERIENCE') );

		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->trainings->getValue(),'DT_FORMATION', [], true) );
		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->activityAreas->getValue(),'COMP_APPLICATIONS', [], true) );
		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->expertiseAreas->getValue(),'COMP_INTERVENTIONS', [], true) );
		$where_cpt_expr = array_merge( $where_cpt_expr, $this->getFilterSearch($filter->languages->getValue(), 'DT_LANGUES', [], true) );
		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->mobilityAreas->getValue(),'PARAM_MOBILITE', [], true) );
		$where_cpt_expr = array_merge($where_cpt_expr, $this->getFilterSearch($filter->tools->getValue(),'DT_OUTILS', [], true) );

		if($filter->visibleProfile->getValue())
			$where_cpt_expr[] = 'PROFIL_VISIBILITE:1';

		//FLAGS
		if(sizeof($filter->flags->getValue()) > 0)
			$where_cpt_expr[] = '('.implode(' OR ', $this->addMultiFieldsValue($filter->flags->getValue(), 'ID_FLAGS')).')';


		$keywords = SolR::instance()->escape($filter->keywords->getValue());

		$order = $this->setOrderExpr($filter->sort->getValue(), $filter->order->getValue());//On construit ORDER BY

		//Construction du filtre dédié aux mots clés
		if($keywords) {
			$searchId = $this->getListIdSearch($keywords, ['COMP' => 'ID_PROFIL']);

			if($searchId)
				$where = array_merge( $where,  $searchId);
			else{
				$solrRaw = [];
				if(preg_match_all('#"([^\"]*)"#', $keywords, $match)) {
					//On supprime l'expression litérale de la recherche en Stemming
					$solrStemming = trim(str_replace($match[0],'', $keywords));
					foreach($match[1] as $quotedWords)
						// reduction de groupes d'espaces
						$solrRaw[] = '"'.trim(preg_replace('/\s+/', ' ',$quotedWords)).'"';
				} else $solrStemming = trim($keywords);

				$solrWildCard = trim(preg_replace('/(([^\*])(\s+|$))/', '\2*', $solrStemming));

				$likeSearch = $keywords;
				switch($filter->keywordsType->getValue()) {
					case SearchEmployees::KEYWORD_TYPE_LASTNAME://Nom
						if($solrWildCard != '') $where[] = 'stPROFIL_NOM:('.$solrWildCard.')';
						if(sizeof($solrRaw) > 0) $where[] = 'rwPROFIL_NOM:('.implode(' ', $solrRaw).')';
						break;
					case SearchEmployees::KEYWORD_TYPE_FIRSTNAME://Prénom
						if($solrWildCard != '') $where[] = 'stPROFIL_PRENOM:('.$solrWildCard.')';
						if(sizeof($solrRaw) > 0) $where[] = 'rwPROFIL_PRENOM:('.implode(' ', $solrRaw).')';
						break;
					case SearchEmployees::KEYWORD_TYPE_FULLNAME://Nom#Prénom
						$tabKeywords = explode('#', $likeSearch);
						if($tabKeywords[0] != '') $where_cpt_expr[] = 'rwPROFIL_NOM:('.$tabKeywords[0].')';
						if(isset($tabKeywords[1]) && $tabKeywords[1] != '') $where_cpt_expr[] = 'rwPROFIL_PRENOM:('.$tabKeywords[1].')';
						break;
					case SearchEmployees::KEYWORD_TYPE_EMAILS://Email
						$where[] = 'PROFIL_EMAILS:('.$keywords.')';
						break;
					case SearchEmployees::KEYWORD_TYPE_TITLE:
						if($solrStemming != '') $where[] = 'stDT_TITRE:('.$solrStemming.')';
						if(sizeof($solrRaw) > 0) $where[] = 'rwDT_TITRE:('.implode(' ', $solrRaw).')';
						$order[] = 'score desc';
						break;
					case SearchEmployees::KEYWORD_TYPE_PHONES:
						$where[] = 'PROFIL_TELS:('.preg_replace('/[^0-9|\*]/', '', $keywords).')';
						break;
					case SearchEmployees::KEYWORD_TYPE_NUMBER:
						if($solrWildCard != '') $where[] = 'stPROFIL_REFERENCE:('.$solrWildCard.')';
						if(sizeof($solrRaw) > 0) $where[] = 'rwPROFIL_REFERENCE:('.implode(' ', $solrRaw).')';
						break;
					case SearchEmployees::KEYWORD_TYPE_RESUME:
						if($solrStemming != '') $where[] = 'stcv:('.$solrStemming.')';
						if(sizeof($solrRaw) > 0) $where[] = 'rwcv:('.implode(' ', $solrRaw).')';
						$order[] = 'score desc';
						break;
					case SearchEmployees::KEYWORD_TYPE_TD:
						if($solrStemming != '') $where[] = 'stdt:('.$solrStemming.')';
						if(sizeof($solrRaw) > 0) $where[] = 'rwdt:('.implode(' ', $solrRaw).')';
						$order[] = 'score desc';
						break;
					default:case SearchEmployees::KEYWORD_TYPE_RESUMETD:
						$where[] = 'stPROFIL_NOM:('.$solrWildCard.')';
						$where[] = 'stPROFIL_PRENOM:('.$solrWildCard.')';
						if($solrStemming != ''){
							$where[] = 'stdt:('.$solrStemming.')';
							$where[] = 'stcv:('.$solrStemming.')';
						}
						if(sizeof($solrRaw) > 0){
							$where[] = 'rwdt:('.implode(' ', $solrRaw).')';
							$where[] = 'rwcv:('.implode(' ', $solrRaw).')';
						}
						$order[] = 'score desc';
				}
			}
		}

		$where_cpt_expr = array_filter($where_cpt_expr);
		$where = array_filter($where);
		//var_dump($where_cpt_expr, $where);
		return $this->solrSearch($where, $where_cpt_expr, $order, $this->getStartOffset($filter->page->getValue(), $filter->maxResults->getValue()), $filter->maxResults->getValue());
	}

	/**
	 * Execute the search
	 *
	 * @param $where an array with some complementary WHERE clauses (build based on keywards)
	 * @param $where_cpt_expr
	 * @param $order
	 * @param $offsetStart
	 * @param $rows
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	private function solrSearch($where, $where_cpt_expr, $order, $offsetStart, $rows) {
		$tabResult = SolR::instance()->search(BM::SOLR_MODULE_RESOURCES, $where, $where_cpt_expr, $order, $offsetStart, $rows);
		if($tabResult !== false) {
			return new SearchResult($tabResult['docs'], $tabResult['numFound']);
		}
		return false;
	}

	/**
	 * map the filter column to the solr column
	 *
	 * @param array $column sorting column (filter key)
	 * @param string $order asc|desc
	 * @return array
	 */
	private function setOrderExpr($column, $order) {
		$mapping = [
			SearchEmployees::ORDERBY_UPDATEDATE                    => 'PROFIL_DATEUPDATE',
			SearchEmployees::ORDERBY_LASTNAME                      => 'srPROFIL_NOM',
			SearchEmployees::ORDERBY_TITLE                         => 'srDT_TITRE',
			SearchEmployees::ORDERBY_AVAILABILITY                  => 'PARAM_DATEDISPO',
			SearchEmployees::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS => 'NB_POS',
			SearchEmployees::ORDERBY_PRICEEXCLUDINGTAX             => 'PARAM_TARIF1',
			SearchEmployees::ORDERBY_MAINMANAGER_LASTNAME          => 'srRESP_NOM'
		];

		$sort = [];
		foreach($column as $c)
			if(array_key_exists($c, $mapping))
				$sort[] = $mapping[$c].' '.$order;

		$sort[] = 'PROFIL_DATEUPDATE desc';
		return $sort;
	}
}
