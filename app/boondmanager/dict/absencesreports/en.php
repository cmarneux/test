<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'PROFIL_NOM' => 'Last Name',
		'PROFIL_PRENOM' => 'First Name',
		'PROFIL_REFERENCE' => 'Number',
		'PROFIL_TYPE' => 'Type',
		'LISTEABSENCES_DATE' => 'Date',
		'PABS_DUREE' => 'Duration',
		'PABS_TYPEHREF' => 'Absence Code',
		'TYPEH_NAME' => 'Absence Name',
		'PABS_DEBUT' => 'Start',
		'PABS_FIN' => 'End',
	],
	'notifications'=>[
		'specials' => [
			'changeField' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'delete' => 'Suppression de la demande d\'absence {0}',
		]
	]
];
