<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'PROFIL_NOM' => 'Nom',
		'PROFIL_PRENOM' => 'Prénom',
		'PROFIL_REFERENCE' => 'Matricule',
		'PROFIL_TYPE' => 'Type',
		'LISTEABSENCES_DATE' => 'Date',
		'PABS_DUREE' => 'Durée',
		'PABS_TYPEHREF' => 'Code Absence',
		'TYPEH_NAME' => 'Nom Absence',
		'PABS_DEBUT' => 'Début',
		'PABS_FIN' => 'Fin',
	],
	'notifications'=>[
		'specials' => [
			'changeField' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'delete' => 'Suppression de la demande d\'absence {0}',
		]
	]
];
