<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'tabs' => [
		\BoondManager\Models\Employee::TAB_INFORMATION    => 'Information',
		\BoondManager\Models\Employee::TAB_ADMINISTRATIVE => 'Administrative',
		\BoondManager\Models\Employee::TAB_TECHNICALDATA             => 'TD',
		\BoondManager\Models\Employee::TAB_ACTIONS        => 'Actions',
		\BoondManager\Models\Employee::TAB_POSITIONINGS   => 'Positioning',
	],
	'values' => [
		'IMMEDIATE' => 'immediate',
		'gender' => [ 'Mr', 'Ms', 'Miss'],
	],
	'sqlLabels' => [
		'lastName' => 'Last Name',
		'firstName' => 'First Name',
		'reference' => 'Number',
		'email1' => 'Email',
		'phone1' => 'Phone',
		'typeOf' => 'Type',
		'state' => 'State',
		'title' => 'Title',
		'skills' => 'Skills',
		'mainManager' => 'Main Manager',
		'hrManager' => 'HR Manager',
		'agency' => 'Agency',
		'availability' => 'Availability'
	],
	'notifications'=>[
		'specials' => [
			'changefield' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'update' => 'Modification of tab "{0}"',
			'delete' => 'Suppression of candidate {0}',
			'addCV' => 'Adding CV "{0}"',
			'deleteCV' => 'Removing CV "{0}"',
			'addDocument' => 'Adding document "{0}"',
			'deleteDocument'=>'Removing document "{0}"',
			'position'=>'Positioning on the opportunity "{0}"'
		]
	]
];
