<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'values' => [
		'gender' => [ 'Mr', 'Mrs', 'Miss'],
	],
	'sqlLabels' => [
		'ID_CRMCONTACT' => 'Reference',
		'CCON_CIVILITE' => 'Civility',
		'CCON_NOM' => 'Last Name',
		'CCON_PRENOM' => 'First Name',
		'CCON_FONCTION' => 'Role',
		'CCON_SERVICE' => 'Service',
		'CCON_EMAIL' => 'Email',
		'CCON_EMAIL2' => 'Email',
		'CCON_EMAIL3' => 'Email',
		'CCON_TEL1' => 'Phone 1',
		'CCON_TEL2' => 'Phone 2',
		'CCON_ADR' => 'Address',
		'CCON_CP' => 'Postcode',
		'CCON_VILLE' => 'Town',
		'CCON_PAYS' => 'Country',
		'CCON_COMMENTAIRE' => 'Technical Perimeter',

		'CSOC_SOCIETE' => 'Company',
		'CSOC_INTERVENTION' => 'Sector activity',
		'CSOC_METIERS' => 'Information',
		'CCON_TYPE' => 'State',
		'CSOC_WEB' => 'Website',
		'CSOC_TEL' => 'Company - Phone',
		'CSOC_ADR' => 'Company - Address',
		'CSOC_CP' => 'Company - Postcode',
		'CSOC_VILLE' => 'Company - Town',
		'CSOC_PAYS' => 'Company - Country',
		'ID_PROFIL' => 'Manager'
	]
];
