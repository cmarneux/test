<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'values' => [
		'gender' => [ 'Mr', 'Mme', 'Mlle'],
	],
	'sqlLabels' => [
		'id' => 'Référence',
		'civility' => 'Civilité',
		'lastName' => 'Nom',
		'firstName' => 'Prénom',
		'function' => 'Fonction',
		'department' => 'Service',
		'email1' => 'Email 1',
		'email2' => 'Email 2',
		'email3' => 'Email 3',
		'phone1' => 'Téléphone 1',
		'phone2' => 'Téléphone 2',
		'address' => 'Adresse',
		'postCode' => 'Code Postal',
		'town' => 'Ville',
		'country' => 'Pays',
		'informationComments' => 'Périmètre Techinque',
		'activityArea' => 'Secteur',
		'state' => 'Etat',
		'influencers' => 'Influenceurs',
		'origin' => [
			'typeOf' => 'Origine'
		],
		'company' => [
			'id'                  => 'Société',
			'informationComments' => 'Informations',
			'phone1'              => 'Société - Téléphone',
			'address'             => 'Société - Address',
			'postCode'            => 'Société - Code postal',
			'town'                => 'Société - Ville',
			'country'             => 'Société - Pays',
		],
		'mainManager' => 'Responsable'
	],
	'notifications'=>[
		'actions' => [
			'update' => 'Modification de l\'onglet "{0}"',
			'delete' => 'Suppression du contact {0}'
		]
	],
	'tabs' => [
		'information' => 'Information'
	]
];
