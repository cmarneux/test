<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'CTR_DEBUT'          => 'Start',
		'CTR_FIN'            => 'End',
		'CTR_DATEPE1'        => 'End date of PP',
		'CTR_DATEPE2'        => 'End date of renewal of PP',
		'CTR_TYPE'           => 'Contract',
		'CTR_CLASSIFICATION' => 'Classification',
		'CTR_CATEGORIE'      => 'Category',
		'CTR_TPSTRAVAIL'     => 'Working time',
		'CTR_DEVISE'         => 'Currency',
		'CTR_SALAIREMENSUEL' => 'Monthly salary',
		'CTR_SALAIREHEURE'   => 'Hourly salary',
		'CTR_NBJRSOUVRE'     => 'Dailies expenses',
		'CTR_CHARGE'         => 'Charges',
		'CTR_FRSJOUR'        => 'Dailies expenses',
		'CTR_FRSMENSUEL'     => 'Monthlies expenses',
		'ID_SOCIETE'         => 'Agency',
	],
	'notifications'=>[
		'actions' => [
			'update' => 'Contract update',
		]
	]
];
