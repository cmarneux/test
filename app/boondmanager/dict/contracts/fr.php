<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'CTR_DEBUT'          => 'Début',
		'CTR_FIN'            => 'Fin',
		'CTR_DATEPE1'        => 'Date de fin de PE',
		'CTR_DATEPE2'        => 'Date de fin de renouvellement de PE',
		'CTR_TYPE'           => 'Contrat',
		'CTR_CLASSIFICATION' => 'Classification',
		'CTR_CATEGORIE'      => 'Catégorie',
		'CTR_TPSTRAVAIL'     => 'Temps de travail',
		'CTR_DEVISE'         => 'Devise',
		'CTR_SALAIREMENSUEL' => 'Salaire mensuel',
		'CTR_SALAIREHEURE'   => 'Salaire horaire',
		'CTR_NBJRSOUVRE'     => 'Nb Jours Ouvrés Annuel',
		'CTR_CHARGE'         => 'Coefficient de charge',
		'CTR_FRSJOUR'        => 'Frais journaliers',
		'CTR_FRSMENSUEL'     => 'Frais mensuels',
		'ID_SOCIETE'         => 'Agence',
	],
	'notifications'=>[
		'actions' => [
			'update' => 'Mis à jour du contrat',
		]
	]
];
