<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'id' => 'Reference',
		'name' => 'Company',
		'expertiseArea' => 'Sector activity',
		'informationComments' => 'Information',
		'state' => 'State',
		'website' => 'Website',
		'phone1' => 'Phone',
		'address' => 'Address',
		'postcode' => 'Postcode',
		'town' => 'Town',
		'country' => 'Pays',
		'mainManager' => 'Manager',
		'influencers' => 'Influenceurs'
	],
	'notifications'=>[
		'actions' => [
			'delete' => 'Deletion of CRM company {0}',
		]
	]
];
