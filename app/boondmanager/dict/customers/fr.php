<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'id' => 'Référence',
		'name' => 'Société',
		'expertiseArea' => 'Secteur',
		'informationComments' => 'Informations',
		'state' => 'Etat',
		'website' => 'Site web',
		'phone1' => 'Téléphone',
		'address' => 'Adresse',
		'postcode' => 'Code postal',
		'town' => 'Ville',
		'country' => 'Pays',
		'mainManager' => 'Responsable',
		'influencers' => 'Influenceurs'
	],
	'notifications'=>[
		'actions' => [
			'delete' => 'Suppression de la société CRM {0}',
		]
	]
];
