<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'values' => [
		'imediate' => 'immediate',
		'gender' => [ 'Mr', 'Mme', 'Mlle'],
	],
	'sqlLabels' => [
		'mainManager' => 'Main Manager',
		'agency' => 'Agency',
		'pole' => 'Pole'
	],
	'tabs' => [
		'information' => 'Information',
		'positionings' => 'Positioning',
		'actions' => 'Actions',
		'projects' => 'Projects'
	],
	'notifications'=>[
		'specials' => [
			'changeField' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'update' => 'Modification of tab "{0}"'
		]
	]
];
