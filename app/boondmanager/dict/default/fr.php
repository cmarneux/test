<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'values' => [ //FIXME
		'imediate' => 'immediat',
		'gender' => [ 'Mr', 'Mme', 'Mlle'],
	],
	'sqlLabels' => [
		'mainManager' => 'Responsable',
		'agency' => 'Agence',
		'pole' => 'Pole'
	],
	'tabs' => [
		'information' => 'Informations',
		'positionings' => 'Positionnements',
		'actions' => 'Actions',
		'projects' => 'Projets'
	],
	'notifications'=>[
		'specials' => [
			'changeField' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'update' => 'Modification de l\'onglet "{0}"'
		]
	]
];
