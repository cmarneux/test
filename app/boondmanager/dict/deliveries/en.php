<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */


return [
	'sqlLabels' => [
		'ID_MISSIONPROJET' => 'Reference',
		'COMP_NOM' => 'Item - Last Name',
		'COMP_PRENOM' => 'Item - First Name',
		'MP_DEBUT' => 'Start',
		'MP_FIN' => 'End',
		'PRJ_REFERENCE' => 'Project',
		'PRJ_TYPEREF' => 'Type',
		'MP_TYPE' => 'State',
		'AO_TITLE' => 'Opportunity',
		'CCON_NOM' => 'Client - Last Name',
		'CCON_PRENOM' => 'Client - First Name',
		'CSOC_SOCIETE' => 'Client - Company',
		'PRJ_DEVISE' => 'Currency',
		'MP_TARIF' => 'Daily sale price ET',
		'MP_CJM' => 'ADC',
		'TOTAL_CA' => 'Turnover ET',
		'TOTAL_MARGE' => 'Margin ET',
		'TOTAL_RENTA' => 'Profit.',
		'PRJ_CP' => 'Postcode',
		'PRJ_VILLE' => 'Town',
		'ID_PROFIL' => 'Manager'
	]
];
