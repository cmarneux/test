<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'ID_MISSIONPROJET' => 'Référence',
		'COMP_NOM' => 'Item - Nom',
		'COMP_PRENOM' => 'Item - Prénom',

		'startDateProduct' => 'Date',
		'startDate' => 'Début',
		'endDate' => 'Fin',

		'PRJ_REFERENCE' => 'Projet',
		'PRJ_TYPEREF' => 'Type',

		'state' => 'Etat',

		'AO_TITLE' => 'Besoin',
		'CCON_NOM' => 'Client - Nom',
		'CCON_PRENOM' => 'Client - Prénom',
		'CSOC_SOCIETE' => 'Client - Société',

		'currency' => 'Devise',

		'averageDailyPriceExcludingTax' => 'Tarif de vente journalier HT',
		'averageDailyCost' => 'CJM',

		'averageDailyPriceExcludingTaxPackage' => 'Tarif projet journalier HT',
		'averageDailyCostPackage' => 'CJM Prestation',
		'numberOfDaysInvoicedOrQuantityPackage' => 'Charge',

		'TOTAL_CA' => 'CA HT',
		'TOTAL_MARGE' => 'Marge HT',
		'TOTAL_RENTA' => 'Renta.',
		'PRJ_CP' => 'Code postal',
		'PRJ_VILLE' => 'Ville',
		'ID_PROFIL' => 'Responsable'
	],
	'notifications' => [
		'actions' => [
			'update' => 'Modification de la prestation {reference} de {relation}',
			'delete' => 'Suppression de la prestation {reference} de {relation}'
		]
	]
];
