<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'extraction' => [
		'kmExpenses' => 'Travel expenses'
	],
	'sqlLabels' => [
		'PROFIL_NOM' => 'Last Name',
		'PROFIL_PRENOM' => 'First Name',
		'PROFIL_REFERENCE' => 'Number',
		'PROFIL_TYPE' => 'Type',
		'LISTETEMPS_DATE' => 'Date',
		'DATE' => 'Date',
		'LISTEFRAIS_DEVISEAGENCE' => 'Currency',
		'LISTEFRAIS_BKMVALUE'     => 'Km scale',
		'TOTAL_KM'                => 'Km Covered',
		'TOTAL_KM_EXPENSES'      => 'Travel Exp.',
		'TOTAL_REAL'              => 'Actual expenses',
		'TOTAL_PACKAGED'          => 'Fixed expenses',
		'TOTAL_TVA'               => 'VAT',
		'TOTAL_EXPENSES'          => 'EXPENSE',
		'TOTAL_REBILLED'          => 'To be rebilled',
		'LISTEFRAIS_AVANCE'       => 'Advance',
		'TOPAYE'                  => 'To pay',
		'LISTEFRAIS_REGLE'        => 'Paid',
		'TYPEFRSREF' => 'Expenses Code',
		'TYPEFRS_NAME' => 'Expenses Name',
		'FRAISREEL_INTITULE' => 'Expenses Description',
		'FRAISREEL_REFACTURE' => 'Allowance',
		'TOTAL_TTC' => 'Amount IT',
		'TOTAL_HT' => 'Amount ET',
	],
];
