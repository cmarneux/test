<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'extraction' => [
		'kmExpenses' => 'Frais Kilométrique'
	],
	'sqlLabels' => [

		'PROFIL_NOM' => 'Nom',
		'PROFIL_PRENOM' => 'Prénom',
		'PROFIL_REFERENCE' => 'Matricule',
		'PROFIL_TYPE' => 'Type',
		'LISTEFRAIS_DATE' => 'Date',
		'DATE' => 'Date',
		'LISTEFRAIS_DEVISEAGENCE' => 'Devise',
		'LISTEFRAIS_BKMVALUE'     => 'Barème KM',
		'TOTAL_KM'                => 'Km Parcourus',
		'TOTAL_KM_EXPENSES'      => 'Frais KM',
		'TOTAL_REAL'              => 'Frais réels',
		'TOTAL_PACKAGED'          => 'Frais forfaitaires',
		'TOTAL_TVA'               => 'TVA',
		'TOTAL_EXPENSES'          => 'FRAIS',
		'TOTAL_REBILLED'          => 'A refacturer',
		'LISTEFRAIS_AVANCE'       => 'Avance',
		'TOPAYE'                  => 'A régler',
		'LISTEFRAIS_REGLE'        => 'Payé',
		'TYPEFRSREF' => 'Code Frais',
		'TYPEFRS_NAME' => 'Nom Frais',
		'FRAISREEL_INTITULE' => 'Description Frais',
		'FRAISREEL_REFACTURE' => 'Refacture',
		'TOTAL_TTC' => 'Montant TTC',
		'TOTAL_HT' => 'Montant HT',
	],
];
