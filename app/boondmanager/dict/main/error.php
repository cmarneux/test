<?php
/**
 * error.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
return [
	1000 => 'Incorrect JSON schema',
	1001 => 'Wrong authentication',
	1002 => 'Wrong or missing attribute',
	1003 => 'This entity doesn\'t exist',
	1004 => 'Rights are not enough to set this attribute',
	1005 => 'This entity cannot be deleted because it still exists relationships',
	1006 => 'This entity cannot be updated because an error occured',
	1007 => 'File cannot be uploaded because the quota is reached',
	1008 => 'File cannot be downloaded',
	1009 => 'No valid email exist for this account',
	1010 => 'Resource cannot be created because the quota is reached',
	1011 => 'User\'s account cannot be activated because the quota is reached',
	1012 => 'Ressource\'s account cannot be activated because the quota is reached',
	1013 => 'This attribute has duplicated items',
	//Application
	1100 => 'Can\'t login because login or password have no match',
	1101 => 'This login doesn\'t exist',
	1102 => 'No attempt to login before with this new device',
	1103 => 'Wrong code to authenticate this device',
	1104 => 'Wrong language',
	1105 => 'Wrong captcha',
	1106 => 'Failed to create a captcha',
	//Customers
	1200 => 'Administrator login already exists',
	1201 => 'Manager login already exists',
	//Accounts
	2000 => 'The password does not meet the permitted characters',
	2001 => 'Login & password must be set',
	2002 => 'The user cannot be created or activated because the quota is reached',
	//Agencies
	2100 => 'This URL is not available',
	2101 => 'The agency\'s details are incomplete',
	2102 => 'The agency\'s details are incomplete',
	2103 => 'The agency\'s details are incomplete',
	//Apps
	2200 => 'Unable to install',
	2201 => 'Unable to uninstall',
	2202 => 'Problem during the usage of the App',
	//Actions
	2300 => 'If company or dependsOn are defined into the body, and dependsOn represents a contact, then both have to be defined',
	2301 => 'endDate have to be superior or equal to startDate',
	//Resources
	2400 => 'If company or contact are defined into the body then both have to be defined',
	//Contracts
	2500 => 'Candidate or resource as query parameter have to be sent',
	2501 => 'endDate have to be superior or equal to startDate',
	2502 => 'probationEndDate have to be superior or equal to startDate and inferior or equal to endDate',
	2503 => 'renewalProbationEndDate have to be superior or equal to probationEndDate and inferior or equal to endDate',
	2504 => 'chargeFactor can only be set if contract do not depends on an external resource',
	//Advantages
	2600 => '',
	//Targets
	2700 => 'periodYear and periodNumber have to be both false or defined',
	//Candidates
	2800 => 'If company or contact are defined into the body then both have to be defined',
	//Products
	2900 => '',
	//Contacts
	3000 => '',
	//Companies
	3100 => '',
	//Opportunities
	3200 => 'Unable to update typeOf into a different mode when a project exists or opportunity\'s mode is `product`',
	3201 => 'If company or contact are defined into the body then both have to be defined',
	//Positionings
	3300 => 'Unable to create a delivery/project because the resource, candidate or product does not exist anymore',
	3301 => 'Unable to create a delivery/project which mode is not `recruitment` because the candidate is not hired',
	3302 => 'Unable to attach a positioning to an existing project when opportunity\'s mode is `recruitment`',
	3303 => 'Unable to modify positioning\'s state when a project is attached to a positioning',
	3304 => 'endDate have to be superior or equal to startDate',
	//Projects
	3400 => 'Unable to create a project which mode is not `package`, `product` (For other project\'s mode, it is necessary to win a positioning)',
	3401 => 'Unable to modify typeOf into a different mode',
	3402 => 'If company or contact are defined into the body then both have to be defined',
	3403 => 'endDate have to be superior or equal to startDate',
	3404 => 'startDate can only be set if project\'s mode is `package` or `recruitment`',
	3405 => 'endDate can only be set if project\'s mode is `package`',
	3406 => 'remainsToBeDone can only be set if project\'s mode is `package` and project\'s synchronizeRemainsToBeDone is false',
	3407 => 'showBatchesMarkersTab can only be set if project\'s mode is `package` or `product',
	3408 => 'allowCreateMarkersOnBatches can only be set if project\'s mode is `package` or `product` and user is not a resource',
	3409 => 'synchronizeRemainsToBeDone can only be set if project\'s mode is `package` and user is not a resource',
	3410 => 'showProductivityPerDelivery can only be set if project\'s mode is `package` or `technicalAssistance`',
	3411 => 'Unable to create a project which mode is different from the opportunity of which it depends',
	3412 => 'batchesMarkers.markers have to contain exactly one element if allowCreateMarkersOnBatches is false',
	3413 => 'Unable to update an additional turnover and cost if a purchase depends on it',
	3414 => 'If batchesMarkers.markers.durationForecast or batchesMarkers.markers.remainsToBeDone are defined then both have to be set',
	3415 => 'batchesMarkers.markers.id can only be set if batch is updated or have to be set if batch exists and allowCreateMarkersOnBatches is false',
	3416 => 'Unable to create/delete batche if API simulaton is not writable',
	3417 => 'projectManagers can only be set if project\'s mode is `package` or `technicalAssistance`',
	3418 => 'batchesMarkers can only be set if project\'s mode is `package`',
	3419 => 'batchesMarkers.title can only be set if API simulaton is writable',
	3420 => 'batchesMarkers.distributionRate can only be set if API simulaton is writable',
	//Deliveries
	3500 => 'isGroupment and isInactivity can not be both set at `true`',
	3502 => 'forceTransferCreation and cancelTransfer can not be both set at `true`',
	3503 => 'startDate can not be set if delivery is a slave',
	3504 => 'endDate can not be set if delivery is a slave',
	3505 => 'endDate have to be superior or equal to startDate',
	3506 => 'title can not be set if delivery depends on a groupment',
	3507 => 'state can not be set if delivery is a slave',
	3508 => 'numberOfDaysInvoicedOrQuantity can not be set if delivery is a slave or depends on a groupment',
	3509 => 'numberOfDaysFree can not be set if project\'s type is `product` or delivery is a slave, inactivity, groupment or depends on a groupment',
	3510 => 'dailyExpenses can not be set if project\'s type is `product` or delivery is a groupment',
	3511 => 'monthlyExpenses can not be set if project\'s type is `product` or delivery is a groupment',
	3512 => 'numberOfWorkingDays can not be set if project\'s type is `product` or delivery is a groupment',
	3513 => 'averageDailyCost can be set only if delivery is a groupment',
	3514 => 'averageDailyContractCost can not be set if delivery is a master, groupment or depends on a contract',
	3515 => 'weeklyWorkingHours can not be set if project\'s type is `product` or delivery is a groupment, inactivity',
	3516 => 'averageHourlyPriceExcludingTax can not be set if project\'s type is `product` or delivery is a groupment, inactivity',
	3517 => 'forceAverageHourlyPriceExcludingTax can not be set if project\'s type is `product` or delivery is a groupment, inactivity',
	3518 => 'conditions can not be set if delivery is a groupment or inactivity',
	3519 => 'slave can not be set if forceTransferCreation is `true`',
	3520 => 'slave can not be set if cancelTransfer is `true`',
	3521 => 'additionalTurnoverAndCosts can not be set if delivery is a groupment',
	3522 => 'expensesDetails can not be set if project\'s type is `product` or delivery is a groupment',
	3523 => 'advantages can not be set if project\'s type is `product` or delivery is a groupment or inactivity',
	3524 => 'exceptionalScales can not be set if project\'s type is `product` or delivery is a groupment or inactivity',
	3525 => 'Unable to update an additional turnover and cost if a purchase depends on it',
	//Purchases
	3600 => 'Additional turnover and costs, if defined, has to belong to the same project than purchase\'s project',
	3601 => 'Additional turnover and costs, if defined, do not depends on a purchase',
	3602 => 'If delivery is set then project have to be set',
	3603 => 'If company or contact are defined into the body then both have to be defined',
	3604 => 'endDate have to be superior or equal to startDate',
	//Payments
	3700 => 'endDate have to be superior or equal to startDate',
	//Expense Reports
	3800 => 'workUnitType, delivery, batch and project can not be different for the same row',
	3801 => 'Times endDate have to be superior or equal to times startDate',
	3802 => 'Delivery can only be set if workUnitType.activityType is production, exceptionalTime or exceptionalCalendar',
	3803 => 'Batch can only be set if workUnitType.activityType is production, exceptionalTime or exceptionalCalendar',
	3804 => 'Project can only be set if workUnitType.activityType is production, exceptionalTime or exceptionalCalendar',
	3805 => 'This delivery is not allowed',
	3806 => 'This batch is not allowed',
	3807 => 'This project is not allowed',
	//TimesReports
	3900 => 'workUnitType, delivery, batch and project can not be different for the same row',
	3901 => 'Times endDate have to be superior or equal to times startDate',
	3902 => 'Delivery can only be set if workUnitType.activityType is production, exceptionalTime or exceptionalCalendar',
	3903 => 'Batch can only be set if workUnitType.activityType is production, exceptionalTime or exceptionalCalendar',
	3904 => 'Project can only be set if workUnitType.activityType is production, exceptionalTime or exceptionalCalendar',
	3905 => 'This delivery is not allowed',
	3906 => 'This batch is not allowed',
	3907 => 'This project is not allowed',
	//AbsencesReports
	4000 => 'workUnitType, delivery, batch and project can not be different for the same row',
	//Validations
	4101 => 'realValidator can not be set if validation\'s state is not `validated`',
	//Orders
	4200 => '',
	//Invoices
	4300 => '',
	//Groupment
	4800 => 'endDate have to be superior or equal to startDate',
	4801 => 'startDate or endDate can be set only if there is no deliveries inside the groupment',
	4802 => 'Each delivery in the attribute deliveries can not be assigned to another groupment',
	4803 => 'In deliveries, weighting can be set only if loadDistribution is weighted',
	4804 => 'In deliveries, schedule can be set only if loadDistribution is manual',
	4805 => 'Each delivery in the attribute deliveries must belongs to the same project',
];

