<?php
/**
 * fr.php
 *
 * Main language's file in French.
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
return [
	'values' => [
		'imediate' => 'immediat',
	],
	'categories' => [
		[ 'id' => 0, 'value' => 'CRM Société' ],
		[ 'id' => 1, 'value' => 'Besoins' ],
		[ 'id' => 2, 'value' => 'Projet' ],
		[ 'id' => 3, 'value' => 'Commandes' ],
		[ 'id' => 4, 'value' => 'Produits' ],
		[ 'id' => 5, 'value' => 'Achats' ],
		[ 'id' => 6, 'value' => 'Actions' ],
		[ 'id' => 7, 'value' => 'Ressources' ],
		[ 'id' => 8, 'value' => 'Candidats' ],
		[ 'id' => 9, 'value' => 'Positionnement' ],
		[ 'id' => 10, 'value' => 'CRM Contact' ],
		[ 'id' => 11, 'value' => 'Facturation' ],
		[ 'id' => 12, 'value' => 'Apps' ]
	],
	'application' => [
		'mail' => [
			'nouveau' => [
				'objet' => 'BoondManager > Connexion d\'un nouveau périphérique',
				'message' => "Bonjour,\n\nUne connexion sur votre compte a été réalisé depuis un nouveau périphérique.\n\nPour supprimer/superviser cet accès, connectez-vous depuis l'adresse suivante : {0} et dirigez-vous sur la page \"Ma fiche/Configuration/Sécurité\"."
			],
			 'code' => [
				 'objet' => 'BoondManager > Code d\'autorisation pour activer un nouveau périphérique',
				 'message' => "Bonjour,\n\nVous êtes en train de vous connecter avec un nouveau périphérique sur votre compte BoondManager.\n\nVeuillez saisir le code de sécurité suivant : {0}"
			 ],
		    'reset' => [
				'objet' => 'BoondManager > Mot de passe oublié ?',
			    'message' => "Bonjour,\n\nSuite à votre demande, si vous désirez réinitialiser votre mot de passe, veuillez vous rendre à l'adresse suivante : {0}"
				]
			],
		'device' => [
			'title' => 'Nouveau périphérique'
		],
	],
	'error' => include __DIR__.'/error.php',
	'warning' => include __DIR__.'/warning.php'
];
