<?php
/**
 * warning.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
return [
	'noContract' => 'No contract found',
	'overlappingContractDates' => 'Overlapping contract dates'
];
