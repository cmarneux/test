<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'values' => [
		'imediate' => 'immediate'
	],
	'tabs' => [
		'information' => 'General Information',
		'simulation' => 'Positioning',
		'projects' => 'Projects/Sells'
	],
	'sqlLabels' => [
		'id'                            => 'Reference',
		'reference'                     => 'Opportunity reference',
		'creationDate'                  => 'Date',
		'title'                         => 'Title',
		'typeOf'                        => 'Type',
		'CCON_NOM'                      => 'Client - Last Name',
		'CCON_PRENOM'                   => 'Client - First Name',
		'CSOC_SOCIETE'                  => 'Client - Company',
		'state'                         => 'State',
		'numberOfActivePositionings'    => 'Pos. Active',
		'activityAreas'                 => 'Area of activity',
		'place'                         => 'Place',
		'expertiseArea'                 => 'Area of expertise',
		'startDate'                     => 'Start',
		'duration'                      => 'Duration',
		'AO_TYPESOURCE'                 => 'Source',
		'currency'                      => 'Currency',
		'previsionnel'                  => 'Weig. turnover ET',
		'turnoverEstimatedExcludingTax' => 'Estimated turnover ET',
		'weighting'                     => 'Weighting',
		'mainManager'                   => 'Manager'
	]
];
