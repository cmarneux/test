<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */


return [
	'values' => [ //FIXME
		'imediate' => 'immediat'
	],
	'tabs' => [
		'information' => 'Informations Générales',
		'simulation' => 'Positionnements',
		'projects' => 'Projets/Ventes'
	],
	'sqlLabels' => [
		'id'                            => 'Référence',
		'reference'                     => 'Référence du besoin',
		'creationDate'                  => 'Date',
		'title'                         => 'Titre',
		'typeOf'                        => 'Type',
		'CCON_NOM'                      => 'Client - Nom',
		'CCON_PRENOM'                   => 'Client - Prénom',
		'CSOC_SOCIETE'                  => 'Client - Société',
		'state'                         => 'Etat',
		'numberOfActivePositionings'    => 'Pos. Actif',
		'activityAreas'                 => 'Domaine d\'application',
		'place'                         => 'Lieu',
		'expertiseArea'                 => 'Domaine d\'intervention',
		'startDate'                     => 'Début',
		'duration'                      => 'Durée',
		'AO_TYPESOURCE'                 => 'Provenance',
		'currency'                      => 'Devise',
		'previsionnel'                  => 'CA Pond. HT',
		'turnoverEstimatedExcludingTax' => 'CA Envisagé HT',
		'weighting'                     => 'Pondération',
		'mainManager'                   => 'Responsable'
	]
];
