<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'ID_PAIEMENT' => 'Reference',
		'PMT_DATE' => 'Date',
		'ACHAT_DATE' => 'Purchase Date',
		'ACHAT_REF' => 'Purchase reference',
		'ACHAT_TITLE' => 'Title',
		'CCON_NOM' => 'Provider - Last Name',
		'CCON_PRENOM' => 'Provider - First Name',
		'CSOC_SOCIETE' => 'Provider - Company',
		'ACHAT_REFOURNISSEUR' => 'Provider reference',
		'PRJ_REFERENCE' => 'Project',
		'ACHAT_TYPE' => 'Type',
		'ACHAT_CATEGORIE' => 'Category',
		'ACHAT_DEBUT' => 'Start',
		'ACHAT_FIN' => 'End',
		'PMT_REFPROVIDER' => 'Bill number',
		'PMT_ETAT' => 'State',
		'PMT_DATEPAIEMENTATTENDU' => 'Payment expected the',
		'PMT_DATEPAIEMENTEFFECTUE' => 'Payment performed',
		'ACHAT_DEVISE' => 'Currency',
		'PMT_MONTANTHT' => 'Amount ET',
		'payeTTC' => 'Amount IT',
		'ID_PROFIL' => 'Manager'
	]
];
