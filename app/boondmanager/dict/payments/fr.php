<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'ID_PAIEMENT' => 'Référence',
		'PMT_DATE' => 'Date',
		'ACHAT_DATE' => 'Date achat',
		'ACHAT_REF' => 'Référence achat',
		'ACHAT_TITLE' => 'Titre',
		'CCON_NOM' => 'Fournisseur - Nom',
		'CCON_PRENOM' => 'Fournisseur - Prénom',
		'CSOC_SOCIETE' => 'Fournisseur - Société',
		'ACHAT_REFOURNISSEUR' => 'Référence fournisseur',
		'PRJ_REFERENCE' => 'Projet',
		'ACHAT_TYPE' => 'Type',
		'ACHAT_CATEGORIE' => 'Categorie',
		'ACHAT_DEBUT' => 'Début',
		'ACHAT_FIN' => 'Fin',
		'PMT_REFPROVIDER' => 'Numéro Facture',
		'PMT_ETAT' => 'Etat',
		'PMT_DATEPAIEMENTATTENDU' => 'Paiement prévu',
		'PMT_DATEPAIEMENTEFFECTUE' => 'Paiement effectué',
		'ACHAT_DEVISE' => 'Devise',
		'PMT_MONTANTHT' => 'Montant HT',
		'payeTTC' => 'Montant TTC',
		'ID_PROFIL' => 'Responsable'
	]
];
