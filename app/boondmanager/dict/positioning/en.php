<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'id' => 'Reference',
		'updateDate' => 'Update',
		'profilLastName' => 'Resource/Candidate - Last Name',
		'profilFirstName' => 'Resource/Candidate - First Name',
		'dependsType' => 'Profile',
		'title' => 'Opportunity',
		'reference' => 'Opportunity reference',
		'typeOf' => 'Type',
		'state' => 'State',
		'currency' => 'Currency',
		'ca' => 'Turnover ET',
		'marge' => 'Margin ET',
		'renta' => 'Profit',
		'informationComments' => 'Comment',
		'contactLastName' => 'Client - Last Name',
		'contactFirstName' => 'Client - First Name',
		'company' => 'Client - Company',
		'mainManager' => 'Manager',
		'profilHrManager' => 'RH Manager',
		'profilMainManager' => 'Main Manager'
	]
];
