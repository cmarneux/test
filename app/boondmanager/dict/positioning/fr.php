<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'id' => 'Référence',
		'updateDate' => 'MAJ',
		'profilLastName' => 'Ressource/Candidat - Nom',
		'profilFirstName' => 'Ressource/Candidat - Prenom',
		'dependsType' => 'Profil',
		'title' => 'Besoin',
		'reference' => 'Référence du besoin',
		'typeOf' => 'Type',
		'state' => 'Etat',
		'currency' => 'Devise',
		'ca' => 'CA HT',
		'marge' => 'Marge HT',
		'renta' => 'Renta.',
		'informationComments' => 'Commentaire',
		'contactLastName' => 'Client - Nom',
		'contactFirstName' => 'Client - Prénom',
		'company' => 'Client - Société',
		'mainManager' => 'Responsable',
		'profilHrManager' => 'Responsable RH',
		'profilMainManager' => 'Responsable Manager'
	]
];
