<?php
/**
 * en.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

return [
	'tabs' => [
		\BoondManager\Models\Product::TAB_INFORMATION  => 'Information',
		\BoondManager\Models\Product::TAB_ACTIONS      => 'Actions',
		\BoondManager\Models\Product::TAB_OPPORTUNITIES=> 'Opportunities',
		\BoondManager\Models\Product::TAB_PROJECTS     => 'Projects'
	],
	'sqlLabels' => [
		'id' => "Reference",
		'name' => 'Name',
		'reference' => 'Product reference',
		'mainManager' => 'Manager',
		'subscription' => 'Type',
		'state' => 'State',
		'description' => 'Description',
		'priceExcludingTax' => 'Rate',
		'taxRate' => 'VAT Rate',
		'currency' => 'Currency',
		'agency' => 'Agency',
		'files' => 'Attachments'
	],
	'notifications'=>[
		'specials' => [
			'changefield' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'update' => 'Modification of tab "{0}"',
			'delete' => 'Suppression of product {0}',
			'addDocument' => 'Adding document "{0}"',
			'deleteDocument'=>'Removing document "{0}"'
		]
	]
];
