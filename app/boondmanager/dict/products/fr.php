<?php
/**
 * fr.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

return [
	'tabs' => [
		\BoondManager\Models\Product::TAB_INFORMATION   => 'Informations',
		\BoondManager\Models\Product::TAB_ACTIONS       => 'Actions',
		\BoondManager\Models\Product::TAB_OPPORTUNITIES => 'Besoins',
		\BoondManager\Models\Product::TAB_PROJECTS      => 'Projets'
	],
	'sqlLabels' => [
		'id' => "Référence",
		'name' => 'Nom',
		'reference' => 'Référence du produit',
		'mainManager' => 'Responsable Manager',
		'subscription' => 'Type',
		'state' => 'Etat',
		'description' => 'Description',
		'priceExcludingTax' => 'Tarif',
		'taxRate' => 'Taux TVA',
		'currency' => 'Devise',
		'agency' => 'Agence',
		'files' => 'Pièces Jointes'
	],
	'notifications'=>[
		'specials' => [
			'changeField' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'update' => 'Modification de l\'onglet "{0}"',
			'delete' => 'Suppression du produit {0}',
			'addDocument' => 'Ajout du document "{0}"',
			'deleteDocument' => 'Suppression du document "{0}"'
		]
	]
];
