<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [

		'ID_PROJET' => 'Reference',
		'PRJ_DEBUT' => 'Start',
		'PRJ_FIN' => 'End',

		'PRJ_REFERENCE' => 'Project',
		'PRJ_TYPEREF' => 'Type',

		'PRJ_ETAT' => 'State',

		'AO_TITLE' => 'Opportunity',
		'CCON_NOM' => 'Client - Last Name',
		'CCON_PRENOM' => 'Client - First Name',
		'CSOC_SOCIETE' => 'Client - Company',
		'PRJ_DEVISE' => 'Currency',

		'TOTAL_CA' => 'Turnover ET',
		'TOTAL_MARGE' => 'Margin ET',
		'TOTAL_RENTA' => 'Profit.',
		'PRJ_CP' => 'Postcode',
		'PRJ_VILLE' => 'Town',
		'ID_PROFIL' => 'Manager'
	]
];
