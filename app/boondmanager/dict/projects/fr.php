<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'ID_PROJET' => 'Référence',
		'PRJ_DEBUT' => 'Début',
		'PRJ_FIN' => 'Fin',

		'PRJ_REFERENCE' => 'Projet',
		'PRJ_TYPEREF' => 'Type',
		
		'PRJ_ETAT' => 'Etat',
		
		'AO_TITLE' => 'Besoin',
		'CCON_NOM' => 'Client - Nom',
		'CCON_PRENOM' => 'Client - Prénom',
		'CSOC_SOCIETE' => 'Client - Société',
		'PRJ_DEVISE' => 'Devise',
		
		'TOTAL_CA' => 'CA HT',
		'TOTAL_MARGE' => 'Marge HT',
		'TOTAL_RENTA' => 'Renta.',
		'PRJ_CP' => 'Code postal',
		'PRJ_VILLE' => 'Ville',
		'ID_PROFIL' => 'Responsable',
	]
];
