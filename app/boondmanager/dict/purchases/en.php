<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'id' => 'Reference',
		'date' => 'Date',
		'reference' => 'Purchase reference',
		'title' => 'Title',
		'CCON_NOM' => 'Provider - Last Name',
		'CCON_PRENOM' => 'Provider - First Name',
		'company' => 'Provider - Company',
		'number' => 'Provider reference',
		'PRJ_REFERENCE' => 'Project',
		'subscription' => 'Type',
		'typeOf' => 'Category',
		'startDate' => 'Start',
		'endDate' => 'End',
		'state' => 'State',
		'currency' => 'Currency',
		'quantity' => 'Quantity',
		'amountExcludingTax' => 'Amount ET',
		'totalHT' => 'Purchase Total ET',
		'totalTTC' => 'Purchase Total IT',
		'engagedPaymentsAmountExcludingTax' => 'Engaged Payments ET',
		'payeTTC' => 'Engaged Payments IT',
		'deltaHT' => 'Delta ET',
		'deltaTTC' => 'Delta IT',
		'paymentsAmountExcludingTax' => 'Payments Total ET',
		'paymentsTTC' => 'Payments Total IT',
		'reinvoiceAmountExcludingTax' => 'Rebilled Total ET',
		'refactureTTC' => 'Rebilled Total IT',
		'mainManager' => 'Manager',
	]
];
