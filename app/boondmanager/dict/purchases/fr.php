<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'id' => 'Référence',
		'date' => 'Date',
		'reference' => 'Référence achat',
		'title' => 'Titre',
		'contact' => 'Contact',
		'CCON_NOM' => 'Fournisseur - Nom',
		'CCON_PRENOM' => 'Fournisseur - Prénom',
		'company' => 'Fournisseur - Société',
		'number' => 'Référence fournisseur',
		'project' => 'Projet',
		'PRJ_REFERENCE' => 'Projet',
		'subscription' => 'Unitaire',
		'typeOf' => 'Catégorie',
		'startDate' => 'Début',
		'endDate' => 'Fin',
		'state' => 'Etat',
		'currency' => 'Devise',
		'quantity' => 'Quantité',
		'amountExcludingTax' => 'Montant HT',
		'totalHT' => 'Total Achat HT',
		'totalTTC' => 'Total Achat TTC',
		'engagedPaymentsAmountExcludingTax' => 'Paiments Engagés HT',
		'payeTTC' => 'Paiments Engagés TTC',
		'deltaHT' => 'Delta HT',
		'deltaTTC' => 'Delta TTC',
		'paymentsAmountExcludingTax' => 'Total Paiements HT',
		'paymentsTTC' => 'Total Paiements TTC',
		'reinvoiceAmountExcludingTax' => 'Total Refacturé HT',
		'refactureTTC' => 'Total Refacturé TTC',
		'toReinvoice' => ' Achat à refacturer',
		'mainManager' => 'Responsable',
	],
	'notifications'=>[
		'actions' => [
			'update' => 'Modification de l\'onglet "{0}"',
			'delete' => 'Suppression de l\'achat {0}',
			'addDocument' => 'Ajout du document "{0}"',
			'deleteDocument' => 'Suppression du document "{0}"'
		]
	]
];
