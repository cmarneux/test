<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'tabs' => [
		\BoondManager\Models\Employee::TAB_INFORMATION      => 'Informations',
		\BoondManager\Models\Employee::TAB_ADMINISTRATIVE   => 'Administratif',
		\BoondManager\Models\Employee::TAB_TECHNICALDATA               => 'DT',
		\BoondManager\Models\Employee::TAB_ACTIONS          => 'Actions',
		\BoondManager\Models\Employee::TAB_POSITIONINGS     => 'Positionnements',
		\BoondManager\Models\Employee::TAB_DELIVERIES       => 'Prestations',
		\BoondManager\Models\Employee::TAB_TIMESREPORTS       => 'Temps',
		\BoondManager\Models\Employee::TAB_EXPENSESREPORTS         => 'Frais',
		\BoondManager\Models\Employee::TAB_ABSENCESREPORTS         => 'Absences',
		\BoondManager\Models\Employee::TAB_ABSENCESACCOUNTS => 'Decomptes'
	],
	'values' => [ //TODO utiliser les trads de specific
		'imediateAvailability' => 'immediate',
		'gender' => [ 'Mr', 'Mme', 'Mlle'],
	],
	'sqlLabels' => [
		'id' => "Référence",
		'lastName' => 'Nom',
		'firstName' => 'Prénom',
		'reference' => 'Matricule',
		'civility' => 'Civilité',
		'email1' => 'Email',
		'email2' => 'Email 2',
		'email3' => 'Email 3',
		'phone1' => 'Téléphone',
		'phone2' => 'Téléphone 2',
		'phone3' => 'Téléphone 3',
		'typeOf' => 'Type',
		'state' => 'Etat',
		'dateOfBirth' => 'Date de naissance',
		'nationality' => 'Nationalité',
		'situation' => 'Situation familiale',
		'numberOfActivePositionings' => 'Pos. Actif',
		'address'=> 'Adresse',
		'postcode'=> 'Code Postal',
		'town'=> 'Ville',
		'country'=> 'Pays',
		'title' => 'Titre',
		'training' => 'Formation',
		'experience' => 'Expérience',
		'skills' => 'Compétences',
		'activityAreas' => 'Domaine d\'application',
		'mainManager' => 'Responsable Manager',
		'hrManager' => 'Responsable RH',
		'agency' => 'Agence',
		'availability' => 'Disponibilité',
		'currency' => 'Devise',
		'averageDailyPriceExcludingTax' => 'Tarif HT',
		'mobilityAreas' => 'Mobilité',
	],
	'notifications'=>[
		'specials' => [
			'changeField' => '{fieldname}: {oldvalue} -> {newvalue}',
			'actionMessage' => "{title}:\n{list|, }"
		],
		'actions' => [
			'update' => 'Modification de l\'onglet "{0}"',
			'delete' => 'Suppression de la ressource {0}',
			'addCV' => 'Ajout du CV "{0}"',
			'deleteCV' => 'Suppression du CV "{0}"',
			'addDocument' => 'Ajout du document "{0}"',
			'deleteDocument' => 'Suppression du document "{0}"',
			'position' => 'Positionnement sur le besoin "{0}"'
		]
	]
];
