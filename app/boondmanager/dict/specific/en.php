<?php
return array (
  'setting' =>
  array (
    'action' =>
    array (
      'forceMultiCreation' => true,
      'sort' => false,
      'contact' =>
      array (
        0 =>
        array (
          'id' => 2,
          'value' => 'Comment',
        ),
        1 =>
        array (
          'id' => 3,
          'value' => 'Todo',
        ),
        2 =>
        array (
          'id' => 10,
          'value' => 'Client Presentation',
        ),
        3 =>
        array (
          'id' => 11,
          'value' => 'Monitoring Mission',
        ),
        4 =>
        array (
          'id' => 60,
          'value' => 'Meeting',
        ),
        5 =>
        array (
          'id' => 61,
          'value' => 'Call',
        ),
        6 =>
        array (
          'id' => 62,
          'value' => 'Email',
        ),
        7 =>
        array (
          'id' => 63,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 64,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 65,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 120,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 121,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 122,
          'value' => '',
        ),
        13 =>
        array (
          'id' => 123,
          'value' => '',
        ),
        14 =>
        array (
          'id' => 124,
          'value' => '',
        ),
        15 =>
        array (
          'id' => 101,
          'value' => 'Notification',
        ),
      ),
      'candidate' =>
      array (
        0 =>
        array (
          'id' => 13,
          'value' => 'Comment',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Todo',
        ),
        2 =>
        array (
          'id' => 0,
          'value' => 'Client Presentation',
        ),
        3 =>
        array (
          'id' => 12,
          'value' => 'Phone Interview',
        ),
        4 =>
        array (
          'id' => 40,
          'value' => 'Physical Interview',
        ),
        5 =>
        array (
          'id' => 41,
          'value' => 'Call',
        ),
        6 =>
        array (
          'id' => 42,
          'value' => 'Email',
        ),
        7 =>
        array (
          'id' => 43,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 44,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 45,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 130,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 131,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 132,
          'value' => '',
        ),
        13 =>
        array (
          'id' => 133,
          'value' => '',
        ),
        14 =>
        array (
          'id' => 134,
          'value' => '',
        ),
        15 =>
        array (
          'id' => 100,
          'value' => 'Notification',
        ),
      ),
      'resource' =>
      array (
        0 =>
        array (
          'id' => 4,
          'value' => 'Comment',
        ),
        1 =>
        array (
          'id' => 5,
          'value' => 'Todo',
        ),
        2 =>
        array (
          'id' => 8,
          'value' => 'Client Presentation',
        ),
        3 =>
        array (
          'id' => 9,
          'value' => 'Monitoring Mission',
        ),
        4 =>
        array (
          'id' => 80,
          'value' => 'Annual HR Interview',
        ),
        5 =>
        array (
          'id' => 81,
          'value' => 'Call',
        ),
        6 =>
        array (
          'id' => 82,
          'value' => 'Email',
        ),
        7 =>
        array (
          'id' => 83,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 84,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 85,
          'value' => 'Medical Visit',
        ),
        10 =>
        array (
          'id' => 140,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 141,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 142,
          'value' => '',
        ),
        13 =>
        array (
          'id' => 143,
          'value' => '',
        ),
        14 =>
        array (
          'id' => 144,
          'value' => '',
        ),
        15 =>
        array (
          'id' => 102,
          'value' => 'Notification',
        ),
      ),
      'opportunity' =>
      array (
        0 =>
        array (
          'id' => 50,
          'value' => 'Comment',
        ),
        1 =>
        array (
          'id' => 51,
          'value' => 'Todo',
        ),
        2 =>
        array (
          'id' => 52,
          'value' => 'Client Presentation',
        ),
        3 =>
        array (
          'id' => 53,
          'value' => 'Call',
        ),
        4 =>
        array (
          'id' => 54,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 55,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 56,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 57,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 58,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 59,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 103,
          'value' => 'Notification',
        ),
      ),
      'project' =>
      array (
        0 =>
        array (
          'id' => 30,
          'value' => 'Comment',
			'collaborative' => false,
        ),
        1 =>
        array (
          'id' => 31,
          'value' => 'Todo',
			'collaborative' => false,
        ),
        2 =>
        array (
          'id' => 32,
          'value' => 'Monitoring Mission',
			'collaborative' => false,
        ),
        3 =>
        array (
          'id' => 33,
          'value' => 'Call',
			'collaborative' => false,
        ),
        4 =>
        array (
          'id' => 34,
          'value' => '',
			'collaborative' => false,
        ),
        5 =>
        array (
          'id' => 35,
          'value' => '',
			'collaborative' => false,
        ),
        6 =>
        array (
          'id' => 36,
          'value' => '',
			'collaborative' => false,
        ),
        7 =>
        array (
          'id' => 37,
          'value' => '',
			'collaborative' => false,
        ),
        8 =>
        array (
          'id' => 38,
          'value' => '',
			'collaborative' => false,
        ),
        9 =>
        array (
          'id' => 39,
          'value' => '',
			'collaborative' => false,
        ),
		10 =>
		  array (
			  'id' => 90,
			  'value' => 'Project Management',
			  'collaborative' => true,
		  ),
        11 =>
        array (
          'id' => 104,
          'value' => 'Notification',
			'collaborative' => false,
        ),
      ),
      'order' =>
      array (
        0 =>
        array (
          'id' => 20,
          'value' => 'Comment',
        ),
        1 =>
        array (
          'id' => 21,
          'value' => 'Todo',
        ),
        2 =>
        array (
          'id' => 22,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 23,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 24,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 25,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 26,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 27,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 28,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 29,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 105,
          'value' => 'Notification',
        ),
      ),
      'invoice' =>
      array (
        0 =>
        array (
          'id' => 70,
          'value' => 'Comment',
        ),
        1 =>
        array (
          'id' => 71,
          'value' => 'Todo',
        ),
        2 =>
        array (
          'id' => 72,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 73,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 74,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 75,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 76,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 77,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 78,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 79,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 110,
          'value' => 'Notification',
        ),
      ),
      'company' =>
      array (
        0 =>
        array (
          'id' => 106,
          'value' => 'Notification',
        ),
      ),
      'product' =>
      array (
        0 =>
        array (
          'id' => 107,
          'value' => 'Notification',
        ),
      ),
      'purchase' =>
      array (
        0 =>
        array (
          'id' => 108,
          'value' => 'Notification',
        ),
      ),
      'app' =>
      array (
        0 =>
        array (
          'id' => 109,
          'value' => 'Notification',
        ),
      ),
    ),
    'state' =>
    array (
      'sort' => false,
      'candidate' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'To deal with !',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'In qualification',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Pool++',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 10,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 11,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 12,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 13,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 14,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 9,
          'value' => 'Pool',
        ),
        13 =>
        array (
          'id' => 3,
          'value' => 'Converted to Resource',
        ),
        14 =>
        array (
          'id' => 5,
          'value' => 'Do not contact anymore',
        ),
      ),
      'resource' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'Current',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => 'Intercontract',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Went out',
        ),
      ),
      'product' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'Current',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Archived',
        ),
      ),
      'opportunity' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Current',
          'active' => true,
        ),
        1 =>
        array (
          'id' => 4,
          'value' => '',
          'active' => true,
        ),
        2 =>
        array (
          'id' => 5,
          'value' => '',
          'active' => true,
        ),
        3 =>
        array (
          'id' => 6,
          'value' => '',
          'active' => true,
        ),
        4 =>
        array (
          'id' => 7,
          'value' => '',
          'active' => true,
        ),
        5 =>
        array (
          'id' => 8,
          'value' => '',
          'active' => true,
        ),
        6 =>
        array (
          'id' => 10,
          'value' => '',
          'active' => true,
        ),
        7 =>
        array (
          'id' => 11,
          'value' => '',
          'active' => true,
        ),
        8 =>
        array (
          'id' => 12,
          'value' => '',
          'active' => true,
        ),
        9 =>
        array (
          'id' => 13,
          'value' => '',
          'active' => true,
        ),
        10 =>
        array (
          'id' => 14,
          'value' => '',
          'active' => true,
        ),
        11 =>
        array (
          'id' => 9,
          'value' => 'Postponed',
          'active' => true,
        ),
        12 =>
        array (
          'id' => 1,
          'value' => 'Won',
          'active' => false,
        ),
        13 =>
        array (
          'id' => 2,
          'value' => 'Lost',
          'active' => false,
        ),
        14 =>
        array (
          'id' => 3,
          'value' => 'Abandoned',
          'active' => false,
        ),
      ),
      'positioning' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Positionned',
          'active' => true,
        ),
        1 =>
        array (
          'id' => 3,
          'value' => 'CV Sent',
          'active' => true,
        ),
        2 =>
        array (
          'id' => 4,
          'value' => 'Client Interviewed',
          'active' => true,
        ),
        3 =>
        array (
          'id' => 5,
          'value' => '',
          'active' => true,
        ),
        4 =>
        array (
          'id' => 6,
          'value' => '',
          'active' => true,
        ),
        5 =>
        array (
          'id' => 7,
          'value' => '',
          'active' => true,
        ),
        6 =>
        array (
          'id' => 8,
          'value' => '',
          'active' => true,
        ),
        7 =>
        array (
          'id' => 9,
          'value' => '',
          'active' => true,
        ),
        8 =>
        array (
          'id' => 1,
          'value' => 'Rejected',
          'active' => false,
        ),
        9 =>
        array (
          'id' => 2,
          'value' => 'Win',
          'active' => false,
        ),
      ),
      'project' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'Current',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Archived',
        ),
      ),
      'delivery' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Signed',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 1,
          'value' => 'Forecast',
        ),
      ),
      'contact' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Prospect',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Client',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 10,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 11,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 12,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 13,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 14,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 7,
          'value' => 'Partner',
        ),
        13 =>
        array (
          'id' => 9,
          'value' => 'Provider',
        ),
        14 =>
        array (
          'id' => 8,
          'value' => 'Archived',
        ),
      ),
      'company' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Prospect',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Client',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 10,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 11,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 12,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 13,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 14,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 7,
          'value' => 'Partner',
        ),
        13 =>
        array (
          'id' => 9,
          'value' => 'Provider',
        ),
        14 =>
        array (
          'id' => 8,
          'value' => 'Archived',
        ),
      ),
      'order' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'Current',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Archived',
        ),
      ),
      'invoice' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Creation',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Transmitted to the Client',
        ),
        2 =>
        array (
          'id' => 4,
          'value' => 'Recovery 1',
        ),
        3 =>
        array (
          'id' => 5,
          'value' => 'Recovery 2',
        ),
        4 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 9,
          'value' => 'Mail to client',
        ),
        8 =>
        array (
          'id' => 2,
          'value' => 'Not paid',
        ),
        9 =>
        array (
          'id' => 3,
          'value' => 'Paid',
        ),
      ),
      'purchase' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => '',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Validated',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => 'Planned',
        ),
      ),
      'payment' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Planned',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Confirmed',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Well-ordered',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => '',
        ),
      ),
      'quotation' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Creation',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Transmitted to the client',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Waiting',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => 'Refused',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => 'Agreement received',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => 'Archived',
        ),
      ),
    ),
    'currency' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'EUR Euro',
        'symbol' => '€',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'USD United States Dollars',
        'symbol' => '$',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'GPB United Kingdom Pounds',
        'symbol' => '£',
      ),
    ),
    'calendar' =>
    array (
      0 => 'France_Sans_Pentecote',
      1 => 'Royaume_Uni',
    ),
    'taxRate' =>
    array (
      0 => '20',
      1 => '10',
      2 => '0',
    ),
    'paymentTerm' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 0,
        'x' => 0,
        'y' => 0,
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 1,
        'x' => 30,
        'y' => 0,
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 2,
        'x' => 30,
        'y' => 0,
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 3,
        'x' => 30,
        'y' => 10,
      ),
      4 =>
      array (
        'id' => 41,
        'value' => 1,
        'x' => 45,
        'y' => 0,
      ),
      5 =>
      array (
        'id' => 52,
        'value' => 2,
        'x' => 45,
        'y' => 0,
      ),
    ),
    'paymentMethod' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Transfer',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Debit',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Bank check',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 'Credit card',
      ),
    ),
    'defaultOpportunityTypeCreated' => 1,
    'defaultPositioningSearchModule' => 'candidates',
    'filterPositioningSearchModule' => false,
    'defaultMail' =>
    array (
      'quotation' =>
      array (
        'object' => 'Quotation from the company _devissociete_',
        'body' => 'Hello,

        Please find enclosed the quotation _devisref_ linked to the opportunity _aoref_.',
      ),
      'invoice' =>
      array (
        'object' => 'Bill of company _factsociete_ - _factref_',
        'body' => 'Hello,

        Please find enclosed the bill _factref_ attached to the command _cmdref_.',
      ),
      'timesReports' =>
      array (
        'object' => 'Timesheet entry',
        'body' => 'Hello _profilprenom_ _profilnom_,

Please log on to your company\'s intranet from the following address : _intranetlink_ in order to enter your timesheet for the current month.

If you do not have your log on details, please contact your manager.

Please do not answer this email.',
      ),
      'expensesReports' =>
      array (
        'object' => 'Expenses entry',
        'body' => 'Hello _profilprenom_ _profilnom_,

Please log on to your company\'s intranet from the following address : _intranetlink_ in order to enter your expenses for the current month.

If you do not have your log on details, please contact your manager.

Please do not answer this email.',
      ),
      'timesReportsAndExpensesReports' =>
      array (
        'object' => 'Timesheet and expenses entry',
        'body' => 'Hello _profilprenom_ _profilnom_,

Please log on to your company\'s intranet from the following address : _intranetlink_ in order to enter your timesheet and expenses for the current month.

If you do not have your log on details, please contact your manager.

Please do not answer this email.',
      ),
      'deliveryOrder' =>
      array (
        'object' => 'Mission order - [MISSION_REFERENCE]',
        'body' => 'Hello,

        Please find enclosed your mission order [MISSION_REFERENCE] for the project [PROJECT_REF].',
      ),
    ),
    'smoothAdditionalData' => true,
    'profitabilityMethodOfCalculating' => 'brandRate',
    'showOwnOrderReference' => false,
    'itemInvoice' =>
    array (
      'monthly' =>
      array (
        0 =>
        array (
          'id' => '1',
          'value' => 'Service delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        1 =>
        array (
          'id' => '1_3',
          'value' => 'Exceptionnal delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        2 =>
        array (
          'id' => '1_4',
          'value' => 'Exceptionnal delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        3 =>
        array (
          'id' => '2',
          'value' => 'Service delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        4 =>
        array (
          'id' => '2_3',
          'value' => 'Exceptionnal delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        5 =>
        array (
          'id' => '2_4',
          'value' => 'Exceptionnal delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        6 =>
        array (
          'id' => '3',
          'value' => 'Service delivery, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        7 =>
        array (
          'id' => '4',
          'value' => '[PRODUCT_NAME] - [PRODUCT_REF], [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
      ),
      'schedule' =>
      array (
        0 =>
        array (
          'id' => '1',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
        1 =>
        array (
          'id' => '2',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
        2 =>
        array (
          'id' => '3',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
        3 =>
        array (
          'id' => '4',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
      ),
    ),
    'showLogoCompany' => false,
    'markdownTextDashboard' => '__Welcome on BoondManager__
> This interface allow you to :
- View/Edit your [technical file](mon-compte/fiche?onglet=2) (TF)
- Fill your [timesheets](mon-compte/fiche?onglet=6) & your [expenses](mon-compte/fiche?onglet=7)
- Perform your [leave requests](mon-compte/fiche?onglet=8)
- View your [projects](tableau-de-bord/liste-projets)

#Navigation
> Up sidebar gives you access to your profile and your list of projects.
You can return at any time on this page by clicking your first name _(Top of the interface side of the head of the fox)_.

#Your profile - Technical File (TF)
> You can view your TF and edit it _(Only if your manager has enabled this feature)_.

#Your profile - TImesheets, Expenses & Leaves
> - Select the document to create or edit
- Enter information document during this period
- Click "Create" or "Save" to save your entries without notifying the next validator
- For timesheets and expense reports, attach your documents
_(Tip: Scan your receipts pasting them on a sheet of white A4)_
- Click "Validate" to warn the next validator
- You can only edit documents that have not been validated by the validator next

#Projets
> - You can view a list of all your projects
- On projects which you are "Project Manager", you can view project data, PDF timesheets of resources, and, only on packages, update the "Remains to be done" & markers',
    'deliveryOrder' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Mission order default',
        'maskSignatureBlock' => false,
        'insert' =>
        array (
          0 =>
          array (
            'value' => 'Your obligations',
            'option' =>
            array (
              0 => '- You must read and respect the rules in force at the customer',
              1 => '- You must read and comply with specific safety regulations at the customer',
              2 => '- You must respect the confidentiality vis-à-vis the information obtained from the client or your employer',
            ),
          ),
          1 =>
          array (
            'value' => 'During the mission',
            'option' =>
            array (
              0 => '- End of each month, make a monthly activity sheet for the past month, have it signed by your project manager, and send it to your employer',
              1 => '- For administrative issues, contact your department assistant',
            ),
          ),
          2 =>
          array (
            'value' => 'Leave',
            'option' =>
            array (
              0 => '- Any foreseeable leave must be reported to your employer as soon as possible',
              1 => '- Especially for the holidays :',
              2 => '#for the expected holiday period, consider the mission requirements in consultation with the client',
              3 => '#make a written request for leave and send it to your employer',
              4 => '#the agreement of your employer, confirming the period of leave to the customer',
              5 => '- Unplanned leaves (illness, accident, acts of God ...) must be reported as soon as possible to your employer and client',
            ),
          ),
          3 =>
          array (
            'value' => 'Organisation of working time',
            'option' =>
            array (
              0 => '- Is the provider to 35H weekly effective ? NO',
              1 => '- Description: You must follow the organization of working time from your employer as described in the company agreement to reduce working time',
              2 => '- You agree to comply with the days of closed society of the customer, and decision-related leave',
            ),
          ),
          4 =>
          array (
            'value' => 'Mission description',
            'option' =>
            array (
              0 => '[MISSION_DESCRIPTION]',
            ),
          ),
          5 =>
          array (
            'value' => 'Special conditions',
            'option' =>
            array (
              0 => '[MISSION_CONDITIONS]',
            ),
          ),
        ),
      ),
    ),
    'sharingEntity' =>
    array (
      'credentials' =>
      array (
        'object' => 'Sharing access to BoondManager',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello [RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME],

To facilitate the collection and analysis of operational data of our company, we decided to collect your timesheets, your expenses and your absence requests via an application online business : BoondManager.

Here is the link to your login page: [BOONDMANAGER_URL].

Your login is: "[RESOURCE_LOGIN]".


This is your first connection ?
Thank you to click this link to set your password : [BOONDMANAGER_RESET]

You can also connect from your smartphone by downloading our Mobile App available on the marketplace of Google, Apple & Windows.

Each month you will receive an emailing to get you to qualify your timesheets (possibly also your expenses).

To help you in the handling of BoondManager, here are some tutorials that we invite you to visit:
https://support.boondmanager.com/hc/fr/articles/206473535-Introduction
https://support.boondmanager.com/hc/fr/articles/205743149-Renseigner-une-feuille-des-temps
https://support.boondmanager.com/hc/fr/articles/206455995-Renseigner-une-note-de-frais
https://support.boondmanager.com/hc/fr/articles/205743159-Faire-une-demande-d-absence

Do not hesitate to contact me for further information.

Regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]
[MANAGER_EMAIL]
[MANAGER_PHONE]',
      ),
      'resource' =>
      array (
        'object' => 'Sharing the Resource profile "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Resource profile :
[RESOURCE_URL]

Email : [RESOURCE_EMAIL1]
Phone : [RESOURCE_PHONE1]
Title : [RESOURCE_TITLE]
Availability : [RESOURCE_AVAILABILITY]
Area of activities : [RESOURCE_ACTIVITIES]
Area of expertises : [RESOURCE_EXPERTISES]
Experience : [RESOURCE_EXPERIENCE]
Training : [RESOURCE_TRAINING]
Tools : [RESOURCE_TOOLS]
Skills : [RESOURCE_SKILLS]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'opportunity' =>
      array (
        'object' => 'Sharing the Opportunity profile "[OPPORTUNITY_TITLE]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Opportunity profile :
[OPPORTUNITY_URL]

Type : [OPPORTUNITY_TYPE]
State : [OPPORTUNITY_STATE]
Start : [OPPORTUNITY_START]
Criteria required : [OPPORTUNITY_CRITERIA]
Area of activity : [OPPORTUNITY_ACTIVITY]
Area of expertise : [OPPORTUNITY_EXPERTISE]
Estimated turnover : [OPPORTUNITY_ESTIMATEDTURNOVER]
Projected budget : [OPPORTUNITY_PROJECTEDBUDGET]
Customer : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Company : [CRMCOMPANY_NAME]
Description : [OPPORTUNITY_DESCRIPTION]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'candidate' =>
      array (
        'object' => 'Sharing the Candidate profile "[CANDIDATE_FIRSTNAME] [CANDIDATE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Candidate profile :
[CANDIDATE_URL]

Email : [CANDIDATE_EMAIL1]
Phone : [CANDIDATE_PHONE1]
Title : [CANDIDATE_TITLE]
Availability : [CANDIDATE_AVAILABILITY]
Global evaluation : [CANDIDATE_EVALUATION]
Actual salary : [CANDIDATE_ACTUALSALARY]
Desired salary : [CANDIDATE_DESIREDSALARY]
Contract : [CANDIDATE_CONTRACT]
Nationality : [CANDIDATE_NATIONALITY]
Date of birth : [CANDIDATE_DATEOFBIRTH]
Source : [CANDIDATE_SOURCE]
Area of activities : [CANDIDATE_ACTIVITIES]
Area of expertises : [CANDIDATE_EXPERTISES]
Experience : [CANDIDATE_EXPERIENCE]
Training : [CANDIDATE_TRAINING]
Tools : [CANDIDATE_TOOLS]
Skills : [CANDIDATE_SKILLS]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'contact' =>
      array (
        'object' => 'Sharing the CRM Contact profile "[CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME] - [CRMCOMPANY_NAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following CRM Contact profile :
[CRMCONTACT_URL]

Email : [CRMCONTACT_EMAIL1]
Phone : [CRMCONTACT_PHONE1]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'company' =>
      array (
        'object' => 'Sharing the CRM Company profile "[CRMCOMPANY_NAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following CRM Company profile :
[CRMCOMPANY_URL]

Website : [CRMCOMPANY_WEBSITE]
Phone : [CRMCOMPANY_PHONE]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'project' =>
      array (
        'object' => 'Sharing the Project profile "[PROJECT_REF]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Project profile :
[PROJECT_URL]

Type : [OPPORTUNITY_TYPE]
Start : [PROJECT_START]
End : [PROJECT_END]
Customer : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Company : [CRMCOMPANY_NAME]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'product' =>
      array (
        'object' => 'Sharing the Product profile "[PRODUCT_NAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Product profile :
[PRODUCT_URL]

Product reference : [PRODUCT_REF]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'purchase' =>
      array (
        'object' => 'Sharing the Purchase profile "[PURCHASE_TITLE]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Purchase profile :
[PURCHASE_URL]

Type : [PURCHASE_TYPE]
Start : [PURCHASE_START]
End : [PURCHASE_END]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'payment' =>
      array (
        'object' => 'Sharing the Payment profile "[PAYMENT_REFERENCE]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Payment profile :
[PAYMENT_URL]

Date : [PAYMENT_DATE]
Amount : [PAYMENT_AMOUNT]
Purchase : [PURCHASE_TITLE]
Type : [PURCHASE_TYPE]
Start : [PURCHASE_START]
End : [PURCHASE_END]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'positioning' =>
      array (
        'object' => 'Sharing the Positioning profile "[POSITIONING_REFERENCE]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Positioning profile :
[POSITIONING_URL]

Start : [POSITIONING_START]
End : [POSITIONING_END]
Opportunity : [OPPORTUNITY_TITLE]
Type : [OPPORTUNITY_TYPE]
Start estimated on opportunity profile : [OPPORTUNITY_START]
Duration estimated on opportunity profile : [OPPORTUNITY_DURATION]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'delivery' =>
      array (
        'object' => 'Sharing the Mission profile "[MISSION_REFERENCE]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Mission profile :
[MISSION_URL]

Project/Type : [MISSION_TYPE]
Start : [MISSION_START]
End : [MISSION_END]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'contract' =>
      array (
        'object' => 'Sharing the Contract profile of resource"[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Contract profile :
[CONTRACT_URL]

Category : [CONTRACT_CATEGORY]
Start : [CONTRACT_START]
End : [CONTRACT_END]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'advantage' =>
      array (
        'object' => 'Sharing the Advantage profile of resource"[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Advantage profile :
[ADVANTAGE_URL]

Type : [ADVANTAGE_TYPE]
Category : [ADVANTAGE_CATEGORY]
Date : [ADVANTAGE_DATE]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'invoice' =>
      array (
        'object' => 'Sharing the Bill profile "[BILL_REF]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Bill profile :
[BILL_URL]

State : [BILL_STATE]
Order : [ORDER_REF]
Project : [PROJECT_REF]
Customer : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Company : [CRMCOMPANY_NAME]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'order' =>
      array (
        'object' => 'Sharing the Order profile "[ORDER_REF]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Order profile :
[ORDER_URL]

Project : [PROJECT_REF]
Customer : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Company : [CRMCOMPANY_NAME]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'timesheet' =>
      array (
        'object' => 'Sharing the Timesheet profile "[TIMESHEET_PERIOD]" of resource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Timesheet profile :
[TIMESHEET_URL]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'expenses' =>
      array (
        'object' => 'Sharing the Expenses profile "[EXPENSES_PERIOD]" of resource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Expenses profile :
[EXPENSES_URL]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'absencesRequest' =>
      array (
        'object' => 'Sharing the Leave Request profile "[ABSENCE_DATE]" of resource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following Leave Request profile :
[ABSENCE_URL]

Title : [ABSENCE_TITLE]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'action' =>
      array (
        'object' => 'Sharing the Action profile "[ACTION_PROFILE]"',
        'body' => 'Recipient(s) : [ALL_RECIPIENTS]

Hello,

I invite you to consult the following profile :
[ACTION_PARENTURL]

Date : [ACTION_DATE]
Title : [ACTION_PROFILE]
Message :
[ACTION_TEXT]

Best regards,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
    ),
    'activityArea' =>
    array (
      0 =>
      array (
        'id' => 'categorie1',
        'value' => 'Category 1',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'categorie1domaine1',
            'value' => 'Domain 1 of Category 1',
          ),
          1 =>
          array (
            'id' => 'categorie1domaine2',
            'value' => 'Domain 2 of Category 1',
          ),
        ),
      ),
      1 =>
      array (
        'id' => 'categorie2',
        'value' => 'Category 2',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'categorie2domaine1',
            'value' => 'Domain 1 of Category 2',
          ),
          1 =>
          array (
            'id' => 'categorie2domaine2',
            'value' => 'Domain 2 of Category 2',
          ),
        ),
      ),
    ),
    'mobilityArea' =>
    array (
      0 =>
      array (
        'id' => 'monde',
        'value' => 'World',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'monde',
            'value' => 'World',
          ),
          1 =>
          array (
            'id' => 'mondeafrique',
            'value' => 'Africa',
          ),
          2 =>
          array (
            'id' => 'mondeameriquesnord',
            'value' => 'North America',
          ),
          3 =>
          array (
            'id' => 'mondeameriquessud',
            'value' => 'South America',
          ),
          4 =>
          array (
            'id' => 'mondeasie',
            'value' => 'Asia',
          ),
          5 =>
          array (
            'id' => 'mondeaustralie',
            'value' => 'Australasia',
          ),
          6 =>
          array (
            'id' => 'mondeeurope',
            'value' => 'Europe',
          ),
        ),
      ),
      1 =>
      array (
        'id' => 'europe',
        'value' => 'Europe',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeeuropeallemagne',
            'value' => 'Germany',
          ),
          1 =>
          array (
            'id' => 'mondeeuropeautriche',
            'value' => 'Austria',
          ),
          2 =>
          array (
            'id' => 'mondeeuropebelgique',
            'value' => 'Belgium',
          ),
          3 =>
          array (
            'id' => 'mondeeuropebulgarie',
            'value' => 'Bulgaria',
          ),
          4 =>
          array (
            'id' => 'mondeeuropechypre',
            'value' => 'Cyprus',
          ),
          5 =>
          array (
            'id' => 'mondeeuropedanemark',
            'value' => 'Denmark',
          ),
          6 =>
          array (
            'id' => 'mondeeuropeespagne',
            'value' => 'Spain',
          ),
          7 =>
          array (
            'id' => 'mondeeuropeestonie',
            'value' => 'Estonia',
          ),
          8 =>
          array (
            'id' => 'mondeeuropefinlande',
            'value' => 'Finland',
          ),
          9 =>
          array (
            'id' => 'mondeeuropefrance',
            'value' => 'France',
          ),
          10 =>
          array (
            'id' => 'mondeeuropegrèce',
            'value' => 'Greece',
          ),
          11 =>
          array (
            'id' => 'mondeeuropehongrie',
            'value' => 'Hungary',
          ),
          12 =>
          array (
            'id' => 'mondeeuropeirlande',
            'value' => 'Ireland',
          ),
          13 =>
          array (
            'id' => 'mondeeuropeitalie',
            'value' => 'Italy',
          ),
          14 =>
          array (
            'id' => 'mondeeuropelettonie',
            'value' => 'Latvia',
          ),
          15 =>
          array (
            'id' => 'mondeeuropelituanie',
            'value' => 'Lithuania',
          ),
          16 =>
          array (
            'id' => 'mondeeuropeluxembourg',
            'value' => 'Luxemburg',
          ),
          17 =>
          array (
            'id' => 'mondeeuropemalte',
            'value' => 'Malta',
          ),
          18 =>
          array (
            'id' => 'mondeeuropepaysbas',
            'value' => 'The Netherlands',
          ),
          19 =>
          array (
            'id' => 'mondeeuropepologne',
            'value' => 'Poland',
          ),
          20 =>
          array (
            'id' => 'mondeeuropeportugal',
            'value' => 'Portugal',
          ),
          21 =>
          array (
            'id' => 'mondeeuroperepubliquetcheque',
            'value' => 'The Czech Republic',
          ),
          22 =>
          array (
            'id' => 'mondeeuroperoumanie',
            'value' => 'Romania',
          ),
          23 =>
          array (
            'id' => 'mondeeuroperoyaumeuni',
            'value' => 'United Kingdom',
          ),
          24 =>
          array (
            'id' => 'mondeeuropeslovaquie',
            'value' => 'Slovakia',
          ),
          25 =>
          array (
            'id' => 'mondeeuropeslovenie',
            'value' => 'Slovenia',
          ),
          26 =>
          array (
            'id' => 'mondeeuropesuede',
            'value' => 'Sweden',
          ),
          27 =>
          array (
            'id' => 'mondeeuropesuisse',
            'value' => 'Switzerland',
          ),
        ),
      ),
      2 =>
      array (
        'id' => 'france',
        'value' => 'France',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeeuropefrancealsace',
            'value' => 'Alsace',
          ),
          1 =>
          array (
            'id' => 'mondeeuropefranceaquitaine',
            'value' => 'Aquitaine',
          ),
          2 =>
          array (
            'id' => 'mondeeuropefranceauvergne',
            'value' => 'Auvergne',
          ),
          3 =>
          array (
            'id' => 'mondeeuropefrancebourgogne',
            'value' => 'Bourgogne',
          ),
          4 =>
          array (
            'id' => 'mondeeuropefrancebretagne',
            'value' => 'Brittany',
          ),
          5 =>
          array (
            'id' => 'mondeeuropefrancecentre',
            'value' => 'Centre',
          ),
          6 =>
          array (
            'id' => 'mondeeuropefrancechampagneardenne',
            'value' => 'Champagne-Ardenne',
          ),
          7 =>
          array (
            'id' => 'mondeeuropefrancecorse',
            'value' => 'Corsica',
          ),
          8 =>
          array (
            'id' => 'mondeeuropefrancefranchecomte',
            'value' => 'Franche-Comté',
          ),
          9 =>
          array (
            'id' => 'mondeeuropefranceiledefrance',
            'value' => 'Ile-de-France',
          ),
          10 =>
          array (
            'id' => 'mondeeuropefrancelanguedocroussillon',
            'value' => 'Languedoc-Roussillon',
          ),
          11 =>
          array (
            'id' => 'mondeeuropefrancelimousin',
            'value' => 'Limousin',
          ),
          12 =>
          array (
            'id' => 'mondeeuropefrancelorraine',
            'value' => 'Lorraine',
          ),
          13 =>
          array (
            'id' => 'mondeeuropefrancemidipyrenees',
            'value' => 'Midi-Pyrénées',
          ),
          14 =>
          array (
            'id' => 'mondeeuropefrancenordpasdecalais',
            'value' => 'Nord-Pas-de-Calais',
          ),
          15 =>
          array (
            'id' => 'mondeeuropefrancebassenormandie',
            'value' => 'Basse-Normandie',
          ),
          16 =>
          array (
            'id' => 'mondeeuropefrancehautenormandie',
            'value' => 'Haute-Normandie',
          ),
          17 =>
          array (
            'id' => 'mondeeuropefrancepaysdelaloire',
            'value' => 'Pays de la Loire',
          ),
          18 =>
          array (
            'id' => 'mondeeuropefrancepicardie',
            'value' => 'Picardie',
          ),
          19 =>
          array (
            'id' => 'mondeeuropefrancepoitoucharentes',
            'value' => 'Poitou-Charentes',
          ),
          20 =>
          array (
            'id' => 'mondeeuropefrancepaca',
            'value' => 'PACA',
          ),
          21 =>
          array (
            'id' => 'mondeeuropefrancerhonealpes',
            'value' => 'Rhône-Alpes',
          ),
        ),
      ),
      3 =>
      array (
        'id' => 'iledefrance',
        'value' => 'Ile-de-France',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeeuropefranceiledefranceparis',
            'value' => 'Paris (75)',
          ),
          1 =>
          array (
            'id' => 'mondeeuropefranceiledefranceseineetmarne',
            'value' => 'Seine-et-Marne(77)',
          ),
          2 =>
          array (
            'id' => 'mondeeuropefranceiledefranceyvelines',
            'value' => 'Yvelines(78)',
          ),
          3 =>
          array (
            'id' => 'mondeeuropefranceiledefranceessone',
            'value' => 'Essone(91)',
          ),
          4 =>
          array (
            'id' => 'mondeeuropefranceiledefrancehautsdeseine(92)',
            'value' => 'Hauts-de-Seine(92)',
          ),
          5 =>
          array (
            'id' => 'mondeeuropefranceiledefranceseinesaintdenis',
            'value' => 'Seine-Saint-Denis(93)',
          ),
          6 =>
          array (
            'id' => 'mondeeuropefranceiledefrancevaldemarne',
            'value' => 'Val-de-Marne(94)',
          ),
          7 =>
          array (
            'id' => 'mondeeuropefranceiledefrancevaloise',
            'value' => 'Val-d\'Oise(95)',
          ),
        ),
      ),
      4 =>
      array (
        'id' => 'asie',
        'value' => 'Asia',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeasieinde',
            'value' => 'India',
          ),
        ),
      ),
      5 =>
      array (
        'id' => 'afrique',
        'value' => 'Africa',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeafriqueafriquedusud',
            'value' => 'South Africa',
          ),
          1 =>
          array (
            'id' => 'mondeafriquealgerie',
            'value' => 'Algeria',
          ),
          2 =>
          array (
            'id' => 'mondeafriqueangola',
            'value' => 'Angola',
          ),
          3 =>
          array (
            'id' => 'mondeafriquebenin',
            'value' => 'Benin',
          ),
          4 =>
          array (
            'id' => 'mondeafriquebotswana',
            'value' => 'Botswana',
          ),
          5 =>
          array (
            'id' => 'mondeafriqueburkinafaso',
            'value' => 'Burkina-Faso',
          ),
          6 =>
          array (
            'id' => 'mondeafriqueburundi',
            'value' => 'Burundi',
          ),
          7 =>
          array (
            'id' => 'mondeafriquecameroun',
            'value' => 'Cameroon',
          ),
          8 =>
          array (
            'id' => 'mondeafriquecapvert',
            'value' => 'Cape Verde',
          ),
          9 =>
          array (
            'id' => 'mondeafriquecentreafricaine',
            'value' => 'Central African Republic',
          ),
          10 =>
          array (
            'id' => 'mondeafriquecomores',
            'value' => 'The Comoro Islands',
          ),
          11 =>
          array (
            'id' => 'mondeafriquecongo',
            'value' => 'Congo',
          ),
          12 =>
          array (
            'id' => 'mondeafriquedemocratiqueducongo',
            'value' => 'Democratic Republic of the Congo',
          ),
          13 =>
          array (
            'id' => 'mondeafriquecoteivoire',
            'value' => 'Ivory Coast',
          ),
          14 =>
          array (
            'id' => 'mondeafriquedjibouti',
            'value' => 'Djibouti',
          ),
          15 =>
          array (
            'id' => 'mondeafriqueegypte',
            'value' => 'Egypt',
          ),
          16 =>
          array (
            'id' => 'mondeafriqueerythree',
            'value' => 'Eritrea',
          ),
          17 =>
          array (
            'id' => 'mondeafriqueethiopie',
            'value' => 'Ethiopia',
          ),
          18 =>
          array (
            'id' => 'mondeafriquegabon',
            'value' => 'Gabon',
          ),
          19 =>
          array (
            'id' => 'mondeafriqueghana',
            'value' => 'Ghana',
          ),
          20 =>
          array (
            'id' => 'mondeafriqueguinee',
            'value' => 'Guinea',
          ),
          21 =>
          array (
            'id' => 'mondeafriqueguineebissau',
            'value' => 'Guinea-Bissau',
          ),
          22 =>
          array (
            'id' => 'mondeafriqueguineeequatoriale',
            'value' => 'Equatorial Guinea ',
          ),
          23 =>
          array (
            'id' => 'mondeafriquekenya',
            'value' => 'Kenya',
          ),
          24 =>
          array (
            'id' => 'mondeafriquelesotho',
            'value' => 'Lesotho',
          ),
          25 =>
          array (
            'id' => 'mondeafriqueliberia',
            'value' => 'Liberia',
          ),
          26 =>
          array (
            'id' => 'mondeafriquelibye',
            'value' => 'Libya',
          ),
          27 =>
          array (
            'id' => 'mondeafriquemadagascar',
            'value' => 'Madagascar',
          ),
          28 =>
          array (
            'id' => 'mondeafriquemalawi',
            'value' => 'Malawi',
          ),
          29 =>
          array (
            'id' => 'mondeafriquemali',
            'value' => 'Mali',
          ),
          30 =>
          array (
            'id' => 'mondeafriquemaroc',
            'value' => 'Morocco',
          ),
          31 =>
          array (
            'id' => 'mondeafriquemaurice',
            'value' => 'Mauritius',
          ),
          32 =>
          array (
            'id' => 'mondeafriquemauritanie',
            'value' => 'Mauritania',
          ),
          33 =>
          array (
            'id' => 'mondeafriquemozambique',
            'value' => 'Mozambique',
          ),
          34 =>
          array (
            'id' => 'mondeafriquenamibie',
            'value' => 'Namibia',
          ),
          35 =>
          array (
            'id' => 'mondeafriqueniger',
            'value' => 'Niger',
          ),
          36 =>
          array (
            'id' => 'mondeafriquenigeria',
            'value' => 'Nigeria',
          ),
          37 =>
          array (
            'id' => 'mondeafriqueouganda',
            'value' => 'Uganda ',
          ),
          38 =>
          array (
            'id' => 'mondeafriquerwanda',
            'value' => 'Rwanda',
          ),
          39 =>
          array (
            'id' => 'mondeafriquesaotomeetprincipe',
            'value' => 'Sao Tomé-e-Principe',
          ),
          40 =>
          array (
            'id' => 'mondeafriquesenegal',
            'value' => 'Senegal',
          ),
          41 =>
          array (
            'id' => 'mondeafriqueseychelles',
            'value' => 'The Seychelles',
          ),
          42 =>
          array (
            'id' => 'mondeafriquesierraleone',
            'value' => 'Sierra Leone',
          ),
          43 =>
          array (
            'id' => 'mondeafriquesomalie',
            'value' => 'Somalia',
          ),
          44 =>
          array (
            'id' => 'mondeafriquesoudan',
            'value' => 'Sudan',
          ),
          45 =>
          array (
            'id' => 'mondeafriqueswaziland',
            'value' => 'Swaziland',
          ),
          46 =>
          array (
            'id' => 'mondeafriquetanzanie',
            'value' => 'Tanzania',
          ),
          47 =>
          array (
            'id' => 'mondeafriquetchad',
            'value' => 'Chad ',
          ),
          48 =>
          array (
            'id' => 'mondeafriquetogo',
            'value' => 'Togo',
          ),
          49 =>
          array (
            'id' => 'mondeafriquetunisie',
            'value' => 'Tunisia',
          ),
          50 =>
          array (
            'id' => 'mondeafriquezambie',
            'value' => 'Zambia',
          ),
          51 =>
          array (
            'id' => 'mondeafriquezimbabwe',
            'value' => 'Zimbabwe',
          ),
        ),
      ),
    ),
    'experience' =>
    array (
      0 =>
      array (
        'id' => 'Position 1.1 - Coefficient 95',
        'value' => 'Position 1.1 - Coefficient 95',
      ),
      1 =>
      array (
        'id' => 'Position 1.2 - Coefficient 100',
        'value' => 'Position 1.2 - Coefficient 100',
      ),
      2 =>
      array (
        'id' => 'Position 2.1 - Coefficient 105',
        'value' => 'Position 2.1 - Coefficient 105',
      ),
      3 =>
      array (
        'id' => 'Position 2.1 - Coefficient 115',
        'value' => 'Position 2.1 - Coefficient 115',
      ),
      4 =>
      array (
        'id' => 'Position 2.2 - Coefficient 130',
        'value' => 'Position 2.2 - Coefficient 130',
      ),
      5 =>
      array (
        'id' => 'Position 2.3 - Coefficient 150',
        'value' => 'Position 2.3 - Coefficient 150',
      ),
      6 =>
      array (
        'id' => 'Position 3.1 - Coefficient 170',
        'value' => 'Position 3.1 - Coefficient 170',
      ),
      7 =>
      array (
        'id' => 'Position 3.2 - Coefficient 210',
        'value' => 'Position 3.2 - Coefficient 210',
      ),
      8 =>
      array (
        'id' => 'Position 3.3 - Coefficient 270',
        'value' => 'Position 3.3 - Coefficient 270',
      ),
      9 =>
      array (
        'id' => 'Position 1.1 - Coefficient 200',
        'value' => 'Position 1.1 - Coefficient 200',
      ),
      10 =>
      array (
        'id' => 'Position 1.2 - Coefficient 210',
        'value' => 'Position 1.2 - Coefficient 210',
      ),
      11 =>
      array (
        'id' => 'Position 1.3.1 - Coefficient 220',
        'value' => 'Position 1.3.1 - Coefficient 220',
      ),
      12 =>
      array (
        'id' => 'Position 1.3.2 - Coefficient 230',
        'value' => 'Position 1.3.2 - Coefficient 230',
      ),
      13 =>
      array (
        'id' => 'Position 1.4.1 - Coefficient 240',
        'value' => 'Position 1.4.1 - Coefficient 240',
      ),
      14 =>
      array (
        'id' => 'Position 1.4.2 - Coefficient 250',
        'value' => 'Position 1.4.2 - Coefficient 250',
      ),
      15 =>
      array (
        'id' => 'Position 2.1 - Coefficient 275',
        'value' => 'Position 2.1 - Coefficient 275',
      ),
      16 =>
      array (
        'id' => 'Position 2.2 - Coefficient 310',
        'value' => 'Position 2.2 - Coefficient 310',
      ),
      17 =>
      array (
        'id' => 'Position 2.3 - Coefficient 355',
        'value' => 'Position 2.3 - Coefficient 355',
      ),
      18 =>
      array (
        'id' => 'Position 3.1 - Coefficient 400',
        'value' => 'Position 3.1 - Coefficient 400',
      ),
      19 =>
      array (
        'id' => 'Position 3.2 - Coefficient 450',
        'value' => 'Position 3.2 - Coefficient 450',
      ),
      20 =>
      array (
        'id' => 'Position 3.3 - Coefficient 500',
        'value' => 'Position 3.3 - Coefficient 500',
      ),
    ),
    'training' =>
    array (
      0 =>
      array (
        'id' => '0',
        'value' => '0 years',
      ),
      1 =>
      array (
        'id' => '1',
        'value' => '1 year',
      ),
      2 =>
      array (
        'id' => '2',
        'value' => '2 years',
      ),
      3 =>
      array (
        'id' => '3',
        'value' => '3 years',
      ),
      4 =>
      array (
        'id' => '4',
        'value' => '4 years',
      ),
      5 =>
      array (
        'id' => '5',
        'value' => '5 years',
      ),
      6 =>
      array (
        'id' => '6',
        'value' => '6 years',
      ),
      7 =>
      array (
        'id' => '7',
        'value' => '7 years',
      ),
      8 =>
      array (
        'id' => '8',
        'value' => '8 years',
      ),
      9 =>
      array (
        'id' => '9',
        'value' => '9 years',
      ),
      10 =>
      array (
        'id' => '10',
        'value' => '10 years',
      ),
      11 =>
      array (
        'id' => '11',
        'value' => '> 10 years',
      ),
    ),
    'expertiseArea' =>
    array (
      0 =>
      array (
        'id' => 'Autres',
        'value' => 'Other',
      ),
      1 =>
      array (
        'id' => 'Aéronautique',
        'value' => 'Aeronautics',
      ),
      2 =>
      array (
        'id' => 'Aérospatial',
        'value' => 'Aerospace',
      ),
      3 =>
      array (
        'id' => 'Agroalimentaire',
        'value' => 'Agrobusiness',
      ),
      4 =>
      array (
        'id' => 'Assurance',
        'value' => 'Insurance',
      ),
      5 =>
      array (
        'id' => 'Automobile',
        'value' => 'Car industry',
      ),
      6 =>
      array (
        'id' => 'Banque',
        'value' => 'Banking',
      ),
      7 =>
      array (
        'id' => 'Bâtiments',
        'value' => 'Building trade',
      ),
      8 =>
      array (
        'id' => 'Biomédical',
        'value' => 'Biomedics',
      ),
      9 =>
      array (
        'id' => 'Chimie',
        'value' => 'Chemicals',
      ),
      10 =>
      array (
        'id' => 'Conseil',
        'value' => 'Counsel',
      ),
      11 =>
      array (
        'id' => 'Défense',
        'value' => 'Defence',
      ),
      12 =>
      array (
        'id' => 'Edition de logiciels',
        'value' => 'Software editing',
      ),
      13 =>
      array (
        'id' => 'Energie',
        'value' => 'Energy',
      ),
      14 =>
      array (
        'id' => 'Environnement',
        'value' => 'Environment',
      ),
      15 =>
      array (
        'id' => 'Ferroviaire',
        'value' => 'Railways',
      ),
      16 =>
      array (
        'id' => 'Grande distribution',
        'value' => 'Mass marketing',
      ),
      17 =>
      array (
        'id' => 'Infrastructure',
        'value' => 'Infrastructures',
      ),
      18 =>
      array (
        'id' => 'Logistique',
        'value' => 'Logistics',
      ),
      19 =>
      array (
        'id' => 'Média',
        'value' => 'Media',
      ),
      20 =>
      array (
        'id' => 'Métallurgie/sidérurgie',
        'value' => 'Metallurgy/iron and steel',
      ),
      21 =>
      array (
        'id' => 'Naval',
        'value' => 'Shipping',
      ),
      22 =>
      array (
        'id' => 'Nucléaire',
        'value' => 'Nuclear',
      ),
      23 =>
      array (
        'id' => 'Oil & Gaz',
        'value' => 'Oil & Gas',
      ),
      24 =>
      array (
        'id' => 'Pétrochimie',
        'value' => 'Petrochemicals',
      ),
      25 =>
      array (
        'id' => 'Pharmacie',
        'value' => 'Pharmaceuticals',
      ),
      26 =>
      array (
        'id' => 'Santé',
        'value' => 'Health',
      ),
      27 =>
      array (
        'id' => 'Secteur public',
        'value' => 'Public sector',
      ),
      28 =>
      array (
        'id' => 'Services',
        'value' => 'Services',
      ),
      29 =>
      array (
        'id' => 'Télécommunications',
        'value' => 'Telecommunications',
      ),
    ),
    'duration' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => '< 1 month',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => '1 month',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => '2 months',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => '3 months',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => '4 months',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => '5 months',
      ),
      6 =>
      array (
        'id' => 6,
        'value' => '6 months',
      ),
      7 =>
      array (
        'id' => 7,
        'value' => '> 6 months',
      ),
      8 =>
      array (
        'id' => 255,
        'value' => 'unspecified',
      ),
    ),
    'languageLevel' =>
    array (
      0 =>
      array (
        'id' => 'scolaire',
        'value' => 'basic',
      ),
      1 =>
      array (
        'id' => 'intermédiaire',
        'value' => 'intermediate',
      ),
      2 =>
      array (
        'id' => 'courant',
        'value' => 'fluent',
      ),
      3 =>
      array (
        'id' => 'maternel',
        'value' => 'mother tongue',
      ),
    ),
    'languageSpoken' =>
    array (
      0 =>
      array (
        'id' => 'anglais',
        'value' => 'English',
      ),
      1 =>
      array (
        'id' => 'espagnol',
        'value' => 'Spanish',
      ),
      2 =>
      array (
        'id' => 'allemand',
        'value' => 'German',
      ),
    ),
    'availability' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Immediate',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => '< 1 month',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Between 1 and 3 months',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => '3 months',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => '> 3 months',
      ),
    ),
    'evaluation' =>
    array (
      0 =>
      array (
        'id' => 'A',
        'value' => 'A',
      ),
      1 =>
      array (
        'id' => 'B',
        'value' => 'B',
      ),
      2 =>
      array (
        'id' => 'C',
        'value' => 'C',
      ),
      3 =>
      array (
        'id' => 'D',
        'value' => 'D',
      ),
    ),
    'typeOf' =>
    array (
      'contract' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'PC',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'FT',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Subcontractor',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Freelance',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Internship',
        ),
      ),
      'resource' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Internal Consultant',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'External Consultant',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Business engineer',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Agency Manager',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Director',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => 'Recruiter',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => 'HR Manager',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => 'Office Manager',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => 'Comptability',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => 'Candidate',
        ),
      ),
      'activity' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Production',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Absence',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Internal',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Exceptional - Time Zone',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Exceptional - Calendar Zone',
        ),
      ),
      'project' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'Delegation of Skills',
          'mode' => 1,
        ),
        1 =>
        array (
          'id' => 2,
          'value' => 'Fixed',
          'mode' => 2,
        ),
        2 =>
        array (
          'id' => 3,
          'value' => 'Recruitement',
          'mode' => 3,
        ),
        3 =>
        array (
          'id' => 4,
          'value' => 'Product',
          'mode' => 4,
        ),
        4 =>
        array (
          'id' => 5,
          'value' => 'Internal Project',
          'mode' => 2,
        ),
      ),
      'subscription' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Unitary',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Monthly',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Quaterly',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Semi-annual',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Annual',
        ),
      ),
      'purchase' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Miscellaneous',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'External Delivery',
        ),
      ),
      'workingTime' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Full time',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Part time',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Half time',
        ),
      ),
      'employee' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Engineer & Executive',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Technician',
        ),
      ),
    ),
    'origin' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => '0 years',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => '1 year',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => '2 years',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => '3 years',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => '4 years',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => '5 years',
      ),
      6 =>
      array (
        'id' => 6,
        'value' => '6 years',
      ),
      7 =>
      array (
        'id' => 7,
        'value' => '7 years',
      ),
      8 =>
      array (
        'id' => 8,
        'value' => '8 years',
      ),
      9 =>
      array (
        'id' => 9,
        'value' => '9 years',
      ),
      10 =>
      array (
        'id' => 10,
        'value' => '10 years',
      ),
      11 =>
      array (
        'id' => 11,
        'value' => '> 10 years',
      ),
    ),
    'source' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Jobboard',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Company website',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Consultant coopting',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 'Client coopting',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => 'Candidate coopting',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => 'Social network',
      ),
      6 =>
      array (
        'id' => 6,
        'value' => 'Other',
      ),
      7 =>
      array (
        'id' => 7,
        'value' => 'Advertisement',
      ),
      8 =>
      array (
        'id' => 8,
        'value' => 'Lounge',
      ),
    ),
    'criteria' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Relationnal',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Technical',
      ),
    ),
    'tool' =>
    array (
      0 =>
      array (
        'id' => 'outil1',
        'value' => 'Tool 1',
      ),
      1 =>
      array (
        'id' => 'outil2',
        'value' => 'Tool 2',
      ),
      2 =>
      array (
        'id' => 'outil3',
        'value' => 'Tool 3',
      ),
    ),
    'situation' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Bachelor',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Married',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Boyfriend/Girlfriend',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 'Divorced',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => 'Widower',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => 'Civil partnership',
      ),
    ),
    'civility' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Mr.',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Mrs.',
      ),
    ),
  ),
  'country' =>
  array (
    0 =>
    array (
      'id' => 'Afghanistan',
      'iso' => 'AF',
      'value' => 'Afghanistan',
    ),
    1 =>
    array (
      'id' => 'Albanie',
      'iso' => 'AL',
      'value' => 'Albania',
    ),
    2 =>
    array (
      'id' => 'Algerie',
      'iso' => 'DZ',
      'value' => 'Algeria',
    ),
    3 =>
    array (
      'id' => 'Samoa_Americaine',
      'iso' => '',
      'value' => 'American_Samoa',
    ),
    4 =>
    array (
      'id' => 'Vierges_Americaines',
      'iso' => 'VI',
      'value' => 'American_Virgin_Islands',
    ),
    5 =>
    array (
      'id' => 'Andorre',
      'iso' => 'AD',
      'value' => 'Andorra',
    ),
    6 =>
    array (
      'id' => 'Angola',
      'iso' => 'AO',
      'value' => 'Angola',
    ),
    7 =>
    array (
      'id' => 'Anguilla',
      'iso' => 'AI',
      'value' => 'Anguilla',
    ),
    8 =>
    array (
      'id' => 'Argentine',
      'iso' => 'AR',
      'value' => 'Argentina',
    ),
    9 =>
    array (
      'id' => 'Armenie',
      'iso' => 'AM',
      'value' => 'Armenia',
    ),
    10 =>
    array (
      'id' => 'Australie',
      'iso' => 'AU',
      'value' => 'Australia',
    ),
    11 =>
    array (
      'id' => 'Autriche',
      'iso' => 'AT',
      'value' => 'Austria',
    ),
    12 =>
    array (
      'id' => 'Azerbaidjan',
      'iso' => 'AZ',
      'value' => 'Azerbaidjan',
    ),
    13 =>
    array (
      'id' => 'Bangladesh',
      'iso' => 'BD',
      'value' => 'Bangladesh',
    ),
    14 =>
    array (
      'id' => 'Barbade',
      'iso' => 'BB',
      'value' => 'Barbados',
    ),
    15 =>
    array (
      'id' => 'Bahrein',
      'iso' => 'BH',
      'value' => 'Bahrain',
    ),
    16 =>
    array (
      'id' => 'Belgique',
      'iso' => 'BE',
      'value' => 'Belgium',
    ),
    17 =>
    array (
      'id' => 'Belize',
      'iso' => 'BZ',
      'value' => 'Belize',
    ),
    18 =>
    array (
      'id' => 'Benin',
      'iso' => 'BJ',
      'value' => 'Benin',
    ),
    19 =>
    array (
      'id' => 'Bermudes',
      'iso' => 'BM',
      'value' => 'Bermuda',
    ),
    20 =>
    array (
      'id' => 'Bielorussie',
      'iso' => '',
      'value' => 'Byelorussia',
    ),
    21 =>
    array (
      'id' => 'Bolivie',
      'iso' => 'BO',
      'value' => 'Bolivia',
    ),
    22 =>
    array (
      'id' => 'Botswana',
      'iso' => 'BW',
      'value' => 'Botswana',
    ),
    23 =>
    array (
      'id' => 'Bhoutan',
      'iso' => 'BT',
      'value' => 'Bhutan',
    ),
    24 =>
    array (
      'id' => 'Boznie_Herzegovine',
      'iso' => 'BA',
      'value' => 'Bosnia_Herzegovina',
    ),
    25 =>
    array (
      'id' => 'Bresil',
      'iso' => 'BR',
      'value' => 'Brazil',
    ),
    26 =>
    array (
      'id' => 'Vierges_Britanniques',
      'iso' => 'VG',
      'value' => 'British_Virgin_Islands',
    ),
    27 =>
    array (
      'id' => 'Brunei',
      'iso' => 'BN',
      'value' => 'Brunei',
    ),
    28 =>
    array (
      'id' => 'Bulgarie',
      'iso' => 'BG',
      'value' => 'Bulgaria',
    ),
    29 =>
    array (
      'id' => 'Burkina_Faso',
      'iso' => 'BF',
      'value' => 'Burkina_Faso',
    ),
    30 =>
    array (
      'id' => 'Birmanie',
      'iso' => 'MM',
      'value' => 'Burma',
    ),
    31 =>
    array (
      'id' => 'Burundi',
      'iso' => 'BI',
      'value' => 'Burundi',
    ),
    32 =>
    array (
      'id' => 'Cambodge',
      'iso' => 'KH',
      'value' => 'Cambodia',
    ),
    33 =>
    array (
      'id' => 'Cameroun',
      'iso' => 'CM',
      'value' => 'Cameroon',
    ),
    34 =>
    array (
      'id' => 'Canada',
      'iso' => 'CA',
      'value' => 'Canada',
    ),
    35 =>
    array (
      'id' => 'Cap_vert',
      'iso' => 'CV',
      'value' => 'Cape_Verde',
    ),
    36 =>
    array (
      'id' => 'Caiman',
      'iso' => 'KY',
      'value' => 'Cayman',
    ),
    37 =>
    array (
      'id' => 'Afrique_Centrale',
      'iso' => 'CF',
      'value' => 'Centrak_Africa',
    ),
    38 =>
    array (
      'id' => 'Tchad',
      'iso' => 'TD',
      'value' => 'Chad',
    ),
    39 =>
    array (
      'id' => 'Chili',
      'iso' => 'CL',
      'value' => 'Chili',
    ),
    40 =>
    array (
      'id' => 'Chine',
      'iso' => 'CN',
      'value' => 'China',
    ),
    41 =>
    array (
      'id' => 'Colombie',
      'iso' => 'CO',
      'value' => 'Colombia',
    ),
    42 =>
    array (
      'id' => 'Congo',
      'iso' => 'CG',
      'value' => 'Congo',
    ),
    43 =>
    array (
      'id' => 'Costa_Rica',
      'iso' => 'CR',
      'value' => 'Costa_Rica',
    ),
    44 =>
    array (
      'id' => 'Croatie',
      'iso' => 'HR',
      'value' => 'Croatia',
    ),
    45 =>
    array (
      'id' => 'Cuba',
      'iso' => 'CU',
      'value' => 'Cuba',
    ),
    46 =>
    array (
      'id' => 'Chypre',
      'iso' => 'CY',
      'value' => 'Cyprus',
    ),
    47 =>
    array (
      'id' => 'Republique_Tcheque',
      'iso' => 'CZ',
      'value' => 'Czech_Republic',
    ),
    48 =>
    array (
      'id' => 'Danemark',
      'iso' => 'DK',
      'value' => 'Denmark',
    ),
    49 =>
    array (
      'id' => 'Djibouti',
      'iso' => 'DJ',
      'value' => 'Djibouti',
    ),
    50 =>
    array (
      'id' => 'Dominique',
      'iso' => 'DM',
      'value' => 'Dominica',
    ),
    51 =>
    array (
      'id' => 'Republique_Dominicaine',
      'iso' => 'DO',
      'value' => 'Dominican_Republic',
    ),
    52 =>
    array (
      'id' => 'Timor_Oriental',
      'iso' => 'TL',
      'value' => 'Eastern_Timor',
    ),
    53 =>
    array (
      'id' => 'Equateur',
      'iso' => 'EC',
      'value' => 'Ecuador',
    ),
    54 =>
    array (
      'id' => 'Egypte',
      'iso' => 'EG',
      'value' => 'Egypt',
    ),
    55 =>
    array (
      'id' => 'Salvador',
      'iso' => 'SV',
      'value' => 'El_Salvador',
    ),
    56 =>
    array (
      'id' => 'Guinee equatoriale',
      'iso' => 'GQ',
      'value' => 'Equatorial_Guinea',
    ),
    57 =>
    array (
      'id' => 'Erythree',
      'iso' => 'ER',
      'value' => 'Eritria',
    ),
    58 =>
    array (
      'id' => 'Estonie',
      'iso' => 'EE',
      'value' => 'Estonia',
    ),
    59 =>
    array (
      'id' => 'Ethiopie',
      'iso' => 'ET',
      'value' => 'Ethiopia',
    ),
    60 =>
    array (
      'id' => 'Fidji',
      'iso' => 'FJ',
      'value' => 'Fidji',
    ),
    61 =>
    array (
      'id' => 'Finlande',
      'iso' => 'FI',
      'value' => 'Finland',
    ),
    62 =>
    array (
      'id' => 'France',
      'iso' => 'FR',
      'value' => 'France',
    ),
    63 =>
    array (
      'id' => 'Guyane_Francaise ',
      'iso' => 'GF',
      'value' => 'French_Guiana',
    ),
    64 =>
    array (
      'id' => 'Gabon',
      'iso' => 'GA',
      'value' => 'Gabon',
    ),
    65 =>
    array (
      'id' => 'Gambie',
      'iso' => 'GM',
      'value' => 'Gambia',
    ),
    66 =>
    array (
      'id' => 'Georgie',
      'iso' => 'GE',
      'value' => 'Georgia',
    ),
    67 =>
    array (
      'id' => 'Allemagne',
      'iso' => 'DE',
      'value' => 'Germany',
    ),
    68 =>
    array (
      'id' => 'Ghana',
      'iso' => 'GH',
      'value' => 'Ghana',
    ),
    69 =>
    array (
      'id' => 'Gibraltar',
      'iso' => 'GI',
      'value' => 'Gibraltar',
    ),
    70 =>
    array (
      'id' => 'Grece',
      'iso' => 'GR',
      'value' => 'Greece',
    ),
    71 =>
    array (
      'id' => 'Grenade',
      'iso' => 'GD',
      'value' => 'Grenada',
    ),
    72 =>
    array (
      'id' => 'Groenland',
      'iso' => 'GL',
      'value' => 'Greenland',
    ),
    73 =>
    array (
      'id' => 'Guadeloupe',
      'iso' => 'GP',
      'value' => 'Guadeloupe',
    ),
    74 =>
    array (
      'id' => 'Guam',
      'iso' => 'GU',
      'value' => 'Guam',
    ),
    75 =>
    array (
      'id' => 'Guatemala',
      'iso' => 'GT',
      'value' => 'Guatemala',
    ),
    76 =>
    array (
      'id' => 'Guernesey',
      'iso' => '',
      'value' => 'Guernsey',
    ),
    77 =>
    array (
      'id' => 'Guinee',
      'iso' => 'GN',
      'value' => 'Guinea',
    ),
    78 =>
    array (
      'id' => 'Guinee_Bissau',
      'iso' => 'GW',
      'value' => 'Guinea_Bissau',
    ),
    79 =>
    array (
      'id' => 'Guyana',
      'iso' => 'GY',
      'value' => 'Guyana',
    ),
    80 =>
    array (
      'id' => 'Haiti',
      'iso' => 'HT',
      'value' => 'Haiti',
    ),
    81 =>
    array (
      'id' => 'Hawaii',
      'iso' => '',
      'value' => 'Hawaii',
    ),
    82 =>
    array (
      'id' => 'Honduras',
      'iso' => 'HN',
      'value' => 'Honduras',
    ),
    83 =>
    array (
      'id' => 'Hong_Kong',
      'iso' => 'HK',
      'value' => 'Hong_Kong',
    ),
    84 =>
    array (
      'id' => 'Hongrie',
      'iso' => 'HU',
      'value' => 'Hungary',
    ),
    85 =>
    array (
      'id' => 'Inde',
      'iso' => 'IN',
      'value' => 'India',
    ),
    86 =>
    array (
      'id' => 'Indonesie',
      'iso' => 'ID',
      'value' => 'Indonesia',
    ),
    87 =>
    array (
      'id' => 'Iran',
      'iso' => 'IR',
      'value' => 'Iran',
    ),
    88 =>
    array (
      'id' => 'Iraq',
      'iso' => 'IQ',
      'value' => 'Iraq',
    ),
    89 =>
    array (
      'id' => 'Irlande',
      'iso' => 'IE',
      'value' => 'Ireland',
    ),
    90 =>
    array (
      'id' => 'Islande',
      'iso' => 'IS',
      'value' => 'Island',
    ),
    91 =>
    array (
      'id' => 'Israel',
      'iso' => 'IL',
      'value' => 'Israel',
    ),
    92 =>
    array (
      'id' => 'Italie',
      'iso' => 'IT',
      'value' => 'Italy',
    ),
    93 =>
    array (
      'id' => 'Cote_d_Ivoire',
      'iso' => 'CI',
      'value' => 'Ivory_Coast',
    ),
    94 =>
    array (
      'id' => 'Jamaique',
      'iso' => 'JM',
      'value' => 'Jamaica',
    ),
    95 =>
    array (
      'id' => 'Jan Mayen',
      'iso' => 'SJ',
      'value' => 'Jan_Mayen',
    ),
    96 =>
    array (
      'id' => 'Japon',
      'iso' => 'JP',
      'value' => 'Japon',
    ),
    97 =>
    array (
      'id' => 'Jersey',
      'iso' => '',
      'value' => 'Jersey',
    ),
    98 =>
    array (
      'id' => 'Jordanie',
      'iso' => 'JO',
      'value' => 'Jordan',
    ),
    99 =>
    array (
      'id' => 'Kazakhstan',
      'iso' => 'KZ',
      'value' => 'Kazakhstan',
    ),
    100 =>
    array (
      'id' => 'Kenya',
      'iso' => 'KE',
      'value' => 'Kenya',
    ),
    101 =>
    array (
      'id' => 'Kirghizstan',
      'iso' => 'KG',
      'value' => 'Kirghizstan',
    ),
    102 =>
    array (
      'id' => 'Kiribati',
      'iso' => 'KI',
      'value' => 'Kiribati',
    ),
    103 =>
    array (
      'id' => 'Koweit',
      'iso' => 'KW',
      'value' => 'Kuwait',
    ),
    104 =>
    array (
      'id' => 'Laos',
      'iso' => 'LA',
      'value' => 'Laos',
    ),
    105 =>
    array (
      'id' => 'Lesotho',
      'iso' => 'LS',
      'value' => 'Lesotho',
    ),
    106 =>
    array (
      'id' => 'Lettonie',
      'iso' => 'LV',
      'value' => 'Latvia',
    ),
    107 =>
    array (
      'id' => 'Liberia',
      'iso' => 'LR',
      'value' => 'Liberia',
    ),
    108 =>
    array (
      'id' => 'Liechtenstein',
      'iso' => 'LI',
      'value' => 'Liechtenstein',
    ),
    109 =>
    array (
      'id' => 'Lituanie',
      'iso' => 'LT',
      'value' => 'Lithuania',
    ),
    110 =>
    array (
      'id' => 'Luxembourg',
      'iso' => 'LU',
      'value' => 'Luxembourg',
    ),
    111 =>
    array (
      'id' => 'Lybie',
      'iso' => '',
      'value' => 'Lybia',
    ),
    112 =>
    array (
      'id' => 'Macao',
      'iso' => 'MO',
      'value' => 'Macao',
    ),
    113 =>
    array (
      'id' => 'Macedoine',
      'iso' => 'MK',
      'value' => 'Macedoina',
    ),
    114 =>
    array (
      'id' => 'Madagascar',
      'iso' => 'MG',
      'value' => 'Madagascar',
    ),
    115 =>
    array (
      'id' => 'Madère',
      'iso' => '',
      'value' => 'Madeira',
    ),
    116 =>
    array (
      'id' => 'Malaisie',
      'iso' => 'MY',
      'value' => 'Malaysia',
    ),
    117 =>
    array (
      'id' => 'Malawi',
      'iso' => 'MW',
      'value' => 'Malawi',
    ),
    118 =>
    array (
      'id' => 'Mali',
      'iso' => 'ML',
      'value' => 'Mali',
    ),
    119 =>
    array (
      'id' => 'Malte',
      'iso' => 'MT',
      'value' => 'Malta',
    ),
    120 =>
    array (
      'id' => 'Maroc',
      'iso' => 'MZ',
      'value' => 'Marocco',
    ),
    121 =>
    array (
      'id' => 'Martinique',
      'iso' => 'MQ',
      'value' => 'Martinique',
    ),
    122 =>
    array (
      'id' => 'Maurice',
      'iso' => 'MU',
      'value' => 'Mauritius',
    ),
    123 =>
    array (
      'id' => 'Mauritanie',
      'iso' => 'MR',
      'value' => 'Mauritania',
    ),
    124 =>
    array (
      'id' => 'Mayotte',
      'iso' => 'YT',
      'value' => 'Mayotte',
    ),
    125 =>
    array (
      'id' => 'Mexique',
      'iso' => 'MX',
      'value' => 'Mexico',
    ),
    126 =>
    array (
      'id' => 'Micronesie',
      'iso' => 'FM',
      'value' => 'Micronesia',
    ),
    127 =>
    array (
      'id' => 'Midway',
      'iso' => '',
      'value' => 'Midway',
    ),
    128 =>
    array (
      'id' => 'Moldavie',
      'iso' => 'MD',
      'value' => 'Moldavia',
    ),
    129 =>
    array (
      'id' => 'Monaco',
      'iso' => 'MC',
      'value' => 'Monaco',
    ),
    130 =>
    array (
      'id' => 'Mongolie',
      'iso' => 'MN',
      'value' => 'Mongolia',
    ),
    131 =>
    array (
      'id' => 'Montenegro',
      'iso' => 'ME',
      'value' => 'Montenegro',
    ),
    132 =>
    array (
      'id' => 'Montserrat',
      'iso' => 'MS',
      'value' => 'Montserrat',
    ),
    133 =>
    array (
      'id' => 'Mozambique',
      'iso' => 'MZ',
      'value' => 'Mozambique',
    ),
    134 =>
    array (
      'id' => 'Namibie',
      'iso' => 'NA',
      'value' => 'Namibia',
    ),
    135 =>
    array (
      'id' => 'Nauru',
      'iso' => 'NR',
      'value' => 'Nauru',
    ),
    136 =>
    array (
      'id' => 'Nepal',
      'iso' => 'NP',
      'value' => 'Nepal',
    ),
    137 =>
    array (
      'id' => 'Nicaragua',
      'iso' => 'NI',
      'value' => 'Nicaragua',
    ),
    138 =>
    array (
      'id' => 'Niger',
      'iso' => 'NE',
      'value' => 'Niger',
    ),
    139 =>
    array (
      'id' => 'Nigeria',
      'iso' => 'NG',
      'value' => 'Nigeria',
    ),
    140 =>
    array (
      'id' => 'Niue',
      'iso' => 'NU',
      'value' => 'Niue',
    ),
    141 =>
    array (
      'id' => 'Norfolk',
      'iso' => 'NF',
      'value' => 'Norfolk',
    ),
    142 =>
    array (
      'id' => 'Coree_du_Nord',
      'iso' => 'KR',
      'value' => 'North_Korea',
    ),
    143 =>
    array (
      'id' => 'Norvege',
      'iso' => 'NO',
      'value' => 'Norway',
    ),
    144 =>
    array (
      'id' => 'Nouvelle_Caledonie',
      'iso' => 'NC',
      'value' => 'New_Caledonia',
    ),
    145 =>
    array (
      'id' => 'Nouvelle_Zelande',
      'iso' => 'NZ',
      'value' => 'New_Zealand',
    ),
    146 =>
    array (
      'id' => 'Oman',
      'iso' => 'OM',
      'value' => 'Oman',
    ),
    147 =>
    array (
      'id' => 'Pakistan',
      'iso' => 'PK',
      'value' => 'Pakistan',
    ),
    148 =>
    array (
      'id' => 'Palau',
      'iso' => 'PW',
      'value' => 'Palau',
    ),
    149 =>
    array (
      'id' => 'Palestine',
      'iso' => 'PS',
      'value' => 'Palestine',
    ),
    150 =>
    array (
      'id' => 'Panama',
      'iso' => 'PA',
      'value' => 'Panama',
    ),
    151 =>
    array (
      'id' => 'Papouasie_Nouvelle_Guinee',
      'iso' => 'PG',
      'value' => 'Papua_New_Guinea',
    ),
    152 =>
    array (
      'id' => 'Paraguay',
      'iso' => 'PY',
      'value' => 'Paraguay',
    ),
    153 =>
    array (
      'id' => 'Perou',
      'iso' => 'PE',
      'value' => 'Peru',
    ),
    154 =>
    array (
      'id' => 'Pologne',
      'iso' => 'PL',
      'value' => 'Poland',
    ),
    155 =>
    array (
      'id' => 'Polynesie',
      'iso' => 'PF',
      'value' => 'Polynesia',
    ),
    156 =>
    array (
      'id' => 'Porto_Rico',
      'iso' => 'PR',
      'value' => 'Puerto_rico',
    ),
    157 =>
    array (
      'id' => 'Portugal',
      'iso' => 'PT',
      'value' => 'Portugal',
    ),
    158 =>
    array (
      'id' => 'Qatar',
      'iso' => 'QA',
      'value' => 'Qatar',
    ),
    159 =>
    array (
      'id' => 'Reunion',
      'iso' => 'RE',
      'value' => 'Reunion_Island',
    ),
    160 =>
    array (
      'id' => 'Roumanie',
      'iso' => 'RO',
      'value' => 'Romania',
    ),
    161 =>
    array (
      'id' => 'Russie',
      'iso' => 'RU',
      'value' => 'Russia',
    ),
    162 =>
    array (
      'id' => 'Rwanda',
      'iso' => 'RW',
      'value' => 'Rwanda',
    ),
    163 =>
    array (
      'id' => 'Sainte_Lucie',
      'iso' => 'LC',
      'value' => 'Saint_Lucia',
    ),
    164 =>
    array (
      'id' => 'Saint_Marin',
      'iso' => 'SM',
      'value' => 'San_Marino',
    ),
    165 =>
    array (
      'id' => 'Sao_Tome_et_Principe',
      'iso' => 'ST',
      'value' => 'Sao_Tome_e_Principe',
    ),
    166 =>
    array (
      'id' => 'Arabie_Saoudite',
      'iso' => 'SA',
      'value' => 'Saudi_Arabia',
    ),
    167 =>
    array (
      'id' => 'Senegal',
      'iso' => 'SN',
      'value' => 'Senegal',
    ),
    168 =>
    array (
      'id' => 'Sierra Leone',
      'iso' => 'SL',
      'value' => 'Sierra_Leone',
    ),
    169 =>
    array (
      'id' => 'Serbie',
      'iso' => 'CS',
      'value' => 'Serbia',
    ),
    170 =>
    array (
      'id' => 'Singapour',
      'iso' => 'SG',
      'value' => 'Singapore',
    ),
    171 =>
    array (
      'id' => 'Slovaquie',
      'iso' => 'SK',
      'value' => 'Slovakia',
    ),
    172 =>
    array (
      'id' => 'Slovenie',
      'iso' => 'SI',
      'value' => 'Slovenia',
    ),
    173 =>
    array (
      'id' => 'Somalie',
      'iso' => 'SO',
      'value' => 'Somalia',
    ),
    174 =>
    array (
      'id' => 'Afrique_du_sud',
      'iso' => 'ZA',
      'value' => 'South_Africa',
    ),
    175 =>
    array (
      'id' => 'Coree_du_Sud',
      'iso' => 'KP',
      'value' => 'South_Korea',
    ),
    176 =>
    array (
      'id' => 'Espagne',
      'iso' => 'ES',
      'value' => 'Spain',
    ),
    177 =>
    array (
      'id' => 'Sri_Lanka',
      'iso' => 'LK',
      'value' => 'Sri_Lanka',
    ),
    178 =>
    array (
      'id' => 'Soudan',
      'iso' => 'SD',
      'value' => 'Sudan',
    ),
    179 =>
    array (
      'id' => 'Surinam',
      'iso' => 'SR',
      'value' => 'Surinam',
    ),
    180 =>
    array (
      'id' => 'Swaziland',
      'iso' => 'SZ',
      'value' => 'Swaziland',
    ),
    181 =>
    array (
      'id' => 'Suede',
      'iso' => 'SE',
      'value' => 'Sweden',
    ),
    182 =>
    array (
      'id' => 'Suisse',
      'iso' => 'CH',
      'value' => 'Switzerland',
    ),
    183 =>
    array (
      'id' => 'Syrie',
      'iso' => 'SY',
      'value' => 'Syria',
    ),
    184 =>
    array (
      'id' => 'Tadjikistan',
      'iso' => 'TJ',
      'value' => 'Tadjikstan',
    ),
    185 =>
    array (
      'id' => 'Taiwan',
      'iso' => 'TW',
      'value' => 'Taiwan',
    ),
    186 =>
    array (
      'id' => 'Tanzanie',
      'iso' => 'TZ',
      'value' => 'Tanzania',
    ),
    187 =>
    array (
      'id' => 'Thailande',
      'iso' => 'TH',
      'value' => 'Thailand',
    ),
    188 =>
    array (
      'id' => 'Bahamas',
      'iso' => 'BS',
      'value' => 'The_Bahamas',
    ),
    189 =>
    array (
      'id' => 'Canaries',
      'iso' => '',
      'value' => 'The_Canary_Islands',
    ),
    190 =>
    array (
      'id' => 'Comores',
      'iso' => 'KM',
      'value' => 'The_Colomo_Islands',
    ),
    191 =>
    array (
      'id' => 'Cook',
      'iso' => 'CK',
      'value' => 'The_Cook_Islands',
    ),
    192 =>
    array (
      'id' => 'Congo_democratique',
      'iso' => 'CD',
      'value' => 'The_Democratic_Republic_of_Congo',
    ),
    193 =>
    array (
      'id' => 'Falkland',
      'iso' => 'FK',
      'value' => 'The_Falkland_Islands',
    ),
    194 =>
    array (
      'id' => 'Feroe',
      'iso' => 'FO',
      'value' => 'The_Faroe_Islands',
    ),
    195 =>
    array (
      'id' => 'Man',
      'iso' => '',
      'value' => 'The_Isle_of_Man',
    ),
    196 =>
    array (
      'id' => 'Liban',
      'iso' => 'LB',
      'value' => 'The_Lebanon',
    ),
    197 =>
    array (
      'id' => 'Maldives',
      'iso' => 'MV',
      'value' => 'The_Maldives',
    ),
    198 =>
    array (
      'id' => 'Marshall',
      'iso' => 'MH',
      'value' => 'The_Marshall_Islands',
    ),
    199 =>
    array (
      'id' => 'Pays_Bas',
      'iso' => 'NL',
      'value' => 'The_Netherlands',
    ),
    200 =>
    array (
      'id' => 'Mariannes du Nord',
      'iso' => 'MP',
      'value' => 'The_Northern_Mariana_Islands',
    ),
    201 =>
    array (
      'id' => 'Philippines',
      'iso' => 'PH',
      'value' => 'The_Philippines',
    ),
    202 =>
    array (
      'id' => 'Salomon',
      'iso' => 'SB',
      'value' => 'The_Salomon_Islands',
    ),
    203 =>
    array (
      'id' => 'Seychelles',
      'iso' => 'SC',
      'value' => 'The_Seychelles',
    ),
    204 =>
    array (
      'id' => 'Ukraine',
      'iso' => 'UA',
      'value' => 'The_Ukraine',
    ),
    205 =>
    array (
      'id' => 'Vatican',
      'iso' => 'VA',
      'value' => 'The_Vatican',
    ),
    206 =>
    array (
      'id' => 'Wallis et Futuma',
      'iso' => 'WF',
      'value' => 'The_Wallis_and_Futuna_Islands',
    ),
    207 =>
    array (
      'id' => 'Sahara Occidental',
      'iso' => 'EH',
      'value' => 'The_Western_Sahara',
    ),
    208 =>
    array (
      'id' => 'Yemen',
      'iso' => 'YE',
      'value' => 'The_Yemen',
    ),
    209 =>
    array (
      'id' => 'Tibet',
      'iso' => '',
      'value' => 'Tibet',
    ),
    210 =>
    array (
      'id' => 'Tonga',
      'iso' => 'TO',
      'value' => 'Tonga',
    ),
    211 =>
    array (
      'id' => 'Togo',
      'iso' => 'TG',
      'value' => 'Togo',
    ),
    212 =>
    array (
      'id' => 'Trinite_et_Tobago',
      'iso' => 'TT',
      'value' => 'Trinidad_and_Tobago',
    ),
    213 =>
    array (
      'id' => 'Tristan da cunha',
      'iso' => '',
      'value' => 'Tristan_da_Cunha',
    ),
    214 =>
    array (
      'id' => 'Tunisie',
      'iso' => 'TN',
      'value' => 'Tunisia',
    ),
    215 =>
    array (
      'id' => 'Turkmenistan',
      'iso' => 'TM',
      'value' => 'Turkmenistan',
    ),
    216 =>
    array (
      'id' => 'Turquie',
      'iso' => 'TR',
      'value' => 'Turkey',
    ),
    217 =>
    array (
      'id' => 'Emirats_Arabes_Unis',
      'iso' => 'AE',
      'value' => 'United_Arab_Emirates',
    ),
    218 =>
    array (
      'id' => 'Royaume_Uni',
      'iso' => 'UK',
      'value' => 'United_Kingdom',
    ),
    219 =>
    array (
      'id' => 'Etats_Unis',
      'iso' => 'US',
      'value' => 'United_States',
    ),
    220 =>
    array (
      'id' => 'Ouganda',
      'iso' => 'UG',
      'value' => 'Uganda',
    ),
    221 =>
    array (
      'id' => 'Uruguay',
      'iso' => 'UY',
      'value' => 'Uruguay',
    ),
    222 =>
    array (
      'id' => 'Ouzbekistan',
      'iso' => 'UZ',
      'value' => 'Uzbekistan',
    ),
    223 =>
    array (
      'id' => 'Vanuatu',
      'iso' => 'VU',
      'value' => 'Vanuatu',
    ),
    224 =>
    array (
      'id' => 'Venezuela',
      'iso' => 'VE',
      'value' => 'Venezuela',
    ),
    225 =>
    array (
      'id' => 'Vietnam',
      'iso' => 'VN',
      'value' => 'Vietnam',
    ),
    226 =>
    array (
      'id' => 'Wake',
      'iso' => '',
      'value' => 'Wake',
    ),
    227 =>
    array (
      'id' => 'Samoa_Occidentales',
      'iso' => '',
      'value' => 'Western_Samoa',
    ),
    228 =>
    array (
      'id' => 'Zambie',
      'iso' => 'ZM',
      'value' => 'Zambia',
    ),
    229 =>
    array (
      'id' => 'Zimbabwe',
      'iso' => 'ZW',
      'value' => 'Zimbabwe',
    ),
  ),
);
