<?php
return array (
  'setting' =>
  array (
    'action' =>
    array (
      'forceMultiCreation' => true,
      'sort' => false,
      'contact' =>
      array (
        0 =>
        array (
          'id' => 2,
          'value' => 'Note',
        ),
        1 =>
        array (
          'id' => 3,
          'value' => 'Rappel / To do',
        ),
        2 =>
        array (
          'id' => 10,
          'value' => 'Présentation Client',
        ),
        3 =>
        array (
          'id' => 11,
          'value' => 'Suivi de Mission',
        ),
        4 =>
        array (
          'id' => 60,
          'value' => 'Rendez-vous',
        ),
        5 =>
        array (
          'id' => 61,
          'value' => 'Appel',
        ),
        6 =>
        array (
          'id' => 62,
          'value' => 'Email',
        ),
        7 =>
        array (
          'id' => 63,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 64,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 65,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 120,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 121,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 122,
          'value' => '',
        ),
        13 =>
        array (
          'id' => 123,
          'value' => '',
        ),
        14 =>
        array (
          'id' => 124,
          'value' => '',
        ),
        15 =>
        array (
          'id' => 101,
          'value' => 'Notification',
        ),
      ),
      'candidate' =>
      array (
        0 =>
        array (
          'id' => 13,
          'value' => 'Note',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Rappel / To do',
        ),
        2 =>
        array (
          'id' => 0,
          'value' => 'Présentation Client',
        ),
        3 =>
        array (
          'id' => 12,
          'value' => 'Entretien Téléphonique',
        ),
        4 =>
        array (
          'id' => 40,
          'value' => 'Entretien Physique',
        ),
        5 =>
        array (
          'id' => 41,
          'value' => 'Appel',
        ),
        6 =>
        array (
          'id' => 42,
          'value' => 'Email',
        ),
        7 =>
        array (
          'id' => 43,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 44,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 45,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 130,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 131,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 132,
          'value' => '',
        ),
        13 =>
        array (
          'id' => 133,
          'value' => '',
        ),
        14 =>
        array (
          'id' => 134,
          'value' => '',
        ),
        15 =>
        array (
          'id' => 100,
          'value' => 'Notification',
        ),
      ),
      'resource' =>
      array (
        0 =>
        array (
          'id' => 4,
          'value' => 'Note',
        ),
        1 =>
        array (
          'id' => 5,
          'value' => 'Rappel / To do',
        ),
        2 =>
        array (
          'id' => 8,
          'value' => 'Présentation Client',
        ),
        3 =>
        array (
          'id' => 9,
          'value' => 'Suivi de Mission',
        ),
        4 =>
        array (
          'id' => 80,
          'value' => 'Entretien RH Annuel',
        ),
        5 =>
        array (
          'id' => 81,
          'value' => 'Appel',
        ),
        6 =>
        array (
          'id' => 82,
          'value' => 'Email',
        ),
        7 =>
        array (
          'id' => 83,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 84,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 85,
          'value' => 'Visite Médicale',
        ),
        10 =>
        array (
          'id' => 140,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 141,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 142,
          'value' => '',
        ),
        13 =>
        array (
          'id' => 143,
          'value' => '',
        ),
        14 =>
        array (
          'id' => 144,
          'value' => '',
        ),
        15 =>
        array (
          'id' => 102,
          'value' => 'Notification',
        ),
      ),
      'opportunity' =>
      array (
        0 =>
        array (
          'id' => 50,
          'value' => 'Note',
        ),
        1 =>
        array (
          'id' => 51,
          'value' => 'Rappel / To do',
        ),
        2 =>
        array (
          'id' => 52,
          'value' => 'Présentation Client',
        ),
        3 =>
        array (
          'id' => 53,
          'value' => 'Appel',
        ),
        4 =>
        array (
          'id' => 54,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 55,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 56,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 57,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 58,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 59,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 103,
          'value' => 'Notification',
        ),
      ),
      'project' =>
      array (
        0 =>
        array (
          'id' => 30,
          'value' => 'Note',
	        'collaborative' => false
        ),
        1 =>
        array (
          'id' => 31,
          'value' => 'Rappel / To do',
          'collaborative' => false
        ),
        2 =>
        array (
          'id' => 32,
          'value' => 'Suivi de Mission',
          'collaborative' => false
        ),
        3 =>
        array (
          'id' => 33,
          'value' => 'Appel',
          'collaborative' => false
        ),
        4 =>
        array (
          'id' => 34,
          'value' => '',
          'collaborative' => false
        ),
        5 =>
        array (
          'id' => 35,
          'value' => '',
          'collaborative' => false
        ),
        6 =>
        array (
          'id' => 36,
          'value' => '',
          'collaborative' => false
        ),
        7 =>
        array (
          'id' => 37,
          'value' => '',
          'collaborative' => false
        ),
        8 =>
        array (
          'id' => 38,
          'value' => '',
          'collaborative' => false
        ),
        9 =>
        array (
          'id' => 39,
          'value' => '',
          'collaborative' => false
        ),
        10 =>
	        array (
		        'id' => 90,
		        'value' => 'Gestion de projet',
		        'collaborative' => true
	        ),
        11=>
        array (
          'id' => 104,
          'value' => 'Notification',
          'collaborative' => false
        ),
      ),
      'order' =>
      array (
        0 =>
        array (
          'id' => 20,
          'value' => 'Note',
        ),
        1 =>
        array (
          'id' => 21,
          'value' => 'Rappel / To do',
        ),
        2 =>
        array (
          'id' => 22,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 23,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 24,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 25,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 26,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 27,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 28,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 29,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 105,
          'value' => 'Notification',
        ),
      ),
      'invoice' =>
      array (
        0 =>
        array (
          'id' => 70,
          'value' => 'Note',
        ),
        1 =>
        array (
          'id' => 71,
          'value' => 'Rappel / To do',
        ),
        2 =>
        array (
          'id' => 72,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 73,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 74,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 75,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 76,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 77,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 78,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 79,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 110,
          'value' => 'Notification',
        ),
      ),
      'company' =>
      array (
        0 =>
        array (
          'id' => 106,
          'value' => 'Notification',
        ),
      ),
      'product' =>
      array (
        0 =>
        array (
          'id' => 107,
          'value' => 'Notification',
        ),
      ),
      'purchase' =>
      array (
        0 =>
        array (
          'id' => 108,
          'value' => 'Notification',
        ),
      ),
      'app' =>
      array (
        0 =>
        array (
          'id' => 109,
          'value' => 'Notification',
        ),
      ),
    ),
    'collaborative' =>
    array (
      'sort' => false,
      'project' =>
      array (
        0 =>
        array (
          'id' => 90,
          'value' => 'Gestion de Projet',
        ),
      ),
    ),
    'state' =>
    array (
      'sort' => false,
      'candidate' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'A traiter !',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'En cours de qualif.',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Vivier++',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 10,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 11,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 12,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 13,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 14,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 9,
          'value' => 'Vivier',
        ),
        13 =>
        array (
          'id' => 3,
          'value' => 'Converti en Ressource',
        ),
        14 =>
        array (
          'id' => 5,
          'value' => 'Ne plus contacter',
        ),
      ),
      'resource' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'En cours',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => 'Intercontrat',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Sortie',
        ),
      ),
      'product' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'En cours',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Archivé',
        ),
      ),
      'opportunity' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'En cours',
          'active' => true,
        ),
        1 =>
        array (
          'id' => 4,
          'value' => '',
          'active' => true,
        ),
        2 =>
        array (
          'id' => 5,
          'value' => '',
          'active' => true,
        ),
        3 =>
        array (
          'id' => 6,
          'value' => '',
          'active' => true,
        ),
        4 =>
        array (
          'id' => 7,
          'value' => '',
          'active' => true,
        ),
        5 =>
        array (
          'id' => 8,
          'value' => '',
          'active' => true,
        ),
        6 =>
        array (
          'id' => 10,
          'value' => '',
          'active' => true,
        ),
        7 =>
        array (
          'id' => 11,
          'value' => '',
          'active' => true,
        ),
        8 =>
        array (
          'id' => 12,
          'value' => '',
          'active' => true,
        ),
        9 =>
        array (
          'id' => 13,
          'value' => '',
          'active' => true,
        ),
        10 =>
        array (
          'id' => 14,
          'value' => '',
          'active' => true,
        ),
        11 =>
        array (
          'id' => 9,
          'value' => 'Reporté',
          'active' => true,
        ),
        12 =>
        array (
          'id' => 1,
          'value' => 'Gagné',
          'active' => false,
        ),
        13 =>
        array (
          'id' => 2,
          'value' => 'Perdu',
          'active' => false,
        ),
        14 =>
        array (
          'id' => 3,
          'value' => 'Abandonné',
          'active' => false,
        ),
      ),
      'positioning' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Positionné',
          'active' => true,
        ),
        1 =>
        array (
          'id' => 3,
          'value' => 'CV Envoyé',
          'active' => true,
        ),
        2 =>
        array (
          'id' => 4,
          'value' => 'Présenté Client',
          'active' => true,
        ),
        3 =>
        array (
          'id' => 5,
          'value' => '',
          'active' => true,
        ),
        4 =>
        array (
          'id' => 6,
          'value' => '',
          'active' => true,
        ),
        5 =>
        array (
          'id' => 7,
          'value' => '',
          'active' => true,
        ),
        6 =>
        array (
          'id' => 8,
          'value' => '',
          'active' => true,
        ),
        7 =>
        array (
          'id' => 9,
          'value' => 'Refus Candidat',
          'active' => true,
        ),
        8 =>
        array (
          'id' => 1,
          'value' => 'Rejeté',
          'active' => false,
        ),
        9 =>
        array (
          'id' => 2,
          'value' => 'Gagné',
          'active' => false,
        ),
      ),
      'project' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'En cours',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Archivé',
        ),
      ),
      'delivery' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Signée',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 1,
          'value' => 'Prévisionnelle',
        ),
      ),
      'contact' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Prospect',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Client',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 10,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 11,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 12,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 13,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 14,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 7,
          'value' => 'Partenaire',
        ),
        13 =>
        array (
          'id' => 9,
          'value' => 'Fournisseur',
        ),
        14 =>
        array (
          'id' => 8,
          'value' => 'Archivé',
        ),
      ),
      'company' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Prospect',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Client',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 10,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 11,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 12,
          'value' => '',
        ),
        10 =>
        array (
          'id' => 13,
          'value' => '',
        ),
        11 =>
        array (
          'id' => 14,
          'value' => '',
        ),
        12 =>
        array (
          'id' => 7,
          'value' => 'Partenaire',
        ),
        13 =>
        array (
          'id' => 9,
          'value' => 'Fournisseur',
        ),
        14 =>
        array (
          'id' => 8,
          'value' => 'Archivé',
        ),
      ),
      'order' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'En cours',
        ),
        1 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        2 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 9,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 0,
          'value' => 'Archivé',
        ),
      ),
      'invoice' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Création',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Transmis au client',
        ),
        2 =>
        array (
          'id' => 4,
          'value' => 'Relance 1',
        ),
        3 =>
        array (
          'id' => 5,
          'value' => 'Relance 2',
        ),
        4 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 9,
          'value' => 'Email au client',
        ),
        8 =>
        array (
          'id' => 2,
          'value' => 'Impayée',
        ),
        9 =>
        array (
          'id' => 3,
          'value' => 'Payée',
        ),
      ),
      'purchase' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => '',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Validé',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => '',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => 'Planifié',
        ),
      ),
      'payment' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Planifié',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Confirmé',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Réglé',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => '',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => '',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => '',
        ),
      ),
      'quotation' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Création',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Transmis au client',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Attente',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => '',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => '',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => '',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => '',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => 'Refusé',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => 'Accord client',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => 'Archivé',
        ),
      ),
    ),
    'currency' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'EUR Euro',
        'symbol' => '€',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'USD United States Dollars',
        'symbol' => '$',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'GPB United Kingdom Pounds',
        'symbol' => '£',
      ),
    ),
    'calendar' =>
    array (
      0 => 'France_Sans_Pentecote',
      1 => 'Royaume_Uni',
    ),
    'taxRate' =>
    array (
      0 => '20',
      1 => '10',
      2 => '0',
    ),
    'paymentTerm' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 0,
        'x' => 0,
        'y' => 0,
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 1,
        'x' => 30,
        'y' => 0,
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 2,
        'x' => 30,
        'y' => 0,
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 3,
        'x' => 30,
        'y' => 10,
      ),
      4 =>
      array (
        'id' => 41,
        'value' => 1,
        'x' => 45,
        'y' => 0,
      ),
      5 =>
      array (
        'id' => 52,
        'value' => 2,
        'x' => 45,
        'y' => 0,
      ),
    ),
    'paymentMethod' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Virement',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Prélèvement',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Chèque',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 'CB',
      ),
    ),
    'defaultOpportunityTypeCreated' => 1,
    'defaultPositioningSearchModule' => 'candidates',
    'filterPositioningSearchModule' => false,
    'defaultMail' =>
    array (
      'quotation' =>
      array (
        'object' => 'Devis de la société _devissociete_',
        'body' => 'Bonjour,

        Veuillez trouver ci-joint le devis _devisref_ attachée à l\'opportunité _aoref_.',
      ),
      'invoice' =>
      array (
        'object' => 'Facture de la société _factsociete_ - _factref_',
        'body' => 'Bonjour,

        Veuillez trouver ci-joint la facture _factref_ attachée à votre commande _cmdref_.',
      ),
      'timesReports' =>
      array (
        'object' => 'Saisie des Temps',
        'body' => 'Bonjour _profilprenom_ _profilnom_,

Veuillez vous connecter à l\'intranet de votre société, depuis l\'adresse suivante : _intranetlink_ afin de saisir vos temps du mois en cours.

Si vous ne disposez pas de vos identifiants de connexion, veuillez contacter votre responsable.

Merci de ne pas répondre à cet email.',
      ),
      'expensesReports' =>
      array (
        'object' => 'Saisie des Frais',
        'body' => 'Bonjour _profilprenom_ _profilnom_,

Veuillez vous connecter à l\'intranet de votre société, depuis l\'adresse suivante : _intranetlink_ afin de saisir vos frais du mois en cours.

Si vous ne disposez pas de vos identifiants de connexion, veuillez contacter votre responsable.

Merci de ne pas répondre à cet email.',
      ),
      'timesReportsAndExpensesReports' =>
      array (
        'object' => 'Saisie des Temps et des Frais',
        'body' => 'Bonjour _profilprenom_ _profilnom_,

Veuillez vous connecter à l\'intranet de votre société, depuis l\'adresse suivante : _intranetlink_ afin de saisir vos temps et vos frais du mois en cours.

Si vous ne disposez pas de vos identifiants de connexion, veuillez contacter votre responsable.

Merci de ne pas répondre à cet email.',
      ),
      'deliveryOrder' =>
      array (
        'object' => 'Ordre de mission - [MISSION_REFERENCE]',
        'body' => 'Bonjour,

        Veuillez trouver ci-joint votre ordre de mission [MISSION_REFERENCE] pour le projet [PROJECT_REF].',
      ),
    ),
    'smoothAdditionalData' => true,
    'profitabilityMethodOfCalculating' => 'brandRate',
    'showOwnOrderReference' => false,
    'itemInvoice' =>
    array (
      'monthly' =>
      array (
        0 =>
        array (
          'id' => '1',
          'value' => 'Prestation de service ([RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]), [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        1 =>
        array (
          'id' => '1_3',
          'value' => 'Prestation exceptionnelle, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        2 =>
        array (
          'id' => '1_4',
          'value' => 'Prestation exceptionnelle, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        3 =>
        array (
          'id' => '2',
          'value' => 'Prestation de service, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        4 =>
        array (
          'id' => '2_3',
          'value' => 'Prestation exceptionnelle, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        5 =>
        array (
          'id' => '2_4',
          'value' => 'Prestation exceptionnelle, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        6 =>
        array (
          'id' => '3',
          'value' => 'Prestation de service, [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
        7 =>
        array (
          'id' => '4',
          'value' => '[PRODUCT_NAME] - [PRODUCT_REF], [BILL_PERIOD_MONTH] [BILL_PERIOD_YEAR]',
        ),
      ),
      'schedule' =>
      array (
        0 =>
        array (
          'id' => '1',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
        1 =>
        array (
          'id' => '2',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
        2 =>
        array (
          'id' => '3',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
        3 =>
        array (
          'id' => '4',
          'value' => '[SCHEDULE_DESCRIPTION]',
        ),
      ),
    ),
    'showLogoCompany' => false,
    'markdownTextDashboard' => '__Bienvenue sur BoondManager__
> Cette interface vous permet de :
- Consulter/Modifier votre [dossier technique](mon-compte/fiche?onglet=2) (DT)
- Saisir vos [feuilles de temps](mon-compte/fiche?onglet=6) & vos [notes de frais](mon-compte/fiche?onglet=7)
- Effectuer vos [demandes d\'absences](mon-compte/fiche?onglet=8)
- Consulter vos [projets](tableau-de-bord/liste-projets)

#Navigation
> La barre latérale haute vous permet d\'accéder à votre fiche et à la liste de vos projets.
Vous pouvez revenir à tout moment sur cette page en cliquant sur votre prénom _(En haut de l\'interface à côté de la tête du renard)_.

#Votre Fiche - Dossier Technique (DT)
> Vous pouvez consulter votre DT et le modifier _(Uniquement si votre responsable a activé cette fonctionnalité)_.

#Votre Fiche - Temps, Frais & Absences
> - Sélectionnez le document à créer ou modifier
- Saisissez les informations du document durant cette période
- Cliquez sur "Créer" ou "Enregistrer" pour sauvegarder vos saisies sans avertir le prochain validateur
- Pour les feuilles de temps et les notes de frais, joignez vos justificatifs
_(Astuce : Scannez vos tickets de caisse en les collant sur une feuille blanche A4)_
- Cliquez sur "Valider" afin d\'avertir le prochain validateur
- Vous pouvez modifier uniquement les documents qui n\'ont pas été validé par le prochain validateur

#Projets
> - Vous pouvez consulter la liste de tous vos projets
- Sur les projets dont vous êtes "Chef de Projet", vous pouvez consulter les données du projet, les PDF de temps des ressources, et, uniquement sur les forfaits, modifier le "Reste à faire" & les jalons',
    'deliveryOrder' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Ordre de mission par défaut',
        'maskSignatureBlock' => false,
        'insert' =>
        array (
          0 =>
          array (
            'value' => 'Vos obligations',
            'option' =>
            array (
              0 => '- Vous devez prendre connaissance et respecter le règlement intérieur en vigueur chez le client',
              1 => '- Vous devez prendre connaissance et respecter les dispositions spécifiques de sécurité en vigueur chez le client',
              2 => '- Vous devez respecter le secret professionnel vis-à-vis des informations obtenues chez le client ou chez votre employeur',
            ),
          ),
          1 =>
          array (
            'value' => 'Pendant la mission',
            'option' =>
            array (
              0 => '- Chaque fin de mois, établissez une feuille d\'activité mensuelle pour le mois écoulé, faites-la signer par votre chef de projet, et envoyez-la à votre employeur',
              1 => '- Pour des problèmes administratifs, contactez votre assistante de département',
            ),
          ),
          2 =>
          array (
            'value' => 'Absence',
            'option' =>
            array (
              0 => '- Toute absence prévisible doit être signalée à votre employeur le plus tôt possible',
              1 => '- En particulier pour les congés :',
              2 => '#pour la période de congés escomptée, tenir compte des impératifs de la mission en concertation avec le client',
              3 => '#établir une demande de congés écrite et l\'adresser à votre employeur',
              4 => '#après accord de votre employeur, confirmer la période de congés au client',
              5 => '- Les absences non prévues (maladie, accident, cas de force majeure ...) doivent être signalées le plus rapidement possible à votre employeur et au client',
            ),
          ),
          3 =>
          array (
            'value' => 'Organisation du temps de travail',
            'option' =>
            array (
              0 => '- Le prestataire est-il aux 35H hebdomadaires effectives ? NON',
              1 => '- Description : Vous devez suivre l\'organisation du temps de travail de votre employeur telle qu\'elle est décrite dans l\'accord d\'entreprise de réduction du temps de travail',
              2 => '- Vous vous engagez à respecter les jours de société fermée du client, et la prise de congé afférente',
            ),
          ),
          4 =>
          array (
            'value' => 'Description de la mission',
            'option' =>
            array (
              0 => '[MISSION_DESCRIPTION]',
            ),
          ),
          5 =>
          array (
            'value' => 'Conditions particulières',
            'option' =>
            array (
              0 => '[MISSION_CONDITIONS]',
            ),
          ),
        ),
      ),
    ),
    'sharingEntity' =>
    array (
      'credentials' =>
      array (
        'object' => 'Partage des accès à BoondManager',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour [RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME],

Afin de faciliter la collecte et l\'analyse des données opérationnelles de notre société, nous avons décidé de collecter vos fiches de temps, vos notes de frais ainsi que vos demandes d\'absences via une application en ligne métier : BoondManager.

Voici le lien vers votre page de connexion : [BOONDMANAGER_URL] .

Votre login est : "[RESOURCE_LOGIN]".


Il s\'agit de votre 1ère connexion ?
Merci de cliquer sur ce lien pour définir votre mot de passe : [BOONDMANAGER_RESET]

Vous pouvez également vous connecter depuis votre smartphone en téléchargeant notre App Mobile accessible sur les marketplace de Google, Apple & Windows.

Chaque mois, un emailing vous sera envoyé pour vous inciter à qualifier vos feuilles de temps (éventuellement également vos notes de frais).

Pour vous aider dans la prise en main de BoondManager, voici quelques tutoriaux que nous vous invitons à consulter :
https://support.boondmanager.com/hc/fr/articles/206473535-Introduction
https://support.boondmanager.com/hc/fr/articles/205743149-Renseigner-une-feuille-des-temps
https://support.boondmanager.com/hc/fr/articles/206455995-Renseigner-une-note-de-frais
https://support.boondmanager.com/hc/fr/articles/205743159-Faire-une-demande-d-absence

N\'hésitez pas à me contacter pour tous renseignements complémentaires.

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]
[MANAGER_EMAIL]
[MANAGER_PHONE]',
      ),
      'resource' =>
      array (
        'object' => 'Partage de la fiche Ressource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Ressource suivante :
[RESOURCE_URL]

Email : [RESOURCE_EMAIL1]
Téléphone : [RESOURCE_PHONE1]
Titre : [RESOURCE_TITLE]
Disponibilité : [RESOURCE_AVAILABILITY]
Domaines d\'applications : [RESOURCE_ACTIVITIES]
Domaines d\'interventions : [RESOURCE_EXPERTISES]
Expérience : [RESOURCE_EXPERIENCE]
Formation : [RESOURCE_TRAINING]
Outils : [RESOURCE_TOOLS]
Compétences : [RESOURCE_SKILLS]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'opportunity' =>
      array (
        'object' => 'Partage de la fiche Besoin "[OPPORTUNITY_TITLE]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Besoin suivante :
[OPPORTUNITY_URL]

Type : [OPPORTUNITY_TYPE]
Etat : [OPPORTUNITY_STATE]
Démarrage : [OPPORTUNITY_START]
Critères requis : [OPPORTUNITY_CRITERIA]
Domaine d\'application : [OPPORTUNITY_ACTIVITY]
Domaine d\'intervention : [OPPORTUNITY_EXPERTISE]
CA envisagé : [OPPORTUNITY_ESTIMATEDTURNOVER]
Budget envisagé : [OPPORTUNITY_PROJECTEDBUDGET]
Client : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Société : [CRMCOMPANY_NAME]
Description : [OPPORTUNITY_DESCRIPTION]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'candidate' =>
      array (
        'object' => 'Partage de la fiche Candidat "[CANDIDATE_FIRSTNAME] [CANDIDATE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Candidat suivante :
[CANDIDATE_URL]

Email : [CANDIDATE_EMAIL1]
Téléphone : [CANDIDATE_PHONE1]
Titre : [CANDIDATE_TITLE]
Disponibilité : [CANDIDATE_AVAILABILITY]
Evaluation globale : [CANDIDATE_EVALUATION]
Salaire actuel : [CANDIDATE_ACTUALSALARY]
Salaire souhaité : [CANDIDATE_DESIREDSALARY]
Contrat : [CANDIDATE_CONTRACT]
Nationalité : [CANDIDATE_NATIONALITY]
Date de naissance : [CANDIDATE_DATEOFBIRTH]
Provenance : [CANDIDATE_SOURCE]
Domaines d\'applications : [CANDIDATE_ACTIVITIES]
Domaines d\'interventions : [CANDIDATE_EXPERTISES]
Expérience : [CANDIDATE_EXPERIENCE]
Formation : [CANDIDATE_TRAINING]
Outils : [CANDIDATE_TOOLS]
Compétences : [CANDIDATE_SKILLS]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'contact' =>
      array (
        'object' => 'Partage de la fiche Contact CRM "[CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME] - [CRMCOMPANY_NAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Contact CRM suivante :
[CRMCONTACT_URL]

Email : [CRMCONTACT_EMAIL1]
Téléphone : [CRMCONTACT_PHONE1]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'company' =>
      array (
        'object' => 'Partage de la fiche Société CRM "[CRMCOMPANY_NAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Société CRM suivante :
[CRMCOMPANY_URL]

Site Web : [CRMCOMPANY_WEBSITE]
Téléphone : [CRMCOMPANY_PHONE]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'project' =>
      array (
        'object' => 'Partage de la fiche Projet "[PROJECT_REF]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Projet suivante :
[PROJECT_URL]

Type : [OPPORTUNITY_TYPE]
Début : [PROJECT_START]
Fin : [PROJECT_END]
Client : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Société : [CRMCOMPANY_NAME]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'product' =>
      array (
        'object' => 'Partage de la fiche Produit "[PRODUCT_NAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Produit suivante :
[PRODUCT_URL]

Référence du produit : [PRODUCT_REF]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'purchase' =>
      array (
        'object' => 'Partage de la fiche Achat "[PURCHASE_TITLE]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Achat suivante :
[PURCHASE_URL]

Type : [PURCHASE_TYPE]
Début : [PURCHASE_START]
Fin : [PURCHASE_END]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'payment' =>
      array (
        'object' => 'Partage de la fiche Paiement "[PAYMENT_REFERENCE]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Paiement suivante :
[PAYMENT_URL]

Date : [PAYMENT_DATE]
Montant : [PAYMENT_AMOUNT]
Achat : [PURCHASE_TITLE]
Type : [PURCHASE_TYPE]
Début : [PURCHASE_START]
Fin : [PURCHASE_END]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'positioning' =>
      array (
        'object' => 'Partage de la fiche Positionnement "[POSITIONING_REFERENCE]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Positionnement suivante :
[POSITIONING_URL]

Début : [POSITIONING_START]
Fin : [POSITIONING_END]
Besoin : [OPPORTUNITY_TITLE]
Type : [OPPORTUNITY_TYPE]
Démarrage prévu sur la fiche besoin : [OPPORTUNITY_START]
Durée prévue sur la fiche besoin : [OPPORTUNITY_DURATION]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'delivery' =>
      array (
        'object' => 'Partage de la fiche Mission "[MISSION_REFERENCE]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Mission suivante :
[MISSION_URL]

Projet/Type : [MISSION_TYPE]
Début : [MISSION_START]
Fin : [MISSION_END]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'contract' =>
      array (
        'object' => 'Partage de la fiche Contrat de la ressource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Contrat suivante :
[CONTRACT_URL]

Catégorie : [CONTRACT_CATEGORY]
Début : [CONTRACT_START]
Fin : [CONTRACT_END]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'advantage' =>
      array (
        'object' => 'Partage de la fiche Avantage de la ressource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Avantage suivante :
[ADVANTAGE_URL]

Type : [ADVANTAGE_TYPE]
Catégorie : [ADVANTAGE_CATEGORY]
Date : [ADVANTAGE_DATE]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'invoice' =>
      array (
        'object' => 'Partage de la fiche Facture "[BILL_REF]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Facture suivante :
[BILL_URL]

Etat : [BILL_STATE]
Commande : [ORDER_REF]
Projet : [PROJECT_REF]
Client : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Société : [CRMCOMPANY_NAME]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'order' =>
      array (
        'object' => 'Partage de la fiche Commande "[ORDER_REF]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche Commande suivante :
[ORDER_URL]

Projet : [PROJECT_REF]
Client : [CRMCONTACT_FIRSTNAME] [CRMCONTACT_LASTNAME]
Société : [CRMCOMPANY_NAME]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'timesheet' =>
      array (
        'object' => 'Partage de la Feuille des Temps "[TIMESHEET_PERIOD]" de la ressource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la Feuille des Temps suivante :
[TIMESHEET_URL]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'expenses' =>
      array (
        'object' => 'Partage de la Note de Frais "[EXPENSES_PERIOD]" de la ressource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la Note de Frais suivante :
[EXPENSES_URL]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'absencesRequest' =>
      array (
        'object' => 'Partage de la Demande d\'Absence "[ABSENCE_DATE]" de la ressource "[RESOURCE_FIRSTNAME] [RESOURCE_LASTNAME]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la Demande d\'Absence suivante :
[ABSENCE_URL]

Titre : [ABSENCE_TITLE]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
      'action' =>
      array (
        'object' => 'Partage de la fiche Action "[ACTION_PROFILE]"',
        'body' => 'Destinataire(s) : [ALL_RECIPIENTS]

Bonjour,

Je vous invite à consulter la fiche suivante :
[ACTION_PARENTURL]

Date : [ACTION_DATE]
Titre : [ACTION_PROFILE]
Message :
[ACTION_TEXT]

Cordialement,

[MANAGER_FIRSTNAME] [MANAGER_LASTNAME]',
      ),
    ),
    'activityArea' =>
    array (
      0 =>
      array (
        'id' => 'categorie1',
        'value' => 'Catégorie 1',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'categorie1domaine1',
            'value' => 'Domaine 1 de la Catégorie 1',
          ),
          1 =>
          array (
            'id' => 'categorie1domaine2',
            'value' => 'Domaine 2 de la Catégorie 1',
          ),
        ),
      ),
      1 =>
      array (
        'id' => 'categorie2',
        'value' => 'Catégorie 2',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'categorie2domaine1',
            'value' => 'Domaine 1 de la Catégorie 2',
          ),
          1 =>
          array (
            'id' => 'categorie2domaine2',
            'value' => 'Domaine 2 de la Catégorie 2',
          ),
        ),
      ),
    ),
    'mobilityArea' =>
    array (
      0 =>
      array (
        'id' => 'monde',
        'value' => 'Monde',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'monde',
            'value' => 'Monde',
          ),
          1 =>
          array (
            'id' => 'mondeafrique',
            'value' => 'Afrique',
          ),
          2 =>
          array (
            'id' => 'mondeameriquesnord',
            'value' => 'Amériques du Nord',
          ),
          3 =>
          array (
            'id' => 'mondeameriquessud',
            'value' => 'Amériques du Sud',
          ),
          4 =>
          array (
            'id' => 'mondeasie',
            'value' => 'Asie',
          ),
          5 =>
          array (
            'id' => 'mondeaustralie',
            'value' => 'Australie',
          ),
          6 =>
          array (
            'id' => 'mondeeurope',
            'value' => 'Europe',
          ),
        ),
      ),
      1 =>
      array (
        'id' => 'europe',
        'value' => 'Europe',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeeuropeallemagne',
            'value' => 'Allemagne',
          ),
          1 =>
          array (
            'id' => 'mondeeuropeautriche',
            'value' => 'Autriche',
          ),
          2 =>
          array (
            'id' => 'mondeeuropebelgique',
            'value' => 'Belgique',
          ),
          3 =>
          array (
            'id' => 'mondeeuropebulgarie',
            'value' => 'Bulgarie',
          ),
          4 =>
          array (
            'id' => 'mondeeuropechypre',
            'value' => 'Chypre',
          ),
          5 =>
          array (
            'id' => 'mondeeuropedanemark',
            'value' => 'Danemark',
          ),
          6 =>
          array (
            'id' => 'mondeeuropeespagne',
            'value' => 'Espagne',
          ),
          7 =>
          array (
            'id' => 'mondeeuropeestonie',
            'value' => 'Estonie',
          ),
          8 =>
          array (
            'id' => 'mondeeuropefinlande',
            'value' => 'Finlande',
          ),
          9 =>
          array (
            'id' => 'mondeeuropefrance',
            'value' => 'France',
          ),
          10 =>
          array (
            'id' => 'mondeeuropegrèce',
            'value' => 'Grèce',
          ),
          11 =>
          array (
            'id' => 'mondeeuropehongrie',
            'value' => 'Hongrie',
          ),
          12 =>
          array (
            'id' => 'mondeeuropeirlande',
            'value' => 'Irlande',
          ),
          13 =>
          array (
            'id' => 'mondeeuropeitalie',
            'value' => 'Italie',
          ),
          14 =>
          array (
            'id' => 'mondeeuropelettonie',
            'value' => 'Lettonie',
          ),
          15 =>
          array (
            'id' => 'mondeeuropelituanie',
            'value' => 'Lituanie',
          ),
          16 =>
          array (
            'id' => 'mondeeuropeluxembourg',
            'value' => 'Luxembourg',
          ),
          17 =>
          array (
            'id' => 'mondeeuropemalte',
            'value' => 'Malte',
          ),
          18 =>
          array (
            'id' => 'mondeeuropepaysbas',
            'value' => 'Pays-Bas',
          ),
          19 =>
          array (
            'id' => 'mondeeuropepologne',
            'value' => 'Pologne',
          ),
          20 =>
          array (
            'id' => 'mondeeuropeportugal',
            'value' => 'Portugal',
          ),
          21 =>
          array (
            'id' => 'mondeeuroperepubliquetcheque',
            'value' => 'République tchèque',
          ),
          22 =>
          array (
            'id' => 'mondeeuroperoumanie',
            'value' => 'Roumanie',
          ),
          23 =>
          array (
            'id' => 'mondeeuroperoyaumeuni',
            'value' => 'Royaume-Uni',
          ),
          24 =>
          array (
            'id' => 'mondeeuropeslovaquie',
            'value' => 'Slovaquie',
          ),
          25 =>
          array (
            'id' => 'mondeeuropeslovenie',
            'value' => 'Slovénie',
          ),
          26 =>
          array (
            'id' => 'mondeeuropesuede',
            'value' => 'Suède',
          ),
          27 =>
          array (
            'id' => 'mondeeuropesuisse',
            'value' => 'Suisse',
          ),
        ),
      ),
      2 =>
      array (
        'id' => 'france',
        'value' => 'France',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeeuropefrancealsace',
            'value' => 'Alsace',
          ),
          1 =>
          array (
            'id' => 'mondeeuropefranceaquitaine',
            'value' => 'Aquitaine',
          ),
          2 =>
          array (
            'id' => 'mondeeuropefranceauvergne',
            'value' => 'Auvergne',
          ),
          3 =>
          array (
            'id' => 'mondeeuropefrancebourgogne',
            'value' => 'Bourgogne',
          ),
          4 =>
          array (
            'id' => 'mondeeuropefrancebretagne',
            'value' => 'Bretagne',
          ),
          5 =>
          array (
            'id' => 'mondeeuropefrancecentre',
            'value' => 'Centre',
          ),
          6 =>
          array (
            'id' => 'mondeeuropefrancechampagneardenne',
            'value' => 'Champagne-Ardenne',
          ),
          7 =>
          array (
            'id' => 'mondeeuropefrancecorse',
            'value' => 'Corse',
          ),
          8 =>
          array (
            'id' => 'mondeeuropefrancefranchecomte',
            'value' => 'Franche-Comté',
          ),
          9 =>
          array (
            'id' => 'mondeeuropefranceiledefrance',
            'value' => 'Ile-de-France',
          ),
          10 =>
          array (
            'id' => 'mondeeuropefrancelanguedocroussillon',
            'value' => 'Languedoc-Roussillon',
          ),
          11 =>
          array (
            'id' => 'mondeeuropefrancelimousin',
            'value' => 'Limousin',
          ),
          12 =>
          array (
            'id' => 'mondeeuropefrancelorraine',
            'value' => 'Lorraine',
          ),
          13 =>
          array (
            'id' => 'mondeeuropefrancemidipyrenees',
            'value' => 'Midi-Pyrénées',
          ),
          14 =>
          array (
            'id' => 'mondeeuropefrancenordpasdecalais',
            'value' => 'Nord-Pas-de-Calais',
          ),
          15 =>
          array (
            'id' => 'mondeeuropefrancebassenormandie',
            'value' => 'Basse-Normandie',
          ),
          16 =>
          array (
            'id' => 'mondeeuropefrancehautenormandie',
            'value' => 'Haute-Normandie',
          ),
          17 =>
          array (
            'id' => 'mondeeuropefrancepaysdelaloire',
            'value' => 'Pays de la Loire',
          ),
          18 =>
          array (
            'id' => 'mondeeuropefrancepicardie',
            'value' => 'Picardie',
          ),
          19 =>
          array (
            'id' => 'mondeeuropefrancepoitoucharentes',
            'value' => 'Poitou-Charentes',
          ),
          20 =>
          array (
            'id' => 'mondeeuropefrancepaca',
            'value' => 'PACA',
          ),
          21 =>
          array (
            'id' => 'mondeeuropefrancerhonealpes',
            'value' => 'Rhône-Alpes',
          ),
        ),
      ),
      3 =>
      array (
        'id' => 'iledefrance',
        'value' => 'Ile-de-France',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeeuropefranceiledefranceparis',
            'value' => 'Paris (75)',
          ),
          1 =>
          array (
            'id' => 'mondeeuropefranceiledefranceseineetmarne',
            'value' => 'Seine-et-Marne(77)',
          ),
          2 =>
          array (
            'id' => 'mondeeuropefranceiledefranceyvelines',
            'value' => 'Yvelines(78)',
          ),
          3 =>
          array (
            'id' => 'mondeeuropefranceiledefranceessone',
            'value' => 'Essone(91)',
          ),
          4 =>
          array (
            'id' => 'mondeeuropefranceiledefrancehautsdeseine(92)',
            'value' => 'Hauts-de-Seine(92)',
          ),
          5 =>
          array (
            'id' => 'mondeeuropefranceiledefranceseinesaintdenis',
            'value' => 'Seine-Saint-Denis(93)',
          ),
          6 =>
          array (
            'id' => 'mondeeuropefranceiledefrancevaldemarne',
            'value' => 'Val-de-Marne(94)',
          ),
          7 =>
          array (
            'id' => 'mondeeuropefranceiledefrancevaloise',
            'value' => 'Val-d\'Oise(95)',
          ),
        ),
      ),
      4 =>
      array (
        'id' => 'asie',
        'value' => 'Asie',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeasieinde',
            'value' => 'Inde',
          ),
        ),
      ),
      5 =>
      array (
        'id' => 'afrique',
        'value' => 'Afrique',
        'option' =>
        array (
          0 =>
          array (
            'id' => 'mondeafriqueafriquedusud',
            'value' => 'Afrique du Sud',
          ),
          1 =>
          array (
            'id' => 'mondeafriquealgerie',
            'value' => 'Algérie',
          ),
          2 =>
          array (
            'id' => 'mondeafriqueangola',
            'value' => 'Angola',
          ),
          3 =>
          array (
            'id' => 'mondeafriquebenin',
            'value' => 'Bénin',
          ),
          4 =>
          array (
            'id' => 'mondeafriquebotswana',
            'value' => 'Botswana',
          ),
          5 =>
          array (
            'id' => 'mondeafriqueburkinafaso',
            'value' => 'Burkina Faso',
          ),
          6 =>
          array (
            'id' => 'mondeafriqueburundi',
            'value' => 'Burundi',
          ),
          7 =>
          array (
            'id' => 'mondeafriquecameroun',
            'value' => 'Cameroun',
          ),
          8 =>
          array (
            'id' => 'mondeafriquecapvert',
            'value' => 'Cap-Vert',
          ),
          9 =>
          array (
            'id' => 'mondeafriquecentreafricaine',
            'value' => 'République Centreafricaine',
          ),
          10 =>
          array (
            'id' => 'mondeafriquecomores',
            'value' => 'Comores',
          ),
          11 =>
          array (
            'id' => 'mondeafriquecongo',
            'value' => 'République du Congo',
          ),
          12 =>
          array (
            'id' => 'mondeafriquedemocratiqueducongo',
            'value' => 'République démocratique du Congo',
          ),
          13 =>
          array (
            'id' => 'mondeafriquecoteivoire',
            'value' => 'Côte d\'Ivoire',
          ),
          14 =>
          array (
            'id' => 'mondeafriquedjibouti',
            'value' => 'Djibouti',
          ),
          15 =>
          array (
            'id' => 'mondeafriqueegypte',
            'value' => 'Egypte',
          ),
          16 =>
          array (
            'id' => 'mondeafriqueerythree',
            'value' => 'Erythrée',
          ),
          17 =>
          array (
            'id' => 'mondeafriqueethiopie',
            'value' => 'Ethiopie',
          ),
          18 =>
          array (
            'id' => 'mondeafriquegabon',
            'value' => 'Gabon',
          ),
          19 =>
          array (
            'id' => 'mondeafriqueghana',
            'value' => 'Ghana',
          ),
          20 =>
          array (
            'id' => 'mondeafriqueguinee',
            'value' => 'Guinée',
          ),
          21 =>
          array (
            'id' => 'mondeafriqueguineebissau',
            'value' => 'Guinée-Bissau',
          ),
          22 =>
          array (
            'id' => 'mondeafriqueguineeequatoriale',
            'value' => 'Guinée équatoriale',
          ),
          23 =>
          array (
            'id' => 'mondeafriquekenya',
            'value' => 'Kenya',
          ),
          24 =>
          array (
            'id' => 'mondeafriquelesotho',
            'value' => 'Lesotho',
          ),
          25 =>
          array (
            'id' => 'mondeafriqueliberia',
            'value' => 'Liberia',
          ),
          26 =>
          array (
            'id' => 'mondeafriquelibye',
            'value' => 'Libye',
          ),
          27 =>
          array (
            'id' => 'mondeafriquemadagascar',
            'value' => 'Madagascar',
          ),
          28 =>
          array (
            'id' => 'mondeafriquemalawi',
            'value' => 'Malawi',
          ),
          29 =>
          array (
            'id' => 'mondeafriquemali',
            'value' => 'Mali',
          ),
          30 =>
          array (
            'id' => 'mondeafriquemaroc',
            'value' => 'Maroc',
          ),
          31 =>
          array (
            'id' => 'mondeafriquemaurice',
            'value' => 'Maurice',
          ),
          32 =>
          array (
            'id' => 'mondeafriquemauritanie',
            'value' => 'Mauritanie',
          ),
          33 =>
          array (
            'id' => 'mondeafriquemozambique',
            'value' => 'Mozambique',
          ),
          34 =>
          array (
            'id' => 'mondeafriquenamibie',
            'value' => 'Namibie',
          ),
          35 =>
          array (
            'id' => 'mondeafriqueniger',
            'value' => 'Niger',
          ),
          36 =>
          array (
            'id' => 'mondeafriquenigeria',
            'value' => 'Nigeria',
          ),
          37 =>
          array (
            'id' => 'mondeafriqueouganda',
            'value' => 'Ouganda',
          ),
          38 =>
          array (
            'id' => 'mondeafriquerwanda',
            'value' => 'Rwanda',
          ),
          39 =>
          array (
            'id' => 'mondeafriquesaotomeetprincipe',
            'value' => 'Sao Tomé-et-Principe',
          ),
          40 =>
          array (
            'id' => 'mondeafriquesenegal',
            'value' => 'Sénégal',
          ),
          41 =>
          array (
            'id' => 'mondeafriqueseychelles',
            'value' => 'Seychelles',
          ),
          42 =>
          array (
            'id' => 'mondeafriquesierraleone',
            'value' => 'Sierra Leone',
          ),
          43 =>
          array (
            'id' => 'mondeafriquesomalie',
            'value' => 'Somalie',
          ),
          44 =>
          array (
            'id' => 'mondeafriquesoudan',
            'value' => 'Soudan',
          ),
          45 =>
          array (
            'id' => 'mondeafriqueswaziland',
            'value' => 'Swaziland',
          ),
          46 =>
          array (
            'id' => 'mondeafriquetanzanie',
            'value' => 'Tanzanie',
          ),
          47 =>
          array (
            'id' => 'mondeafriquetchad',
            'value' => 'Tchad',
          ),
          48 =>
          array (
            'id' => 'mondeafriquetogo',
            'value' => 'Togo',
          ),
          49 =>
          array (
            'id' => 'mondeafriquetunisie',
            'value' => 'Tunisie',
          ),
          50 =>
          array (
            'id' => 'mondeafriquezambie',
            'value' => 'Zambie',
          ),
          51 =>
          array (
            'id' => 'mondeafriquezimbabwe',
            'value' => 'Zimbabwe',
          ),
        ),
      ),
    ),
    'classification' =>
    array (
      0 =>
      array (
        'id' => 'Position 1.1 - Coefficient 95',
        'value' => 'Position 1.1 - Coefficient 95',
      ),
      1 =>
      array (
        'id' => 'Position 1.2 - Coefficient 100',
        'value' => 'Position 1.2 - Coefficient 100',
      ),
      2 =>
      array (
        'id' => 'Position 2.1 - Coefficient 105',
        'value' => 'Position 2.1 - Coefficient 105',
      ),
      3 =>
      array (
        'id' => 'Position 2.1 - Coefficient 115',
        'value' => 'Position 2.1 - Coefficient 115',
      ),
      4 =>
      array (
        'id' => 'Position 2.2 - Coefficient 130',
        'value' => 'Position 2.2 - Coefficient 130',
      ),
      5 =>
      array (
        'id' => 'Position 2.3 - Coefficient 150',
        'value' => 'Position 2.3 - Coefficient 150',
      ),
      6 =>
      array (
        'id' => 'Position 3.1 - Coefficient 170',
        'value' => 'Position 3.1 - Coefficient 170',
      ),
      7 =>
      array (
        'id' => 'Position 3.2 - Coefficient 210',
        'value' => 'Position 3.2 - Coefficient 210',
      ),
      8 =>
      array (
        'id' => 'Position 3.3 - Coefficient 270',
        'value' => 'Position 3.3 - Coefficient 270',
      ),
      9 =>
      array (
        'id' => 'Position 1.1 - Coefficient 200',
        'value' => 'Position 1.1 - Coefficient 200',
      ),
      10 =>
      array (
        'id' => 'Position 1.2 - Coefficient 210',
        'value' => 'Position 1.2 - Coefficient 210',
      ),
      11 =>
      array (
        'id' => 'Position 1.3.1 - Coefficient 220',
        'value' => 'Position 1.3.1 - Coefficient 220',
      ),
      12 =>
      array (
        'id' => 'Position 1.3.2 - Coefficient 230',
        'value' => 'Position 1.3.2 - Coefficient 230',
      ),
      13 =>
      array (
        'id' => 'Position 1.4.1 - Coefficient 240',
        'value' => 'Position 1.4.1 - Coefficient 240',
      ),
      14 =>
      array (
        'id' => 'Position 1.4.2 - Coefficient 250',
        'value' => 'Position 1.4.2 - Coefficient 250',
      ),
      15 =>
      array (
        'id' => 'Position 2.1 - Coefficient 275',
        'value' => 'Position 2.1 - Coefficient 275',
      ),
      16 =>
      array (
        'id' => 'Position 2.2 - Coefficient 310',
        'value' => 'Position 2.2 - Coefficient 310',
      ),
      17 =>
      array (
        'id' => 'Position 2.3 - Coefficient 355',
        'value' => 'Position 2.3 - Coefficient 355',
      ),
      18 =>
      array (
        'id' => 'Position 3.1 - Coefficient 400',
        'value' => 'Position 3.1 - Coefficient 400',
      ),
      19 =>
      array (
        'id' => 'Position 3.2 - Coefficient 450',
        'value' => 'Position 3.2 - Coefficient 450',
      ),
      20 =>
      array (
        'id' => 'Position 3.3 - Coefficient 500',
        'value' => 'Position 3.3 - Coefficient 500',
      ),
    ),
    'experience' =>
    array (
      0 =>
      array (
        'id' => '0',
        'value' => '0 an',
      ),
      1 =>
      array (
        'id' => '1',
        'value' => '1 an',
      ),
      2 =>
      array (
        'id' => '2',
        'value' => '2 ans',
      ),
      3 =>
      array (
        'id' => '3',
        'value' => '3 ans',
      ),
      4 =>
      array (
        'id' => '4',
        'value' => '4 ans',
      ),
      5 =>
      array (
        'id' => '5',
        'value' => '5 ans',
      ),
      6 =>
      array (
        'id' => '6',
        'value' => '6 ans',
      ),
      7 =>
      array (
        'id' => '7',
        'value' => '7 ans',
      ),
      8 =>
      array (
        'id' => '8',
        'value' => '8 ans',
      ),
      9 =>
      array (
        'id' => '9',
        'value' => '9 ans',
      ),
      10 =>
      array (
        'id' => '10',
        'value' => '10 ans',
      ),
      11 =>
      array (
        'id' => '11',
        'value' => '> 10 ans',
      ),
    ),
	  'training' => array(
		0 => array(
			'id' => 'Bac',
			'value' => 'Bac'
		),
		1 => array(
			'id' => 'Bac+1',
			'value' => 'Bac+1'
		),
		2 => array(
			'id' => 'Bac+2',
			'value' => 'Bac+2'
		),
		3 => array(
			'id' => 'Bac+3',
			'value' => 'Bac+3'
		),
		4 => array(
			'id' => 'Bac+4',
			'value' => 'Bac+4'
		),
		5 => array(
			'id' => 'Bac+5',
			'value' => 'Bac+5'
		),
		6 => array(
			'id' => 'Bac+6',
			'value' => 'Bac+6'
		),
		7 => array(
			'id' => 'Bac+7',
			'value' => 'Bac+7'
		),
		8 => array(
			'id' => 'Bac+8',
			'value' => 'Bac+8'
		),
	  ),
    'expertiseArea' =>
    array (
      0 =>
      array (
        'id' => 'Autres',
        'value' => 'Autres',
      ),
      1 =>
      array (
        'id' => 'Aéronautique',
        'value' => 'Aéronautique',
      ),
      2 =>
      array (
        'id' => 'Aérospatial',
        'value' => 'Aérospatial',
      ),
      3 =>
      array (
        'id' => 'Agroalimentaire',
        'value' => 'Agroalimentaire',
      ),
      4 =>
      array (
        'id' => 'Assurance',
        'value' => 'Assurance',
      ),
      5 =>
      array (
        'id' => 'Automobile',
        'value' => 'Automobile',
      ),
      6 =>
      array (
        'id' => 'Banque',
        'value' => 'Banque',
      ),
      7 =>
      array (
        'id' => 'Bâtiments',
        'value' => 'Bâtiments',
      ),
      8 =>
      array (
        'id' => 'Biomédical',
        'value' => 'Biomédical',
      ),
      9 =>
      array (
        'id' => 'Chimie',
        'value' => 'Chimie',
      ),
      10 =>
      array (
        'id' => 'Conseil',
        'value' => 'Conseil',
      ),
      11 =>
      array (
        'id' => 'Défense',
        'value' => 'Défense',
      ),
      12 =>
      array (
        'id' => 'Edition de logiciels',
        'value' => 'Edition de logiciels',
      ),
      13 =>
      array (
        'id' => 'Energie',
        'value' => 'Energie',
      ),
      14 =>
      array (
        'id' => 'Environnement',
        'value' => 'Environnement',
      ),
      15 =>
      array (
        'id' => 'Ferroviaire',
        'value' => 'Ferroviaire',
      ),
      16 =>
      array (
        'id' => 'Grande distribution',
        'value' => 'Grande distribution',
      ),
      17 =>
      array (
        'id' => 'Infrastructure',
        'value' => 'Infrastructure',
      ),
      18 =>
      array (
        'id' => 'Logistique',
        'value' => 'Logistique',
      ),
      19 =>
      array (
        'id' => 'Média',
        'value' => 'Média',
      ),
      20 =>
      array (
        'id' => 'Métallurgie/sidérurgie',
        'value' => 'Métallurgie/sidérurgie',
      ),
      21 =>
      array (
        'id' => 'Naval',
        'value' => 'Naval',
      ),
      22 =>
      array (
        'id' => 'Nucléaire',
        'value' => 'Nucléaire',
      ),
      23 =>
      array (
        'id' => 'Oil & Gaz',
        'value' => 'Oil & Gaz',
      ),
      24 =>
      array (
        'id' => 'Pétrochimie',
        'value' => 'Pétrochimie',
      ),
      25 =>
      array (
        'id' => 'Pharmacie',
        'value' => 'Pharmacie',
      ),
      26 =>
      array (
        'id' => 'Santé',
        'value' => 'Santé',
      ),
      27 =>
      array (
        'id' => 'Secteur public',
        'value' => 'Secteur public',
      ),
      28 =>
      array (
        'id' => 'Services',
        'value' => 'Services',
      ),
      29 =>
      array (
        'id' => 'Télécommunications',
        'value' => 'Télécommunications',
      ),
    ),
    'duration' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => '< 1 mois',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => '1 mois',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => '2 mois',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => '3 mois',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => '4 mois',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => '5 mois',
      ),
      6 =>
      array (
        'id' => 6,
        'value' => '6 mois',
      ),
      7 =>
      array (
        'id' => 7,
        'value' => '> 6 mois',
      ),
      8 =>
      array (
        'id' => 255,
        'value' => 'Indéterminée',
      ),
    ),
    'languageLevel' =>
    array (
      0 =>
      array (
        'id' => 'scolaire',
        'value' => 'scolaire',
      ),
      1 =>
      array (
        'id' => 'intermédiaire',
        'value' => 'intermédiaire',
      ),
      2 =>
      array (
        'id' => 'courant',
        'value' => 'courant',
      ),
      3 =>
      array (
        'id' => 'maternel',
        'value' => 'maternel',
      ),
    ),
    'languageSpoken' =>
    array (
      0 =>
      array (
        'id' => 'anglais',
        'value' => 'Anglais',
      ),
      1 =>
      array (
        'id' => 'espagnol',
        'value' => 'Espagnol',
      ),
      2 =>
      array (
        'id' => 'allemand',
        'value' => 'Allemand',
      ),
    ),
    'availability' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Immédiate',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => '< 1 mois',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'De 1 à 3 mois',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => '3 mois',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => '> 3 mois',
      ),
    ),
    'evaluation' =>
    array (
      0 =>
      array (
        'id' => 'A',
        'value' => 'A',
      ),
      1 =>
      array (
        'id' => 'B',
        'value' => 'B',
      ),
      2 =>
      array (
        'id' => 'C',
        'value' => 'C',
      ),
      3 =>
      array (
        'id' => 'D',
        'value' => 'D',
      ),
    ),
    'typeOf' =>
    array (
      'contract' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'CDI',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'CDD',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Sous-traitant',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Freelance',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Stage',
        ),
      ),
      'resource' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Consultant Interne',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Consultant Externe',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Ingénieur d\'affaires',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Responsable d\'agence',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Directeur',
        ),
        5 =>
        array (
          'id' => 5,
          'value' => 'Chargé de recrutement',
        ),
        6 =>
        array (
          'id' => 6,
          'value' => 'Responsable RH',
        ),
        7 =>
        array (
          'id' => 7,
          'value' => 'Office Manager',
        ),
        8 =>
        array (
          'id' => 8,
          'value' => 'Comptabilité',
        ),
        9 =>
        array (
          'id' => 9,
          'value' => 'Candidat',
        ),
      ),
      'activity' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Production',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Absence',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Interne',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Exceptionnelle - Plage Horaire',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Exceptionnelle - Plage Calendaire',
        ),
      ),
      'project' =>
      array (
        0 =>
        array (
          'id' => 1,
          'value' => 'Régie',
          'mode' => 1,
        ),
        1 =>
        array (
          'id' => 2,
          'value' => 'Forfait',
          'mode' => 2,
        ),
        2 =>
        array (
          'id' => 3,
          'value' => 'Recrutement',
          'mode' => 3,
        ),
        3 =>
        array (
          'id' => 4,
          'value' => 'Produit',
          'mode' => 4,
        ),
        4 =>
        array (
          'id' => 5,
          'value' => 'Projet Interne',
          'mode' => 2,
        ),
      ),
      'subscription' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Unitaire',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Mensuel',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Trimestriel',
        ),
        3 =>
        array (
          'id' => 3,
          'value' => 'Semestriel',
        ),
        4 =>
        array (
          'id' => 4,
          'value' => 'Annuel',
        ),
      ),
      'purchase' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Divers',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Prestation Externe',
        ),
      ),
      'workingTime' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Temps plein',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'Temps partiel',
        ),
        2 =>
        array (
          'id' => 2,
          'value' => 'Mi-temps',
        ),
      ),
      'employee' =>
      array (
        0 =>
        array (
          'id' => 0,
          'value' => 'Ingénieur & Cadre',
        ),
        1 =>
        array (
          'id' => 1,
          'value' => 'E.T.A.M',
        ),
      ),
    ),
    'origin' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => '0 an',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => '1 an',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => '2 ans',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => '3 ans',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => '4 ans',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => '5 ans',
      ),
      6 =>
      array (
        'id' => 6,
        'value' => '6 ans',
      ),
      7 =>
      array (
        'id' => 7,
        'value' => '7 ans',
      ),
      8 =>
      array (
        'id' => 8,
        'value' => '8 ans',
      ),
      9 =>
      array (
        'id' => 9,
        'value' => '9 ans',
      ),
      10 =>
      array (
        'id' => 10,
        'value' => '10 ans',
      ),
      11 =>
      array (
        'id' => 11,
        'value' => '> 10 ans',
      ),
    ),
    'source' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Jobboard',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Site Société',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Cooptation Intervenant',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 'Cooptation Client',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => 'Cooptation Candidat',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => 'Réseau Social',
      ),
      6 =>
      array (
        'id' => 6,
        'value' => 'Autres',
      ),
      7 =>
      array (
        'id' => 7,
        'value' => 'Annonce',
      ),
      8 =>
      array (
        'id' => 8,
        'value' => 'Salon',
      ),
    ),
    'criteria' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Relationnel',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Technique',
      ),
    ),
    'tool' =>
    array (
      0 =>
      array (
        'id' => 'outil1',
        'value' => 'Outil 1',
      ),
      1 =>
      array (
        'id' => 'outil2',
        'value' => 'Outil 2',
      ),
      2 =>
      array (
        'id' => 'outil3',
        'value' => 'Outil 3',
      ),
    ),
    'situation' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'Célibataire',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Marié(e)',
      ),
      2 =>
      array (
        'id' => 2,
        'value' => 'Concubinage',
      ),
      3 =>
      array (
        'id' => 3,
        'value' => 'Divorcé(e)',
      ),
      4 =>
      array (
        'id' => 4,
        'value' => 'Veuf(ve)',
      ),
      5 =>
      array (
        'id' => 5,
        'value' => 'PACS',
      ),
    ),
    'civility' =>
    array (
      0 =>
      array (
        'id' => 0,
        'value' => 'M.',
      ),
      1 =>
      array (
        'id' => 1,
        'value' => 'Mme.',
      ),
    ),
  ),
  'country' =>
  array (
    0 =>
    array (
      'id' => 'Afghanistan',
      'iso' => 'AF',
      'value' => 'Afghanistan',
    ),
    1 =>
    array (
      'id' => 'Afrique_Centrale',
      'iso' => 'CF',
      'value' => 'Afrique_Centrale',
    ),
    2 =>
    array (
      'id' => 'Afrique_du_sud',
      'iso' => 'ZA',
      'value' => 'Afrique_du_Sud',
    ),
    3 =>
    array (
      'id' => 'Albanie',
      'iso' => 'AL',
      'value' => 'Albanie',
    ),
    4 =>
    array (
      'id' => 'Algerie',
      'iso' => 'DZ',
      'value' => 'Algerie',
    ),
    5 =>
    array (
      'id' => 'Allemagne',
      'iso' => 'DE',
      'value' => 'Allemagne',
    ),
    6 =>
    array (
      'id' => 'Andorre',
      'iso' => 'AD',
      'value' => 'Andorre',
    ),
    7 =>
    array (
      'id' => 'Angola',
      'iso' => 'AO',
      'value' => 'Angola',
    ),
    8 =>
    array (
      'id' => 'Anguilla',
      'iso' => 'AI',
      'value' => 'Anguilla',
    ),
    9 =>
    array (
      'id' => 'Arabie_Saoudite',
      'iso' => 'SA',
      'value' => 'Arabie_Saoudite',
    ),
    10 =>
    array (
      'id' => 'Argentine',
      'iso' => 'AR',
      'value' => 'Argentine',
    ),
    11 =>
    array (
      'id' => 'Armenie',
      'iso' => 'AM',
      'value' => 'Armenie',
    ),
    12 =>
    array (
      'id' => 'Australie',
      'iso' => 'AU',
      'value' => 'Australie',
    ),
    13 =>
    array (
      'id' => 'Autriche',
      'iso' => 'AT',
      'value' => 'Autriche',
    ),
    14 =>
    array (
      'id' => 'Azerbaidjan',
      'iso' => 'AZ',
      'value' => 'Azerbaidjan',
    ),
    15 =>
    array (
      'id' => 'Bahamas',
      'iso' => 'BS',
      'value' => 'Bahamas',
    ),
    16 =>
    array (
      'id' => 'Bangladesh',
      'iso' => 'BD',
      'value' => 'Bangladesh',
    ),
    17 =>
    array (
      'id' => 'Barbade',
      'iso' => 'BB',
      'value' => 'Barbade',
    ),
    18 =>
    array (
      'id' => 'Bahrein',
      'iso' => 'BH',
      'value' => 'Bahrein',
    ),
    19 =>
    array (
      'id' => 'Belgique',
      'iso' => 'BE',
      'value' => 'Belgique',
    ),
    20 =>
    array (
      'id' => 'Belize',
      'iso' => 'BZ',
      'value' => 'Belize',
    ),
    21 =>
    array (
      'id' => 'Benin',
      'iso' => 'BJ',
      'value' => 'Benin',
    ),
    22 =>
    array (
      'id' => 'Bermudes',
      'iso' => 'BM',
      'value' => 'Bermudes',
    ),
    23 =>
    array (
      'id' => 'Bielorussie',
      'iso' => '',
      'value' => 'Bielorussie',
    ),
    24 =>
    array (
      'id' => 'Birmanie',
      'iso' => 'MM',
      'value' => 'Birmanie',
    ),
    25 =>
    array (
      'id' => 'Bolivie',
      'iso' => 'BO',
      'value' => 'Bolivie',
    ),
    26 =>
    array (
      'id' => 'Botswana',
      'iso' => 'BW',
      'value' => 'Botswana',
    ),
    27 =>
    array (
      'id' => 'Bhoutan',
      'iso' => 'BT',
      'value' => 'Bhoutan',
    ),
    28 =>
    array (
      'id' => 'Boznie_Herzegovine',
      'iso' => 'BA',
      'value' => 'Boznie_Herzegovine',
    ),
    29 =>
    array (
      'id' => 'Bresil',
      'iso' => 'BR',
      'value' => 'Bresil',
    ),
    30 =>
    array (
      'id' => 'Brunei',
      'iso' => 'BN',
      'value' => 'Brunei',
    ),
    31 =>
    array (
      'id' => 'Bulgarie',
      'iso' => 'BG',
      'value' => 'Bulgarie',
    ),
    32 =>
    array (
      'id' => 'Burkina_Faso',
      'iso' => 'BF',
      'value' => 'Burkina_Faso',
    ),
    33 =>
    array (
      'id' => 'Burundi',
      'iso' => 'BI',
      'value' => 'Burundi',
    ),
    34 =>
    array (
      'id' => 'Caiman',
      'iso' => 'KY',
      'value' => 'Caiman',
    ),
    35 =>
    array (
      'id' => 'Cambodge',
      'iso' => 'KH',
      'value' => 'Cambodge',
    ),
    36 =>
    array (
      'id' => 'Cameroun',
      'iso' => 'CM',
      'value' => 'Cameroun',
    ),
    37 =>
    array (
      'id' => 'Canada',
      'iso' => 'CA',
      'value' => 'Canada',
    ),
    38 =>
    array (
      'id' => 'Canaries',
      'iso' => '',
      'value' => 'Canaries',
    ),
    39 =>
    array (
      'id' => 'Cap_vert',
      'iso' => 'CV',
      'value' => 'Cap_Vert',
    ),
    40 =>
    array (
      'id' => 'Chili',
      'iso' => 'CL',
      'value' => 'Chili',
    ),
    41 =>
    array (
      'id' => 'Chine',
      'iso' => 'CN',
      'value' => 'Chine',
    ),
    42 =>
    array (
      'id' => 'Chypre',
      'iso' => 'CY',
      'value' => 'Chypre',
    ),
    43 =>
    array (
      'id' => 'Colombie',
      'iso' => 'CO',
      'value' => 'Colombie',
    ),
    44 =>
    array (
      'id' => 'Comores',
      'iso' => 'KM',
      'value' => 'Comores',
    ),
    45 =>
    array (
      'id' => 'Congo',
      'iso' => 'CG',
      'value' => 'Congo',
    ),
    46 =>
    array (
      'id' => 'Congo_democratique',
      'iso' => 'CD',
      'value' => 'Congo_democratique',
    ),
    47 =>
    array (
      'id' => 'Cook',
      'iso' => 'CK',
      'value' => 'Cook',
    ),
    48 =>
    array (
      'id' => 'Coree_du_Nord',
      'iso' => 'KR',
      'value' => 'Coree_du_Nord',
    ),
    49 =>
    array (
      'id' => 'Coree_du_Sud',
      'iso' => 'KP',
      'value' => 'Coree_du_Sud',
    ),
    50 =>
    array (
      'id' => 'Costa_Rica',
      'iso' => 'CR',
      'value' => 'Costa_Rica',
    ),
    51 =>
    array (
      'id' => 'Cote_d_Ivoire',
      'iso' => 'CI',
      'value' => 'Côte_d_Ivoire',
    ),
    52 =>
    array (
      'id' => 'Croatie',
      'iso' => 'HR',
      'value' => 'Croatie',
    ),
    53 =>
    array (
      'id' => 'Cuba',
      'iso' => 'CU',
      'value' => 'Cuba',
    ),
    54 =>
    array (
      'id' => 'Danemark',
      'iso' => 'DK',
      'value' => 'Danemark',
    ),
    55 =>
    array (
      'id' => 'Djibouti',
      'iso' => 'DJ',
      'value' => 'Djibouti',
    ),
    56 =>
    array (
      'id' => 'Dominique',
      'iso' => 'DM',
      'value' => 'Dominique',
    ),
    57 =>
    array (
      'id' => 'Egypte',
      'iso' => 'EG',
      'value' => 'Egypte',
    ),
    58 =>
    array (
      'id' => 'Emirats_Arabes_Unis',
      'iso' => 'AE',
      'value' => 'Emirats_Arabes_Unis',
    ),
    59 =>
    array (
      'id' => 'Equateur',
      'iso' => 'EC',
      'value' => 'Equateur',
    ),
    60 =>
    array (
      'id' => 'Erythree',
      'iso' => 'ER',
      'value' => 'Erythree',
    ),
    61 =>
    array (
      'id' => 'Espagne',
      'iso' => 'ES',
      'value' => 'Espagne',
    ),
    62 =>
    array (
      'id' => 'Estonie',
      'iso' => 'EE',
      'value' => 'Estonie',
    ),
    63 =>
    array (
      'id' => 'Etats_Unis',
      'iso' => 'US',
      'value' => 'Etats_Unis',
    ),
    64 =>
    array (
      'id' => 'Ethiopie',
      'iso' => 'ET',
      'value' => 'Ethiopie',
    ),
    65 =>
    array (
      'id' => 'Falkland',
      'iso' => 'FK',
      'value' => 'Falkland',
    ),
    66 =>
    array (
      'id' => 'Feroe',
      'iso' => 'FO',
      'value' => 'Feroe',
    ),
    67 =>
    array (
      'id' => 'Fidji',
      'iso' => 'FJ',
      'value' => 'Fidji',
    ),
    68 =>
    array (
      'id' => 'Finlande',
      'iso' => 'FI',
      'value' => 'Finlande',
    ),
    69 =>
    array (
      'id' => 'France',
      'iso' => 'FR',
      'value' => 'France',
    ),
    70 =>
    array (
      'id' => 'Gabon',
      'iso' => 'GA',
      'value' => 'Gabon',
    ),
    71 =>
    array (
      'id' => 'Gambie',
      'iso' => 'GM',
      'value' => 'Gambie',
    ),
    72 =>
    array (
      'id' => 'Georgie',
      'iso' => 'GE',
      'value' => 'Georgie',
    ),
    73 =>
    array (
      'id' => 'Ghana',
      'iso' => 'GH',
      'value' => 'Ghana',
    ),
    74 =>
    array (
      'id' => 'Gibraltar',
      'iso' => 'GI',
      'value' => 'Gibraltar',
    ),
    75 =>
    array (
      'id' => 'Grece',
      'iso' => 'GR',
      'value' => 'Grece',
    ),
    76 =>
    array (
      'id' => 'Grenade',
      'iso' => 'GD',
      'value' => 'Grenade',
    ),
    77 =>
    array (
      'id' => 'Groenland',
      'iso' => 'GL',
      'value' => 'Groenland',
    ),
    78 =>
    array (
      'id' => 'Guadeloupe',
      'iso' => 'GP',
      'value' => 'Guadeloupe',
    ),
    79 =>
    array (
      'id' => 'Guam',
      'iso' => 'GU',
      'value' => 'Guam',
    ),
    80 =>
    array (
      'id' => 'Guatemala',
      'iso' => 'GT',
      'value' => 'Guatemala',
    ),
    81 =>
    array (
      'id' => 'Guernesey',
      'iso' => '',
      'value' => 'Guernesey',
    ),
    82 =>
    array (
      'id' => 'Guinee',
      'iso' => 'GN',
      'value' => 'Guinee',
    ),
    83 =>
    array (
      'id' => 'Guinee_Bissau',
      'iso' => 'GW',
      'value' => 'Guinee_Bissau',
    ),
    84 =>
    array (
      'id' => 'Guinee equatoriale',
      'iso' => 'GQ',
      'value' => 'Guinee_Equatoriale',
    ),
    85 =>
    array (
      'id' => 'Guyana',
      'iso' => 'GY',
      'value' => 'Guyana',
    ),
    86 =>
    array (
      'id' => 'Guyane_Francaise ',
      'iso' => 'GF',
      'value' => 'Guyane_Francaise',
    ),
    87 =>
    array (
      'id' => 'Haiti',
      'iso' => 'HT',
      'value' => 'Haiti',
    ),
    88 =>
    array (
      'id' => 'Hawaii',
      'iso' => '',
      'value' => 'Hawaii',
    ),
    89 =>
    array (
      'id' => 'Honduras',
      'iso' => 'HN',
      'value' => 'Honduras',
    ),
    90 =>
    array (
      'id' => 'Hong_Kong',
      'iso' => 'HK',
      'value' => 'Hong_Kong',
    ),
    91 =>
    array (
      'id' => 'Hongrie',
      'iso' => 'HU',
      'value' => 'Hongrie',
    ),
    92 =>
    array (
      'id' => 'Inde',
      'iso' => 'IN',
      'value' => 'Inde',
    ),
    93 =>
    array (
      'id' => 'Indonesie',
      'iso' => 'ID',
      'value' => 'Indonesie',
    ),
    94 =>
    array (
      'id' => 'Iran',
      'iso' => 'IR',
      'value' => 'Iran',
    ),
    95 =>
    array (
      'id' => 'Iraq',
      'iso' => 'IQ',
      'value' => 'Iraq',
    ),
    96 =>
    array (
      'id' => 'Irlande',
      'iso' => 'IE',
      'value' => 'Irlande',
    ),
    97 =>
    array (
      'id' => 'Islande',
      'iso' => 'IS',
      'value' => 'Islande',
    ),
    98 =>
    array (
      'id' => 'Israel',
      'iso' => 'IL',
      'value' => 'Israel',
    ),
    99 =>
    array (
      'id' => 'Italie',
      'iso' => 'IT',
      'value' => 'Italie',
    ),
    100 =>
    array (
      'id' => 'Jamaique',
      'iso' => 'JM',
      'value' => 'Jamaique',
    ),
    101 =>
    array (
      'id' => 'Jan Mayen',
      'iso' => 'SJ',
      'value' => 'Jan Mayen',
    ),
    102 =>
    array (
      'id' => 'Japon',
      'iso' => 'JP',
      'value' => 'Japon',
    ),
    103 =>
    array (
      'id' => 'Jersey',
      'iso' => '',
      'value' => 'Jersey',
    ),
    104 =>
    array (
      'id' => 'Jordanie',
      'iso' => 'JO',
      'value' => 'Jordanie',
    ),
    105 =>
    array (
      'id' => 'Kazakhstan',
      'iso' => 'KZ',
      'value' => 'Kazakhstan',
    ),
    106 =>
    array (
      'id' => 'Kenya',
      'iso' => 'KE',
      'value' => 'Kenya',
    ),
    107 =>
    array (
      'id' => 'Kirghizstan',
      'iso' => 'KG',
      'value' => 'Kirghizistan',
    ),
    108 =>
    array (
      'id' => 'Kiribati',
      'iso' => 'KI',
      'value' => 'Kiribati',
    ),
    109 =>
    array (
      'id' => 'Koweit',
      'iso' => 'KW',
      'value' => 'Koweit',
    ),
    110 =>
    array (
      'id' => 'Laos',
      'iso' => 'LA',
      'value' => 'Laos',
    ),
    111 =>
    array (
      'id' => 'Lesotho',
      'iso' => 'LS',
      'value' => 'Lesotho',
    ),
    112 =>
    array (
      'id' => 'Lettonie',
      'iso' => 'LV',
      'value' => 'Lettonie',
    ),
    113 =>
    array (
      'id' => 'Liban',
      'iso' => 'LB',
      'value' => 'Liban',
    ),
    114 =>
    array (
      'id' => 'Liberia',
      'iso' => 'LR',
      'value' => 'Liberia',
    ),
    115 =>
    array (
      'id' => 'Liechtenstein',
      'iso' => 'LI',
      'value' => 'Liechtenstein',
    ),
    116 =>
    array (
      'id' => 'Lituanie',
      'iso' => 'LT',
      'value' => 'Lituanie',
    ),
    117 =>
    array (
      'id' => 'Luxembourg',
      'iso' => 'LU',
      'value' => 'Luxembourg',
    ),
    118 =>
    array (
      'id' => 'Lybie',
      'iso' => '',
      'value' => 'Lybie',
    ),
    119 =>
    array (
      'id' => 'Macao',
      'iso' => 'MO',
      'value' => 'Macao',
    ),
    120 =>
    array (
      'id' => 'Macedoine',
      'iso' => 'MK',
      'value' => 'Macedoine',
    ),
    121 =>
    array (
      'id' => 'Madagascar',
      'iso' => 'MG',
      'value' => 'Madagascar',
    ),
    122 =>
    array (
      'id' => 'Madère',
      'iso' => '',
      'value' => 'Madère',
    ),
    123 =>
    array (
      'id' => 'Malaisie',
      'iso' => 'MY',
      'value' => 'Malaisie',
    ),
    124 =>
    array (
      'id' => 'Malawi',
      'iso' => 'MW',
      'value' => 'Malawi',
    ),
    125 =>
    array (
      'id' => 'Maldives',
      'iso' => 'MV',
      'value' => 'Maldives',
    ),
    126 =>
    array (
      'id' => 'Mali',
      'iso' => 'ML',
      'value' => 'Mali',
    ),
    127 =>
    array (
      'id' => 'Malte',
      'iso' => 'MT',
      'value' => 'Malte',
    ),
    128 =>
    array (
      'id' => 'Man',
      'iso' => '',
      'value' => 'Man',
    ),
    129 =>
    array (
      'id' => 'Mariannes du Nord',
      'iso' => 'MP',
      'value' => 'Mariannes_du_Nord',
    ),
    130 =>
    array (
      'id' => 'Maroc',
      'iso' => 'MZ',
      'value' => 'Maroc',
    ),
    131 =>
    array (
      'id' => 'Marshall',
      'iso' => 'MH',
      'value' => 'Marshall',
    ),
    132 =>
    array (
      'id' => 'Martinique',
      'iso' => 'MQ',
      'value' => 'Martinique',
    ),
    133 =>
    array (
      'id' => 'Maurice',
      'iso' => 'MU',
      'value' => 'Maurice',
    ),
    134 =>
    array (
      'id' => 'Mauritanie',
      'iso' => 'MR',
      'value' => 'Mauritanie',
    ),
    135 =>
    array (
      'id' => 'Mayotte',
      'iso' => 'YT',
      'value' => 'Mayotte',
    ),
    136 =>
    array (
      'id' => 'Mexique',
      'iso' => 'MX',
      'value' => 'Mexique',
    ),
    137 =>
    array (
      'id' => 'Micronesie',
      'iso' => 'FM',
      'value' => 'Micronesie',
    ),
    138 =>
    array (
      'id' => 'Midway',
      'iso' => '',
      'value' => 'Midway',
    ),
    139 =>
    array (
      'id' => 'Moldavie',
      'iso' => 'MD',
      'value' => 'Moldavie',
    ),
    140 =>
    array (
      'id' => 'Monaco',
      'iso' => 'MC',
      'value' => 'Monaco',
    ),
    141 =>
    array (
      'id' => 'Mongolie',
      'iso' => 'MN',
      'value' => 'Mongolie',
    ),
    142 =>
    array (
      'id' => 'Montenegro',
      'iso' => 'ME',
      'value' => 'Montenegro',
    ),
    143 =>
    array (
      'id' => 'Montserrat',
      'iso' => 'MS',
      'value' => 'Montserrat',
    ),
    144 =>
    array (
      'id' => 'Mozambique',
      'iso' => 'MZ',
      'value' => 'Mozambique',
    ),
    145 =>
    array (
      'id' => 'Namibie',
      'iso' => 'NA',
      'value' => 'Namibie',
    ),
    146 =>
    array (
      'id' => 'Nauru',
      'iso' => 'NR',
      'value' => 'Nauru',
    ),
    147 =>
    array (
      'id' => 'Nepal',
      'iso' => 'NP',
      'value' => 'Nepal',
    ),
    148 =>
    array (
      'id' => 'Nicaragua',
      'iso' => 'NI',
      'value' => 'Nicaragua',
    ),
    149 =>
    array (
      'id' => 'Niger',
      'iso' => 'NE',
      'value' => 'Niger',
    ),
    150 =>
    array (
      'id' => 'Nigeria',
      'iso' => 'NG',
      'value' => 'Nigeria',
    ),
    151 =>
    array (
      'id' => 'Niue',
      'iso' => 'NU',
      'value' => 'Niue',
    ),
    152 =>
    array (
      'id' => 'Norfolk',
      'iso' => 'NF',
      'value' => 'Norfolk',
    ),
    153 =>
    array (
      'id' => 'Norvege',
      'iso' => 'NO',
      'value' => 'Norvege',
    ),
    154 =>
    array (
      'id' => 'Nouvelle_Caledonie',
      'iso' => 'NC',
      'value' => 'Nouvelle_Caledonie',
    ),
    155 =>
    array (
      'id' => 'Nouvelle_Zelande',
      'iso' => 'NZ',
      'value' => 'Nouvelle_Zelande',
    ),
    156 =>
    array (
      'id' => 'Oman',
      'iso' => 'OM',
      'value' => 'Oman',
    ),
    157 =>
    array (
      'id' => 'Ouganda',
      'iso' => 'UG',
      'value' => 'Ouganda',
    ),
    158 =>
    array (
      'id' => 'Ouzbekistan',
      'iso' => 'UZ',
      'value' => 'Ouzbekistan',
    ),
    159 =>
    array (
      'id' => 'Pakistan',
      'iso' => 'PK',
      'value' => 'Pakistan',
    ),
    160 =>
    array (
      'id' => 'Palau',
      'iso' => 'PW',
      'value' => 'Palau',
    ),
    161 =>
    array (
      'id' => 'Palestine',
      'iso' => 'PS',
      'value' => 'Palestine',
    ),
    162 =>
    array (
      'id' => 'Panama',
      'iso' => 'PA',
      'value' => 'Panama',
    ),
    163 =>
    array (
      'id' => 'Papouasie_Nouvelle_Guinee',
      'iso' => 'PG',
      'value' => 'Papouasie_Nouvelle_Guinee',
    ),
    164 =>
    array (
      'id' => 'Paraguay',
      'iso' => 'PY',
      'value' => 'Paraguay',
    ),
    165 =>
    array (
      'id' => 'Pays_Bas',
      'iso' => 'NL',
      'value' => 'Pays_Bas',
    ),
    166 =>
    array (
      'id' => 'Perou',
      'iso' => 'PE',
      'value' => 'Perou',
    ),
    167 =>
    array (
      'id' => 'Philippines',
      'iso' => 'PH',
      'value' => 'Philippines',
    ),
    168 =>
    array (
      'id' => 'Pologne',
      'iso' => 'PL',
      'value' => 'Pologne',
    ),
    169 =>
    array (
      'id' => 'Polynesie',
      'iso' => 'PF',
      'value' => 'Polynesie',
    ),
    170 =>
    array (
      'id' => 'Porto_Rico',
      'iso' => 'PR',
      'value' => 'Porto_Rico',
    ),
    171 =>
    array (
      'id' => 'Portugal',
      'iso' => 'PT',
      'value' => 'Portugal',
    ),
    172 =>
    array (
      'id' => 'Qatar',
      'iso' => 'QA',
      'value' => 'Qatar',
    ),
    173 =>
    array (
      'id' => 'Republique_Dominicaine',
      'iso' => 'DO',
      'value' => 'Republique_Dominicaine',
    ),
    174 =>
    array (
      'id' => 'Republique_Tcheque',
      'iso' => 'CZ',
      'value' => 'Republique_Tcheque',
    ),
    175 =>
    array (
      'id' => 'Reunion',
      'iso' => 'RE',
      'value' => 'Reunion',
    ),
    176 =>
    array (
      'id' => 'Roumanie',
      'iso' => 'RO',
      'value' => 'Roumanie',
    ),
    177 =>
    array (
      'id' => 'Royaume_Uni',
      'iso' => 'UK',
      'value' => 'Royaume_Uni',
    ),
    178 =>
    array (
      'id' => 'Russie',
      'iso' => 'RU',
      'value' => 'Russie',
    ),
    179 =>
    array (
      'id' => 'Rwanda',
      'iso' => 'RW',
      'value' => 'Rwanda',
    ),
    180 =>
    array (
      'id' => 'Sahara Occidental',
      'iso' => 'EH',
      'value' => 'Sahara_Occidental',
    ),
    181 =>
    array (
      'id' => 'Sainte_Lucie',
      'iso' => 'LC',
      'value' => 'Sainte_Lucie',
    ),
    182 =>
    array (
      'id' => 'Saint_Marin',
      'iso' => 'SM',
      'value' => 'Saint_Marin',
    ),
    183 =>
    array (
      'id' => 'Salomon',
      'iso' => 'SB',
      'value' => 'Salomon',
    ),
    184 =>
    array (
      'id' => 'Salvador',
      'iso' => 'SV',
      'value' => 'Salvador',
    ),
    185 =>
    array (
      'id' => 'Samoa_Occidentales',
      'iso' => '',
      'value' => 'Samoa_Occidentales',
    ),
    186 =>
    array (
      'id' => 'Samoa_Americaine',
      'iso' => '',
      'value' => 'Samoa_Americaine',
    ),
    187 =>
    array (
      'id' => 'Sao_Tome_et_Principe',
      'iso' => 'ST',
      'value' => 'Sao_Tome_et_Principe',
    ),
    188 =>
    array (
      'id' => 'Senegal',
      'iso' => 'SN',
      'value' => 'Senegal',
    ),
    189 =>
    array (
      'id' => 'Serbie',
      'iso' => 'CS',
      'value' => 'Serbie',
    ),
    190 =>
    array (
      'id' => 'Seychelles',
      'iso' => 'SC',
      'value' => 'Seychelles',
    ),
    191 =>
    array (
      'id' => 'Sierra Leone',
      'iso' => 'SL',
      'value' => 'Sierra_Leone',
    ),
    192 =>
    array (
      'id' => 'Singapour',
      'iso' => 'SG',
      'value' => 'Singapour',
    ),
    193 =>
    array (
      'id' => 'Slovaquie',
      'iso' => 'SK',
      'value' => 'Slovaquie',
    ),
    194 =>
    array (
      'id' => 'Slovenie',
      'iso' => 'SI',
      'value' => 'Slovenie',
    ),
    195 =>
    array (
      'id' => 'Somalie',
      'iso' => 'SO',
      'value' => 'Somalie',
    ),
    196 =>
    array (
      'id' => 'Soudan',
      'iso' => 'SD',
      'value' => 'Soudan',
    ),
    197 =>
    array (
      'id' => 'Sri_Lanka',
      'iso' => 'LK',
      'value' => 'Sri_Lanka',
    ),
    198 =>
    array (
      'id' => 'Suede',
      'iso' => 'SE',
      'value' => 'Suede',
    ),
    199 =>
    array (
      'id' => 'Suisse',
      'iso' => 'CH',
      'value' => 'Suisse',
    ),
    200 =>
    array (
      'id' => 'Surinam',
      'iso' => 'SR',
      'value' => 'Surinam',
    ),
    201 =>
    array (
      'id' => 'Swaziland',
      'iso' => 'SZ',
      'value' => 'Swaziland',
    ),
    202 =>
    array (
      'id' => 'Syrie',
      'iso' => 'SY',
      'value' => 'Syrie',
    ),
    203 =>
    array (
      'id' => 'Tadjikistan',
      'iso' => 'TJ',
      'value' => 'Tadjikistan',
    ),
    204 =>
    array (
      'id' => 'Taiwan',
      'iso' => 'TW',
      'value' => 'Taiwan',
    ),
    205 =>
    array (
      'id' => 'Tonga',
      'iso' => 'TO',
      'value' => 'Tonga',
    ),
    206 =>
    array (
      'id' => 'Tanzanie',
      'iso' => 'TZ',
      'value' => 'Tanzanie',
    ),
    207 =>
    array (
      'id' => 'Tchad',
      'iso' => 'TD',
      'value' => 'Tchad',
    ),
    208 =>
    array (
      'id' => 'Thailande',
      'iso' => 'TH',
      'value' => 'Thailande',
    ),
    209 =>
    array (
      'id' => 'Tibet',
      'iso' => '',
      'value' => 'Tibet',
    ),
    210 =>
    array (
      'id' => 'Timor_Oriental',
      'iso' => 'TL',
      'value' => 'Timor_Oriental',
    ),
    211 =>
    array (
      'id' => 'Togo',
      'iso' => 'TG',
      'value' => 'Togo',
    ),
    212 =>
    array (
      'id' => 'Trinite_et_Tobago',
      'iso' => 'TT',
      'value' => 'Trinite_et_Tobago',
    ),
    213 =>
    array (
      'id' => 'Tristan da cunha',
      'iso' => '',
      'value' => 'Tristan_de_cuncha',
    ),
    214 =>
    array (
      'id' => 'Tunisie',
      'iso' => 'TN',
      'value' => 'Tunisie',
    ),
    215 =>
    array (
      'id' => 'Turkmenistan',
      'iso' => 'TM',
      'value' => 'Turkmenistan',
    ),
    216 =>
    array (
      'id' => 'Turquie',
      'iso' => 'TR',
      'value' => 'Turquie',
    ),
    217 =>
    array (
      'id' => 'Ukraine',
      'iso' => 'UA',
      'value' => 'Ukraine',
    ),
    218 =>
    array (
      'id' => 'Uruguay',
      'iso' => 'UY',
      'value' => 'Uruguay',
    ),
    219 =>
    array (
      'id' => 'Vanuatu',
      'iso' => 'VU',
      'value' => 'Vanuatu',
    ),
    220 =>
    array (
      'id' => 'Vatican',
      'iso' => 'VA',
      'value' => 'Vatican',
    ),
    221 =>
    array (
      'id' => 'Venezuela',
      'iso' => 'VE',
      'value' => 'Venezuela',
    ),
    222 =>
    array (
      'id' => 'Vierges_Americaines',
      'iso' => 'VI',
      'value' => 'Vierges_Americaines',
    ),
    223 =>
    array (
      'id' => 'Vierges_Britanniques',
      'iso' => 'VG',
      'value' => 'Vierges_Britanniques',
    ),
    224 =>
    array (
      'id' => 'Vietnam',
      'iso' => 'VN',
      'value' => 'Vietnam',
    ),
    225 =>
    array (
      'id' => 'Wake',
      'iso' => '',
      'value' => 'Wake',
    ),
    226 =>
    array (
      'id' => 'Wallis et Futuma',
      'iso' => 'WF',
      'value' => 'Wallis et Futuma',
    ),
    227 =>
    array (
      'id' => 'Yemen',
      'iso' => 'YE',
      'value' => 'Yemen',
    ),
    228 =>
    array (
      'id' => 'Zambie',
      'iso' => 'ZM',
      'value' => 'Zambie',
    ),
    229 =>
    array (
      'id' => 'Zimbabwe',
      'iso' => 'ZW',
      'value' => 'Zimbabwe',
    ),
  ),
);
