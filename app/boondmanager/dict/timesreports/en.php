<?php
/**
 * en.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'lastName' => 'Last Name',
		'firstName' => 'First Name',
		'reference' => 'Number',
		'typeOf' => 'Type',
		'term' => 'Date',
		'date' => 'Date',
		'project' => 'Project',
		'TEMPS_TYPEHREF' => 'Hour Code',
		'TYPEH_NAME' => 'Hour Name',
		'TEMPS_DUREE' => 'Quantity'
	],
];
