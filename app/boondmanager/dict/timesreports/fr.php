<?php
/**
 * fr.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

return [
	'sqlLabels' => [
		'lastName' => 'Nom',
		'firstName' => 'Prénom',
		'reference' => 'Matricule',
		'typeOf' => 'Type',
		'term' => 'Date',
		'date' => 'Date',
		'project' => 'Projet',
		'TEMPS_TYPEHREF' => 'Code Heure',
		'TYPEH_NAME' => 'Nom Heure',
		'TEMPS_DUREE' => 'Quantité'
	],
];
