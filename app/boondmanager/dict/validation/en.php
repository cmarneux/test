<?php
/**
 * en.php
 */
return [
	'timesReport' => [
		'mail' => [
			'waitingForValidation' => [
				'objet' => 'Pending timesheet validation',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe timesheet of {firstNameEmployee} {lastNameEmployee} is pending your validation.\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email."
			],
			'validaded' => [
				'object' => 'Validation of timesheet',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe timesheet of {firstNameEmployee} {lastNameEmployee} has been validated by {firstNameNextValidator} {lastNameNextValidator}.\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email."
			],
			'rejected' => [
				'objet' => 'Refusal of timesheet',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe timesheet of {firstNameEmployee} {lastNameEmployee} has been rejected by {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nThe reason is : \"{reason}\".\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email.",
			],
			'refused' => [
				'objet' => 'Refusal of timesheet',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe timesheet of {firstNameEmployee} {lastNameEmployee} has been refused by {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nThe reason is : \"{reason}\".\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email.",
			],
		],
	],
	'expensesReport' => [
		'mail' => [
			'waitingForValidation' => [
				'objet' => 'Pending expenses validation',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe expenses of {firstNameEmployee} {lastNameEmployee} is pending your validation.\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email."
			],
			'validaded' => [
				'object' => 'Validation of expenses',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe expenses of {firstNameEmployee} {lastNameEmployee} has been validated by {firstNameNextValidator} {lastNameNextValidator}.\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email."
			],
			'rejected' => [
				'objet' => 'Refusal of expenses',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe expenses of {firstNameEmployee} {lastNameEmployee} has been rejected by {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nThe reason is : \"{reason}\".\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email.",
			],
			'refused' => [
				'objet' => 'Refusal of expenses',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe expenses of {firstNameEmployee} {lastNameEmployee} has been refused by {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nThe reason is : \"{reason}\".\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email.",
			],
		],
	],
	'absencesReport' => [
		'mail' => [
			'waitingForValidation' => [
				'objet' => 'Pending validation of leave request',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe leave request of {firstNameEmployee} {lastNameEmployee} is pending your validation.\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email."
			],
			'validaded' => [
				'object' => 'Validation of leave request',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe leave request of {firstNameEmployee} {lastNameEmployee} has been validated by {firstNameNextValidator} {lastNameNextValidator}.\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email."
			],
			'rejected' => [
				'objet' => 'Refusal of leave request',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe leave request of {firstNameEmployee} {lastNameEmployee} has been rejected by {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nThe reason is : \"{reason}\".\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email.",
			],
			'refused' => [
				'objet' => 'Refusal of leave request',
				'message' => "Hello {firstNameValidator} {lastNameValidator},\n\nThe leave request of {firstNameEmployee} {lastNameEmployee} has been refused by {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nThe reason is : \"{reason}\".\n\nIf you would like to read it, please log in using the following link : {url}\n\nPlease do not answer this email.",
			],
		],
	]
];
