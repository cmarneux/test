<?php
/**
 * fr.php
 */
return [
	'timesReport' => [
		'mail' => [
			'waitingForValidation' => [
				'objet' => 'Attente de validation d\'une feuille des temps',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator}, \n\nLa feuille des temps de {firstNameEmployee} {lastNameEmployee} est en attente de votre validation.\n\nSi vous souhaitez la consulter, connectez-vous à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email."
			],
			'validaded' => [
				'object' => 'Feuille des temps validée',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa feuille des temps de {firstNameEmployee} {lastNameEmployee} a été validée par {firstNameNextValidator} {lastNameNextValidator}.\n\nSi vous souhaitez la consulter, connectez-vous à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email."
			],
			 'rejected' => [
				 'objet' => 'Rejet d\'une feuille des temps',
				 'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa feuille des temps de {firstNameEmployee} {lastNameEmployee} a été rejetée par {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nLe motif est : \"{reason}\".\n\nVeuillez la modifier en vous connectant à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email.",
			 ],
			'refused' => [
				'objet' => 'Rejet d\'une feuille des temps',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa feuille des temps de {firstNameEmployee} {lastNameEmployee} a été refusée par {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nLe motif est : \"{reason}\".\n\nMerci de ne pas répondre à cet email."
			],
		],
	],
	'expensesReport' => [
		'mail' => [
			'waitingForValidation' => [
				'objet' => 'Attente de validation d\'une note de frais',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator}, \n\nLa note de frais de {firstNameEmployee} {lastNameEmployee} est en attente de votre validation.\n\nSi vous souhaitez la consulter, connectez-vous à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email."
			],
			'validaded' => [
				'object' => 'Note de frais validée',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa note de frais de {firstNameEmployee} {lastNameEmployee} a été validée par {firstNameNextValidator} {lastNameNextValidator}.\n\nSi vous souhaitez la consulter, connectez-vous à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email."
			],
			'rejected' => [
				'objet' => 'Rejet d\'une note de frais',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa note de frais de {firstNameEmployee} {lastNameEmployee} a été rejetée par {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nLe motif est : \"{reason}\".\n\nVeuillez la modifier en vous connectant à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email.",
			],
			'refused' => [
				'objet' => 'Rejet d\'une note de frais',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa note de frais de {firstNameEmployee} {lastNameEmployee} a été refusée par {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nLe motif est : \"{reason}\".\n\nMerci de ne pas répondre à cet email."
			],
		],
	],
	'absencesReport' => [
		'mail' => [
			'waitingForValidation' => [
				'objet' => 'Attente de validation d\'une demande d\'absence',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator}, \n\nLa demande d'absence de {firstNameEmployee} {lastNameEmployee} est en attente de votre validation.\n\nSi vous souhaitez la consulter, connectez-vous à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email."
			],
			'validaded' => [
				'object' => 'Demande d\'absence validée',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa demande d'absence de {firstNameEmployee} {lastNameEmployee} a été validée par {firstNameNextValidator} {lastNameNextValidator}.\n\nSi vous souhaitez la consulter, connectez-vous à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email."
			],
			'rejected' => [
				'objet' => 'Rejet d\'une demande d\'absence',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa demande d'absence de {firstNameEmployee} {lastNameEmployee} a été rejetée par {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nLe motif est : \"{reason}\".\n\nVeuillez la modifier en vous connectant à l'adresse suivante : {url}\n\nMerci de ne pas répondre à cet email.",
			],
			'refused' => [
				'objet' => 'Rejet d\'une demande d\'absence',
				'message' => "Bonjour {firstNameValidator} {lastNameValidator},\n\nLa demande d'absence de {firstNameEmployee} {lastNameEmployee} a été refusée par {firstNamePreviousValidator} {lastNamePreviousValidator}.\n\nLe motif est : \"{reason}\".\n\nMerci de ne pas répondre à cet email."
			],
		],
	]
];
