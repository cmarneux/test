<?php
/**
 * abstractcontroller.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib;

use BoondManager\Lib\Log\Message;
use Wish\Filters\AbstractFilters;
use Wish\Encoders\JsonAPI;
use Wish\Specifications\InterfaceSpecification;
use BoondManager\Services\CurrentUser;
use BoondManager\Lib\MySQL\DbFactory;
use BoondManager\Services\BM;
use BoondManager\Services\Login;
use Wish\Tools;

/**
 * Class AbstractController
 * @package BoondManager\Lib
 */
abstract class AbstractController {
	/**
	 * @var RequestAccess request
	 */
	protected $requestAccess;

	protected $requireClient = true;

	/**
	 * Send JSON response.
	 *
	 * @param array $object Additionnal data array
	 * @param boolean $send Response type :
	 * - If `true` then returns nothing (__void__)
	 * - If `false` then returns *array*
	 * @param boolean $csrf `true` if we change CSRF, `false` if we juste send it, `null` otherwise.
	 * @return array
	 *
	 * @note Data are :
	 * - BoondManager actual version (Necessary to allow front-end to reload)
	 * - User state of connection
	 * - New CSRF if changed
	 * - Additionnal meta data
	 * - Additionnal objet data
	*/
	public static function _sendJSONResponse($object = array(), $send = true, $csrf = false){
		$contextID = Tools::getContext()->startTiming('sendJSON');
		$f3 = \Base::instance();

		$jsonData = (is_array($object) && sizeof($object) > 0)?$object:array();

		$jsonData['meta']['version'] = $f3->get('BOONDMANAGER.VERSION');
		$jsonData['meta']['isLogged'] = CurrentUser::instance()->isLogged();
		$jsonData['meta']['language'] = BM::getLanguage();

		if($csrf) {
			$f3->set('SESSION.csrf', base64_encode(openssl_random_pseudo_bytes(32)));
			$jsonData['meta']['csrf'] = $f3->exists('SESSION.csrf')?$f3->get('SESSION.csrf'):'';
		}

		// just for an easy reading of the output, we sort items to help us to quickly find what we are looking for
		uksort($jsonData, function($k, $k2){
			$weight = ['meta' => 10, 'error' => 5, 'errors' => 5, 'data' => 3, 'include' => 1];
			$weightA = isset($weight[$k]) ? $weight[$k] : 0;
			$weightB = isset($weight[$k2]) ? $weight[$k2] : 0;

			return $weightA < $weightB;
		});

		if($send) {
			header('Expires: ' . date(DATE_RFC1123, 0) );
			header('Last-Modified: ' . date(DATE_RFC1123) );
			header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
			header('Cache-Control: post-check=0, pre-check=0', false);
			header('Pragma: no-cache');
			header('Content-Type: application/json');
			switch(BM::getCurrentAPI()) {
				case BM::API_TYPE_DEFAULT:
					if(isset($jsonData['data'])) {
						$jsonAPIData = JsonAPI::instance()->setIncludeEmptyObjects(BM::includeEmptyObjects())->serialize($jsonData['data']);
						$jsonData = array_merge($jsonData, $jsonAPIData);
					}
			}
			echo json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
		}

		Tools::getContext()->endTiming($contextID);
		return $jsonData;
	}

	/**
	 * Controller init.
	 *
	 * Global access authorization.
	 */
	public function beforeRoute($f3) {
		$ctxID = Tools::getContext()->startTiming('beforeRoute');

		// APPEL REST
		BM::setCurrentAPI( BM::API_TYPE_DEFAULT );

		if(!Login::connexionWithBasicAuth())
			Login::connexionWithXJWTHeader();

		BM::initForRequest();

		$currentUser = CurrentUser::instance();

		if(BM::isNavigatorCheckEnabled() && $currentUser->isLogged() && !$currentUser->checkNavigator()){
			// ALERT ALERT ALERT : Somebody attemps to steel a user session
			session_regenerate_id(false);
			Login::deleteLoginData(true);
			Tools::getContext()->endTiming($ctxID);
			$this->error(400);
		}

		if($this->requireClient && !$currentUser->isLogged()) {
			Tools::getContext()->endTiming($ctxID);
			$this->error(403);
		}

		// create the object request
		$this->requestAccess = new RequestAccess();
		$this->requestAccess->setUser($currentUser);

		Log::addInfo( Message::getAPI($currentUser->getUserId(), $this->requestAccess->getAPIPattern(), $this->requestAccess->getRouteParams(true)));

		Tools::getContext()->endTiming($ctxID);
	}

	public static function internalMock() {
		$f3 = \Base::instance();
		BM::setMockingErrorHandler();
		$f3['INTERNAL'] = true;
		$f3['QUIET'] = true;
		call_user_func_array( array($f3, 'mock'), func_get_args());
		$f3['QUIET'] = false;
		$f3['INTERNAL'] = null;
		BM::setPageErrorHandler();
		return $f3['RESPONSE'];
	}

	/**
	 * Controller cleaning.
	 */
	public function afterRoute() {
		DbFactory::instance()->closeAll();
	}

	/**
	 * Throw a 403 error if no access.
	 *
	 * @param bool $access Calculated access
	 */
	protected function checkAccess($access) {
		if(!$access) $this->error(403);
	}

	/**
	 * Check that the specification is satisfied with the current user
	 * Throw a 403 error if no access.
	 * @param InterfaceSpecification $spec
	 * @param null $data
	 */
	protected function checkAccessWithSpec(InterfaceSpecification $spec, $data = null) {
		$this->requestAccess->data = $data;
		$contextID = Tools::getContext()->startTiming('checkAccessWithSpec');
		$access = $spec->isSatisfiedBy($this->requestAccess);
		Tools::getContext()->endTiming($contextID);
		$this->checkAccess( $access );
	}

	/**
	 * check a filter and trigger an error if not valid
	 * @param \Wish\Filters\AbstractFilters $filter
	 * @param int $code
	 * @param string $msg
	 */
	protected function checkFilter(AbstractFilters $filter, $code = BM::ERROR_GLOBAL_INCORRECT_JSON_SCHEMA, $msg = '') {
		if(!$filter->isValid()) {
			$data = ['invalidData' => $filter->getInvalidDatas()];
			$this->error($code, $msg, $data);
		}
	}

	/**
	 * Throw an error
	 * @param int $code HTTP error type
	 * @param string $msg
	 * @param null $data
	 * @die
	 */
	public function error($code, $msg = '', $data = null) {
		if($code >= 1000 && !$msg) $msg = BM::getErrorMessage($code);
		DbFactory::instance()->closeAll();
		$f3 = \Base::instance();
		$f3->set('ERRORDATA', $data);
		$f3->error($code, $msg);
	}

	/**
	 * Send JSON response
	 * @param array $object
	 * @param bool $send
	 * @param bool $csrf
	 */
	public function sendJSONResponse($object = array(), $send = true, $csrf = false) {
		self::_sendJSONResponse($object, $send, $csrf);
	}
}
