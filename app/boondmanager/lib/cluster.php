<?php
/**
 * Created by PhpStorm.
 * User: Wish
 * Date: 04/12/2016
 * Time: 21:53
 */
namespace BoondManager\Lib;

use BoondManager\Services\BM;
use Wish\FTP;

class Cluster
{

	/**#@+
	 * List of all web servers in cluster
	 * @var int
	 **/
	const
		PROTOCOL_FTP = 'FTP',
		PROTOCOL_LOCAL = 'LOCAL';
	/**#@-*/

	/**
	 * @var array
	 */
	protected $cluster;

	/**
	 * Cluster constructor.
	 * @param array $cluster
	 */
	public function __construct($cluster = array()) {
		$this->cluster = $cluster;

		if(!$this->cluster) {
			$webServers = explode(':', \Base::instance()->get('CLUSTER.SERVERS'));
			$protocol = \Base::instance()->get('CLUSTER.PROTOCOL');

			foreach($webServers as $ip) {
				if($protocol == self::PROTOCOL_LOCAL)
					$this->cluster[] = ['PROTOCOL' => $protocol,
										'ROOTDIRECTORY' => BM::getRootPath().'/'];
				else
					$this->cluster[] = ['PROTOCOL' => $protocol,
										'SERVER' => $ip,
										'USER' => \Base::instance()->get('CLUSTER.USER'),
										'PWD' => \Base::instance()->get('CLUSTER.PWD'),
										'ROOTDIRECTORY' => \Base::instance()->get('CLUSTER.ROOT_DIRECTORY').'/'];
			}
		}
	}

	/**
	 * @param string $file
	 * @param bool $localDelete
	 * @return bool
	 */
	public function deleteFile($file, $localDelete = true)
	{
		$result = true;
		foreach($this->cluster as $server) {
			if($localDelete && ($server['SERVER'] == $_SERVER['REMOTE_ADDR'] || $server['PROTOCOL'] == self::PROTOCOL_LOCAL)) {
				if(!file_exists($server['ROOTDIRECTORY'].$file) || !unlink($server['ROOTDIRECTORY'].$file))
					$result &= false;
			} else {
				$pFTP = new FTP($server['SERVER'], $server['USER'], $server['PWD'], $server['ROOTDIRECTORY']);
				$pFTP->openFTPSocket();
				$result &= $pFTP->deleteFile($file);
				$pFTP->closeFTPSocket();
			}
		}
		return $result;
	}

	/**
	 * @param string $file
	 * @param string $target
	 * @param bool $localUpload
	 * @return boolcd
	 */
	public function uploadFile($file, $target, $localUpload = true)
	{
		$result = true;
		foreach($this->cluster as $server) {
			if($localUpload && ($server['SERVER'] == $_SERVER['REMOTE_ADDR'] || $server['PROTOCOL'] == self::PROTOCOL_LOCAL)) {
				if(realpath($server['ROOTDIRECTORY'].$file) != realpath($server['ROOTDIRECTORY'].$target)) {
					$result &= copy($server['ROOTDIRECTORY'] . $file, $server['ROOTDIRECTORY'] . $target);
					exec('chmod 775 ' . $server['ROOTDIRECTORY'] . $target);
				}
			} else {
				echo 'A';
				$pFTP = new FTP($server['SERVER'], $server['USER'], $server['PWD'], $server['ROOTDIRECTORY']);
				$pFTP->openFTPSocket();
				if(!$pFTP->uploadFile($file, $target, true, false) || !$pFTP->updateChmod($target,'0775'))
					$result &= false;
				$pFTP->closeFTPSocket();
			}
		}
		return $result;
	}

	/**
	 * @param string $directory
	 * @param bool $localDelete
	 * @return bool
	 */
	public function deleteDirectory($directory, $localDelete = true)
	{
		$result = true;
		foreach($this->cluster as $server) {
			if($localDelete && ($server['SERVER'] == $_SERVER['REMOTE_ADDR'] || $server['PROTOCOL'] == self::PROTOCOL_LOCAL)) {
				if(!file_exists($server['ROOTDIRECTORY'].$directory) || !$this->deleteRecursiveDirectory($server['ROOTDIRECTORY'].$directory))
					$result &= false;
			} else {
				$pFTP = new FTP($server['SERVER'], $server['USER'], $server['PWD'], $server['ROOTDIRECTORY']);
				$pFTP->openFTPSocket();
				$result &= $pFTP->deleteDirectory($directory);
				$pFTP->closeFTPSocket();
			}
		}
		return $result;
	}

	/**
	 * @param string $directory
	 * @param bool $localCreation
	 * @return bool
	 */
	public function createDirectory($directory, $localCreation = true)
	{
		$result = true;
		foreach($this->cluster as $server) {
			if($localCreation && ($server['SERVER'] == $_SERVER['REMOTE_ADDR'] || $server['PROTOCOL'] == self::PROTOCOL_LOCAL)) {
				if(!file_exists($server['ROOTDIRECTORY'].$directory)) {
					$result &= mkdir($server['ROOTDIRECTORY'].$directory);
					exec('chmod 775 '.$server['ROOTDIRECTORY'].$directory);
				}
			} else {
				$pFTP = new FTP($server['SERVER'], $server['USER'], $server['PWD'], $server['ROOTDIRECTORY']);
				$pFTP->openFTPSocket();
				if(!$pFTP->createDirectory($directory) || !$pFTP->updateChmod($directory,'0775'))
					$result &= false;
				$pFTP->closeFTPSocket();
			}
		}
		return $result;
	}

	/**
	 * @param string $directory
	 * @return bool
	 */
	private function deleteRecursiveDirectory($directory) {
		if(is_dir($directory)) {
			foreach(scandir($directory) as $object) {
				if($object == "." || $object == "..") continue;    // ADDITION TO SKIP CURRENT AND PARRENT FOLDERS
				if(filetype($directory."/".$object) == "dir") $this->deleteRecursiveDirectory($directory."/".$object); else unlink($directory."/".$object);
			}
			return rmdir($directory);// delete empty directories
		}
	}
}
