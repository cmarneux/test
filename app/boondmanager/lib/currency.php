<?php
/**
 * devise.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib;

use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;

/**
 * Class Currency
 * @package BoondManager\Lib
 */
class Currency {
    /** @var integer */
    protected $defaultCurrency;

    /** @var string */
    protected $defaultSymbol;

    /** @var string */
    protected $defaultName;

    /** @var float */
    protected $defaultExchangeRate;

	/**
	 * Currency constructor.
	 */
    public function __construct() {
		$currentUser = CurrentUser::instance();
        $this->defaultCurrency = $currentUser->getCurrency();
		$this->defaultExchangeRate = $currentUser->getExchangeRate();
        $this->defaultName = $this->getName($this->defaultCurrency);
        $this->defaultSymbol = $this->getSymbol($this->defaultCurrency);
    }

	/**
	 * @param $defaultCurrency
	 * @param $defaultExchangeRate
	 * @return bool
	 */
    function setDefault($defaultCurrency, $defaultExchangeRate) {
		$this->defaultCurrency = $defaultCurrency;
		$this->defaultSymbol = $this->getSymbol($defaultCurrency);
		$this->defaultName = $this->getName($defaultCurrency);
		$this->defaultExchangeRate = ($defaultExchangeRate) ? $defaultExchangeRate : 1;
        return false;
    }

    /**
     * Récupère le taux de change par défaut.
     * @return float
     */
    function getDefaultExchangeRate() {
    	return $this->defaultExchangeRate;
    }

    /**
     * @return integer
     */
    function getDefaultCurrency() {
    	return $this->defaultCurrency;
    }

    /**
     * @return string
     */
    function getDefaultSymbol() {
    	return $this->defaultSymbol;
    }

    /**
     * @return string
     */
    function getDefaultName() {
    	return $this->defaultName;
    }

	/**
	 * @param $currency
	 * @return string
	 * @throws \Exception
	 */
    function getName($currency) {
    	$name = Dictionary::lookup('specific.setting.currency', $currency);
    	if(is_null($name)) throw new \Exception('Currency not found '.$currency);
		return $name;
    }

	/**
	 * @param $currency
	 * @return string
	 * @throws \Exception
	 */
    function getSymbol($currency) {
		$symbol = Dictionary::lookup('specific.setting.currency', $currency, null, 'id', 'symbol');
		if(is_null($symbol)) throw new \Exception('Currency not found '.$currency);
		return $symbol;
    }

    /**
     * Format correctement un taux de change.
     * @param float $exchangeRate Taux de change à formater.
     * @param integer $round Nombre de chiffres après la virgule lors d'un arrondi.
     * @return float
     */
    function getExchangeRate($exchangeRate, $round = -1) {
        if($exchangeRate <= 0)
            return 1;
        else
            return $round >= 0 ? round($exchangeRate, 2) : $exchangeRate;
    }

	/**
	 * Convertit un montant financier stocké en base de données selon un taux de change spécifique.
	 * @param double $amountFromBDD Montant financier stocké en base de données.
	 * @param float|int $exchangeRateFromBDD Taux de change spécifique.
	 * @param integer $round Nombre de chiffres après la virgule lors d'un arrondi.
	 * @param string $thousands_sep Définit le séparateur des milliers.
	 * @param string $decimalSep
	 * @return float
	 */
    function getAmountForUI($amountFromBDD, $exchangeRateFromBDD = 1, $round = -1, $thousands_sep = ' ', $decimalSep = '.') {
        $amountForUI = $amountFromBDD / ($this->getExchangeRate($exchangeRateFromBDD) * $this->defaultExchangeRate);
        return $round >= 0 ? number_format(round($amountForUI, $round), $round, $decimalSep, $thousands_sep) : $amountForUI;
    }

    /**
     * Convertit un montant financier à partir d'un taux spécifique vers une valeur stockable en base de données.
     * @param double $tarif_ui Montant financier dans un taux spécifique.
     * @param float $taux_ui Taux de change spécifique.
     * @param integer $round Nombre de chiffres après la virgule lors d'un arrondi.
     * @return double
     */
    function getAmountForBDD($tarif_ui, $taux_ui = 1.0, $round = -1) {
        $tarif = $tarif_ui * $this->getExchangeRate($taux_ui) * $this->defaultExchangeRate;
        return $round >= 0 ? round($tarif, $round) : $tarif;
    }

    /**
     * Indique si la devise spécifique correspond à celle par défaut.
     * @param integer $value Valeur de la devise spécifique.
     * @return boolean `true` si les devises sont identiques, `false` sinon.
     */
    function isSameToDefault($value) {
    	return $value == $this->defaultCurrency ? true : false;
    }
}
