<?php
/**
 * absence.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\WorkUnitTypeOnlyReference;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;

/**
 * Class Absence
 * @package BoondManager\OldModels\Filters\Profiles
 * @property InputDB id
 * @property InputDate startDate
 * @property InputDate endDate
 * @property InputFloat duration
 * @property InputString title
 * @property WorkUnitTypeOnlyReference workUnitType
 */
class Absence extends AbstractFilters{

	protected $_objectClass = \BoondManager\Models\Absence::class;

	public function __construct( $name = 'absence')
	{
		parent::__construct($name);

		$id = new InputDB('id');
		$id->addFilterExistsInDB('TAB_PABSENCE', 'ID_PABSENCE = ?');
		$this->addInput( $id );

		$this->addInput( new InputDate('startDate') );
		$this->addInput( new InputDate('endDate') );
		$this->addInput( new InputFloat('duration') );

		$title = new InputString('title');
		$title->setMaxLength(250);
		$this->addInput( $title );

		$this->addInput( new WorkUnitTypeOnlyReference() );

		$this->startDate->setRequired(true);
		$this->endDate->setRequired(true);
		$this->duration->setRequired(true);

		$this->workUnitType->setRequired(true);
	}
}
