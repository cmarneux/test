<?php
/**
 * abstractlists.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Lib\Filters;

use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use Wish\Filters\AbstractFilters;

/**
 * Class AbstractSearch
 * @property \Wish\Filters\Inputs\InputString keywords
 * @property \Wish\Filters\Inputs\InputValue order
 * @property \Wish\Filters\Inputs\InputMultiValues perimeterManagers
 * @property InputMultiValues perimeterAgencies
 * @property \Wish\Filters\Inputs\InputMultiValues perimeterBusinessUnits
 * @property \Wish\Filters\Inputs\InputMultiValues perimeterPoles
 * @property \Wish\Filters\Inputs\InputBoolean narrowPerimeter
 * @property InputMultiValues flags
 * @property InputInt page
 * @property InputInt maxResults
 * @property \Wish\Filters\Inputs\InputValue extraction
 * @property \Wish\Filters\Inputs\InputValue encoding
 * @package BoondManager\Lib\Filters
 */
abstract class AbstractSearch extends AbstractFilters {
	/**
	 * @var string
	 */
	const PERIMETER_MODULE = '';

	/**#@+
	 * @var string ORDER asc|desc
	 */
	const
		ORDER_ASC = 'asc',
		ORDER_DESC = 'desc';
	/**#@-*/

	/**#@+
	 * Extraction types
	 * @var string
	 */
	const
		EXTRACTION_CSV = 'csv',
		EXTRACTION_JSON = 'json';
	/**#@-*/

	/**#@+
	 * Encoding types
	 * @var string
	 */
	const
		ENCODING_UTF_8 = 'UTF-8',
		ENCODING_ISO_8859_15 = 'ISO-8859-15';
	/**#@-*/

	/**
	 * Available perimeter
	 * @var Perimeter
	 */
	private $_availablePerimeter;

	/**
	 * the Selected Perimeter (based on what's available)
	 * @var Perimeter
	 */
	private $_selectedPerimeter;

	/**
	 * flags used for the filter
	 * @var \BoondManager\Models\FlagsList
	 */
	private $_flags;

	/**
	 * AbstractSearch constructor.
	 * @param bool $addPerimeter
	 * @param bool $addFlags
	 */
	public function __construct($addPerimeter = true, $addFlags = true) {
		parent::__construct();

		$f3 = \Base::instance();
		if($addPerimeter) {
			$perimeter = CurrentUser::instance()->getSearchPerimeter(static::PERIMETER_MODULE);
			$this->setAvailablePerimeter($perimeter);
			$this->narrowPerimeter->setDefaultValue(CurrentUser::instance()->getNarrowPerimeter());

		}

		if($addFlags) {
			$flags = CurrentUser::instance()->getSearchFlags();
			$this->setAvailableFlags($flags);
		}

		$input = new InputInt('page');
		$input->setModeDefaultValue(1)
			->setMin(1);
		$this->addInput($input);

		$input = new InputInt('maxResults');
		$input->setModeDefaultValue($f3->get('BOONDMANAGER.NB_RESULTATS_APPLICATION'))
			->setMin(1)
			->setMax($f3->get('BOONDMANAGER.NB_MAX_RESULTATS_MYSQL'));
		$this->addInput($input);

		$this->addInput(new InputString('keywords'));

		$input = new InputValue('order');
		$input->setModeDefaultValue(self::ORDER_ASC)
			->addFilterInArray([self::ORDER_ASC, self::ORDER_DESC]);
		$this->addInput($input);

		$input = new InputValue('extraction');
		$input->setModeDefaultValue(self::EXTRACTION_JSON)
			->addFilterInArray([self::EXTRACTION_CSV, self::EXTRACTION_JSON]);
		$this->addInput($input);

		$input = new InputValue('encoding');
		$input->setModeDefaultValue(self::ENCODING_UTF_8)
			->addFilterInArray([self::ENCODING_UTF_8, self::ENCODING_ISO_8859_15]);
		$this->addInput($input);
	}

	/**
	 * disable the max result limit
	 */
	public function disableMaxResultLimit(){
		$this->maxResults->resetFilters();
		$this->maxResults->addFilter(FILTER_VALIDATE_INT, ['min_range'=>1, 'max_range'=>2147483647]);
		$this->maxResults->setValue(2147483647);
	}

	/**
	 * create a filter from the perimeter available
	 * @param Perimeter $perimeter
	 * @return $this
	 */
	public function setAvailablePerimeter(Perimeter $perimeter) {
		$this->_availablePerimeter = $perimeter;
		$this->_selectedPerimeter = null;

		// should search be wide (using OR) or narrow (using AND) on perimeter categories (user, agency, bus, ...)
		$narrowPerimeter = new InputBoolean('narrowPerimeter');
		$narrowPerimeter->setModeDefaultValue( $perimeter->matchAll() );
		$this->addInput($narrowPerimeter);

		$pManagers = new InputMultiValues('perimeterManagers');
		$pManagers->addFilterInArray($perimeter->getManagersIDs());
		if($perimeter->isPerimeterRequired()){
			$pManagers->setAllowEmptyValue( false );
			$pManagers->setModeDefaultValue( $perimeter->getManagersIDs() );
		}
		$this->addInput($pManagers);

		if($agenciesIDs = $perimeter->getAgenciesIDs()) {
			$pAgencies = new InputMultiValues('perimeterAgencies');
			$pAgencies->addFilterInArray($agenciesIDs);
			if($perimeter->isPerimeterRequired()){
				$pAgencies->setAllowEmptyValue( false );
				$pAgencies->setModeDefaultValue( $agenciesIDs );
			}
			$this->addInput($pAgencies);
		}

		if($polesIDs = $perimeter->getPolesIDs()) {
			$pPoles = new InputMultiValues('perimeterPoles');
			$pPoles->addFilterInArray($polesIDs);
			if($perimeter->isPerimeterRequired()){
				$pPoles->setAllowEmptyValue( false );
				$pPoles->setModeDefaultValue( $polesIDs );
			}
			$this->addInput($pPoles);
		}

		if($busIDs = $perimeter->getBUsIDs()) {
			$pBUs = new InputMultiValues('perimeterBusinessUnits');
			$pBUs->addFilterInArray($busIDs);
			if($perimeter->isPerimeterRequired()){
				$pBUs->setAllowEmptyValue( false );
				$pBUs->setModeDefaultValue( $busIDs );
			}
			$this->addInput($pBUs);
		}

		return $this;
	}

	public function setIndifferentPerimeter(){
		$perimeter = new Perimeter();
		$perimeter->setRequiredPerimeter(false);
		$this->setAvailablePerimeter($perimeter);
	}

	/**
	 * return the available perimeter used to build the filter
	 * @return Perimeter
	 */
	public function getAvailablePerimeter(){
		return $this->_availablePerimeter;
	}

	/**
	 * return a selected Perimeter
	 * @return Perimeter
	 */
	public function getSelectedPerimeter(){
		$this->_selectedPerimeter = new Perimeter( $this->getAvailablePerimeter()->getUser() );

		foreach ($this->perimeterManagers->getValue() as $id)
			$this->_selectedPerimeter->addManager( $this->getAvailablePerimeter()->getManager( $id ) );

		if($this->perimeterAgencies) {
			foreach ($this->perimeterAgencies->getValue() as $id)
				$this->_selectedPerimeter->addAgency( $this->getAvailablePerimeter()->getAgency( $id ) );
		}

		if($this->perimeterPoles) {
			foreach ($this->perimeterPoles->getValue() as $id)
				$this->_selectedPerimeter->addPole( $this->getAvailablePerimeter()->getPole( $id ) );
		}

		if($this->perimeterBusinessUnits) {
			foreach ($this->perimeterBusinessUnits->getValue() as $id)
				$this->_selectedPerimeter->addBU( $this->getAvailablePerimeter()->getBU( $id ) );
		}

		return $this->_selectedPerimeter;
	}

	/**
	 * build a filter on flags available
	 * @param FlagsList $flags
	 * @return $this
	 */
	public function setAvailableFlags(FlagsList $flags){
		$this->_flags = $flags;
		$input = new inputMultiValues('flags');
		$input->addFilterInDict($flags->getFormattedList());
		$this->addInput($input);
		return $this;
	}

	/**
	 * return the flags used to build the filter
	 * @return FlagsList
	 */
	public function getAvailableFlags(){
		return $this->_flags;
	}

	protected function postValidation()
	{
		parent::postValidation();

		if(   !$this->extraction->isDefined()
			|| $this->extraction->getValue() != 'csv'
			|| !CurrentUser::instance()->hasRight(BM::RIGHT_EXTRACTION, static::PERIMETER_MODULE)
		){
			$this->extraction->setDisabled(true);
		}
	}

	/**
	 * clone a filter and clone the perimeter and flags
	 */
	public function __clone(){
		$this->_selectedPerimeter = clone $this->_selectedPerimeter;
		$this->_availablePerimeter = clone $this->_availablePerimeter;
		$this->_flags = clone $this->_flags;
		parent::__clone();
	}
}
