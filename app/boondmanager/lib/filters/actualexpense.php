<?php
/**
 * actualexpense.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Currency;
use BoondManager\Lib\Filters\Inputs\Attributes\ExchangeRate;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;
use Wish\MySQL\Where;

/**
 * Class ActualExpense
 * @property InputDB id
 * @property InputString title
 * @property InputInt number
 * @property InputFloat tax
 * @property InputBoolean reinvoiced
 * @property InputEnum activityType
 * @property Currency currency
 * @property InputFloat exchangeRate
 */
class ActualExpense extends Expense {

	public function __construct($name = 'actualExpenses') {

		parent::__construct($name);

		$id = new InputDB('id');
		$id->addFilterExistsInDB('TAB_FRAISREEL', new Where('ID_FRAISREEL = ?'));
		$this->addInput($id);

		$title = new InputString('title');
		$title->setMaxLength(250);
		$this->addInput($title);

		$number = new InputInt('number');
		$number->setmin(0);
		$this->addInput($number);


		$this->addInput( new Currency() );
		$this->addInput( new ExchangeRate() );
		$this->addInput( new InputFloat('tax'));
		$this->addInput( new InputBoolean('reinvoiced') );

		$this->title->setRequired(true);
		$this->number->setRequired(true);
		$this->currency->setRequired(true);
		$this->exchangeRate->setRequired(true);
		$this->tax->setRequired(true);
		$this->reinvoiced->setRequired(true);
	}
}
