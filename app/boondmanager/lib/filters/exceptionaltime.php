<?php
/**
 * exceptionaltime.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDB;
use Wish\MySQL\Where;

/**
 * Class ExceptionalTime
 * @property InputDB id
 */
class ExceptionalTime extends Time {

	public function __construct($name = 'exceptionalTime') {

		parent::__construct($name);

		$this->addInput( new InputDate('endDate') );

		$id = new InputDB('id');
		$id->addFilterExistsInDB('TAB_TEMPSEXCEPTION', new Where('ID_TEMPSEXCEPTION = ?'));
		$this->addInput($id);
	}

	protected function postValidation()
	{
		if($this->endDate->isDefined() && $this->startDate->isDefined() &&
			$this->endDate->getValue() < $this->startDate->getValue()
		) {
			$this->startDate->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
			$this->endDate->invalidate(BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE);
		}
	}
}
