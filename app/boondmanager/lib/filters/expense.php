<?php
/**
 * expense.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Batch;
use BoondManager\Lib\Filters\Inputs\Attributes\Delivery;
use BoondManager\Lib\Filters\Inputs\Attributes\ExpenseTypeOnlyReference;
use BoondManager\Lib\Filters\Inputs\Attributes\Project;
use BoondManager\Services\BM;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use BoondManager\Models;

/**
 * Class Expense
 * @package BoondManager\Models\Filters\Profiles
 * @property InputDB id
 * @property InputDateTime startDate
 * @property InputFloat numberOfKilometers
 * @property InputFloat amountIncludingTax
 * @property InputBoolean isKilometricExpense
 * @property ExpenseTypeOnlyReference expenseType
 * @property InputEnum activityType
 * @property Delivery delivery
 * @property Project project
 * @property Batch batch
 *
 */
class Expense extends AbstractFilters{

	protected $_objectClass = Models\Expense::class;

	public function __construct($name = 'expense')
	{
		parent::__construct($name);

		$this->addInput( new InputFloat('numberOfKilometers') );
		$this->addInput( new InputFloat('amountIncludingTax') );
		$this->addInput( new InputBoolean('isKilometricExpense') );
		$this->addInput( new ExpenseTypeOnlyReference() );
		$this->expenseType->reference->setRequired(false);
		$this->addInput( new InputDate('startDate') );

		$activity = new InputEnum('activityType');
		$activity->setAllowedValues(['production','absence', 'internal']);
		$this->addInput($activity);

		$this->addInput( new Delivery() );
		$this->addInput( new Project());
		$this->addInput( new Batch() );

		/*
		 *  fields required
		 */
		$this->startDate->setRequired(true);
		$this->amountIncludingTax->setRequired(true);
		$this->isKilometricExpense->setRequired(true);
		$this->activityType->setRequired(true);
	}

	protected function postValidation() {
		if($this->isKilometricExpense->getValue()) {
			if($this->expenseType->isDefined()) $this->expenseType->invalidate('can\t be related to an expense type if it is a kilometric expense');
			if(!$this->numberOfKilometers->isDefined()) $this->numberOfKilometers->invalidate('field is required');

		} else { // not a kilometric expense
			$this->expenseType->reference->setRequired(true);
			if($this->numberOfKilometers->isDefined()) $this->numberOfKilometers->invalidate('an expense can\'t have a number of kilometer if not flaged as a kilometricExpense');
		}
	}
}
