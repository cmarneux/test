<?php
/**
 * extract.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Extract
 * @package Wish\Filters\Inputs\InputValue
 * @property InputValue $encoding
 * @property InputValue $format
 */
class Extract extends AbstractFilters{
	/**#@+
	 * Extraction's format
	 * @var string
	 **/
	const
		FORMAT_CSV = 'csv',
		FORMAT_JSON = 'json';

	/**#@+
	 * Extraction's encoding
	 * @var string
	 **/
	const
		ENCODING_UTF8 = 'utf8',
		ENCODING_ISO_8859_15 = 'iso-8859-15';

	/**
	 * Extract constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$format = new InputValue('format', self::FORMAT_JSON);
		$format->addFilterInArray([ self::FORMAT_JSON, self::FORMAT_CSV ]);

		$encoding = new InputValue('encoding', self::ENCODING_UTF8);
		$encoding->addFilterInArray([self::ENCODING_ISO_8859_15, self::ENCODING_UTF8]);

		$this->addInput([$format, $encoding]);
	}
}
