<?php
/**
 * fixedexpense.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputInt;
use Wish\MySQL\Where;

/**
 * Class FixedExpense
 * @property InputDB id
 * @property InputInt row
 */
class FixedExpense extends Expense {

	public function __construct($name = 'fixedExpense') {

		parent::__construct($name);

		$id = new InputDB('id');
		$id->addFilterExistsInDB('TAB_FRAIS', new Where('ID_FRAIS = ?'));
		$this->addInput($id);

		$row = new InputDB('row');
		$row->setRequired(true);
		$row->addFilterCallback(function($value) {
			if($value <= 0 ) return $value;
			else return $this->existsInDB($value, 'TAB_LIGNEFRAIS', new Where('ID_LIGNEFRAIS = ?'));
		});
		$this->addInput( $row );
	}
}
