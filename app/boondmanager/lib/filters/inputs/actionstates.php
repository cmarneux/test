<?php
/**
 * actionstates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputMultiValues;
use BoondManager\Services\BM;
use BoondManager\Services;

class ActionStates extends AbstractFilters{

	private $selectedCategoriesFromStates = [];

	public function __construct($name, array $categories)
	{
		parent::__construct();

		$this->setName($name);

		$dict = Services\Actions::getStates($categories);

		foreach($dict as $catID => $subDict){
			$input = new InputMultiValues(BM::getCategoryName($catID));
			$input->addFilterInDict($subDict);
			$input->addFilter(FILTER_CALLBACK, function($value) use ($catID){
				if(!array_key_exists($catID, $this->selectedCategoriesFromStates))
					$this->selectedCategoriesFromStates[$catID] = [];

				$this->selectedCategoriesFromStates[$catID][] = $value;
				return $value;
			});
			$this->addInput($input);
		}
	}

	// surchache pour ember ><
	public function setData($data)
	{
		// on essaye de lire le format "ember" et de le convertir. si ça ne marche pas des la premiere  valeur,
		// on transmet directement les data entrantes sans modifications
		if($data && !is_array($data)) $data = [$data];

		if(is_array($data)) {
			$emberData = [];
			$emberFormat = true;
			foreach ($data as $key => $value) {
				if (is_string($value)) {
					$subData = explode('_', $value);
					if(count($subData) != 2) continue;
					$module = $subData[0];
					$id = $subData[1];
					if (!isset($emberData[ $module ])) $emberData[ $module ] = [];
					$emberData[ $module ][] = $id;
				} else {
					$emberFormat = false;
					break;
				}
			}
			$data = ($emberFormat)?$emberData:$data;
		}else{
			$data = [];
		}

		return parent::setData($data);
	}

	public function getValues($cat = null)
	{
		if(is_null($cat)) return $this->selectedCategoriesFromStates;
		else if(isset($this->selectedCategoriesFromStates[$cat])) return $this->selectedCategoriesFromStates[$cat];
		else return [];
	}
}
