<?php
/**
 * absencesquota.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;

/**
 * Class AloneMarker
 * @property InputId id
 * @property InputEnum period
 * @property WorkUnitTypeOnlyReference workUnitType
 * @property InputFloat quotaAcquired
 * @property InputEnum useBeingAcquired
 * @property InputFloat monthlyIncrement
 * @property InputBoolean autoDeferral
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class AbsencesQuota extends AbstractFilters {
    protected $_objectClass = Models\AbsencesQuota::class;

    /**
     * AbsencesQuota constructor.
     */
    public function __construct() {
        parent::__construct();

        $input = new InputId();
        $this->addInput($input);

        $input = new InputEnum('period');
        $input->setAllowedValues(Models\AbsencesQuota::ACCOUNTSPERIOD);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new WorkUnitTypeOnlyReference();
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputFloat('quotaAcquired');
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputEnum('useBeingAcquired');
        $input->setAllowedValues(Models\AbsencesQuota::USEBEINGACQUIRED);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputFloat('monthlyIncrement');
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputBoolean('autoDeferral');
        $input->setRequired(true);
        $this->addInput($input);
    }

    /**
     * @param array $absencesQuotas
     * @return $this
     */
    public function setAllowedAbsencesQuotas($absencesQuotas) {
        $this->id->addFilterIdExists($absencesQuotas);
        return $this;
    }

    /**
     * @param array $workUnitTypes
     * @return $this
     */
    public function setAllowedWorkUnitTypes($workUnitTypes) {
        $this->workUnitType->reference->addFilterReferenceExists($workUnitTypes);
        return $this;
    }
}
