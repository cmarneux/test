<?php
/**
 * additionalturnoverandcosts.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models\DeliveryDetail;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;

/**
 * Class AdditionalTurnoverAndCosts
 * @property InputId id
 * @property InputDate date
 * @property InputBoolean state
 * @property InputString title
 * @property InputFloat turnoverExcludingTax
 * @property InputFloat costsExcludingTax
 * @package BoondManager\Lib\Inputs\Filters\Attributes
 */
class AdditionalTurnoverAndCosts extends AbstractFilters {

	protected $_objectClass = DeliveryDetail::class;

	/**
	 * AdditionalTurnoverAndCosts constructor.
	 */
	public function __construct() {
		parent::__construct();

		$input = new InputId();
		$this->addInput($input);

		$input = new InputDate('date');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputBoolean('state');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputString('title');
		$input->setMaxLength(100);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('turnoverExcludingTax');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('costsExcludingTax');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param $additionalTurnoverAndCosts
	 * @return $this
	 */
	public function setAllowedTurnoverAndCosts($additionalTurnoverAndCosts) {
		$this->id->addFilterIdExists($additionalTurnoverAndCosts);
		return $this;
	}
}
