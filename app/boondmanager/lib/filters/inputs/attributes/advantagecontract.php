<?php
/**
 * advantagetype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use BoondManager\Models;
use Wish\Filters\Inputs\InputReference;

/**
 * Class Advantage
 * @package BoondManager\Models\Filters\Profiles
 * @property InputReference $reference
 * @property InputEnum frequency
 * @property InputEnum category
 * @property InputFloat $participationQuota
 * @property InputFloat $agencyQuota
 * @property InputFloat $employeeQuota
 */
class AdvantageContract extends AbstractFilters{

	protected $_objectClass = Models\AdvantageType::class;

	/**
	 * Advantage constructor.
	 * @param string $name
	 */
	public function __construct($name)
	{
		parent::__construct($name);

		$reference = new InputReference();
		$reference->setRequired(true);

		$frequency = new InputEnum('frequency');
		$frequency->setRequired(true);
		$frequency->setAllowedValues( Models\AdvantageType::getFrequencies() );

		$category = new InputEnum('category');
		$category->setRequired(true);
		$category->setAllowedValues(Models\AdvantageType::getCategories());

		$participationAmount = new InputFloat('participationQuota');

		$agencyAmount = new InputFloat('agencyQuota');

		$employeeAmount = new InputFloat('employeeQuota');

		$this->addInput([$reference, $frequency, $category, $participationAmount, $agencyAmount, $employeeAmount]);
	}

	/**
	 * @param Models\AdvantageType[] $advantagesTypes
	 * @return $this
	 */
	public function setAllowedAdvantageTypes($advantagesTypes) {
		$this->reference->addFilterReferenceExists($advantagesTypes);
		return $this;
	}
}

