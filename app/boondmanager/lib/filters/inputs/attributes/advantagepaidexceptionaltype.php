<?php
/**
 * advantagepaidexceptionaltype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use BoondManager\Services\BM;
use Wish\Filters\AbstractFilters;
use Wish\Tools;

/**
 * Class AdvantagePaidExceptionalType
 * @property WorkUnitTypeOnlyReference workUnitType
 * @property AdvantageTypeOnlyReference advantageType
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class AdvantagePaidExceptionalType extends AbstractFilters {
	protected $_objectClass = Models\AdvantagePaidExceptionalType::class;

	/**
	 * WorkUnitType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'advantagePaidExceptionalType') {
		parent::__construct();

		$this->setName($name);

		$input = new WorkUnitTypeOnlyReference();
		$input->setRequired(true);
		$this->addInput($input);

		$input = new AdvantageTypeOnlyReference();
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $workUnitTypes
	 * @return $this
	 */
	public function setAllowedWorkUnitTypes($workUnitTypes) {
		$this->workUnitType->setAllowedWorkUnitTypes($workUnitTypes);
		$this->workUnitType->onlyExceptionalWorkUnit($workUnitTypes, Models\Agency::ERROR_AGENCY_WRONG_ACTIVITYTYPE_FOR_ADVANTAGES);
		return $this;
	}

	/**
	 * @param array $advantageTypes
	 * @return $this
	 */
	public function setAllowedAdvantageTypes($advantageTypes) {
		$this->advantageType->setAllowedAdvantageTypes($advantageTypes);
		$this->advantageType->onlyPunctualAdvantage($advantageTypes, Models\Agency::ERROR_AGENCY_WRONG_ADVANTAGEFREQUENCY);
		return $this;
	}
}
