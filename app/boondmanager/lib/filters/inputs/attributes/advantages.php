<?php
/**
 * advantages.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models\AdvantageType;
use BoondManager\Models\Agency;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputRelationId;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;

/**
 * Class Advantages
 * @property InputInt reference
 * @property InputValue frequency
 * @property InputValue category
 * @property InputFloat participationQuota
 * @property InputFloat agencyQuota
 * @property InputFloat employeeQuota
 * @property InputRelationId agency
 * @package BoondManager\Lib\Inputs\Filters\Attributes
 */
class Advantages extends AbstractFilters {

	protected $_objectClass = AdvantageType::class;

	/**
	 * Advantages constructor.
	 */
	public function __construct() {
		parent::__construct();

		$input = new InputInt('reference');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputEnum('frequency');
		$input->setRequired(true);
		$input->setAllowedValues(AdvantageType::MAPPER_FREQUENCY);
		$this->addInput($input);

		$input = new InputEnum('category');
		$input->setRequired(true);
		$input->setAllowedValues(AdvantageType::MAPPER_CATEGORY);
		$this->addInput($input);

		$input = new InputFloat('participationQuota');
		$input->setRequired(true);
		$this->addInput( $input );

		$input = new InputFloat('agencyQuota');
		$input->setRequired(true);
		$this->addInput( $input );

		$input = new InputFloat('employeeQuota');
		$input->setRequired(true);
		$this->addInput( $input );

		$input = new InputRelationId('agency');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param AdvantageType[] $advantagesTypes
	 * @return $this
	 */
	public function setAllowedReferences($advantagesTypes) {
		$this->reference->addFilterInArray(Tools::getFieldsToArray($advantagesTypes, 'reference'));
		return $this;
	}

	/**
	 * @param Agency[] $agencies
	 * @return $this
	 */
	public function setAllowedAgencies($agencies){
		$this->agency->addFilterInIds(Tools::useColumnAsKey('id', $agencies));
		return $this;
	}
}
