<?php
/**
 * advantagetype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;

/**
 * Class AdvantageType
 * @property InputId id
 * @property InputReference reference
 * @property InputString name
 * @property InputEnum frequency
 * @property InputEnum category
 * @property InputFloat participationQuota
 * @property InputFloat agencyQuota
 * @property InputFloat employeeQuota
 * @property InputBoolean default
 * @property InputBoolean state
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class AdvantageType extends AbstractFilters {
	protected $_objectClass = Models\AdvantageType::class;

	/**
	 * WorkUnitType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'advantageType') {
		parent::__construct($name);

		$input = new InputId();
		$this->addInput($input);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputString('name');
		$input->setMaxLength(100);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputEnum('frequency');
		$input->setRequired(true);
		$input->setAllowedValues(Models\AdvantageType::MAPPER_FREQUENCY);
		$this->addInput($input);

		$input = new InputEnum('category');
		$input->setRequired(true);
		$input->setAllowedValues(Models\AdvantageType::MAPPER_CATEGORY);
		$this->addInput($input);

		$input = new InputFloat('participationQuota');
		$this->addInput( $input );

		$input = new InputFloat('agencyQuota');
		$this->addInput( $input );

		$input = new InputFloat('employeeQuota');
		$this->addInput( $input );

		$input = new InputBoolean('default');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputBoolean('state');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $advantageTypes
	 * @return $this
	 */
	public function setAllowedAdvantageTypes($advantageTypes) {
		$this->id->addFilterIdExists($advantageTypes);
		return $this;
	}

	protected function postValidation() {
		if($this->category->getValue() == Models\AdvantageType::CATEGORY_LOAN && $this->frequency->getValue() != Models\AdvantageType::FREQUENCY_PUNCTUAL)
			$this->category->invalidateIfDebug(Models\Agency::ERROR_AGENCY_CATEGORY_LOAN_CANNOT_BE_SET);

		if($this->category->getValue() == Models\AdvantageType::CATEGORY_PACKAGE && $this->frequency->getValue() == Models\AdvantageType::FREQUENCY_PUNCTUAL)
			$this->category->invalidateIfDebug(Models\Agency::ERROR_AGENCY_CATEGORY_PACKAGE_CANNOT_BE_SET);

		if($this->category->getValue() == Models\AdvantageType::CATEGORY_VARIABLESALARYBASIS && $this->frequency->getValue() == Models\AdvantageType::FREQUENCY_PUNCTUAL)
			$this->category->invalidateIfDebug(Models\Agency::ERROR_AGENCY_CATEGORY_VARIABLESALARYBASIS_CANNOT_BE_SET);

		if($this->participationQuota->isDefined() && !in_array($this->category->getValue(), [Models\AdvantageType::CATEGORY_PACKAGE, Models\AdvantageType::CATEGORY_VARIABLESALARYBASIS]))
			$this->category->invalidateIfDebug(Models\Agency::ERROR_AGENCY_PARTICIPATIONQUOTA_CANNOT_BE_SET);

		if($this->employeeQuota->isDefined() && !in_array($this->category->getValue(), [Models\AdvantageType::CATEGORY_PACKAGE, Models\AdvantageType::CATEGORY_VARIABLESALARYBASIS]))
			$this->category->invalidateIfDebug(Models\Agency::ERROR_AGENCY_EMPLOYEEQUOTA_CANNOT_BE_SET);

		if($this->agencyQuota->isDefined() && $this->category->getValue() == Models\AdvantageType::CATEGORY_LOAN)
			$this->category->invalidateIfDebug(Models\Agency::ERROR_AGENCY_AGENCYQUOTA_CANNOT_BE_SET);
	}
}
