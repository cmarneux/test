<?php
/**
 * advantagetypeonlyreference.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use BoondManager\Services\BM;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputReference;
use Wish\Tools;

/**
 * Class AdvantageTypeOnlyReference
 * @property InputReference reference
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class AdvantageTypeOnlyReference extends AbstractFilters {
	protected $_objectClass = Models\AdvantageType::class;

	/**
	 * WorkUnitType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'advantageType') {
		parent::__construct();

		$this->setName($name);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param Models\AdvantageType[] $advantageTypes
	 * @return $this
	 */
	public function setAllowedAdvantageTypes($advantageTypes) {
		$this->reference->addFilterReferenceExists($advantageTypes);
		return $this;
	}

	/**
	 * @param array $advantageTypes
	 * @param int|string $errorMsg
	 * @return $this
	 */
	public function onlyPunctualAdvantage($advantageTypes, $errorMsg = BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST) {
		$this->reference->addFilterCallback(function($value) use($advantageTypes) {
			$allowedAdvantageTypes = Tools::useColumnAsKey('reference', $advantageTypes);
			/**
			 * @var Models\AdvantageType $at
			 */
			if(array_key_exists($value, $allowedAdvantageTypes)) {
				$at = $allowedAdvantageTypes[$value];
				return $at->frequency == Models\AdvantageType::FREQUENCY_PUNCTUAL ? $value : false;
			}
			return false;
		}, $errorMsg);
		return $this;
	}
}
