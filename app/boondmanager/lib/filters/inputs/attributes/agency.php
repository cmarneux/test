<?php
/**
 * agency.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;

/**
 * Class Agency
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Agency extends AbstractFilters{
	/**
	 * Agency constructor.
	 * @param $name
	 */
	public function __construct($name = 'agency'){
		parent::__construct();

		$this->setName($name);

		$this->addInput([
			(new InputInt('id'))->setMin(1),
			(new InputString('name'))->setMinLength(1),
		]);
	}
}
