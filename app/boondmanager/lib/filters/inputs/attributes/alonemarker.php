<?php
/**
 * alonemarker.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputRelationId;

/**
 * Class AloneMarker
 * @property InputId id
 * @property InputDate date
 * @property InputString title
 * @property InputFloat progressRate
 * @property InputRelationId resource
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class AloneMarker extends AbstractFilters {
	/**
	 * AloneMarker constructor.
	 * @param array $aloneMarkers
	 */
	public function __construct($aloneMarkers = []) {
		parent::__construct();

		$input = new InputId();
		$input->addFilterIdExists($aloneMarkers);
		$this->addInput($input);

		$input = new InputString('title');
		$input->setMaxLength(100);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputDate('date');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('progressRate');
		$input->setMin(0);
		$input->setMax(100);
		$input->setRequired(true);
		$this->addInput($input);
	}
}
