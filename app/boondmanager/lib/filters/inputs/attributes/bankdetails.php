<?php
/**
 * bankdetails.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputString;

/**
 * Class BankDetails
 * @property InputId id
 * @property InputString description
 * @property InputString iban
 * @property InputString bic
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class BankDetails extends AbstractFilters {
    protected $_objectClass = Models\BankDetails::class;

    /**
     * WorkUnitType constructor.
     */
    public function __construct($name = 'bankDetails') {
        parent::__construct();

        $this->setName($name);

        $input = new InputId();
        $this->addInput($input);

        $input = new InputString('description');
        $input->setMaxLength(500);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputString('iban');
        $input->setMaxLength(34);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputString('bic');
        $input->setMaxLength(11);
        $input->setRequired(true);
        $this->addInput($input);
    }

    /**
     * @param array $banksDetails
     * @return $this
     */
    public function setAllowedBanksDetails($banksDetails) {
        $this->id->addFilterIdExists($banksDetails);
        return $this;
    }
}
