<?php
/**
 * batch.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;

/**
 * Class Delivery
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 * @property InputInt id
 */
class Batch extends AbstractFilters{

	protected $_objectClass = \BoondManager\Models\Batch::class;

	/**
	 * Batch constructor.
	 * @param $name
	 */
	public function __construct($name = 'batch'){
		parent::__construct($name);

		$id = new InputInt('id');
		$id->setMin(1);
		$this->addInput($id);
	}
}
