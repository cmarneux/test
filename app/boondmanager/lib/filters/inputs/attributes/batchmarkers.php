<?php
/**
 * batchmarkers.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputMultiObjects;

/**
 * Class BatchMarkers
 * @property InputId id
 * @property InputString title
 * @property InputFloat durationForecast
 * @property InputFloat remainsToBeDone
 * @property InputFloat distributionRate
 * @property InputMultiObjects markers
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class BatchMarkers extends AbstractFilters {
	/**
	 * BatchesMarkers constructor.
	 * @param array $batches
	 * @param array $markers
	 * @param array $resources
	 */
	public function __construct($batches = [], $markers = [], $resources = []) {
		parent::__construct();

		$input = new InputId();
		$input->addFilterIdExists($batches);
		$this->addInput($input);

		$input = new InputString('title');
        $input->setRequired(true);
		$input->setMaxLength(100);
		$this->addInput($input);

		$input = new InputFloat('distributionRate');
        $input->setRequired(true);
		$input->setMin(0);
		$input->setMax(100);
		$this->addInput($input);

		$input = new InputMultiObjects('markers', (new Marker($markers, $resources)));
		$input->setRequired(true);
		$this->addInput($input);
	}
}
