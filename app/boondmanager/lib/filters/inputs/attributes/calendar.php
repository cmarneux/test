<?php
/**
 * calendar.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputEnum;

/**
 * Class Calendar
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Calendar extends InputEnum {
	/**
	 * Calendar constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'calendar', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setAllowedValues(Dictionary::getDict('specific.setting.calendar'));
	}
}
