<?php
/**
 * contactdetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Lib\Filters\Inputs\Attributes\Address;
use BoondManager\Lib\Filters\Inputs\Attributes\Country;
use BoondManager\Lib\Filters\Inputs\Attributes\Email;
use BoondManager\Lib\Filters\Inputs\Attributes\Phone;
use BoondManager\Lib\Filters\Inputs\Attributes\PostCode;
use BoondManager\Lib\Filters\Inputs\Attributes\Town;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;

/**
 * Class ContactDetails
 * @package BoondManager\Models\Filters\Profiles
 * @property \Wish\Filters\Inputs\InputInt id
 * @property \Wish\Filters\Inputs\InputString name
 * @property \Wish\Filters\Inputs\InputString contact
 * @property \Wish\Filters\Inputs\InputString phone1
 * @property \Wish\Filters\Inputs\InputString email1
 * @property \Wish\Filters\Inputs\InputString email2
 * @property \Wish\Filters\Inputs\InputString email3
 * @property \Wish\Filters\Inputs\InputString address1
 * @property \Wish\Filters\Inputs\InputString address2
 * @property \Wish\Filters\Inputs\InputString address3
 * @property InputString postcode
 * @property \Wish\Filters\Inputs\InputString town
 * @property \Wish\Filters\Inputs\InputString country
 * @property \Wish\Filters\Inputs\InputBoolean state
 */
class ContactDetails extends AbstractFilters{

	protected $_objectClass = \BoondManager\Models\ContactDetails::class;

	public function __construct(){
		parent::__construct();

		$id = new InputInt('id');
		$this->addInput($id);

		$name = new InputString('name');
		$name->setMaxLength(100);
		$this->addInput($name);

		$input = new InputString('contact');
		$input->setMaxLength(200);
		$this->addInput($input);

		$this->addInput(new Phone('phone1'));
		$this->addInput(new Email('email1'));
		$this->addInput(new Email('email2'));
		$this->addInput(new Email('email3'));
		$this->addInput(new Address('address1'));
		$this->addInput(new Address('address2'));
		$this->addInput(new Address('address3'));
		$this->addInput(new PostCode());
		$this->addInput(new Town());
		$this->addInput(new Country());

		$input = new InputBoolean('state');
		$this->addInput($input);
	}
}
