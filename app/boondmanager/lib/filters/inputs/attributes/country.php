<?php
/**
 * country.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputDict;
use BoondManager\Services\Dictionary;

/**
 * Class Country
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Country extends InputDict {
	/**
	* Country constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'country', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, 'specific.country', $defaultValue, $required, $mode);
	}
}
