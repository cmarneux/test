<?php
/**
 * currency.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputDict;
use BoondManager\Services\Dictionary;

/**
 * Class Currency
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Currency extends InputDict {
	/**
	 * Currency constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param bool|int $mode
	 */
	public function __construct($name = 'currency', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, 'specific.setting.currency', $defaultValue, $required, $mode);
	}
}
