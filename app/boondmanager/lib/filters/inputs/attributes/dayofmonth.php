<?php
/**
 * dayofmonth.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputEnum;

/**
 * Class DayOfMonth
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class DayOfMonth extends InputEnum {
	const ALLOWED_VALUES = [
		'lastDayOfMonth',
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28
	];

	/**
	 * DayOfMonth constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'dayOfMonth', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

        $this->setStrict(true);
		$this->setAllowedValues(self::ALLOWED_VALUES);
	}
}
