<?php
/**
 * delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;

/**
 * Class Delivery
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 * @property InputInt id
 */
class Delivery extends AbstractFilters{

	protected $_objectClass = \BoondManager\Models\Delivery::class;

	/**
	 * Delivery constructor.
	 * @param $name
	 */
	public function __construct($name = 'delivery'){
		parent::__construct($name);

		$id = new InputInt('id');
		$id->setMin(1);
		$this->addInput($id);
	}
}
