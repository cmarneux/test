<?php
/**
 * saveadministrative.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use BoondManager\Models;


/**
 * Class DesiredSalary
 * @package BoondManager\Models\Filters\Profiles\Candidates
 * @property InputFloat min
 * @property InputFloat max
 */
class DesiredSalary extends AbstractFilters{

	protected $_objectClass = Models\DesiredSalary::class;

	public function __construct()
	{
		parent::__construct();
		$this->setName('desiredSalary');

		$min = new InputFloat('min');
		$max = new InputFloat('max');

		$this->addInput([$min, $max]);
	}
}
