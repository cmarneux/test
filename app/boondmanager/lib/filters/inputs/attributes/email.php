<?php
/**
 * email.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputString;

/**
 * Class Email
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Email extends InputString {
	/**
	 * Email constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'email', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setMaxLength(100);
		$this->addFilter(FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE);
	}
}
