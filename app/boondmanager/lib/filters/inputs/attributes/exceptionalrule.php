<?php
/**
 * exceptionalrule.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Tools;

/**
 * Class ExceptionalRule
 * @property InputInt reference
 * @property InputFloat priceExcludingTaxOrPriceRate
 * @property InputFloat grossCostOrSalaryRate
 * @package BoondManager\Lib\Inputs\Filters\Attributes
 */
class ExceptionalRule extends AbstractFilters {

	protected $_objectClass = \BoondManager\Models\ExceptionalRule::class;
	/**
	 * ExceptionalRule constructor.
	 * @param \BoondManager\Models\ExceptionalRule[] $rules
	 */
	public function __construct($rules = []) {
		parent::__construct();

		$input = new InputEnum('reference');
		$input->setRequired(true);
		$input->setAllowedValues(Tools::getFieldsToArray($rules, 'reference'));
		$this->addInput($input);

		$input = new InputFloat('priceExcludingTaxOrPriceRate');
		$input->setRequired(true)->setAllowEmptyValue(true);
		$this->addInput( $input );

		$input = new InputFloat('grossCostOrSalaryRate');
		$input->setRequired(true)->setAllowEmptyValue(true);
		$this->addInput( $input );
	}
}
