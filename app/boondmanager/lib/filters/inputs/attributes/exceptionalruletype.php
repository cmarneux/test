<?php
/**
 * exceptionalruletype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;

/**
 * Class ExceptionalRuleType
 * @property InputId id
 * @property InputReference reference
 * @property InputString name
 * @property InputFloat priceExcludingTaxOrPriceRate
 * @property InputFloat grossCostOrSalaryRate
 * @property InputBoolean state
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class ExceptionalRuleType extends AbstractFilters {
	protected $_objectClass = Models\ExceptionalRuleType::class;

	/**
	 * WorkUnitType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'exceptionalRuleType') {
		parent::__construct();

		$this->setName($name);

		$input = new InputId();
		$this->addInput($input);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputString('name');
		$input->setMaxLength(250);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('priceExcludingTaxOrPriceRate');
		$input->setRequired(true);
		$this->addInput( $input );

		$input = new InputFloat('grossCostOrSalaryRate');
		$input->setRequired(true);
		$this->addInput( $input );

		$input = new InputBoolean('state');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $exceptionalRuleTypes
	 * @return $this
	 */
	public function setAllowedExceptionalRuleTypes($exceptionalRuleTypes) {
		$this->id->addFilterIdExists($exceptionalRuleTypes);
		return $this;
	}
}
