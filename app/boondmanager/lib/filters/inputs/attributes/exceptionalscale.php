<?php
/**
 * exceptionalscale.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use BoondManager\Services\Agencies;
use BoondManager\Services\BM;
use BoondManager\Services\Companies;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputRelationship;


/**
 * Class ExceptionalScale
 * @property InputInt reference
 * @property InputMultiObjects exceptionalRules
 * @property InputRelationship dependsOn
 * @package BoondManager\Lib\Inputs\Filters\Attributes
 */
class ExceptionalScale extends AbstractFilters {

	protected $_objectClass = Models\ExceptionalScales::class;
	/**
	 * ExceptionalScale constructor.
	 * @param Models\ExceptionalRule[] $exceptionalRules
	 */
	public function __construct($exceptionalRules = []) {
		parent::__construct();

		$input = new InputInt('reference');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputMultiObjects('exceptionalRules', new ExceptionalRule($exceptionalRules));
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputRelationship('dependsOn');
		$input->setRequired(true);
		$input->addFilterCallback(function($value) {
			/** @var InputRelationship $this */
			switch ($this->getType()) {
				case 'company' : return Companies::find($value);
				case 'agency' : return Agencies::find($value);
			}
			return false;
		},  BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
