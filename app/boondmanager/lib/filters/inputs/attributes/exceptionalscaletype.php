<?php
/**
 * exceptionalscaletype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;
use Wish\Tools;

/**
 * Class ExceptionalScaleType
 * @property InputReference reference
 * @property InputString name
 * @property InputMultiObjects workUnitTypes
 * @property InputMultiObjects exceptionalRuleTypes
 * @property InputBoolean default
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class ExceptionalScaleType extends AbstractFilters {
	protected $_objectClass = Models\ExceptionalScaleType::class;

	/**
	 * WorkUnitType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'exceptionalScaleType') {
		parent::__construct();

		$this->setName($name);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputString('name');
		$input->setMaxLength(250);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputMultiObjects('workUnitTypes', new WorkUnitTypeOnlyReference());
		$input->setRequired(true);
		$input->setAllowEmptyValue(true);
		$this->addInput($input);

		$input = new InputMultiObjects('exceptionalRuleTypes', new ExceptionalRuleType());
		$input->setRequired(true);
		$input->setAllowEmptyValue(true);
		$this->addInput($input);

		$input = new InputBoolean('default');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $exceptionalScaleTypes
	 * @return $this
	 */
	public function setAllowedExceptionalScaleTypes($exceptionalScaleTypes) {
		$this->reference->addFilterReferenceExists($exceptionalScaleTypes);
		return $this;
	}

	/**
	 * @param array $workUnitTypes
	 * @return $this
	 */
	public function setAllowedWorkUnitTypes($workUnitTypes) {
		/**
		 * @var WorkUnitTypeOnlyReference $template
		 */
		//Necessary to update the Model because this attribute is an InputMultiObject
		$template = $this->workUnitTypes->getModel();
		$template->setAllowedWorkUnitTypes($workUnitTypes);
		$template->onlyExceptionalWorkUnit($workUnitTypes, Models\Agency::ERROR_AGENCY_WRONG_ACTIVITYTYPE_FOR_SCALES);

		/**
		 * @var WorkUnitTypeOnlyReference $wut
		 */
		foreach($this->workUnitTypes->getItems() as $wut) {
			$wut->setAllowedWorkUnitTypes($workUnitTypes);
			$wut->onlyExceptionalWorkUnit($workUnitTypes, Models\Agency::ERROR_AGENCY_WRONG_ACTIVITYTYPE_FOR_SCALES);
		}

		$this->workUnitTypes->addGroupFilterCallback(function($array) use($workUnitTypes) {
			var_dump('test');
			$allowedWorkUnitTypes = Tools::useColumnAsKey('reference', $workUnitTypes);
			$newArray = [];
			foreach($array as $entry) {
				/**
				 * @var Models\WorkUnitType $wut
				 */
				if(array_key_exists($entry->reference->getValue(), $allowedWorkUnitTypes)) {
					$wut = $allowedWorkUnitTypes[$entry->reference->getValue()];
					$key = $wut->activityType;
					$newArray[$key] = $key;
				}
			}
			var_dump($newArray);
			return sizeof($newArray) == 1 ? $array : false;
		}, Models\Agency::ERROR_AGENCY_DIFFERENT_ACTIVITYTYPE_FOR_SCALE);

		$this->workUnitTypes->addGroupFilterOnAttribute('reference');

		return $this;
	}

	/**
	 * @param array $exceptionalRuleTypes
	 * @return $this
	 */
	public function setAllowedExceptionalRuleTypes($exceptionalRuleTypes) {
		/**
		 * @var ExceptionalRuleType $template
		 */
		//Necessary to update the Model because this attribute is an InputMultiObject
		$template = $this->exceptionalRuleTypes->getModel();
		$template->setAllowedExceptionalRuleTypes($exceptionalRuleTypes);

		/**
		 * @var ExceptionalRuleType $ert
		 */
		foreach($this->exceptionalRuleTypes->getItems() as $ert)
			$ert->setAllowedExceptionalRuleTypes($exceptionalRuleTypes);

		$this->exceptionalRuleTypes->addGroupFilterOnAttribute('id');
		$this->exceptionalRuleTypes->addGroupFilterOnAttribute('reference');
		return $this;
	}
}
