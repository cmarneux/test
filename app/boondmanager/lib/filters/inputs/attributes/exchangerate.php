<?php
/**
 * exchangerate.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputFloat;

/**
 * Class ExchangeRate
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class ExchangeRate extends InputFloat {
	/**
	* ExchangeRate constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'exchangeRate', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setMin(0);
	}
}
