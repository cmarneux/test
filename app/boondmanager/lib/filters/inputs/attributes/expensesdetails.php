<?php
/**
 * additionalturnoverandcosts.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models\Agency;
use BoondManager\Models\ExpenseDetail;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationId;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;

/**
 * Class ExpensesDetails
 * @property InputId id
 * @property ExpenseTypeOnlyReference expenseType
 * @property InputValue periodicity
 * @property InputFloat netAmount
 * @property InputRelationId agency
 * @package BoondManager\Lib\Inputs\Filters\Attributes
 */
class ExpensesDetails extends AbstractFilters {

	protected $_objectClass = ExpenseDetail::class;

	/**
	 * ExpensesDetails constructor.
	 */
	public function __construct() {
		parent::__construct();

		$this->addInput( new InputId() );

		$input = new ExpenseTypeOnlyReference();
		$input->setRequired(true);
		$this->addInput( $input );

		$input = new InputEnum('periodicity');
		$input->setAllowedValues([ExpenseDetail::PERIODICITY_DAILY, ExpenseDetail::PERIODICITY_MONTHLY]);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('netAmount');
		$input->setRequired(true);
		$this->addInput( $input );

		// ?
		$input = new InputRelationId('agency');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param ExpenseDetail[] $expensesDetails
	 * @return $this
	 */
	public function setAllowedDetails($expensesDetails) {
		$this->id->addFilterIdExists($expensesDetails);
		return $this;
	}

	/**
	 * @param Agency[] $agencies
	 * @return $this
	 */
	public function setAllowedAgencies($agencies){
		$this->agency->addFilterInIds(Tools::useColumnAsKey('id', $agencies));
		return $this;
	}
}
