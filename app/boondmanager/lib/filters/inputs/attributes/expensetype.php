<?php
/**
 * expensetype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;

/**
 * Class ExpenseType
 * @property InputId id
 * @property InputReference reference
 * @property InputString name
 * @property InputBoolean state
 * @property InputFloat taxRate
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class ExpenseType extends AbstractFilters {
    protected $_objectClass = Models\ExpenseType::class;

	/**
	 * ExpenseType constructor.
	 * @param string $name
	 */
    public function __construct( $name = 'expenseType') {
        parent::__construct($name);

        $input = new InputId();
        $this->addInput($input);

        $input = new InputReference();
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputString('name');
        $input->setMaxLength(250);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputBoolean('state');
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputFloat('taxRate');
        $input->setRequired(true);
        $this->addInput($input);
    }

    /**
     * @param array $expenseTypes
     * @return $this
     */
    public function setAllowedExpenseTypes($expenseTypes) {
        $this->id->addFilterIdExists($expenseTypes);
        return $this;
    }
}
