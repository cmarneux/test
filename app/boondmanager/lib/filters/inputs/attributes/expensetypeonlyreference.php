<?php
/**
 * expensetypeonlyreference.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputReference;

/**
 * Class ExpenseTypeOnlyReference
 * @property InputReference reference
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class ExpenseTypeOnlyReference extends AbstractFilters{
	protected $_objectClass = Models\ExpenseType::class;

	/**
	 * ExpenseTypeOnlyReference constructor.
	 * @param $name
	 */
	public function __construct($name = 'expenseType'){
		parent::__construct($name);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $expenseTypes
	 * @return $this
	 */
	public function setAllowedExpenseTypes($expenseTypes) {
		$this->reference->addFilterReferenceExists($expenseTypes);
		return $this;
	}

	public function getExpenseType() {
		return $this->reference->getObject();
	}

	public function toObject()
	{
		return $this->getExpenseType();
	}
}
