<?php
/**
 * groupmentdelivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models\Delivery;
use BoondManager\Models;
use BoondManager\Services\DeliveriesInactivitiesGroupments;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationId;

/**
 * Class Deliveries
 * @property InputRelationId delivery
 * @property InputFloat weighting
 * @property InputFloat schedule
 * @package BoondManager\Lib\Inputs\Filters\Attributes
 */
class GroupmentDelivery extends AbstractFilters {

	protected $_objectClass = Models\GroupmentDelivery::class;

	/**
	 * Deliveries constructor.
	 */
	public function __construct() {
		parent::__construct();

		$input = new InputRelationId('delivery');
		$input->addFilterCallback(function($id){
			$entity = DeliveriesInactivitiesGroupments::find($id);
			if($entity instanceof Delivery) return $entity;
			return false;
		});
		$this->addInput($input);

		$this->addInput( (new InputFloat('weighting')) );
		$this->addInput( (new InputFloat('schedule')) );
	}

	public function setProject(Models\Project $project){;
		$this->delivery->addFilterCallback(function($entity) use ($project){
			if($project->id == $entity->project->id) return $entity;
			return false;
		});
	}
}
