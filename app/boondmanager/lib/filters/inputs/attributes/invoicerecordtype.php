<?php
/**
 * invoicerecordtype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;
use Wish\Tools;

/**
 * Class InvoiceRecordType
 * @property InputId id
 * @property InputReference reference
 * @property InputString name
 * @property InputMultiObjects workUnitTypes
 * @property InputMultiObjects expenseTypes
 * @property InputBoolean schedule
 * @property InputBoolean reinvoicedPurchase
 * @property InputBoolean product
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class InvoiceRecordType extends AbstractFilters {
	protected $_objectClass = Models\InvoiceRecordType::class;

	/**
	 * WorkUnitType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'invoiceRecordType') {
		parent::__construct();

		$this->setName($name);

		$input = new InputId();
		$this->addInput($input);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputString('name');
		$input->setMaxLength(250);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputMultiObjects('workUnitTypes', new WorkUnitTypeOnlyReference());
		$input->setRequired(true);
		$input->setAllowEmptyValue(true);
		$this->addInput($input);

		$input = new InputMultiObjects('expenseTypes', new ExpenseTypeOnlyReference());
		$input->setRequired(true);
		$input->setAllowEmptyValue(true);
		$this->addInput($input);

		$input = new InputBoolean('schedule');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputBoolean('reinvoicedPurchase');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputBoolean('product');
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $invoiceRecordTypes
	 * @return $this
	 */
	public function setAllowedInvoiceRecordTypes($invoiceRecordTypes) {
		$this->id->addFilterIdExists($invoiceRecordTypes);
		return $this;
	}

	/**
	 * @param array $workUnitTypes
	 * @return $this
	 */
	public function setAllowedWorkUnitTypes($workUnitTypes) {
		/**
		 * @var WorkUnitTypeOnlyReference $template
		 */
		//Necessary to update the Model because this attribute is an InputMultiObject
		$template = $this->workUnitTypes->getModel();
		$template->setAllowedWorkUnitTypes($workUnitTypes);

		$this->workUnitTypes->addGroupFilterOnAttribute('reference');
		return $this;
	}

	/**
	 * @param array $expenseTypes
	 * @return $this
	 */
	public function setAllowedExpenseTypes($expenseTypes) {
		/**
		 * @var ExpenseTypeOnlyReference $template
		 */
		//Necessary to update the Model because this attribute is an InputMultiObject
		$template = $this->expenseTypes->getModel();
		$template->setAllowedExpenseTypes($expenseTypes);

		$this->expenseTypes->addGroupFilterOnAttribute('reference');
		return $this;
	}
}
