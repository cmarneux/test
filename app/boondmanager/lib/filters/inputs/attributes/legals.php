<?php
/**
 * legals.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputString;

/**
 * Class Legals
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Legals extends InputString {
    /**
     * Legals constructor.
     * @param string $name
     * @param null $defaultValue if `null`, the default value is the current date
     * @param bool $required
     * @param int $mode
     */
    public function __construct($name = 'legals', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
        parent::__construct($name, $defaultValue, $required, $mode);

        $this->setMaxLength(5000);
    }
}
