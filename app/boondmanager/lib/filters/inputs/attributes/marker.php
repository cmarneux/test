<?php
/**
 * marker.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputId;
use Wish\Tools;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputRelationId;

/**
 * Class Marker
 * @property InputId id
 * @property InputString title
 * @property InputFloat durationForecast
 * @property InputFloat remainsToBeDone
 * @property InputFloat progressRate
 * @property InputRelationId resource
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Marker extends AbstractFilters {
	/**
	 * Marker constructor.
	 * @param array $markers
	 * @param array $resources
	 */
	public function __construct($markers = [], $resources = []) {
		parent::__construct();

		$input = new InputId();
		$input->addFilterIdExists($markers);
		$this->addInput($input);

		$input = new InputDate('date');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputString('title');
		$input->setMaxLength(100);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('durationForecast');
		$input->setMin(0);
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('remainsToBeDone');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputRelationId('resource');
		$input->addFilterInIds(Tools::useColumnAsKey('id', $resources));
		$this->addInput($input);
	}
}
