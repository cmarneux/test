<?php
/**
 * project.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Tools;

/**
 * Class Project
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 * @property InputInt id
 */
class Project extends AbstractFilters{

	protected $_objectClass = \BoondManager\Models\Project::class;

	/**
	 * Project constructor.
	 * @param $name
	 */
	public function __construct($name = 'project'){
		parent::__construct($name);

		$id = new InputInt('id');
		$id->setMin(1);
		$this->addInput($id);
	}
}
