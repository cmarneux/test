<?php
/**
 * rateperkilometertype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;

/**
 * Class RatePerKilometerType
 * @property InputId id
 * @property InputReference reference
 * @property InputString name
 * @property InputBoolean state
 * @property InputFloat amount
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class RatePerKilometerType extends AbstractFilters {
    protected $_objectClass = Models\RatePerKilometerType::class;

    /**
     * RatePerKilometerType constructor.
     */
    public function __construct() {
        parent::__construct();

        $input = new InputId();
        $this->addInput($input);

        $input = new InputReference();
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputString('name');
        $input->setMaxLength(250);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputBoolean('state');
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputFloat('amount');
        $input->setRequired(true);
        $this->addInput($input);
    }

    /**
     * @param array $ratePerKilometerTypes
     * @return $this
     */
    public function setAllowedRatePerKilometerTypes($ratePerKilometerTypes) {
        $this->id->addFilterIdExists($ratePerKilometerTypes);
        return $this;
    }
}
