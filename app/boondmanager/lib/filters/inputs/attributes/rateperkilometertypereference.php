<?php
/**
 * rateperkilometertypereference.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;

/**
 * Class RatePerKilometerTypeReference
 * @property InputId id
 * @property InputReference reference
 * @property InputString name
 * @property InputBoolean state
 * @property InputFloat amount
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class RatePerKilometerTypeReference extends AbstractFilters {

	protected $_objectClass = Models\RatePerKilometerType::class;

	/**
	 * RatePerKilometerType constructor.
	 * @param string $name
	 */
	public function __construct($name = 'ratePerKilometerType') {
		parent::__construct($name);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputFloat('amount');
		$input->setRequired(true);
		$this->addInput($input);
	}
}
