<?php
/**
 * referencemask.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputString;

/**
 * Class ReferenceMask
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class ReferenceMask extends InputString {
    /**
     * ReferenceMask constructor.
     * @param string $name
     * @param null $defaultValue if `null`, the default value is the current date
     * @param bool $required
     * @param int $mode
     */
    public function __construct($name = 'referenceMask', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
        parent::__construct($name, $defaultValue, $required, $mode);
    }

    public function setFilterInvoiceOrQuotationReferenceMask() {
        $this->setMaxLength(200);
        $this->addFilterCallback(function($value){
            return preg_match('/([a-zA-Z0-9\[\]\{\}_\-\/]{1,})/', $value, $match) ? $match[1] : false;
        });
        return $this;
    }

    public function setFilterDefaultReferenceMask() {
        $this->setMaxLength(50);
        $this->addFilterCallback(function($value){
            return preg_match('/([a-zA-Z0-9\[\]_\-\/]{0,})/', $value, $match) ? $match[1] : false;
        });
        return $this;
    }
}
