<?php
/**
 * source.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models\OriginOrSource;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Source
 * @package BoondManager\Models\Filters\Profiles
 * @property InputValue typeOf
 * @property \Wish\Filters\Inputs\InputString detail
 */
class SourceOrOrigin extends AbstractFilters{

	protected $_objectClass = OriginOrSource::class;

	public function __construct($name){
		parent::__construct();

		$this->setName($name);

		$id = new InputDict('typeOf', 'specific.setting.source');
		$id->addAllowedValue(-1);
		$this->addInput($id);

		$name = new InputString('detail');
		$name->setMaxLength(100);
		$this->addInput($name);
	}
}
