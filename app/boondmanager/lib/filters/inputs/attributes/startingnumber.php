<?php
/**
 * startingnumber.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use Wish\Filters\Inputs\InputString;

/**
 * Class StartingNumber
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class StartingNumber extends InputString {
    /**
     * StartingNumber constructor.
     * @param string $name
     * @param null $defaultValue if `null`, the default value is the current date
     * @param bool $required
     * @param int $mode
     */
    public function __construct($name = 'startingNumber', $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
        parent::__construct($name, $defaultValue, $required, $mode);

        $this->setMaxLength(50);
        $this->addFilterCallback(function($value){
            return preg_match('/([0-9]*)/', $value, $match) ? $match[1] : false;
        });
    }
}
