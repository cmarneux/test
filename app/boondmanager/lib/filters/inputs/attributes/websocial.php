<?php
/**
 * websocial.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

/**
 * Class WebSocial
 * @package BoondManager\Lib\Filters\Inputs
 * @property InputValue network
 * @property InputString url
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class WebSocial extends AbstractFilters{

	protected $_objectClass = Models\WebSocial::class;

	public function __construct(){
		parent::__construct();

		$network = new InputEnum('network');
		$network->setAllowedValues(Models\WebSocial::NETWORK_MAPPER);

		$url = new InputString('url');
		$url->addFilter(FILTER_VALIDATE_URL);

		$this->addInput([$network, $url]);
	}
}
