<?php
/**
 * workflow.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Services\BM;
use BoondManager\Services\Managers;
use BoondManager\Models;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Tools;

/**
 * Class Workflow
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class Workflow extends InputMultiValues {
    public function __construct($name = 'workflow', array $defaultValue = [], $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE) {
        parent::__construct($name, $defaultValue, $required, $mode);

        $this->addFilterCallback(function($value) {
            if(is_string($value) && in_array($value, Models\Workflow::WORKFLOW_VALIDATOR, true)) {
                return $value;
            } else if(is_array($value) && isset($value['id']) && isset($value['type']) && $value['type'] == 'resource') {
                $managers = Tools::useColumnAsKey('id', Managers::getAllGroupManagers(false));
                if(is_numeric($value['id']) && array_key_exists($value['id'], $managers)) {
                    $instance = new Models\Account();
                    $instance->fromArray($managers[$value['id']]);
                    return $instance;
                }
            }
            return false;
        }, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
    }

    /**
     * sanitize the multi input to keep only one item of each
     * @return $this
     */
    public function addUnicityFilter() {
        $this->addGroupFilterCallback(function($array){
            $newArray = [];
            foreach($array as $entry) {
                $isExist = false;
                foreach($newArray as $j => $newEntry) {
                    if(is_string($entry) && is_string($newEntry) && $entry === $newEntry ||
                        $entry instanceof Models\Account && $newEntry instanceof Models\Account && $entry->id == $newEntry->id) {
                        $isExist = true;
                        break;
                    }
                }

                if($isExist)
                    return false;
                else $newArray[] = $entry;
            }
            return $array;
        }, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
        return $this;
    }
}
