<?php
/**
 * workunittype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;

use BoondManager\Models;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputId;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputReference;
use Wish\Filters\Inputs\InputString;

/**
 * Class WorkUnitType
 * @property InputId id
 * @property InputInt reference
 * @property InputString name
 * @property InputEnum activityType
 * @property InputBoolean state
 * @property InputBoolean warning
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class WorkUnitType extends AbstractFilters {
    protected $_objectClass = Models\WorkUnitType::class;

    /**
     * WorkUnitType constructor.
     */
    public function __construct() {
        parent::__construct();

        $input = new InputId();
        $this->addInput($input);

        $input = new InputReference();
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputString('name');
        $input->setMaxLength(250);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputEnum('activityType');
        $input->setAllowedValues(Models\WorkUnitType::ACTIVITY_MAPPER);
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputBoolean('state');
        $input->setRequired(true);
        $this->addInput($input);

        $input = new InputBoolean('warning');
        $this->addInput($input);
    }

    /**
     * @param array $workUnitTypes
     * @return $this
     */
    public function setAllowedWorkUnitTypes($workUnitTypes) {
        $this->id->addFilterIdExists($workUnitTypes);
        return $this;
    }

	protected function postValidation() {
		if($this->activityType != Models\WorkUnitType::ACTIVITY_ABSENCE && $this->warning->isDefined())
			$this->warning->invalidateIfDebug(Models\Agency::ERROR_AGENCY_WARNING_CANNOT_BE_SET);
	}
}
