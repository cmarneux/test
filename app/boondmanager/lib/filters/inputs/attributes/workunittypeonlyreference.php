<?php
/**
 * workunittypeonlyreference.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Attributes;


use BoondManager\Models;
use BoondManager\Services\BM;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputReference;
use Wish\Tools;

/**
 * Class WorkUnitTypeOnlyReference
 * @property InputReference reference
 * @package BoondManager\Lib\Filters\Inputs\Attributes
 */
class WorkUnitTypeOnlyReference extends AbstractFilters{

	protected $_objectClass = Models\WorkUnitType::class;

	private $allowedWorkUnit;

	/**
	 * WorkUnitTypeOnlyReference constructor.
	 * @param $name
	 */
	public function __construct($name = 'workUnitType'){
		parent::__construct($name);

		$input = new InputReference();
		$input->setRequired(true);
		$this->addInput($input);
	}

	/**
	 * @param array $workUnitTypes
	 * @return $this
	 */
	public function setAllowedWorkUnitTypes($workUnitTypes) {
		$this->allowedWorkUnit = $workUnitTypes;
		$this->reference->addFilterReferenceExists($workUnitTypes);
		return $this;
	}

	/**
	 * @param array $workUnitTypes
	 * @param int|string $errorMsg
	 * @return $this
	 */
	public function onlyExceptionalWorkUnit($workUnitTypes, $errorMsg = BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST) {
		$this->allowedWorkUnit = $workUnitTypes;
		$this->reference->addFilterCallback(function($value) use($workUnitTypes) {
			$allowedWorkUnitTypes = Tools::useColumnAsKey('reference', $workUnitTypes);
			/**
			 * @var Models\WorkUnitType $wut
			 */
			if(array_key_exists($value, $allowedWorkUnitTypes)) {
				$wut = $allowedWorkUnitTypes[$value];
				return in_array($wut->activityType, [Models\WorkUnitType::ACTIVITY_EXCEPTIONALCALENDAR, Models\WorkUnitType::ACTIVITY_EXCEPTIONALTIME]) ? $value : false;
			}
			return false;
		}, $errorMsg);
		return $this;
	}

	/**
	 * @return Models\WorkUnitType
	 */
	public function getWorkUnitType() {
		$allowedWorkUnitTypes = Tools::useColumnAsKey('reference', $this->allowedWorkUnit);
		return $allowedWorkUnitTypes[ $this->reference->getValue() ];
	}
}
