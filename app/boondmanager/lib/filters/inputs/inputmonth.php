<?php
/**
 * inputmonth.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs;

use Wish\Filters\Inputs\InputValue;

class InputMonth extends InputValue{

	const ERROR_INCORRECT_MONTH_FORMAT = 'month format is incorrect';

	public function __construct($name, $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE)
	{
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->addFilterCallback(function($value){
			if(preg_match('/^([0-9]{4})-([0-9]{1,2})/', $value, $matches))
				return date('Y-m-d', mktime(0,0,0,$matches[2], 1, $matches[1]));
			else
				return false;
		}, self::ERROR_INCORRECT_MONTH_FORMAT);
	}
}
