<?php
/**
 * bankdetails.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use Wish\Filters\Inputs\InputRelationship;
use BoondManager\Services\BM;
use BoondManager\Models;
use Wish\Tools;

/**
 * Class BankDetails
 * @package BoondManager\Inputs
 */
class BankDetails extends InputRelationship {
    /**
     * BankDetails constructor.
     * @param string $name
     * @param null $defaultValue if `null`, the default value is the current date
     * @param bool $required
     * @param int $mode
     */
    public function __construct($name = 'bankDetails', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
        parent::__construct($name, $defaultValue, $required, $mode);

        $this->setAllowEmptyValue(true);

        //FIXME : Doit être appelé en premier sinon cela plante
        $this->addFilterType('bankdetails');
    }

    /**
     * @param Models\BankDetails[] $banksDetails
     * @return $this
     */
    public function setAllowedBanksDetails(array $banksDetails) {
        $this->addFilterCallback(function($value) use($banksDetails) {
            $details = Tools::useColumnAsKey('id', $banksDetails);
            if(is_numeric($value) && array_key_exists($value, $details)) {
                $instance = new Models\BankDetails();
                $instance->fromArray($details[$value]);
                return $instance;
            } else return false;
        }, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
        return $this;
    }
}
