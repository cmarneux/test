<?php
/**
 * contact.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use Wish\Filters\Inputs\InputRelationship;
use BoondManager\Services\BM;
use BoondManager\Services\Contacts;

/**
 * Class Contact
 * @package BoondManager\Inputs
 */
class Contact extends InputRelationship {
	/**
	* Contact constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'contact', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setAllowEmptyValue(true);

		//FIXME : Doit être appelé en premier sinon cela plante
		$this->addFilterType('contact');

		$this->addFilter(FILTER_CALLBACK, function ($value) {
			return Contacts::find($value);
		}, null, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
