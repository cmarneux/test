<?php
/**
 * opportunity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use Wish\Filters\Inputs\InputRelationship;
use BoondManager\Services\BM;
use BoondManager\Services\Opportunities;
use Wish\MySQL\Where;

/**
 * Class Opportunity
 * @package BoondManager\Lib\Filters\Inputs\Relationships
 */
class ContactDetails extends InputRelationship {
	/**
	* Opportunity constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'contactDetails', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->addFilterType('detail');

		$this->addFilterCallback(function ($value) {
			if($value instanceof \BoondManager\Models\ContactDetails) return $value;
			else return \BoondManager\Services\ContactDetails::get($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
