<?php
/**
 * contract.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use BoondManager\Services\Contracts;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputRelationship;

/**
 * Class Contact
 * @package BoondManager\Inputs
 */
class Contract extends InputRelationship {
	/**
	 * Contact constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'contract', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setAllowEmptyValue(true);

		//FIXME : Doit être appelé en premier sinon cela plante
		$this->addFilterType('contract');

		$this->addFilterCallback(function ($value) {
			return Contracts::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}

