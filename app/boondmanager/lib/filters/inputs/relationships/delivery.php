<?php
/**
 * Delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use BoondManager\Services\BM;
use BoondManager\Services\Deliveries;
use BoondManager\Services\DeliveriesInactivitiesGroupments;
use Wish\Filters\Inputs\InputRelationship;

/**
 * Class Delivery
 * @package BoondManager\Inputs
 */
class Delivery extends InputRelationship {
	/**
	 * Delivery constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'delivery', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setAllowEmptyValue(true);

		//FIXME : Doit être appelé en premier sinon cela plante
		$this->addFilterType('delivery');

		$this->addFilterCallback(function ($value) {
			$entity = DeliveriesInactivitiesGroupments::find($value);
			if($entity instanceof \BoondManager\Models\Delivery) return $entity;
			else return false;
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}

