<?php
/**
 * employee.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use BoondManager\Services\BM;
use BoondManager\Services\Employees;
use Wish\Filters\Inputs\InputRelationship;

/**
 * Class Agency
 * @package BoondManager\Inputs
 */
class Employee extends InputRelationship {
	/**
	* Agency constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'resource', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->addFilterType('resource');

		$this->addFilterCallback(function ($value) {
			if($value instanceof \BoondManager\Models\Employee) return $value;
			else return Employees::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
