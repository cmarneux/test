<?php
/**
 * mainmanager.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use Wish\Filters\Inputs\InputRelationship;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Models;
use BoondManager\Services\CurrentUser;

/**
 * Class MainManager
 * @package BoondManager\Inputs\Relationships
 */
class HrManager extends InputRelationship {
	/**
	* MainManager constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'hrManager', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		//Doit être appelé en premier sinon cela plante
		$this->addFilterType('resource');

		$this->addFilterCallback(function($value) {
			$managers = Tools::useColumnAsKey('id', CurrentUser::instance()->getManagerForReassignment());
			if(is_numeric($value) && array_key_exists($value, $managers)) {
				$instance = new Models\Account();
				$instance->fromArray($managers[$value]);
				return $instance;
			} else return false;
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
