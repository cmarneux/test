<?php
/**
 * influencers.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters\Inputs\Relationships;

use BoondManager\Services\BM;
use BoondManager\Services\Managers;
use Wish\Filters\Inputs\InputMultiRelationships;
use Wish\Tools;

class Influencers extends InputMultiRelationships {

	public function __construct($name = 'influencers') {

		parent::__construct($name);

		// check type
		$this->addFilterCallback(function($value){
			if($this->getType() == 'resource' ) return $value;
			else return false;
		}, InputMultiRelationships::ERROR_INCORRECT_SCHEMA);

		// transform id into a manager
		$managers = Tools::useColumnAsKey('id', Managers::getAllGroupManagers($onlyActive = false));
		$this->addFilterCallback(function($value) use ($managers){
			if(array_key_exists($value, $managers)) {
				return clone $managers[$value];
			} else return false;
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);

		$this->setMode(self::MODE_ERROR_ON_INCORRECT_VALUE);

		// unicity
		$this->addGroupFilterCallback(function($array) {
			$newArray = [];
			foreach($array as $entry) {
				$key = $entry->id;
				if(isset($newArray[$key]))
					return false;
				else $newArray[$key] = $key;
			}
			return $array;
		}, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
	}
}
