<?php
/**
 * opportunity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use Wish\Filters\Inputs\InputRelationship;
use BoondManager\Services\BM;
use BoondManager\Services\Opportunities;

/**
 * Class Opportunity
 * @package BoondManager\Lib\Filters\Inputs\Relationships
 */
class Opportunity extends InputRelationship {
	/**
	* Opportunity constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'opportunity', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->addFilterType('opportunity');

		$this->addFilterCallback(function ($value) {
			if($value instanceof \BoondManager\Models\Opportunity) return $value;
			else return Opportunities::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
