<?php
/**
 * pole.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use Wish\Filters\Inputs\InputRelationship;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Models;
use BoondManager\Services\CurrentUser;

/**
 * Class Pole
 * @package BoondManager\Inputs
 */
class Pole extends InputRelationship {
	/**
	* Pole constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name = 'pole', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setAllowEmptyValue(true);

		//FIXME : Doit être appelé en premier sinon cela plante
		$this->addFilterType('pole');

		$this->addFilterCallback(function($value) {
			$poles = Tools::useColumnAsKey('id', CurrentUser::instance()->getPolesForReassignment());
			if(is_numeric($value) && array_key_exists($value, $poles)) {
				$instance = new Models\Pole();
				$instance->fromArray($poles[$value]);
				return $instance;
			} else return false;
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}
}
