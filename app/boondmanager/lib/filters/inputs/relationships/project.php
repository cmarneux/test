<?php
/**
 * Delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Filters\Inputs\Relationships;

use BoondManager\Services\BM;
use BoondManager\Services\Projects;
use Wish\Filters\Inputs\InputRelationship;

/**
 * Class Project
 * @package BoondManager\Inputs
 */
class Project extends InputRelationship {
	/**
	 * Project constructor.
	 * @param string $name
	 * @param null $defaultValue if `null`, the default value is the current date
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'project', $defaultValue = null, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setAllowEmptyValue(true);

		//FIXME : Doit être appelé en premier sinon cela plante
		$this->addFilterType('project');

		$this->addFilterCallback(function ($value) {
			return Projects::find($value);
		}, BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
	}

	/**
	 * @return \BoondManager\Models\Project|false
	 */
	public function getValue() {
		return parent::getValue();
	}
}

