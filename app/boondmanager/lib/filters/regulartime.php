<?php
/**
 * regulartime.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputInt;
use Wish\MySQL\Where;

/**
 * Class RegularTime
 * @property InputDB id
 * @property InputInt row
 */
class RegularTime extends Time {

	public function __construct($name = 'regularTime') {

		parent::__construct($name);

		$id = new InputDB('id');
		$id->addFilterExistsInDB('TAB_TEMPS', new Where('ID_TEMPS = ?'));
		$this->addInput($id);

		$this->addInput( new InputInt('row') );

		/*
		 *  fields required
		 */
		$this->startDate->setRequired(true);
		$this->duration->setRequired(true);
		$this->row->setRequired(true);
		$this->workUnitType->setRequired(true);
	}
}
