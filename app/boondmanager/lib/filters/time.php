<?php
/**
 * time.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Filters;

use BoondManager\Lib\Filters\Inputs\Attributes\Batch;
use BoondManager\Lib\Filters\Inputs\Attributes\Delivery;
use BoondManager\Lib\Filters\Inputs\Attributes\Project;
use BoondManager\Lib\Filters\Inputs\Attributes\WorkUnitTypeOnlyReference;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;
use BoondManager\Models;

/**
 * Class Time
 * @property InputDateTime startDate
 * @property InputDateTime endDate
 * @property InputFloat duration
 * @property InputBoolean recovering
 * @property InputString description
 * @property WorkUnitTypeOnlyReference workUnitType
 * @property Delivery delivery
 * @property Project project
 * @property Batch batch
 */
class Time extends AbstractFilters {

	protected $_objectClass = Models\Time::class;

	public function __construct($name) {

		parent::__construct();

		$this->setName($name);

		$this->addInput( new InputDate('startDate') );
		$this->addInput( new InputFloat('duration') );
		$this->addInput( new InputBoolean('recovering') );
		$this->addInput( new InputString('description') );
		$this->addInput( new WorkUnitTypeOnlyReference('workUnitType') );

		$this->addInput( new Delivery() );
		$this->addInput( new Batch() );
		$this->addInput( new Project() );
	}
}
