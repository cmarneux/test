<?php
/**
 * ged.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Lib;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use OpenXMLDocumentFactory;
use OpenXMLFatalException;
use Registry;
use Wish\FTP;
use BoondManager\Databases\Local\File;

/**
 * Librairie de gestion de la GED.
 * @namespace \BoondManager\Lib
 */
class GED {
	/**
	 * @var FTP
	 */
	private $pFTP;

	/**#@+
	 * @var string
	 **/
	const
		DEFAULT_GED = 'GEDA';
	/**#@-*/

	/**#@+
	 * @var string
	 **/
	const
		GED_DT = '/DT',
		GED_DOCUMENTS = '/Documents',
		GED_FILES = '/Fichiers',
		GED_DOWNLOADCENTER = '/DownloadCenter',
		GED_JUSTIFICATIVES = '/Justificatifs',
		GED_APP_CORPORATE = '/CorporateDocs',
		GED_CANDIDATES = '/Candidats';
	/**#@-*/

	/**
	 * GED constructor.
	 */
	public function __construct()
	{
		$this->pFTP = new FTP();
	}

	/**
	 *	Return class instance
	 *	@return static
	 **/
	static function instance() {
		if (!Registry::exists($class=get_called_class())) {
			$ref=new \Reflectionclass($class);
			$args=func_get_args();
			Registry::set($class,
				$args?$ref->newinstanceargs($args):new $class);
		}
		return Registry::get($class);
	}

	/**
     * Déplace un fichier téléchargé.
     *
     * __Le fichier doit remplir certains critères de types d'extension et de taille !__
     * @param  string  $idhtml      Nom du paramètre de la variable `$_FILES`.
     * @param  string  $target      Chemin où déplacer le fichier téléchargé.
     * @param  array   $arrayExt    Tableau des extensions autorisées.
     * @param  integer $maxfilesize Taille maximale autorisé en octect du fichier téléchargé.
     * @return boolean `true` si succès, `false` sinon.
     */
    public function getUploadedFile($idhtml, $target, $arrayExt = array(), $maxfilesize = 8192000) {
        $result = false;
        if(isset($_FILES[$idhtml]) && $_FILES[$idhtml]['size'] > 0 && $_FILES[$idhtml]['size'] <= $maxfilesize && $_FILES[$idhtml]['error'] == 0 && (sizeof($arrayExt) == 0 || in_array($this->getUploadedFileExtension($idhtml), $arrayExt)))
            $result = move_uploaded_file($_FILES[$idhtml]['tmp_name'], $target);
        return $result;
    }

    /**
     * Retourne l'extension du fichier téléchargé.
     * @param string  $idhtml Nom du paramètre de la variable `$_FILES`.
     * @see \BoondManager\Lib\GED::getFileExtension() getFileExtension
     * @return string Chaîne de caractère contenant les éléments.
    */
    public function getUploadedFileExtension($idhtml) {return $this->getFileExtension($_FILES[$idhtml]['name']);}

    /**
     * Récupère le contenu d'un fichier.
     * @param  string $filename Chemin du fichier.
     * @return string Contenu du fichier.
     */
    public function getFileData($filename){
        $handle = fopen($filename,"r");
        $data = fread($handle, filesize($filename));
        fclose($handle);
        return $data;
    }

    /**
     * Retourne l'extension d'un fichier.
     * @param string $filename Chemin du fichier.
     * @link http://php.net/manual/fr/function.pathinfo.php.
     * @return string Chaîne de caractère contenant les éléments.
    */
    public function getFileExtension($filename){
        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    /**
     * Retourne le fichier qui existe pour un chemin de fichier sans extension possible et une liste d'extensions autorisées.
     * @param  string  $filewithoutextension Chemin du fichier sans extension à tester.
     * @param  array   $tabExtensions        Tableau des extensions autorisées.
     * @link http://php.net/manual/fr/function.pathinfo.php.
     * @return boolean|string `false` si aucun fichier n'existe, sinon retourne le nom complet du fichier (sans le chemin et avec l'extension).
     */
    public function isFileWithoutExtensionExist($filewithoutextension, $tabExtensions = array('')) {
        foreach($tabExtensions as $extension) if(file_exists($filewithoutextension.'.'.$extension)) return pathinfo($filewithoutextension.'.'.$extension, PATHINFO_BASENAME);
        return false;
    }

    /**
     * Récupère le type MIME d'un fichier.
     * @param  string $filename Chemin du fichier à téléchargé.
     * @link http://php.net/manual/fr/function.pathinfo.php.
     * @return string Type MIME
     */
    public function getFileMimeType($filename){
        switch(pathinfo($filename, PATHINFO_EXTENSION)) {
            case 'pdf':return "application/pdf";break;
            case 'doc':case 'docx':return "application/msword";break;
            case 'txt':case 'rtf':return 'text/plain';break;
            case 'gif':return "image/gif";break;
            case 'png':return "image/png";break;
            case 'jpg':case 'jpeg':return "image/jpeg";break;
            case 'csv':case 'xls':return "application/msexcel";break;
            default:return 'application/octet-stream';break;
        }
    }

    /**
     * Retourne les informations d'un fichier téléchargé s'il respecte les conditions requises.
     * @param  string  $idhtml      Nom du paramètre de la variable `$_FILES`.
     * @param  integer $maxfilesize Taille maximale autorisé en octect du fichier téléchargé.
     * @return array Tableau vide si les conditions ne sont pas respectés, sinon contient 2 éléments :
     * - `FILE_PATH` : Chemin complet du fichier
     * - `FILE_NAME` : Nom du fichier
     */
    public function updateFile($idhtml, $maxfilesize = 8192000) {
        //On test si le fichier respecte les conditions de téléchargement
        if($idhtml['size'] > 0 && $idhtml['size'] <= $maxfilesize && $idhtml['error'] == 0 )
            return array('FILE_PATH' => $idhtml['tmp_name'],'FILE_NAME' => $idhtml['name']);
        else
            return array();
    }

    /**
     * Retourne les informations d'un CV téléchargé s'il respecte les conditions requises.
     * @param  array  $file  array from `$_FILES['file']`.
     * @param  integer $maxfilesize Taille maximale autorisé en octect du fichier téléchargé.
     * @see \BoondManager\Lib\GED::filterDocumentFile() filterDocumentFile
     * @return array Tableau vide si les conditions ne sont pas respectés, sinon contient 3 éléments :
     * - `CV_PATH` : Chemin complet du CV
     * - `CV_NAME` : Nom du CV
     * - `CV_TEXT` : Contenu filtré, pour permettre l'indexation en base de données, du CV
     */
    public function updateCV(array $file, $maxfilesize = 8192000) {
        //On test si le fichier respecte les conditions de téléchargement
        if($file['size'] > 0 && $file['size'] <= $maxfilesize && $file['error'] == 0 )
            return $this->filterDocumentFile($file['tmp_name'], $file['name']);
        else
            return array();
    }

    /**
     * Filtre un document pour permettre son indexation en base de données.
     * @param  string $path     Chemin complet du document à filtrer.
     * @param  string $filename Nom du document à filtrer_, utilisé pour identifier son extension)_.
     * @return array Contenu filtré du document
     */
    public function filterDocumentFile($path, $filename) {
        switch($this->getFileExtension($filename)) {
            case 'pdf':$filtertext = utf8_encode($this->filterPDFFile($path));break;
            case 'doc':$filtertext = utf8_encode($this->filterMSWORDFile($path));break;
            case 'docx':$filtertext = utf8_encode($this->filterOpenXMLFile($path));break;
            default:$filtertext = utf8_encode(file_get_contents($path));break;
        }
        return array('CV_PATH' => $path, 'CV_NAME' => $filename, 'CV_TEXT' => $filtertext);
    }

    /**
     * Filtre un document PDF.
     * @param  string $filename Chemin complet du document à filtrer.
     * @return string Contenu filtré du document
     */
    public function filterPDFFile($filename) {
        //On appelle pdftotext qui créé un fichier .txt de notre pdf temporaire
        system(\Base::instance()->get('MAIN_ROOTPATH')."/cgi-bin/pdftotext ".escapeshellcmd($filename), $ret);
        switch($ret) {
            case 0:
                $outpath = preg_replace('/\.pdf$/', '', $filename).'.txt';
                $result_data = file_get_contents($outpath);//on récupère le contenu texte du PDF
                unlink($outpath);//On supprime le fichier .txt
                break;
            case 1:$result_data = "PDFTOTEXT: Could not find pdf file";break;
            case 127:$result_data = "PDFTOTEXT: Could not find tool";break;
            default:$result_data = "PDFTOTEXT: Error undefined=".$ret;break;
        }
        return preg_replace('/\s{2,}/', ' ', $result_data);
    }

    /**
     * Filtre un document Word.
     *
     * Uniquement pour les anciennes versions de Microsoft Word (< 2000)_, où l'extension est `.doc`_ !
     * @param  string $filename Chemin complet du document à filtrer.
     * @return string Contenu filtré du document
     */
    public function filterMSWORDFile($filename) {
        //On recopie le fichier en .doc, seul format que lit catdoc
        $target = $filename.'.doc';
        copy($filename, $target);

        //On appelle catdoc qui créé un fichier .txt de notre Word temporaire (On part d'un charset cp1252 vers US-ASCII, par défaut, si un charset est spécifié dans le document il sera privilégié)
        $outpath = preg_replace('/\.doc$/', '', $filename).'.txt';
        system(\Base::instance()->get('MAIN_ROOTPATH')."/cgi-bin/catdoc -scp1252 -dus-ascii -w ".escapeshellcmd($target).' > '.escapeshellcmd($outpath), $ret);
        //On supprime le fichier .doc
        unlink($target);

        if(file_exists($outpath) && $ret == 0) {
            $outtext = file_get_contents($outpath);//on récupère le contenu texte du PDF
            unlink($outpath);//On supprime le fichier .txt
        } else {
            //Si la conversion n'a pas réussie, on récupère tout le contenu du document Word sans le filtrer
            $data = $this->getFileData($filename);
            $lines = explode(chr(0x0D),$data);
            $outtext = "";
            foreach($lines as $thisline){if( strpos($thisline, chr(0x00)) == false && strlen($thisline) != 0 )$outtext .= $thisline." ";}
            if(file_exists($outpath)) unlink($outpath);
        }

        switch($ret) {
            case 0:break;
            case 1:$outtext = "CATDOC: Could not find charset or word file\n".$outtext;break;
            case 127:$outtext = "CATDOC: Could not find tool\n".$outtext;break;
            default:$outtext = "CATDOC: Error undefined=".$ret."\n".$outtext;break;
        }
        return preg_replace('/\s{2,}/', ' ', $outtext);
    }

    /**
     * Filtre un document Word.
     *
     * Uniquement pour les anciennes versions de Microsoft Word (> 2000)_, où l'extension est `.docx`_ !
     * @param  string $filename Chemin complet du document à filtrer.
     * @return string Contenu filtré du document
     */
    public function filterOpenXMLFile($filename) {
        try {
            $mydoc = OpenXMLDocumentFactory::openDocument($filename);
            $data = $mydoc->getHTMLPreview();
        }
        catch (OpenXMLFatalException $e) {
            return $data = 'OPENXML: Could not extract word data';
        }
        return preg_replace('/\s{2,}/', ' ', $data);
    }

    /**
     * Récupère un document local/téléchargé et le copie/déplace localement ou sur la GED.
     * @param  integer  $type   Type de catégorie du document.
     * @param  integer  $id     Identifiant du document.
     * @param  string  $filename Chemin du document à copier.
     * @param  boolean $copy  Indique si on copie le document ou si on déplace celui téléchargé.
     * @see \BoondManager\Lib\GED::getTargetPath() getTargetPath
     * @see \BoondManager\Lib\FTP::uploadFile() uploadFile
     * @return boolean `true` si succès, `false` sinon.
     */
    public function uploadFile($type, $id, $filename, $copy = false) {
        if($this->pFTP->openFTPSocket()) {
            $target = $this->getTargetPath($type, $id);
            $result = $this->pFTP->uploadFile($filename, $target, $copy);
			$this->pFTP->closeFTPSocket();
            return $result;
        }
        return false;
    }

	/**
	 * Supprime un document de la GED.
	 * @param int $type
	 * @param int $id
	 * @return bool
	 */
    public function deleteFile($type, $id) {
        if($this->pFTP->openFTPSocket()) {
            $target = $this->getTargetPath($type, $id);
            $result = $this->pFTP->deleteFile($target);
			$this->pFTP->closeFTPSocket();
            return $result;
        }
        return false;
    }

    /**
     * Récupère un document de la GED.
     * @param  integer  $type   Type de catégorie du document.
     * @param  integer  $id     Identifiant du document.
     * @see \BoondManager\Lib\GED::getTargetPath() getTargetPath
     * @see \BoondManager\Lib\FTP::getFile() getFile
     * @return array|false
     */
    public function getFile($type, $id) {
        if($this->pFTP->openFTPSocket()) {
            $target = $this->getTargetPath($type, $id);
            //On récupère le fichier
            //  Si il est en local, cela renvoit un tableau contenant le nom filename et la taille du fichier
            //  Si il est sur un serveur FTP, cela renvoit un tableau contenant le nom du fichier temporaire créé en local et la taille du fichier
            //  Si erreur alors tableau vide
            $file = $this->pFTP->getFile($target);
			$this->pFTP->closeFTPSocket();
            if(sizeof($file) == 0) return false;
            return $file;
        }
        return false;
    }

	/**
	* Construit le chemin d'un document de la GED.
	* @param  integer  $type   Type de catégorie du document :
	* - `0` : CV d'une ressource
	* - `1` : Document d'une fich achat/produit/CRM/commande/action/positionnement/prestation/
	* - `2` : Justificatif d'une feuille de temps, note de frais ou demande d'absences
	* - `3` : CV d'un candidat
	* - `4` : Pièce administrative d'une ressource ou d'un candidat
	* - `5` : Fichier du centre de téléchargement
	* @param  integer  $id     Identifiant du document.
	* @see \BoondManager\Lib\GED::getDirectoryPath() getDirectoryPath
	* @return string Chemin complet du document de la GED.
	*/
	private function getTargetPath($type, $id) {
		$dossier = $this->getDirectoryPath($type);
		switch($type) {
			case File::TYPE_CANDIDATE_RESUME:
			case File::TYPE_RESOURCE_RESUME :return $dossier.'cv'.$id;break;
			case File::TYPE_JUSTIFICATIVE: return $dossier.'justif'.$id;break;//Justificatif Temps, Frais & Absences
			case File::TYPE_DOCUMENT: return $dossier.'doc'.$id;break;//Document
			case File::TYPE_DOWNLOADCENTER: return $dossier.'dl'.$id;break;//Fichier du Download Center
			default:return $dossier.'file'.$id;break;//Pièce Administrative Ressource ou Candidat
		}
	}

	/**
	 * @return string
	 */
	public function getGEDDirectory()
	{
		return $this->pFTP->getRootPath() . (BM::isCustomerInterfaceActive() ? BM::getCustomerInterfaceGEDDirectory() : self::DEFAULT_GED) . '/' . BM::getCustomerCode();
	}

	/**
	 * Construit le chemin du répertoire d'un type de catégorie de la GED.
	 * @param  integer $type Type de catégorie du document :
	 * - `0` : CV d'une ressource
	 * - `1` : Document d'une fich achat/produit/CRM/commande/action/positionnement/prestation/
	 * - `2` : Justificatif d'une feuille de temps, note de frais ou demande d'absences
	 * - `3` : CV d'un candidat
	 * - `4` : Pièce administrative d'une ressource ou d'un candidat
	 * - `5` : Fichier du centre de téléchargement
	 * @return string Chemin complet du répertorie de la GED.
	 */
	public function getDirectoryPath($type)
	{
		$dossier = $this->getGEDDirectory();
		switch ($type) {
			case File::TYPE_CANDIDATE_RESUME:
				return $dossier . self::GED_CANDIDATES .'/';
				break;//CV Candidat
			case File::TYPE_RESOURCE_RESUME:
				return $dossier . self::GED_DT .'/';
				break;//CV Ressource
			case File::TYPE_JUSTIFICATIVE:
				return $dossier . self::GED_JUSTIFICATIVES .'/';
				break;//Justificatif Temps, Frais & Absences
			case File::TYPE_DOCUMENT:
				return $dossier . self::GED_DOCUMENTS .'/';
				break;//Document
			case File::TYPE_DOWNLOADCENTER:
				return $dossier . self::GED_DOWNLOADCENTER .'/';
				break;//Fichier du Download Center
			default:
				return $dossier . self::GED_FILES .'/';
				break;//Pièce Administrative Ressource ou Candidat
			case -1:
				return $dossier;
				break;
		}
	}

    /**
     * Récupère la taille d'un répertoire de la GED.
     * @param  integer  $type   Type de catégorie du document.
     * @param string $unite Unité souhaitée pour exprimer la taille du répertoire :
     * - `K` : Kilo-octet
     * - `M` : Méga-octet
     * - `G` : Giga-octet
     * @see \BoondManager\Lib\FTP::sizeDirectory() sizeDirectory
     * @return integer Taille du répertoire dans l'unité souhaitée arrondit à 2 chiffres après la virgule.
     */
    public function getDirectorySize($type, $unite = 'G') {
        $size = 0;
        if($this->pFTP->openFTPSocket()) {
            $size = $this->pFTP->sizeDirectory($this->getDirectoryPath($type), $unite);
			$this->pFTP->closeFTPSocket();
        }
        return $size;
    }

	/*public function getListJSONFilesGED(\BoondManager\OldModels\ObjectBDD $dataBDD, $idfile = '', $appviewer = '', $tabDocuments = array(), $callbackSession = false) {
        $response = array('result' => 0, 'language' => \Base::instance()->get('SESSION.language'));

		//On gère la visionneuse
		if($appviewer != '') {
			//On récupère l'URL de connexion à cette API
            $apiBDD = new \BoondManager\OldModels\ObjectBDD\Fiches\App(false);
			$etat = $apiBDD->getMainObject($appviewer);
            $apiBDD->dbClose();

			$tabFiles = array();
			foreach($tabDocuments as $document) {if($dataBDD->exists($document[0])) {foreach($dataBDD->get($document[0]) as $file) if($idfile == '' || ($idfile == $file[$document[1]])) $tabFiles[] = array('id' => $file[$document[1]], 'category' => $document[3]);}}

			$wishRest = new \Wish\Rest();
			$payload = array('id_user' => \Wish\Tools::instance()->objectID_BM_encode(array(\Base::instance()->get('SESSION.user.id'), \Base::instance()->get('SESSION.user.company.web'))),
							 'id_client' => \Wish\Tools::instance()->objectID_BM_encode(array(\Base::instance()->get('SESSION.user.company.web'))),
							 'language' => \Base::instance()->get('SESSION.language'),
							 'url_callback' => $callbackSession?\Base::instance()->get('BMDATA.URL_UI'):'',
							 'files' => $tabFiles);
			if($etat && sizeof($tabFiles) > 0 && $wishRest->isResponseOK($wishRest->setUrl($apiBDD['MAINAPI']['MAINAPI_URL'].'/url-documents')->get(array('signed_request' => \Wish\Tools::instance()->signedRequest_encode($payload, $apiBDD['MAINAPI']['MAINAPI_KEY']))),'files')) {
				$response['result'] = 1;
                $response['appviewerid'] = $appviewer;
                $response['appviewername'] = $apiBDD['MAINAPI']['MAINAPI_NOM'];
                $response['embedded'] = ($wishRest->getResponse('embedded') == false)?0:1;
				$tabURL = $wishRest->getResponse('files');
				foreach($tabURL as $url) {
                    foreach($tabDocuments as $document) {
                        if($dataBDD->exists($document[0]) && $url['category'] == $document[3]) {
                            foreach($dataBDD->get($document[0]) as $file)
                                if($url['id'] == $file[$document[1]])
                                    $response['file'][] = array('name' => $file[$document[2]],
                                                                'url' => \Base::instance()->get('BOONDMANAGER.APP_URL').'fichedocument?id='.$file[$document[1]],
                                                                'viewernormal' => $url['normal_url'],
                                                                'viewerembedded' => $url['embedded_url'],
                                                                'categorie' => $document[3]);
                        }
                    }
                }
			} else  $response['appviewer'] = $appviewer;
		} else {
			foreach($tabDocuments as $document)
                if($dataBDD->exists($document[0])) {
                    $response['result'] = 1;
                    break;
                }
			foreach($tabDocuments as $document) {
                if($dataBDD->exists($document[0])) {
                    foreach($dataBDD->get($document[0]) as $file)
                        $response['file'][] = array('name' => $file[$document[2]],
                                                    'url' => \Base::instance()->get('BOONDMANAGER.APP_URL').'/fichedocument?id='.$file[$document[1]]);
                }
            }
		}
        return $response; //Réponse JSON
	}*/

	/**
	 * Envoit un fichier vers un navigateur web.
	 * @param int $id
	 * @param string $filename
	 * @param int $type
	 * @return bool
	 */
	public function downloadFileGED($id, $filename = '', $type = File::TYPE_CANDIDATE_RESUME) {
		if(in_array($type , [File::TYPE_RESOURCE_RESUME, File::TYPE_CANDIDATE_RESUME]) && $this->getFileExtension($filename) == 'txt') {//CV Candidat ou Ressource
			$fileBDD = new File($this);
			if($fileBDD->getObject($type, $id)) {
				//On récupère le contenu du fichier TXT
				$file = array(tempnam(\Base::instance()->get('MAIN_ROOTPATH').'/tmp', 'TXT'), 0, true);
				file_put_contents($file[0], $fileBDD['OBJECT']['CV_TEXT']);
				$file[1] = filesize($file[0]);
			} else $file = false;
		} else $file = $this->getFile($type, $id);

		if($file) {
			header('Content-Description: File Transfer');
			header('Content-Type: '.$this->getFileMimeType($filename));
			header('Content-Disposition: attachment; filename="'.basename(htmlspecialchars($filename)).'"');
			header('Content-Transfer-Encoding: base64');
			header('Expires: 0');
			header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: '.$file[1]);
			readfile($file[0]);//On envoit le fichier
			if($file[2]) unlink($file[0]);//Le fichier provient d'un serveur FTP distant (Il a donc été téléchargé dans un dossier temporaire => on supprime donc ce fichie temporaire)
			return true;
		}
		return false;
	}

    /**
     * @param mixed $dataBDD
     * @param string $todo
     * @param string $index
     * @param array $tabDocument
     * @param string $idfile
     * @param string $typeFiche
     * @param string $idFiche
     * @param string $ongletFiche
     * @param $filetype
     * @param $quota
     * @param bool $manageMebuBar
     * @param string $appviewer
     * @param string $baction
     * @param string $htmlFile
     * @DEPRECATED
     */
	/*public function uploadFileGED(mixed $dataBDD, $todo = '', $index = '', $tabDocument = array(), $idfile = '', $typeFiche = '', $idFiche = '', $ongletFiche = '', $filetype = FILE_CVCANDIDAT, $quota = QUOTA_PIECEJOINTE, $manageMebuBar = false, $appviewer = '', $baction = '', $htmlFile = 'file') {
        die('do migration'); //TODO faire la migration (mais dans le service)
		if(sizeof($tabDocument) == 6) {
			//On configure la réponse XML
			Wish_Registry::getInstance()->set('wish_headers', array("Content-Type" => "text/xml")); //On force le viewer à indiquer qu'il s'agit d'une réponse XML
			Wish_Registry::getInstance()->set('wish_layouttype', 'xml'); //On indique au viewer que le document est de l'XML

			$response = '';
			$result = false;
			switch($todo) {
				case 'delete':
					$fileData = new BoondManager_ObjectBDD_File($dataBDD->getDBinstance(), $this);
					$idfileReturn = 0;
					//On vérifie si le document appartient à la fiche
					if($dataBDD->isRegistered($tabDocument[0])) {foreach($dataBDD->get($tabDocument[0]) as $cv) {if($cv[$tabDocument[1]] == $idfile) {$result = true;$filename = $cv[$tabDocument[2]];break;}}}
					if($result) {
						if(sizeof($dataBDD->get($tabDocument[0])) == $quota) $result = 2;
						$fileData->deleteFileData($filetype, $idfile);
						//Gestion des notifications
						switch($typeFiche) {
							case 'candidat':
								if($manageMebuBar)
									BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteCV($filename);
								else
									BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);
								break;
							case 'ressource':
								if($manageMebuBar)
									BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteCV($filename);
								else
									BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);
								break;
							case 'achat':BoondManager_Notification_Achat::getInstance($dataBDD->get('ID_ACHAT'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'paiement':BoondManager_Notification_Paiement::getInstance($dataBDD->get('ID_PAIEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deletePJ('PMT'.$dataBDD->get('ID_PAIEMENT'), $filename);break;
							case 'action':BoondManager_Notification_Action::getInstance($dataBDD->get('ID_ACTION'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->update();break;
							case 'besoin':BoondManager_Notification_Besoin::getInstance($dataBDD->get('ID_AO'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'positionnement':BoondManager_Notification_Positionnement::getInstance($dataBDD->get('ID_POSITIONNEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'crmsociete':BoondManager_Notification_CRMSociete::getInstance($dataBDD->get('ID_CRMSOCIETE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'commande':BoondManager_Notification_Commande::getInstance($dataBDD->get('ID_BONDECOMMANDE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'produit':BoondManager_Notification_Produit::getInstance($dataBDD->get('ID_PRODUIT'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'projet':BoondManager_Notification_Projet::getInstance($dataBDD->get('ID_PROJET'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
                            case 'mission':BoondManager_Notification_Mission::getInstance($dataBDD->get('ID_MISSIONPROJET'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'temps':break;
							case 'frais':break;
							case 'absence':break;
						}
						if($manageMebuBar) {if(sizeof($dataBDD->get($tabDocument[0])) == 2) {foreach($dataBDD->get($tabDocument[0]) as $cv) {if($cv[$tabDocument[1]] != $idfile) {$idfileReturn = $cv[$tabDocument[1]];break;}}} else if(sizeof($dataBDD->get($tabDocument[0])) == 1) $idfileReturn = -1;}
					}
					$response = '<file todo="delete" value="'.intval($result).'" filename="" idfile="'.$idfileReturn.'" index="'.convertHTMLSpecialChars($index).'" idfiche="'.convertHTMLSpecialChars($idFiche).'" ongletfiche="'.convertHTMLSpecialChars($ongletFiche).'" typefiche="'.convertHTMLSpecialChars($typeFiche).'" appviewer="'.convertHTMLSpecialChars($appviewer).'" urlbase="'.convertHTMLSpecialChars(APP_URL).'" baction="'.convertHTMLSpecialChars($baction).'" vajax="wajax" vtab="wtabFile" language="'.convertHTMLSpecialChars(Wish_Session::getInstance()->get('wish_language')).'" />';
					break;
				case 'join':
					$fileData = new BoondManager_ObjectBDD_File($dataBDD->getDBinstance(), $this);

					$idfile = false;
					$filename = '';

					//On vérifie si la capacité de stockage n'est pas atteinte
					$maxquota = false;
					$todo = 'join';
					$abonnementBDD = new BoondManager_ObjectBDD_Abonnement($dataBDD->getDBinstance());
					if($abonnementBDD->getAbonnementGroupe() && $abonnementBDD->get('AB_MAXSTORAGE') <= $this->getDirectorySize(-1)) {$maxquota = true;$todo = 'quota-max';}

					if(!$maxquota && sizeof($dataBDD->get($tabDocument[0])) < $quota && intval($dataBDD->get($tabDocument[4])) > 0) {
						$pjData = ($manageMebuBar)?$this->updateCV($htmlFile, 8192000):$this->updateFile($htmlFile, 8192000);
						if(sizeof($pjData) > 0) {
							$idfile = $fileData->newFileData($filetype, $pjData[$tabDocument[3]], $pjData[$tabDocument[2]], ($manageMebuBar)?$pjData['CV_TEXT']:'', $dataBDD->get($tabDocument[4]), false, $tabDocument[5]);
							$filename = $pjData[$tabDocument[2]];
							//Gestion des notifications
							switch($typeFiche) {
								case 'candidat':
									if($manageMebuBar)
										BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addCV($filename);//Gestion des notifications
									else
										BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);//Gestion des notifications
									break;
								case 'ressource':
									if($manageMebuBar)
										BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addCV($filename);
									else
										BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);
									break;
								case 'achat':BoondManager_Notification_Achat::getInstance($dataBDD->get('ID_ACHAT'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'paiement':BoondManager_Notification_Paiement::getInstance($dataBDD->get('ID_PAIEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addPJ('PMT'.$dataBDD->get('ID_PAIEMENT'), $filename);break;
								case 'action':BoondManager_Notification_Action::getInstance($dataBDD->get('ID_ACTION'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->update();break;
								case 'besoin':BoondManager_Notification_Besoin::getInstance($dataBDD->get('ID_AO'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'positionnement':BoondManager_Notification_Positionnement::getInstance($dataBDD->get('ID_POSITIONNEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'crmsociete':BoondManager_Notification_CRMSociete::getInstance($dataBDD->get('ID_CRMSOCIETE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'commande':BoondManager_Notification_Commande::getInstance($dataBDD->get('ID_BONDECOMMANDE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'produit':BoondManager_Notification_Produit::getInstance($dataBDD->get('ID_PRODUIT'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'projet':BoondManager_Notification_Projet::getInstance($dataBDD->get('ID_PROJET'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
                                case 'mission':BoondManager_Notification_Mission::getInstance($dataBDD->get('ID_MISSIONPROJET'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'temps':break;
								case 'frais':break;
								case 'absence':break;
							}
						}
					}

					if($idfile) {if(sizeof($dataBDD->get($tabDocument[0])) < ($quota-1)) $result = 2; else $result = true;} else $result = false;
					//On renvoit une page simple avec le script ci-dessous avec d'informer l'IFRAME de la page de se rafraîchir
					echo '<script language="javascript" type="text/javascript">window.top.window.Wish_StopUpload(\''.intval($result).'\',\''.convertHTMLSpecialChars($index).'\',\''.$todo.'\',\''.convertHTMLSpecialChars($filename).'\',\''.$idfile.'\',\''.convertHTMLSpecialChars($idFiche).'\',\''.convertHTMLSpecialChars($ongletFiche).'\',\''.convertHTMLSpecialChars($typeFiche).'\',\''.convertHTMLSpecialChars($appviewer).'\',\''.convertHTMLSpecialChars(APP_URL).'\',\''.convertHTMLSpecialChars($baction).'\',\'wajax\',\'wtabFile\',\''.convertHTMLSpecialChars(Wish_Session::getInstance()->get('wish_language')).'\');</script>';
					$dataBDD->dbClose();
					exit;//On quitte car le script ne doit rien renvoyer d'autre au navigateur client
					break;
			}

			//On positionne la réponse XML
			Wish_Registry::getInstance()->set('wish_xmlmessage', $response);
		} else {
			//En cas d'erreur on renvoit le script indiquant de terminer le webservices (et donc de cacher la barre de chargement
			echo '<script language="javascript" type="text/javascript">window.top.window.Wish_StopUpload(\'0\',\''.convertHTMLSpecialChars($index).'\',\''.$todo.'\',\'\',\'0\',\''.convertHTMLSpecialChars($idFiche).'\',\''.convertHTMLSpecialChars($ongletFiche).'\',\''.convertHTMLSpecialChars($typeFiche).'\',\''.convertHTMLSpecialChars($appviewer).'\',\''.convertHTMLSpecialChars(APP_URL).'\',\''.convertHTMLSpecialChars($baction).'\',\'wajax\',\'wtabFile\',\''.convertHTMLSpecialChars(Wish_Session::getInstance()->get('wish_language')).'\');</script>';
			$dataBDD->dbClose();
			exit;//On quitte car le script ne doit rien renvoyer d'autre au navigateur client
		}
	}*/
}
