<?php
/**
 * log.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib;

use BoondManager\Lib\Log\Emailer;
use BoondManager\Lib\Log\Formater;
use BoondManager\Services\BM;
use Wish\Log\LoggerInterface;

class Log extends \Prefab {

	const FLAG_SENDMAIL = 0b0001;
	const FLAG_WRITE    = 0b0010;

	const TYPE_ERROR = 'error';
	const TYPE_INFO = 'info';
	const TYPE_WARNING = 'warning';

	/**
	 * @var \Log[]
	 */
	private $logger = [];

	/**
	 * @var Emailer[]
	 */
	private $emailer = [];

	/**
	 * @var Formater
	 */
	private $fileFormatter;
	/**
	 * @var Formater
	 */
	private $emailFormatter;

	protected function __construct()
	{
		$this->emailFormatter = new Formater(Formater::FOR_EMAIL);
		$this->fileFormatter = new Formater(Formater::FOR_FILE);
	}

	/**
	 * @param $name
	 * @return LoggerInterface
	 */
	private function getWriter($name){
		if(!isset($this->logger[$name])){
			$this->logger[$name] = new \Log("$name.log");
		}
		return $this->logger[$name];
	}

	private function getEmailer($severity){

		if(!isset($this->emailer[$severity])){
			$this->emailer[$severity] = new Emailer($severity);
		}
		return $this->emailer[$severity];
	}

	public function add($message, $severity, $flags = null){
		if($flags & self::FLAG_WRITE){
			$logger = $this->getWriter($severity);
			$logger->write( $this->fileFormatter->format($message) );
		}

		if($flags & self::FLAG_SENDMAIL && $this->mailActivated()){
			$logger = $this->getEmailer($severity);
			$logger->write( $this->emailFormatter->format($message) );
		}
	}

	private function mailActivated(){
		return \Base::instance()->get('BOONDMANAGER.REPORT_MAIL_ERRORS') == 1 || \Base::instance()->get('BOONDMANAGER.REPORT_MAIL_ERRORS') == 2 && !BM::isDevelopmentMode();
	}

	/**
	 * should be used to log system error
	 * @param $message
	 * @param int $flags
	 */
	public static function addError($message, $flags = self::FLAG_WRITE | self::FLAG_SENDMAIL){
		self::instance()->add($message, self::TYPE_ERROR, $flags);
	}

	/**
	 * used to log datas
	 * @param $message
	 * @param int $flags
	 */
	public static function addInfo($message, $flags = self::FLAG_WRITE){
		self::instance()->add($message, self::TYPE_INFO, $flags);
	}

	/**
	 * should be used to log error related to business rules
	 * @param $message
	 * @param int $flags
	 */
	public static function addWarning($message, $flags = self::FLAG_WRITE | self::FLAG_SENDMAIL){
		self::instance()->add($message, self::TYPE_WARNING, $flags);
	}
}
