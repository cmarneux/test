<?php
/**
 * emailer.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Log;

use BoondManager\Lib\Log;
use BoondManager\Services\BM;
use Wish\Email;
use Wish\Log\LoggerInterface;

class Emailer implements LoggerInterface {

	private $mail;
	private $to;
	private $subject;
	private $from;

	public function __construct($severity)
	{
		$this->mail = new Email();

		$subject = \Base::instance()->get('BMEMAIL.SUBJECT');
		if(!$subject) $subject = 'BoondManager Lab Notification';

		$severity = strtoupper($severity);

		$this->subject = "[$severity] $subject";
		$this->to = ($severity == Log::TYPE_WARNING) ? \Base::instance()->get('BMEMAIL.CONTACT') : \Base::instance()->get('BMEMAIL.HOTLINE');
		$this->from = \Base::instance()->get('BMEMAIL.NOREPLY');
	}

	public function write($message)
	{
		$this->mail->Send($this->to, $this->subject, $message, $this->from);
	}
}
