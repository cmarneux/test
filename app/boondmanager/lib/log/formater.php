<?php
/**
 * formater.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Log;

use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use Wish\Log\FormaterInterface;

class Formater implements FormaterInterface{

	const FOR_FILE = 1;
	const FOR_EMAIL = 2;

	private $for;

	public function __construct($for)
	{
		$this->for = $for;
	}

	/**
	 * gather data for file or email formater
	 * @return array
	 */
	protected function gatherCommonData(){
		$data = [];
		$data['Environment'] = BM::getEnvironnement();
		$data['Auth type'] = BM::getAuthType();
		$data['User ID'] = CurrentUser::instance()->isLogged() ? CurrentUser::instance()->getUserId() : '';
		$data['User Name'] = CurrentUser::instance()->getFullName();
		$data['User Email'] = CurrentUser::instance()->getEmail();
		$data['User Type'] = CurrentUser::instance()->getAccount()->level;
		$data['User login'] = CurrentUser::instance()->getAccount()->login;
		$data['User Agent'] = $_SERVER['HTTP_USER_AGENT'];
		$data['IP Address'] = $_SERVER['REMOTE_ADDR'];

		$requestParts = explode('?', $_SERVER['REQUEST_URI']);
		$data['Api'] = $_SERVER['REQUEST_METHOD'] . ' ' . $requestParts[0];

		return $data;
	}

	/**
	 * gather data for the file formater only
	 * @return array
	 */
	protected function gatherFileData(){
		return [
			'Site' => $_SERVER['SERVER_NAME']
		];
	}

	/**
	 * gather data for the email formater only
	 * @return array
	 */
	protected function gatherEmailData(){
		return [];
	}

	/**
	 * @param string $message
	 * @return string
	 */
	protected function emailFormat($message){
		$data = array_merge($this->gatherCommonData(), $this->gatherEmailData());

		$mail = '';
		foreach($data as $key => $value){
			$mail .= "$key: $value\n";
		}

		$mail .= "\n$message\n";

		return $mail;
	}

	/**
	 * @param string $message
	 * @return string
	 */
	protected function fileFormat($message){
		$data = array_merge($this->gatherCommonData(), $this->gatherFileData());
		$data[] = $message;

		return implode(' - ', $data);
	}

	/**
	 * @param string $message
	 * @return string the date & the remote adress will be included by F3\Log
	 */
	public function format($message)
	{
		if($this->for == self::FOR_EMAIL){
			return $this->emailFormat($message);
		}else{
			return $this->fileFormat($message);
		}
	}
}
