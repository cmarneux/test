<?php
/**
 * message.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Log;

class Message{

	const EXEC_TIME = 'answered in %f ms';
	const MAX_QUOTA = 'user %d has reach his quota';
	const API_REQUESTED = 'user %d request the api %s';
	const API_REQUESTED_PARAMS = self::API_REQUESTED.' with %s';

	public static function execTime($time){
		return sprintf(self::EXEC_TIME, $time);
	}

	public static function maxQuota($userID){
		return sprintf(self::EXEC_TIME, $userID);
	}

	public static function getAPI($userID, $api, $params = null){
		return $params ? sprintf(self::API_REQUESTED_PARAMS, $userID, $api, $params) : sprintf(self::API_REQUESTED, $userID, $api);
	}
}
