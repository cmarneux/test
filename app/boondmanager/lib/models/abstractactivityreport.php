<?php
/**
 * activityinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Models;

use BoondManager\Models\Agency;
use BoondManager\Models\Employee;
use BoondManager\Models\Validation;
use BoondManager\Services\Validations;
use Wish\Models\ModelJSONAPI;

/**
 * @property bool closed
 * @property Validation[] $validations
 * @property Employee resource
 * @property Agency agency
 * @property Employee[] validationWorkflow
 */
abstract class AbstractActivityReport
extends ModelJSONAPI
implements HasAgencyInterface, HasPoleInterface, HasManagerInterface, HasHrManagerInterface {

	public function isClosed() {
		return $this->closed;
	}

	abstract public function getValidationWorkflow();

	public function calculateValidationWorkflow(){
		$workflow = $this->getValidationWorkflow();
		$this->validationWorkflow = Validations::getValidatorsDataFromWorkflow($this->resource, $workflow);
	}

	public function initRelationships()
	{
		$this->setRelationships('ID_PROFIL', 'resource', Employee::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setGroupedRelationships('VALIDATIONS', 'validations');
		$this->setGroupedRelationships('validationWorkflow', 'validationWorkflow');
	}
}
