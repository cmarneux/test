<?php
/**
 * abstractfile.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class AbstractFile
 * @property int id
 * @property string name
 * @package BoondManager\Lib\Models
 * @property ModelJSONAPI dependsOn
 */
abstract class AbstractFile extends ModelJSONAPI {

	const SUBTYPE = '__UNDEFINED__';

	/**
	 * @var string
	 */
	protected static $_jsonType = 'document';

	/**
	 * return the object ID
	 * @return string
	 */
	public function getID()
	{
		return $this->id.'_'.$this->getSubType();
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getSubType() {
		return static::SUBTYPE;
	}

	/**
	 * @return int
	 */
	abstract public function getSubTypeID();

	/**
	 * @return string
	 */
	abstract public function getParentType();
}
