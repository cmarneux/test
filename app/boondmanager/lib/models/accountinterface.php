<?php
/**
 * accountinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Models;

interface AccountInterface{
	public function isRoot();
	public function isSupport();
	public function isEmployee();
	public function isManager();
}
