<?php
/**
 * hasagencyinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

/**
 * Interface HasAgencyInterface
 * @package BoondManager\Lib\Models
 */
interface HasAgencyInterface{
	/**
	 * @return mixed
	 */
	public function getAgencyID();
}
