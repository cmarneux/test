<?php
/**
 * hasagencytrait.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

use BoondManager\Models\Agency;

/**
 * Class HasAgencyTrait
 * @package BoondManager\Models\MySQL\RowObject
 * @property int ID_SOCIETE
 * @property Agency agency
 */
trait HasAgencyTrait{
	/**
	 * return the agency ID
	 * @return int
	 */
	public function getAgencyID()
	{
		if($this->agency) return intval($this->agency->getID());
		else return null;
	}
}
