<?php
/**
 * hashrmanagerinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

/**
 * Interface HasHrManagerInterface
 * @package BoondManager\Lib\Models
 */
interface HasHrManagerInterface{
	/**
	 * @return mixed
	 */
	public function getHrManagerID();
}
