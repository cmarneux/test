<?php
/**
 * hasmanagerinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

/**
 * Interface HasManagerInterface
 * @package BoondManager\Lib\Models
 */
interface HasManagerInterface{
	/**
	 * @return mixed
	 */
	public function getManagerID();
}
