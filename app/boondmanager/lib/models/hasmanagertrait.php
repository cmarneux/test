<?php
/**
 * hasmanagertrait.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

use BoondManager\Models\Account;

/**
 * Class HasManagerTrait
 * @package BoondManager\Models\MySQL\RowObject
 * @property int ID_PROFIL
 * @property Account mainManager
 */
trait HasManagerTrait{
	/**
	 * return the manager ID
	 * @return int
	 */
	public function getManagerID()
	{
		if($this->mainManager) return intval($this->mainManager->getID());
		else return null;
	}
}
