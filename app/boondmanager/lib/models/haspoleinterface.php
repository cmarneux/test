<?php
/**
 * haspoleinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

/**
 * Interface HasPoleInterface
 * @package BoondManager\Lib\Models
 */
interface HasPoleInterface{
	/**
	 * @return mixed
	 */
	public function getPoleID();
}
