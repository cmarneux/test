<?php
/**
 * haspoletrait.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Models;

use BoondManager\Models\Pole;

/**
 * Class HasPoleTrait
 * @package BoondManager\Models\MySQL\RowObject
 * @property int ID_POLE
 * @property Pole pole
 */
trait HasPoleTrait{
	/**
	 * return the pole ID
	 * @return int
	 */
	public function getPoleID()
	{
		if ($this->pole) return intval($this->pole->getID());
		else return null;
	}
}
