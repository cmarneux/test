<?php
/**
 * Created by PhpStorm.
 * User: Wish
 * Date: 26/11/2016
 * Time: 14:34
 */
namespace BoondManager\Lib\MySQL;

use BoondManager\Services\BM;
use Wish\MySQL\DbLink;

/**
 * Class DbFactory
 *
 * a DB builder (Client,Boondmanager,...)
 *
 * @package Wish\MySQL\MySQL
 */
class DbFactory extends \Prefab
{
	/**
	 * @var \Wish\MySQL\DbLink local database
	 */
	private $dbClient;

	/**
	 * @var \Wish\MySQL\DbLink BoondManager database
	 */
	private $dbBM;

	/**
	 * return a DB instance for the local database
	 * @return \Wish\MySQL\DbLink
	 */
	public function getDbClient()
	{
		if(!$this->dbClient) $this->dbClient = $this->buildDbClient();
		return $this->dbClient;
	}

	/**
	 * return a DB instance for the BoondManager database
	 * @return DbLink
	 */
	public function getDbBoondManager()
	{
		if(!$this->dbBM) $this->dbBM = $this->buildDbBoondManager();
		return $this->dbBM;
	}

	/**
	 * Build the Db for BoondManager
	 * @return DbLink
	 */
	private function buildDbBoondManager()
	{
		$server = \Base::instance()->get('BMMYSQL.SERVER');
		$port = \Base::instance()->get('BMMYSQL.PORT');
		$dbName = \Base::instance()->get('BMMYSQL.DATABASE');
		$user = \Base::instance()->get('BMMYSQL.USER');
		$pass = \Base::instance()->get('BMMYSQL.PWD');

		$dsn = "mysql:host=$server;port=$port;dbname=$dbName";

		return new DbLink($dsn, $user, $pass);
	}

	/**
	 * Build the Db for the current Client
	 * @return DbLink
	 * @throws \Exception
	 */
	private function buildDbClient()
	{
		if(!BM::isCustomerInterfaceActive()) \Base::instance()->error(500, 'No database found');

		$server = BM::getCustomerInterfaceDbServer();
		$port = BM::getCustomerInterfaceDbPort();
		$dbName = BM::getCustomerInterfaceDbName();
		$user = BM::getCustomerInterfaceDbUser();
		$pass = BM::getCustomerInterfaceDbPwd();

		$dsn = "mysql:host=$server;port=$port;dbname=$dbName";

		try {
			$db = new DbLink($dsn, $user, $pass);
		} catch(\Exception $e) {
			\Base::instance()->error(500, 'Unable to connect to database');
			throw $e;
		}
		return $db;
	}

	/**
	 * close all DB connections
	 */
	public function closeDbClient()
	{
		if ($this->dbClient) {
			$this->dbClient->closeConnection();
			$this->dbClient = null;
		}
	}

	/**
	 * close all DB connections
	 */
	public function closeDbBoondManager()
	{
		if ($this->dbBM) {
			$this->dbBM->closeConnection();
			$this->dbBM = null;
		}
	}

	/**
	 * close all DB connections
	 */
	public function closeAll()
	{
		$this->closeDbBoondManager();

		$this->closeDbClient();
	}

	/**
	 * destructor
	 * Close all DB connections if their are still opened
	 */
	public function __destruct()
	{
		$this->closeAll();
	}
}
