<?php
/**
 * requestaccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Lib;

use BoondManager\Services\CurrentUser;
use Wish\Models\Model;

/**
 * Class RequestAccess
 * @package BoondManager\Lib
 */
class RequestAccess{
	/**
	 * @var CurrentUser the user
	 */
	public $user;

	/**
	 * @var Model some data loaded with the request
	 */
	public $data;

	/**
	 * @var string HTTP VERB
	 */
	public $VERB = 'get';

	/**
	 * @var int id
	 */
	public $id = 0;

	/**
	 * Request constructor.
	 */
	public function __construct($data = null, $user = null){
		$f3 = \Base::instance();
		if($f3->exists('VERB')) $this->VERB = strtolower($f3->get('VERB'));
		if($f3->exists('PARAMS.id')) $this->id = $f3->get('PARAMS.id');

		$this->data = $data;
		if(!$user) $this->user = CurrentUser::instance();
	}

	/**
	 * return the request in a string format
	 * example:
	 *      get:/profile/resource
	 * @return string
	 */
	public function getStringRequest(){
		die('TODO : A développer au moment de traiter les Apps');
	}

	public function getAPIPattern(){
		$f3 = \Base::instance();

		$pattern = $f3['PATTERN'];

		//str_replace('@controller', $this->get('controller'), $pattern);

		return str_replace(['@controller', '@subController'], [$this->get('controller'), $this->get('subController')], $pattern);
	}

	public function getRouteParams($asString = false){
		$params = array_diff_key( \Base::instance()->get('PARAMS'), ['controller'=>0, 'subController'=>0, 0=>0]);
		if(!$asString) return $params;

		$pString = [];
		foreach($params as $key=>$value){
			$pString[] = "$key=$value";
		}

		return implode(' ',$pString);
	}

	/**
	 * set the current user
	 * @param CurrentUser $user
	 */
	public function setUser(CurrentUser $user){
		$this->user = $user;
	}

	/**
	 * get the current user
	 * @return CurrentUser
	 */
	public function getUser(){
		return $this->user;
	}

	/**
	 * retrieve a param (GET or POST)
	 * @param string $name parameter's name
	 * @param mixed $default default value if the parameter doesn't exists
	 * @return mixed|null
	 */
	public function get($name, $default = null){
		$key = "PARAMS.$name";
		if(\Base::instance()->exists($key)) return \Base::instance()->get($key);

		$key = "REQUEST.$name";
		if(\Base::instance()->exists($key)) return \Base::instance()->get($key);
		else return $default;
	}

	/**
	* get all parameters sent in the request
	* @return array
	*/
	public function getParams() {
		$f3 = \Base::instance();
		if(func_num_args()){
			$args = func_get_args();
			return array_intersect_key($f3['REQUEST'], array_flip($args));
		}else {
			return $f3['REQUEST'];
		}
	}

	/**
	 * data setter
	 * @param mixed $data
	 * @return $this
	 */
	public function setData($data){
		$this->data = $data;
		return $this;
	}
}
