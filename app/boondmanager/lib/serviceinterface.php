<?php
/**
 * serviceInterface.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib;

use Wish\Filters\AbstractJsonAPI;
use BoondManager\Models\Rights;
use Wish\Models\ModelJSONAPI;
use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Services\BM;
use Wish\MySQL\SearchResult;

/**
 * Interface ServiceInterface
 * @package BoondManager\Lib
 */
interface ServiceInterface{
	/**
	 * Check if object exists
	 * @param int $id
	 * @return ModelJSONAPI|null
	 */
	public static function find($id);

	/**
	 * Search objects
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	public static function search(AbstractSearch $filter);

	/**
	 * Get object
	 * @param int $id
	 * @param string $tab
	 * @return \Wish\Models\ModelJSONAPI
	 */
	public static function get($id, $tab = BM::TAB_DEFAULT);

	/**
	 * Update object
	 * @param AbstractJsonAPI $filter
	 * @param \Wish\Models\ModelJSONAPI $object
	 * @param string $tab
	 * @return boolean
	 */
	public static function update($filter, ModelJSONAPI &$object, $tab = BM::TAB_DEFAULT);

	/**
	 * Save object
	 * @param \Wish\Models\ModelJSONAPI $object
	 * @return boolean
	 */
	public static function create(ModelJSONAPI $object);

	/**
	 * Delete object
	 * @param int $id
	 * @return boolean
	 */
	public static function delete($id);

	/**
	 * Build object from filter
	 * @param \Wish\Filters\AbstractJsonAPI $filter
	 * @param \Wish\Models\ModelJSONAPI $object
	 * @param string $tab
	 * @return \Wish\Models\ModelJSONAPI
	 */
	public static function buildFromFilter($filter, ModelJSONAPI $object = null, $tab = BM::TAB_DEFAULT);

	/**
	 * Get object's rights
	 * @param \Wish\Models\ModelJSONAPI $object
	 * @return Rights
	 */
	public static function getRights(ModelJSONAPI $object);
}
