<?php
/**
 * solr.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Lib;

use Wish\Email;
use Wish\Web;
use BoondManager\Services\BM;
use Wish\Tools;

/**
 * Handle all work related to SolR Database
 * @namespace \BoondManager\Lib
 */
class SolR extends \Prefab {
	/** @var  string SolR DB connection port */
	protected $port;

	/** @var  Web Web instance*/
	protected $web;

	/**
	 * SolR constructor.
	 */
	function __construct() {
		$this->web = new Web();
		$this->port = \Base::instance()->get('BMSOLR.PORT');
	}

	/**
	 * Escape characters for SolR
	 * @param  string $keywords the string to escaped
	 * @return string the escaped string
	 */
    public static function escape($keywords) {
    	return str_replace(array('\\','&','|','!','(',')','{','}','[',']','^',':','/'),
    					   array('\\\\','\\&','\\|','\\!','\\(','\\)','\\{','\\}','\\[','\\]','\\^','\\:','\\/'),
    					   $keywords);
    }

	/**
	 * @param $keywords
	 * @param $raw
	 * @param $stemming
	 * @param $wildcard
	 */
    public static function parseStemming($keywords, &$raw, &$stemming, &$wildcard) {
    	//On récupère les mots entre expression litérale
        $tabRaw = array();
        if(substr_count($keywords, '"') % 2 != 0) $keywords = str_replace('"','', $keywords);
        if(preg_match_all('#([+-]?)"([^\"]*)"#', $keywords, $match)) {
            $stemming = str_replace($match[0],'', $keywords);//On supprime l'expression litérale de la recherche en Stemming
            foreach($match[2] as $word) $tabRaw[] = explode(' ', $word);
            foreach($tabRaw as $i => $tabWords) {
                foreach($tabWords as $j => $word) if($word == '') unset($tabWords[$j]); else $tabWords[$j] = $word;
                $tabRaw[$i] = ((isset($match[1][$i]) && $match[1][$i] != '') ? $match[1][$i] : '').'"'.implode(' ', $tabWords).'"';
            }
        } else $stemming = $keywords;

        //On filtre les caractères -+ isolés
        $stemming = trim(preg_replace('/((^|\s)([+-]|\s)+(\s|$))/', ' ', $stemming));

        $tabWildcardSearch = explode(' ', $stemming);
        foreach($tabWildcardSearch as $i => $word) if($word != '' && substr($word, -1) != '*') $tabWildcardSearch[$i] .= '*';
        $wildcard = implode(' ', $tabWildcardSearch);

        $raw = implode(' ', $tabRaw);
    }

	/**
	 * Handle SolR errors (send an email)
	 * @param string $module
	 * @param string $client
	 * @param string $serveur
	 * @param int $errorType
	 *  0 => failed a research
	 *  1 => failed to import data from MySQL
	 *  2 => failed to  delete a client
	 *  3 => failed the data optimization
	 * @param array $tabError an array with the key 'msg'
	 */
	private function manageError($module, $client, $serveur, $errorType = 0, $tabError = array()) {
		switch($errorType) {
			case 1:  $errorTitle = 'SolR a échoué dans l\'importation des données MySQL !'; break;
			case 2:  $errorTitle = 'SolR a échoué dans la suppression d\'un client !'; break;
			case 3:  $errorTitle = 'SolR a échoué dans l\'optimisation de ses données !'; break;
			default: $errorTitle = 'SolR a échoué dans une recherche !'; break;
		}

		$emailObj = new Email();
		$error = $errorTitle.PHP_EOL.PHP_EOL.
			'Erreur : '.(isset($tabError['msg'])?$tabError['msg']:'').PHP_EOL.
			'Serveur : '.$serveur.PHP_EOL.
			'Module : '.$module.PHP_EOL.
			(($client != '')?'Client : '.$client:'');
		$emailObj->Send(\Base::instance()->get('EMAIL.HOTLINE'), 'Erreur sur BoondManager', $error , \Base::instance()->get('EMAIL.NOREPLY'));
		Tools::logError($error);
	}

	/**
	 * Perform a search on SolR Database
	 *
	 * @param  string $module Module name, &isin; `["candidats", "ressource"]`. ( @see BM::SOLR_MODULE_* )
	 * @param  array  $qParams an array for the `q` parameter sent in the query
	 * @param  array  $fqParams an array for the `fq` parameter sent in the query
	 * @param  array  $sortParams an array for the `sort` parameter sent in the query
	 * @param  integer $start start offset
	 * @param  integer $rows number of rows
	 * @param  array  $flParams   [description]
	 * @link http://lucene.apache.org/solr/quickstart.html
	 * @return array|false response
	 */
    public function search($module, $qParams = array(), $fqParams = array(), $sortParams = array(), $start, $rows, $flParams = array()) {
		if(\Base::instance()->get('BMSOLR.ENABLED') && \Base::instance()->exists('SESSION.CLIENT')) {
			$tabServer = explode(':', \Base::instance()->get('SESSION.CLIENT.BMMYSQL.SERVER'));
			try {
				$fqParams = array_merge(array('client:'.\Base::instance()->get('SESSION.CLIENT.BMMYSQL.DATABASE')), $fqParams);
				$payload = array('start' => $start, 'rows' => $rows);
				if(sizeof($qParams)>0) $payload['q'] = implode(' OR ', array_filter($qParams)); else $payload['q'] = '*:*';
				if(sizeof($fqParams)>0) $payload['fq'] = implode(' AND ', array_filter($fqParams));
				if(sizeof($sortParams)>0) $payload['sort'] = implode(',', array_filter($sortParams));
				if(sizeof($flParams)>0) $payload['fl'] = implode(',', array_filter($flParams));

				$response = $this->web->setUrl('http://'.$tabServer[0].':'.$this->port.'/'.(BM::isProductionMode()?$tabServer[0]:'solr').'/'.$module.'/select', 'application/json')->get($payload);
				if($response && isset($response['responseHeader'])) {
					if(isset($response['responseHeader']['status']) && $response['responseHeader']['status'] == 0)
						return $response['response'];
					else
						throw new \Exception($this->web->getLastError());
				}
			} catch(\Exception $e) {
				$this->manageError($module, \Base::instance()->get('SESSION.CLIENT.BMMYSQL.DATABASE'), $tabServer[0], 0, $e->getMessage());
			}
		}
		return false;
    }

	/**
	 * Perform a synchronisation with the SolR database
	 *
	 * This function call a sync. script in order to force SolR to retrieve data from MySQL server
	 * @param array $tabModules the modules to synchronize (@see BM::SOLR_MODULE_*)
	 * @link http://lucene.apache.org/solr/quickstart.html
	 */
	private function Synchronisation($tabModules) {
		if(\Base::instance()->get('BMSOLR.ENABLED') && \Base::instance()->exists('SESSION.CLIENT')) {
			$newImportDate = date('Y-m-d H:i:s', time());
			$solrBDD = new \BoondManager\Databases\Local\SolR();
			$tabServer = explode(':',\Base::instance()->get('SESSION.CLIENT.BMMYSQL.SERVER'));
			if(!is_array($tabModules)) $tabModules = array($tabModules);

			foreach($tabModules as $module) {
				switch($module) {
					case BM::SOLR_MODULE_CANDIDATES: $tModule = 8;break;
					case BM::SOLR_MODULE_RESOURCES: $tModule = 7;break;
					case BM::SOLR_MODULE_CRM_COMPANIES: $tModule = 0;break;
					case BM::SOLR_MODULE_CRM_CONTACTS: $tModule = 10;break;
					default:$tModule = -1;
				}
				if($tModule >= 0 && $solrEntry = $solrBDD->getObject($tModule)) {//On récupère la date de dernière MAJ de Solr
					BM::execScript( 'php '.BM::getRootPath() .'/cgi-bin/SolR/SolR.php '.$tabServer[0].' '.BM::getCustomerCode().' '.$module.' '.$solrEntry['SOLR_LASTIMPORT'].' '.BM::getEnvironnement().' '.$tModule, true);
					$solrBDD->setObject(array('SOLR' => array('SOLR_LASTIMPORT' => $newImportDate)), $solrEntry['ID_SOLR']);//On lance la MAJ de SolR & on mémorise cette date
				}
			}
		}
	}
	/**
	 * Retrieve the status of the SolR Database
	 * @param string $ip SolR Database server IP
	 * @param string $module module's name, &isin; `["candidats", "ressource"]`. (@see BM::SOLR_MODULE)
	 * @param string $client Client connection URL
	 * @link http://lucene.apache.org/solr/quickstart.html
	 * @return boolean if `true` the dotabase is busy, otherwise, it's available for queries
	 */
    public function status($ip, $module, $client = '') {
		if(\Base::instance()->get('BMSOLR.ENABLED')) {
			try {
				$payload['command'] = 'status';
				$response = $this->web->setUrl('http://'.$ip.':'.$this->port.'/'.(BM::isProductionMode()?$ip:'solr').'/'.$module.'/dataimport', 'application/json')->get($payload);
				if($response && isset($response['responseHeader'])) {
					if(isset($response['responseHeader']['status']) && $response['responseHeader']['status'] == 0 && isset($response['status']) && $response['status'] == 'idle') {
						//On vérifie si le module a précédemment terminé correctement ou en erreur le dernier import
						if(isset($response['statusMessages']) && isset($response['statusMessages']['Delta Dump started']) && isset($response['statusMessages']['Rolledback']) && $response['statusMessages']['Rolledback'] != "") {//ERREUR
							$error = 'SolR a effecturé un RollBack !'.PHP_EOL
								.PHP_EOL
								.'Dump Started Date : '.$response['statusMessages']['Delta Dump started'].PHP_EOL
								.'Rolled Back Date : '.$response['statusMessages']['Rolledback'].PHP_EOL
								.'IP Serveur : '. $ip .PHP_EOL
								.'Module : '. $module .PHP_EOL
								.'Client : '.$client;
							Tools::logError($error);
							$email = new Email();
							$email->Send( \Base::instance()->get('MAIL.HOTLINE'), 'Erreur sur BoondManager', $error, \Base::instance()->get('MAIL.NOREPLY') );
						}
						return true;
					}
				}
			} catch(\Exception $e) {}
		}
		return false;
    }

    /**
	 * trigger an import to update the SolR server
	 * @param string $ip SolR Database server IP
	 * @param string $client Client connection URL
	 * @param string $module module's name, &isin; `["candidats", "ressource"]`. (@see BM::SOLR_MODULE)
	 * @param string $last_import  last import date to update only newer data
	 *
	 * The date must have the following format: `YYYY-MM-DD HH:II:SS`.
	 * @link http://lucene.apache.org/solr/quickstart.html
	 * @return boolean
	 */
    public function dataimport($ip, $client, $module, $last_import = '') {
		if(\Base::instance()->get('BMSOLR.ENABLED')) {
			try {
				$payload['command'] = 'delta-import';
				$payload['client'] = $client;
				if(preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/", $last_import, $match)) $payload['last_import'] = $last_import; //Si il n'y a pas de date, cela revient à faire un full-import des données

				$response = $this->web->setUrl('http://'.$ip.':'.$this->port.'/'.(BM::isProductionMode()?$ip:'solr').'/'.$module.'/dataimport', 'application/json')->get($payload);
				if($response && isset($response['responseHeader'])) {
					if(isset($response['responseHeader']['status']) && $response['responseHeader']['status'] == 0) return true;
					else throw new \Exception($this->web->getLastError());
				}
			} catch(\Exception $e) {
				$this->manageError($module, $client, $ip, 1, $e->getMessage());
			}
		}
		return false;
    }

    /**
	 * @inheritdoc self::Synchronisation()
	 */
    public function Creation($module) {return $this->Synchronisation($module);}

	/**
	 * @inheritdoc self::Synchronisation()
	 */
	public function Suppression($module) {return $this->Synchronisation($module);}

	/**
	 * @inheritdoc self::Synchronisation()
	 */
	public function Modification($module) {return $this->Synchronisation($module);}

	/**
	 * Optimize Solr Tables
	 *
	 * @param string $module module's name, &isin; `["candidats", "ressource"]`.
	 * @param string $ip SolR server IP
	 * @link http://lucene.apache.org/solr/quickstart.html
	 * @return boolean
	 */
    public function optimize($module, $ip) {
		if(\Base::instance()->get('BMSOLR.ENABLED')) {
			try {
				$payload['optimize'] = "true";
				$payload['wt'] = 'json';
				$response = $this->web->setUrl('http://'.$ip.':'.$this->port.'/'.(BM::isProductionMode()?$ip:'solr').'/'.$module.'/update', 'application/json')->post($payload);
				if($response && isset($response['responseHeader'])) {
					if(isset($response['responseHeader']['status']) && $response['responseHeader']['status'] == 0) return true;
					else throw new \Exception($this->web->getLastError());
				}
			} catch(\Exception $e) {
				$this->manageError($module, '', $ip, 3, $e->getMessage());
			}
		}
		return false;
    }

    /**
	 * Delete all client data on SolR
	 *
	 * @param string $ip Solr server IP
	 * @param string $module module's name, &isin; `["candidats", "ressource"]`.
	 * @param string $client client connection URL
	 * @link http://lucene.apache.org/solr/quickstart.html
	 * @return boolean
	 */
	public function deleteClient($ip, $module, $client) {
		if(\Base::instance()->get('BMSOLR.ENABLED')) {
			try {
				$payload['commit'] = "true";
				$payload['wt'] = 'json';
				$response = $this->web->setUrl('http://'.$ip.':'.$this->port.'/'.(BM::isProductionMode()?$ip:'solr').'/'.$module.'/update', 'application/json')->post('{"delete":{"query":"client:'.self::escape($client).'"}}', $payload);
				if($response && isset($response['responseHeader'])) {
					if(isset($response['responseHeader']['status']) && $response['responseHeader']['status'] == 0)
						return true;
					else
						throw new \Exception($this->web->getLastError());
				}
			} catch(\Exception $e) {
				$this->manageError($module, $client, $ip, 2, $e->getMessage());
			}
		}
		return false;
	}
}
