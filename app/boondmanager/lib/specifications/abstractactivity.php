<?php
/**
 * activityvalidationstrait.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Lib\Specifications;
use BoondManager\Databases\Local\ExpensesReport;
use BoondManager\Lib\Models\AbstractActivityReport;
use BoondManager\Models\AbsencesReport;
use BoondManager\Models\TimesReport;
use BoondManager\Models\Validation;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Validations;
use Wish\Specifications\AbstractSpecificationItem;

/**
 * Trait TabBehavior to manage Tabs
 * @package BoondManager\Models\Specifications\RequestAccess
 */
abstract class AbstractActivity extends AbstractSpecificationItem {

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return array
	 */
	private function calculateRights($entity, $user){
		if($entity->isClosed()) {
			return [
				$allow_validation = false,
				$allow_devalidation = false,
				$allow_rejection = false,
				$allow_pdf = true,
				$allow_save = false,
				$allow_pdfprj = true,
				$allow_upload = false
			];
		}

		$allow_validation   = false;//Par défaut, la validation d'une fiche n'est pas autorisée à chaque membre
		$allow_devalidation = false;
		$allow_rejection    = false;
		$allow_pdf          = true;
		$allow_save         = true;
		$allow_pdfprj       = true;
		$allow_upload       = false;

		// employee's case
		if($entity->resource->id == $user->getEmployeeId() && !$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)) {
			foreach($entity->validations as $val){
				if($val->state == Validation::VALIDATION_VALIDATED) {
					if($val->expectedValidator->id == $user->getEmployeeId()){
						$allow_validation = false;
						$allow_devalidation = true;
						$allow_save = false;
					}else{
						$allow_validation = false;
						$allow_devalidation = false;
						$allow_save = false;
						break;
					}
				}
			}
		} else {
			//On récupère la position du manager actuel dans la liste du circuit de validation

			$validators = Validations::getValidatorsDataFromWorkflow($entity->resource, $entity->getValidationWorkflow());

			$i_circuit = 0;
			$valsIDs = [];
			foreach($validators as $i => $val) {
				$valsIDs[] = $val->id;//On construit le tableau des validateurs

				if( $val->id == $user->getEmployeeId() || $user->isMyManager($val->id)) {
					$i_circuit = $i+1;
					$allow_validation = true;
				}
			}

			foreach($entity->validations as $val){
				if($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $val->expectedValidator->id != $entity->resource->id && in_array($val->expectedValidator->id, $valsIDs)){
					$allow_upload = true;

					if($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $val->expectedValidator->id == $user->getEmployeeId() || $user->isMyManager($val->expectedValidator->id)){
						//Si il s'agit du manager ou d'un de ses responsables
						if($val->state == Validation::VALIDATION_VALIDATED) {
							$allow_validation = false;
							$allow_devalidation = true; //Si un état est validé, alors on autorise la dévalidation
							$allow_save = false;
						} else {
							$allow_validation = true;
							$allow_save = true;
						}
					}else{
						$i = -1;
						//Sinon, il s'agit de quelqu'un d'autre
						//On recherche sa position dans le circuit de validation
						foreach($valsIDs as $i => $id) {
							if( $id == $val->expectedValidator->id )
								break;
						}

						//Si une personne après lui dans le circuit de validation à valider alors il ne peut plus rien faire
						if($i > $i_circuit && $val->state == Validation::VALIDATION_VALIDATED ){
							$allow_devalidation = false;
							$allow_validation = false;
							$allow_save = false;
							break;
						}
					}
				}
			}

			if($allow_validation) $allow_rejection = true;
			if($allow_save) $allow_upload = true;
		}

		return [$allow_validation, $allow_devalidation, $allow_rejection, $allow_pdf, $allow_save, $allow_pdfprj, $allow_upload];
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canValidate($entity, $user) {
		$rights = $this->calculateRights($entity, $user);
		return $rights[0];
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canDevalidate($entity, $user) {
		$rights = $this->calculateRights($entity, $user);
		return $rights[1];
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canReject($entity, $user) {
		$rights = $this->calculateRights($entity, $user);
		return $rights[2];
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canAllowPDF($entity, $user) {
		// simplification du test
		return true;
		/*
		$rights = $this->calculateRights($entity, $user);
		return $rights[3];
		*/
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canAllowPDFPrj($entity, $user) {
		// simplification du test
		return true;
		/*
		$rights = $this->calculateRights($entity, $user);
		return $rights[5];
		*/
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canSaveActivity($entity, $user) {
		$rights = $this->calculateRights($entity, $user);
		return $rights[4];
	}

	/**
	 * @param AbstractActivityReport $entity
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function canAllowUpload($entity, $user) {
		$rights = $this->calculateRights($entity, $user);
		return $rights[6];
	}
}
