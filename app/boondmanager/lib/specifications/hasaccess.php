<?php
/**
 * userhaveaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Specifications;

use BoondManager\Lib\RequestAccess;
use BoondManager\Services\BM;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\CurrentUser;

/**
 * Class HasAccess
 * @package BoondManager\Models\Specifications\RequestAccess
 */
class HasAccess extends AbstractSpecificationItem{
	/**
	 * @var string tab name
	 */
	private $tab;

	/**
	 * @var string module name
	 */
	private $module;

	/**
	 * @var int level
	 */
	private $level;

	/**
	 * HasAccess constructor.
	 * @param $module
	 * @param null $tab
	 * @param null $level
	 */
	public function __construct($module, $tab = null, $level = null){
		return $this->resetData($module, $tab, $level);
	}

	/**
	 * @param string $module
	 * @param string $tab
	 * @param mixed $level
	 * @return $this
	 */
	public function resetData($module, $tab = null, $level = null) {
		$this->tab = $tab;
		$this->module = $module;
		$this->level = $level;
		return $this;
	}

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object) {

		$user = $object->getUser()->getAccount();

		$module = $this->module;
		$level = $this->level;
		$tab = $this->tab;

		// TODO : decide if theses specials cases should be handled there or build with modules in the Account mapper
		switch ($user->level) {
			case BM::USER_TYPE_ADMINISTRATOR:
			case BM::USER_TYPE_ROOT:
				if( in_array($module, [
					BM::MODULE_ACCOUNTS,
					BM::MODULE_AGENCIES,
					BM::MODULE_BUSINESS_UNITS,
					BM::MODULE_APPS,
					BM::MODULE_SUBSCRIPTION
				])) return true;
				break;
			case BM::USER_TYPE_MANAGER:
				if( in_array($module, [
					BM::MODULE_FLAGS,
					BM::MODULE_ACTIONS
				])) return true;
				break;
		}

		if($module == BM::MODULE_DASHBOARD)
			return true;

		$moduleAccess = boolval($user->modules->$module);
		if(!$moduleAccess) return false;

		if($tab) {
			$rightValue = $user->rights->$module->$tab;

			if(!is_null($level) && $rightValue !== false) {
				return is_array($level) ? in_array($rightValue, $level) : ($rightValue == $level);
			}else
				return boolval($rightValue);
		}
		return true;
	}
}
