<?php
/**
 * HasAllPerimeterManager.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Lib\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;

/**
 * Class HasAllPerimeterManager
 * @package BoondManager\Models\Specifications\User
 */
class HasAllPerimeterManager extends AbstractSpecificationItem{
    /**
     * check if the object match the specification
     * @param CurrentUser $object
     * @return bool
     */
    public function isSatisfiedBy($object)
    {
        return $object->hasRight(BM::RIGHT_GLOBAL_SHOWALLMANAGERS, null, [
            BM::RIGHT_SHOWALLMANAGERS_PERIMETRE_ET_AFFECTATION,
            BM::RIGHT_SHOWALLMANAGERS_PERIMETRE
        ]);
    }
}
