<?php
/**
 * userhaveaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Specifications;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\CurrentUser;

/**
 * Class UserHaveRight
 * @package BoondManager\Models\Specifications\RequestAccess
 */
class HaveAccess extends AbstractSpecificationItem{
	/**
	 * @var string tab name
	 */
	private $tab;

	/**
	 * @var string module name
	 */
	private $module;

	/**
	 * @var int level
	 */
	private $level;

	/**
	 * HaveAccess constructor.
	 * @param $module
	 * @param null $tab
	 * @param null $level
	 */
	public function __construct($module, $tab = null, $level = null){
		$this->tab = $tab;
		$this->module = $module;
		$this->level = $level;
	}

	/**
	 * check if the object match the specification
	 * @param mixed $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		/**
		 * @var CurrentUser $user
		 */
		$user = $object->getUser();

		return $user->hasAccess($this->module, $this->tab, $this->level);
	}
}
