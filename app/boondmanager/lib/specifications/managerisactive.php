<?php
/**
 * managerisactive.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Lib\Specifications;

use BoondManager\Databases\Local\Manager;
use BoondManager\Services\BM;
use Wish\Specifications\AbstractSpecificationItem;

/**
 * Class ManagerisActive
 * @package BoondManager\Lib\Specifications
 */
class ManagerIsActive extends AbstractSpecificationItem{

    /**
     * check if the object match the specification
     * @param \BoondManager\Models\Account $object
     * @return bool
     */
    public function isSatisfiedBy($object)
    {
        return ($object->USER_ABONNEMENT == BM::DB_ACCESS_ACTIVE);
    }
}
