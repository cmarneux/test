<?php
/**
 * tabbehavior.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Lib\Specifications;

/**
 * Trait TabBehavior to manage Tabs
 * @package BoondManager\Models\Specifications\RequestAccess
 */
trait TabBehavior{
	/**
	 * @var int a current tab
	 */
	private $tab;

	/**
	 * TabBehavior constructor.
	 * @param int $tab
	 */
	public function __construct($tab){
		$this->setTab($tab);
	}

	/**
	 * set a current tab
	 * @param int $tab
	 */
	public function setTab($tab){
		$this->tab = $tab;
	}

	/**
	 * get the current tab
	 * @return int
	 */
	public function getTab(){
		return $this->tab;
	}
}
