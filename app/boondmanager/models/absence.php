<?php
/**
 * absence.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property integer id
 * @property string startDate
 * @property string enDate
 * @property string title
 * @property int duration
 * @property WorkUnitType workUnitType
 * @property AbsencesReport absencesReport
 * @property TimesReport timesReport
 * @package BoondManager\Models
 */
class Absence extends ModelJSONAPI{

	/**
	 * @var string
	 */
	protected static $_jsonType = 'absence';

	const MAPPER = [
		'ID_PABSENCE'   => ['name' => 'id', 'type' => self::TYPE_STRING],
		'PABS_DEBUT'    => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'PABS_FIN'      => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'PABS_INTITULE' => ['name' => 'title', 'type' => self::TYPE_STRING],
		'PABS_DUREE'    => ['name' => 'duration', 'type' => self::TYPE_FLOAT],
		'workUnitType'  => ['name' => 'workUnitType', 'type'=>self::TYPE_OBJECT], //~ groupment of PABS_TYPEHREF et TYPEH_NAME
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function initRelationships(){
		$this->setRelationships('ID_LISTEABSENCES', 'absencesReport', AbsencesReport::class);
	}
}
