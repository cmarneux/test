<?php
/**
 * absencesaccount.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class AbsencesAccount
 * @property string $id
 * @property string $period INTQUO_BEINGACQUIRED modified
 * @property integer $year INTQUO_BEINGACQUIRED modified
 * @property float $amountAcquired INTQUO_QUOTA
 * @property float amountBeingAcquired INTQUO_BEINGACQUIRED
 * @property float INTQUO_USED amount used (virtual)
 * @property float INTQUO_DELTA amount left (virtual)
 * @property float amountAcquiredUsed virtual
 * @property float amountAcquiredAsked virtual
 * @property float amountBeingAcquiredUsed virtual
 * @property float amountBeingAcquiredAsked virtual
 * @property float deltaAcquiredUsed virtual
 * @property float deltaAcquiredAsked virtual
 * @property float deltaBeingAcquiredUsed virtual
 * @property float deltaBeingAcquiredAsked virtual
 * @property float deltaAskedAndUsed virtual
 * @property float usePose
 * @property WorkUnitType workUnitType
 * @property Agency agency
 * @package BoondManager\Models
 */
class AbsencesAccount extends ModelJSONAPI{
	/**
	 * @var string
	 */
	protected static $_jsonIDAttribute = 'ID_ABSENCESACCOUNT'; // virtual field

	/**
	 * @var string
	 */
	protected static $_jsonType = 'absencesaccount';

	const MAPPER = [
		'ID_ABSENCESACCOUNT'   => ['name' => 'id', 'type' => self::TYPE_STRING],
		'INTQUO_PERIODE'       => ['name' => 'period', 'type' => self::TYPE_STRING],
		'INTQUO_YEAR'          => ['name' => 'year', 'type' => self::TYPE_INT],
		'INTQUO_QUOTA'         => ['name' => 'amountAcquired', 'type' => self::TYPE_FLOAT],
		//'amountAcquired'           => ['name' => 'amountAcquired', 'type' => self::TYPE_FLOAT],
		'amountAcquiredAsked'      => ['name' => 'amountAcquiredAsked', 'type' => self::TYPE_FLOAT],
		'amountAcquiredUsed'       => ['name' => 'amountAcquiredUsed', 'type' => self::TYPE_FLOAT],
		'INTQUO_BEINGACQUIRED' => ['name' => 'amountBeingAcquired', 'type' => self::TYPE_FLOAT],
		//'amountBeingAcquired'      => ['name' => 'amountBeingAcquired', 'type' => self::TYPE_FLOAT],
		'amountBeingAcquiredAsked' => ['name' => 'amountBeingAcquiredAsked', 'type' => self::TYPE_FLOAT],
		'amountBeingAcquiredUsed'  => ['name' => 'amountBeingAcquiredUsed', 'type' => self::TYPE_FLOAT],
		'usePose'                  => ['name' => 'usePose', 'type' => self::TYPE_FLOAT],
		'deltaAcquiredAsked'       => ['name' => 'deltaAcquiredAsked', 'type' => self::TYPE_FLOAT],
		'deltaAskedAndUsed'        => ['name' => 'deltaAskedAndUsed', 'type' => self::TYPE_FLOAT],
		'deltaBeingAcquiredAsked'  => ['name' => 'deltaBeingAcquiredAsked', 'type' => self::TYPE_FLOAT],
		'deltaBeingAcquiredUsed'   => ['name' => 'deltaBeingAcquiredUsed', 'type' => self::TYPE_FLOAT],
		'workUnitType'         => ['name' => 'workUnitType', 'type' => self::TYPE_OBJECT],
		// virtual fields
		'INTQUO_USED'          => ['name' => 'amountUsed', 'type' => self::TYPE_FLOAT],
		'INTQUO_DELTA'         => ['name' => 'delta', 'type' => self::TYPE_FLOAT],
	];

	/**
	 * callback function after a key/field is updated
	 * @param $updateKey
	 */
	protected function onUpdatedData($updateKey, $value = null, $oldvalue = null){
		switch($updateKey){
			case 'INTQUO_PERIODE':
				if(count($exploded = explode('-', $this->period)) > 1){
					$this->period      = AbsencesQuota::ACCOUNTSPERIOD[intval($exploded[1])];
					$this->year = $exploded[0];
				}
				break;
		}
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
	}
}
