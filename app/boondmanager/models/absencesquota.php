<?php
/**
 * absencesquota.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class AbsencesQuota
 * @property string id
 * @property string period
 * @property int year
 * @property float quotaAcquired
 * @property bool|string useBeingAcquired
 * @property float monthlyIncrement
 * @property bool autoDeferral
 * @property WorkUnitType workUnitType
 * @package BoondManager\Models
 */
class AbsencesQuota extends ModelJSONAPI{

	/**
	 * @var string
	 */
	protected static $_jsonType = 'absencesquota';

	const MAPPER = [
		'ID_TYPEDECOMPTE'        => ['name' => 'id', 'type' => self::TYPE_STRING],
		'TYPED_STARTDECOMPTE'    => ['name' => 'period', 'type' => self::TYPE_STRING, 'mapper' => AbsencesQuota::ACCOUNTSPERIOD],
		'year'                   => ['name' => 'year', 'type' => self::TYPE_INT],
		'TYPED_QUOTA'            => ['name' => 'quotaAcquired', 'type' => self::TYPE_FLOAT],
		'TYPED_USEBEINGACQUIRED' => ['name' => 'useBeingAcquired', 'type' => self::TYPE_IGNORE, 'serializeCallback' => 'serializeUseBeingAcquired', 'unserializeCallback' => 'unserializeUseBeingAcquired'],
		'TYPED_INCR'             => ['name' => 'monthlyIncrement', 'type' => self::TYPE_FLOAT],
		'TYPED_REPORT'           => ['name' => 'autoDeferral', 'type' => self::TYPE_BOOLEAN],
		'workUnitType'           => ['name' => 'workUnitType', 'type' => self::TYPE_OBJECT]
	];

	/**#@+
	 * @var array ACCOUNTSPERIOD
	 */
	const ACCOUNTSPERIOD = [
		1 => 'JanuaryToDecember',
		2 => 'FebruaryToJanuary',
		3 => 'MarchToFebruary',
		4 => 'AprilToMarch',
		5 => 'MayToApril',
		6 => 'JuneToMay',
		7 => 'JulyToJune',
		8 => 'AugustToJuly',
		9 => 'SeptemberToAugust',
		10 => 'OctoberToSeptember',
		11 => 'NovemberToOctober',
		12 => 'DecemberToNovember',
	];
	/**#@-*/

	/**#@+
	 * @var array CALCULATIONMETHOD
	 */
	const CALCULATIONMETHOD = [
		0 => 'takenUntilN-1AndAskedFromN',
		1 => 'totalTakenAndAsked'
	];
	/**#@-*/

	/**#@+
	 * @var array USEBEINGACQUIRED
	 */
	const USEBEINGACQUIRED = [
		0 => 'inactive',
		1 => 'AllowTakenAbsencesOnBeingAcquired',
		2 => 'ForbidTakenAbsencesOnBeingAcquired'
	];
	/**#@-*/

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public static function serializeUseBeingAcquired($value) {
		switch($value) {
			case 'AllowTakenAbsencesOnBeingAcquired':$value = 1;break;
			case 'ForbidTakenAbsencesOnBeingAcquired':$value = 2;break;
			default:$value = 0;break;
		}
		return $value;
	}

	public static function unserializeUseBeingAcquired($value) {
		switch($value) {
			case 1:$value = 'AllowTakenAbsencesOnBeingAcquired';break;
			case 2:$value = 'ForbidTakenAbsencesOnBeingAcquired';break;
			default:$value = false;break;
		}
		return $value;
	}

	public function getStartDate(){
		$monthId = Tools::reverseMapData($this->period, self::ACCOUNTSPERIOD);
		return date('Y-m-d', mktime(0,0,0, $monthId, 1, $this->year));
	}

	public function getEndDate(){
		$monthId = Tools::reverseMapData($this->period, self::ACCOUNTSPERIOD);
		return date('Y-m-d', mktime(0,0,0, $monthId, 0, $this->year+1));
	}
}
