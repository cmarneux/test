<?php
/**
 * AbsencesReport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractActivityReport;
use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasHrManagerInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasPoleInterface;
use Wish\Models\Model;

/**
 * class AbsencesReport
 * @property int $id
 * @property string $creationDate
 * @property string $title
 * @property string $state
 * @property string $informationComments
 * @property boolean $closed
 * @property Validation[] $validations
 * @property AbsencesReport[] LISTE_PABSENCES
 * @property Employee resource
 * @property Agency agency
 * @property Absence[] $absences
 * @property Absence[] absencesPeriods
 * @property AbsencesAccount[] absencesAccounts
 * @property Model[] $files
 * @package BoondManager\Models
 */
class AbsencesReport extends AbstractActivityReport implements HasAgencyInterface, HasPoleInterface, HasManagerInterface, HasHrManagerInterface{
	use HasAgencyTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'absencesreport';

	const MAPPER = [
		'ID_LISTEABSENCES'           => ['name' => 'id', 'type' => self::TYPE_INT],
		'LISTEABSENCES_DATE'         => ['name' => 'creationDate', 'type' => self::TYPE_DATE],
		'LISTEABSENCES_TITRE'        => ['name' => 'title', 'type' => self::TYPE_STRING],
		'LISTEABSENCES_ETAT'         => ['name' => 'state', 'type' => self::TYPE_STRING, 'mapper' => Validation::STATE_MAPPER],
		'LISTEABSENCES_COMMENTAIRES' => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'LISTEABSENCES_CLOTURE'      => ['name' => 'closed', 'type' => self::TYPE_BOOLEAN],
		'absencesPeriods'            => ['name' => 'absencesPeriods', 'type' => self::TYPE_ARRAY],
		'absencesAccounts'           => ['name' => 'absencesAccounts', 'type' => self::TYPE_ARRAY]
	];

	protected function init() {
		$this->absencesAccounts = [];
		$this->absencesPeriods = [];
	}


	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		parent::initRelationships();
		$this->setGroupedRelationships('LIST_ABSENCES', 'absences');
	}

	public function getValidationWorkflow(){
		return $this->resource->absencesReportsWorkflow ? $this->resource->absencesReportsWorkflow : $this->agency->absencesReportsWorkflow;
	}

	/**
	 * @return int|null
	 */
	public function getHrManagerID() {
		return $this->resource->getHrManagerID();
	}

	/**
	 * @return int|null
	 */
	public function getManagerID() {
		return $this->resource->getManagerID();
	}

	/**
	 * @return int|null
	 */
	public function getPoleID() {
		return $this->resource->getPoleID();
	}
}
