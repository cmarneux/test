<?php
/**
 * account.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

/**
 * Class Manager
 * @package BoondManager\Models
 */
class Account extends Employee {
	const INACTIVE = 'inactive';
	const HIRING_DATE = 'hiringDate';
}
