<?php
/**
 * accountmodules.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

/**
 * class AccountModules
 * @property bool flags
 * @property bool resources
 * @property bool accounts
 * @property bool agencies
 * @property bool businessUnits
 * @property bool poles
 * @property bool marketplace
 * @property bool apps
 * @property bool subscription
 * @property bool projects
 * @property bool opportunities
 * @property bool candidates
 * @property bool crm
 * @property bool activityExpenses
 * @property bool billing
 * @property bool products
 * @property bool purchases
 * @property bool reporting
 * @property bool actions
 * @property bool dashboard
 * @property bool architecture
 * @property bool customers
 * @property bool reglements
 * @property bool downloadCenter
 * @package BoondManager\Models
 */
class AccountModules extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_RESSOURCE'    => ['name' => BM::MODULE_RESOURCES, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_PROJET'       => ['name' => BM::MODULE_PROJECTS, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_BESOIN'       => ['name' => BM::MODULE_OPPORTUNITIES, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_CANDIDAT'     => ['name' => BM::MODULE_CANDIDATES, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_PRODUIT'      => ['name' => BM::MODULE_PRODUCTS, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_CRM'          => ['name' => BM::MODULE_CRM, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ACHAT'        => ['name' => BM::MODULE_PURCHASES, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_REPORTING'    => ['name' => BM::MODULE_REPORTING, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_TPSFRS'       => ['name' => BM::MODULE_ACTIVITIES_EXPENSES, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_FACTURATION'  => ['name' => BM::MODULE_BILLING, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWFACTURES' => ['name' => BM::MODULE_SUBSCRIPTION, 'type' => self::TYPE_BOOLEAN],
			// has no match in db and must be set manually
			'actions'             => ['name' => BM::MODULE_ACTIONS, 'type' => self::TYPE_BOOLEAN],
			'dashboard'           => ['name' => BM::MODULE_DASHBOARD, 'type' => self::TYPE_BOOLEAN],
			'architecture'        => ['name' => BM::MODULE_ADMIN_ARCHITECTURE, 'type' => self::TYPE_BOOLEAN],
			'customers'           => ['name' => BM::MODULE_ADMIN_CUSTOMERS, 'type' => self::TYPE_BOOLEAN],
			'reglements'          => ['name' => BM::MODULE_ADMIN_PAYMENTS, 'type' => self::TYPE_BOOLEAN],
			'apps'                => ['name' => BM::MODULE_APPS, 'type' => self::TYPE_BOOLEAN],
			'accounts'            => ['name' => BM::MODULE_ACCOUNTS, 'type' => self::TYPE_BOOLEAN],
			'flags'               => ['name' => BM::MODULE_FLAGS, 'type' => self::TYPE_BOOLEAN],
			'marketplace'         => ['name' => BM::MODULE_MARKETPLACE, 'type' => self::TYPE_BOOLEAN],
			'poles'               => ['name' => BM::MODULE_POLES, 'type' => self::TYPE_BOOLEAN],
			'businessUnits'       => ['name' => BM::MODULE_BUSINESS_UNITS, 'type' => self::TYPE_BOOLEAN],
			'agencies'            => ['name' => BM::MODULE_AGENCIES, 'type' => self::TYPE_BOOLEAN],
			'downloadCenter'      => ['name' => BM::MODULE_DOWNLOAD_CENTER, 'type' => self::TYPE_BOOLEAN],
			'triggerAccounts'     => ['name' => BM::MODULE_TRIGGER_ACCOUNTS, 'type' => self::TYPE_BOOLEAN],
			'absencesAccounts'    => ['name' => BM::MODULE_ABSENCESACCOUNTS, 'type' => self::TYPE_BOOLEAN],

		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
