<?php
/**
 * accountrights.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;


/**
 * Class UserRights
 * @package BoondManager\Models
 * @property AccountRights\Main main
 * @property AccountRights\Resources resources
 * @property AccountRights\Candidates candidates
 * @property AccountRights\CRM crm
 * @property AccountRights\Opportunities opportunities
 * @property AccountRights\Projects projects
 * @property AccountRights\Products products
 * @property AccountRights\Purchases purchases
 * @property AccountRights\Billing billing
 * @property AccountRights\Reporting reporting
 * @property AccountRights\Actions actions
 * @property AccountRights\ActivityExpenses activityExpenses
 * @property AccountRights\Flags flags
 */
class AccountRights extends ModelJSON {

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition(){
		return [
			'main' 						   => ['name' => 'main', 'type' => self::TYPE_OBJECT],
			BM::MODULE_RESOURCES           => ['name' => BM::MODULE_RESOURCES, 'type' => self::TYPE_OBJECT],
			BM::MODULE_OPPORTUNITIES       => ['name' => BM::MODULE_OPPORTUNITIES, 'type' => self::TYPE_OBJECT],
			BM::MODULE_PROJECTS            => ['name' => BM::MODULE_PROJECTS, 'type' => self::TYPE_OBJECT],
			BM::MODULE_CANDIDATES          => ['name' => BM::MODULE_CANDIDATES, 'type' => self::TYPE_OBJECT],
			BM::MODULE_CRM                 => ['name' => BM::MODULE_CRM, 'type' => self::TYPE_OBJECT],
			BM::MODULE_ACTIVITIES_EXPENSES => ['name' => BM::MODULE_ACTIVITIES_EXPENSES, 'type' => self::TYPE_OBJECT],
			BM::MODULE_BILLING             => ['name' => BM::MODULE_BILLING, 'type' => self::TYPE_OBJECT],
			BM::MODULE_PRODUCTS            => ['name' => BM::MODULE_PRODUCTS, 'type' => self::TYPE_OBJECT],
			BM::MODULE_PURCHASES           => ['name' => BM::MODULE_PURCHASES, 'type' => self::TYPE_OBJECT],
			BM::MODULE_REPORTING           => ['name' => BM::MODULE_REPORTING, 'type' => self::TYPE_OBJECT],
			BM::MODULE_ACTIONS             => ['name' => BM::MODULE_ACTIONS, 'type' => self::TYPE_OBJECT],
			BM::MODULE_FLAGS               => ['name' => BM::MODULE_FLAGS, 'type' => self::TYPE_OBJECT],
		];
	}
}
