<?php
/**
 * actions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Actions extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_WRITEALLACTIONS'         => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONACTIONS'       => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONACTIONS' => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONACTIONS' => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN]
		];
	}
}
