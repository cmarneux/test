<?php
/**
 * activityexpenses.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class ActivityExpenses extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_EXTRACTIONTPSFRS'       => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONTPSFRS' => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONTPSFRS' => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXPORTATIONTPSFRS'      => ['name' => BM::RIGHT_EXPORTATION, 'type' => self::TYPE_BOOLEAN],
		];
	}
}
