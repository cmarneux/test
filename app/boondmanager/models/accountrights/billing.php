<?php
/**
 * billing.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Billing extends ModelJSON {

	const MAPPER = [
		'CONFIG_ALLOWMODIFYREFERENCEBDC' => ['name' => BM::RIGHT_EDIT_BDC_REFERENCES, 'type' => self::TYPE_BOOLEAN],
		'CONFIG_SHOWALLBDC'              => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
		'CONFIG_WRITEALLBDC'             => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
		'CONFIG_EXTRACTIONBDC'           => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
		'CONFIG_ALLOWAFFECTATIONBDC'     => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
		'CONFIG_ALLOWSUPPRESSIONBDC'     => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
		'CONFIG_ALLOWMODIFYDATEBILLS'    => ['name' => BM::RIGHT_EDIT_CREATION_DATE, 'type' => self::TYPE_BOOLEAN],
		'CONFIG_EXPORTATIONBDC'          => ['name' => BM::RIGHT_EXPORTATION, 'type' => self::TYPE_BOOLEAN],
	];

	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}
}
