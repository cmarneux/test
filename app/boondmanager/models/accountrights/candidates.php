<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

/**
 * Class Candidates
 * @package BoondManager\Models\AccountRights
 * @property bool creation
 * @property mixed showAll
 * @property bool writeAll
 * @property bool extraction
 * @property bool deletion
 * @property bool assignment
 * @property bool editCreationDate
 * @property bool actions
 */
class Candidates extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_ALLOWCREERCANDIDATS'              => ['name' => BM::RIGHT_CREATION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLCANDIDATS'                 => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
			'CONFIG_WRITEALLCANDIDATS'                => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONCANDIDATS'              => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONCANDIDATS'        => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONCANDIDATS'        => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWMODIFYCREATIONDATECANDIDATS' => ['name' => BM::RIGHT_EDIT_CREATION_DATE, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWACCESSACTIONSCANDIDATS'      => ['name' => BM::RIGHT_ACCESS_ACTIONS, 'type' => self::TYPE_BOOLEAN],
		];
	}
}
