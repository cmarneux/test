<?php
/**
 * crm.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class CRM extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_SHOWALLCRM'          => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
			'CONFIG_WRITEALLCRM'         => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONCRM'       => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONCRM' => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONCRM' => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXPORTATIONCRM'      => ['name' => BM::RIGHT_EXPORTATION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWMODIFYCREATIONDATECRM' => ['name' => BM::RIGHT_EDIT_CREATION_DATE, 'type' => self::TYPE_BOOLEAN]
		];
	}
}

