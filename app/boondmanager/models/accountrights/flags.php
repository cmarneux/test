<?php
/**
 * flags.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Flags extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_SHOWALLFLAGS' => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_BOOLEAN],
		];
	}
}

