<?php
/**
 * main.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Main extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_SHOWGROUPE'      => ['name' => BM::RIGHT_GLOBAL_SHOWGROUPE, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLMANAGERS' => ['name' => BM::RIGHT_GLOBAL_SHOWALLMANAGERS, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALLMANAGER_MAPPING],
			'CONFIG_SHOWBUPOLE'      => ['name' => BM::RIGHT_GLOBAL_SHOWBUPOLES, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLPOLES'    => ['name' => BM::RIGHT_GLOBAL_SHOWALLPOLES, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWPOLE_MAPPING],
			'CONFIG_SHOWALLAGENCIES' => ['name' => BM::RIGHT_GLOBAL_SHOWALLAGENCIES, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWAGENCY_MAPPING],
			'CONFIG_ADMINISTRATOR'   => ['name' => BM::MODULE_ADMIN, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWFACTURES'    => ['name' => BM::MODULE_SUBSCRIPTION, 'type' => self::TYPE_BOOLEAN],
		];
	}
}
