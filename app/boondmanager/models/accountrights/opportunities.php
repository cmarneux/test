<?php
/**
 * opportunities.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Opportunities extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_ALLOWMODIFYCREATIONDATEBESOINS' => ['name' => BM::RIGHT_EDIT_CREATION_DATE, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLBESOINS'                 => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
			'CONFIG_WRITEALLBESOINS'                => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONBESOINS'              => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONBESOINS'        => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONBESOINS'        => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWMASKEDDATABESOINS'         => ['name' => BM::RIGHT_MASKDATA, 'type' => self::TYPE_BOOLEAN],
		];
	}
}

