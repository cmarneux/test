<?php
/**
 * products.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Products extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_ALLOWCREERPRODUITS'       => ['name' => BM::RIGHT_CREATION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLPRODUITS'          => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
			'CONFIG_WRITEALLPRODUITS'         => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONPRODUITS'       => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONPRODUITS' => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONPRODUITS' => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
		];
	}
}
