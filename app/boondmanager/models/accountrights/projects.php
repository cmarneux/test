<?php
/**
 * projects.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Projects extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_ALLOWMODIFYCREATIONDATEPROJETS' => ['name' => BM::RIGHT_EDIT_CREATION_DATE, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLPROJETS'                 => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
			'CONFIG_WRITEALLPROJETS'                => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONPROJETS'              => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONPROJETS'        => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONPROJETS'        => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWACCESSADMINPROJETS'        => ['name' => BM::RIGHT_ACCESS_ADMINISTRATIVE, 'type' => self::TYPE_BOOLEAN],
		];
	}
}
