<?php
/**
 * reporting.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Reporting extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_EXTRACTIONREPORTING' => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
		];
	}
}

