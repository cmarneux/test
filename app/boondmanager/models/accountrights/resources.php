<?php
/**
 * resources.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\AccountRights;

use BoondManager\Services\BM;
use Wish\Models\ModelJSON;

class Resources extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'CONFIG_ALLOWCREERRESSOURCES'              => ['name' => BM::RIGHT_CREATION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_WRITEALLRESSOURCES'        		   => ['name' => BM::RIGHT_WRITEALL, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_EXTRACTIONRESSOURCES'              => ['name' => BM::RIGHT_EXTRACTION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWAFFECTATIONRESSOURCES'        => ['name' => BM::RIGHT_ASSIGNMENT, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWSUPPRESSIONRESSOURCES'        => ['name' => BM::RIGHT_DELETION, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_SHOWALLRESSOURCES'                 => ['name' => BM::RIGHT_SHOWALL, 'type' => self::TYPE_IGNORE, 'mapper' => BM::SHOWALL_MAPPING],
			'CONFIG_ALLOWMODIFYCREATIONDATERESSOURCES' => ['name' => BM::RIGHT_EDIT_CREATION_DATE, 'type' => self::TYPE_BOOLEAN],
			'CONFIG_ALLOWACCESSADMINRESSOURCES' 	   => ['name' => BM::RIGHT_ACCESS_ADMINISTRATIVE, 'type' => self::TYPE_IGNORE, 'mapper' => BM::ACCESSADMINISTRATIVE_MAPPING],
			'CONFIG_ALLOWACCESSACTIONSRESSOURCES' 	   => ['name' => BM::RIGHT_ACCESS_ACTIONS, 'type' => self::TYPE_BOOLEAN],
		];
	}
}
