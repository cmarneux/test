<?php
/**
 * actions.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;
use BoondManager\Services\BM;
use BoondManager\Services\Actions;

/**
 * Class Actions
 * @property int $id
 * @property int $ID_PARENT
 * @property int $ID_RESPUSER
 * @property Account mainManager
 * @property string $startDate
 * @property int $typeOf
 * @property string $text
 * @property string $ACTION_TITRE
 * @property int $priority
 * @property int $state
 * @property int $ID_DOCUMENT
 * @property int $numberOfFiles
 * @property boolean $isNotification
 * @property boolean canWriteAction
 * @property boolean canReadAction
 * @property int $category
 * @property Candidate|Resource|Product|Company|Contact|Project|Invoice|Purchase|Opportunity $dependsOn
 * @package BoondManager\Models
 */
class Action extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'action';

	const MAPPER = [
		'ID_ACTION'        => ['name' => 'id', 'type' => self::TYPE_INT],
		'ACTION_DATE'      => ['name' => 'startDate', 'type' => self::TYPE_DATETIME],
		'ACTION_CREATEDAT' => ['name' => 'creationDate', 'type' => self::TYPE_DATE],
		'ACTION_TYPE'      => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'ACTION_TEXTE'     => ['name' => 'text', 'type' => self::TYPE_STRING],
		'ACTION_ETAT'      => ['name' => 'state', 'type' => self::TYPE_INT],
		'ACTION_STATUT'    => ['name' => 'priority', 'type' => self::TYPE_INT],
		'numberOfFiles'    => ['name' => 'numberOfFiles', 'type' => self::TYPE_STRING],
		'ACTION_CATEGORY'  => ['name' => 'category', 'type' => self::TYPE_STRING],
		'NB_DOCUMENT'      => ['name' => 'numberOfFiles', 'type' => self::TYPE_INT],
		'isNotification'   => ['name' => 'isNotification', 'type' => self::TYPE_BOOLEAN],
		'canReadAction'    => ['name' => 'canReadAction', 'type' => self::TYPE_BOOLEAN],
		'canWriteAction'   => ['name' => 'canWriteAction', 'type' => self::TYPE_BOOLEAN],
	];

	/**
	 * callback used after updating data. Used to rebuild some fields based on another
	 * @param string $updateKey
	 */
	public function onUpdatedData($updateKey, $value = null, $oldvalue = null){
		switch($updateKey){
			case 'typeOf': $this->onActionTypeUpdate(); break;
		}
	}

	/**
	 * method triggered by self::onUpdateData()
	 */
	private function onActionTypeUpdate(){
		$this->isNotification = (in_array($this->typeOf, Actions::getNotificationsIDs()));
		$this->category = (Actions::getCategoryFromType($this->typeOf));
	}

	public function isCollaborative(){
		return Actions::isCollaborative($this->typeOf);
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_RESPUSER', 'mainManager', Account::class);
		$this->setDynamicTypedRelationship('ID_PARENT', 'dependsOn', function ($obj){
			switch ($obj->ACTION_CATEGORY){
				case BM::CATEGORY_CRM_CONTACT: return Contact::class;
				case BM::CATEGORY_CRM_COMPANY: return Company::class;
				case BM::CATEGORY_CANDIDATE: return Candidate::class;
				case BM::CATEGORY_RESOURCE: return Employee::class;
				case BM::CATEGORY_PRODUCT: return Product::class;
				case BM::CATEGORY_OPPORTUNITY: return Opportunity::class;
				case BM::CATEGORY_PROJECT: return Project::class;
				case BM::CATEGORY_BILLING: return Invoice::class;
				case BM::CATEGORY_ORDER: return Order::class;
				case BM::CATEGORY_PURCHASE: return Purchase::class;
				case BM::CATEGORY_APP: return App::class;
			}
		});
	}
}
