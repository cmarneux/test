<?php
/**
 * advantage.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int $id
 * @property float $employeeQuota
 * @property float $participationQuota
 * @property float $agencyQuota
 * @property float $employeeAmount
 * @property float $participationAmount
 * @property float $agencyAmount
 * @property int $quantity
 * @property string $informationComments
 * @property float $costCharged
 *
 * @property float exchangeRate
 * @property float $exchangeRateAgency
 * @property int currency
 * @property int currencyAgency
 *
 * @property int $category
 *
 * @property float costPaid
 * @property AdvantageType $advantageType
 *
 * @property Employee resource
 * @property Contract contract
 * @property Agency agency
 * @property Project project
 * @property Delivery delivery
 * @package BoondManager\Models
 */
class Advantage extends ModelJSONAPI {

	const MAPPER = [
		'ID_AVANTAGE'              => ['name' => 'id', 'type' => self::TYPE_INT],
		'AVG_DATE'                 => ['name' => 'date', 'type' => self::TYPE_DATE],
		'AVG_QUANTITE'             => ['name' => 'quantity', 'type' => self::TYPE_FLOAT],
		'AVG_DEVISE'               => ['name' => 'currency', 'type' => self::TYPE_INT],
		'AVG_CHANGE'               => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'AVG_DEVISEAGENCE'         => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'AVG_CATEGORIE'            => ['name' => 'category', 'type' => self::TYPE_INT],
		'AVG_CHANGEAGENCE'         => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'AVG_COUT'                 => ['name' => 'costCharged', 'type' => self::TYPE_FLOAT],
		'AVG_COMMENTAIRES'         => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'AVG_MONTANTRESSOURCE'     => ['name' => 'employeeAmount', 'type' => self::TYPE_FLOAT],
		'AVG_MONTANTSOCIETE'       => ['name' => 'agencyAmount', 'type' => self::TYPE_FLOAT],
		'AVG_MONTANTPARTICIPATION' => ['name' => 'participationAmount', 'type' => self::TYPE_FLOAT],
		'AVG_QUOTARESSOURCE'       => ['name' => 'employeeQuota', 'type' => self::TYPE_FLOAT],
		'AVG_QUOTASOCIETE'         => ['name' => 'agencyQuota', 'type' => self::TYPE_FLOAT],
		'AVG_QUOTAPARTICIPATION'   => ['name' => 'participationQuota', 'type' => self::TYPE_FLOAT],
		'costPaid'                 => ['name' => 'costPaid', 'type' => self::TYPE_FLOAT],
		'advantageType'            => ['name' => 'advantageType', 'type' => self::TYPE_OBJECT],
	];

	/**
	 * @var string
	 */
	protected static $_jsonType = 'advantage';

	public function getCategory(){
		return ($this->category) ? $this->category : $this->advantageType->category;
	}

	public function getQuotaResource(){
		return $this->employeeQuota ? $this->employeeQuota : $this->advantageType->employeeQuota ;
	}

	public function calculateCostPaid(){
		$this->costPaid = ($this->getCategory() == AdvantageType::CATEGORY_PACKAGE)
			? $this->getQuotaResource() * $this->quantity
			: $this->employeeAmount * $this->quantity;

		return $this->costPaid;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function initRelationships(){
		$this->setRelationships('ID_PROFIL', 'resource', Employee::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_CONTRAT', 'contract', Contract::class);
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setRelationships('ID_MISSIONPROJET', 'delivery', Delivery::class);
	}
}
