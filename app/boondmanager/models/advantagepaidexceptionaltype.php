<?php
/**
 * exceptionalpaidadvantagetype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class AdvantagePaidExceptionalType
 * @property WorkUnitType workUnitType
 * @property AdvantageType advantageType
 * @package BoondManager\Models
 */
class AdvantagePaidExceptionalType extends ModelJSONAPI{
	protected $_jsonType = 'advantagepaidexceptionaltype';

	const MAPPER = [
		'workUnitType'  => ['name' => 'workUnitType', 'type' => self::TYPE_OBJECT],
		'advantageType' => ['name' => 'advantageType', 'type' => self::TYPE_OBJECT]
	];

	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
