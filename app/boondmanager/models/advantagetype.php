<?php
/**
 * advantageType.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class AdvantageType
 * @property string id
 * @property string reference
 * @property string name
 * @property float participationQuota
 * @property float employeeQuota
 * @property float agencyQuota
 * @property boolean default
 * @property boolean state
 * @property string category
 * @property string frequency
 * @property Agency agency
 * @package BoondManager\Models
 */
class AdvantageType extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'advantagetype';

	const MAPPER = [
		'ID_TYPEAVANTAGE'          => ['name' => 'id', 'type' => self::TYPE_STRING],
		'TYPEA_REF'                => ['name' => 'reference', 'type' => self::TYPE_INT],
		'TYPEA_NAME'               => ['name' => 'name', 'type' => self::TYPE_STRING],
		'TYPEA_TYPE'               => ['name' => 'frequency', 'type' => self::TYPE_STRING, 'mapper' => self::MAPPER_FREQUENCY],
		'TYPEA_CATEGORIE'          => ['name' => 'category', 'type' => self::TYPE_STRING, 'mapper' => self::MAPPER_CATEGORY],
		'TYPEA_QUOTAPARTICIPATION' => ['name' => 'participationQuota', 'type' => self::TYPE_FLOAT, 'removeIfNull' => true],
		'TYPEA_QUOTARESSOURCE'     => ['name' => 'employeeQuota', 'type' => self::TYPE_FLOAT, 'removeIfNull' => true],
		'TYPEA_QUOTASOCIETE'       => ['name' => 'agencyQuota', 'type' => self::TYPE_FLOAT, 'removeIfNull' => true],
		'TYPEA_ETAT'               => ['name' => 'state', 'type' => self::TYPE_BOOLEAN],
		'TYPEA_DEFAUT'             => ['name' => 'default', 'type' => self::TYPE_BOOLEAN]
	];

	/**#@+
	 * @var int TAB_TYPEAVANTAGE.TYPEA_TYPE
	 */
	const
		FREQUENCY_PUNCTUAL = 'punctual',
		FREQUENCY_DAILY = 'daily',
		FREQUENCY_MONTHLY = 'monthly',
		FREQUENCY_QUATERLY = 'quarterly',
		FREQUENCY_SEMIANNUAL = 'semiAnnual',
		FREQUENCY_ANNUAL = 'annual';
	/**#@-*/

	/**#@+
	 * @var int TAB_TYPEAVANTAGE.TYPEA_CATEGORIE
	 */
	const
		CATEGORY_FIXEDAMOUNT = 'fixedAmount',
		CATEGORY_VARIABLESALARYBASIS = 'variableSalaryBasis',
		CATEGORY_PACKAGE = 'package',
		CATEGORY_LOAN = 'loan';
	/**#@-*/

	const MAPPER_FREQUENCY = [self::FREQUENCY_PUNCTUAL, self::FREQUENCY_DAILY, self::FREQUENCY_MONTHLY, self::FREQUENCY_QUATERLY, self::FREQUENCY_SEMIANNUAL, self::FREQUENCY_ANNUAL];
	const MAPPER_CATEGORY = [self::CATEGORY_FIXEDAMOUNT, self::CATEGORY_VARIABLESALARYBASIS, self::CATEGORY_PACKAGE, self::CATEGORY_LOAN];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
	}

	/**
	 * @return array
	 */
	public static function getFrequencies(){
		return self::MAPPER_FREQUENCY;
	}

	/**
	 * @return array
	 */
	public static function getCategories(){
		return self::MAPPER_CATEGORY;
	}

	/**
	 * @return bool
	 */
	public function isContractual(){
		return $this->frequency != 0;
	}

	/**
	 * @return bool
	 */
	public function isPunctual(){
		return !$this->isContractual();
	}
}
