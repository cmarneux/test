<?php
/**
 * agency.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Services\BM;
use BoondManager\Services\Validations;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * Class Agency
 * @property string id
 * @property string name
 * @property string group
 * @property string logo
 * @property string vatNumber
 * @property string address
 * @property string postcode
 * @property string town
 * @property string country
 * @property boolean state
 * @property int staff
 * @property string email1
 * @property string phone1
 * @property string registrationNumber
 * @property string registeredOffice
 * @property string apeCode
 * @property string legalStatus
 * @property string code
 * @property string key
 * @property int currency
 * @property float exchangeRate
 * @property float chargeFactor
 * @property float numberOfWorkingDays
 * @property string calendar
 * @property float workUnitRate
 * @property boolean showDecompte
 * @property boolean downloadCenter
 * @property boolean intranet
 * @property array expensesReportsWorkflow
 * @property array absencesReportsWorkflow
 * @property array timesReportsWorkflow
 * @property ExpenseType[] expenseTypes
 * @property WorkUnitType[] workUnitTypes
 * @property RatePerKilometerType[] ratePerKilometerTypes
 * @property AbsencesQuota[] absencesQuotas
 * @property string activityExpensesLogo
 * @property string timesLegals
 * @property string expensesLegals
 * @property string absencesLegals
 * @property boolean|string|int timesCreationAndMailingDate
 * @property boolean|string|int expensesCreationAndMailingDate
 * @property boolean contractualExpensesAutomaticFilling
 * @property boolean allowResourcesToViewAbsencesAccounts
 * @property boolean maskExceedingWarningsWithQuotas
 * @property string defaultAbsencesAccountsPeriod
 * @property string absencesCalculationMethod
 * @property string projectsTechnicalAssistanceReferenceMask
 * @property string projectsPackageReferenceMask
 * @property string projectsRecruitmentReferenceMask
 * @property string projectsProductReferenceMask
 * @property float projectsRebillingRate
 * @property boolean allowAlonesMarkers
 * @property boolean allowAdvantagesOnProjects
 * @property boolean allowExceptionalScalesOnProjects
 * @property string|integer jDay
 * @property string purchasesReferenceMask
 * @property int purchasesPaymentTerm
 * @property int purchasesPaymentMethod
 * @property float purchasesTaxRate
 * @property string opportunitiesTechnicalAssistanceReferenceMask
 * @property string opportunitiesPackageReferenceMask
 * @property string opportunitiesRecruitmentReferenceMask
 * @property string opportunitiesProductReferenceMask
 * @property string quotationsLegals
 * @property string quotationsReferenceMask
 * @property int quotationsStartingNumber
 * @property int quotationsValidityInDays
 * @property int quotationsPaymentTerm
 * @property float quotationsTaxRate
 * @property boolean quotationsShowCompanyVATNumberOnPDF
 * @property boolean quotationsShowCompanyNumberOnPDF
 * @property boolean quotationsShowOpportunityReferenceOnPDF
 * @property boolean quotationsShowFooterOnPDF
 * @property string invoicesLogo
 * @property InvoiceRecordType[] invoiceRecordTypes
 * @property BankDetails[] banksDetails
 * @property string invoicesLegals
 * @property array invoicesLockingStates
 * @property boolean invoicesAutomaticCreation
 * @property int invoicesDeltaCreationInDays
 * @property int invoicesStateForSendingMailToClient
 * @property int invoicesStateToSetAfterSendingMailToClient
 * @property string invoicesMailSenderType
 * @property string invoicesMailSenderCustomized
 * @property string invoicesReferenceMask
 * @property string invoicesStartingNumber
 * @property int ordersPaymentTerm
 * @property int ordersPaymentMethod
 * @property float ordersTaxRate
 * @property boolean ordersCopyCommentsOnNewInvoice
 * @property boolean ordersShowCommentsOnPDF
 * @property boolean ordersShowFactorOnPDF
 * @property boolean ordersShowCompanyVATNumberOnPDF
 * @property boolean ordersShowCompanyNumberOnPDF
 * @property boolean ordersShowBankDetailsOnPDF
 * @property boolean ordersShowProjectReferenceOnPDF
 * @property boolean ordersShowResourcesNameOnPDF
 * @property boolean ordersShowAverageDailyPriceOnPDF
 * @property boolean ordersShowNumberOfWorkignDaysOnPDF
 * @property boolean ordersShowFooterOnPDF
 * @property boolean ordersSeparateActivityExpensesAndPurchases
 * @property boolean ordersGroupDeliveries
 * @property BankDetails bankDetails
 * @property Company factor
 * @property float contractsNumberOfHoursPerWeek
 * @property boolean allowExceptionalScalesOnContracts
 * @property ExceptionalScaleType[] exceptionalScaleTypes
 * @property advantagePaidExceptionalType[] advantagePaidExceptionalTypes
 * @property advantageType[] advantageTypes
 * @package BoondManager\Models
 */
class Agency extends ModelJSONAPI {
    /**#@+
     * @var int ERROR
     */
    const
        ERROR_AGENCY_MAILSENDERCUSTOMIZED_WRONG_TYPE = 2100,
        ERROR_AGENCY_MAILSENDERCUSTOMIZED_WRONG_EMAIL = 2101,
		ERROR_AGENCY_WRONG_ACTIVITYTYPE_FOR_ADVANTAGES = 2102,
		ERROR_AGENCY_WRONG_ADVANTAGEFREQUENCY = 2103,
		ERROR_AGENCY_WRONG_ACTIVITYTYPE_FOR_SCALES = 2104,
		ERROR_AGENCY_DIFFERENT_ACTIVITYTYPE_FOR_SCALE = 2105,
		ERROR_AGENCY_CATEGORY_PACKAGE_CANNOT_BE_SET = 2106,
		ERROR_AGENCY_CATEGORY_VARIABLESALARYBASIS_CANNOT_BE_SET = 2107,
		ERROR_AGENCY_CATEGORY_LOAN_CANNOT_BE_SET = 2108,
		ERROR_AGENCY_EMPLOYEEQUOTA_CANNOT_BE_SET = 2109,
		ERROR_AGENCY_PARTICIPATIONQUOTA_CANNOT_BE_SET = 2110,
		ERROR_AGENCY_AGENCYQUOTA_CANNOT_BE_SET = 2111,
		ERROR_AGENCY_WARNING_CANNOT_BE_SET = 2112;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'agency';

	const MAPPER = [
		'ID_SOCIETE'     => ['name' => 'id', 'type' => self::TYPE_STRING],
		'SOCIETE_RAISON' => ['name' => 'name', 'type' => self::TYPE_STRING],
		'SOCIETE_GROUPE' => ['name' => 'group', 'type' => self::TYPE_STRING],
		'logo'           => ['name' => 'logo', 'type' => self::TYPE_STRING],
		'SOCIETE_TVA' 	 => ['name' => 'vatNumber', 'type' => self::TYPE_STRING],
		'SOCIETE_ADR' 	 => ['name' => 'address', 'type' => self::TYPE_STRING],
		'SOCIETE_CP' 	 => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'SOCIETE_VILLE'  => ['name' => 'town', 'type' => self::TYPE_STRING],
		'SOCIETE_PAYS' 	 => ['name' => 'country', 'type' => self::TYPE_STRING],
		'SOCIETE_ETAT' 	 => ['name' => 'state', 'type' => self::TYPE_BOOLEAN],
		'SOCIETE_EFFECTIF' 	 			=> ['name' => 'staff', 'type' => self::TYPE_INT],
		'SOCIETE_EMAIL' 	 			=> ['name' => 'email1', 'type' => self::TYPE_STRING],
		'SOCIETE_TEL' 	 				=> ['name' => 'phone1', 'type' => self::TYPE_STRING],
		'registrationNumber' 	 		=> ['name' => 'registrationNumber', 'type' => self::TYPE_STRING],
		'SOCIETE_RCS' 	 				=> ['name' => 'registeredOffice', 'type' => self::TYPE_STRING],
		'SOCIETE_NAF' 	 				=> ['name' => 'apeCode', 'type' => self::TYPE_STRING],
		'SOCIETE_STATUT' 	 			=> ['name' => 'legalStatus', 'type' => self::TYPE_STRING],
		'GRPCONF_WEB'                   => ['name' => 'code', 'type' => self::TYPE_STRING],
		'GRPCONF_HASH'                  => ['name' => 'key', 'type' => self::TYPE_STRING],
		'GRPCONF_DEVISE'                => ['name' => 'currency', 'type' => self::TYPE_INT],
		'GRPCONF_CHANGE'                => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'GRPCONF_NBJRSOUVRE'            => ['name' => 'numberOfWorkingDays', 'type' => self::TYPE_FLOAT],
		'GRPCONF_CHARGE'                => ['name' => 'chargeFactor', 'type' => self::TYPE_FLOAT],
		'GRPCONF_TAUXHORAIRE'           => ['name' => 'workUnitRate', 'type' => self::TYPE_FLOAT],
		'GRPCONF_CALENDRIER'            => ['name' => 'calendar', 'type' => self::TYPE_STRING],
		'GRPCONF_DOWNLOADCENTER'        => ['name' => 'downloadCenter', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_INTRANET'              => ['name' => 'intranet', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_VALIDATIONFRAIS'       => ['name' => 'expensesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'GRPCONF_VALIDATIONABSENCES'    => ['name' => 'absencesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'GRPCONF_VALIDATIONTEMPS'       => ['name' => 'timesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'workUnitTypes'     		=> ['name' => 'workUnitTypes', 'type' => self::TYPE_ARRAY],
		'expenseTypes'      		=> ['name' => 'expenseTypes', 'type' => self::TYPE_ARRAY],
		'ratePerKilometerTypes'   	=> ['name' => 'ratePerKilometerTypes', 'type' => self::TYPE_ARRAY],
		'absencesQuotas'    		=> ['name' => 'absencesQuotas', 'type' => self::TYPE_ARRAY],
		'activityExpensesLogo'		=> ['name' => 'activityExpensesLogo', 'type' => self::TYPE_STRING],
		'GRPCONF_MENTIONSTEMPS'		=> ['name' => 'timesLegals', 'type' => self::TYPE_STRING],
		'GRPCONF_MENTIONSFRAIS'		=> ['name' => 'expensesLegals', 'type' => self::TYPE_STRING],
		'GRPCONF_MENTIONSABSENCES'	=> ['name' => 'absencesLegals', 'type' => self::TYPE_STRING],
		'GRPCONF_DATEMAILTEMPS'		=> ['name' => 'timesCreationAndMailingDate', 'type' => self::TYPE_IGNORE, 'serializeCallback' => 'serializeCreationAndMailingDate', 'unserializeCallback' => 'unserializeCreationAndMailingDate'],
		'GRPCONF_DATEMAILFRAIS'		=> ['name' => 'expensesCreationAndMailingDate', 'type' => self::TYPE_IGNORE, 'serializeCallback' => 'serializeCreationAndMailingDate', 'unserializeCallback' => 'unserializeCreationAndMailingDate'],
		'GRPCONF_CTRAUTOFILLEXPENSES'	=> ['name' => 'contractualExpensesAutomaticFilling', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_SHOWDECOMPTE'		=> ['name' => 'allowResourcesToViewAbsencesAccounts', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_ALERTDECOMPTE'		=> ['name' => 'maskExceedingWarningsWithQuotas', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_STARTDECOMPTE'		=> ['name' => 'defaultAbsencesAccountsPeriod', 'type' => self::TYPE_STRING, 'mapper' => AbsencesQuota::ACCOUNTSPERIOD],
		'GRPCONF_CALCULDECOMPTE'	=> ['name' => 'absencesCalculationMethod', 'type' => self::TYPE_STRING, 'mapper' => AbsencesQuota::CALCULATIONMETHOD],
		'projectsTechnicalAssistanceReferenceMask'      => ['name' => 'projectsTechnicalAssistanceReferenceMask', 'type' => self::TYPE_STRING],
		'projectsPackageReferenceMask'         			=> ['name' => 'projectsPackageReferenceMask', 'type' => self::TYPE_STRING],
		'projectsRecruitmentReferenceMask'         		=> ['name' => 'projectsRecruitmentReferenceMask', 'type' => self::TYPE_STRING],
		'projectsProductReferenceMask'         			=> ['name' => 'projectsProductReferenceMask', 'type' => self::TYPE_STRING],
		'GRPCONF_TAUXREFACTURATION'         			=> ['name' => 'projectsRebillingRate', 'type' => self::TYPE_FLOAT, 'serializeCallback' => 'serializeRebillingRate', 'unserializeCallback' => 'unserializeRebillingRate'],
		'GRPCONF_PRJALLOWJALONS'        				=> ['name' => 'allowAlonesMarkers', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_PRJALLOWAVANTAGES'     				=> ['name' => 'allowAdvantagesOnProjects', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_PRJALLOWTEMPSEXCEPTION'				=> ['name' => 'allowExceptionalScalesOnProjects', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_JDATERESSOURCEPLANNER' 				=> ['name' => 'jDay', 'type' => self::TYPE_IGNORE, 'serializeCallback' => 'serializeJDay', 'unserializeCallback' => 'unserializeJDay'],
		'GRPCONF_MASKREFACHAT'							=> ['name' => 'purchasesReferenceMask', 'type' => self::TYPE_STRING],
		'GRPCONF_ACHATCONDREGLEMENT'					=> ['name' => 'purchasesPaymentTerm', 'type' => self::TYPE_INT],
		'GRPCONF_ACHATTYPEPAYMENT'						=> ['name' => 'purchasesPaymentMethod', 'type' => self::TYPE_INT],
		'GRPCONF_ACHATTAUXTVA'							=> ['name' => 'purchasesTaxRate', 'type' => self::TYPE_FLOAT],
		'opportunitiesTechnicalAssistanceReferenceMask' => ['name' => 'opportunitiesTechnicalAssistanceReferenceMask', 'type' => self::TYPE_STRING],
		'opportunitiesPackageReferenceMask'         	=> ['name' => 'opportunitiesPackageReferenceMask', 'type' => self::TYPE_STRING],
		'opportunitiesRecruitmentReferenceMask'         => ['name' => 'opportunitiesRecruitmentReferenceMask', 'type' => self::TYPE_STRING],
		'opportunitiesProductReferenceMask'         	=> ['name' => 'opportunitiesProductReferenceMask', 'type' => self::TYPE_STRING],
		'GRPCONF_DEVISMENTIONS'         				=> ['name' => 'quotationsLegals', 'type' => self::TYPE_STRING],
		'quotationsReferenceMask'        				=> ['name' => 'quotationsReferenceMask', 'type' => self::TYPE_STRING],
		'quotationsStartingNumber'     					=> ['name' => 'quotationsStartingNumber', 'type' => self::TYPE_STRING],
		'GRPCONF_DEVISVALIDITE'							=> ['name' => 'quotationsValidityInDays', 'type' => self::TYPE_INT],
		'GRPCONF_DEVISCONDREGLEMENT' 					=> ['name' => 'quotationsPaymentTerm', 'type' => self::TYPE_INT],
		'GRPCONF_DEVISTAUXTVA'							=> ['name' => 'quotationsTaxRate', 'type' => self::TYPE_FLOAT],
		'GRPCONF_DEVISSHOWTVAIC'						=> ['name' => 'quotationsShowCompanyVATNumberOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_DEVISSHOWNUMBER'						=> ['name' => 'quotationsShowCompanyNumberOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_DEVISSHOWAOREFERENCE'					=> ['name' => 'quotationsShowOpportunityReferenceOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_DEVISSHOWFOOTER'						=> ['name' => 'quotationsShowFooterOnPDF', 'type' => self::TYPE_BOOLEAN],
		'invoicesLogo'						=> ['name' => 'invoicesLogo', 'type' => self::TYPE_STRING],
		'invoiceRecordTypes'				=> ['name' => 'invoiceRecordTypes', 'type' => self::TYPE_ARRAY],
		'banksDetails'						=> ['name' => 'banksDetails', 'type' => self::TYPE_ARRAY],
		'GRPCONF_BDCMENTIONS'				=> ['name' => 'invoicesLegals', 'type' => self::TYPE_STRING],
		'GRPCONF_ETATFIXEFACTURATION'		=> ['name' => 'invoicesLockingStates', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeLockingStates', 'unserializeCallback' => 'unserializeLockingStates'],
		'GRPCONF_AUTOCREATION'				=> ['name' => 'invoicesAutomaticCreation', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_DELTAFACTURATION'			=> ['name' => 'invoicesDeltaCreationInDays', 'type' => self::TYPE_INT],
		'GRPCONF_ETATBEFOREMAILFACTURATION'	=> ['name' => 'invoicesStateForSendingMailToClient', 'type' => self::TYPE_INT],
		'GRPCONF_ETATAFTERMAILFACTURATION'	=> ['name' => 'invoicesStateToSetAfterSendingMailToClient', 'type' => self::TYPE_INT],
		'invoicesMailSenderType'			=> ['name' => 'invoicesMailSenderType', 'type' => self::TYPE_STRING],
		'invoicesMailSenderCustomized'		=> ['name' => 'invoicesMailSenderCustomized', 'type' => self::TYPE_STRING],
		'invoicesReferenceMask'				=> ['name' => 'invoicesReferenceMask', 'type' => self::TYPE_STRING],
		'invoicesStartingNumber'			=> ['name' => 'invoicesStartingNumber', 'type' => self::TYPE_STRING],
		'GRPCONF_BDCCONDREGLEMENT'			=> ['name' => 'ordersPaymentTerm', 'type' => self::TYPE_INT],
		'GRPCONF_BDCTYPEPAYMENT'			=> ['name' => 'ordersPaymentMethod', 'type' => self::TYPE_INT],
		'GRPCONF_BDCTAUXTVA'				=> ['name' => 'ordersTaxRate', 'type' => self::TYPE_FLOAT],
		'GRPCONF_BDCCOPYCOMMENTS'			=> ['name' => 'ordersCopyCommentsOnNewInvoice', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWCOMMENTS'			=> ['name' => 'ordersShowCommentsOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCFACTOR'					=> ['name' => 'ordersShowFactorOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWTVAIC'				=> ['name' => 'ordersShowCompanyVATNumberOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWNUMBER'				=> ['name' => 'ordersShowCompanyNumberOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWRIB'				=> ['name' => 'ordersShowBankDetailsOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWPRJREFERENCE'		=> ['name' => 'ordersShowProjectReferenceOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWINTNAME'			=> ['name' => 'ordersShowResourcesNameOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWTARIFJRS'			=> ['name' => 'ordersShowAverageDailyPriceOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWJRSOUVRES'			=> ['name' => 'ordersShowNumberOfWorkignDaysOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSHOWFOOTER'				=> ['name' => 'ordersShowFooterOnPDF', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCSEPARATETPSFRS'			=> ['name' => 'ordersSeparateActivityExpensesAndPurchases', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_BDCGROUPMISSION'			=> ['name' => 'ordersGroupDeliveries', 'type' => self::TYPE_BOOLEAN],
        'GRPCONF_CTRDUREEHEBDOMADAIRE'		=> ['name' => 'contractsNumberOfHoursPerWeek', 'type' => self::TYPE_FLOAT],
        'GRPCONF_CTRALLOWTEMPSEXCEPTION'	=> ['name' => 'allowExceptionalScalesOnContracts', 'type' => self::TYPE_BOOLEAN],
        'GRPCONF_BAREMESEXCEPTION'          => ['name' => 'exceptionalScaleTypes', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeScales', 'unserializeCallback' => 'unserializeScales'],
        'GRPCONF_AVANTAGESEXCEPTION'        => ['name' => 'advantagePaidExceptionalTypes', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeExceptionalAdvantages', 'unserializeCallback' => 'unserializeExceptionalAdvantages'],
        'advantageTypes'    		        => ['name' => 'advantageTypes', 'type' => self::TYPE_ARRAY]
	];

	/**#@+
	 * Availables tabs
	 * @var string TAB
	 */
	const TAB_INFORMATION = 'information';
	const TAB_ACTIVITYEXPENSES = 'activity-expenses';
	const TAB_BILLING = 'billing';
	const TAB_OPPORTUNITIES = 'oppportunities';
	const TAB_PROJECTS = 'projects';
	const TAB_PURCHASES = 'purchases';
	const TAB_RESOURCES = 'resources';
	/**#@-*/

    /**#@+
     * @var int GRPCONF_MAILFACTURATION
     */
    const
        MAILSENDERTYPE_MAINMANAGER = 'mainManager',
        MAILSENDERTYPE_CONTACT = 'contact',
        MAILSENDERTYPE_CUSTOMIZED = 'customized';
    /**#@-*/

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	public function initRelationships(){
		$this->setRelationships('ID_RIB', 'bankDetails', BankDetails::class);
		$this->setRelationships('ID_FACTOR', 'factor', Company::class);
	}

	/**
	 * @return string
	 */
	public function getLogo(){
		$img_file = Tools::whichImageWithExtensionExist( BM::getRootPath() . '/www/img/_clients/logo/' . $this->code);
		return $img_file ? $img_file : '';
	}

	/**
	 * build some parameters (logo)
	 * @return $this
	 */
	public function calculateData(){
		$this->logo = $this->getLogo();
		return $this;
	}

	public static function serializeRebillingRate($value) {
		return $value >= 100 ? 1 : 100 / (100 - $value);
	}

	public static function unserializeRebillingRate($value) {
		return 100 * ($value - 1) / $value;
	}

	public static function serializeCreationAndMailingDate($value) {
		switch($value) {
			case 'inactive':$value = 0;break;
			case 'lastDayOfMonth':$value = 99;break;
		}
		return $value;
	}

	public static function unserializeCreationAndMailingDate($value) {
		switch($value) {
			case 0:$value = 'inactive';break;
			case 99:$value = 'lastDayOfMonth';break;
		}
		return $value;
	}

	public static function serializeJDay($value) {
		switch($value) {
			case 'actualDay':$value = 0;break;
			case 'lastDayOfMonth':$value = 99;break;
		}
		return $value;
	}

	public static function unserializeJDay($value) {
		switch($value) {
			case 0:$value = 'actualDay';break;
			case 99:$value = 'lastDayOfMonth';break;
		}
		return $value;
	}

    public static function serializeLockingStates($array){
        return Tools::serializeArray($array);
    }

    public static function unserializeLockingStates($string){
        $tabStates = Tools::unserializeArray($string);
        foreach($tabStates as $i => $state) $tabStates[$i] = intval($state);
        return $tabStates;
    }

	public static function serializeValidations($data) {
		return Validations::serializeValidationWorkflow($data);
	}

	public static function unserializeValidations($string) {
		return Validations::unserializeValidationWorkflow( $string );
	}

	public static function serializeExceptionalAdvantages($array){
		return Tools::serializeDoubleArray(array_map(function($at){
			/** @var AdvantagePaidExceptionalType $at */
			return [$at->workUnitType->reference, $at->advantageType->reference];
		}, $array));
	}

	public static function unserializeExceptionalAdvantages($string){
		$advantages =  [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row){
			$advantages[] = new AdvantagePaidExceptionalType([
				'workUnitType' => new WorkUnitType(['reference' => $row[0]]),
				'advantageType' => new AdvantageType(['reference' => $row[1]])
			]);
		}

		return $advantages;
	}

	public static function serializeScales($array){
		return Tools::serializeDoubleArray(array_map(function($est){
			/** @var ExceptionalScaleType $est */
			$refs = Tools::getFieldsToArray($est->workUnitTypes, 'reference');
			return [$est->reference, $est->name, implode('_', $refs), $est->default ? 1 : 0];
		}, $array));
	}

	public static function unserializeScales($string){
		$scales = [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row) {
			$references = explode('_', $row[2]);
			$workUnitTypes = [];
			foreach($references as $ref) {
				$workUnitTypes[] = new WorkUnitType(['reference' => $ref]);
			}
			$scales[] = new ExceptionalScaleType([
				'reference' => $row[0],
				'name' => $row[1],
				'workUnitTypes' => $workUnitTypes,
				'default' => $row[3] == 1 ? true : false,
				'exceptionalRules' => []
			]);
		}
		return $scales;
	}
}
