<?php
/**
 * alonemarker.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class AloneMarker
 * @property int id
 * @property string LOT_TITRE
 * @package BoondManager\Models\MySQL\RowObject
 */
class AloneMarker extends \Wish\Models\ModelJSON {

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_JALON'          => ['name' => 'id', 'type' => self::TYPE_INT],
			'JALON_TITRE'       => ['name' => 'title', 'type' => self::TYPE_STRING],
			'JALON_VALUE'       => ['name' => 'progressRate', 'type' => self::TYPE_FLOAT],
			'JALON_DATE' 		=> ['name' => 'date', 'type' => self::TYPE_DATE],
			'RESOURCE'    		=> ['name' => 'resource', 'type' => self::TYPE_OBJECT]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->toArray();
	}
}
