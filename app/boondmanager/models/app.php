<?php
/**
 * app.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class App
 * @property string IAPI_TOKEN
 * @property int id
 * @property string name
 * @property string key
 * @property string title
 * @property string description
 * @property string version
 * @property string category
 * @property string token
 * @property string expirationDate
 * @property string creationDate
 * @property string visibility
 * @property array hostsAllowed
 * @property array apiAllowed
 *
 * @package BoondManager\Models\MySQL\RowObject
 */
class App extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'app';

	/**#@+
	 * App validation
	 * @var int
	 */
	const VALIDATION_ON = 1;
	const VALIDATION_OFF = 0;
	/**#@-*/

	/**
	 *
	 */
	public function initRelationships() {

	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return [
			'ID_MAINAPI'            => ['name' => 'id', 'type' => self::TYPE_INT],
			'MAINAPI_NOM'           => ['name' => 'name', 'type' => self::TYPE_STRING],
			'MAINAPI_KEY'           => ['name' => 'key', 'type' => self::TYPE_STRING],
			'MAINAPI_TITRE'         => ['name' => 'title', 'type' => self::TYPE_STRING],
			'MAINAPI_DESCRIPTION'   => ['name' => 'description', 'type' => self::TYPE_STRING],
			'MAINAPI_CATEGORIE'     => ['name' => 'category', 'type' => self::TYPE_STRING, 'mapper' => self::CATEGORY_MAPPER],
			'MAINAPI_VERSION'       => ['name' => 'version', 'type' => self::TYPE_STRING],
			'MAINAPI_HOSTSALLOWED'  => ['name' => 'hostsAllowed', 'type' => self::TYPE_ARRAY],
			'MAINAPI_APIALLOWED'    => ['name' => 'apiAllowed', 'type' => self::TYPE_ARRAY],
			'IAPI_VISIBILITY'       => ['name' => 'visibility', 'type' => self::TYPE_STRING, 'mapper' => self::VISIBILITY_MAPPER],
			'IAPI_DATEINSTALLATION' => ['name' => 'creationDate', 'type' => self::TYPE_DATE],
			'IAPI_EXPIRATION'       => ['name' => 'expirationDate', 'type' => self::TYPE_DATE],
			'IAPI_TOKEN'            => ['name' => 'token', 'type' => self::TYPE_STRING],
		];
	}

	/**
	 * @return string
	 */
	public function getReference() {
		return 'APP'.$this->id;
	}

	const
		CATEGORY_CERTIFICATION = 'certification',
		CATEGORY_TEMPLATES = 'templates',
		CATEGORY_MAIL = 'mail',
		CATEGORY_CALENDAR = 'calendar',
		CATEGORY_VIEWER = 'viewer',
		CATEGORY_EMAILING = 'emailing',
		CATEGORY_MAILCALENDAR = 'mailAndCalendar',
		CATEGORY_GED = 'ged',
		CATEGORY_OTHER = 'other';

	/**#@+
	 * @var int MAINAPI_CATEGORIE
	 */
	const CATEGORY_MAPPER = [
		0 => self::CATEGORY_OTHER,
		1 => self::CATEGORY_MAIL,
		2 => self::CATEGORY_CALENDAR,
		3 => self::CATEGORY_GED,
		4 => self::CATEGORY_CERTIFICATION,
		5 => self::CATEGORY_VIEWER,
		6 => self::CATEGORY_EMAILING,
		7 => self::CATEGORY_TEMPLATES,
		8 => self::CATEGORY_MAILCALENDAR,
	];
	/**#@-*/

	/**#@+
	 * @var int IAPI_VISIBILITY
	 */

	const
		VISIBILITY_ALLOWEDMANAGERS = 'allowedManagers',
		VISIBILITY_ALLMANAGERS = 'allManagers',
		VISIBILITY_ALLOWEDMANAGERSANDACTIVESRESOURCES = 'allowedManagersAndActivesResources',
		VISIBILITY_ALLMANAGERSANDRESOURCES = 'allManagersAndResources';

	const VISIBILITY_MAPPER = [
		0 => self::VISIBILITY_ALLOWEDMANAGERS,
		1 => self::VISIBILITY_ALLMANAGERS,
		2 => self::VISIBILITY_ALLOWEDMANAGERSANDACTIVESRESOURCES,
		3 => self::VISIBILITY_ALLMANAGERSANDRESOURCES
	];
	/**#@-*/
}
