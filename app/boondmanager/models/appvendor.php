<?php
/**
 * appvendor.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Models;

/**
 * Class AppVendor
 * @package BoondManager\Models\MySQL\RowObject
 */
class AppVendor extends App{}
