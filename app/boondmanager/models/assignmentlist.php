<?php
/**
 * assigmentlist.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\JSONApiObjectInterface;
use BoondManager\Services\CurrentUser;

class AssignmentList implements JSONApiObjectInterface{
	/**
	 * @var CurrentUser
	 */
	private $user;

	/**
	 * @var Account[]
	 */
	private $managers, $hrManagers, $agencies, $poles, $influencers;

	/**
	 * AssignmentList constructor.
	 * @param CurrentUser $user
	 */
	public function __construct(CurrentUser $user)
	{
		$this->user = $user;

		$this->managers = $this->user->getManagerForReassignment();
		$this->hrManagers = $this->user->getHRForReassignment();
		$this->agencies = $this->user->getAgenciesForReassignment();
		$this->poles = $this->user->getPolesForReassignment();
		$this->influencers = $this->user->getInfluencersForReassignment();
	}

	/**
	 * @return Account[]
	 */
	public function getManagers(){
		return $this->managers;
	}

	/**
	 * @return Account[]
	 */
	public function getHRManager(){
		return $this->hrManagers;
	}

	/**
	 * @return Agency[]
	 */
	public function getAgencies(){
		return $this->agencies;
	}

	/**
	 * @return Pole[]
	 */
	public function getPoles(){
		return $this->poles;
	}

	/**
	 * @return Employee[]
	 */
	public function getInfluencers(){
		return $this->influencers;
	}

	/**
	 * return the object ID
	 * @return string
	 */
	public function getID()
	{
		return strval($this->user->getUserId());
	}

	/**
	 * set the object ID
	 * @param string $id
	 * @return mixed
	 */
	public function setID($id)
	{
		$this->user->getAccount()->userId = $id;
	}

	/**
	 * return the object type
	 * @return string
	 */
	public function getType()
	{
		return 'assignment';
	}

	/**
	 * return the object attributes
	 * @return array
	 */
	public function getAttributes()
	{
		return [];
	}

	/**
	 * return the object relationships
	 * @return \Wish\Models\JSONApiObjectInterface[]
	 */
	public function getRelationships()
	{
		return [
			'mainManagers' => $this->getManagers(),
			'hrManagers' => $this->getHRManager(),
			'agencies' => $this->getAgencies(),
			'poles' => $this->getPoles(),
			'influencers' => $this->getInfluencers()
		];
	}
}
