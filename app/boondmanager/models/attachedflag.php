<?php
/**
 * attachedflag.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class AttachedFlag
 * @property string id
 * @property Flag flag
 * @property Company|Opportunity|Project|Order|Product|Purchase|Action|Employee|Candidate|Positioning|Contact|Invoice dependsOn
 *
 * @package BoondManager\Models
 */
class AttachedFlag extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'attachedflag';

	/**#@+
	 * @var int Depends on type
	 */
	const
		TYPE_COMPANY = 0,
		TYPE_OPPORTUNITY = 1,
		TYPE_PROJECT = 2,
		TYPE_ORDER = 3,
		TYPE_PRODUCT = 4,
		TYPE_PURCHASE = 5,
		TYPE_ACTION = 6,
		TYPE_RESOURCE = 7,
		TYPE_CANDIDATE = 8,
		TYPE_POSITIONING = 9,
		TYPE_CONTACT = 10,
		TYPE_INVOICE = 11;
	/**#@-*/

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_ATTACHEDFLAGS_WRONG_QUERY_PARAMETERS = 4500;
	/**#@-*/

	protected function initRelationships(){
		$this->setRelationships('ID_USERFLAG', 'flag', Flag::class);

		$this->setDynamicTypedRelationship('ID_PARENT', 'dependsOn', function($object){
			$class = null;
			switch($object['FLAG_TYPE']) {
				case self::TYPE_COMPANY: $class = Company::class;break;
				case self::TYPE_OPPORTUNITY: $class = Opportunity::class;break;
				case self::TYPE_PROJECT: $class = Project::class;break;
				case self::TYPE_ORDER: $class = Order::class;break;
				case self::TYPE_PRODUCT: $class = Product::class;break;
				case self::TYPE_PURCHASE: $class = Purchase::class;break;
				case self::TYPE_ACTION: $class = Action::class;break;
				case self::TYPE_RESOURCE: $class = Employee::class;break;
				case self::TYPE_CANDIDATE: $class = Candidate::class;break;
				case self::TYPE_POSITIONING: $class = Positioning::class;break;
				case self::TYPE_CONTACT: $class = Contact::class;break;
				case self::TYPE_INVOICE: $class = Invoice::class;break;
			}
			return $class;
		});
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_FLAG' => ['name' => 'id', 'type' => self::TYPE_STRING]
		];
	}
}
