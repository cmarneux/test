<?php
/**
 * bankdetails.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class BankDetails
 * @property string id
 * @property string description
 * @property string iban
 * @property string bic
 * @package BoondManager\Models
 */
class BankDetails extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'bankdetails';

	const MAPPER = [
		'ID_RIB'       		=> ['name' => 'id', 'type' => self::TYPE_STRING],
		'RIB_DESCRIPTION'   => ['name' => 'description', 'type' => self::TYPE_STRING],
		'RIB_IBAN' 			=> ['name' => 'iban', 'type' => self::TYPE_STRING],
		'RIB_BIC'         	=> ['name' => 'bic', 'type' => self::TYPE_STRING]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
