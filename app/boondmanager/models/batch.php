<?php
/**
 * batch.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Batch
 * @property int $id
 * @property string $title
 */
class Batch extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'batch';

	const MAPPER = [
		'ID_LOT'    => ['name' => 'id', 'type' => self::TYPE_STRING],
		'LOT_TITRE' => ['name' => 'title', 'type' => self::TYPE_STRING],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
