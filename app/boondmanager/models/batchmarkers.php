<?php
/**
 * batchmarkers.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class BatchMarkers
 * @property int id
 * @property string title
 * @property array markers
 * @property array resources
 * @package BoondManager\Models
 */
class BatchMarkers extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return [
			'ID_LOT'          => ['name' => 'id', 'type' => self::TYPE_STRING],
			'LOT_TITRE'       => ['name' => 'title', 'type' => self::TYPE_STRING],
			'LOT_VALUE'       => ['name' => 'distributionRate', 'type' => self::TYPE_FLOAT],
			'TIMESPRODUCTION' => ['name' => 'timesProduction', 'type' => self::TYPE_FLOAT],
			'BALANCE'         => ['name' => 'balance', 'type' => self::TYPE_FLOAT],
			'REMAINSTOBEDONE'  => ['name' => 'remainsToBeDone', 'type' => self::TYPE_FLOAT],
			'DIFFERENCE'      => ['name' => 'difference', 'type' => self::TYPE_FLOAT],
			'DURATIONFORECAST'=> ['name' => 'durationForecast', 'type' => self::TYPE_FLOAT],
			'PROGRESSRATE'	  => ['name' => 'progressRate', 'type' => self::TYPE_FLOAT],
			'TURNOVERSIGNEDDISTRIBUTEDEXCLUDINGTAX'		=> ['name' => 'turnoverSignedDistributedExcludingTax', 'type' => self::TYPE_FLOAT],
			'TURNOVERPROGRESSDISTRIBUTEDEXCLUDINGTAX'	=> ['name' => 'turnoverProgressDistributedExcludingTax', 'type' => self::TYPE_FLOAT],
			'MARKERS'         => ['name' => 'markers', 'type' => self::TYPE_ARRAY],
			'RESOURCES'       => ['name' => 'resources', 'type' => self::TYPE_ARRAY]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->toArray();
	}
}
