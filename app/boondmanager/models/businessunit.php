<?php
/**
 * bu.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class BU
 * @property int $id
 * @property string $name
 * @property Account[] $managers
 * @property Account[] $excludedManagersInSearch
 *
 * @package BoondManager\Models\MySQL\RowObject
 */
class BusinessUnit extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'businessunit';

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_BUSINESSUNIT_WRONG_EXCLUDEMANAGERS = 4600;
	/**#@-*/

	const MAPPER = [
		'ID_BUSINESSUNIT'   => ['name' => 'id', 'type' => self::TYPE_INT],
		'BU_NAME'           => ['name' => 'name', 'type' => self::TYPE_STRING]
	];

	/**
	 *
	 */
	public function init() {
		$this->managers         = [];
		$this->excludedManagersInSearch = [];
	}

	/**
	 *
	 */
	protected function initRelationships() {
		$this->setGroupedRelationships('MANAGERS', 'managers');
		$this->setGroupedRelationships('excludedManagersInSearch', 'excludedManagersInSearch');
	}

	/**
	 * true if the bu contains all its managers
	 * @return bool
	 */
	public function areManagersLoaded(){
		return count($this->managers) > 0;
	}

	/**
	 * @return array|Account[]
	 */
	public function getSearchableManagers(){
		return array_filter($this->managers, function ($manager) {
			foreach($this->excludedManagersInSearch as $m){
				if($m->id == $manager->id) return false;
			}
			return true;
		});
	}

	/**
	 * lookup for a manager in the bu
	 * @param $idProfil
	 * @param null $idUser
	 * @return bool
	 * @throws \Exception
	 */
	public function hasManager($idProfil, $idUser = null){
		if($idUser) throw new \Exception('please use ID_PROFIL INSTEAD');
		foreach ($this->managers as $manager){
			if($idProfil && $manager->id == $idProfil) return true;

		}
		return false;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}
}
