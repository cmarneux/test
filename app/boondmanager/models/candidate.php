<?php
/**
 * candidate.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasHrManagerInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;
use Wish\Models\PublicFieldsInterface;
use Wish\Models\PublicFieldsTrait;

/**
 * Class Candidate
 * @property int $id
 * @property int ID_DT
 * @property string $firstName
 * @property string $lastName
 * @property int $typeOf
 * @property int $state
 * @property string $mobilityAreas
 * @property int NB_CV
 * @property boolean $visibility
 * @property Account mainManager
 * @property Account hrManager
 * @property Resume resume
 * @property OriginOrSource $source
 * @property mixed availability
 * @property int PARAM_TYPEDISPO
 * @property string PARAM_DATEDISPO
 * @property DesiredSalary desiredSalary
 * @property array activityAreas
 * @property array expertiseAreas
 * @property array $tools
 * @property float $currencyAgency
 * @property float $exchangeRateAgency
 * @property float $currency
 * @property float $exchangeRate
 * @property int $desiredContract
 * @property boolean isEntityDeleting
 * @property boolean isEntityUpdating
 * @property Company company
 * @property Contact contact
 * @property Contract contract
 * @property Notation[] evaluations
 * @property Reference[] references
 * @property File[] files
 * @property WebSocial[] socialNetworks
 * @property Employee resource
 * @property Contact providerContact
 * @property Company providerCompany
 *
 * @package BoondManager\Models
 */
class Candidate extends ModelJSONAPI implements HasAgencyInterface, HasPoleInterface, HasManagerInterface, HasHrManagerInterface{
	use HasManagerTrait, HasPoleTrait, HasAgencyTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'candidate';

	const REF_PREFIX = 'CAND';

	/**#@+
	 * Availables tabs
	 * @var integer TAB
	 */
	const
		TAB_DEFAULT = 0,
		TAB_INFORMATION = 'information',
		TAB_ADMINISTRATIVE = 'administrative',
		TAB_TD = 'technicalData',
		TAB_ACTIONS = 'actions',
		TAB_FLAGS = 'flags',
		TAB_POSITIONINGS = 'positionings';
	/**#@-*/

	/**#@+
	 * @var string Type
	 */
	const TYPE_CANDIDATE = 9;

	const STATE_HIRED = 3;

	const MAPPER = [
		'ID_PROFIL'            => ['name' => 'id', 'type' => self::TYPE_INT],
		'PARAM_CHANGE'         => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'PARAM_CHANGEAGENCE'   => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'PARAM_COMMENTAIRE'    => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'PARAM_COMMENTAIRE2'   => ['name' => 'administrativeComments', 'type' => self::TYPE_STRING],
		'PARAM_CONTRAT'        => ['name' => 'desiredContract', 'type' => self::TYPE_INT],
		'PARAM_DEVISE'         => ['name' => 'currency', 'type' => self::TYPE_INT],
		'PARAM_DEVISEAGENCE'   => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'PARAM_DATEDISPO'      => ['name' => 'availability', 'type' => self::TYPE_IGNORE],
		'PARAM_MOBILITE'       => ['name' => 'mobilityAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'PARAM_TARIF1'         => ['name' => 'actualSalary', 'type' => self::TYPE_FLOAT],
		'PARAM_TYPEDISPO'      => ['name' => 'availabilityDelay', 'type' => self::TYPE_INT],
		'PROFIL_ADR'           => ['name' => 'address', 'type' => self::TYPE_STRING],
		'PROFIL_CIVILITE'      => ['name' => 'civility', 'type' => self::TYPE_INT],
		'PROFIL_CP'            => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'PROFIL_DATE'          => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'PROFIL_DATENAISSANCE' => ['name' => 'dateOfBirth', 'type' => self::TYPE_DATE],
		'PROFIL_DATEUPDATE'    => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'PROFIL_EMAIL'         => ['name' => 'email1', 'type' => self::TYPE_STRING],
		'PROFIL_EMAIL2'        => ['name' => 'email2', 'type' => self::TYPE_STRING],
		'PROFIL_EMAIL3'        => ['name' => 'email3', 'type' => self::TYPE_STRING],
		'PROFIL_ETAT'          => ['name' => 'state', 'type' => self::TYPE_INT],
		'PROFIL_LIEUNAISSANCE' => ['name' => 'placeOfBirth', 'type' => self::TYPE_STRING],
		'PROFIL_NATIONALITE'   => ['name' => 'nationality', 'type' => self::TYPE_STRING],
		'PROFIL_NOM'           => ['name' => 'lastName', 'type' => self::TYPE_STRING],
		'PROFIL_NUMSECU'       => ['name' => 'healthCareNumber', 'type' => self::TYPE_STRING],
		'PROFIL_PAYS'          => ['name' => 'country', 'type' => self::TYPE_STRING],
		'PROFIL_PRENOM'        => ['name' => 'firstName', 'type' => self::TYPE_STRING],
		'PROFIL_REFERENCE'     => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'PROFIL_SITUATION'     => ['name' => 'situation', 'type' => self::TYPE_INT],
		'PROFIL_STATUT'        => ['name' => 'globalEvaluation', 'type' => self::TYPE_STRING],
		'PROFIL_TEL1'          => ['name' => 'phone1', 'type' => self::TYPE_STRING],
		'PROFIL_TEL2'          => ['name' => 'phone2', 'type' => self::TYPE_STRING],
		'PROFIL_TEL3'          => ['name' => 'phone3', 'type' => self::TYPE_STRING],
		'PROFIL_TYPE'          => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'PROFIL_FAX'           => ['name' => 'fax', 'type' => self::TYPE_STRING],
		'PROFIL_VILLE'         => ['name' => 'town', 'type' => self::TYPE_STRING],
		'PROFIL_VISIBILITE'    => ['name' => 'visibility', 'type' => self::TYPE_BOOLEAN],
		'COMP_COMPETENCE'      => ['name' => 'skills', 'type' => self::TYPE_STRING],
		'COMP_APPLICATIONS'    => ['name' => 'activityAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'COMP_INTERVENTIONS'   => ['name' => 'expertiseAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'DT_LANGUES'           => ['name' => 'languages', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeLanguages', 'unserializeCallback' => 'unserializeLanguages'],
		'DT_OUTILS'            => ['name' => 'tools', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeTools', 'unserializeCallback' => 'unserializeTools'],
		'DT_DIPLOMES'          => ['name' => 'diplomas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'DT_EXPERIENCE'        => ['name' => 'experience', 'type' => self::TYPE_INT],
		'DT_FORMATION'         => ['name' => 'training', 'type' => self::TYPE_STRING],
		'DT_TITRE'             => ['name' => 'title', 'type' => self::TYPE_STRING],
		'NB_CV'                => ['name' => 'numberOfResumes', 'type' => self::TYPE_INT],
		'NB_POS'               => ['name' => 'numberOfActivePositionings', 'type' => self::TYPE_INT],
		'WEBSOCIALS'           => ['name' => 'socialNetworks', 'type' => self::TYPE_ARRAY],
		'NOTATIONS'            => ['name' => 'evaluations', 'type' => self::TYPE_ARRAY],
		'source'               => ['name' => 'source', 'type' => self::TYPE_OBJECT],
		'desiredSalary'        => ['name' => 'desiredSalary', 'type' => self::TYPE_OBJECT],
		'isEntityUpdating'     => ['name' => 'isEntityUpdating', 'type' => self::TYPE_BOOLEAN],
		'isEntityDeleting'     => ['name' => 'isEntityDeleting', 'type' => self::TYPE_BOOLEAN],
		'REFERENCES'           => ['name' => 'references', 'type' => self::TYPE_ARRAY],
		'CURRENT_UPDATE'       => ['name' => 'isEntityUpdating', 'type' => self::TYPE_BOOLEAN],
		'CURRENT_DELETE'       => ['name' => 'isEntityDeleting', 'type' => self::TYPE_BOOLEAN],
		'ID_DT'                => ['name' => 'ID_DT', 'type' => self::TYPE_INT], // should be private
	];

	/**
	 * @return array
	 */
	public static function getAllTabs()
	{
		return [self::TAB_INFORMATION, self::TAB_ADMINISTRATIVE, self::TAB_TD, self::TAB_ACTIONS, self::TAB_POSITIONINGS];
	}

	/**
	 * @return bool
	 */
	public function isVisible(){
		return $this->visibility==1;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	public function getFullName() {
		return $this->lastName . ' ' . $this->firstName;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_PROFIL_RESPMANAGER', 'mainManager', Account::class);
		$this->setRelationships('ID_PROFIL_RESPRH', 'hrManager', Account::class);
		$this->setRelationships('ID_CV', 'resume', Resume::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);
		$this->setRelationships('ID_PROFILCONSULTANT', 'resource', Employee::class);
		$this->setRelationships('ID_PROFILCONSULTANT', 'contract', Contract::class);

		$this->setRelationships('ID_CRMCONTACT', 'providerContact', Contact::class);
		$this->setRelationships('ID_CRMSOCIETE', 'providerCompany', Company::class);

		$this->setGroupedRelationships('FILES', 'files');
		$this->setGroupedRelationships('CVs', 'resumes');
	}

	/**
	 * @return int
	 */
	public function getHrManagerID()
	{
		return ($this->hrManager) ? $this->hrManager->id : null;
	}

	/**
	 * @param $value
	 * @return string
	 */
	public static function serializeLanguages($value){
		return implode('#', array_map(function($value){
			/** @var Language $value */
			return $value->language.'|'.$value->level;
		}, $value));
	}

	/**
	 * @param $value
	 * @return array
	 */
	public static function unserializeLanguages($value){
		return array_map(function($value){
			list($language, $level) = explode('|', $value);
			return new Language([
				'language' => $language,
				'level' => $level
			]);
		},explode('#', $value));
	}

	/**
	 * @param $value
	 * @return string
	 */
	public static function serializeTools($value){
		return implode('#', array_map(function($value){
			/** @var Tool $value */
			return $value->tool.'|'.$value->level;
		}, $value));
	}

	/**
	 * @param $value
	 * @return array
	 */
	public static function unserializeTools($value){
		return array_map(function($value){
			list($tool, $level) = explode('|', $value);
			return new Tool([
				'tool' => $tool,
				'level' => $level
			]);
		},explode('#', $value));
	}
}

/**
 * Class DesiredSalary
 * @package BoondManager\Models
 */
class DesiredSalary implements PublicFieldsInterface{
	use PublicFieldsTrait;

	/**
	 * @var int
	 */
	public $min, $max;

	/**
	 * DesiredSalary constructor.
	 * @param int $min
	 * @param int $max
	 */
	public function __construct($min = 0, $max = 0)
	{
		$this->min = $min;
		$this->max = $max;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'min' => ['name' => 'min', 'type' => self::TYPE_FLOAT],
			'max' => ['name' => 'max', 'type' => self::TYPE_FLOAT],
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return [
			'min' => $this->min,
			'max' => $this->max
		];
	}
}
