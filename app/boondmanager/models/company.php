<?php
/**
 * company.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Company
 * @property int id
 * @property string name
 * @property string $nic
 * @property string $siren
 * @property string $country
 * @property string $vatNumber
 * @property string[] $departments
 * @property string[] $expertiseAreas
 * @property string registrationNumber
 * @property ContactDetails billingDetails
 * @property Account mainManager
 * @property Account[] $influencers
 * @property Document[] files
 * @property OriginOrSource $origin
 * @property Company parentCompany
 * @property Company[] subsidiaries
 * @property WebSocial[] socialNetworks
 * @property ExceptionalScaleType[] exceptionalScaleTypes
 */
class Company extends ModelJSONAPI  implements HasManagerInterface, HasPoleInterface, HasAgencyInterface{
	use HasManagerTrait, HasAgencyTrait, HasPoleTrait;

	const MAPPER = [
		'ID_CRMSOCIETE'      => ['name' => 'id', 'type' => self::TYPE_STRING],
		'CSOC_DATEUPDATE'    => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'CSOC_DATE'          => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'CSOC_SOCIETE'       => ['name' => 'name', 'type' => self::TYPE_STRING],
		'CSOC_INTERVENTION'  => ['name' => 'expertiseArea', 'type' => self::TYPE_STRING],
		'CSOC_TYPE'          => ['name' => 'state', 'type' => self::TYPE_INT],
		'CSOC_METIERS'       => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'CSOC_WEB'           => ['name' => 'website', 'type' => self::TYPE_STRING],
		'CSOC_TEL'           => ['name' => 'phone1', 'type' => self::TYPE_STRING],
		'CSOC_ADR'           => ['name' => 'address', 'type' => self::TYPE_STRING],
		'CSOC_FAX'           => ['name' => 'fax', 'type' => self::TYPE_STRING],
		'CSOC_CP'            => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'CSOC_VILLE'         => ['name' => 'town', 'type' => self::TYPE_STRING],
		'CSOC_PAYS'          => ['name' => 'country', 'type' => self::TYPE_STRING],
		'CSOC_NUMERO'        => ['name' => 'number', 'type' => self::TYPE_STRING],
		'CSOC_EFFECTIF'      => ['name' => 'staff', 'type' => self::TYPE_INT],
		'CSOC_SERVICES'      => ['name' => 'departments', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'CSOC_TVA'           => ['name' => 'vatNumber', 'type' => self::TYPE_STRING],
		'CSOC_STATUT'        => ['name' => 'legalStatus', 'type' => self::TYPE_STRING],
		'CSOC_NAF'           => ['name' => 'apeCode', 'type' => self::TYPE_STRING],
		'CSOC_RCS'           => ['name' => 'registeredOffice', 'type' => self::TYPE_STRING],
		'CSOC_NIC'           => ['name' => 'nic', 'type' => self::TYPE_STRING],
		'CSOC_SIREN'         => ['name' => 'siren', 'type' => self::TYPE_STRING],
		'FACT_COORDONNEES'   => ['name' => 'billingDetails', 'type' => self::TYPE_ARRAY],
		'WEBSOCIALS'         => ['name' => 'socialNetworks', 'type' => self::TYPE_ARRAY],
		'CSOC_BAREMESEXCEPTION' => ['name' => 'exceptionalScaleTypes', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeScales', 'unserializeCallback' => 'unserializeScales'],
		'origin'             => ['name' => 'origin', 'type' => self::TYPE_OBJECT],
		'registrationNumber' => ['name' => 'registrationNumber', 'type' => self::TYPE_STRING],
		'CURRENT_UPDATE'     => ['name' => 'isEntityUpdating', 'type' => self::TYPE_BOOLEAN],
		'CURRENT_DELETE'     => ['name' => 'isEntityDeleting', 'type' => self::TYPE_BOOLEAN]
	];

	const REF_PREFIX = 'CSOC';

	/**
	 * @var string
	 */
	protected static $_jsonType = 'company';

	/**#@+
	 * @var string Tab
	 */
	const TAB_INFORMATION = 'information';
	const TAB_ACTIONS = 'actions';
	const TAB_CONTACTS = 'contacts';
	const TAB_OPPORTUNITIES = 'opportunities';
	const TAB_PROJECTS = 'projects';
	const TAB_PURCHASES = 'purchases';
	const TAB_INVOICES = 'invoices';
	const TAB_SETTING = 'setting';
	const TAB_DEFAULT = 0;

	/**
	 * Company constructor.
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		parent::__construct($data);
	}

	/**
	 * @param $updateKey
	 * @param null $value
	 * @param null $oldvalue
	 * @deprecated
	 */
	protected function onUpdatedData($updateKey, $value = null, $oldvalue = null){
		switch($updateKey){
			case 'nic':
			case 'siren':
				$this->registrationNumber  = $this->siren.$this->nic;
				break;
			case 'registrationNumber':
				if(is_string($this->registrationNumber) && strlen($this->registrationNumber) == 15){
					$this->siren = substr($this->registrationNumber, 0, 9);
					$this->nic   = substr($this->registrationNumber, 9, 5);
				}
				break;
		}
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition(){
		return self::MAPPER;
	}

	/**
	 * @return array
	 */
	public static function getAllTabs()
	{
		return [
			self::TAB_INFORMATION, self::TAB_CONTACTS, self::TAB_ACTIONS, self::TAB_OPPORTUNITIES, self::TAB_PROJECTS,
			self::TAB_PURCHASES, self::TAB_INVOICES
		];
	}

	/**
	 * @param Company $company
	 * @return $this
	 */
	public function mergeCompany(Company $company){
		foreach($company->toArray() as $key => $data) {
			$this->$key = $data;
		}
		return $this;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);
		$this->setRelationships('ID_RESPSOC', 'parentCompany', Company::class);

		$this->setGroupedRelationships('INFLUENCERS', 'influencers');
		$this->setGroupedRelationships('DOCUMENTS', 'files');
	}

	public static function serializeScales($array){
		return Tools::serializeDoubleArray(array_map(function($est){
			/** @var ExceptionalScaleType $est */
			$refs = Tools::getFieldsToArray($est->workUnitTypes, 'reference');
			return [$est->reference, $est->name, implode('_', $refs), $est->agency->id];
		}, $array));
	}

	public static function unserializeScales($string){
		$scales = [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row) {
			$references = explode('_', $row[2]);
			$workUnitTypes = [];
			foreach($references as $ref) {
				$workUnitTypes[] = new WorkUnitType(['reference' => $ref]);
			}
			$scales[] = new ExceptionalScaleType([
				'reference' => $row[0],
				'name' => $row[1],
				'workUnitTypes' => $workUnitTypes,
				'exceptionalRules' => [],
				'agency' => new Agency( ['id' => $row[3]] )
			]);
		}
		return $scales;
	}
}
