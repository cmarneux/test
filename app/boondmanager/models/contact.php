<?php
/**
 * contact.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Contact
 * @property int id
 * @property Account mainManager
 * @property string lastName
 * @property string firstName
 * @property Company company
 * @property OriginOrSource $origin
 * @property Action lastAction
 * @property string[] $activityAreas
 * @property string[] $tools
 * @property Account[] $influencers
 * @property WebSocial[] socialNetworks
 * @property bool canReadContact
 * @property bool canWriteContact
 */
class Contact extends ModelJSONAPI  implements HasManagerInterface, HasPoleInterface, HasAgencyInterface{
	use HasManagerTrait, HasAgencyTrait, HasPoleTrait;

	const REF_PREFIX = 'CCON';

	const MAPPER = [
		'ID_CRMCONTACT'     => ['name' => 'id', 'type' => self::TYPE_STRING],
		'CCON_ADR'          => ['name' => 'address', 'type' => self::TYPE_STRING],
		'CCON_APPLICATIONS' => ['name' => 'activityAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'CCON_CIVILITE'     => ['name' => 'civility', 'type' => self::TYPE_INT],
		'CCON_COMMENTAIRE'  => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'CCON_CP'           => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'CCON_DATE'         => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'CCON_DATEUPDATE'   => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'CCON_EMAIL'        => ['name' => 'email1', 'type' => self::TYPE_STRING],
		'CCON_EMAIL2'       => ['name' => 'email2', 'type' => self::TYPE_STRING],
		'CCON_EMAIL3'       => ['name' => 'email3', 'type' => self::TYPE_STRING],
		'CCON_FAX'          => ['name' => 'fax', 'type' => self::TYPE_STRING],
		'CCON_FONCTION'     => ['name' => 'function', 'type' => self::TYPE_STRING],
		'CCON_NOM'          => ['name' => 'lastName', 'type' => self::TYPE_STRING],
		'CCON_OUTILS'       => ['name' => 'tools', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'CCON_PAYS'         => ['name' => 'country', 'type' => self::TYPE_STRING],
		'CCON_PRENOM'       => ['name' => 'firstName', 'type' => self::TYPE_STRING],
		'CCON_SERVICE'      => ['name' => 'department', 'type' => self::TYPE_STRING],
		'CCON_TEL1'         => ['name' => 'phone1', 'type' => self::TYPE_STRING],
		'CCON_TEL2'         => ['name' => 'phone2', 'type' => self::TYPE_STRING],
		'CCON_TYPE'         => ['name' => 'state', 'type' => self::TYPE_INT],
		'CCON_VILLE'        => ['name' => 'town', 'type' => self::TYPE_STRING],
		'WEBSOCIALS'        => ['name' => 'socialNetworks', 'type' => self::TYPE_ARRAY],
		'origin'            => ['name' => 'origin', 'type' => self::TYPE_OBJECT],
		'CURRENT_UPDATE'    => ['name' => 'isEntityUpdating', 'type' => self::TYPE_BOOLEAN],
		'CURRENT_DELETE'    => ['name' => 'isEntityDeleting', 'type' => self::TYPE_BOOLEAN],
		'canReadContact'    => ['name' => 'canReadContact', 'type' => self::TYPE_BOOLEAN],
		'canWriteContact'   => ['name' => 'canWriteContact', 'type' => self::TYPE_BOOLEAN]
	];

	/**#@+
	 * @var int TYPE
	 */
	const TYPE_PROSPECT = 'prospect';
	const TYPE_PROSPECT_BDD = 0;
	const TYPE_CLIENT = 'client';
	const TYPE_CLIENT_BDD = 1;
	const TYPE_PROVIDER = 'provider';
	const TYPE_PROVIDER_BDD = 9;
	/**#@-*/

	/**#@+
	 * @var string Tab
	 */
	const TAB_DEFAULT = -1;
	const TAB_INFORMATION = 'information';
	const TAB_ACTIONS = 'actions';
	const TAB_OPPORTUNITIES = 'opportunities';
	const TAB_PROJECTS = 'projects';
	const TAB_PURCHASES = 'purchases';
	const TAB_ORDERS = 'orders';
	const TAB_INVOICES = 'invoices';

	/**
	 * @var string
	 */
	protected static $_jsonType = 'contact';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_CRMSOCIETE', 'company', Company::class);
		$this->setRelationships('ID_ACTION', 'lastAction', Action::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);

		$this->setGroupedRelationships('INFLUENCERS', 'influencers');
	}

	/**
	 * get all tabs
	 * @return array
	 */
	public static function getAllTabs(){
		return [
			self::TAB_INFORMATION, self::TAB_ACTIONS, self::TAB_PROJECTS, self::TAB_OPPORTUNITIES, self::TAB_PURCHASES,
			self::TAB_INVOICES, self::TAB_ORDERS
		];
	}

	/**
	 * @return string
	 */
	public function getFullName() {
		return $this->lastName .' '. $this->firstName;
	}
}
