<?php
/**
 * contactdetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class ContactDetails
 * @property int ID_COORDONNEES
 * @package BoondManager\Models\MySQL\RowObject
 */
class ContactDetails extends ModelJSONAPI {

	protected static $_jsonType = 'detail';

	const MAPPER = [
		'ID_COORDONNEES' => ['name' => 'id', 'type' => self::TYPE_STRING],
		'COORD_NOM'      => ['name' => 'name', 'type' => self::TYPE_STRING],
		'COORD_CONTACT'  => ['name' => 'contact', 'type' => self::TYPE_STRING],
		'COORD_TEL'      => ['name' => 'phone1', 'type' => self::TYPE_STRING],
		'COORD_EMAIL'    => ['name' => 'email1', 'type' => self::TYPE_STRING],
		'COORD_EMAIL2'   => ['name' => 'email2', 'type' => self::TYPE_STRING],
		'COORD_EMAIL3'   => ['name' => 'email3', 'type' => self::TYPE_STRING],
		'COORD_ADR1'     => ['name' => 'address1', 'type' => self::TYPE_STRING],
		'COORD_ADR2'     => ['name' => 'address2', 'type' => self::TYPE_STRING],
		'COORD_ADR3'     => ['name' => 'address3', 'type' => self::TYPE_STRING],
		'COORD_CP'       => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'COORD_VILLE'    => ['name' => 'town', 'type' => self::TYPE_STRING],
		'COORD_PAYS'     => ['name' => 'country', 'type' => self::TYPE_STRING],
		'COORD_ETAT'     => ['name' => 'state', 'type' => self::TYPE_BOOLEAN]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
