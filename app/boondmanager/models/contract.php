<?php
/**
 * contract.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasHrManagerInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasPoleInterface;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Contract
 *
 * @property int $id
 *
 * @property string $startDate
 * @property mixed $endDate
 * @property int $typeOf
 * @property float $monthlySalary
 * @property float $monthlyExpenses
 * @property float $dailyExpenses
 * @property int $employeeType
 * @property int $workingTimeType
 * @property string $classification
 * @property int $currency
 * @property float $exchangeRate
 * @property int $currencyAgency
 * @property float $exchangeRateAgency
 * @property float $contractAverageDailyCost
 * @property int numberOfHoursPerWeek
 *
 * @property float $chargeFactor
 * @property float $numberOfWorkingDays
 *
 * @property float $contractAverageDailyProductionCost
 * @property bool $forceContractAverageDailyProductionCost
 *
 * @property AdvantageType[] advantages
 * @property ExceptionalScales[] exceptionalScales
 * @property ExpenseDetail[] expensesDetails
 *
 * @property Employee|Candidate dependsOn
 * @property Agency agency
 * @property Contract childContract
 * @property Contract $relatedContract
 *
 */
class Contract extends ModelJSONAPI  implements HasHrManagerInterface, HasAgencyInterface, HasManagerInterface, HasPoleInterface{
	use HasAgencyTrait;

	protected static $_jsonType = 'contract';

	const MAPPER = [
		'ID_CONTRAT'             => ['name' => 'id', 'type' => self::TYPE_STRING],
		'CTR_DEBUT'              => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'CTR_FIN'                => ['name' => 'endDate', 'type' => self::TYPE_IGNORE, 'removeIfEqual' => '3000-01-01'],
		'CTR_TYPE'               => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'CTR_SALAIREMENSUEL'     => ['name' => 'monthlySalary', 'type' => self::TYPE_FLOAT],
		'CTR_FRSMENSUEL'         => ['name' => 'monthlyExpenses', 'type' => self::TYPE_FLOAT],
		'CTR_FRSJOUR'            => ['name' => 'dailyExpenses', 'type' => self::TYPE_FLOAT],
		'CTR_CATEGORIE'          => ['name' => 'employeeType', 'type' => self::TYPE_INT],
		'CTR_TPSTRAVAIL'         => ['name' => 'workingTimeType', 'type' => self::TYPE_INT],
		'CTR_CLASSIFICATION'     => ['name' => 'classification', 'type' => self::TYPE_STRING],
		'CTR_DEVISE'             => ['name' => 'currency', 'type' => self::TYPE_INT],
		'CTR_CHANGE'             => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'CTR_DEVISEAGENCE'       => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'CTR_CHANGEAGENCE'       => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'CTR_CJMCONTRAT'         => ['name' => 'contractAverageDailyCost', 'type' => self::TYPE_FLOAT],
		'CTR_DUREEHEBDOMADAIRE'  => ['name' => 'numberOfHoursPerWeek', 'type' => self::TYPE_INT],
		'CTR_DATEPE1'            => ['name' => 'probationEndDate', 'type' => self::TYPE_DATE],
		'CTR_DATEPE2'            => ['name' => 'renewalProbationEndDate', 'type' => self::TYPE_DATE],
		'CTR_SALAIREHEURE'       => ['name' => 'hourlySalary', 'type' => self::TYPE_FLOAT],
		'CTR_FORCESALAIREHEURE'  => ['name' => 'forceHourlySalary', 'type' => self::TYPE_BOOLEAN],
		'CTR_NBJRSOUVRE'         => ['name' => 'numberOfWorkingDays', 'type' => self::TYPE_FLOAT],
		'CTR_CHARGE'             => ['name' => 'chargeFactor', 'type' => self::TYPE_FLOAT],
		'CTR_COMMENTAIRES'       => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'CTR_CJMPRODUCTION'      => ['name' => 'contractAverageDailyProductionCost', 'type' => self::TYPE_FLOAT],
		'CTR_FORCECJMPRODUCTION' => ['name' => 'forceContractAverageDailyProductionCost', 'type' => self::TYPE_BOOLEAN],
		'CTR_REGLESTEMPSEXCEPTION' => ['name' => 'exceptionalScales', 'type' => self::TYPE_ARRAY, 'unserializeCallback' => 'unserializeRules', 'serializeCallback' => 'serializeRules'],
		'CTR_AVANTAGES'          => ['name' => 'advantages', 'type' => self::TYPE_ARRAY, 'unserializeCallback' => 'unserializeAdvantages', 'serializeCallback' => 'serializeAdvantages'],
		'expensesDetails'        => ['name' => 'expensesDetails', 'type' => self::TYPE_ARRAY]
	];

	const REF_PREFIX = 'CTR';

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_CONTRACT_NEED_RESOURCEORCANDIDATE = 2500;
	/**#@-*/

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function initRelationships(){
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_PARENT', 'relatedContract', Contract::class);
		$this->setRelationships('ID_ENFANT', 'childContract', Contract::class);
		$this->setDynamicTypedRelationship('ID_PROFIL', 'dependsOn', function($item){
			if($item->PROFIL_TYPE == Candidate::TYPE_CANDIDATE)
				return Candidate::class;
			else
				return Employee::class;
		});
	}

	public function getHrManagerID()
	{
		return $this->dependsOn->getHrManagerID();
	}

	public function getManagerID()
	{
		return $this->dependsOn->getManagerID();
	}

	public function getPoleID()
	{
		return $this->dependsOn->getPoleID();
	}

	/*
	 * Serializers
	 */
	public static function serializeAdvantages($array){
		return Tools::serializeDoubleArray(array_map(function($at){
			/** @var AdvantageType $at */
			return [
				$at->reference,
				Tools::reverseMapData($at->frequency, AdvantageType::MAPPER_FREQUENCY),
				Tools::reverseMapData($at->category, AdvantageType::MAPPER_CATEGORY),
				$at->participationQuota,
				$at->employeeQuota,
				$at->agencyQuota];
		}, $array));
	}

	public static function unserializeAdvantages($string){
		$advantages =  [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row){
			$advantages[] = new AdvantageType([
				'reference'          => $row[0],
				'frequency'          => Tools::mapData($row[1], AdvantageType::MAPPER_FREQUENCY),
				'category'           => Tools::mapData($row[2], AdvantageType::MAPPER_CATEGORY),
				'participationQuota' => $row[3],
				'employeeQuota'      => $row[4],
				'agencyQuota'        => $row[5]
			]);
		}
		return $advantages;
	}

	public static function serializeRules($array){
		return implode('#', array_map(function($scale){
			/** @var ExceptionalScales $scale */
			$data = [$scale->reference.'_'.$scale->parentType.'_'.$scale->parentId];
			foreach($scale->exceptionalRules as $rule){
				/** @var Rules $rule */
				$data[] = "{$rule->reference}_{$rule->priceExcludingTaxOrPriceRate}_{$rule->grossCostOrSalaryRate}";
			}
			return implode('|', $data);
		}, $array));
	}

	public static function unserializeRules($string){
		$exceptionalScales = [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row){
			$scaleData = explode('_', array_shift($row));
			$rules = [];
			for($i=0; $i < count($row); $i++){
				$ruleData = explode('_', $row);
				$rules[] = new Rules([
					'reference'                    => $ruleData[ 0 ],
					'priceExcludingTaxOrPriceRate' => $ruleData[ 1 ],
					'grossCostOrSalaryRate'        => $ruleData[ 2 ]
				]);
			}
			$exceptionalScales[] = new ExceptionalScales([
				'reference'        => $scaleData[0],
				'exceptionalRules' => $rules,
				'dependsOn'        => [
					'type' => $scaleData[1] ? 'company' : 'agency',
					'id'   => $scaleData[2]
				]
			]);
		}
		return $exceptionalScales;
	}
}
