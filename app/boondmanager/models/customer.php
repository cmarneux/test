<?php
/**
 * customer.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Customer
 * @property int id
 * @property string name
 * @property string code
 * @property string email
 * @property string firstName
 * @property string lastName
 * @property string language
 * @property string version
 * @property string clientKey
 * @property string clientToken
 * @property string creationDate
 * @property bool downloadCenter
 * @property bool intranet
 * @property Subscription subscription
 * @property User administrator
 * @property User mainSupport
 * @property Company company
 * @property DatabaseServer databaseServer
 * @property GEDServer gedServer
 * @property NodeServer nodeServer
 * @package BoondManager\Models
 */
class Customer extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'customer';

	const MAPPER = [
		'ID_CLIENT'              => ['name' => 'id', 'type' => self::TYPE_INT],
		'CLIENT_GROUPE'          => ['name' => 'name', 'type' => self::TYPE_STRING],
		'CLIENT_WEB'             => ['name' => 'code', 'type' => self::TYPE_STRING],
		'CLIENT_DATEINSCRIPTION' => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'firstName'              => ['name' => 'firstName', 'type' => self::TYPE_STRING],
		'lastName'               => ['name' => 'lastName', 'type' => self::TYPE_STRING],
		'language'               => ['name' => 'language', 'type' => self::TYPE_STRING],
		'CLIENT_EMAIL'           => ['name' => 'email', 'type' => self::TYPE_STRING],
		'CLIENT_VERSION'         => ['name' => 'version', 'type' => self::TYPE_STRING],
		'GRPCONF_HASH'           => ['name' => 'clientKey', 'type' => self::TYPE_STRING],
		'clientToken'            => ['name' => 'clientToken', 'type' => self::TYPE_STRING],
		'GRPCONF_DOWNLOADCENTER' => ['name' => 'downloadCenter', 'type' => self::TYPE_BOOLEAN],
		'GRPCONF_INTRANET'       => ['name' => 'intranet', 'type' => self::TYPE_BOOLEAN]
	];

	/**#@+
	 * @var string
	 */
	const DEFAULT_LASTNAME = 'Hugo';
	const DEFAULT_FIRSTNAME = 'Victor';

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_CUSTOMER_WRONG_ADMINISTRATOR_LOGIN = 1200,
		ERROR_CUSTOMER_WRONG_MANAGER_LOGIN = 1201;
	/**#@-*/

	/**
	 *
	 */
	public function initRelationships() {
		$this->setRelationships('ID_RESPUSER', 'mainSupport', User::class);
		$this->setRelationships('ID_CRMSOCIETE', 'company', Company::class);
		$this->setRelationships('administrator', 'administrator', User::class);
		$this->setRelationships('subscription', 'subscription', Subscription::class);
		$this->setRelationships('databaseServer', 'databaseServer', DatabaseServer::class);
		$this->setRelationships('gedServer', 'gedServer', GEDServer::class);
		$this->setRelationships('nodeServer', 'nodeServer', NodeServer::class);
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 * @return string
	 */
	public function getReference() {
		return 'CUST'.$this->id;
	}
}
