<?php
/**
 * databaseserver.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * class DatabaseServer
 * @property string url
 * @property int port
 * @property string login
 * @property string password
 * @property bool useAsDefault
 * @package BoondManager\Models
 */
class DatabaseServer extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'BDD_URL' => ['name'=>'url', 'type'=>self::TYPE_STRING],
			'BDD_PORT' => ['name'=>'port', 'type'=>self::TYPE_INT],
			'BDD_USER' => ['name'=>'login', 'type'=>self::TYPE_STRING],
			'BDD_PASS' => ['name'=>'password', 'type'=>self::TYPE_STRING],
			'BDD_DEFAULT' => ['name'=>'useAsDefault', 'type'=>self::TYPE_BOOLEAN]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
