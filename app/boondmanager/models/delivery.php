<?php
/**
 * delivery.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Delivery
 * @property string id
 * @property Project project
 * @property Resource|Product dependsOn
 * @property Contract contract
 * @property Delivery groupment
 * @property int state
 * @property string title
 * @property string startDate date
 * @property string endDate date
 * @property float numberOfDaysInvoicedOrQuantity
 * @property float numberOfDaysFree
 * @property float dailyExpenses
 * @property float monthlyExpenses
 * @property float averageHourlyPriceExcludingTax
 * @property float averageDailyPriceExcludingTax
 * @property float additionalTurnoverExcludingTax
 * @property float additionalCostsExcludingTax
 * @property float averageDailyCost
 * @property float turnoverProductionExcludingTax
 * @property float costsProductionExcludingTax
 * @property float marginProductionExcludingTax
 * @property float profitabilityProduction
 * @property float regularTimesSimulated
 * @property float regularTimesProduction
 * @property float exceptionalTimesProduction
 * @property float exceptionalCalendarsProduction
 * @property float expensesSimulated
 * @property float expensesProduction
 * @property DeliveryDetail[] additionalTurnoverAndCosts
 * @property float turnoverSimulatedExcludingTax
 * @property float costsSimulatedExcludingTax
 * @property float marginSimulatedExcludingTax
 * @property float profitabilitySimulated
 * @property int numberOfOrders
 * @property int numberOfCorrelatedOrders
 * @property float averayDailyContractCost
 * @property float weeklyWorkingHours
 * @property bool forceAverageHourlyPriceExcludingTax
 * @property float occupationRate
 * @property bool canReadProject
 * @property bool canWriteProject
 * @property Advantage[] advantages
 * @property Document[] files
 * @property expenseDetail[] expensesDetails
 * @property Delivery[] deliveries
 * @property Contract[] contracts
 * @property Delivery master
 * @property Delivery slave
 * @property ExceptionalScales[] exceptionalScales
 * @property Order order last correlated order
 *
 */
class Delivery extends ModelJSONAPI{
	/**
	 * @var string
	 */
	protected static $_jsonType = 'delivery';

	const REF_PREFIX = 'MIS';

	const MAPPER = [
		'ID_MISSIONPROJET'               => ['name' => 'id', 'type' => self::TYPE_STRING],
		'MP_DEBUT'                       => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'MP_FIN'                         => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'MP_NOM'                         => ['name' => 'title', 'type' => self::TYPE_STRING],
		'MP_TYPE'                        => ['name' => 'state', 'type' => self::TYPE_INT],
		'MP_NBJRSFACTURE'                => ['name' => 'numberOfDaysInvoicedOrQuantity', 'type' => self::TYPE_FLOAT],
		'MP_NBJRSGRATUIT'                => ['name' => 'numberOfDaysFree', 'type' => self::TYPE_FLOAT],
		'MP_FRSJOUR'                     => ['name' => 'dailyExpenses', 'type' => self::TYPE_FLOAT],
		'MP_FRSMENSUEL'                  => ['name' => 'monthlyExpenses', 'type' => self::TYPE_FLOAT],
		'MP_TARIF'                       => ['name' => 'averageDailyPriceExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_TARIFHEURE'                  => ['name' => 'averageHourlyPriceExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_TARIFADDITIONNEL'            => ['name' => 'additionalTurnoverExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_INVESTISSEMENT'              => ['name' => 'additionalCostsExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_CJM'                         => ['name' => 'averageDailyCost', 'type' => self::TYPE_FLOAT],
		'MP_PRJM'                        => ['name' => 'averageDailyContractCost', 'type' => self::TYPE_FLOAT],
		'MP_COMMENTAIRES'                => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'MP_CONDITIONS'                  => ['name' => 'conditions', 'type' => self::TYPE_STRING],
		'MP_AVANTAGES'                   => ['name' => 'advantages', 'type' => self::TYPE_ARRAY, 'unserializeCallback' => 'unserializeAdvantages', 'serializeCallback' => 'serializeAdvantages'],
		'MP_NBJRSOUVRE'                  => ['name' => 'numberOfWorkingDays', 'type' => self::TYPE_INT],
		'MP_DUREEHEBDOMADAIRE'           => ['name' => 'weeklyWorkingHours', 'type' => self::TYPE_FLOAT],
		'MP_FORCETARIFHEURE'             => ['name' => 'forceAverageHourlyPriceExcludingTax', 'type' => self::TYPE_BOOLEAN],
		'MP_REGLESTEMPSEXCEPTION'        => ['name' => 'exceptionalScales', 'type' => self::TYPE_ARRAY, 'unserializeCallback' => 'unserializeScales', 'serializeCallback' => 'serializeScales'],
		'TOTAL_CASIMUHT'                 => ['name' => 'turnoverSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_COUTSIMUHT'               => ['name' => 'costsSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_MARGESIMUHT'              => ['name' => 'marginSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_RENTASIMU'                => ['name' => 'profitabilitySimulated', 'type' => self::TYPE_FLOAT],
		'MISSIONSDETAILS'                => ['name' => 'additionalTurnoverAndCosts', 'type' => self::TYPE_ARRAY],
		'turnoverProductionExcludingTax' => ['name' => 'turnoverProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsProductionExcludingTax'    => ['name' => 'costsProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'marginProductionExcludingTax'   => ['name' => 'marginProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'profitabilityProduction'        => ['name' => 'profitabilityProduction', 'type' => self::TYPE_FLOAT],
		'occupationRate'                 => ['name' => 'occupationRate', 'type' => self::TYPE_FLOAT],
		//APIs project/{id}/productivity
		'regularTimesSimulated'          => ['name' => 'regularTimesSimulated', 'type' => self::TYPE_FLOAT],
		'regularTimesProduction'         => ['name' => 'regularTimesProduction', 'type' => self::TYPE_FLOAT],
		'exceptionalTimesProduction'     => ['name' => 'exceptionalTimesProduction', 'type' => self::TYPE_FLOAT],
		'exceptionalCalendarsProduction' => ['name' => 'exceptionalCalendarsProduction', 'type' => self::TYPE_FLOAT],
		'expensesSimulated'              => ['name' => 'expensesSimulated', 'type' => self::TYPE_FLOAT],
		'expensesProduction'             => ['name' => 'expensesProduction', 'type' => self::TYPE_FLOAT],
		//APIs deliveries, billing-deliveries-purchases-balance
		'NB_BDC'           => ['name' => 'numberOfOrders', 'type' => self::TYPE_INT],
		//~ utilisés par l'api billing-deliveries-purchases-balance
		'NB_COR'           => ['name' => 'numberOfCorrelatedOrders', 'type' => self::TYPE_INT],

		//APIs billing-deliveries-purchases-balance
		'canReadProject'   => ['name' => 'canReadProject', 'type' => self::TYPE_BOOLEAN],
		'canWriteProject'  => ['name' => 'canWriteProject', 'type' => self::TYPE_BOOLEAN],

		// resources/deliveries
		'canReadDelivery'  => ['name' => 'canReadDelivery', 'type' => self::TYPE_BOOLEAN],
		'canWriteDelivery' => ['name' => 'canWriteDelivery', 'type' => self::TYPE_BOOLEAN]
	];

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_DELIVERY_STARTDATE_CANNOT_BE_SET = 3500,
		ERROR_DELIVERY_ENDDATE_CANNOT_BE_SET = 3501,
		ERROR_DELIVERY_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE= 3502,
		ERROR_DELIVERY_TITLE_CANNOT_BE_SET = 3503,
		ERROR_DELIVERY_STATE_CANNOT_BE_SET = 3504,
		ERROR_DELIVERY_NUMBEROFDAYSINVOICEDORQUANTITY_CANNOT_BE_SET = 3505,
		ERROR_DELIVERY_NUMBEROFDAYSFREE_CANNOT_BE_SET = 3506,
		ERROR_DELIVERY_DAILYEXPENSES_CANNOT_BE_SET = 3507,
		ERROR_DELIVERY_MONTHLYEXPENSES_CANNOT_BE_SET = 3508,
		ERROR_DELIVERY_NUMBEROFWORKINGDAYS_CANNOT_BE_SET = 3509,
		ERROR_DELIVERY_AVERAGEDAILYCONTRACTCOST_CANNOT_BE_SET = 3510,
		ERROR_DELIVERY_WEKKLYWORKINGHOURS_CANNOT_BE_SET = 3511,
		ERROR_DELIVERY_AVERAGEHOURLYPRICEEXCLUDINGTAX_CANNOT_BE_SET = 3512,
		ERROR_DELIVERY_FORCEAVERAGEHOURLYPRICEEXCLUDINGTAX_CANNOT_BE_SET = 3513,
		ERROR_DELIVERY_SLAVE_CANNOT_BE_SET_IF_FORCETRANSFER = 3514,
		ERROR_DELIVERY_EXPENSESDETAILS_CANNOT_BE_SET = 3515,
		ERROR_DELIVERY_ADVANTAGES_CANNOT_BE_SET = 3516,
		ERROR_DELIVERY_EXCEPTIONALSCALES_CANNOT_BE_SET = 3517,
		ERROR_DELIVERY_CANNOT_UPDATE_TURNOVERANDCOSTS = 3518,
		ERROR_DELIVERY_SLAVE_IS_INCORRECT = 3519,
        ERROR_DELIVERY_FORCETRANSFER_CANNOT_BE_SET = 3520,
        ERROR_DELIVERY_SLAVE_CANNOT_BE_SET = 3521;
	/**#@-*/

	/**#@+
	 * @var int Value of forecast state
	 */
	const
		STATE_SIGNED = 0,
		STATE_FORECAST = 1;
	/**#@-*/

	/**#@+
	 * @var int Depends on type
	 */
	const
		TYPE_RESOURCE = 0,
		TYPE_PRODUCT = 1;
	/**#@-*/

	public function initRelationships(){
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setRelationships('ID_CONTRAT', 'contract', Contract::class);
		$this->setGroupedRelationships('contracts', 'contracts');
		$this->setGroupedRelationships('files', 'files');
		$this->setRelationships('ID_PARENT', 'groupment', Delivery::class);
		$this->setRelationships('order', 'order', Order::class);
		$this->setDynamicTypedRelationship('ITEM_TYPE', 'dependsOn', function($item){
			if($item->ITEM_TYPE == self::TYPE_RESOURCE)
				return Employee::class;
			else
				return Product::class;
		});
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	public function isMaster(){
		return boolval($this->slave);
	}

	public function isSlave(){
		return boolval($this->master);
	}

	/*
	 * Serializers
	 */
	public static function serializeAdvantages($array){
		return Tools::serializeDoubleArray(array_map(function($at){
			/** @var AdvantageType $at */
			return [
				$at->reference,
				Tools::reverseMapData($at->frequency, AdvantageType::getFrequencies()),
				Tools::reverseMapData($at->category, AdvantageType::getCategories()),
				$at->participationAmount,
				$at->employeeAmount,
				$at->agencyAmount
			];
		}, $array));
	}

	public static function unserializeAdvantages($string){
		$advantages =  [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row){
			$advantages[] = new AdvantageType([
				'reference'           => $row[0],
				'frequency'           => Tools::mapData($row[1], AdvantageType::getFrequencies()),
				'category'            => Tools::mapData($row[2], AdvantageType::getCategories()),
				'participationAmount' => $row[3],
				'employeeAmount'      => $row[4],
				'agencyAmount'        => $row[5],
				'agency'              => new Agency(['id' => $row[6]])
			]);
		}
		return $advantages;
	}

	public static function serializeScales($array){
		return Tools::serializeDoubleArray(array_map(function($scale){
			/** @var ExceptionalScales $scale */
			$data = [$scale->reference.'_'.$scale->parentType.'_'.$scale->parentId];
			foreach($scale->exceptionalRules as $rule){
				/** @var Rules $rule */
				$data[] = "{$rule->reference}_{$rule->priceExcludingTaxOrPriceRate}_{$rule->grossCostOrSalaryRate}";
			}
			return $data;
		}, $array));
	}

	public static function unserializeScales($string){
		$scales = [];
		$data = Tools::unserializeDoubleArray($string);
		foreach($data as $row) {
			$ref = array_shift($row);
			list($bareme, $type, $id) = explode('_',$ref);
			$rules = [];
			for($i = 0; $i < count($row); $i++){
				$ruleData = explode('_', $row);
				$rules[] = new Rules([
					'reference'                    => $ruleData[0],
					'priceExcludingTaxOrPriceRate' => $ruleData[1],
					'grossCostOrSalaryRate'        => $ruleData[2],
				]);
			}

			$scales[] = new ExceptionalScales([
				'id' => $ref,
				'reference' => $bareme,
				'exceptionalRules' => $rules,
				// unofficial fields
				'dependsOn' => ($type) ? new Company(['id' => $id]) : new Agency(['id' => $id])
			]);

		}
		return $scales;
	}

	/*
	 * calculation area
	 */

	/**
	 * override averageHourlyPrice if not forced
	 * @return $this;
	 */
	public function calculateAverageHourlyPrice() {
		if (!$this->forceAverageHourlyPriceExcludingTax) {
			if ($this->weeklyWorkingHours == 0) {
				$this->averageHourlyPriceExcludingTax = 0;
			} else {
				$this->averageHourlyPriceExcludingTax = $this->averageDailyPriceExcludingTax * 5 / $this->weeklyWorkingHours;
			}
		}
		return $this;
	}

	/**
	 * override daily & monthly expenses from expensesDetails if they exists
	 * @return $this;
	 */
	public function calculateExpensesFromDetails(){

		if($this->expensesDetails) {
			$daily   = 0;
			$monthly = 0;
			foreach ($this->expensesDetails as $exInput) {
				if ($exInput->periodicity == ExpenseDetail::PERIODICITY_DAILY) $daily += $exInput->netAmount;
				else $monthly += $exInput->netAmount;
			}
			$this->dailyExpenses = $daily;
			$this->monthlyExpenses = $monthly;
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateContractCost(){
		if($this->contract) $this->averayDailyContractCost = $this->contract->contractAverageDailyCost;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateAdditionalTurnoverAndCosts() {
		if($this->additionalTurnoverAndCosts) {
			$turnover   = 0;
			$cost = 0;
			foreach ($this->additionalTurnoverAndCosts as $item) {
				/** @var DeliveryDetail $item */
				$turnover += $item->turnoverExcludingTax;
				$cost += $item->costsExcludingTax;
			}
			$this->additionalTurnoverExcludingTax = $turnover;
			$this->additionalCostsExcludingTax = $cost;
		}
		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateData(){
		$this->calculateContractCost();
		$this->calculateAverageHourlyPrice();
		$this->calculateExpensesFromDetails();
		$this->calculateAdditionalTurnoverAndCosts();
		return $this;
	}
}
