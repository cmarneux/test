<?php
/**
 * deliverydetail.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class PositioningDetail
 * @property int id
 * @property float turnoverExcludingTax
 * @property float costsExcludingTax
 * @property string title
 * @property Purchase purchase
 * @property string date date
 * @property boolean state
 * @package BoondManager\Models
 */
class DeliveryDetail extends ModelJSON{

    /**#@+
     * @var mixed TAB_MISSIONDETAILS:PARENT_TYPE
     */
    const
        PARENT_PROJECT = 'project',
        PARENT_PROJECT_KEY = 0,
        PARENT_DELIVERY = 'delivery',
        PARENT_DELIVERY_KEY = 1;
    /**#@-*/

	/**#@+
	 * @var int TAB_MISSIONDETAILS:MISDETAILS_ETAT
	 */
	const
		STATE_ON = 1;
	/**#@-*/

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_MISSIONDETAILS'         => ['name' => 'id', 'type' => self::TYPE_STRING],
			'MISDETAILS_TARIF'          => ['name' => 'turnoverExcludingTax', 'type' => self::TYPE_FLOAT],
			'MISDETAILS_INVESTISSEMENT' => ['name' => 'costsExcludingTax', 'type' => self::TYPE_FLOAT],
			'MISDETAILS_TITRE'          => ['name' => 'title', 'type' => self::TYPE_STRING],
			'MISDETAILS_DATE'           => ['name' => 'date', 'type' => self::TYPE_DATE],
			'MISDETAILS_ETAT'           => ['name' => 'state', 'type' => self::TYPE_BOOLEAN],
			'PURCHASE'          		=> ['name' => 'purchase', 'type' => self::TYPE_OBJECT]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->toArray();
	}
}
