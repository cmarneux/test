<?php
/**
 * device.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class Device
 * @property int id
 * @property string sha
 * @property string browser
 * @property string server
 * @property string name
 * @property string authorizationDate
 * @property string lastLogInDate
 * @property string internetProtocol
 * @property boolean isManaged
 * @property boolean isSessionExists
 * @property int userId
 * @package BoondManager\Models
 */
class Device extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'device';

	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_DEVICEALLOWED'   => ['name' => 'id', 'type' => self::TYPE_STRING],
			'DALL_SHA'           => ['name' => 'sha', 'type' => self::TYPE_STRING],
			'DALL_NAVIGATEUR'    => ['name' => 'browser', 'type' => self::TYPE_STRING],
			'DALL_SERVER'        => ['name' => 'server', 'type' => self::TYPE_STRING],
			'DALL_NOM'           => ['name' => 'name', 'type' => self::TYPE_STRING],
			'DALL_DATE'          => ['name' => 'authorizationDate', 'type' => self::TYPE_DATETIME],
			'DALL_LASTCONNEXION' => ['name' => 'lastLogInDate', 'type' => self::TYPE_DATETIME],
			'DALL_IP'            => ['name' => 'internetProtocol', 'type' => self::TYPE_STRING, 'serializeCallback' => 'serializeInternetProtocol', 'unserializeCallback' => 'unserializeInternetProtocol'],
			'ID_USER'            => ['name' => 'userId', 'type' => self::TYPE_INT],
			'isManaged'           => ['name' => 'isManaged', 'type' => self::TYPE_BOOLEAN],
			'isSessionExists'           => ['name' => 'isSessionExists', 'type' => self::TYPE_BOOLEAN]
		];
	}

	public static function serializeInternetProtocol($data) {
		return ip2long($data);
	}

	public static function unserializeInternetProtocol($data) {
		return long2ip($data);
	}
}
