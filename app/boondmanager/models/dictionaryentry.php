<?php
/**
 * DictionaryEntry.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Services\Dictionary;
use BoondManager\Services\BM;

/**
 * Class DictionaryEntry
 * @package BoondManager\Models
 */
class DictionaryEntry
{
	/**
	 * @var string a dictionary key
	 */
	private $key;

	/**
	 * @var array some params to replace in the entry
	 */
	private $param;

	/**
	 * @var array Cache the translation for the entry
	 */
	private $perLanguageCache = [];

	/**
	 * DictionaryEntry constructor.
	 * @param string $key
	 * @param array $param
	 */
	public function __construct($key, $param = null)
	{
		$this->key   = $key;
		$this->param = $param;
	}

	/**
	 * translate the entry
	 * @param string $language
	 * @return mixed
	 */
	public function translate($language = null)
	{
		if (!$language) $language = BM::getLanguage();
		if (!isset($this->perLanguageCache[ $language ])) {
			$strParams = [];
			if ($this->param) {
				$strParams = $this->translateParams($this->param, $language);
			}
			$this->perLanguageCache[ $language ] = Dictionary::getDict($this->key, $strParams, $language);
		}
		return $this->perLanguageCache[ $language ];
	}

	/**
	 * Translate recursively the params
	 * @param mixed $params
	 * @param string $language
	 * @return array
	 */
	private function translateParams($params, $language)
	{

		if (!is_array($params)) $params = [$params];

		$strParams = [];

		// stringify all params
		foreach ($params as $key => $param) {
			if ($param instanceof DictionaryEntry) $strParams[ $key ] = $param->translate($language);
			else if (is_array($param)) $strParams[ $key ] = $this->translateParams($param, $language);
			else $strParams[ $key ] = $param;
		}

		return $strParams;
	}
}
