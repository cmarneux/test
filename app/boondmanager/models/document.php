<?php
/**
 * document.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractFile;
use BoondManager\Databases\Local;

/**
 * class Document
 * @property int $id
 * @property int ID_PARENT
 * @property string $name
 * @property Company|Opportunity|Project|Order|Purchase|Action|Delivery|Positioning $dependsOn
 * @package BoondManager\Models
 */
class Document extends AbstractFile {
	const PARENT_TYPE_PRODUCT = 'product';
	const PARENT_TYPE_ACTION = 'action';
	const PARENT_TYPE_ORDER = 'order';
	const PARENT_TYPE_PROJECT = 'project';
	const PARENT_TYPE_COMPANY = 'company';
	const PARENT_TYPE_PURCHASE = 'purchase';
	const PARENT_TYPE_DELIVERY = 'delivery';
	const PARENT_TYPE_POSITIONING = 'positioning';
	const PARENT_TYPE_OPPORTUNITY = 'opportunity';

	const SUBTYPE = 'document';
	const MAPPER_DOCUMENT_PARENT_TYPE_ID = [
		Document::PARENT_TYPE_ACTION      => Document::TYPE_ACTION,
		Document::PARENT_TYPE_COMPANY     => Document::TYPE_COMPANY,
		Document::PARENT_TYPE_PROJECT     => Document::TYPE_PROJECT,
		Document::PARENT_TYPE_ORDER       => Document::TYPE_ORDER,
		Document::PARENT_TYPE_PRODUCT     => Document::TYPE_PRODUCT,
		Document::PARENT_TYPE_PURCHASE    => Document::TYPE_PURCHASE,
		Document::PARENT_TYPE_DELIVERY    => Document::TYPE_DELIVERY,
		Document::PARENT_TYPE_POSITIONING => Document::TYPE_POSITIONING,
		Document::PARENT_TYPE_OPPORTUNITY => Document::TYPE_OPPORTUNITY
	];
	/**
	 * @var string
	 */
	protected static $_jsonType = 'document';

	/**#@+
	 * @var int TYPE
	 */
	const
		TYPE_COMPANY = 0,
		TYPE_OPPORTUNITY = 1, // ?
		TYPE_PROJECT = 2,
		TYPE_ORDER = 3,
		TYPE_PRODUCT = 4,
		TYPE_PURCHASE = 5,
		TYPE_ACTION = 6,
		TYPE_DELIVERY = 7,
		TYPE_POSITIONING = 9;
	/**#@-*/

	const MAPPER = [
		'ID_DOCUMENT' => ['name' => 'id', 'type' => self::TYPE_INT],
		'FILE_NAME'   => ['name' => 'name', 'type' => self::TYPE_STRING]
	];

	/**
	 * @var Resource
	 */
	public $resource;

	/**
	 * @var Candidate
	 */
	public $candidate;

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 * @return string
	 */
	public function getParentType() {
		switch (get_class($this->dependsOn)) {
			case Company::class: return self::PARENT_TYPE_COMPANY;
			case Opportunity::class: return self::PARENT_TYPE_OPPORTUNITY;
			case Project::class: return self::PARENT_TYPE_PROJECT;
			case Order::class: return self::PARENT_TYPE_ORDER;
			case Product::class: return self::PARENT_TYPE_PRODUCT;
			case Purchase::class: return self::PARENT_TYPE_PURCHASE;
			case Action::class: return self::PARENT_TYPE_ACTION;
			case Delivery::class: return self::PARENT_TYPE_DELIVERY;
			case Positioning::class: return self::PARENT_TYPE_POSITIONING;
			default: return null;
		}
	}

	/**
	 * @return int
	 */
	public function getSubTypeID() {
		return Local\File::TYPE_DOCUMENT;
	}
}
