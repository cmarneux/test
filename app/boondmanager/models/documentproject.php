<?php
/**
 * documentproject.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Document
 * @property string id
 * @property string name
 * @package BoondManager\Models
 */
class DocumentProject extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'document';

	/**
	 * return the object ID
	 * @return string
	 */
	public function getID()
	{
		return $this->id.'_project';
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_DOCUMENT'       => ['name' => 'id', 'type' => self::TYPE_INT],
			'FILE_NAME' 		=> ['name' => 'name', 'type' => self::TYPE_STRING]
		];
	}
}
