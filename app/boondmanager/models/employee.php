<?php
/**
 * resource.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Models;

use BoondManager\Services\Validations;
use Wish\Models\ModelJSONAPI;
use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasHrManagerInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use BoondManager\Services\BM;
use Wish\Tools;

/**
 * class Resource
 * @property int id
 * @property string lastName
 * @property string firstName
 * @property string email1
 * @property string email2
 * @property string email3
 * @property string phone1
 * @property string phone2
 * @property string phone3
 * @property string currencyAgency
 * @property float exchangeRateAgency
 * @property string currency
 * @property float exchangeRate
 * @property bool visibility
 * @property bool resourceCanModifyTechnicalData
 * @property array mobilityAreas
 * @property int typeOf
 * @property int numberOfResumes
 * @property string reference
 * @property Reference references
 * @property string country
 * @property int state
 * @property array timesProduction
 * @property string availability
 * @property bool forceAvailability
 * @property Tool[] tools
 * @property Language[] languages
 * @property WebSocial[] socialNetworks
 * @property Account mainManager
 * @property Account hrManager
 * @property Resume resume
 * @property Resume[] resumes
 * @property File[] files
 * @property Agency agency
 * @property Candidate candidate
 * @property Contact providerContact
 * @property Company providerCompany
 * @property Pole pole
 * @property Account account // DEPRECATED
 * @property Contract contract
 * @property Contract[] contracts
 * @property AbsencesQuota[] absencesAccounts
 * @property integer userId
 * @property string subscription
 * @property string level
 * @property string homePage
 * @property boolean defaultSearch
 * @property boolean narrowPerimeter
 * @property string activityExpensesStartDate
 * @property string documentsAutoCreation
 * @property string login
 * @property string password
 * @property string hash
 * @property string language
 * @property string startContract
 * @property string endContract
 * @property boolean logInOnlyFromThirdParty
 * @property boolean manageAllowedDevices
 * @property boolean warningForNewDevice
 * @property boolean googleThirdPartyExists
 * @property boolean $microsoftThirdPartyExists
 * @property boolean trustelemThirdPartyExists
 * @property string|float workUnitRate
 * @property array workUnitTypesAllowedForAbsences
 * @property string allowExceptionalTimes
 * @property array expensesReportsWorkflow
 * @property array absencesReportsWorkflow
 * @property array timesReportsWorkflow
 * @property string dashboardGraphsAvailable
 * @property array dashboardGraphsPerimeter
 * @property string dashboardGraphsPeriod
 * @property AccountModules modules
 * @property array navigationBar
 * @property AccountRights rights
 * @property string lastLogInDate datetime
 * @property string userToken
 * @property string startTimesExpenses date
 * @property array commercialScorecards
 * @property array humanResourcesScorecards
 * @property array recruitmentScorecards
 * @property array activityExpensesScorecards
 * @property array billingScorecards
 * @property array globalScorecards
 * @property array companiesDistributionScorecards
 * @property array projectsScorecards
 * @property array resourcesScorecards
 * @property array apps
 * @property array otherAgencies
 * @property array otherPoles
 * @property array devices
 * @property Role role
 * @property App advancedAppTemplate
 * @property App advancedAppCertification
 * @property App advancedAppCalendar
 * @property App advancedAppMail
 * @property App advancedAppEmailing
 * @property App advancedAppViewer
 * @package BoondManager\Models
 */
class Employee extends ModelJSONAPI implements HasManagerInterface, HasHrManagerInterface, HasPoleInterface, HasAgencyInterface{
	use HasAgencyTrait, HasPoleTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'resource';

	const REF_PREFIX = 'COMP';

	const MAPPING = [
		'ID_PROFIL'            => ['name' => 'id', 'type' => self::TYPE_STRING],
		'PROFIL_NOM'           => ['name' => 'lastName', 'type' => self::TYPE_STRING],
		'PROFIL_PRENOM'        => ['name' => 'firstName', 'type' => self::TYPE_STRING],
		'PROFIL_CIVILITE'      => ['name' => 'civility', 'type' => self::TYPE_INT],
		'PROFIL_REFERENCE'     => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'PROFIL_TYPE'          => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'PROFIL_DATE'          => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'PROFIL_DATENAISSANCE' => ['name' => 'dateOfBirth', 'type' => self::TYPE_STRING],
		'PROFIL_LIEUNAISSANCE' => ['name' => 'placeOfBirth', 'type' => self::TYPE_STRING],
		'PROFIL_NATIONALITE'   => ['name' => 'nationality', 'type' => self::TYPE_STRING],
		'PROFIL_NUMSECU'       => ['name' => 'healthCareNumber', 'type' => self::TYPE_STRING],
		'PROFIL_SITUATION'     => ['name' => 'situation', 'type' => self::TYPE_INT],
		'PROFIL_DATEUPDATE'    => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'PROFIL_EMAIL'         => ['name' => 'email1', 'type' => self::TYPE_STRING],
		'PROFIL_EMAIL2'        => ['name' => 'email2', 'type' => self::TYPE_STRING],
		'PROFIL_EMAIL3'        => ['name' => 'email3', 'type' => self::TYPE_STRING],
		'PROFIL_TEL1'          => ['name' => 'phone1', 'type' => self::TYPE_STRING],
		'PROFIL_TEL2'          => ['name' => 'phone2', 'type' => self::TYPE_STRING],
		'PROFIL_TEL3'          => ['name' => 'phone3', 'type' => self::TYPE_STRING],
		'PROFIL_FAX'           => ['name' => 'fax', 'type' => self::TYPE_STRING],
		'PROFIL_ETAT'          => ['name' => 'state', 'type' => self::TYPE_INT],
		'PROFIL_ADR'           => ['name' => 'address', 'type' => self::TYPE_STRING],
		'PROFIL_CP'            => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'PROFIL_VILLE'         => ['name' => 'town', 'type' => self::TYPE_STRING],
		'PROFIL_PAYS'          => ['name' => 'country', 'type' => self::TYPE_STRING],
		'PROFIL_VISIBILITE'    => ['name' => 'visibility', 'type' => self::TYPE_BOOLEAN],
		'PROFIL_STATUT'        => ['name' => 'function', 'type' => self::TYPE_STRING],
		'PARAM_CHANGE'         => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'PARAM_CHANGEAGENCE'   => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'PARAM_DEVISE'         => ['name' => 'currency', 'type' => self::TYPE_INT],
		'PARAM_DEVISEAGENCE'   => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'PARAM_MOBILITE'       => ['name' => 'mobilityAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'PARAM_DATEDISPO'      => ['name' => 'availability', 'type' => self::TYPE_IGNORE],
		'PARAM_TYPEDISPO'      => ['name' => 'forceAvailability', 'type' => self::TYPE_BOOLEAN],
		'PARAM_TARIF1'         => ['name' => 'averageDailyPriceExcludingTax', 'type' => self::TYPE_FLOAT],
		'PARAM_COMMENTAIRE'    => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'PARAM_COMMENTAIRE2'   => ['name' => 'administrativeComments', 'type' => self::TYPE_STRING],
		'availability'         => ['name' => 'availability', 'type' => self::TYPE_STRING],
		'NB_POS'               => ['name' => 'numberOfActivePositionings', 'type' => self::TYPE_INT],
		'NB_CV'                => ['name' => 'numberOfResumes', 'type' => self::TYPE_INT],
		'WEBSOCIALS'           => ['name' => 'socialNetworks', 'type' => self::TYPE_ARRAY],
		'DT_TITRE'             => ['name' => 'title', 'type' => self::TYPE_STRING],
		'COMP_COMPETENCE'      => ['name' => 'skills', 'type' => self::TYPE_STRING],
		'COMP_APPLICATIONS'    => ['name' => 'activityAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'COMP_INTERVENTIONS'   => ['name' => 'expertiseAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'DT_LANGUES'           => ['name' => 'languages', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeLanguages', 'unserializeCallback' => 'unserializeLanguages'],
		'DT_OUTILS'            => ['name' => 'tools', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeTools', 'unserializeCallback' => 'unserializeTools'],
		'DT_DIPLOMES'          => ['name' => 'diplomas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'DT_EXPERIENCE'        => ['name' => 'experience', 'type' => self::TYPE_INT],
		'DT_FORMATION'         => ['name' => 'training', 'type' => self::TYPE_STRING],
		'REFERENCES'           => ['name' => 'references', 'type' => self::TYPE_ARRAY],
		'PROFIL_ALLOWCHANGE'   => ['name' => 'resourceCanModifyTechnicalData', 'type' => self::TYPE_BOOLEAN],
		// resources/{id}/*-reports
		'MAX_FIN_CONTRAT'              => ['name' => 'endContract', 'type' => self::TYPE_DATE],
		'MIN_DEBUT_CONTRAT'            => ['name' => 'startContract', 'type' => self::TYPE_DATE],
		//APIs projects/{id}/batches-markers
		'TIMESPRODUCTION'      => ['name' => 'timesProduction', 'type' => self::TYPE_FLOAT],
		'absencesAccounts'     => ['name' => 'absencesAccounts', 'type' => self::TYPE_ARRAY],
		//SOLR
		'CURRENT_DELETE'       => ['name' => 'isEntityDeleting', 'type' => self::TYPE_BOOLEAN],
		'CURRENT_UPDATE'       => ['name' => 'isEntityUpdating', 'type' => self::TYPE_BOOLEAN],
		//APIs accounts
		'ID_USER'       			 => ['name' => 'userId', 'type' => self::TYPE_INT],
		'USER_ABONNEMENT'      		 => ['name' => 'subscription', 'type' => self::TYPE_STRING, 'mapper' => BM::USER_ACCESS_MAPPING],
		'USER_TYPE'      	   		 => ['name' => 'level', 'type' => self::TYPE_STRING, 'serializeCallback' => 'serializeAccount', 'unserializeCallback' => 'unserializeAccount'],
		'USER_HOMEPAGE'              => ['name' => 'homePage', 'type' => self::TYPE_STRING, 'mapper' => self::MAPPER_MENUPATH],
		'USER_DEFAULTSEARCH'         => ['name' => 'defaultSearch', 'type' => self::TYPE_BOOLEAN],
		'USER_JOINCATEGORY'          => ['name' => 'narrowPerimeter', 'type' => self::TYPE_BOOLEAN],
		'USER_TPSFRSSTART'      	 => ['name' => 'activityExpensesStartDate', 'type' => self::TYPE_STRING, 'serializeCallback' => 'serializeActivityExpensesStartDate', 'unserializeCallback' => 'unserializeActivityExpensesStartDate'],
		'USER_TPSFRSETAT'      		 => ['name' => 'documentsAutoCreation', 'type' => self::TYPE_STRING, 'mapper' => self::DOCUMENTAUTOCREATION_MAPPER],
		'USER_LOGIN'                 => ['name' => 'login', 'type' => self::TYPE_STRING],
		'USER_PWD'                   => ['name' => 'password', 'type' => self::TYPE_STRING],
		'hash'                   	 => ['name' => 'hash', 'type' => self::TYPE_STRING],
		'USER_LANGUE'                => ['name' => 'language', 'type' => self::TYPE_STRING],
		'USER_CONNECTIONTYPE'        => ['name' => 'logInOnlyFromThirdParty', 'type' => self::TYPE_BOOLEAN],
		'USER_SECURITYCOOKIE'        => ['name' => 'manageAllowedDevices', 'type' => self::TYPE_BOOLEAN],
		'USER_SECURITYALERT'         => ['name' => 'warningForNewDevice', 'type' => self::TYPE_BOOLEAN],
		'googleThirdPartyExists'     => ['name' => 'googleThirdPartyExists', 'type' => self::TYPE_BOOLEAN],
		'microsoftThirdPartyExists'  => ['name' => 'microsoftThirdPartyExists', 'type' => self::TYPE_BOOLEAN],
		'trustelemThirdPartyExists'  => ['name' => 'trustelemThirdPartyExists', 'type' => self::TYPE_BOOLEAN],
		'USER_TAUXHORAIRE'           => ['name' => 'workUnitRate', 'type' => self::TYPE_IGNORE, 'serializeCallback' => 'serializeWorkUnitRate', 'unserializeCallback' => 'unserializeWorkUnitRate'],
		'USER_ALLOWABSENCESTYPEHREF' => ['name' => 'workUnitTypesAllowedForAbsences', 'type' => self::TYPE_ARRAY, 'doubleSerializedArray' => true],
		'USER_ALLOWTEMPSEXCEPTION'   => ['name' => 'allowExceptionalTimes', 'type' => self::TYPE_STRING, 'mapper' => self::EXCEPTIONALTIMES_MAPPER],
		'USER_VALIDATIONFRAIS'       => ['name' => 'expensesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'USER_VALIDATIONABSENCES'    => ['name' => 'absencesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'USER_VALIDATIONTEMPS'       => ['name' => 'timesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'USER_TPSFRSSTART'           => ['name' => 'startTimesExpenses', 'type' => self::TYPE_DATE],
		'dashboardGraphsAvailable'   => ['name' => 'dashboardGraphsAvailable', 'type' => self::TYPE_STRING],
		'dashboardGraphsPerimeter'   => ['name' => 'dashboardGraphsPerimeter', 'type' => self::TYPE_ARRAY],
		'dashboardGraphsPeriod'   	 => ['name' => 'dashboardGraphsPeriod', 'type' => self::TYPE_STRING],
		'modules'                    => ['name' => 'modules', 'type' => self::TYPE_OBJECT],
		'navigationBar'              => ['name' => 'navigationBar', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeNavigation', 'unserializeCallback' => 'unserializeNavigation'],
		'rights'                     => ['name' => 'rights', 'type' => self::TYPE_OBJECT],
		'USER_LASTCONNEXION'         => ['name' => 'lastLogInDate', 'type' => self::TYPE_DATETIME],
		'commercialScorecards'       => ['name' => 'commercialScorecards', 'type' => self::TYPE_ARRAY],
		'humanResourcesScorecards'   => ['name' => 'humanResourcesScorecards', 'type' => self::TYPE_ARRAY],
		'recruitmentScorecards'      => ['name' => 'recruitmentScorecards', 'type' => self::TYPE_ARRAY],
		'activityExpensesScorecards' => ['name' => 'activityExpensesScorecards', 'type' => self::TYPE_ARRAY],
		'billingScorecards'          => ['name' => 'billingScorecards', 'type' => self::TYPE_ARRAY],
		'globalScorecards'           => ['name' => 'globalScorecards', 'type' => self::TYPE_ARRAY],
		'companiesDistributionScorecards' => ['name' => 'companiesDistributionScorecards', 'type' => self::TYPE_ARRAY],
		'projectsScorecards'         => ['name' => 'projectsScorecards', 'type' => self::TYPE_ARRAY],
		'resourcesScorecards'        => ['name' => 'resourcesScorecards', 'type' => self::TYPE_ARRAY],
		'ID_DT'                      => ['name' => 'ID_DT', 'type' => self::TYPE_INT],
		'userToken'                  => ['name' => 'userToken', 'type' => self::TYPE_STRING]
	];

	/**#@+
	 * @var int DISPO
	 */
	const DISPO_ASAP = 1;
	const DISPO_DATE = 2;
	const DISPO_FORCEASAP = 3;
	const DISPO_FORCEDATE = 4;
	/**#@-*/

	/**#@+
	 * tabs available
	 * @var string TAB
	 */
	const TAB_INFORMATION = 'information';
	const TAB_ADMINISTRATIVE = 'administrative';
	const TAB_ADVANTAGES = 'advantages';
	const TAB_TECHNICALDATA = 'technical-data';
	const TAB_ACTIONS = 'actions';
	const TAB_POSITIONINGS = 'positionings';
	const TAB_DELIVERIES = 'deliveries';
	const TAB_TIMESREPORTS = 'times-reports';
	const TAB_EXPENSESREPORTS = 'expenses-reports';
	const TAB_ABSENCESREPORTS = 'absences-reports';
	const TAB_ABSENCESACCOUNTS = 'absences-accounts';
	const TAB_SETTING_ABSENCESACCOUNTS = 'setting/absences-accounts';
	const TAB_FLAGS = 'attached-flags';
	const TAB_SETTING_INTRANET = 'setting/intranet';
	const TAB_SETTING_TARGET = 'setting/target';
	const TAB_SETTING_SECURITY = 'setting/security';
	/**#@-*/

	/**#@+
	 * resource state
	 * @var int STATE
	 */
	const STATE_CURRENT = 1;
	const STATE_ARCHIVED = 0;
	/**#@-TAB_SETTING_TARGET

	/**#@+
	 * @var int TYPE
	 */
	const TYPE_INTERNAL_RESOURCE = 0;
	const TYPE_EXTERNAL_RESOURCE = 1;
	/**#@-*/

	/**#@+
	 * @var int USER_TPSFRSETAT
	 */
	const DOCUMENTAUTOCREATION_MAPPER = [
		0 => 'inactive',
		1 => 'timesheetsAndExpenses',
		2 => 'expenses',
		3 => 'timesheets',
	];
	/**#@-*/

	/**#@+
	 * @var int USER_ALLOWTEMPSEXCEPTION
	 */
	const EXCEPTIONALTIMES_MAPPER = [
		0 => 'inactive',
		1 => 'withRecovering',
		2 => 'withoutRecovering',
	];
	/**#@-*/

	const
		DASHBOARDGRAPH_SUMMARY = 'summary',
		DASHBOARDPERIOD_THISYEAR = 'thisYear';

	/**#@+
	 * @var int CONFIG_DASHBOARD
	 */
	const DASHBOARDGRAPHS_MAPPER = [
		0 => 'myPeriodicalsTargets',
		1 => 'recruitmentsFunnel',
		2 => 'salesFunnel',
		3 => 'opportunitiesDistribution',
		4 => 'candidatesDistribution',
		5 => 'chargedTurnoverSignedTurnover',
		6 => 'chargedTurnoverProductionTurnoverSignedTurnover',
		7 => self::DASHBOARDGRAPH_SUMMARY,
		8 => 'chargedTurnoverChargedMargin',
		9 => 'productionTurnoverProductionMargin',
		10 => 'signedTurnoverSignedMargin'
	];

	const DASHBOARDPERIODS_MAPPER = [
		0 => 'thisWeek',
		1 => 'thisMonth',
		2 => 'thisQuarterly',
		3 => 'thisHalfYear',
		4 => self::DASHBOARDPERIOD_THISYEAR,
		5 => 'thisFiscalPeriod'
	];
	/**#@-*/

	public function initRelationships(){
		$this->setRelationships('ID_PROFIL_RESPMANAGER', 'mainManager', Employee::class);
		$this->setRelationships('ID_PROFIL_RESPRH', 'hrManager', Employee::class);
		$this->setRelationships('ID_PROFILCANDIDAT', 'candidate', Candidate::class);
		$this->setRelationships('ID_CV', 'resume', Resume::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);

		$this->setRelationships('ID_CRMCONTACT', 'providerContact', Contact::class);
		$this->setRelationships('ID_CRMSOCIETE', 'providerCompany', Company::class);

		$this->setGroupedRelationships('CONTRATS', 'contracts');
		$this->setGroupedRelationships('FILES', 'files');
		$this->setGroupedRelationships('CVs', 'resumes');

		$this->setRelationships('ID_ROLE', 'role', Role::class);
		$this->setGroupedRelationships('apps', 'apps');
		$this->setGroupedRelationships('otherAgencies', 'otherAgencies');
		$this->setGroupedRelationships('otherPoles', 'otherPoles');
		$this->setGroupedRelationships('devices', 'devices');
		$this->setRelationships('advancedAppTemplate', 'advancedAppTemplate', App::class);
		$this->setRelationships('advancedAppCertification', 'advancedAppCertification', App::class);
		$this->setRelationships('CONFIG_APICALENDAR', 'advancedAppCalendar', App::class);
		$this->setRelationships('CONFIG_APIMAIL', 'advancedAppMail', App::class);
		$this->setRelationships('CONFIG_APIEMAILING', 'advancedAppEmailing', App::class);
		$this->setRelationships('CONFIG_APIVIEWER', 'advancedAppViewer', App::class);
	}

	/**
	 * @return bool true if the user type is an administrator client
	 */
	public function isAdministrator() {
		return $this->level == BM::USER_TYPE_ADMINISTRATOR;
	}

	/**
	 * @return bool true if the user type is a support root
	 */
	public function isRoot() {
		return $this->level == BM::USER_TYPE_ROOT;
	}

	/**
	 * @return bool true if the user type is an employee client
	 */
	public function isEmployee() {
		return $this->level == BM::USER_TYPE_RESOURCE;
	}

	/**
	 * @return bool true if the user type is a manager support
	 */
	public function isSupport() {
		return $this->level == BM::USER_TYPE_SUPPORT;
	}

	/**
	 * @return bool true if the user type is a manager client
	 */
	public function isManager() {
		return $this->level == BM::USER_TYPE_MANAGER;
	}


		/**
	 * check if the resource does not belongs to a manager, ie is a top manager
	 * @return bool
	 */
	public function isTopManager() {
		return !$this->mainManager;
	}

	/**
	 * check if the resource is visible
	 * @return bool
	 */
	public function isVisible() {
		return $this->visibility==1;
	}

	/**
	 * @return string
	 */
	public function getFullName() {
		return $this->lastName.' '.$this->firstName;
	}

	public function calculateUserToken() {
		$this->userToken = Tools::objectID_BM_encode([$this->userId, BM::getCustomerCode()] );
		return $this;
	}

	/**
	 * get a list of all tab ID
	 * @return array
	 */
	public static function getAllTabs() {
		return [
			self::TAB_INFORMATION, self::TAB_ADMINISTRATIVE, self::TAB_ADVANTAGES, self::TAB_TECHNICALDATA, self::TAB_ACTIONS,
			self::TAB_POSITIONINGS , self::TAB_DELIVERIES, self::TAB_TIMESREPORTS, self::TAB_EXPENSESREPORTS,
			self::TAB_ABSENCESREPORTS, self::TAB_ABSENCESACCOUNTS
		];
	}

	/**
	 * does the resource have a contract ongoing
	 * @return bool
	 */
	public function hasContract() {
		return boolval($this->contract);
	}

	/**
	 * is the resource an external consultant
	 * @return bool
	 */
	public function isExternalConsultant() {
		return $this->typeOf == self::TYPE_EXTERNAL_RESOURCE;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPING;
	}

	/**
	 * @return int|null
	 */
	public function getManagerID() {
		if($this->mainManager) return intval($this->mainManager->getID());
		else return null;
	}

	/**
	 * @return int|null
	 */
	public function getHrManagerID() {
		if($this->hrManager) return intval($this->hrManager->getID());
		else return null;
	}

	/**
	 * @return string
	 */
	public function getCandidateReference() {
		if($this->candidate) return $this->candidate->getReference();
		else return null;
	}

	public static function serializeLanguages($value) {
		return implode('#', array_map(function($value){
			/** @var Language $value */
			return $value->language.'|'.$value->level;
		}, $value));
	}

	public static function unserializeLanguages($value) {
		return array_map(function($value){
			list($language, $level) = explode('|', $value);
			return new Language([
				'language' => $language,
				'level' => $level
			]);
		},explode('#', $value));
	}

	public static function serializeTools($value) {
		return Tools::serializeDoubleArray(array_map(function($value){
			/** @var Tool $value */
			return [$value->tool, $value->level];
		}, $value));
	}

	public static function unserializeTools($value) {
		return array_map(function($value){
			list($tool, $level) = explode('|', $value);
			return new Tool([
				'tool' => $tool,
				'level' => $level
			]);
		},explode('#', $value));
	}

	public static function serializeAccount($value) {
		if($value == BM::USER_TYPE_ADMINISTRATOR && !BM::isCustomerInterfaceActive())
			$subscription = BM::DB_TYPE_ROOT;
		else
			$subscription = Tools::reverseMapData($value, BM::USER_TYPE_MAPPING);
		return $subscription;
	}

	public static function unserializeAccount($value){
		$subscription = Tools::mapData($value, BM::USER_TYPE_MAPPING);
		if($subscription == BM::USER_TYPE_ADMINISTRATOR && !BM::isCustomerInterfaceActive())
			$subscription = BM::USER_TYPE_ROOT;
		return $subscription;
	}

	public static function serializeValidations($data) {
		return Validations::serializeValidationWorkflow($data);
	}

	public static function unserializeValidations($string) {
		return Validations::unserializeValidationWorkflow( $string );
	}

	public static function serializeActivityExpensesStartDate($data) {
		if($data == null)
			$string = 'hiringDate';
		else if($data == Tools::MAX_DATE)
			$string = 'inactive';
		else
			$string = $data;
		return $string;
	}

	public static function unserializeActivityExpensesStartDate($string) {
		switch($string) {
			case 'inactive':$value = Tools::MAX_DATE;break;
			case 'hiringDate':$value = null;break;
			default:$value = $string;break;
		}
		return $value;
	}

	public static function serializeWorkUnitRate($data) {
		if($data == null)
			$value = 'notUsed';
		else
			$value = floatval($data);
		return $value;
	}

	public static function unserializeWorkUnitRate($data) {
		if($data == 'notUsed')
			$value = null;
		else
			$value = $data;
		return $value;
	}

	const MAPPER_MENUPATH = [
		'tableau-de-bord/liste-candidats' => 'candidates',
		'tableau-de-bord/liste-ressources' => "resources",
		'tableau-de-bord/liste-crm' => "crm",
		'tableau-de-bord/liste-besoins' => "opportunities",
		'tableau-de-bord/liste-produits' => "products",
		'tableau-de-bord/liste-projets' => "projects",
		'tableau-de-bord/achats' => "purchases",
		'tableau-de-bord/liste-positionnements' => "positionings",
		'tableau-de-bord/liste-temps-frais-absences' => "activityExpenses",
		'tableau-de-bord/liste-commandes-factures' => "billing",
		'tableau-de-bord/rapports-activite' => "reporting",
		'tableau-de-bord/liste-actions' => "actions",
	];

	public static function unserializeNavigation($data) {
		$data = Tools::unserializeArray($data);
		foreach($data as &$entry) {
			if(preg_match('/^[0-9]+$/', $entry)) {
				$entry = new App(['id' => $entry]);
			}else {
				$entry = Tools::mapData($entry, self::MAPPER_MENUPATH);
				if(!$entry) $entry = 'dashboard';
			}
		}
		return $data;
	}

	public static function serializeValidation($data) {
		$array = [];
		foreach($data as $entry){
			if($entry instanceof App) $array[] = $entry->id;
			else $array[] = Tools::reverseMapData($entry, self::MAPPER_MENUPATH);
		}
		return Tools::serializeArray(array_filter($array));
	}

	public function hasStartedActivity(){
		return $this->startTimesExpenses != '3000-01-01';
	}
}
