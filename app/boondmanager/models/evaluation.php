<?php
/**
 * evaluation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\PublicFieldsInterface;
use Wish\Models\PublicFieldsTrait;

class Evaluation implements PublicFieldsInterface{
	use PublicFieldsTrait;

	public $evaluation, $criteria;

	const MAPPER = [
		'evaluation' => ['name' => 'evaluation', 'type' => self::TYPE_STRING],
		'criteria'   => ['name' => 'criteria', 'type' => self::TYPE_INT]
	];

	public function __construct($evaluation, $criteria)
	{
		$this->evaluation = $evaluation;
		$this->criteria = $criteria;
	}

	public static function getPublicFieldsDefinition(){
		return self::MAPPER;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return [
			'evaluation' => $this->evaluation,
			'criteria' => $this->criteria
		];
	}
}
