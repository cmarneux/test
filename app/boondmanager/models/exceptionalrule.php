<?php
/**
 * exceptionalrule.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class ExceptionalRule
 */
class ExceptionalRule extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'exceptionalrule';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
		    'ID_REGLEEXCEPTION'  => ['name' => 'id', 'type' => self::TYPE_STRING],
            'REGLE_NAME'         => ['name' => 'name', 'type' => self::TYPE_STRING],
			'REGLE_REF'          => ['name' => 'reference', 'type' => self::TYPE_STRING],
			'REGLE_TARIF'        => ['name' => 'priceExcludingTaxOrPriceRate', 'type' => self::TYPE_FLOAT],
			'REGLE_COUT'         => ['name' => 'grossCostOrSalaryRate', 'type' => self::TYPE_FLOAT],
			'REGLE_ETAT'         => ['name' => 'state', 'type' => self::TYPE_BOOLEAN],
		];
	}
}
