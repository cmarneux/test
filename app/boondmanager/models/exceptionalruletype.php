<?php
/**
 * exceptionalruletype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class ExceptionalRuleType
 * @property string id
 * @property string reference
 * @property string name
 * @property float priceExcludingTaxOrPriceRate
 * @property float grossCostOrSalaryRate
 * @property boolean state
 * @package BoondManager\Models
 */
class ExceptionalRuleType extends ModelJSONAPI {

	protected $_jsonType = 'exceptionalruletype';

	const MAPPER = [
		'ID_REGLEEXCEPTION'     => ['name' => 'id', 'type' => self::TYPE_STRING],
		'REGLE_REF'        		=> ['name' => 'reference', 'type' => self::TYPE_STRING],
		'REGLE_NAME'            => ['name' => 'name', 'type' => self::TYPE_STRING],
		'REGLE_TARIF'    		=> ['name' => 'priceExcludingTaxOrPriceRate', 'type' => self::TYPE_FLOAT],
		'REGLE_COUT'          	=> ['name' => 'grossCostOrSalaryRate', 'type' => self::TYPE_FLOAT],
		'REGLE_ETAT' 			=> ['name' => 'state', 'type' => self::TYPE_BOOLEAN]
	];

	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
