<?php
/**
 * exceptionalscales.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class ExceptionalScale
 * @package BoondManager\Models\MySQL\RowObject
 * @property int reference
 * @property string name
 * @property mixed state
 * @property int parentType // PARENT_TYPE
 * @property int parentId // ID_PARENT
 * @property Rules[] $exceptionalRules
 * @property Company|Agency dependsOn
 * @package BoondManager\Models
 */
class ExceptionalScales extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonIDAttribute = 'reference';

	const MAPPER = [
		'reference'        => ['name' => 'reference', self::TYPE_STRING],
		'name'             => ['name' => 'name', self::TYPE_STRING],
		'exceptionalRules' => ['name' => 'exceptionalRules', self::TYPE_ARRAY]
	];

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition(){
		return self::MAPPER;
	}

	public function initRelationships() {
		$this->setRelationships('dependsOn', 'dependsOn', '');
	}
}
