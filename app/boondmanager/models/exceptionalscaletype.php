<?php
/**
 * exceptionalscaletype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class ExceptionalScaleType
 * @property string reference
 * @property string name
 * @property WorkUnitType[] workUnitTypes
 * @property bool default
 * @property ExceptionalRule[] exceptionalRuleTypes
 * @property Agency agency
 * @package BoondManager\Models
 */
class ExceptionalScaleType extends ModelJSONAPI {

	protected $_jsonType = 'exceptionalscaletype';

	const MAPPER = [
		'reference'        => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'name'             => ['name' => 'name', 'type' => self::TYPE_STRING],
		'workUnitTypes'    => ['name' => 'workUnitTypes', 'type' => self::TYPE_ARRAY],
		'default'          => ['name' => 'default', 'type' => self::TYPE_BOOLEAN],
		'exceptionalRuleTypes' => ['name' => 'exceptionalRuleTypes', 'type' => self::TYPE_ARRAY]
	];

	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('dependsOn', 'dependsOn', '');
	}
}
