<?php
/**
 * expense.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int $id
 * @property float FRAIS_MONTANT
 * @property float amountIncludingTax
 * @property float numberOfKilometers
 * @property string FRAIS_TYPE
 * @property ExpenseType expenseType
 * @property string activityType
 * @property float $tax
 * @property bool isKilometricExpense
 * @property string startDate
 * @property string title
 * @property int currency
 * @property float exchangeRate
 * @property int number
 * @property bool reinvoiced
 * @property int row
 * @property Project project
 * @property Delivery delivery
 * @property Batch batch
 * @package BoondManager\Models
 */
class Expense extends ModelJSONAPI {
	/**#@+
	 * @var string FRAIS_TYPE
	 */
	const
		CATEGORY_ACTUAL = 'actual', // frais réels
		CATEGORY_FIXED = 'fixed';   // frais forfaitaires
	/**#@-*/

	const
		TYPE_PRODUCTION = 'production',
		TYPE_INTERNAL = 'internal',
		TYPE_ABSENCE = 'absence';

	const DB_TYPE_INTERNAL = -2;
	const DB_TYPE_ABSENCE = -1;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'expense';

	const MAPPER = [
		'ID_FRAIS'            => ['name' => 'id', 'type' => self::TYPE_INT],
		'FRAIS_TYPE'          => ['name' => 'category', 'type' => self::TYPE_STRING],
		'expenseType'         => ['name' => 'expenseType', 'type' => self::TYPE_OBJECT], //~ Combinaison de TAB_TYPEFRAIS.(TYPEFRS_REF, TYPEFRS_NAME, TYPEFRS_TVA)
		'ID_LIGNEFRAIS'       => ['name' => 'row', 'type' => self::TYPE_INT],
		'FRAIS_DATE'          => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'FRAIS_REFACTURE'     => ['name' => 'reinvoiced', 'type' => self::TYPE_BOOLEAN],
		'amountIncludingTax'  => ['name' => 'amountIncludingTax', 'type' => self::TYPE_FLOAT],
		'numberOfKilometers'  => ['name' => 'numberOfKilometers', 'type' => self::TYPE_FLOAT],
		'isKilometricExpense' => ['name' => 'isKilometricExpense', 'type' => self::TYPE_BOOLEAN],
		'FRAIS_TVA'           => ['name' => 'tax', 'type' => self::TYPE_FLOAT],
		'FRAIS_NUM'           => ['name' => 'number', 'type' => self::TYPE_INT],
		'FRAIS_INTITULE'      => ['name' => 'title', 'type' => self::TYPE_STRING],
		'FRAIS_DEVISE'        => ['name' => 'currency', 'type' => self::TYPE_INT],
		'FRAIS_CHANGE'        => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'activityType'        => ['name' => 'activityType', 'type' => self::TYPE_STRING],
	];

	/**
	 * callback function after a key/field is updated
	 * @param $updateKey
	 * @deprecated
	 */
	protected function onUpdatedData($updateKey, $value = null, $oldvalue = null){
		switch($updateKey){
			case 'category':
				if(!is_string($this->category)) $this->category = [
					self::CATEGORY_ACTUAL,
					self::CATEGORY_FIXED,
				][$value];
				break;
			// FRAIS_VALUE => alias de FRAIS_MONTANT alias FRAISREEL_MONTANT
			// just in case ...
			case 'FRAISREEL_MONTANT':
			case 'FRAIS_VALUE':
				$this->FRAIS_MONTANT = $value;
				break;
		}
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships() {
		$this->setRelationships('ID_LISTEFRAIS', 'expensesReport', ExpensesReport::class);
		$this->setRelationships('ID_MISSIONPROJET', 'delivery', Delivery::class);
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setRelationships('ID_LOT', 'batch', Batch::class);
	}
}
