<?php
/**
 * expensedetail.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * Class ExpenseDetail
 * @package BoondManager\Models\MySQL\RowObject
 * @property int id
 * @property string periodicity
 * @property float netAmount
 * @property ExpenseType expenseType
 * @property Agency agency
 * @package BoondManager\Models
 */
class ExpenseDetail extends ModelJSONAPI {

	const PERIODICITY_MONTHLY = 'monthly';
	const PERIODICITY_DAILY = 'daily';


	const MAPPER = [
		'ID_FRAISDETAILS'       => ['name' => 'id', 'type' => self::TYPE_STRING],
		'FRSDETAILS_TYPE'       => ['name' => 'periodicity', 'type' => self::TYPE_STRING, 'mapper' => [self::PERIODICITY_DAILY, self::PERIODICITY_MONTHLY]],
		'FRSDETAILS_MONTANT'    => ['name' => 'netAmount', 'type' => self::TYPE_FLOAT],
		'expenseType'           => ['name' => 'expenseType', 'type' => self::TYPE_OBJECT]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition(){
		return self::MAPPER;
	}

	public function initRelationships() {
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
	}
}
