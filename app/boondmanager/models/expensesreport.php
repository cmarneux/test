<?php
/**
 * ExpensesReport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractActivityReport;
use BoondManager\Lib\Models\HasAgencyTrait;

/**
 * class ExpensesReport
 * @property int $id
 * @property string $state
 * @property string $term
 * @property RatePerKilometerType $ratePerKilometerType
 * @property mixed $advance
 * @property boolean $paid
 * @property int $currencyAgency
 * @property float $exchangeRateAgency
 * @property Validation[] $valivalidations
 * @property Employee resource
 * @property Agency agency
 * @property Expense[] $actualExpenses
 * @property Expense[] $fixedExpenses
 * @property Proof[] files
 * @property TimesReport timesReport
 * @property Project[] projects
 * @property Order[] orders
 * @property Validation[] validations
 * @package BoondManager\Models
 */
class ExpensesReport extends AbstractActivityReport{
	use HasAgencyTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'expensesreport';

	const ERROR_ROW_INCONSISTENCY = 3800;
	const ERROR_ENDDATE_STARTDATE = 3801;
	const ERROR_DELIVERY_INCOMPATIBLE_WITH_WORKUNITTYPE = 3802;
	const ERROR_BATCH_INCOMPATIBLE_WITH_WORKUNITTYPE = 3803;
	const ERROR_PROJECT_INCOMPATIBLE_WITH_WORKUNITTYPE = 3804;
	const ERROR_DELIVERY_ID_NOT_ALLOWED = 3805;
	const ERROR_BATCH_ID_NOT_ALLOWED = 3806;
	const ERROR_PROJECT_ID_NOT_ALLOWED = 3807;

	const MAPPER = [
		'ID_LISTEFRAIS'           => ['name' => 'id', 'type' => self::TYPE_INT],
		'LISTEFRAIS_DATE'         => ['name' => 'term', 'type' => self::TYPE_TERM],
		'LISTEFRAIS_COMMENTAIRES' => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'LISTEFRAIS_CLOTURE'      => ['name' => 'closed', 'type' => self::TYPE_BOOLEAN],
		//'LISTEFRAIS_BKMVALUE'     => ['name' => 'ratePerKilometer', 'type' => self::TYPE_FLOAT],
		'ratePerKilometer'        => ['name' => 'ratePerKilometer', 'type' => self::TYPE_OBJECT],
		'LISTEFRAIS_REGLE'        => ['name' => 'paid', 'type' => self::TYPE_BOOLEAN],
		'LISTEFRAIS_AVANCE'       => ['name' => 'advance', 'type' => self::TYPE_FLOAT],
		'LISTEFRAIS_DEVISEAGENCE' => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'LISTEFRAIS_CHANGEAGENCE' => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'LISTEFRAIS_ETAT'         => ['name' => 'state', 'type' => self::TYPE_STRING, 'mapper' => Validation::STATE_MAPPER],
		'actualExpenses'          => ['name' => 'actualExpenses', 'type' => self::TYPE_ARRAY],
		'fixedExpenses'           => ['name' => 'fixedExpenses', 'type' => self::TYPE_ARRAY],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function initRelationships(){
		parent::initRelationships();
		$this->setGroupedRelationships('files', 'files');
		$this->setRelationships('timesReport', 'timesReport', TimesReport::class);
		$this->setGroupedRelationships('orders', 'orders');
		$this->setGroupedRelationships('projects', 'projects');
	}

	public function getStartPeriod(){
		return $this->term;
	}

	public function getEndPeriod(){
		$date = explode('-', $this->term);
		return date('Y-m-d', mktime(0,0,0, $date[1]+1, 0, $date[0]));
	}

	public function getValidationWorkflow(){
		return $this->resource->expensesReportsWorkflow ? $this->resource->expensesReportsWorkflow : $this->agency->expensesReportsWorkflow;
	}

	/**
	 * @return mixed
	 */
	public function getHrManagerID() {
		return $this->resource->getHrManagerID();
	}

	/**
	 * @return mixed
	 */
	public function getManagerID() {
		return $this->resource->getManagerID();
	}

	/**
	 * @return mixed
	 */
	public function getPoleID() {
		return $this->resource->getPoleID();
	}
}
