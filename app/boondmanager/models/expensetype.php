<?php
/**
 * expensetype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class ExpenseTypeOnlyReference
 * @property string id
 * @property string reference
 * @property float taxRate
 * @property string name
 * @property bool state
 * @package BoondManager\Models
 */
class ExpenseType extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'expensetype';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_TYPEFRAIS' => ['name' => 'id', 'type' => self::TYPE_STRING],
			'TYPEFRS_REF'  => ['name' => 'reference', 'type' => self::TYPE_INT],
			'TYPEFRS_TVA'  => ['name' => 'taxRate', 'type' => self::TYPE_FLOAT],
			'TYPEFRS_NAME' => ['name' => 'name', 'type' => self::TYPE_STRING],
			'TYPEFRS_ETAT' => ['name' => 'state', 'type' => self::TYPE_BOOLEAN]
		];
	}
}
