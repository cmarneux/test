<?php
/**
 * file.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractFile;
use BoondManager\Databases\Local;

/**
 * class Document
 * @property int $id
 * @property string $name
 * @property Candidate|Employee dependsOn
 */
class File extends AbstractFile
{
	const PARENT_TYPE_CANDIDATE = 'candidate';
	const PARENT_TYPE_RESOURCE = 'resource';

	const SUBTYPE = 'file';

	protected static $_jsonType = 'document';

	const MAPPER =  [
		'ID_FILE'   => ['name' => 'id', 'type' => self::TYPE_INT],
		'FILE_NAME' => ['name' => 'name', 'type' => self::TYPE_STRING]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 * @return string
	 */
	public function getParentType() {
		switch (get_class($this->dependsOn)) {
			case Employee::class: return self::PARENT_TYPE_RESOURCE;
			case Candidate::class: return self::PARENT_TYPE_CANDIDATE;
			default: return null;
		}
	}

	/**
	 * @return int
	 */
	public function getSubTypeID() {
		return Local\File::TYPE_OTHER;
	}
}
