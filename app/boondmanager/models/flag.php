<?php
/**
 * flag.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use Wish\Models\ModelJSONAPI;

/**
 * Class Flag
 * @property string id
 * @property string name
 * @property Employee mainManager
 * @package BoondManager\Models
 */
class Flag extends ModelJSONAPI  implements HasManagerInterface {
	use HasManagerTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'flag';


	const REF_PREFIX = 'FLAG';

	protected function initRelationships(){
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_USERFLAG'  => ['name' => 'id', 'type' => self::TYPE_STRING],
			'FLAG_LIBELLE' => ['name'=>'name', 'type'=>self::TYPE_STRING]
		];
	}
}
