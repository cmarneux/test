<?php
/**
 * flags.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Models;


/**
 * Class Flags
 *
 * Chaque élément du tableau peut avoir 2 formats possibles :
 * - Si le flag appartient à l'utilisateur : array([TAB_USERFLAG.ID_USERFLAG](../bddclient/classes/TAB_USERFLAG.html#property_ID_USERFLAG), [TAB_USERFLAG.FLAG_LIBELLE](../bddclient/classes/TAB_USERFLAG.html#property_FLAG_LIBELLE))
 * - Si le flag n'appartient pas à l'utilisateur : array([TAB_USERFLAG.ID_USERFLAG](../bddclient/classes/TAB_USERFLAG.html#property_ID_USERFLAG), [TAB_USERFLAG.FLAG_LIBELLE](../bddclient/classes/TAB_USERFLAG.html#property_FLAG_LIBELLE), [TAB_PROFIL.PROFIL_NOM](../bddclient/classes/TAB_PROFIL.html#property_PROFIL_NOM).' '.[TAB_PROFIL.PROFIL_PRENOM](../bddclient/classes/TAB_PROFIL.html#property_PROFIL_PRENOM))
 *
 *
 * @package BoondManager\Models
 */
class FlagsList{

    /**
     * the list of flags
     * @var Flag[]
     */
    private $flags = array();

    /**
     * add a flag
     * @param Flag $flag
     */
    public function addFlag($flag){
        $this->flags[] = $flag;
    }

	/**
	 * sort the flag based on a given user id
	 * @param $userID
	 * @return $this
	 */
	public function sortWithUser($userID){
		usort($this->flags, function ($a, $b) use ($userID){
			if($a['ID_USERFLAG'] != $b['ID_USERFLAG']){
				if($a['ID_USERFLAG'] == $userID) return 1;
				else if($b['ID_USERFLAG'] == $userID) return -1;
			}

			// tri secondaire
			if($a['ID_FLAG'] > $b['ID_FLAG']) return 1;
			else if($a['ID_FLAG'] == $b['ID_FLAG']) return 0;
			else return -1;
		});
		return $this;
	}

    /**
     * return a formatted list for the front
     * @return array
     */
    public function getFormattedList(){
		$list = [];
		foreach($this->flags as $f){
			$list[] = [
				'id' => $f->id,
				'name' => $f->name,
				'mainManager' => [
					'id' => $f->mainManager->id,
					'firstName' => $f->mainManager->firstName,
					'lastName' => $f->mainManager->lastName
				]
			];
		}
		return $list;
    }

    /**
     * get all flags
     * @return array
     */
    public function getFlags(){
        return $this->flags;
    }
}
