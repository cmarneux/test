<?php
/**
 * gedserver.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * class GEDServer
 * @property string url
 * @property string directory
 * @property bool useAsDefault
 * @package BoondManager\Models
 */
class GEDServer extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'GED_URL' => ['name'=>'url', 'type'=>self::TYPE_STRING],
			'GED_PATH' => ['name'=>'directory', 'type'=>self::TYPE_STRING],
			'GED_DEFAULT' => ['name'=>'useAsDefault', 'type'=>self::TYPE_BOOLEAN]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
