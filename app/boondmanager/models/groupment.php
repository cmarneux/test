<?php
/**
 * groupment.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Delivery
 * @property string id
 * @property Project project
 * @property int state
 * @property string title
 * @property string startDate date
 * @property string endDate date
 * @property float numberOfDaysInvoicedOrQuantity
 * @property float numberOfDaysFree
 * @property float averageDailyPriceExcludingTax
 * @property float additionalTurnoverExcludingTax
 * @property float additionalCostsExcludingTax
 * @property float averageDailyCost
 * @property float turnoverProductionExcludingTax
 * @property float costsProductionExcludingTax
 * @property float marginProductionExcludingTax
 * @property float profitabilityProduction
 * @property float regularTimesSimulated
 * @property float regularTimesProduction
 * @property float exceptionalTimesProduction
 * @property float exceptionalCalendarsProduction
 * @property float expensesSimulated
 * @property float expensesProduction
 * @property float turnoverSimulatedExcludingTax
 * @property float costsSimulatedExcludingTax
 * @property float marginSimulatedExcludingTax
 * @property float profitabilitySimulated
 * @property int numberOfOrders
 * @property int numberOfCorrelatedOrders
 * @property int loadDistribution
 * @property bool canReadProject
 * @property bool canWriteProject
 * @property Document[] files
 * @property GroupmentDelivery[] deliveries
 *
 */
class Groupment extends ModelJSONAPI{
	/**
	 * @var string
	 */
	protected static $_jsonType = 'groupment';

	const REF_PREFIX = 'MIS';

	const MAPPER = [
		'ID_MISSIONPROJET'               => ['name' => 'id', 'type' => self::TYPE_STRING],
		'MP_DEBUT'                       => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'MP_FIN'                         => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'MP_NOM'                         => ['name' => 'title', 'type' => self::TYPE_STRING],
		'MP_TYPE'                        => ['name' => 'state', 'type' => self::TYPE_INT],
		'MP_NBJRSFACTURE'                => ['name' => 'numberOfDaysInvoicedOrQuantity', 'type' => self::TYPE_FLOAT],
		'NBJRSGRATUIT'                   => ['name' => 'numberOfDaysFree', 'type' => self::TYPE_FLOAT],
		'MP_TARIF'                       => ['name' => 'averageDailyPriceExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_TARIFADDITIONNEL'            => ['name' => 'additionalTurnoverExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_INVESTISSEMENT'              => ['name' => 'additionalCostsExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_CJM'                         => ['name' => 'averageDailyCost', 'type' => self::TYPE_FLOAT],
		'MP_PRJM'                        => ['name' => 'averayDailyContractCost', 'type' => self::TYPE_FLOAT],
		'MP_COMMENTAIRES'                => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'MP_CONDITIONS'                  => ['name' => 'deliveries', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeConditions', 'unserializeCallback' => 'unserializeConditions'],
		'TOTAL_CASIMUHT'                 => ['name' => 'turnoverSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_COUTSIMUHT'               => ['name' => 'costsSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_MARGESIMUHT'              => ['name' => 'marginSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_RENTASIMU'                => ['name' => 'profitabilitySimulated', 'type' => self::TYPE_FLOAT],

		'turnoverProductionExcludingTax' => ['name' => 'turnoverProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsProductionExcludingTax'    => ['name' => 'costsProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'marginProductionExcludingTax'   => ['name' => 'marginProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'profitabilityProduction'        => ['name' => 'profitabilityProduction', 'type' => self::TYPE_FLOAT],

		'ITEM_TYPE'                      => ['name' => 'loadDistribution', 'type' => self::TYPE_STRING, 'mapper' => self::DISTRIBUTION_MAPPER],
		//APIs project/{id}/productivity
		'regularTimesSimulated'          => ['name' => 'regularTimesSimulated', 'type' => self::TYPE_FLOAT],
		'regularTimesProduction'         => ['name' => 'regularTimesProduction', 'type' => self::TYPE_FLOAT],
		'exceptionalTimesProduction'     => ['name' => 'exceptionalTimesProduction', 'type' => self::TYPE_FLOAT],
		'exceptionalCalendarsProduction' => ['name' => 'exceptionalCalendarsProduction', 'type' => self::TYPE_FLOAT],
		'expensesSimulated'              => ['name' => 'expensesSimulated', 'type' => self::TYPE_FLOAT],
		'expensesProduction'             => ['name' => 'expensesProduction', 'type' => self::TYPE_FLOAT],
		//APIs deliveries, billing-deliveries-purchases-balance
		'NB_BDC'           => ['name' => 'numberOfOrders', 'type' => self::TYPE_INT],
		//~ utilisés par l'api billing-deliveries-purchases-balance
		'NB_COR'           => ['name' => 'numberOfCorrelatedOrders', 'type' => self::TYPE_INT],

		//APIs billing-deliveries-purchases-balance
		'canReadProject'   => ['name' => 'canReadProject', 'type' => self::TYPE_BOOLEAN],
		'canWriteProject'  => ['name' => 'canWriteProject', 'type' => self::TYPE_BOOLEAN],

		// resources/deliveries
		'canReadDelivery'  => ['name' => 'canReadDelivery', 'type' => self::TYPE_BOOLEAN],
		'canWriteDelivery' => ['name' => 'canWriteDelivery', 'type' => self::TYPE_BOOLEAN],
	];

	const
		TYPE_PROPORTIONAL = 'proportional',
		TYPE_WEIGHTED = 'weighted',
		TYPE_MANUAL = 'manual';

	const DISTRIBUTION_MAPPER = [
		5 => self::TYPE_PROPORTIONAL,
		6 => self::TYPE_WEIGHTED,
		7 => self::TYPE_MANUAL,
	];

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_GROUPMENT_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE = 4800,
		ERROR_GROUPMENT_DATE_CANT_BE_SET_IF_SUB_DELIVERIES = 4801,
		ERROR_GROUPMENT_DELIVERY_MUST_BE_FREE_OF_GROUPMENT = 4802,
		ERROR_GROUPMENT_WEIGHTING_CANT_BE_SET = 4803,
		ERROR_GROUPMENT_SCHEDULE_CANT_BE_SET = 4804,
		ERROR_GROUPMENT_DELIVERY_MUST_BELONGS_TO_SAME_PROJECT = 4805;
	/**#@-*/

	/**#@+
	 * @var int Value of forecast state
	 */
	const
		STATE_FORECAST = 1;
	/**#@-*/

	public function initRelationships(){
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setGroupedRelationships('files', 'files');
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	public static function serializeConditions($array) {
		return Tools::serializeDoubleArray(array_map(function($delPond) {
			/** @var GroupmentDelivery $delPond */
			return [$delPond->delivery->id, $delPond->weighting, $delPond->schedule];
		}, $array));
	}

	public static function unserializeConditions($string) {
		$data = Tools::unserializeDoubleArray($string);
		return array_map(function($array) {
			return new GroupmentDelivery([
				'delivery' => new Delivery(['id' => $array[0]]),
				'weighting' => $array[1],
				'schedule' => $array[2]
			]);
		}, $data);
	}

	public function calculateData(){
		$this->calculateDates();
		$this->calculateDaysFree();
		return $this;
	}

	/**
	 * calculate start and end dates based on deliveries
	 * @return $this
	 */
	public function calculateDates(){

		if($this->deliveries) {

			$minDate = null;
			$maxDate = null;

			foreach ($this->deliveries as $deliveryPonderation) {
				$startTime = strtotime($deliveryPonderation->delivery->startDate);
				$endTime   = strtotime($deliveryPonderation->delivery->endDate);

				if (is_null($minDate)) $minDate = $startTime;
				if (is_null($maxDate)) $maxDate = $endTime;

				$minDate = min($minDate, $startTime);
				$maxDate = max($maxDate, $endTime);
			}

			if ($minDate) $this->startDate = date('Y-m-d', $minDate);
			if ($maxDate) $this->endDate = date('Y-m-d', $maxDate);
		}

		return $this;
	}

	/**
	 * calculate number of days free based on deliveries
	 * @return $this
	 */
	public function calculateDaysFree(){
		$this->calculateWeighting();

		if($this->deliveries) {
			$charge = 0;
			foreach ($this->deliveries as $delivery) {
				$charge += $delivery->schedule;
			}

			$this->numberOfDaysFree = -min($charge, $this->numberOfDaysInvoicedOrQuantity);
		}
		return $this;
	}

	/**
	 * calculate weighting (Is automaticaly called in calculateDaysFree())
	 * @return $this
	 */
	public function calculateWeighting(){
		$countDelivery = count($this->deliveries);
		foreach($this->deliveries as $delivery){
			switch ($this->loadDistribution) {
				case self::TYPE_PROPORTIONAL:
					$delivery->weighting = 0; // 1 / $this->deliveries->count();
					$delivery->schedule = $this->numberOfDaysInvoicedOrQuantity / $countDelivery;
					break;
				case self::TYPE_WEIGHTED :
					$delivery->schedule = $this->numberOfDaysInvoicedOrQuantity * $delivery->weighting / 100 ;
					break;
				default : // MANUAL
					$delivery->weighting = 0;
			}
		}
		return $this;
	}
}
