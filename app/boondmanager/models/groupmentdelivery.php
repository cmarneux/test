<?php
/**
 * groupmentdelivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;


use Wish\Models\ModelJSON;

/**
 * Class DeliveryPonderation
 * @property float weighting
 * @property float schedule
 * @property Delivery delivery
 * @package BoondManager\Models
 */
class GroupmentDelivery extends ModelJSON {
	const MAPPER = [
		'delivery' => ['name' => 'delivery', 'type' => self::TYPE_OBJECT],
		'schedule' => ['name' => 'schedule', 'type' => self::TYPE_FLOAT],
		'weighting' => ['name' => 'weighting', 'type' => self::TYPE_FLOAT]
	];

	public static function getPublicFieldsDefinition(){
		return self::MAPPER;
	}
}
