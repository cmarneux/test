<?php
/**
 * inputid.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class Id
 * @property int id,
 * @package BoondManager\Models\MySQL\RowObject
 */
class Id extends \Wish\Models\ModelJSON {

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return [
			'id' => ['name'=>'id', 'type'=>self::TYPE_INT]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
