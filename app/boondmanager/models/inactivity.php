<?php
/**
 * inactivity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Delivery
 * @property string id
 * @property Employee resource
 * @property Contract contract
 * @property int state
 * @property string startDate date
 * @property string endDate date
 * @property float numberOfDaysInvoicedOrQuantity
 * @property float dailyExpenses
 * @property float monthlyExpenses
 * @property float additionalTurnoverExcludingTax
 * @property float additionalCostsExcludingTax
 * @property float averageDailyCost
 * @property float averageDailyContractCost
 * @property float turnoverProductionExcludingTax
 * @property float costsProductionExcludingTax
 * @property float marginProductionExcludingTax
 * @property float profitabilityProduction
 * @property float regularTimesSimulated
 * @property float regularTimesProduction
 * @property float exceptionalTimesProduction
 * @property float exceptionalCalendarsProduction
 * @property float expensesSimulated
 * @property float expensesProduction
 * @property float costsSimulatedExcludingTax
 * @property int numberOfOrders
 * @property int numberOfCorrelatedOrders
 * @property float occupationRate
 * @property bool canReadProject
 * @property bool canWriteProject
 * @property string inactivityType
 * @property Document[] files
 * @property expenseDetail[] expensesDetails
 * @property Delivery[] deliveries
 * @property Contract[] contracts
 * @property Project project
 *
 */
class Inactivity extends ModelJSONAPI{
	const INACTIVITY_MAPPER = [
		-2 => self::INACTIVITY_INTERNAL,
		-1 => self::INACTIVITY_ABSENCE
	];
	/**#@-*/
	const INACTIVITY_INTERNAL = 'internal';
	const INACTIVITY_ABSENCE = 'absence';
	/**
	 * @var string
	 */
	protected static $_jsonType = 'inactivity';

	const REF_PREFIX = 'MIS';

	const MAPPER = [
		'ID_MISSIONPROJET'               => ['name' => 'id', 'type' => self::TYPE_STRING],
		'MP_DEBUT'                       => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'MP_FIN'                         => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'MP_TYPE'                        => ['name' => 'state', 'type' => self::TYPE_INT],
		'MP_NOM'                         => ['name' => 'title', 'type' => self::TYPE_STRING],
		'MP_NBJRSFACTURE'                => ['name' => 'numberOfDaysInvoicedOrQuantity', 'type' => self::TYPE_FLOAT],
		'MP_FRSJOUR'                     => ['name' => 'dailyExpenses', 'type' => self::TYPE_FLOAT],
		'MP_FRSMENSUEL'                  => ['name' => 'monthlyExpenses', 'type' => self::TYPE_FLOAT],
		'MP_TARIFADDITIONNEL'            => ['name' => 'additionalTurnoverExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_INVESTISSEMENT'              => ['name' => 'additionalCostsExcludingTax', 'type' => self::TYPE_FLOAT],
		'MP_CJM'                         => ['name' => 'averageDailyCost', 'type' => self::TYPE_FLOAT],
		'MP_PRJM'                        => ['name' => 'averayDailyContractCost', 'type' => self::TYPE_FLOAT],
		'MP_COMMENTAIRES'                => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'MP_NBJRSOUVRE'                  => ['name' => 'numberOfWorkingDays', 'type' => self::TYPE_INT],
		'TOTAL_COUTSIMUHT'               => ['name' => 'costsSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsProductionExcludingTax'    => ['name' => 'costsProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'occupationRate'                 => ['name' => 'occupationRate', 'type' => self::TYPE_FLOAT],
		//APIs project/{id}/productivity
		'regularTimesSimulated'          => ['name' => 'regularTimesSimulated', 'type' => self::TYPE_FLOAT],
		'regularTimesProduction'         => ['name' => 'regularTimesProduction', 'type' => self::TYPE_FLOAT],
		'exceptionalTimesProduction'     => ['name' => 'exceptionalTimesProduction', 'type' => self::TYPE_FLOAT],
		'exceptionalCalendarsProduction' => ['name' => 'exceptionalCalendarsProduction', 'type' => self::TYPE_FLOAT],
		'expensesSimulated'              => ['name' => 'expensesSimulated', 'type' => self::TYPE_FLOAT],
		'expensesProduction'             => ['name' => 'expensesProduction', 'type' => self::TYPE_FLOAT],
		//APIs deliveries, billing-deliveries-purchases-balance
		'NB_BDC'           => ['name' => 'numberOfOrders', 'type' => self::TYPE_INT],
		//~ utilisés par l'api billing-deliveries-purchases-balance
		'NB_COR'           => ['name' => 'numberOfCorrelatedOrders', 'type' => self::TYPE_INT],

		//APIs billing-deliveries-purchases-balance
		'canReadProject'   => ['name' => 'canReadProject', 'type' => self::TYPE_BOOLEAN],
		'canWriteProject'  => ['name' => 'canWriteProject', 'type' => self::TYPE_BOOLEAN],

		// resources/deliveries
		'canReadDelivery'  => ['name' => 'canReadDelivery', 'type' => self::TYPE_BOOLEAN],
		'canWriteDelivery' => ['name' => 'canWriteDelivery', 'type' => self::TYPE_BOOLEAN],

		'inactivityType'   => ['name' => 'inactivityType', 'type' => self::TYPE_STRING, 'removeIfEmpty' => true],
	];

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_DELIVERY_ISGROUPMENT_AND_ISINACTIVITY_CANNOT_BE_BOTH_TRUE= 3500,
		//ERROR_DELIVERY_FORCETRANSFERCREATION_AND_CANCELTRANSFER_CANNOT_BE_BOTH_TRUE= 3502,
		ERROR_DELIVERY_STARTDATE_CANNOT_BE_SET = 3501,
		ERROR_DELIVERY_ENDDATE_CANNOT_BE_SET = 3502,
		ERROR_DELIVERY_ENDDATE_MUST_BE_SUPERIOR_AT_STARTDATE= 3503,
		ERROR_DELIVERY_TITLE_CANNOT_BE_SET = 3504,
		ERROR_DELIVERY_STATE_CANNOT_BE_SET = 3505,
		ERROR_DELIVERY_NUMBEROFDAYSINVOICEDORQUANTITY_CANNOT_BE_SET = 3506,
		ERROR_DELIVERY_NUMBEROFDAYSFREE_CANNOT_BE_SET = 3507,
		ERROR_DELIVERY_DAILYEXPENSES_CANNOT_BE_SET = 3508,
		ERROR_DELIVERY_MONTHLYEXPENSES_CANNOT_BE_SET = 3509,
		ERROR_DELIVERY_NUMBEROFWORKINGDAYS_CANNOT_BE_SET = 3510,
		ERROR_DELIVERY_AVERAGEDAILYCOST_CANNOT_BE_SET = 3511,
		ERROR_DELIVERY_AVERAGEDAILYCONTRACTCOST_CANNOT_BE_SET = 3512,
		ERROR_DELIVERY_WEKKLYWORKINGHOURS_CANNOT_BE_SET = 3513,
		ERROR_DELIVERY_AVERAGEHOURLYPRICEEXCLUDINGTAX_CANNOT_BE_SET = 3514,
		ERROR_DELIVERY_FORCEAVERAGEHOURLYPRICEEXCLUDINGTAX_CANNOT_BE_SET = 3515,
		ERROR_DELIVERY_CONDITIONS_CANNOT_BE_SET = 3516,
		ERROR_DELIVERY_SLAVE_CANNOT_BE_SET_IF_FORCETRANSFER = 3517,
		//ERROR_DELIVERY_SLAVE_CANNOT_BE_SET_IF_CANCELTRANSFER = 3520,
		ERROR_DELIVERY_ADDITIONALTURNOVERANDCOSTS_CANNOT_BE_SET = 3518,
		ERROR_DELIVERY_EXPENSESDETAILS_CANNOT_BE_SET = 3519,
		ERROR_DELIVERY_ADVANTAGES_CANNOT_BE_SET = 3520,
		ERROR_DELIVERY_EXCEPTIONALSCALES_CANNOT_BE_SET = 3521,
		ERROR_DELIVERY_CANNOT_UPDATE_TURNOVERANDCOSTS = 3522,
		ERROR_DELIVERY_SLAVE_MISSING_PARAMETER = 3523;
	/**#@-*/

	/**#@+
	 * @var int Value of forecast state
	 */
	const
		STATE_FORECAST = 1;
	/**#@-*/

	public function initRelationships(){
		$this->setRelationships('ID_CONTRAT', 'contract', Contract::class);
		$this->setRelationships('ITEM_TYPE', 'resource', Employee::class);
		$this->setRelationships('project', 'project', Project::class);
		$this->setGroupedRelationships('contracts', 'contracts');
		$this->setGroupedRelationships('files', 'files');
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 * @return $this
	 */
	public function calculateData() {
		$this->calculateContractCost();
		$this->calculateExpensesFromDetails();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateContractCost() {
		if($this->contract) {
			$this->averageDailyContractCost = $this->contract->contractAverageDailyCost;
		}
		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateExpensesFromDetails() {

		if($this->expensesDetails) {
			$daily   = 0;
			$monthly = 0;
			foreach ($this->expensesDetails as $exInput) {
				if ($exInput->periodicity == ExpenseDetail::PERIODICITY_DAILY) $daily += $exInput->netAmount;
				else $monthly += $exInput->netAmount;
			}
			$this->dailyExpenses = $daily;
			$this->monthlyExpenses = $monthly;
		}

		return $this;
	}
}
