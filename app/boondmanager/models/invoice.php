<?php
/**
 * invoice.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasPoleInterface;
use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int $id
 * @property boolean creditNote
 * @property Company company
 * @property Contact contact
 * @property Order order
 * @property string startDate
 * @property string endDate
 * @property float turnoverInvoicedExcludingTax
 * @property float turnoverInvoicedIncludingTax
 * @property array activityDetails
 * @property InvoiceRecord[] invoiceRecords
 * @property Schedule termOfPayment
 */
class Invoice extends ModelJSONAPI implements HasPoleInterface , HasManagerInterface , HasAgencyInterface {

	const TAB_INFORMATION = 'information';
	const TAB_ACTIONS = 'actions';
	const TAB_FLAGS = 'attachedFlags';

	/**
	 * @var string
	 */
	protected static $_jsonType = 'invoice';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_FACTURATION'             => ['name' => 'id', 'type' => self::TYPE_INT],
			'FACT_DATE'                  => ['name' => 'date', 'type' => self::TYPE_DATE],
			'FACT_DEBUT'                 => ['name' => 'startDate', 'type' => self::TYPE_DATE],
			'FACT_FIN'                   => ['name' => 'endDate', 'type' => self::TYPE_DATE],
			'FACT_DATEREGLEMENTATTENDUE' => ['name' => 'expectedPaymentDate', 'type' => self::TYPE_DATE],
			'FACT_DATEREGLEMENTRECUE'    => ['name' => 'performedPaymentDate', 'type' => self::TYPE_DATE],
			'TOTAL_CAFACTUREHT'          => ['name' => 'turnoverInvoicedExcludingTax', 'type' => self::TYPE_FLOAT],
			'TOTAL_CAFACTURETTC'         => ['name' => 'turnoverInvoicedIncludingTax', 'type' => self::TYPE_FLOAT],
			'FACT_COMMENTAIRE'           => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
			'FACT_TAUXREMISE'            => ['name' => 'discountRate', 'type' => self::TYPE_FLOAT],
			'FACT_CLOTURE'               => ['name' => 'closed', 'type' => self::TYPE_BOOLEAN],
			'FACT_TYPEPAYMENT'           => ['name' => 'paymentMethod', 'type' => self::TYPE_INT],
			'FACT_AVOIR'                 => ['name' => 'creditNote', 'type' => self::TYPE_BOOLEAN],
			'FACT_REF'                   => ['name' => 'reference', 'type' => self::TYPE_STRING],
			'FACT_ETAT'                  => ['name' => 'state', 'type' => self::TYPE_INT],
			'FACT_DEVISE'                => ['name' => 'currency', 'type' => self::TYPE_INT],
			'FACT_CHANGE'                => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
			'FACT_DEVISEAGENCE'          => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
			'FACT_CHANGEAGENCE'          => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
			'FACT_SHOWCOMMENTAIRE'       => ['name' => 'showCommentsOnPDF', 'type' => self::TYPE_BOOLEAN],
			'canReadInvoice'             => ['name' => 'canReadInvoice', 'type' => self::TYPE_BOOLEAN],
			'canWriteInvoice'            => ['name' => 'canWriteInvoice', 'type' => self::TYPE_BOOLEAN],
			'activityDetails'            => ['name' => 'activityDetails', 'type' => self::TYPE_ARRAY],
			'invoiceRecords'             => ['name' => 'invoiceRecords', 'type' => self::TYPE_ARRAY],
		];
	}

	public function initRelationships(){
		$this->setRelationships('ID_BONDECOMMANDE', 'order', Order::class);
		$this->setRelationships('ID_ECHEANCIER', 'termOfPayment', Schedule::class);

		$this->setRelationships('ID_CRMSOCIETE', 'company', Company::class);
		$this->setRelationships('ID_CRMCONTACT', 'contact', Contact::class);
	}

	/**
	 * @return array
	 */
	public static function getAllTabs() {
		return [self::TAB_INFORMATION, self::TAB_ACTIONS, self::TAB_FLAGS];
	}

	/**
	 * @return int
	 */
	public function getAgencyID() {
		return $this->order->getAgencyID();
	}

	/**
	 * @return int
	 */
	public function getManagerID() {
		return $this->order->getManagerID();
	}

	/**
	 * @return int
	 */
	public function getPoleID() {
		return $this->order->getPoleID();
	}
}
