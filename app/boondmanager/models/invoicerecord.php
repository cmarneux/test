<?php
/**
 * invoicerecord.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class InvoiceRecord
 * @package BoondManager\Models
 * @property string id
 * @property string description
 * @property InvoiceRecordType invoiceRecordType
 * @property float taxRate
 * @property float quantity
 * @property float amountExcludingTax
 * @property float turnoverExcludingTax
 * @property float turnoverIncludingTax
 */
class InvoiceRecord extends ModelJSONAPI {

	const MAPPER = [
		'ID_ITEMFACTURE'       => ['name' => 'id', 'type' => self::TYPE_STRING],
		'ITEM_DESCRIPTION'     => ['name' => 'description', 'type' => self::TYPE_STRING],
		'ITEM_TAUXTVA'         => ['name' => 'taxRate', 'type' => self::TYPE_FLOAT],
		'ITEM_QUANTITE'        => ['name' => 'quantity', 'type' => self::TYPE_FLOAT],
		'ITEM_MONTANTHT'       => ['name' => 'amountExcludingTax', 'type' => self::TYPE_FLOAT],
		'invoiceRecordType'    => ['name' => 'invoiceRecordType', 'type' => self::TYPE_OBJECT],
		'turnoverExcludingTax' => ['name' => 'turnoverExcludingTax', 'type' => self::TYPE_FLOAT],
		'turnoverIncludingTax' => ['name' => 'turnoverIncludingTax', 'type' => self::TYPE_FLOAT],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function calculateTurnovers() {
		$this->turnoverExcludingTax = $this->amountExcludingTax * $this->quantity;
		$this->turnoverIncludingTax = $this->turnoverExcludingTax * (1 + $this->taxRate / 100);
		return $this;
	}
}
