<?php
/**
 * invoicerecordtype.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class InvoiceRecordType
 * @property string id
 * @property int reference
 * @property string name
 * @property WorkUnitType[] workUnitTypes
 * @property ExpenseType[] expenseTypes
 * @property bool schedule
 * @property bool reinvoicedPurchase
 * @property bool product
 * @package BoondManager\Models
 */
class InvoiceRecordType extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'invoicerecordtype';

	const MAPPER = [
		'ID_TYPEFACTURATION'      	=> ['name' => 'id', 'type' => self::TYPE_STRING],
		'TYPEF_REF'          		=> ['name' => 'reference', 'type' => self::TYPE_INT],
		'TYPEF_NAME'   				=> ['name' => 'name', 'type' => self::TYPE_STRING],
		'workUnitTypes' 			=> ['name' => 'workUnitTypes', 'type' => self::TYPE_ARRAY],
		'expenseTypes'         		=> ['name' => 'expenseTypes', 'type' => self::TYPE_ARRAY],
		'schedule'         			=> ['name' => 'schedule', 'type' => self::TYPE_BOOLEAN],
		'reinvoicedPurchase'        => ['name' => 'reinvoicedPurchase', 'type' => self::TYPE_BOOLEAN],
		'product'         			=> ['name' => 'product', 'type' => self::TYPE_BOOLEAN]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
