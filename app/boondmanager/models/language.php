<?php
/**
 * language.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class Language
 * @property string language
 * @property string level
 */
class Language extends \Wish\Models\ModelJSON {

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'language' => ['name' => 'language', 'type' => self::TYPE_STRING],
			'level' => ['name' => 'level', 'type' => self::TYPE_STRING]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
