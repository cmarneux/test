<?php
/**
 * login.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * class Customer
 * @property int userId
 * @property string userLogin
 * @property string customerCode
 * @package BoondManager\Models
 */
class Login extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_USER' => ['name' => 'userId', 'type' => self::TYPE_INT],
			'USER_LOGIN' => ['name' => 'userLogin', 'type' => self::TYPE_STRING],
			'CLIENT_WEB' => ['name' => 'customerCode', 'type' => self::TYPE_STRING]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
