<?php
/**
 * marker.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class Marker
 * @property int id
 * @property string $title
 * @property Employee resource
 * @property string date
 * @property float progressInDays
 * @property float durationForecast
 * @property float remainsToBeDone
 * @property mixed batch
 * @package BoondManager\Models
 */
class Marker extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_JALON'          => ['name' => 'id', 'type' => self::TYPE_STRING],
			'JALON_DATE'        => ['name' => 'date', 'type' => self::TYPE_DATE],
			'JALON_TITRE'       => ['name' => 'title', 'type' => self::TYPE_STRING],
			'JALON_VALUE'       => ['name' => 'progressInDays', 'type' => self::TYPE_FLOAT],
			'JALON_DUREE'       => ['name' => 'durationForecast', 'type' => self::TYPE_FLOAT],
			'REMAINSTOBEDONE'   => ['name' => 'remainsToBeDone', 'type' => self::TYPE_FLOAT],
			'PROGRESSRATE'	  	=> ['name' => 'progressRate', 'type' => self::TYPE_FLOAT],
			'RESOURCE'    		=> ['name' => 'resource', 'type' => self::TYPE_OBJECT],
			'BATCH'    			=> ['name' => 'batch', 'type' => self::TYPE_OBJECT]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->toArray();
	}
}
