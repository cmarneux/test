<?php
/**
 * nodeserver.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * class NodeServer
 * @property string internalURL
 * @property string externalURL
 * @property bool useAsDefault
 * @package BoondManager\Models
 */
class NodeServer extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'NODE_URL' => ['name'=>'externalURL', 'type'=>self::TYPE_STRING],
			'NODE_SERVER' => ['name'=>'internalURL', 'type'=>self::TYPE_STRING],
			'NODE_DEFAULT' => ['name'=>'useAsDefault', 'type'=>self::TYPE_BOOLEAN]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
