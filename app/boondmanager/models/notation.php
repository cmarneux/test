<?php
/**
 * notation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\Model;
use Wish\Models\ModelJSON;
use Wish\Models\PublicFieldsInterface;
use Wish\Models\PublicFieldsTrait;

/**
 * Class Notation
 * @property int ID_NOTATION
 * @property array NOTATION_NOTES
 * @property string NOTATION_COMMENTAIRE
 * @property string NOTATION_DATE
 * @property int ID_PROFIL
 * @property string PROFIL_PRENOM
 * @property string PROFIL_NOM
 * @package BoondManager\Models
 */
class Notation extends ModelJSON
{

	const MAPPER = [
		'ID_NOTATION'          => ['name' => 'id', 'type' => self::TYPE_STRING],
		'NOTATION_DATE'        => ['name' => 'date', 'type' => self::TYPE_DATE],
		'NOTATION_NOTES'       => ['name' => 'notations', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeNotations', 'unserializeCallback' => 'unserializeNotations'],
		'NOTATION_COMMENTAIRE' => ['name' => 'comments', 'type' => self::TYPE_STRING],
		'ID_PROFIL'            => ['name' => 'manager', 'type' => self::TYPE_STRING],
		'PROFIL_PRENOM'        => ['name' => 'firstName', 'type' => self::TYPE_STRING],
		'PROFIL_NOM'           => ['name' => 'lastName', 'type' => self::TYPE_STRING],
	];

	/**
	 * @param array $notations
	 * @return string;
	 */
	public static function serializeNotations(array $notations){
		return implode('#', array_map(function($value){
			/** @var Evaluation $value */
			return $value->criteria.'|'.$value->evaluation;
		}, $notations));
	}

	/**
	 * @param string $notations
	 * @return array
	 */
	public static function unserializeNotations($notations){
		return array_map(function($value){
			list($criteria, $ev) = explode('|', $value);
			return new Evaluation($ev, $criteria);
		}, explode('#',$notations));
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
