<?php
/**
 * notification.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Services\CurrentUser;
use Wish\Models\Model;

class Notification{

	/**
	 * @var array
	 */
	private $_recipients = [];

	/**
	 * @var array
	 */
	private $_pushParams = [];

	/**
	 * @var DictionaryEntry[]
	 */
	private $_changes = [];

	/** @var Model */
	private $_item = null;

	/**
	 * @var string
	 */
	private $_title = '';

	/**
	 * @var DictionaryEntry
	 */
	private $_text = null;

	private $_type;

	private $_action = null;

	const TYPE_CREATE = 'create';
	const TYPE_UPDATE = 'update';
	const TYPE_DELETE = 'delete';
	const TYPE_ADD_DOCUMENT = 'addDocument';
	const TYPE_DELETE_DOCUMENT = 'deleteDocument';

	private $_targetPath;
	private $_targetName;
	private $_targetType;
	private $_targetID;
	private $_moreData;

	private $_sticky = false;

	public function __construct($item, $action){
		$this->_authorID = CurrentUser::instance()->getEmployeeId();
		$this->_authorName = CurrentUser::instance()->getFullName();

		$this->setAction($action);

		switch (get_class($item)) {
			case Delivery::class:
			case Purchase::class:
			case Company::class:
				$this->_targetID = $item->id;
				$this->_targetType = $item->getType();
				break;
			case Candidate::class:
			case Contact::class:
			case Employee::class:
				/** @var Employee|Candidate|Contact $item*/
				$this->_targetID = $item->id;
				$this->_targetType = $item->getType();
				$this->_targetName = $item->getBackup()->getFullName();
				break;
			default: throw new \Exception('class not handled: '.get_class($item));
		}
	}

	public function setPath($path) {
		$this->_targetPath = $path;
		return $this;
	}

	public function setTarget($path, $name){
		$this->_targetPath = $path;
		$this->_targetName = $name;
		return $this;
	}

	public function getType(){
		return $this->_type;
	}

	public function setItem($item){
		$this->_item = $item;
		return $this;
	}

	public function setTitle($title){
		$this->_title = $title;
		return $this;
	}

	public function getTitle(){
		return $this->_title;
	}

	public function addRecipient(Account $manager){
		$this->_recipients[$manager->id] = $manager;
		return $this;
	}

	public function setPushParams($params){
		$this->_pushParams = $params;
		return $this;
	}

	public function addChangeText(DictionaryEntry $buildDictEntryForField)
	{
		$this->_changes[] = $buildDictEntryForField;
		return $this;
	}

	public function setText(DictionaryEntry $textEntry){
		$this->_text = $textEntry;
		return $this;
	}

	public function getChangedList()
	{
		return $this->_changes;
	}

	public function toArray(){
		$data = [
			'item' => [
				'type' => $this->_targetType,
				'id' => $this->_targetID,
			],
			'author' => [
				'id' => $this->_authorID,
				'name' => $this->_authorName
			],
			'action' => $this->_action
		];

		if($this->_targetPath)
			$data['item']['path'] = $this->_targetPath;

		if($this->_targetName)
			$data['item']['name'] = $this->_targetName;

		if($this->_moreData)
			$data['data'] = $this->_moreData;

		return $data;
	}

	public function additionalData($data) {
		$this->_moreData = $data;
		return $this;
	}

	public function setAction($_action){
		if(!is_string($_action) || !in_array($_action, [
			self::TYPE_ADD_DOCUMENT, self::TYPE_DELETE_DOCUMENT, self::TYPE_UPDATE, self::TYPE_DELETE, self::TYPE_CREATE
		])) {
			throw new \Exception("unvalid action '$_action'");
		}
		$this->_action = $_action;
	}

	public function setSticky($sticky = true){
		$this->_sticky = boolval($sticky);
		return $this;
	}

	public function isSticky()
	{
		return $this->_sticky;
	}

	public function getRecipients()
	{
		return array_values($this->_recipients);
	}

	public function getAction()
	{
		return $this->_action;
	}
}
