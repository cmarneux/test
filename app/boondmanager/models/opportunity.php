<?php
/**
 * opportunity.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Opportunity
 * @property int id
 * @property bool visibility
 * @property array activityAreas
 * @property string expertiseArea
 * @property array place
 * @property string reference
 * @property boolean $startASAP true if start ASAP, otherwise refer to AO_DEBUT
 * @property int typeOf
 * @property string startDate date
 * @property string[] tools
 * @property Account mainManager
 * @property Pole pole
 * @property Agency agency
 * @property Contact contact
 * @property Company company
 * @property Document[] files
 * @property float $weighting
 * @property float AO_TARIFADDITIONNEL
 * @property float AO_INVESTISSEMENT
 * @property string $title
 * @property int mode
 * @property int $currency
 * @property int $currencyAgency
 * @property float $exchangeRateAgency
 * @property float $exchangeRate
 * @property float $turnoverEstimatedExcludingTax
 * @property float turnoverWeightedExcludingTax
 * @property boolean validateSimulation
 * @property int $numberOfProjects
 * @property OriginOrSource origin
 * @property float turnoverPositioningsExcludingTax
 * @property float costsPositioningsExcludingTax
 * @property float marginPositioningsExcludingTax
 * @property float profitabilityPositioningsExcludingTax
 * @property PositioningDetail[] $additionalTurnoverAndCosts
 * @property bool canReadOpportunity
 * @property bool canWriteOpportunity
 * @property bool canReadContact
 * @property bool canReadCompany
 * //TODO completer les autres propriétés
 */
class Opportunity extends ModelJSONAPI  implements HasPoleInterface, HasAgencyInterface, HasManagerInterface {
	use HasPoleTrait, HasAgencyTrait, HasManagerTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'opportunity';

	/**#@+
	 * @var string Tab
	 */
	const
		TAB_INFORMATION = 'information',
		TAB_ACTIONS = 'actions',
		TAB_PROJECTS = 'projects',
		TAB_POSITIONINGS = 'positionings',
		TAB_SIMULATION = 'simulation',
		TAB_DEFAULT = '';
	/**#@-*/

	const REF_PREFIX = 'AO';

	const MAPPER = [
		'ID_AO'                                 => ['name' => 'id', 'type' => self::TYPE_INT],
		'AO_DEPOT'                              => ['name' => 'creationDate', 'type' => self::TYPE_DATE],
		'AO_DATEUPDATE'                         => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'AO_TITLE'                              => ['name' => 'title', 'type' => self::TYPE_STRING],
		'AO_REF'                                => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'AO_TYPEREF'                            => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'AO_TYPE'                               => ['name' => 'mode', 'type' => self::TYPE_INT],
		'AO_DESCRIPTION'                        => ['name' => 'description', 'type' => self::TYPE_STRING],
		'AO_CRITERES'                           => ['name' => 'criteria', 'type' => self::TYPE_STRING],
		'AO_INTERVENTION'                       => ['name' => 'expertiseArea', 'type' => self::TYPE_STRING],
		'AO_APPLICATIONS'                       => ['name' => 'activityAreas', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'AO_OUTILS'                             => ['name' => 'tools', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
		'AO_ETAT'                               => ['name' => 'state', 'type' => self::TYPE_INT],
		'AO_LIEU'                               => ['name' => 'place', 'type' => self::TYPE_STRING],
		'AO_DEBUT'                              => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'AO_TYPEDEBUT'                          => ['name' => 'startASAP', 'type' => self::TYPE_BOOLEAN],
		'AO_DUREE'                              => ['name' => 'duration', 'type' => self::TYPE_STRING],
		'AO_DEVISE'                             => ['name' => 'currency', 'type' => self::TYPE_INT],
		'AO_CHANGE'                             => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'AO_DEVISEAGENCE'                       => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'AO_CHANGEAGENCE'                       => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'AO_PROBA'                              => ['name' => 'weighting', 'type' => self::TYPE_FLOAT],
		'AO_BUDGET'                             => ['name' => 'estimatesExcludingTax', 'type' => self::TYPE_FLOAT],
		'AO_CA'                                 => ['name' => 'turnoverEstimatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'AO_TARIFADDITIONNEL'                   => ['name' => 'AO_TARIFADDITIONNEL', 'type' => self::TYPE_FLOAT],
		'AO_INVESTISSEMENT'                     => ['name' => 'AO_INVESTISSEMENT', 'type' => self::TYPE_FLOAT],
		'turnoverWeightedExcludingTax'          => ['name' => 'turnoverWeightedExcludingTax', 'type' => self::TYPE_FLOAT],
		'AO_VISIBILITE'                         => ['name' => 'visibility', 'type' => self::TYPE_BOOLEAN],
		'AO_CORRELATIONPOS'                     => ['name' => 'validateSimulation', 'type' => self::TYPE_BOOLEAN],
		'NB_POS'                                => ['name' => 'numberOfActivePositionings', 'type' => self::TYPE_INT],
		'origin'                                => ['name' => 'origin', 'type' => self::TYPE_OBJECT],
		'canReadContact'                        => ['name' => 'canReadContact', 'type' => self::TYPE_BOOLEAN],
		'canReadCompany'                        => ['name' => 'canReadCompany', 'type' => self::TYPE_BOOLEAN],
		'canReadOpportunity'                    => ['name' => 'canReadOpportunity', 'type' => self::TYPE_BOOLEAN],
		'canWriteOpportunity'                   => ['name' => 'canWriteOpportunity', 'type' => self::TYPE_BOOLEAN],
		'turnoverPositioningsExcludingTax'      => ['name' => 'turnoverPositioningsExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsPositioningsExcludingTax'         => ['name' => 'costsPositioningsExcludingTax', 'type' => self::TYPE_FLOAT],
		'marginPositioningsExcludingTax'        => ['name' => 'marginPositioningsExcludingTax', 'type' => self::TYPE_FLOAT],
		'profitabilityPositioningsExcludingTax' => ['name' => 'profitabilityPositioningsExcludingTax', 'type' => self::TYPE_FLOAT],
		'POSITIONDETAILS'                       => ['name' => 'additionalTurnoverAndCosts', 'type' => self::TYPE_ARRAY],
		'NB_PROJETS'                            => ['name' => 'numberOfProjects', 'type' => self::TYPE_INT]
	];

	/**
	 * @var
	 */
	public $totalTurnorver, $totalCosts;

	/**
	 * @return bool
	 */
	public function isVisible(){
		return $this->visibility == 1;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_PROFIL', 'mainManager', Account::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);

		$this->setRelationships('ID_CRMCONTACT', 'contact', Contact::class);
		$this->setRelationships('ID_CRMSOCIETE', 'company', Company::class);

		$this->setGroupedRelationships('DOCUMENTS', 'files');
	}

	/**
	 * @return array
	 */
	public static function getAllTabs(){
		return [
			self::TAB_INFORMATION, self::TAB_POSITIONINGS, self::TAB_ACTIONS, self::TAB_PROJECTS, self::TAB_SIMULATION
		];
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 * @param $totalTurnover
	 * @param $totalCosts
	 */
	public function calculatePositioningsData($totalTurnover, $totalCosts){
		$this->totalCosts = $totalCosts;
		$this->totalTurnorver = $totalTurnover;

		$this->turnoverPositioningsExcludingTax = $this->totalTurnorver + $this->AO_TARIFADDITIONNEL;
		$this->costsPositioningsExcludingTax = $this->totalCosts + $this->AO_INVESTISSEMENT;
		$this->marginPositioningsExcludingTax = $this->turnoverPositioningsExcludingTax - $this->costsPositioningsExcludingTax;
		if($this->costsPositioningsExcludingTax)
			$this->profitabilityPositioningsExcludingTax = $this->marginPositioningsExcludingTax/$this->costsPositioningsExcludingTax*100;
		else
			$this->profitabilityPositioningsExcludingTax = 0;
	}

	/**
	 * @return float
	 */
	public function calculateTurnoverWeightedExcludingTax(){
		return $this->turnoverWeightedExcludingTax = $this->turnoverEstimatedExcludingTax * $this->weighting / 100;
	}
}
