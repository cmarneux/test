<?php
/**
 * order.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int id
 * @property string creationDate
 * @property string number
 * @property string reference
 * @property boolean customerAgreement
 * @property string startDate
 * @property string endDate
 * @property float turnoverInvoicedExcludingTax
 * @property float turnoverOrderedExcludingTax
 * @property float deltaInvoicedExcludingTax
 * @property int state
 * @property TurnoverState[] $turnoverStates
 * @property boolean canReadOrder
 * @property boolean canWriteOrder
 * @property Account mainManager
 * @property Project project
 * @property int numberOfInvoices
 * @property Document[] files
 * @property BankDetails bankDetail
 * @property boolean createInvoiceAutomatically
 * @property boolean copyCommentsOnNewInvoice
 * @property boolean showCommentsOnPDF
 * @property boolean showFactorOnPDF
 * @property boolean showCompanyVATNumberOnPDF
 * @property boolean showCompanyNumberOnPDF
 * @property boolean showBankDetailsOnPDF
 * @property boolean showProjectReferenceOnPDF
 * @property boolean showResourcesNameOnPDF
 * @property boolean showAverageDailyPriceOnPDF
 * @property boolean showNumberOfWorkignDaysOnPDF
 * @property boolean showFooterOnPDF
 * @property boolean separateActivityExpensesAndPurchases
 * @property ContactDetails billingDetail
 * @property Company factor
 * @property Schedule[] schedules
 * @property Delivery[]|Purchase[] deliveriesPurchases
 *
 */
class Order extends ModelJSONAPI implements HasManagerInterface, HasPoleInterface, HasAgencyInterface {
	use HasManagerTrait;

	const TAB_INFORMATION = 'information';
	const TAB_ACTIONS = 'actions';
	const TAB_INVOICES = 'invoices';
	const TAB_ATTACHED_FLAGS = 'attachedFlags';

	const REF_PREFIX = 'BDC';

	protected static $_jsonType = 'order';

	const MAPPER = [
		'ID_BONDECOMMANDE'     => ['name' => 'id', 'type' => self::TYPE_INT],
		'BDC_DATE'             => ['name' => 'date', 'type' => self::TYPE_DATE],
		'BDC_REFCLIENT'        => ['name' => 'number', 'type' => self::TYPE_STRING],
		'BDC_REF'              => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'BDC_ACCORDCLIENT'     => ['name' => 'customerAgreement', 'type' => self::TYPE_BOOLEAN],
		'BDC_DEBUT'            => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'BDC_FIN'              => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'BDC_TYPEREGLEMENT'    => ['name' => 'typeOf', 'type' => self::TYPE_STRING, 'mapper' => self::MAPPER_TYPE],
		'BDC_LANGUE'           => ['name' => 'language', 'type' => self::TYPE_STRING],
		'BDC_TYPEPAYMENT'      => ['name' => 'paymentMethod', 'type' => self::TYPE_INT],
		'BDC_CONDREGLEMENT'    => ['name' => 'paymentTerm', 'type' => self::TYPE_INT],
		'BDC_TAUXTVA'          => ['name' => 'taxRate', 'type' => self::TYPE_FLOAT],
		'BDC_COMMENTAIRE'      => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'BDC_MENTIONS'         => ['name' => 'legals', 'type' => self::TYPE_STRING],
		'BDC_AUTOCREATION'     => ['name' => 'createInvoiceAutomatically', 'type' => self::TYPE_BOOLEAN],
		'BDC_COPYCOMMENTS'     => ['name' => 'copyCommentsOnNewInvoice', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWCOMMENTS'     => ['name' => 'showCommentsOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWFACTOR'       => ['name' => 'showFactorOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWTVAIC'        => ['name' => 'showCompanyVATNumberOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWNUMBER'       => ['name' => 'showCompanyNumberOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWRIB'          => ['name' => 'showBankDetailsOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWPRJREFERENCE' => ['name' => 'showProjectReferenceOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWINTNAME'      => ['name' => 'showResourcesNameOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWTARIFJRS'     => ['name' => 'showAverageDailyPriceOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWJRSOUVRES'    => ['name' => 'showNumberOfWorkignDaysOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SHOWFOOTER'       => ['name' => 'showFooterOnPDF', 'type' => self::TYPE_BOOLEAN],
		'BDC_SEPARATETPSFRS'   => ['name' => 'separateActivityExpensesAndPurchases', 'type' => self::TYPE_BOOLEAN],
		'BDC_GROUPMISSION'     => ['name' => 'groupDeliveries', 'type' => self::TYPE_BOOLEAN],
		'TOTAL_CAFACTUREHT'    => ['name' => 'turnoverInvoicedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_CAPRODUCTIONHT' => ['name' => 'turnoverProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_MONTANTBDC'     => ['name' => 'turnoverOrderedExcludingTax', 'type' => self::TYPE_FLOAT],
		'DOCUMENT_DELTA'       => ['name' => 'deltaInvoicedExcludingTax', 'type' => self::TYPE_FLOAT],
		'BDC_ETAT'             => ['name' => 'state', 'type' => self::TYPE_INT],
		'NB_FACTURE'           => ['name' => 'numberOfInvoices', 'type' => self::TYPE_INT],
		'turnoverStates'       => ['name' => 'turnoverStates', 'type' => self::TYPE_ARRAY],
		'schedules'            => ['name' => 'schedules', 'type' => self::TYPE_ARRAY],
		'canReadOrder'         => ['name' => 'canReadOrder', 'type' => self::TYPE_BOOLEAN],
		'canWriteOrder'        => ['name' => 'canWriteOrder', 'type' => self::TYPE_BOOLEAN],
	];

	const MAPPER_TYPE = [
		0 => 'schedule',
		1 => 'monthly'
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	public function initRelationships(){
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setGroupedRelationships('FILES', 'files');
		$this->setRelationships('ID_RIB', 'bankDetail', BankDetails::class);
		$this->setRelationships('ID_COORDONNES', 'billingDetail', ContactDetails::class);
		$this->setGroupedRelationships('DELPURCHASES', 'deliveriesPurchases');
		$this->setRelationships('ID_FACTOR', 'factor', Company::class);
	}

	public function calculateData() {
		$this->calculateDates();
		return $this;
	}

	public function calculateDates() {

		if($this->deliveriesPurchases) {
			$min = $max = null;
			foreach($this->deliveriesPurchases as $entity) {
				$min = is_null($min) ? $entity->startDate : min($min, $entity->startDate);
				$max = is_null($max) ? $entity->maxDate : max($max, $entity->endDate);
			}
			$this->startDate = $min;
			$this->endDate = $min;
		}else {
			$this->startDate = $this->project->startDate;
			$this->endDate = $this->project->endDate;
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public static function getAllTabs()
	{
		return [self::TAB_INFORMATION, self::TAB_ACTIONS, self::TAB_INVOICES, self::TAB_ATTACHED_FLAGS];
	}

	/**
	 * @return mixed
	 */
	public function getAgencyID() {
		return $this->project->getAgencyID();
	}

	/**
	 * @return mixed
	 */
	public function getPoleID() {
		return $this->project->getPoleID();
	}
}
