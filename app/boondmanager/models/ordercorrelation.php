<?php
/**
 * ordercorrelation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;

/**
 * class OrderCorrelation
 * @property int id
 *
 */
class OrderCorrelation extends ModelJSONAPI {

	protected static $_jsonType = 'ordercorrelation';

	const MAPPER = [
		'ID_CORRELATIONBONDECOMMANDE' => ['name' => 'id', 'type' => self::TYPE_INT],
		'CORBDC_TYPE'                 => ['name' => 'CORBDC_TYPE', 'type' => self::TYPE_STRING, 'mapper' => self::MAPPER_TYPE],
	];

	const DBTYPE_DELIVERY = 0;
	const DBTYPE_PURCHASE = 1;

	const MAPPER_TYPE = [
		self::DBTYPE_DELIVERY => 'delivery',
		self::DBTYPE_PURCHASE => 'purchase'
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	public function initRelationships(){
	}
}
