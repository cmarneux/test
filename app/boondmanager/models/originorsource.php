<?php
/**
 * origin.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class OriginOrSource
 * @property int $typeOf,
 * @property string $detail
 * @package BoondManager\Models\MySQL\RowObject
 */
class OriginOrSource extends ModelJSON {

	const MAPPER = [
		'TYPE_SOURCE' => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'SOURCE'      => ['name' => 'detail', 'type' => self::TYPE_STRING]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
