<?php
/**
 * payment.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasPoleInterface;
use Wish\Models\ModelJSONAPI;

/**
 * class Payment
 * @property int $id
 * @property string $date
 * @property string $performedDate
 * @property string $expectedDate
 * @property int $state
 * @property string startDate
 * @property string endDate
 * @property int paymentMethod
 * @property string number
 * @property string informationComments
 * @property float TOTAL_PMTHT
 * @property float $amountExcludingTax
 * @property float $amountIncludingTax
 * @property float TOTAL_PMTTTC
 * @property float $taxRate
 * @property array activityDetails
 * @property Purchase purchase
 * @property File[] files
 */
class Payment extends ModelJSONAPI implements HasPoleInterface , HasManagerInterface , HasAgencyInterface {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'payment';

	/**#@+
	 * payment state
	 * @var int PMT_ETAT
	 */
	const STATE_PLANNED = 0;
	const STATE_CONFIRMED = 1;
	const STATE_WELL_ORDERED = 2;
	/**#@-*/

	const MAPPER = [
		'ID_PAIEMENT'              => ['name' => 'id', 'type' => self::TYPE_INT],
		'PMT_DATE'                 => ['name' => 'date', 'type' => self::TYPE_DATE],
		'PMT_ETAT'                 => ['name' => 'state', 'type' => self::TYPE_INT],
		'PMT_TYPEPAYMENT'          => ['name' => 'paymentMethod', 'type' => self::TYPE_INT],
		'PMT_TAUXTVA'              => ['name' => 'taxRate', 'type' => self::TYPE_FLOAT],
		'PMT_DEBUT'                => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'PMT_FIN'                  => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'PMT_REFPROVIDER'          => ['name' => 'number', 'type' => self::TYPE_STRING],
		'PMT_DATEPAIEMENTATTENDU'  => ['name' => 'expectedDate', 'type' => self::TYPE_DATE],
		'PMT_DATEPAIEMENTEFFECTUE' => ['name' => 'performedDate', 'type' => self::TYPE_DATE],
		'PMT_MONTANTHT'            => ['name' => 'amountExcludingTax', 'type' => self::TYPE_FLOAT],
		'PMT_MONTANTTTC'           => ['name' => 'amountIncludingTax', 'type' => self::TYPE_FLOAT],
		'PMT_COMMENTAIRES'         => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'ACTIVITYDETAILS'          => ['name' => 'activityDetails', 'type' => self::TYPE_ARRAY],
		'TOTAL_PAIEMENTSHT'        => ['name' => 'amountExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_PAIEMENTSTTC'       => ['name' => 'amountIncludingTax', 'type' => self::TYPE_FLOAT],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships()
	{
		$this->setRelationships('ID_ACHAT', 'purchase', Purchase::class);
		$this->setGroupedRelationships('files', 'files');
	}

	/**
	 * @return mixed
	 */
	public function getAgencyID()
	{
		return $this->purchase->getAgencyID();
	}

	/**
	 * @return mixed
	 */
	public function getManagerID()
	{
		$this->purchase->getManagerID();
	}

	/**
	 * @return mixed
	 */
	public function getPoleID()
	{
		$this->purchase->getPoleID();
	}
}
