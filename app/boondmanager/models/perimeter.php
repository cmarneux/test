<?php
/**
 * perimeter.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Models;

use Wish\Models\JSONApiObjectTrait;
use Wish\Models\JSONApiObjectInterface;
use BoondManager\Services\CurrentUser;

/**
 * Class Perimeter
 * @package BoondManager\Models
 */
class Perimeter implements \Countable, \Iterator, JSONApiObjectInterface{
	use JSONApiObjectTrait;

	/**#@+
	 * @var string TYPE entry type
	 */
	const
		TYPE_BU      = 'BU',
		TYPE_AGENCY  = 'AGENCY',
		TYPE_MANAGER = 'MANAGER',
		TYPE_POLE    = 'POLE';
	/**#@-*/

	/**#@+
	 * @var string Narrow perimeter type
	 */
	const MATCH_ALL = 'all',
		  MATCH_ANY = 'any';

	/**
	 * @var CurrentUser|null the manager working with this perimeter
	 */
	private $manager;

	/**
	 * @var array perimeter agencies
	 */
	private $agencies = array();

	/**
	 * @var array perimeter BUs
	 */
	private $BUs = array();

	/**
	 * @var array perimeter managers
	 */
	private $managers = array();

	/**
	 * @var array perimeter poles
	 */
	private $poles = array();

	/**
	 * @var array list of all keys (internal cache)
	 */
	private $keyCache = null;

	/**
	 * show if the value "Indifférent" can be used by the user for the research
	 *
	 * This array is build based on {@see \BoondManager\Models\CurrentUser::getPerimeter() getPerimeterSelect}.
	 *
	 * It has 2 possibles values depending of the "Indifferent" value:
	 * - [ 0=> '', 1=> "Indifférent" ] (key 1 depends of the user language)
	 * - [ 0=> ID_USER, 1=> FIRSTNAME+LASTNAME ]
	 *
	 * @var array
	 */
	private $indifferent_perimeter;

	/**
	 * @var int current key
	 */
	private $_currentKey;

	/**
	 * @var bool
	 */
	private $perimeterRequired = true;

	/**
	 * @var Perimeter
	 */
	private $defaultPerimeter;

	/**
	 * @var string
	 */
	private $_id;

	/**
	 * @var
	 */
	private $match;

	/**
	 * Perimeter constructor.
	 * @param CurrentUser $manager the manager who owns the perimeter
	 * @param string $module
	 */
	public function __construct(CurrentUser $manager = null, $module = null){
		$this->manager = $manager;
		$this->_id = $module.'-' . ($manager ? $manager->getUserId() : 0);
	}

	/**
	 * @return string
	 */
	public function getID(){
		return $this->_id;
	}

	/**
	 * @return array
	 */
	public function getAttributes(){
		return [
			'required' => $this->isPerimeterRequired()
		];
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'perimeters';
	}

	/**
	 * @return array
	 */
	public function getRelationships()
	{
		return [
			'managers' => $this->managers,
			'businessUnits' => $this->BUs,
			'agencies' => $this->agencies,
			'poles' => $this->poles
		];
	}

	/**
	* return the user who owns the perimeter (or null)
	* @return CurrentUser
	*/
	public function getUser(){
		return $this->manager;
	}

	/**
	 * add an agency
	 * @param Agency $agency
	 * @return $this
	 */
	public function addAgency(Agency $agency){
		if(!array_key_exists($agency->getID(), $this->agencies)) {
			$this->agencies[$agency->getID()] = $agency;
			$this->resetCache();
		}
		return $this;
	}

	/**
	* get the list of all agencies
	* @return Agency[]
	*/
	public function getAgencies(){
		return array_values($this->agencies);
	}

	/**
	 * get a list of agencies IDs (db formatted)
	 * @return array
	 */
	public function getAgenciesIDs(){
		return array_keys($this->agencies);
	}

	/**
	 * @param $id
	 * @return null|Agency
	 */
	public function getAgency($id){
		return array_key_exists($id, $this->agencies) ? $this->agencies[$id] : null;
	}

	/**
	 * add a business unit
	 * @param BusinessUnit $bu
	 * @return $this
	 */
	public function addBU(BusinessUnit $bu){
		if(!array_key_exists($bu->getID(), $this->BUs)) {
			$this->BUs[$bu->getID()] = $bu;
			$this->resetCache();
		}
		return $this;
	}

	/**
	* get the list of business units
	* @return BusinessUnit[]
	*/
	public function getBUs(){
		return array_values($this->BUs);
	}

	/**
	 * @return array
	 */
	public function getBUsIDs(){
		return array_keys($this->BUs);
	}

	/**
	 * @param int $id
	 * @return null|BusinessUnit
	 */
	public function getBU($id){
		return array_key_exists($id, $this->BUs) ? $this->BUs[$id] : null;
	}

	/**
	 * add a pole
	 * @param Pole $pole
	 * @return $this
	 */
	public function addPole(Pole $pole){
		if(!array_key_exists($pole->getID(), $this->poles)) {
			$this->poles[$pole->getID()] = $pole;
			$this->resetCache();
		}
		return $this;
	}

	/**
	 * @param int $id
	 * @return null|Pole
	 */
	public function getPole($id){
		return array_key_exists($id, $this->poles) ? $this->poles[$id] : null;
	}

	/**
	* get the list of poles
	* @return Pole[]
	*/
	public function getPoles(){
		return array_values($this->poles);
	}

	/**
	 * @return array
	 */
	public function getPolesIDs(){
		return array_keys($this->poles);
	}

	/**
	 * add a manager
	 * @param Resource|Account $manager
	 * @return $this
	 * @throws \Exception
	 */
	public function addManager($manager){
		if($manager instanceof Employee){
			if(!array_key_exists($manager->getID(), $this->managers)){
				// we use the getID so we can have an ID wich is a string a allow us to keep the `addManager` order
				$this->managers[ $manager->getID() ] = $manager;
				$this->resetCache();
			}
		}else{
			throw new \Exception('manager must be an instance of '.Employee::class.' and not '.get_class($manager));
		}
		return $this;
	}

	/**
	 * @param $id
	 * @return null|Account
	 */
	public function getManager($id){
		return array_key_exists($id, $this->managers) ? $this->managers[$id] : null;
	}

	/**
	* get the list of managers
	* @return Account[]
	*/
	public function getManagers(){
		return array_values($this->managers);
	}

	/**
	 * @return array
	 */
	public function getManagersIDs(){
		return array_keys($this->managers);
	}

	/**
	* Get the perimeter list formatted for the front
	*
	* @return array
	*
	* @deprecated
	*/
	public function getFormattedList(){
		return [
			'required' => $this->isPerimeterRequired(),
			'agencies' => $this->agencies,
			'bus' => $this->BUs,
			'managers' => $this->managers,
			'poles' => $this->poles
		];
	}

	/**
	* alias of {@see \BoondManager\Models|Perimeter::isPerimeterMoreThanManager isPerimeterMoreThanManager()}
	*
	* @return bool
	*/
	public function isPerimeterMoreThanMe(){
		return $this->isPerimeterMoreThanManager();
	}

	/**
	* tell if the perimeter has another manager than the current user
	* FIXME
	* @return boolean
	*/
	public function isPerimeterMoreThanManager(){
		foreach($this->managers as $m){
			if($this->manager->id != $m->id) return true;
		}
		return false;
	}

	/**
	 * @param $match
	 * @return $this
	 * @throws \Exception
	 */
	public function setDefaultMatch($match){
		if(in_array($match, [
			self::MATCH_ALL, self::MATCH_ANY
		])) {
			$this->match = $match;
			return $this;
		} else throw new \Exception('wrong match');
	}

	/**
	 * @return bool
	 */
	public function matchAll(){
		return $this->match === self::MATCH_ALL;
	}

	/**
	* clear the internal cache
	*/
	private function resetCache(){
		$this->keyCache = null;
	}

	/**
	* set a perimeter
	* @see self::perimetre_indifferent
	* @param array $data
	* @return $this
	*/
	public function setPerimeterIndifferent(array $data){
		$this->indifferent_perimeter = $data;
		return $this;
	}

	/**
	* indicate if the perimeter indifferent is available to the user
	* @return boolean
	*/
	public function isPerimeterIndifferent(){
		return !$this->perimeterRequired;
	}

	/**
	* Return the variable {@see \BoondManager\Models\Perimetre::$indifferent_perimeter $indifferent_perimeter}.
	* @return array
	*/
	public function getIndifferentPerimeter(){
		return $this->indifferent_perimeter;
	}

	/**
	 * @param Perimeter $perimeter
	 * @deprecated
	 */
	public function setDefaultPerimeter(Perimeter $perimeter){
		$this->defaultPerimeter = $perimeter;
	}

	/**
	 * @return Perimeter
	 * @deprecated
	 */
	public function getDefaultPerimeter(){
		return $this->defaultPerimeter;
	}

	/**
	 * @param $bool
	 */
	public function setRequiredPerimeter($bool){
		$this->perimeterRequired = boolval($bool);
	}

	/**
	 * @return bool
	 */
	public function isPerimeterRequired(){
		return $this->perimeterRequired;
	}

	/**
	* Count elements of an object
	* @link http://php.net/manual/en/countable.count.php
	* @return int The custom count as an integer.
	* </p>
	* <p>
	* The return value is cast to an integer.
	* @since 5.1.0
	*/
	public function count()
	{
		return count($this->agencies) + count($this->BUs) + count($this->managers) + count($this->poles);
	}

	/**
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 * @since 5.0.0
	 */
	public function current()
	{
		if($this->_currentKey < count($this->managers)) {
			return $this->getManagers()[$this->_currentKey];
		}else if($this->_currentKey < count($this->agencies)){
			$key = $this->_currentKey - count($this->managers);
			return $this->getAgencies()[$key];
		}else if($this->_currentKey < count($this->BUs)){
			$key = $this->_currentKey - count($this->managers) - count($this->agencies);
			return $this->getBUs()[$key];
		}else{
			$key = $this->_currentKey - count($this->managers) - count($this->agencies) - count($this->BUs);
			return $this->getPoles()[$key];
		}
    }

	/**
	* Move forward to next element
	* @link http://php.net/manual/en/iterator.next.php
	* @return void Any returned value is ignored.
	* @since 5.0.0
	*/
	public function next()
	{
		$this->_currentKey++;
	}

	/**
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 * @since 5.0.0
	 */
	public function key()
	{
		return $this->_currentKey;
	}

	/**
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 * @since 5.0.0
	 */
	public function valid()
	{
		return $this->_currentKey < $this->count();
	}

	/**
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function rewind()
	{
		$this->_currentKey = 0;
	}
}
