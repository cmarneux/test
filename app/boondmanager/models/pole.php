<?php
/**
 * pole.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class Pole
 * @property int id
 * @property string POLE_NAME
 * @property mixed POLE_ETAT
 * @package BoondManager\Models
 */
class Pole extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'pole';

	const MAPPER = [
		'ID_POLE'   => ['name' => 'id', 'type' => self::TYPE_INT],
		'POLE_NAME' => ['name' => 'name', 'type' => self::TYPE_STRING],
		'POLE_ETAT' => ['name' => 'state', 'type' => self::TYPE_BOOLEAN]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}
}
