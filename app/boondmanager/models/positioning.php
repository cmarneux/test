<?php
/**
 * positioning.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Positioning
 * @property int $id
 * @property int COMP_TYPE
 * @property int CRMRESP_IDSOCIETE
 * @property int CRMRESP_IDPROFIL
 * @property float $averageDailyPriceExcludingTax
 * @property float $numberOfDaysInvoicedOrQuantity
 * @property float $numberOfDaysFree
 * @property float $averageDailyCost
 * @property float POS_CORRELATION
 * @property float POS_COMMENATIRE
 * @property float POS_FIN
 * @property Account mainManager
 * @property Opportunity opportunity
 * @property Product product
 * @property Employee resource
 * @property Candidate candidate
 * @property boolean canReadItem
 * @property boolean canReadContact
 * @property boolean canReadCompany
 * @property boolean canReadOpportunity
 * @property Document[] files
 * @property float additionalTurnoverAndCosts
 * @property Agency agency
 * @property Product|Candidate|Employee dependsOn
 * @package BoondManager\Models
 */
class Positioning extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'positioning';

	const REF_PREFIX = 'POS';

	const MAPPER = [
		'ID_POSITIONNEMENT'             => ['name' => 'id', 'type' => self::TYPE_INT],
		'POS_DATE'                      => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'POS_DATEUPDATE'                => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'POS_ETAT'                      => ['name' => 'state', 'type' => self::TYPE_INT],
		'POS_DEBUT'                     => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'POS_FIN'                       => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'POS_COMMENTAIRE'               => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'POS_CORRELATION'               => ['name' => 'includedInSimulation', 'type' => self::TYPE_BOOLEAN],
		'POS_TARIF'                     => ['name' => 'averageDailyPriceExcludingTax', 'type' => self::TYPE_FLOAT],
		'POS_CJM'                       => ['name' => 'averageDailyCost', 'type' => self::TYPE_FLOAT],
		'POS_NBJRSFACTURE'              => ['name' => 'numberOfDaysInvoicedOrQuantity', 'type' => self::TYPE_FLOAT],
		'POS_NBJRSGRATUIT'              => ['name' => 'numberOfDaysFree', 'type' => self::TYPE_FLOAT],
		//~ 'POSITIONDETAILS'    => ['name' => 'additionalTurnoverAndCosts'], //~ On ne les veut pas en dépendances
		'additionalTurnoverAndCosts'    => ['name' => 'additionalTurnoverAndCosts'], //~ le mapping est déjà fait
		'canReadItem'                   => ['name' => 'canReadItem', 'type' => self::TYPE_BOOLEAN],
		'canReadContact'                => ['name' => 'canReadContact', 'type' => self::TYPE_BOOLEAN],
		'canReadCompany'                => ['name' => 'canReadCompany', 'type' => self::TYPE_BOOLEAN],
		'canReadOpportunity'            => ['name' => 'canReadOpportunity', 'type' => self::TYPE_BOOLEAN]
	];

	const TAB_DEFAULT = 0;

	/**#@+
	 * @var boolean TAB_POSITIONNEMENT:POS_ETAT
	 */
	const
		STATE_WON = 'won',
		STATE_WON_KEY = 2,
		STATE_WAITING = 'waiting',
		STATE_WAITING_KEY = 0,
		STATE_REJECTED = 'rejected',
		STATE_REJECTED_KEY = 1;
	/**#@-*/

	/**#@+
	 * @var integer TAB_POSITIONNEMENT:ITEM_TYPE
	 */
	const
		ENTITY_PROFILE = 'resourcesOrCandidates', //~ positioning of a resource or candidate
		ENTITY_PROFILE_KEY = 0,
		ENTITY_PRODUCT = 'products',
		ENTITY_PRODUCT_KEY = 1;
	/**#@-*/

	/**
	 * @return bool
	 */
	public function isVisible(){
		return $this->opportunity->visibility;
	}

	public function dependsOnProduct(){
		return ($this->dependsOn instanceof Product);
	}

	public function dependsOnProfil(){
		return $this->dependsOn instanceof Employee || $this->dependsOn instanceof Candidate;
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_RESPPROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_AO', 'opportunity', Opportunity::class);
		$this->setRelationships('ID_PROJET', 'project', Project::class);

		$this->setDynamicTypedRelationship('ID_ITEM', 'dependsOn', function($object){
			/** @var self $object */
			if($object->ITEM_TYPE == self::ENTITY_PRODUCT) $class = Product::class;
			else if($object->COMP_TYPE == Candidate::TYPE_CANDIDATE) $class = Candidate::class;
			else $class = Employee::class;
			return $class;
		});

		//~ $this->setGroupedRelationships('POSITIONDETAILS', 'additionalTurnoverAndCosts'); //On ne les veut pas en dépendances
		//~ $this->setGroupedRelationships('DOCUMENTS', 'files');
		$this->setGroupedRelationships('files', 'files'); //~ le mapping est déjà fait
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
