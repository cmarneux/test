<?php
/**
 * positioningdetail.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class PositioningDetail
 * @property int ID_POSITIONDETAILS
 * @property float POSDETAILS_TARIF
 * @property float POSDETAILS_INVESTISSEMENT
 * @property string POSDETAILS_TITRE
 * @package BoondManager\Models
 */
class PositioningDetail extends ModelJSON {
    /**#@+
     * @var mappingArray TAB_POSITIONDETAILS:PARENT_TYPE
     */
    const
        PARENT_OPPORTUNITY = 'opportunity',
        PARENT_OPPORTUNITY_KEY = 0,
        PARENT_POSITIONING = 'positioning',
        PARENT_POSITIONING_KEY = 1;
    /**#@-*/

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_POSITIONDETAILS'        => ['name' => 'id', 'type' => self::TYPE_INT],
			'POSDETAILS_TARIF'          => ['name' => 'turnoverExcludingTax', 'type' => self::TYPE_FLOAT],
			'POSDETAILS_INVESTISSEMENT' => ['name' => 'costsExcludingTax', 'type' => self::TYPE_FLOAT],
			'POSDETAILS_TITRE'          => ['name' => 'title', 'type' => self::TYPE_STRING],
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
