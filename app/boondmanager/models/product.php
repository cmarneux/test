<?php
/**
 * product.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;

/**
 * class Product
 * @property int $id
 * @property int ID_PROFIL
 * @property int ID_SOCIETE
 * @property int ID_POLE
 * @property int currency
 * @property int currencyAgency
 * @property float exchangeRate
 * @property float exchangeRateAgency
 * @property int subscription
 * @property string name
 * @property string reference
 * @property float priceExcludingTax
 * @property Account mainManager;
 * @property Agency agency
 * @property Pole pole
 * @property Document[] $files
 */
class Product extends ModelJSONAPI  implements HasManagerInterface, HasPoleInterface, HasAgencyInterface{
	use HasAgencyTrait, HasPoleTrait, HasManagerTrait;

	const MAPPER = [
		'ID_PRODUIT'           => ['name' => 'id', 'type' => self::TYPE_INT],
		'PRODUIT_REF'          => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'PRODUIT_TYPE'         => ['name' => 'subscription', 'type' => self::TYPE_INT],
		'PRODUIT_NOM'          => ['name' => 'name', 'type' => self::TYPE_STRING],
		'PRODUIT_TARIFHT'      => ['name' => 'priceExcludingTax', 'type' => self::TYPE_FLOAT],
		'PRODUIT_DEVISE'       => ['name' => 'currency', 'type' => self::TYPE_INT],
		'PRODUIT_CHANGE'       => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'PRODUIT_DATEUPDATE'   => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'PRODUIT_DEVISEAGENCE' => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'PRODUIT_CHANGEAGENCE' => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'PRODUIT_ETAT'         => ['name' => 'state', 'type' => self::TYPE_INT],
		'PRODUIT_TAUXTVA'      => ['name' => 'taxRate', 'type' => self::TYPE_FLOAT],
		'PRODUIT_DESCRIPTION'  => ['name' => 'description', 'type' => self::TYPE_STRING]
	];

	/**
	 * @var string
	 */
	protected static $_jsonType = 'product';

	const REF_PREFIX = 'PROD';

	/**#@+
	 * Availables tabs
	 * @var integer TAB
	 */
	const TAB_INFORMATION = 'information';
	const TAB_ACTIONS = 'actions';
	const TAB_PROJECTS = 'projects';
	const TAB_OPPORTUNITIES = 'opportunities';
	/**#@-*/

	/**#@+
	 * product state
	 * @var int STATE
	 */
	const STATE_ACTIVATED = 1;
	const STATE_LOCKED = 9;
	/**#@-*/

	/**
	 * get a list of all tab ID
	 * @return array
	 */
	public static function getAllTabs(){
		return [
			self::TAB_INFORMATION, self::TAB_ACTIONS, self::TAB_OPPORTUNITIES , self::TAB_PROJECTS
		];
	}

	/**
	 *
	 */
	public function initRelationships()
	{
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);

		$this->setGroupedRelationships('DOCUMENTS', 'files');
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition(){
		return self::MAPPER;
	}
}
