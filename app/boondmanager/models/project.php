<?php
/**
 * project.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * class Project
 * @property string id
 * @property Employee mainManager
 * @property Agency agency
 * @property Pole pole
 * @property Opportunity opportunity
 * @property Contact contact
 * @property Company company
 * @property Contact technical
 * @property array projectManagers
 * @property Candidate candidate
 * @property array files
 * @property array resources
 * @property string creationDate datetime
 * @property string updateDate datetime
 * @property int typeOf
 * @property int mode
 * @property string reference
 * @property boolean showBatchesMarkersTab
 * @property int currency
 * @property float exchangeRate
 * @property int currencyAgency
 * @property float exchangeRateAgency
 * @property int numberOfAdvantages
 * @property int numberOfPurchases
 * @property int state
 * @property string address
 * @property string postcode
 * @property string town
 * @property string country
 * @property string informationComments
 * @property string startDate date
 * @property string endDate date
 * @property boolean synchronizeRemainsToBeDone
 * @property boolean allowCreateMarkersOnBatches
 * @property string update productivityDate
 * @property float remainsToBeDone
 * @property boolean showProductivityPerDelivery
 * @property array batchesMarkers
 * @property array aloneMarkers
 * @property array additionalTurnoverAndCosts
 * @property array productivity
 * @property float turnoverSignedExcludingTax
 * @property float costsSignedExcludingTax
 * @property float marginSignedExcludingTax
 * @property float profitabilitySigned
 * @property float turnoverForecastExcludingTax
 * @property float costsForecastExcludingTax
 * @property float marginForecastExcludingTax
 * @property float profitabilityForecast
 * @property float turnoverSimulatedExcludingTax
 * @property float costsSimulatedExcludingTax
 * @property float marginSimulatedExcludingTax
 * @property float profitabilitySimulated
 * @property float turnoverOrderedExcludingTax
 * @property float deltaOrderedExcludingTax
 * @property float additionalTurnoverExcludingTax
 * @property float additionalCostsExcludingTax
 * @property float turnoverProductionExcludingTax
 * @property float costsProductionExcludingTax
 * @property float marginProductionExcludingTax
 * @property float profitabilityProduction
 * @property float turnoverResourcesExcludingTax
 * @property float turnoverPurchasesAndAdditionnalExcludingTax
 * @property float costsResourcesExcludingTax
 * @property float costsPurchasesAndAdditionnalExcludingTax'
 * @property int numberOfOrders
 * @property Batch[] batches
 * @property Delivery[] deliveries
 * @property Purchase[] purchases
 * @package BoondManager\Models
 */
class Project extends ModelJSONAPI  implements HasAgencyInterface, HasManagerInterface, HasPoleInterface {
	use HasManagerTrait, HasPoleTrait, HasAgencyTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'project';

	/**#@+
	 * Default project state
	 * @var int
	 */
	const DEFAULT_PROJECT_STATE = 1;

	/**#@+
	 * Available tabs
	 * @var int
	 */
	const TAB_INFORMATION = 'information';
	const TAB_BATCHES_MARKERS = 'batches-markers';
	const TAB_ACTIONS = 'actions';
	const TAB_SIMULATION = 'simulation';
	const TAB_DELIVERIES = 'deliveries';
	const TAB_ADVANTAGES = 'advantages';
	const TAB_PURCHASES = 'purchases';
	const TAB_PRODUCTIVITY = 'productivity';
	const TAB_ORDERS = 'orders';
	const TAB_FLAGS = 'attached-flags';

	/**#@+
	 * @var int ERROR
	 */
	const
		ERROR_PROJECT_WRONG_CREATION_TYPE = 3400,
		ERROR_PROJECT_WRONG_MODE = 3401,
		ERROR_PROJECT_COMPANY_CONTACT_HAVE_TO_BE_DEFINED = 3402,
		ERROR_PROJECT_ENDDATE_SUPERIOR_OR_EQUAL_STARTDATE = 3403,
		ERROR_PROJECT_WRONG_STARTDATE = 3404,
		ERROR_PROJECT_WRONG_ENDDATE = 3405,
		ERROR_PROJECT_WRONG_REMAINSTOBEDONE = 3406,
		ERROR_PROJECT_WRONG_ALLOWBATCHESMARKERSTAB = 3407,
		ERROR_PROJECT_WRONG_ALLOWCREATEMARKERSONBATCHES = 3408,
		ERROR_PROJECT_WRONG_SYNCHRONIZEREMAINSTOBEDONE = 3409,
		ERROR_PROJECT_WRONG_SHOWPRODUCTIVITYPERDELIVERY = 3410,
		ERROR_PROJECT_DIFFERENT_MODE_FROM_OPPORTUNITY = 3411,
		ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_ONE_ELEMENT = 3412,
		ERROR_PROJECT_WRONG_ADDITIONALTURNOVERCOSTS_PURCHASE = 3413,
		ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_DURATIONFORECAST_AND_REMAINSTOBEDONE = 3414,
		ERROR_PROJECT_WRONG_BATCHESMARKERS_MARKERS_ID = 3415,
		ERROR_PROJECT_BATCH_CANNOT_BE_CREATED_DELETED = 3416,
		ERROR_PROJECT_WRONG_PROJECTMANAGERS = 3417,
		ERROR_PROJECT_WRONG_BATCHESMARKERS = 3418,
		ERROR_PROJECT_WRONG_BATCHESMARKERS_TITLE = 3419,
		ERROR_PROJECT_WRONG_BATCHESMARKERS_DISTRIBUTIONRATE = 3419;

	const REF_PREFIX = 'PRJ';

	const MAPPER = [
		//TAB_DEFAULT
		'ID_PROJET'        => ['name' => 'id', 'type' => self::TYPE_STRING],
		'PRJ_DATE'         => ['name' => 'creationDate', 'type' => self::TYPE_DATETIME],
		'PRJ_DATEUPDATE'   => ['name' => 'updateDate', 'type' => self::TYPE_DATETIME],
		'PRJ_TYPEREF'      => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'PRJ_TYPE'         => ['name' => 'mode', 'type' => self::TYPE_INT],
		'PRJ_REFERENCE'    => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'PRJ_LOTSTAB'      => ['name' => 'showBatchesMarkersTab', 'type' => self::TYPE_BOOLEAN],
		'PRJ_DEVISE'       => ['name' => 'currency', 'type' => self::TYPE_INT],
		'PRJ_CHANGE'       => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'PRJ_DEVISEAGENCE' => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'PRJ_CHANGEAGENCE' => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'NB_AVANTAGES' 	   => ['name' => 'numberOfAdvantages', 'type' => self::TYPE_INT],
		'NB_ACHATS' 	   => ['name' => 'numberOfPurchases', 'type' => self::TYPE_INT],
		//TAB_INFORMATION
		'PRJ_ETAT'         => ['name' => 'state', 'type' => self::TYPE_INT],
		'PRJ_ADR'          => ['name' => 'address', 'type' => self::TYPE_STRING],
		'PRJ_CP'           => ['name' => 'postcode', 'type' => self::TYPE_STRING],
		'PRJ_VILLE'        => ['name' => 'town', 'type' => self::TYPE_STRING],
		'PRJ_PAYS'         => ['name' => 'country', 'type' => self::TYPE_STRING],
		'PRJ_COMMENTAIRES' => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'PRJ_DEBUT'        => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'PRJ_FIN'          => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		//TAB_BATCHES_MARKERS
		'PRJ_CORRELATIONJALON'  => ['name' => 'synchronizeRemainsToBeDone', 'type' => self::TYPE_BOOLEAN],
		'PRJ_LOTVIEW'  	   => ['name' => 'allowCreateMarkersOnBatches', 'type' => self::TYPE_BOOLEAN],
		'PRJ_DATELOTUPDATE'=> ['name' => 'updateProductivityDate', 'type' => self::TYPE_DATETIME],
		'PRJ_RESTEAFAIRE'  => ['name' => 'remainsToBeDone', 'type' => self::TYPE_FLOAT],
		'batchesMarkers'   => ['name' => 'batchesMarkers', 'type' => self::TYPE_ARRAY],
		'aloneMarkers'     => ['name' => 'aloneMarkers', 'type' => self::TYPE_ARRAY],
		'batches'   	   => ['name' => 'batches', 'type' => self::TYPE_ARRAY],
		//TAB_SIMULATION
		'MISSIONSDETAILS'  => ['name' => 'additionalTurnoverAndCosts', 'type' => self::TYPE_ARRAY],
		'PRJ_CONSOMMATIONVIEW'	=> ['name' => 'showProductivityPerDelivery', 'type' => self::TYPE_BOOLEAN],
		//TAB_PRODUCTIVITTY
		'PRJ_TARIFADDITIONNEL'  => ['name' => 'additionalTurnoverExcludingTax', 'type' => self::TYPE_FLOAT],
		'PRJ_INVESTISSEMENT'    => ['name' => 'additionalCostsExcludingTax', 'type' => self::TYPE_FLOAT],
		//APIs projects
		'TOTAL_CASIMUHT'       => ['name' => 'turnoverSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_MARGESIMUHT'    => ['name' => 'marginSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_RENTASIMU'      => ['name' => 'profitabilitySimulated', 'type' => self::TYPE_FLOAT],
		'TOTAL_COUTSIMUHT'     => ['name' => 'costsSimulatedExcludingTax', 'type' => self::TYPE_FLOAT],
		//APIs projects/{id}/deliveries
		'marginSignedExcludingTax' => ['name' => 'marginSignedExcludingTax', 'type' => self::TYPE_FLOAT],
		'profitabilitySigned'      => ['name' => 'profitabilitySigned', 'type' => self::TYPE_FLOAT],
		'costsSignedExcludingTax'  => ['name' => 'costsSignedExcludingTax', 'type' => self::TYPE_FLOAT],
		'turnoverForecastExcludingTax'  => ['name' => 'turnoverForecastExcludingTax', 'type' => self::TYPE_FLOAT],
		'marginForecastExcludingTax'    => ['name' => 'marginForecastExcludingTax', 'type' => self::TYPE_FLOAT],
		'profitabilityForecast'      	=> ['name' => 'profitabilityForecast', 'type' => self::TYPE_FLOAT],
		'costsForecastExcludingTax'     => ['name' => 'costsForecastExcludingTax', 'type' => self::TYPE_FLOAT],
		//APIs billing-projects-balance
		'TOTAL_CASIGNEHT'  => ['name' => 'turnoverSignedExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_MONTANTBDC' => ['name' => 'turnoverOrderedExcludingTax', 'type' => self::TYPE_FLOAT],
		'DOCUMENT_DELTA'   => ['name' => 'deltaOrderedExcludingTax', 'type' => self::TYPE_FLOAT],
		'NB_BDC'           => ['name' => 'numberOfOrders', 'type' => self::TYPE_INT],
		//APIs projects/{id}/productivity & projects/{id}/orders
		'turnoverProductionExcludingTax'              => ['name' => 'turnoverProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsProductionExcludingTax'                 => ['name' => 'costsProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'marginProductionExcludingTax'                => ['name' => 'marginProductionExcludingTax', 'type' => self::TYPE_FLOAT],
		'profitabilityProduction'                     => ['name' => 'profitabilityProduction', 'type' => self::TYPE_FLOAT],
		'turnoverResourcesExcludingTax'               => ['name' => 'turnoverResourcesExcludingTax', 'type' => self::TYPE_FLOAT],
		'turnoverPurchasesAndAdditionnalExcludingTax' => ['name' => 'turnoverPurchasesAndAdditionnalExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsResourcesExcludingTax'                  => ['name' => 'costsResourcesExcludingTax', 'type' => self::TYPE_FLOAT],
		'costsPurchasesAndAdditionnalExcludingTax'    => ['name' => 'costsPurchasesAndAdditionnalExcludingTax', 'type' => self::TYPE_FLOAT],
		// orders only
		'turnoverInvoicedExcludingTax' => ['name' => 'turnoverInvoicedExcludingTax', 'type' => self::TYPE_FLOAT],
		'canReadProject' => ['name' => 'canReadProject', 'type' => self::TYPE_BOOLEAN],
		'canWriteProject' => ['name' => 'canWriteProject', 'type' => self::TYPE_BOOLEAN],
	];

	/**
	 *
	 */
	public function initRelationships() {
		//TAB_DEFAULT
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);
		$this->setGroupedRelationships('ID_PROFILCDP', 'projectManagers');
		//TAB_INFORMATION
		$this->setRelationships('ID_AO', 'opportunity', Opportunity::class);
		$this->setRelationships('ID_CRMTECHNIQUE', 'technical', Contact::class);
		$this->setRelationships('ID_CRMCONTACT', 'contact', Contact::class);
		$this->setRelationships('ID_CRMSOCIETE', 'company', Company::class);
		$this->setRelationships('CANDIDATE', 'candidate', Candidate::class);
		$this->setGroupedRelationships('FILES', 'files');
		//TAB_BATCHES_MARKERS, TAB_SIMULATION
		$this->setGroupedRelationships('RESOURCES', 'resources');
		//TAB_PRODUCTIVITY
		$this->setGroupedRelationships('deliveries', 'deliveries');
		//APIs times-reports, expenses-reports
		$this->setGroupedRelationships('batches', 'batches');
		$this->setGroupedRelationships('purchases', 'purchases');
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function isInactivity(){
		return $this->mode < 0;
	}

	public function getInactivityType(){
		return Tools::mapData($this->mode, Inactivity::INACTIVITY_MAPPER);
	}

	/**
	 * @return array
	 */
	public static function getAllTabs() {
		return [
			self::TAB_INFORMATION,
			self::TAB_BATCHES_MARKERS,
			self::TAB_ACTIONS,
			self::TAB_SIMULATION,
			self::TAB_DELIVERIES,
			self::TAB_ADVANTAGES,
			self::TAB_PURCHASES,
			self::TAB_PRODUCTIVITY,
			self::TAB_ORDERS,
			self::TAB_FLAGS
		];
	}
}
