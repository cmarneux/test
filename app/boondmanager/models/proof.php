<?php
/**
 * resume.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractFile;
use BoondManager\Databases\Local;

/**
 * class Resume
 * @property int $id
 * @property string $name
 * @property string JUSTIF_TYPE
 * @package BoondManager\Models
 */
class Proof extends AbstractFile {

	const MAPPER = [
		'ID_JUSTIFICATIF' => ['name' => 'id', 'type' => self::TYPE_STRING],
		'FILE_NAME'       => ['name' => 'name', 'type' => self::TYPE_STRING],
	];

	/**#@+
	 * @var int JUSTIF_TYPE
	 */
	const
		TYPE_EXPENSESREPORT = 0,
		TYPE_TIMESREPORT = 1,
		TYPE_ABSENCESREPORT = 2,
		TYPE_PAYMENT = 3;
	/**#@-*/

	const PARENT_TYPE_ABSENCES_REPORT = 'absencesReport';
	const PARENT_TYPE_TIMES_REPORT = 'timesReport';
	const PARENT_TYPE_EXPENSES_REPORT = 'expensesReport';
	const PARENT_TYPE_PAYMENT = 'payment';

	const SUBTYPE = 'proof';

	/**
	 * @var Resource
	 */
	public $resource;

	/**
	 * @var Candidate
	 */
	public $candidate;

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function getParentType() {
		switch ($this->JUSTIF_TYPE) {
			case self::TYPE_EXPENSESREPORT: return self::PARENT_TYPE_EXPENSES_REPORT;
			case self::TYPE_ABSENCESREPORT: return self::PARENT_TYPE_EXPENSES_REPORT;
			case self::TYPE_TIMESREPORT: return self::PARENT_TYPE_TIMES_REPORT;
			case self::TYPE_PAYMENT: return self::PARENT_TYPE_PAYMENT;
		}
	}

	/**
	 * @return int
	 */
	public function getSubTypeID() {
		return Local\File::TYPE_JUSTIFICATIVE;
	}
}
