<?php
/**
 * purchase.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasAgencyTrait;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasManagerTrait;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\Models\HasPoleTrait;
use Wish\Models\ModelJSONAPI;

/**
 * class Purchase
 * @property Company company
 * @property Account mainManager
 * @property Contact contact
 * @property Project project
 * @property Agency agency
 * @property Pole pole
 * @property Delivery delivery
 * @property Document[] DOCUMENTS
 * @property int $id
 * @property string $title
 * @property string $reference
 * @property string $date
 * @property string $number
 * @property string $startDate
 * @property string $endDate
 * @property int $subscription
 * @property int $typeOf
 * @property int $currency
 * @property int $currencyAgency
 * @property int $exchangeRate
 * @property int $exchangeRateAgency
 * @property float $taxRate
 * @property float $amountExcludingTax
 * @property float $amountIncludingTax
 * @property int $quantity
 * @property boolean ACHAT_SHOWCOMMENTAIRE
 * @property float $reinvoiceAmountExcludingTax
 * @property float $reinvoiceRate
 * @property boolean $toReinvoice
 * @property float $totalAmountIncludingTax
 * @property float $totalAmountExcludingTax
 * @property mixed ACHAT_CONDREGLEMENT
 * @property mixed ACHAT_TYPEPAYMENT
 * @property float engagedPaymentsAmountExcludingTax
 * @property float deltaExcludingTax
 * @property boolean canReadPurchase
 * @property boolean canWritePurchase
 * @property Document[] files
 * @property Order order last correlated order
 */
class Purchase extends ModelJSONAPI  implements HasManagerInterface, HasAgencyInterface, HasPoleInterface{
	use HasManagerTrait, HasPoleTrait, HasAgencyTrait;
	/**
	 * @var string
	 */
	protected static $_jsonType = 'purchase';

	/**#@+
	 * Available tabs
	 * @var string
	 */
	const TAB_DEFAULT = 0;
	const TAB_INFORMATION = 'information';
	const TAB_ACTIONS = 'actions';
	const TAB_PAYMENTS = 'payments';
	const TAB_FLAGS = 'flags';

	/**#@+
	 * purchase state
	 * @var int ACHAT_ETAT
	 */
	const STATE_PLANNED = 9;
	const STATE_VALIDATED = 1;
	/**#@-*/

	const MAPPER = [
		'ID_ACHAT'                => ['name' => 'id', 'type' => self::TYPE_STRING],
		'ACHAT_DATE'              => ['name' => 'date', 'type' => self::TYPE_DATE],
		'ACHAT_TITLE'             => ['name' => 'title', 'type' => self::TYPE_STRING],
		'ACHAT_TYPE'              => ['name' => 'subscription', 'type' => self::TYPE_INT],
		'ACHAT_CATEGORIE'         => ['name' => 'typeOf', 'type' => self::TYPE_INT],
		'ACHAT_REF'               => ['name' => 'reference', 'type' => self::TYPE_STRING],
		'ACHAT_REFFOURNISSEUR'    => ['name' => 'number', 'type' => self::TYPE_STRING],
		'ACHAT_CONDREGLEMENT'     => ['name' => 'paymentTerm', 'type' => self::TYPE_INT],
		'ACHAT_TYPEPAYMENT'       => ['name' => 'paymentMethod', 'type' => self::TYPE_INT],
		'ACHAT_TAUXTVA'           => ['name' => 'taxRate', 'type' => self::TYPE_FLOAT],
		'ACHAT_COMMENTAIRE'       => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'ACHAT_QUANTITE'          => ['name' => 'quantity', 'type' => self::TYPE_INT],
		'ACHAT_MONTANTHT'         => ['name' => 'amountExcludingTax', 'type' => self::TYPE_FLOAT],
		'ACHAT_MONTANTTTC'        => ['name' => 'amountIncludingTax', 'type' => self::TYPE_FLOAT],
		'ACHAT_ETAT'              => ['name' => 'state', 'type' => self::TYPE_INT],
		'ACHAT_DEVISE'            => ['name' => 'currency', 'type' => self::TYPE_INT],
		'ACHAT_CHANGE'            => ['name' => 'exchangeRate', 'type' => self::TYPE_FLOAT],
		'ACHAT_DEVISEAGENCE'      => ['name' => 'currencyAgency', 'type' => self::TYPE_INT],
		'ACHAT_CHANGEAGENCE'      => ['name' => 'exchangeRateAgency', 'type' => self::TYPE_FLOAT],
		'ACHAT_REFACTURE'         => ['name' => 'toReinvoice', 'type' => self::TYPE_BOOLEAN],
		'ACHAT_TAUXREFACTURATION' => ['name' => 'reinvoiceRate', 'type' => self::TYPE_FLOAT],
		'ACHAT_TARIFFACTURE'      => ['name' => 'reinvoiceAmountExcludingTax', 'type' => self::TYPE_FLOAT],
		'ACHAT_SHOWCOMMENTAIRE'   => ['name' => 'showInformationCommentsOnPDF', 'type' => self::TYPE_BOOLEAN],
		'TOTAL_MONTANT'           => ['name' => 'totalAmountExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_MONTANTTTC'        => ['name' => 'totalAmountIncludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_PAIEMENTS'         => ['name' => 'paymentsAmountExcludingTax', 'type' => self::TYPE_FLOAT],
		'TOTAL_CONSOMMES'         => ['name' => 'engagedPaymentsAmountExcludingTax', 'type' => self::TYPE_FLOAT],
		'DELTAEXCLUDINGTAX'       => ['name' => 'deltaExcludingTax', 'type' => self::TYPE_FLOAT],
		// les 4 champs suivants sont utilisés par l'api billing-deliveries-purchases-balance
		'NB_BDC'                  => ['name' => 'numberOfOrders', 'type' => self::TYPE_INT],
		'NB_COR'                  => ['name' => 'numberOfCorrelatedOrders', 'type' => self::TYPE_INT],
		'ACHAT_DEBUT'             => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'ACHAT_FIN'               => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		// rights
		'canReadPurchase'         => ['name' => 'canReadPurchase', 'type' => self::TYPE_BOOLEAN],
		'canWritePurchase'        => ['name' => 'canWritePurchase', 'type' => self::TYPE_BOOLEAN]
	];

	const REF_PREFIX = 'ACH';

	/**#@+
	 * Errors
	 */
	const ERROR_ADDITIONAL_COSTS_MUST_BELONGS_TO_SAME_PROJECTS = 3600;
	const ERROR_ADDITIONAL_COSTS_MUST_NOT_BE_RELATED_TO_OTHER_PURCHASES = 3601;
	const ERROR_ADDITIONAL_COSTS_MUST_BE_SET_WITH_PROJECTS = 3602;
	const ERROR_COMPANY_AND_CONTACTS_MUST_BOTH_BE_SET = 3603;
	const ERROR_ENDDATE_SUPERIOR_AT_STARTDATE = 3604;
	const ERROR_NO_STARTDATE_IF_UNITARY = 3605;
	const ERROR_NO_ENDDATE_IF_UNITARY = 3606;
	const ERROR_FIELD_CANTBE_SET_WITH_DELIVERY = 3607;
	const ERROR_AMOUNT_EXCLUDING_TAX_MUST_BE_INFERIOR_AT_INCLUDING_TAX = 3608;
	const ERROR_PURCHASE_IS_NOT_REINVOICED = 3609;
	const ERROR_REINVOICED_AMOUNT_AND_RATE_CANT_BE_BOTH_SET = 3610;
	const ERROR_BILLING_DETAILS_REQUIRE_COMPANY = 3611;
	/**#@- */

	const
		CREATION_MANUALLY = 'manually',
		CREATION_AUTO_PLANNED = 'automaticallyInPlannedState',
		CREATION_AUTO_CONFIRMED = 'automaticallyInConfirmedState',
		CREATION_AUTO_ORDERED = 'automaticallyInWellOrderedState';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships()
	{
		$this->setRelationships('ID_PROFIL', 'mainManager', Employee::class);
		$this->setRelationships('ID_POLE', 'pole', Pole::class);
		$this->setRelationships('ID_SOCIETE', 'agency', Agency::class);
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setRelationships('ID_CRMCONTACT', 'contact', Contact::class);
		$this->setRelationships('ID_CRMSOCIETE', 'company', Company::class);
		$this->setRelationships('ID_MISSIONPROJET', 'delivery', Delivery::class);
		$this->setRelationships('order', 'order', Order::class);
		$this->setGroupedRelationships('DOCUMENTS', 'files');
	}

	/**
	 * get all tabs
	 * @return array
	 */
	public static function getAllTabs(){
		return [
			self::TAB_INFORMATION, self::TAB_ACTIONS, self::TAB_PAYMENTS,
		];
	}

	/**
	 * @return $this
	 */
	public function calculateData() {
		$this->calculateReinvoiced();
		$this->calculateAmounts();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateReinvoiced() {
		if($this->toReinvoice && $this->reinvoiceRate) {
			$this->reinvoiceAmountExcludingTax = $this->amountExcludingTax * $this->quantity * (1 + $this->taxRate/100) / (1 - $this->reinvoiceRate/100);
		}
		return $this;
	}

	/**
	 * @return $this
	 */
	public function calculateAmounts() {
		$this->totalAmountExcludingTax = $this->amountExcludingTax * $this->quantity;
		$this->totalAmountIncludingTax = $this->amountIncludingTax * $this->quantity;
		$this->amountIncludingTax      = $this->amountExcludingTax * ( 1 + $this->taxRate/100 );
		$this->deltaExcludingTax       = $this->engagedPaymentsAmountExcludingTax - $this->totalAmountExcludingTax;
		return $this;
	}
}
