<?php
/**
 * rateperkilometertype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class RatePerKilometerType
 * @property string id
 * @property int reference
 * @property string name
 * @property bool state
 * @property float amount
 */
class RatePerKilometerType extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'rateperkilometertype';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_BAREMEKM'  => ['name' => 'id', 'type' => self::TYPE_STRING],
			'BKM_REF' 	   => ['name'=>'reference', 'type'=>self::TYPE_INT],
			'BKM_NAME' 	   => ['name'=>'name', 'type'=>self::TYPE_STRING],
			'BKM_VALUE'    => ['name'=>'amount', 'type'=>self::TYPE_FLOAT],
			'BKM_ETAT'         => ['name' => 'state', 'type' => self::TYPE_BOOLEAN]
		];
	}
}
