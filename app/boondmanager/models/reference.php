<?php
/**
 * reference.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class Reference
 * @package BoondManager\Models\MySQL\RowObject
 * @property int $id
 * @property string $title
 * @property string $description
 */
class Reference extends ModelJSON{

	const MAPPER = [
		'ID_REFERENCE'    => ['name' => 'id', 'type' => self::TYPE_STRING],
		'REF_TITRE'       => ['name' => 'title', 'type' => self::TYPE_STRING],
		'REF_DESCRIPTION' => ['name' => 'description', 'type' => self::TYPE_STRING]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
