<?php
/**
 * resume.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractFile;
use BoondManager\Databases\Local;

/**
 * class Resume
 * @property int $id
 * @property int ID_PROFIL
 * @property string $name
 * @property string CV_TEXT
 * @property Candidate|Employee dependsOn
 */
class Resume extends AbstractFile {

	const PARENT_TYPE_CANDIDATE_RESUME = 'candidateResume';
	const PARENT_TYPE_RESOURCE_RESUME = 'resourceResume';

	const SUBTYPE = 'resume';

	protected static $_jsonType = 'document';

	const MAPPER = [
		'ID_CV'   => ['name' => 'id', 'type' => self::TYPE_INT],
		'CV_NAME' => ['name' => 'name', 'type' => self::TYPE_STRING],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 * @return string
	 */
	public function getParentType() {
		switch (get_class($this->dependsOn)) {
			case Employee::class: return self::PARENT_TYPE_RESOURCE_RESUME;
			case Candidate::class: return self::PARENT_TYPE_CANDIDATE_RESUME;
			default: return null;
		}
	}

	/**
	 * @return int
	 */
	public function getSubTypeID() {
		if($this->dependsOn instanceof Employee) return Local\File::TYPE_RESOURCE_RESUME;
		else return Local\File::TYPE_CANDIDATE_RESUME;
	}
}
