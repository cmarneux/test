<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\JSONApiObjectTrait;
use Wish\Models\JSONApiObjectInterface;
use BoondManager\Services\CurrentUser;

class Rights implements JSONApiObjectInterface {
	use JSONApiObjectTrait;

	protected static $_jsonType = 'rights';

	private $user, $module, $entity;

	private $actions = [],
	        $apis    = [],
	        $fields  = [];

	public function __construct(CurrentUser $user, $module, JSONApiObjectInterface $entity = null)
	{
		$this->user = $user;
		$this->module = $module;
		$this->entity = $entity;
	}

	public function getID()
	{
		$id = [$this->module];
		if($this->entity) $id[] = $this->entity->getID();
		return implode('_', $id);
	}

	public function getAttributes()
	{
		$attributes = [];
		if(sizeof($this->actions) > 0) $attributes['actions'] = $this->actions;
		if(sizeof($this->apis) > 0) $attributes['apis'] = $this->apis;
		if(sizeof($this->fields) > 0) $attributes['attributes'] = $this->fields;
		return $attributes;
	}

	public function addAction($key, $value){
		$this->actions[$key] = $value;
		return $this;
	}

	public function addApi($key, $read, $write){
		$this->apis[$key] = ['read' => $read, 'write'=>$write];
		return $this;
	}

	public function addField($key, $read, $write){
		$this->fields[$key]['read'] = $read;
		$this->fields[$key]['write'] = $write;
		return $this;
	}
}
