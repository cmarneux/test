<?php
/**
 * role.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class Role
 * @property int id
 * @property string name
 *
 * @package BoondManager\Models
 */
class Role extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'role';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_ROLE'           => ['name' => 'id', 'type' => self::TYPE_STRING],
			'ROLE_NAME'      	=> ['name' => 'name', 'type' => self::TYPE_STRING]
		];
	}
}
