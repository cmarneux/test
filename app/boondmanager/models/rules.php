<?php
/**
 * rules.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * Class Rules
 * @package BoondManager\Models\MySQL\RowObject
 * @property int $id
 * @property mixed REGLE_BAREMEREF
 * @property mixed $state
 * @property int $reference
 * @property string $name
 * @property float $priceExcludingTaxOrPriceRate
 * @property float $grossCostOrSalaryRate
 * @property mixed ID_PARENT
 * @package BoondManager\Models
 */
class Rules extends ModelJSONAPI {

	protected $_jsonIDAttribute = 'reference';

	const MAPPER = [
		'REGLE_REF'   => ['name' => 'reference', self::TYPE_INT],
		'REGLE_NAME'  => ['name' => 'name', self::TYPE_STRING],
		'REGLE_TARIF' => ['name' => 'priceExcludingTaxOrPriceRate', self::TYPE_FLOAT],
		'REGLE_COUT'  => ['name' => 'grossCostOrSalaryRate', self::TYPE_FLOAT]
	];

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->toArray();
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}
}
