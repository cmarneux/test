<?php
/**
 * schedule.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int $id
 * @property int NB_FACTURE
 * @package BoondManager\Models
 */
class Schedule extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'schedule';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_ECHEANCIER'     => ['name' => 'id', 'type' => self::TYPE_INT],
			'ECH_DATE'          => ['name' => 'date', 'type' => self::TYPE_DATE],
			'ECH_DESCRIPTION'   => ['name' => 'title', 'type' => self::TYPE_STRING],
			'ECH_MONTANTHT'     => ['name' => 'turnoverTermOfPaymentExcludingTax', 'type' => self::TYPE_FLOAT],
			'TOTAL_CAFACTUREHT' => ['name' => 'turnoverInvoicedExcludingTax', 'type' => self::TYPE_FLOAT],
			'DOCUMENT_DELTA'    => ['name' => 'deltaInvoicedExcludingTax', 'type' => self::TYPE_FLOAT],
			// check doc
			'NB_FACTURE'        => ['name' => 'NB_FACTURE', 'type' => self::TYPE_INT]
		];
	}

	/**
	 *
	 */
	public function initRelationships(){
		$this->setRelationships('ID_BONDECOMMANDE', 'order', Order::class);
		//~ $this->setRelationships('ID_FACTURATION', 'invoice', Invoice::class); // FIXME : Plusieurs factures peuvent être asscociées à une même échéance
	}
}
