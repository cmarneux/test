<?php
/**
 * billingschedulesbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models\SearchResults;

use Wish\Models\SearchResult;


/**
 * Class BillingSchedulesBalance
 * @package BoondManager\Databases\Local\SearchResults;
 *
 */
class BillingSchedulesBalance extends SearchResult {
	/**
	 * BillingSchedulesBalance constructor.
	 * @param null $rows
	 * @param null $total
	 * @param array $meta
	 */
	public function __construct($rows = null, $total=null, $meta=[]) {
		parent::__construct($rows, $total);
	}

}
