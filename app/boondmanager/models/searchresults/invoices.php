<?php
/**
 * invoices.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models\SearchResults;

use Wish\Models\SearchResult;


/**
 * Class Invoices
 * @package BoondManager\Databases\Local\SearchResults;
 *
 */
class Invoices extends SearchResult {

	public $turnoverExcludingTax = 0;
	public $turnoverIncludingTax = 0;

	private $states = [];

	/**
	 * Invoices constructor.
	 * @param null $rows
	 * @param null $total
	 * @param array $meta
	 */
	public function __construct($rows = null, $total=null, $meta=[]) {
		parent::__construct($rows, $total);
	}

	/**
	 * create a state amount
	 * @param $id
	 * @param $amount
	 * @param null $amountTTC
	 */
	public function setStateAmount($id, $amount, $amountTTC = null)
	{
		$this->states[$id] = new StateInvoices($id, floatval($amount), floatval($amountTTC));
	}

	/**
	 * @return StatePayment[]
	 */
	public function getStatesAmounts()
	{
		return array_values($this->states);
	}
}
