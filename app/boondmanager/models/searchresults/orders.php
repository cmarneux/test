<?php
/**
 * orders.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\SearchResults;

use BoondManager\Models\Order;
use Wish\Models\SearchResult;


/**
 * Class Opportunities
 * @package BoondManager\Databases\Local\SearchResults;
 * @property Order[] $rows
 */
class Orders extends SearchResult {

	/**
	 * @var float Sum of invoiced amount excluding tax
	 */
	public $turnoverInvoicedExcludingTax;
	/**
	 * @var float Sum of ordered amount excluding tax
	 */
	public $turnoverOrderedExcludingTax;
	/**
	 * @var float delta
	 */
	public $deltaInvoicedExcludingTax;

	/**
	 * Opportunities constructor.
	 * @param null $rows
	 * @param null $total
	 * @param array $meta
	 */
	public function __construct($rows = null, $total=null, $meta=[]) {
		parent::__construct($rows, $total);
	}

}
