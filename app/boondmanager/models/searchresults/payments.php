<?php
/**
 * Created by PhpStorm.
 * User: wish
 * Date: 29/11/2016
 * Time: 10:58
 */
namespace BoondManager\Models\SearchResults;

use Wish\Models\SearchResult;

/**
 * Class Payments
 * @package BoondManager\Models\MySQL\Local\SearchResults
 * @property float TOTAL_MONTANTHT
 * @property float TOTAL_MONTANTTTC
 * @property float TOTAL_PAIEMENTSHT
 * @property float TOTAL_PAIEMENTSTTC
 *
 */
class Payments extends SearchResult
{

	public $TOTAL_CONSOMMESHT, $TOTAL_CONSOMMESTTC;

	/**
	 * @var StatePayment[]
	 */
	private $states = [];

	/**
	 * create a state amount
	 * @param $id
	 * @param $amount
	 * @param null $amountTTC
	 */
	public function addStateAmount($id, $amount, $amountTTC = null)
	{
		$this->states[] = new StatePayment($id, floatval($amount), floatval($amountTTC));

		$this->__set('TOTAL_PMTHT' . $id, $amount);
		$this->__set('TOTAL_PMTTTC' . $id, $amountTTC);

		if ($id > 0) {
			$this->TOTAL_CONSOMMESHT += $amount;
			$this->TOTAL_CONSOMMESTTC += $amountTTC;
		}
	}

	/**
	 * @return StatePayment[]
	 */
	public function getStatesAmounts()
	{
		return $this->states;
	}
}
