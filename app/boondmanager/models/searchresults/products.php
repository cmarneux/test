<?php
/**
 * products.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\Models\SearchResults;

use Wish\Models\SearchResult;


/**
 * Class Products
 * @package BoondManager\Databases\Local\SearchResults;
 *
 */
class Products extends SearchResult {
	/**
	 * Products constructor.
	 * @param null $rows
	 * @param null $total
	 * @param array $meta
	 */
	public function __construct($rows = null, $total=null, $meta=[]) {
		parent::__construct($rows, $total);
	}

}
