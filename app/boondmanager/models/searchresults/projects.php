<?php
/**
 * projects.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models\SearchResults;

use BoondManager\Services\BM;
use Wish\Models\SearchResult;


/**
 * Class Projects
 * @package BoondManager\Databases\Local\SearchResults;
 * @property float turnoverSimulatedExcludingTax
 * @property float costsSimulatedExcludingTax
 * @property float marginSimulatedExcludingTax
 * @property float profitabilitySimulated
 *
 */
class Projects extends SearchResult {

	/**
	 * Projects constructor.
	 * @param null $rows
	 * @param null $total
	 * @param array $meta
	 */
	public function __construct($rows = null, $total=null, $meta=[]) {
		parent::__construct($rows, $total);

		if($meta) {
			$this->turnoverSimulatedExcludingTax = $meta['TOTAL_CASIMUHT'];
			$this->costsSimulatedExcludingTax = $meta['TOTAL_COUTSIMUHT'];
			$this->marginSimulatedExcludingTax = $this->turnoverSimulatedExcludingTax - $this->costsSimulatedExcludingTax;
			if(BM::isProfitabilityCalculatingBasedOnMarginRate())
				$this->profitabilitySimulated = $this->costsSimulatedExcludingTax == 0 ? 0 : 100 * $this->marginSimulatedExcludingTax / $this->costsSimulatedExcludingTax;
			else
				$this->profitabilitySimulated = $this->turnoverSimulatedExcludingTax == 0 ? 0 : 100 * $this->marginSimulatedExcludingTax / $this->turnoverSimulatedExcludingTax;
		}else{
			$this->turnoverSimulatedExcludingTax = $this->costsSimulatedExcludingTax = $this->marginSimulatedExcludingTax = $this->profitabilitySimulated = 0;
		}
	}

}
