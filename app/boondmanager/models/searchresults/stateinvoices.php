<?php
/**
 * stateinvoices.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models\SearchResults;

class StateInvoices
{
	public $id, $turnoverInvoicedExcludingTax, $turnoverInvoicedIncludingTax;

	public function __construct($id, $amount, $amountTTC)
	{
		$this->id = $id;
		$this->turnoverInvoicedExcludingTax = $amount;
		$this->turnoverInvoicedIncludingTax = $amountTTC;
	}
}
