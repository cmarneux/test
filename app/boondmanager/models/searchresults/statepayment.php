<?php
/**
 * Created by PhpStorm.
 * User: wish
 * Date: 29/11/2016
 * Time: 11:02
 */
namespace BoondManager\Models\SearchResults;

class StatePayment
{
	public $id, $amountExcludingTax, $amountIncludingTax;

	public function __construct($id, $amount, $amountTTC)
	{
		$this->id = $id;
		$this->amountExcludingTax = $amount;
		$this->amountIncludingTax = $amountTTC;
	}
}
