<?php
/**
 * subscription.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Subscription
 * @property int id
 * @property string startDate
 * @property string endDate
 * @property int state
 * @property int numberOfActivableManagers
 * @property int numberOfIntranetActivated
 * @property int numberOfIntranetMax
 * @property int numberOfResourcesNotArchived
 * @property int numberOfResourcesMaxPerManager
 * @property int storageQuantityMax
 * @property float storageQuantityUse
 */
class Subscription extends ModelJSONAPI {
	/**
	 * @var string
	 */
	protected static $_jsonType = 'subscription';

	const DEFAULT_STORAGE_PER_MANAGER = 3;
	const DEFAULT_RESOURCES_PER_MANAGER = 15;
	const DEFAULT_MAX_RESOURCES_INTRANET = 0;

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'ID_ABONNEMENT' => ['name' => 'id', 'type' => self::TYPE_INT],
			'AB_DEBUT' => ['name' => 'startDate', 'type' => self::TYPE_DATE],
			'AB_FIN' => ['name' => 'endDate', 'type' => self::TYPE_DATE],
			'AB_TYPE' => ['name' => 'state', 'type' => self::TYPE_STRING],
			'AB_NBMANAGERS' => ['name' => 'numberOfActivableManagers', 'type' => self::TYPE_INT],
			'NBINTRANET' => ['name' => 'numberOfIntranetActivated', 'type' => self::TYPE_INT],
			'AB_MAXRESSOURCESINTRANET' => ['name' => 'numberOfIntranetMax', 'type' => self::TYPE_INT],
			'NBRESSOURCES' => ['name' => 'numberOfResourcesNotArchived', 'type' => self::TYPE_INT],
			'AB_MAXRESSOURCESPERMANAGER' => ['name' => 'numberOfResourcesMaxPerManager', 'type' => self::TYPE_INT],
			'AB_MAXSTORAGE' => ['name' => 'storageQuantityMax', 'type' => self::TYPE_INT],
			'AB_STORAGEUSE' => ['name' => 'storageQuantityUse', 'type' => self::TYPE_FLOAT]
		];
	}

	/**
	 * @return string
	 */
	public function getReference() {
		return 'CUS'.$this->id;
	}
}
