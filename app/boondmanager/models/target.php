<?php
/**
 * target.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int id
 * @property string category
 * @property string operator
 * @property string periodType
 * @property string periodNumber
 * @property string periodYear
 * @property int value
 * @property bool scorecard
 */
class Target extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'target';

	const MAPPING = [
		'ID_OBJECTIF'    => ['name' => 'id', 'type' => self::TYPE_INT],
		'OBJ_CATEGORIE'  => ['name' => 'category', 'type' => self::TYPE_STRING, 'mapper' => self::CATEGORY_MAPPER],
		'OBJ_OPERATEUR'  => ['name' => 'operator', 'type' => self::TYPE_STRING],
		'OBJ_PERIODE'    => ['name' => 'periodType', 'type' => self::TYPE_STRING, 'mapper' => self::PERIOD_MAPPER],
		'periodNumber'   => ['name' => 'periodNumber', 'type' => self::TYPE_IGNORE],
		'periodYear'     => ['name' => 'periodYear', 'type' => self::TYPE_IGNORE],
		'OBJ_VALUE'      => ['name' => 'value', 'type' => self::TYPE_INT],
		'OBJ_INDICATEUR' => ['name' => 'scorecard', 'type' => self::TYPE_OBJECT],
	];

	const PERIOD_WEEKLY = 'weekly';
	const PERIOD_MONTHLY = 'monthly';
	const PERIOD_QUATERLY = 'quaterly';
	const PERIOD_SEMIANNUAL = 'semiAnnual';
	const PERIOD_ANNUAL = 'annual';

	const ALL_DATES = 'allDates' ;

	const PERIOD_MAPPER = [
		0 => self::PERIOD_WEEKLY,
		1 => self::PERIOD_MONTHLY,
		2 => self::PERIOD_QUATERLY,
		3 => self::PERIOD_SEMIANNUAL,
		4 => self::PERIOD_ANNUAL
	];

	const OPERATOR_LESS = '<';
	const OPERATOR_LESS_OR_EQUAL = '<=';
	const OPERATOR_EQUAL = '=';
	const OPERATOR_MORE = '>';
	const OPERATOR_MORE_OR_EQUAL = '>=';

	const CATEGORY_COMMERCIAL = 'commercial';
	const CATEGORY_HR = 'humanResources';
	const CATEGORY_RECRUITMENT = 'recruitment';
	const CATEGORY_ACTIVITY = 'activityExpenses';
	const CATEGORY_BILLING = 'billing';
	const CATEGORY_GLOBAL = 'global';

	const CATEGORY_MAPPER = [
		0 => self::CATEGORY_COMMERCIAL,
		1 => self::CATEGORY_HR,
		2 => self::CATEGORY_RECRUITMENT,
		3 => self::CATEGORY_ACTIVITY,
		4 => self::CATEGORY_GLOBAL,
		5 => self::CATEGORY_BILLING
	];

	const INDICATOR_COMMERCIAL_MAPPER = [
		['id' => 0, 'name' => "turnoverSignedExcludingTax", 'typeOf' => 'money'],
		['id' => 1, 'name' => 'turnoverWeightedExcludingTax', 'typeOf' => 'money'],
		['id' => 2, 'name' => 'costsSignedExcludingTax', 'typeOf' => 'money'],
		['id' => 3, 'name' => 'marginSignedExcludingTax', 'typeOf' => 'money'],
		['id' => 4, 'name' => 'profitabilitySigned', 'typeOf' => 'percentage'],
		['id' => 5, 'name' => 'numberOfProjectsCreated', 'typeOf' => 'number'],
		['id' => 6, 'name' => 'numberofProjectsInProgress', 'typeOf' => 'number'],
		['id' => 7, 'name' => 'numberOfDeliveriesInProgress', 'typeOf' => 'number'],
		['id' => 8, 'name' => 'numberOfOpportunitiesCreated', 'typeOf' => 'number'],
		['id' => 9, 'name' => 'numberOfPositioningsCreated', 'typeOf' => 'number'],
		['id' => 10, 'name' => 'numberOfActionsOnContactsPerTypes', 'typeOf' => 'number'],
		['id' => 11, 'name' => 'numberOfDeliveriesStarted', 'typeOf' => 'number'],
		['id' => 12, 'name' => 'numberOfDeliveriesStopped', 'typeOf' => 'number'],
		['id' => 13, 'name' => 'turnoverForecastExcludingTax', 'typeOf' => 'money'],
		['id' => 14, 'name' => 'costsForecastExcludingTax', 'typeOf' => 'money'],
		['id' => 15, 'name' => 'marginForecastExcludingTax', 'typeOf' => 'money'],
		['id' => 16, 'name' => 'profitabilityForecast', 'typeOf' => 'money'],
		['id' => 17, 'name' => 'turnoverSimulatedExcludingTax', 'typeOf' => 'money'],
		['id' => 18, 'name' => 'costsSimulatedExcludingTax', 'typeOf' => 'money'],
		['id' => 19, 'name' => 'marginSimulatedExcludingTax', 'typeOf' => 'money'],
		['id' => 20, 'name' => 'profitabilitySimulated', 'typeOf' => 'money'],
		['id' => 21, 'name' => 'numberOfActionsOnOpportunitiesPerTypes', 'typeOf' => 'number'],
		['id' => 22, 'name' => 'numberOfActionsOnProjectsPerTypes', 'typeOf' => 'number'],
		['id' => 23, 'name' => 'numberOfTechnicalAssistanceProjectsInProgress', 'typeOf' => 'number'],
		['id' => 24, 'name' => 'numberofPackageProjectsInProgress', 'typeOf' => 'number'],
		['id' => 25, 'name' => 'rateOfTechnicalAssistanceProjectsInProgress', 'typeOf' => 'percentage'],
		['id' => 26, 'name' => 'rateOfPackageProjectsInProgress', 'typeOf' => 'percentage'],
		['id' => 27, 'name' => 'numberOfRecruitmentProjectsInProgress', 'typeOf' => 'number'],
		['id' => 28, 'name' => 'numberOfProductProjectsInProgress', 'typeOf' => 'number'],
		['id' => 29, 'name' => 'rateOfRecruimentProjectsInProgress', 'typeOf' => 'percentage'],
		['id' => 30, 'name' => 'rateOfProductProjectsInProgress', 'typeOf' => 'percentage'],
		['id' => 31, 'name' => 'turnoverWeightedExcludingTaxPerStates', 'typeOf' => 'money'],
		['id' => 32, 'name' => 'numberOfContactsCreated', 'typeOf' => 'number'],
		['id' => 33, 'name' => 'numberOfCompaniesCreated', 'typeOf' => 'number'],
		['id' => 34, 'name' => "numberOfOpportunitiesCreated", 'typeOf' => 'number'],
		// ne correspond pas à la doc
		['id' => 35, 'name' => 'numberOfActionsCreatedOnContactsPerTypes', 'typeOf' => 'number']
	];

	const INDICATOR_HR_MAPPER = [
		['id' => 0, 'name' => 'numberOfInternalResources', 'typeOf' => 'number'],
		['id' => 1, 'name' => 'numberOfHired', 'typeOf' => 'number'],
		['id' => 2, 'name' => 'numberOfDepartures', 'typeOf' => 'number'],
		['id' => 3, 'name' => 'numberOfResourcesOnDeliveries', 'typeOf' => 'number'],
		['id' => 4, 'name' => 'numberOfExternalResourcesOnDeliveries', 'typeOf' => 'number'],
		['id' => 5, 'name' => 'numberOfInternalResourcesOnDeliveries', 'typeOf' => 'number'],
		['id' => 6, 'name' => 'numberOfResourcesOnInternalActivity', 'typeOf' => 'number'],
		['id' => 7, 'name' => 'numberOfResourcesOnAbsencesActivity', 'typeOf' => 'number'],
		['id' => 8, 'name' => 'numberOfActionsOnResourcesPerTypes', 'typeOf' => 'number'],
		['id' => 9, 'name' => 'costsOfInternalSignedTimeExcludingTax', 'typeOf' => 'money'],
		['id' => 10, 'name' => 'costsOfAbsencesSignedTimeExcludingTax', 'typeOf' => 'money'],
		['id' => 11, 'name' => 'durationOfInternalSignedTime', 'typeOf' => 'number'],
		['id' => 12, 'name' => 'durationOfAbsencesSignedTime', 'typeOf' => 'number'],
		['id' => 13, 'name' => 'internalActivityRate', 'typeOf' => 'percentage'],
		['id' => 14, 'name' => 'occupationRate', 'typeOf' => 'percentage'],
		['id' => 15, 'name' => 'numberOfPositioningsCreated', 'typeOf' => 'number'],
		['id' => 16, 'name' => 'costsOfAllActivityExcludingTax', 'typeOf' => 'money'],
		['id' => 17, 'name' => 'numberOfExternalResources', 'typeOf' => 'number']
	];

	const INDICATOR_RECRUITMENT_MAPPER = [
		['id' => 0, 'name' => 'numberOfCandidatesCreated', 'typeOf' => 'number'],
		['id' => 1, 'name' => 'numberOfActionsOnCandidatesPerTypes', 'typeOf' => 'number'],
		['id' => 2, 'name' => 'numberOfPositioningsCreated', 'typeOf' => 'number'],
		['id' => 3, 'name' => 'numberOfCandidatesCreatedPerStates', 'typeOf' => 'number'],
		['id' => 4, 'name' => 'numberOfCandidatesCreatedPerSources', 'typeOf' => 'number']
	];

	const INDICATOR_ACTIVITY_MAPPER = [
		['id' => 0, 'name' => 'durationOfAllSignedTime', 'typeOf' => 'number'],
		['id' => 1, 'name' => 'durationOfAllUsedTime', 'typeOf' => 'number'],
		['id' => 2, 'name' => 'costsOfSignedExpensesExcludingTax', 'typeOf' => 'money'],
		['id' => 3, 'name' => 'costsOfUsedExpensesExcludingTax', 'typeOf' => 'money'],
		['id' => 4, 'name' => 'numberOfValidatedTimesReports', 'typeOf' => 'number'],
		['id' => 5, 'name' => 'numberOfRejectedTimesReports', 'typeOf' => 'number'],
		['id' => 6, 'name' => 'numberOfWaitingForValidationTimesReports', 'typeOf' => 'number'],
		['id' => 7, 'name' => 'numberOfValidatedExpensesReports', 'typeOf' => 'number'],
		['id' => 8, 'name' => 'numberOfRejectedExpensesReports', 'typeOf' => 'number'],
		['id' => 9, 'name' => 'numberOfWaitingForValidationExpensesReports', 'typeOf' => 'number'],
		['id' => 10, 'name' => 'numberOfValidatedAbsencesReports', 'typeOf' => 'number'],
		['id' => 11, 'name' => 'numberOfRejectedAbsencesReports', 'typeOf' => 'number'],
		['id' => 12, 'name' => 'numberOfWaitingForValidationAbsencesReports', 'typeOf' => 'number'],
		['id' => 13, 'name' => 'durationOfAllPlannedTime', 'typeOf' => 'number'],
		['id' => 14, 'name' => 'durationOfProductionAndExceptionalUsedTimeAndCalendar', 'typeOf' => 'number'],
		['id' => 15, 'name' => 'durationOfAllUsedTimePerActivity', 'typeOf' => 'number'],
		['id' => 16, 'name' => 'costsOfProductionAndExceptionalUsedTimeAndCalendarExcludingTax', 'typeOf' => 'money'],
		['id' => 17, 'name' => 'costsOfAllUsedTimeExcludingTaxPerActivity', 'typeOf' => 'money']
	];

	const INDICATOR_GLOBAL_MAPPER = [
		['id' => 0, 'name' => 'turnoverInvoicedExcludingTaxFollowingInvoicesDatePerStates', 'typeOf' => 'money'],
		['id' => 1, 'name' => 'turnoverInvoicedExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 2, 'name' => 'turnoverOrderedExcludingTax', 'typeOf' => 'money'],
		['id' => 3, 'name' => 'deltaInvoicedExcludingTaxFollowingInvoicesDate', 'typeOf' => ''],
		['id' => 4, 'name' => 'turnoverInvoicedExcludingTaxFollowingCommandsPeriod', 'typeOf' => 'money'],
		['id' => 5, 'name' => 'turnoverInvoicedProFormaExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 6, 'name' => 'turnoverInvoicedExcludingTaxFollowingInvoicesPeriodPerStates', 'typeOf' => 'money'],
		['id' => 7, 'name' => 'turnoverInvoicedExcludingTaxFollowingInvoicesPeriod', 'typeOf' => 'money'],
		['id' => 8, 'name' => 'turnoverInvoicedProFormaExcludingTaxFollowingInvoicesPeriod', 'typeOf' => 'money']
	];

	const INDICATOR_BILLING_MAPPER = [
		['id' => 0, 'name' => 'turnoverSignedExcludingTax', 'typeOf' => 'money'],
		['id' => 1, 'name' => 'costsRealExcludingTax', 'typeOf' => 'money'],
		['id' => 2, 'name' => 'marginRealSignedExcludingTax', 'typeOf' => 'money'],
		['id' => 3, 'name' => 'profitabilityRealSigned', 'typeOf' => 'money'],
		['id' => 4, 'name' => 'turnoverInvoicedPaidExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 5, 'name' => 'marginContributiveInvoicedPaidExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 6, 'name' => 'profitabilityContributiveInvoicedPaidFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 7, 'name' => 'costsAdministrativeExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 8, 'name' => 'marginExploitationInvoicedPaidExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 9, 'name' => 'profitabilityExploitationInvoicedPaidFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 10, 'name' => 'turnoverInvoicedExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 11, 'name' => 'marginContributiveInvoicedExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 12, 'name' => 'profitabilityContributiveInvoicedFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 13, 'name' => 'marginExploitationInvoicedExcludingTaxFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 14, 'name' => 'profitabilityExploitationInvoicedFollowingInvoicesDate', 'typeOf' => 'money'],
		['id' => 15, 'name' => 'turnoverProductionExcludingTax', 'typeOf' => 'money'],
		['id' => 16, 'name' => 'marginContributiveProductionExcludingTax', 'typeOf' => 'money'],
		['id' => 17, 'name' => 'profitabilityContributiveProduction', 'typeOf' => 'money'],
		['id' => 18, 'name' => 'marginExploitationProductionExcludingTax', 'typeOf' => 'money'],
		['id' => 19, 'name' => 'profitabilityExploitationProduction', 'typeOf' => 'money'],
		['id' => 20, 'name' => 'costsOfPurchasesExcludingTax', 'typeOf' => 'money'],
		// ne correspond pas à la doc
		['id' => 21, 'name' => "turn overInvoicedExcludingTaxFollowingInvoicesPeriod"],
		['id' => 22, 'name' => 'marginContributiveInvoicedExcludingTaxFollowingInvoicesPeriod', 'typeOf' => 'money'],
		['id' => 23, 'name' => 'profitabilityContributiveInvoicedFollowingInvoicesPeriod', 'typeOf' => 'money'],
		['id' => 24, 'name' => 'marginExploitationInvoicedExcludingTaxFollowingInvoicesPeriod', 'typeOf' => 'money'],
		['id' => 25, 'name' => 'profitabilityExploitationInvoicedFollowingInvoicesPeriod', 'typeOf' => 'money']
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPING;
	}
}
