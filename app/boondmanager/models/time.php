<?php
/**
 * time.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class Advantage
 * @property int id
 * @property string category
 * @property int row
 * @property string startDate
 * @property string endDate
 * @property float duration
 * @property bool recovering
 * @property string description
 * @property bool calculationMethod
 * @property float cost
 * @property float priceExcludingTax
 * @property bool processed
 * @property array rules
 * @property WorkUnitType workUnitType
 * @property Project project
 * @property Batch batch
 * @property TimesReport timesReport
 * @property Delivery delivery
 * @package BoondManager\Models
 */
class Time extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'time';

	const MAPPER = [
		'ID_TEMPS'              => ['name' => 'id', 'type' => self::TYPE_INT],
		'TEMPS_TYPE'            => ['name' => 'category', 'type' => self::TYPE_STRING],
		'workUnitType'          => ['name' => 'workUnitType', 'type' => self::TYPE_OBJECT],// groupment of TAB_TYPEHEURE.TYPEH_REF, TAB_TYPEHEURE.TYPEH_TYPEACTIVITE and TAB_TYPEHEURE.TYPEH_NAME
		'ID_LIGNETEMPS'         => ['name' => 'row', 'type' => self::TYPE_INT],
		'TEMPS_DATE'            => ['name' => 'startDate', 'type' => self::TYPE_DATE],
		'TEMPSEXP_FIN'          => ['name' => 'endDate', 'type' => self::TYPE_DATE],
		'TEMPS_DUREE'           => ['name' => 'duration', 'type' => self::TYPE_FLOAT],
		'TEMPSEXP_RECUPERATION' => ['name' => 'recovering', 'type' => self::TYPE_BOOLEAN],
		'TEMPSEXP_DESCRIPTION'  => ['name' => 'description', 'type' => self::TYPE_STRING],
		'TEMPSEXP_MODECALCUL'   => ['name' => 'calculationMethod', 'type' => self::TYPE_BOOLEAN],
		'TEMPSEXP_COUT'         => ['name' => 'cost', 'type' => self::TYPE_FLOAT],
		'TEMPSEXP_TARIF'        => ['name' => 'priceExcludingTax', 'type' => self::TYPE_FLOAT],
		'TEMPSEXP_MANAGE'       => ['name' => 'processed', 'type' => self::TYPE_BOOLEAN],
//            utilisé par l'api timesreports/:id
		'rules'                 => ['name' => 'rules', self::TYPE_ARRAY],
	];

	const WARNING_OUTSIDE_DELIVERY_DATES = 'outsideDeliveryDates';
	const WARNING_WRONG_DELIVERY_MONTH = 'wrongDeliveryMonth';
	const WARNING_OUTSIDE_CONTRACT_DATES = 'outsideContractDates';
	const WARNING_NO_DELIVERIES_ON_PROJECT = 'noDeliveryOnProject';
	const WARNING_WRONG_ABSENCES = 'wrongAbsences';
	const WARNING_TOO_MUCH_ABSENCES = 'moreThanAbsenceAccountAcquired';
	const WARNING_TOO_MUCH_DAYS = 'moreThanNumberOfWorkingDays';

	/**#@+
	 * @var string TYPEH_TYPEACTIVITY
	 */
	const
		ACTIVITY_REGULAR_PRODUCTION = 0,
		ACTIVITY_REGULAR_ABSENCE = 1,
		ACTIVITY_REGULAR_INTERNAL = 2,
		ACTIVITY_EXCEPTIONAL_TIME = 3,
		ACTIVITY_EXCEPTIONAL_CALENDAR = 4;
	/**#@-*/

	/**#@+
	 * @var string TEMPS_TYPE
	 */
	const
		TYPE_TIME_REGULAR = 'regular',
		TYPE_TIME_EXCEPTIONAL = 'exceptional';
	/**#@-*/

	/**
	 * callback function after a key/field is updated
	 * @param $updateKey
	 * @deprecated
	 */
	protected function onUpdatedData($updateKey, $value = null, $oldvalue = null){
		switch($updateKey){
			case 'category':
				if(!is_string($this->category)) $this->category = [
					self::TYPE_TIME_REGULAR,
					self::TYPE_TIME_EXCEPTIONAL,
					self::TYPE_TIME_EXCEPTIONAL,
					self::TYPE_TIME_EXCEPTIONAL,
				][$value];
				break;
		}
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	/**
	 * @return bool
	 */
	public function isAbsence() {
		return $this->workUnitType->activityType == WorkUnitType::ACTIVITY_ABSENCE;
	}

	/**
	 * @return bool
	 */
	public function isInternal() {
		return $this->workUnitType->activityType == WorkUnitType::ACTIVITY_INTERNAL;
	}

	public function initRelationships(){
		$this->setRelationships('ID_LISTETEMPS', 'timesReport', TimesReport::class);
		$this->setRelationships('ID_PROJET', 'project', Project::class);
		$this->setRelationships('ID_MISSIONPROJET', 'delivery', Delivery::class);
		$this->setRelationships('ID_LOT', 'batch', Batch::class); //usilisé par GET times-reports/:id
//		$this->setGroupedRelationships('TEMPSEXP_REGLES', 'rules'); //s'il s'agit d'un temps exceptionnel
	}
}
