<?php
/**
 * time.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

/**
 * class Advantage
 * @property int id
 * @property string category
 * @property int row
 * @property string startDate
 * @property string endDate
 * @property float duration
 * @property bool recovering
 * @property string description
 * @property bool calculationMethod
 * @property float cost
 * @property float priceExcludingTax
 * @property bool processed
 * @property array rules
 * @property WorkUnitType workUnitType
 * @property Project project
 * @property Batch batch
 * @property TimesReport timesReport
 * @property Delivery delivery
 * @package BoondManager\Models
 */
class TimeExceptional extends Time {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'time';

	const MAPPER = [
		'ID_TEMPSEXCEPTION'     => ['name' => 'id', 'type' => self::TYPE_INT],
		'TEMPS_TYPE'            => ['name' => 'category', 'type' => self::TYPE_STRING],
		'workUnitType'          => ['name' => 'workUnitType', 'type' => self::TYPE_OBJECT],// groupment of TAB_TYPEHEURE.TYPEH_REF, TAB_TYPEHEURE.TYPEH_TYPEACTIVITE and TAB_TYPEHEURE.TYPEH_NAME
		'ID_LIGNETEMPS'         => ['name' => 'row', 'type' => self::TYPE_INT],
		'TEMPSEXP_DEBUT'        => ['name' => 'startDate', 'type' => self::TYPE_DATETIME],
		'TEMPSEXP_FIN'          => ['name' => 'endDate', 'type' => self::TYPE_DATETIME],
		'TEMPS_DUREE'           => ['name' => 'duration', 'type' => self::TYPE_FLOAT],
		'TEMPSEXP_RECUPERATION' => ['name' => 'recovering', 'type' => self::TYPE_BOOLEAN],
		'TEMPSEXP_DESCRIPTION'  => ['name' => 'description', 'type' => self::TYPE_STRING],
		'TEMPSEXP_MODECALCUL'   => ['name' => 'calculationMethod', 'type' => self::TYPE_BOOLEAN],
		'TEMPSEXP_COUT'         => ['name' => 'cost', 'type' => self::TYPE_FLOAT],
		'TEMPSEXP_TARIF'        => ['name' => 'priceExcludingTax', 'type' => self::TYPE_FLOAT],
		'TEMPSEXP_MANAGE'       => ['name' => 'processed', 'type' => self::TYPE_BOOLEAN],
		'rules'                 => ['name' => 'rules', self::TYPE_ARRAY],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}
}
