<?php
/**
 * timesreport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractActivityReport;
use BoondManager\Lib\Models\HasAgencyTrait;
use Wish\Models\Model;

/**
 * class TimesReport
 * @property int $id
 * @property string $state
 * @property string $term
 * @property string informationComments
 * @property bool closed
 * @property Employee resource
 * @property Agency agency
 * @property Validation[] $validations
 * @property Time[] regularTimes
 * @property Model[] exceptionalTimes
 * @property ExpensesReport expensesReport
 * @property Proof[] files
 * @property Project[] projects
 * @property Order[] orders
 * @package BoondManager\Models
 */
class TimesReport extends AbstractActivityReport{
	use HasAgencyTrait;

	/**
	 * @var string
	 */
	protected static $_jsonType = 'timesreport';

	const ERROR_ROW_INCONSISTENCY = 3900;
	const ERROR_ENDDATE_STARTDATE = 3901;
	const ERROR_DELIVERY_INCOMPATIBLE_WITH_WORKUNITTYPE = 3902;
	const ERROR_BATCH_INCOMPATIBLE_WITH_WORKUNITTYPE = 3903;
	const ERROR_PROJECT_INCOMPATIBLE_WITH_WORKUNITTYPE = 3904;
	const ERROR_DELIVERY_ID_NOT_ALLOWED = 3905;
	const ERROR_BATCH_ID_NOT_ALLOWED = 3906;
	const ERROR_PROJECT_ID_NOT_ALLOWED = 3907;

	const MAPPER = [
		'ID_LISTETEMPS'           => ['name' => 'id', 'type' => self::TYPE_STRING],
		'LISTETEMPS_DATE'         => ['name' => 'term', 'type' => self::TYPE_TERM],
		'LISTETEMPS_ETAT'         => ['name' => 'state', 'type' => self::TYPE_STRING, 'mapper' => Validation::STATE_MAPPER],
		//~ Champs utilisés par GET times-reports/:id
		'LISTETEMPS_COMMENTAIRES' => ['name' => 'informationComments', 'type' => self::TYPE_STRING],
		'LISTETEMPS_CLOTURE'      => ['name' => 'closed', 'type' => self::TYPE_BOOLEAN],
		'regularTimes'            => ['name' => 'regularTimes', 'type' => self::TYPE_ARRAY],
		'exceptionalTimes'        => ['name' => 'exceptionalTimes', 'type' => self::TYPE_ARRAY],
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function initRelationships(){
		parent::initRelationships();
		$this->setGroupedRelationships('files', 'files');
		$this->setRelationships('expensesReport', 'expensesReport', ExpensesReport::class);
		$this->setGroupedRelationships('orders', 'orders');
		$this->setGroupedRelationships('projects', 'projects');
	}

	public function getStartPeriod(){
		return $this->term;
	}

	public function getEndPeriod(){
		$date = explode('-', $this->term);
		return date('Y-m-d', mktime(0,0,0, $date[1]+1, 0, $date[0]));
	}

	public function getValidationWorkflow(){
		return $this->resource->timesReportsWorkflow ? $this->resource->timesReportsWorkflow : $this->agency->timesReportsWorkflow;
	}

	/**
	 * @return float|int
	 */
	public function getTotalRegularTime() {
		$sum = 0;
		foreach($this->regularTimes as $time) {
			if( ! $time->isAbsence() && !$time->isInternal() ) $sum += $time->duration;
		}
		return $sum;
	}

	/**
	 * @return float|int
	 */
	public function getTotalAbsences() {
		$sum = 0;
		foreach($this->regularTimes as $time) {
			if( $time->isAbsence() ) $sum += $time->duration;
		}
		return $sum;
	}

	/**
	 * @return float|int
	 */
	public function getTotalInternal() {
		$sum = 0;
		foreach($this->regularTimes as $time) {
			if( $time->isInternal() ) $sum += $time->duration;
		}
		return $sum;
	}

	/**
	 * @return mixed
	 */
	public function getHrManagerID() {
		return $this->resource->getHrManagerID();
	}

	/**
	 * @return mixed
	 */
	public function getManagerID() {
		return $this->resource->getManagerID();
	}

	/**
	 * @return mixed
	 */
	public function getPoleID() {
		return $this->resource->getPoleID();
	}
}
