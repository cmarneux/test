<?php
/**
 * tool.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class Tool
 * @property string tool
 * @property string level
 * @package BoondManager\Models
 */
class Tool extends ModelJSON {
	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'tool' => ['name' => 'tool', 'type' => self::TYPE_STRING],
			'level' => ['name' => 'level', 'type' => self::TYPE_STRING]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
