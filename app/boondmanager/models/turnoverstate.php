<?php
/**
 * TurnoverState.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class TurnoverState
 * @property int id,
 * @property float turnoverExcludingTax
 * @package BoondManager\Models\MySQL\RowObject
 */
class TurnoverState extends ModelJSON {

	const MAPPER = [
		'id'                   => ['name' => 'id', 'type' => self::TYPE_INT],
		'turnoverExcludingTax' => ['name' => 'turnoverExcludingTax', 'type' => self::TYPE_FLOAT]
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}
}
