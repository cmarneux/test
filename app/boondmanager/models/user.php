<?php
/**
 * user.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AccountInterface;
use BoondManager\Services\BM;
use BoondManager\Services\Validations;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;

/**
 * Class User
 * @property int id
 * @property string login
 * @property string account
 * @property int state
 * @property string workUnitRate
 * @property mixed allowExceptionalTimes
 * @property string homePage connection URL
 * @property boolean securityAlert
 * @property boolean securityCookie
 * @property Employee resource
 * @property Role role
 * @property boolean defaultSearch
 * @property boolean narrowPerimeter used for the search perimiter. If true, search must match all filter
 * @property string lastConnexion datetime
 * @property string language
 * @property string client
 * @property boolean connexionType
 * @property string startTimesExpenses date
 * @property string $password
 * @property AccountModules modules
 * @property AccountRights rights
 * @property UserConfig config
 * @property array navigationBar
 * @property string firstName
 * @property string lastName
 * @property mixed USER_ALLOWABSENCESTYPEHREF
 * @property array $expensesReportsWorkflow
 * @property array $timesReportsWorkflow
 * @property array $absencesReportsWorkflow
 * @property UserAdvancedApps advancedApps
 *
 * @package BoondManager\Models
 *
 * @TODO: ajouter un champ CONFIG pour documenter le mysql\user::getObject
 */
class User extends ModelJSONAPI implements AccountInterface {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'account';

	const MAPPER = [
		'ID_USER'                    => ['name' => 'id', 'type' => self::TYPE_STRING],
		'USER_ALLOWTEMPSEXCEPTION'   => ['name' => 'allowExceptionalTimes'], //~ Booléen où string withRecovering, withoutRecovering
		'USER_CONNECTIONTYPE'        => ['name' => 'connexionType', 'type' => self::TYPE_BOOLEAN],
		'USER_DEFAULTSEARCH'         => ['name' => 'defaultSearch', 'type' => self::TYPE_BOOLEAN],
		'USER_HOMEPAGE'              => ['name' => 'homePage', 'type' => self::TYPE_STRING],
		'USER_JOINCATEGORY'          => ['name' => 'narrowPerimeter', 'type' => self::TYPE_BOOLEAN],
		'USER_LANGUE'                => ['name' => 'language', 'type' => self::TYPE_STRING],
		'USER_LASTCONNEXION'         => ['name' => 'lastConnexion', 'type' => self::TYPE_DATETIME],
		'USER_LOGIN'                 => ['name' => 'login', 'type' => self::TYPE_STRING],
		'USER_PWD'                   => ['name' => 'password', 'type' => self::TYPE_STRING],
		'USER_TAUXHORAIRE'           => ['name' => 'workUnitRate', 'type' => self::TYPE_FLOAT],
		'USER_TYPE'                  => ['name' => 'account', 'type' => self::TYPE_STRING],
		'USER_ABONNEMENT'            => ['name' => 'state', 'type' => self::TYPE_STRING],
		'USER_SECURITYCOOKIE'        => ['name' => 'securityCookie', 'type' => self::TYPE_BOOLEAN],
		'USER_SECURITYALERT'         => ['name' => 'securityAlert', 'type' => self::TYPE_BOOLEAN],
		'USER_TPSFRSSTART'           => ['name' => 'startTimesExpenses', 'type' => self::TYPE_DATE],
		'USER_ALLOWABSENCESTYPEHREF' => ['name' => 'USER_ALLOWABSENCESTYPEHREF', 'type' => self::TYPE_ARRAY, 'doubleSerializedArray' => true],
		'USER_VALIDATIONFRAIS'       => ['name' => 'expensesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'USER_VALIDATIONABSENCES'    => ['name' => 'absencesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'USER_VALIDATIONTEMPS'       => ['name' => 'timesReportsWorkflow', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializeValidations', 'unserializeCallback' => 'unserializeValidations'],
		'config'                     => ['name' => 'config', 'type' => self::TYPE_OBJECT],
		'timesReportsWorkflow'       => ['name' => 'timesReportsWorkflow', 'type' => self::TYPE_ARRAY],
		'expensesReportsWorkflow'    => ['name' => 'expensesReportsWorkflow', 'type' => self::TYPE_ARRAY],
		'absencesReportsWorkflow'    => ['name' => 'absencesReportsWorkflow', 'type' => self::TYPE_ARRAY],
		'modules'                    => ['name' => 'modules', 'type' => self::TYPE_OBJECT],
		'navigationBar'              => ['name' => 'navigationBar', 'type' => self::TYPE_ARRAY],
		'advancedApps'               => ['name' => 'advancedApps', 'type' => self::TYPE_OBJECT],
		'rights'                     => ['name' => 'rights', 'type' => self::TYPE_OBJECT],
		'USER_PRENOM'                => ['name' => 'firstName', 'type' => self::TYPE_STRING], //support Only
		'USER_NOM'                   => ['name' => 'lastName', 'type' => self::TYPE_STRING]    //support Only
	];

	const REF_PREFIX = 'USER';

	const USER_ALLOWTEMPSEXCEPTION_NO = false;
	const USER_ALLOWTEMPSEXCEPTION_NO_BDD = 0;
	const USER_ALLOWTEMPSEXCEPTION_WITH_RECOVERING = 'withRecovering';
	const USER_ALLOWTEMPSEXCEPTION_WITH_RECOVERING_BDD = 1;
	const USER_ALLOWTEMPSEXCEPTION_WITHOUT_RECOVERING = 'withoutRecovering';
	const USER_ALLOWTEMPSEXCEPTION_WITHOUT_RECOVERING_BDD = 2;

	/**
	 * callback function after a key/field is updated
	 * @param $updateKey
	 */
	protected function onUpdatedData($updateKey, $value = null, $oldvalue = null){
		switch($updateKey){
			case 'USER_ALLOWTEMPSEXCEPTION':
				if(!is_string($this->allowExceptionalTimes)) $this->allowExceptionalTimes = [
					self::USER_ALLOWTEMPSEXCEPTION_NO,
					self::USER_ALLOWTEMPSEXCEPTION_WITH_RECOVERING,
					self::USER_ALLOWTEMPSEXCEPTION_WITHOUT_RECOVERING,
				][$value];
				break;
		}
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	protected function initRelationships()
	{
		$this->setRelationships('ID_PROFIL', 'resource', Employee::class);
		$this->setRelationships('ID_ROLE', 'role', Role::class);
		$this->setGroupedRelationships('apps', 'apps');
	}

	/**
	 * @return bool
	 */
	public function isAdministrator(){
		return $this->account == BM::USER_TYPE_ADMINISTRATOR;
	}

	/**
	 * @return bool
	 */
	public function isRoot(){
		return $this->account == BM::USER_TYPE_ROOT;
	}

	/**
	 * @return bool true if the user type is an employee
	 */
	public function isEmployee(){
		return $this->account == BM::USER_TYPE_RESOURCE;
	}

	/**
	 * @return bool true if the user type is from support
	 */
	public function isSupport(){
		return $this->account == BM::USER_TYPE_SUPPORT;
	}

	public function isManager()
	{
		return $this->account == BM::USER_TYPE_MANAGER;
	}

	public static function serializeValidations($data) {
		return Validations::serializeValidationWorkflow($data);
	}

	public static function unserializeValidations($string) {
		return Validations::unserializeValidationWorkflow( $string );
	}
}
