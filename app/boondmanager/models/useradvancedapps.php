<?php
/**
 * useradvancedapps.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class UserAdvancedApps
 * @package BoondManager\Models
 * @property int templates
 * @property int certification
 * @property int calendar
 * @property int mail
 * @property int viewer
 * @property int emailing
 */
class UserAdvancedApps extends ModelJSON{
	public static function getPublicFieldsDefinition()
	{
		return [
			'templates'     => ['name' => 'templates', 'type' => self::TYPE_INT],
			'certification' => ['name' => 'certification', 'type' => self::TYPE_INT],
			'calendar'      => ['name' => 'calendar', 'type' => self::TYPE_INT],
			'mail'          => ['name' => 'mail', 'type' => self::TYPE_INT],
			'viewer'        => ['name' => 'viewer', 'type' => self::TYPE_INT],
			'emailing'      => ['name' => 'emailing', 'type' => self::TYPE_INT]
		];
	}
}
