<?php
/**
 * validation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use BoondManager\Lib\Models\AbstractActivityReport;
use Wish\Models\ModelJSONAPI;

/**
 * Class Validation
 * @package BoondManager\Models\MySQL\RowObject
 * @property int $state
 * @property string $date
 * @property int $id
 * @property string typeOf
 * @property string reason
 * @property string reasonTypeOf
 * @property Account $realValidator
 * @property Account expectedValidator
 * @property ExpensesReport|TimesReport|AbsencesReport dependsOn
 */
class Validation extends ModelJSONAPI {

	const MAPPER = [
		'ID_VALIDATION' => ['name' => 'id', 'type' => self::TYPE_STRING],
		'VAL_ETAT'      => ['name' => 'state', 'type' => self::TYPE_STRING, 'mapper' => self::STATE_MAPPER],
		'VAL_TYPE'      => ['name' => 'typeOf', 'type' => self::TYPE_STRING, 'mapper' => self::TYPE_MAPPER],
		'VAL_DATE'      => ['name' => 'date', 'type' => self::TYPE_DATE],
		//~ Utilisé par GET times-reports/:id
		'VAL_MOTIF'     => ['name' => 'reason', 'type' => self::TYPE_STRING],
		'reasonTypeOf'  => ['name' => 'reasonTypeOf', 'type' => self::TYPE_STRING]
	];

	/**
	 * @var string
	 */
	protected static $_jsonType = 'validation';

	/**#@+
	 * Possible states of a validation
	 */
	const VALIDATION_NONE = 'savedAndNoValidation';
	const VALIDATION_REJECTED = 'rejected';
	const VALIDATION_VALIDATED = 'validated';
	const VALIDATION_WAITING = 'waitingForValidation';
	/**#@-*/

	const STATE_MAPPER = [
		0 => self::VALIDATION_NONE,
		1 => self::VALIDATION_VALIDATED,
		2 => self::VALIDATION_WAITING,
		3 => self::VALIDATION_REJECTED
	];

	/**#@+
	 * Possible types of a validation
	 */
	const TYPE_TIMESREPORT = 'timesreport';
	const TYPE_TIMESREPORT_BDD = 0;
	const TYPE_EXPENSESREPORT = 'expensesreport';
	const TYPE_EXPENSESREPORT_BDD = 1;
	const TYPE_ABSENCESREPORT = 'absencesreport';
	const TYPE_ABSENCESREPORT_BDD = 2;
	/**#@-*/

	const REASON_TYPE_CORRECTION_FOR_PREVIOUS_VALIDATOR = 'correctionForPreviousValidator';
	const REASON_TYPE_CORRECTION_FOR_ALL_VALIDATOR = 'correctionForAllValidator';
	const REASON_TYPE_REFUSED = 'refused';

	const TYPE_MAPPER = [
		self::TYPE_TIMESREPORT_BDD => self::TYPE_TIMESREPORT,
		self::TYPE_EXPENSESREPORT_BDD => self::TYPE_EXPENSESREPORT,
		self::TYPE_ABSENCESREPORT_BDD => self::TYPE_ABSENCESREPORT
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return self::MAPPER;
	}

	/**
	 *
	 */
	public function initRelationships(){
		//~ Utilisé par GET times-reports/:id
		$this->setRelationships('ID_VALIDATEUR', 'realValidator', Employee::class);
		$this->setRelationships('ID_PROFIL', 'expectedValidator', Employee::class);
		$this->setRelationships('dependsOn', 'dependsOn', AbstractActivityReport::class);
	}
}
