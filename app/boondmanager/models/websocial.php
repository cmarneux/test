<?php
/**
 * socialnetwork.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSON;

/**
 * Class WebSocial
 * @package BoondManager\Models
 */
class WebSocial extends ModelJSON {
	/**#@+
	 * @var int SOCIALNETWORK
	 */
	const
		WEBSOCIAL_VIADEO = 0,
		WEBSOCIAL_LINKEDIN = 1,
		WEBSOCIAL_FACEBOOK = 2,
		WEBSOCIAL_TWITTER = 3;
	/**#@-*/

	/**#@+
	 * @var int WEBSOCIAL
	 */
	const
		WEBSOCIAL_TYPE_CANDIDATE = 0,
		WEBSOCIAL_TYPE_RESOURCE = 1,
		WEBSOCIAL_TYPE_CONTACT_CRM = 2,
		WEBSOCIAL_TYPE_COMPANY_CRM = 3;
	/**#@-*/

	const NETWORK_MAPPER = [
		self::WEBSOCIAL_VIADEO => 'viadeo',
		self::WEBSOCIAL_LINKEDIN => 'linkedin',
		self::WEBSOCIAL_FACEBOOK => 'facebook',
		self::WEBSOCIAL_TWITTER => 'twitter'
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [
			'WEBSOCIAL_NETWORK' => ['name' => 'network', self::TYPE_STRING, 'mapper' => self::NETWORK_MAPPER],
			'WEBSOCIAL_URL'     => ['name' => 'url', self::TYPE_STRING]
		];
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->toArray();
	}
}
