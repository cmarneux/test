<?php
/**
 * workflow.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Models;

/**
 * class Workflow
 */
class Workflow {
    const WORKFLOW_VALIDATOR = [
        0 => "N",
        -1 => "N+1",
        -2 => "N+2",
        -3 => "N+3",
    ];
}
