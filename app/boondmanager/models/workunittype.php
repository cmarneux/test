<?php
/**
 * workunittype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Models;

use Wish\Models\ModelJSONAPI;

/**
 * class WorkUnitType
 * @property string id
 * @property string name
 * @property int reference
 * @property string activityType
 * @property bool state
 * @property bool warning
 * @package BoondManager\Models
 */
class WorkUnitType extends ModelJSONAPI {

	/**
	 * @var string
	 */
	protected static $_jsonType = 'workunittype';

	const MAPPER = [
		'ID_TYPEHEURE'       => ['name' => 'id', 'type' => self::TYPE_STRING],
		'TYPEH_REF'          => ['name' => 'reference', 'type' => self::TYPE_INT],
		'TYPEH_TYPEACTIVITE' => ['name' => 'activityType', 'type' => self::TYPE_STRING, 'mapper' => self::ACTIVITY_MAPPER],
		'TYPEH_NAME'         => ['name' => 'name', 'type' => self::TYPE_STRING],
		'state'              => ['name' => 'state', 'type' => self::TYPE_BOOLEAN],
		'warning'            => ['name' => 'warning', 'type' => self::TYPE_BOOLEAN]
	];

	/**#@+
	 * @var int TAB_TYPEHEURE.TYPEH_TYPEACTIVITE
	 */
	const
		ACTIVITY_PRODUCTION = 'production',
		ACTIVITY_ABSENCE = 'absence',
		ACTIVITY_INTERNAL = 'internal',
		ACTIVITY_EXCEPTIONALTIME = 'exceptionalTime',
		ACTIVITY_EXCEPTIONALCALENDAR = 'exceptionalCalendar',
		ACTIVITY_ABSENCE_BDD = -1,
		ACTIVITY_INTERNAL_BDD = -2;
	/**#@-*/

	const ACTIVITY_MAPPER = [
		0 => self::ACTIVITY_PRODUCTION,
		1 => self::ACTIVITY_ABSENCE,
		2 => self::ACTIVITY_INTERNAL,
		3 => self::ACTIVITY_EXCEPTIONALTIME,
		4 => self::ACTIVITY_EXCEPTIONALCALENDAR
	];

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition() {
		return self::MAPPER;
	}

	public function isAbsenceOrInternal(){
		return $this->activityType == self::ACTIVITY_ABSENCE || $this->activityType == self::ACTIVITY_INTERNAL;
	}

	public function isAbsence() {
		return $this->activityType == self::ACTIVITY_ABSENCE;
	}
}
