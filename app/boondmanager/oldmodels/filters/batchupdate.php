<?php
/**
 * resources.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Resources
 * @package BoondManager\Models\Filters\Batch
 * @property-read InputValue updateField
 * @property-read InputValue updateValue
 * @property-read InputValue markAsDelete
 * @property-read InputValue addFlag
 * @property-read InputValue removeFlag
 */
class BatchUpdate extends \Wish\Filters\AbstractFilters{
	public function __construct()
	{
		parent::__construct();

		$field = new InputValue('updateField');
		$this->addInput($field);

		$field = new InputValue('updateValue');
		$this->addInput($field);

		$delete = new InputBoolean('markAsDelete');
		$this->addInput($delete);

		$flag = new InputValue('addFlag');
		$this->addInput($flag);

		$flag = new InputValue('removeFlag');
		$this->addInput($flag);
	}
}
