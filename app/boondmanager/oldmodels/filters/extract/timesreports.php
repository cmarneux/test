<?php
/**
 * timesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Extract;

use Wish\Filters\Inputs\InputBoolean;
use BoondManager\Lib\Filters\Extract;

/**
 * Class TimesReports
 * @package BoondManager\Models\Filters\Extract
 * @property \Wish\Filters\Inputs\InputBoolean $fullExtract
 */
class TimesReports extends Extract{

	public function __construct()
	{
		parent::__construct();

		$type = new InputBoolean('fullExtract', 0);
		$this->addInput($type);
	}
}
