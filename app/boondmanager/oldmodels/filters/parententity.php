<?php
/**
 * parententity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\OldModels\Filters;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use BoondManager\Services\BM;

class ParentEntity extends AbstractFilters{

	public $category = null;
	public $parent = null;

	public function __construct($allowedCategory = [])
	{
		parent::__construct();

		$catMap = [
			BM::CATEGORY_CRM_COMPANY => 'company',
			BM::CATEGORY_OPPORTUNITY => 'opportunity',
			BM::CATEGORY_PROJECT     => 'project',
			BM::CATEGORY_ORDER       => 'order',
			BM::CATEGORY_PRODUCT     => 'product',
			BM::CATEGORY_PURCHASE    => 'purchase',
			BM::CATEGORY_ACTION      => 'action',
			BM::CATEGORY_RESOURCE    => 'resource',
			BM::CATEGORY_CANDIDATE   => 'candidate',
			BM::CATEGORY_POSITIONING => 'positioning',
			BM::CATEGORY_CRM_CONTACT => 'contact',
			BM::CATEGORY_BILLING     => 'bill',
			BM::CATEGORY_APP         => 'app'
		];

		if(!$allowedCategory) $allowedCategory = array_keys($catMap);

		foreach($allowedCategory as $cat){
			if(!array_key_exists($cat, $catMap)) continue;
			$name = $catMap[$cat];
			$input = new InputInt($name);
			$input->addFilter(FILTER_CALLBACK, function($value) use ($cat){
				if(!$this->parent && $value){
					$this->category = $cat;
					$this->parent   = $value;
					return $value;
				}else return false;
			});
			$this->addInput($input);
		}
	}

	public function isValid(){
		$test = parent::isValid();
		return $test;
	}

	public function getParent(){
		return $this->parent;
	}
}
