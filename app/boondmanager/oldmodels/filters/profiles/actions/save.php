<?php
/**
 * action.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles\Actions;

use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\MySQL\Where;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

/**
 * Class Action
 * @property InputValue $typeOf
 * @property InputDateTime $date
 * @property \Wish\Filters\Inputs\InputString $text
 * @property \Wish\Filters\Inputs\InputRelationship $dependsOn
 * @property InputValue $other
 * @property InputRelationship $mainManager
 * @package BoondManager\Models\Filters\Profiles
 */
class Save extends AbstractJsonAPI{

	/**
	 * Profil constructor.
	 */
	public function __construct($category = null){
		parent::__construct();

		if($category){
			$dictTypes = Services\Actions::getCustomTypes( $category , $incNotif = false);
		}else{
			$dictTypes = Services\Actions::getTypes( Services\Actions::getUserCategories() );
		}
		$type = new InputValue('typeOf');
        $type->addFilterInDict( $dictTypes );
		$type->setRequired(true);

		$date = new InputDateTime('date');

		$text = new InputString('text');

		$dependsOn = new InputRelationship('dependsOn');
		$dependsOn->setRequired(true);
		$dependsOn->addFilter(FILTER_CALLBACK, function($value){
			if(!$this->typeOf->isValid()) return false;
			switch($this->getCategory()){
				case BM::CATEGORY_CANDIDATE: return $this->dependsOn->existsInDB($value, 'TAB_PROFIL', new Where('ID_PROFIL=?')); break;
				case BM::CATEGORY_RESOURCE: return $this->dependsOn->existsInDB($value, 'TAB_PROFIL', new Where('ID_PROFIL=?')); break;
				case BM::CATEGORY_OPPORTUNITY: return $this->dependsOn->existsInDB($value, 'TAB_AO', new Where('ID_AO=?')); break;
				case BM::CATEGORY_CRM_CONTACT: return $this->dependsOn->existsInDB($value, 'TAB_CRMCONTACT', new Where('ID_CRMCONTACT=?')); break;
			}
			return false;
		});

		/* FIXME : NON DOCUMENTÉ (pour l'instant)
		$idOther = new InputInt('ID_OTHER');
		$idOther->addFilter(FILTER_CALLBACK, function($value){
			if(!$this->typeOf->isValid()) return false;
			if($this->getCategory() == BM::CATEGORY_CRM_CONTACT) return $value;
			else return false;
		});
		$this->addInput($idOther);
		*/

		$managerList = Tools::getFieldsToArray( Services\Actions::getAllGroupManagers(false), 'ID_PROFIL') ;
		$resp = new InputRelationship('mainManager');
		$resp->addFilterInArray($managerList);
		$resp->setRequired(true);

		$this->addInput([$type, $date, $text, $dependsOn, $resp]);
	}

	private $cat = null;
	public function getCategory(){
		if($this->typeOf->isValid()){
			if(!$this->cat){
				$this->cat = Services\Actions::getCategoryFromType($this->typeOf->getValue());
			}
			return $this->cat;
		}return null;
	}
}
