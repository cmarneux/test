<?php
/**
 * batch.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputInt;
use Wish\MySQL\Where;

/**
 * Class Batch
 * @package BoondManager\Models\Filters\Profiles
 * @property \Wish\Filters\Inputs\InputInt id
 */
class Batch extends AbstractFilters{

	/**
	 * Batch constructor.
	 * @param $name
	 */
	public function __construct($name){
		parent::__construct();

		$this->setName($name);

		$this->addInput([
			(new InputDB('id'))->addFilterExistsInDB('TAB_LOT', new Where('ID_LOT = ?')),
		]);
	}
}
