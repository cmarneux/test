<?php
/**
 * importresume.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles\Candidates;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputBoolean;

/**
 * Class ImportResume
 * @package BoondManager\Controllers\Profiles\Candidates
 * @property InputBoolean importResume
 */
class ImportResume extends AbstractFilters{
	public function __construct()
	{
		parent::__construct();

		$this->addInput( new InputBoolean('importResume') );
	}
}
