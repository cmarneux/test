<?php
/**
 * notation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles\Candidates;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDict;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Services\Dictionary;
use BoondManager\Models;

/**
 * Class Reference
 * @property \Wish\Filters\Inputs\InputString comments
 * @property \Wish\Filters\Inputs\InputValue id
 * @property InputDate date
 * @property InputMultiObjects notations
 * @package BoondManager\Models\Filters\Profiles\Resources
 */
class Notation extends AbstractFilters{

	protected $_objectClass = Models\Notation::class;

	/**
	 * Reference constructor.
	 */
	public function __construct(){
		parent::__construct();

		$id = new InputValue('id');
		$id->addFilter(FILTER_VALIDATE_INT);
		$this->addInput($id);

		$date = new InputDate('date');
		$this->addInput($date);

		$notes = new InputMultiObjects('notations', new Notes());
		$this->addInput($notes);

		$description = new InputString('comments');
		$this->addInput($description);
	}

	/**
	 * return notations as a string formated for the database
	 * @return string
	 */
	public function serializeNotations(){
		return implode('#', array_map(function($value){
			return $value['criteria'].'|'.$value['evaluation'];
		}, $this->notations->getValue()));
	}
}

/**
 * Class Notes
 * @package BoondManager\Models\Filters\Profiles\Candidates
 * @property string evaluation
 * @property int criteria
 */
class Notes extends AbstractFilters{

	public function __construct()
	{
		parent::__construct();

		$input = new InputDict('evaluation', 'specific.setting.evaluation');
		$input->setRequired(true);
		$this->addInput($input);

		$input = new InputDict('criteria', 'specific.setting.criteria');
		$input->setRequired(true);
		$this->addInput($input);
	}
}
