<?php
/**
 * delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputInt;
use Wish\MySQL\Where;

/**
 * Class Delivery
 * @package BoondManager\Models\Filters\Profiles
 * @property InputInt id
 */
class Delivery extends AbstractFilters{

	/**
	 * Delivery constructor.
	 * @param $name
	 */
	public function __construct($name){
		parent::__construct();

		$this->setName($name);

		$this->addInput([
			(new InputDB('id'))->addFilterExistsInDB('TAB_MISSIONPROJET', new Where('ID_MISSIONPROJET = ?')),
		]);
	}
}
