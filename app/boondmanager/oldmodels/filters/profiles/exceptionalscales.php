<?php
/**
 * exceptionalscales.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use BoondManager\OldModels\Filters\Profiles\Rules;

class ExceptionalScales extends AbstractFilters{
	public function __construct()
	{
		$reference = new InputInt('reference');

		$name = new InputString('name');

		$rules = new InputMultiObjects('rules', new Rules('rules'));

		$this->addInput([$reference, $name, $rules]);
	}
}
