<?php
/**
 * expensedetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use BoondManager\Lib\Filters\Inputs\Attributes\ExpenseTypeOnlyReference;
use BoondManager\Models\ExpenseDetail;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputEnum;
use Wish\Filters\Inputs\InputFloat;

/**
 * Class ExpenseDetails
 * @package BoondManager\Models\Filters\Profiles
 * @property InputDB id
 * @property ExpenseTypeOnlyReference $expenseType
 * @property InputEnum periodicity
 * @property InputFloat netAmount
 */
class ExpenseDetails extends AbstractFilters{

	protected $_objectClass = ExpenseDetail::class;

	public function __construct($name = 'expenseDetails')
	{
		parent::__construct($name);

		$id = new InputDB('id');
		//$id->addFilterExistsInDB('TAB_FRAISDETAILS', new Where('ID_FRAISDETAILS = ?'));

		$reference = new ExpenseTypeOnlyReference();
		$reference->setRequired(true);

		$periodicity = new InputEnum('periodicity');
		$periodicity->setAllowedValues([ExpenseDetail::PERIODICITY_DAILY, ExpenseDetail::PERIODICITY_MONTHLY]);

		$netAmount = new InputFloat('netAmount');

		$this->addInput([$id, $reference, $periodicity, $netAmount]);
	}

	/**
	 * @param $refs
	 * @return $this
	 */
	public function setExpenseReferenceAllowed($refs) {
		$this->expenseType->setAllowedExpenseTypes($refs);
		return $this;
	}
}
