<?php
/**
 * expensetype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;
use Wish\MySQL\Where;

/**
 * Class ExpenseTypeOnlyReference
 * @package BoondManager\Models\Filters\Profiles
 * @property InputDB reference
 * @property InputFloat taxRate
 * @property InputString name
 */
class ExpenseType extends AbstractFilters{
	public function __construct($name, $parentFilter)
	{
		parent::__construct();

		$this->setName($name);

		$this->addInput([
			(new InputDB('reference'))->addFilter(FILTER_CALLBACK, function($value) use ($parentFilter){
				$agency = $parentFilter->agency->filter()->getValue();
				return $this->reference->existsInDB($value, 'TAB_TYPEFRAIS', new Where('TYPEFRS_REF = ? AND ID_SOCIETE = '.intval($agency))) ? $value:false;
			})/*->setRequired(true) TODO remettre celà quand AbstractFilters sera au point */,
			new InputFloat('taxRate'),
			new InputString('name'),
		]);
	}
}
