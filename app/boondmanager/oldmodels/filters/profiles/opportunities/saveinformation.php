<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles\Opportunities;

use BoondManager\Lib\Filters\Inputs\Attributes\SourceOrOrigin;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\Profiles\Origin;
use BoondManager\Services;

/**
 * Class SaveInformation
 * @package BoondManager\Models\Filters\Profiles\Opportunities
 * @property InputString title
 * @property InputDateTime creationDate
 * @property InputDateTime updateDate
 * @property \Wish\Filters\Inputs\InputValue mode
 * @property \Wish\Filters\Inputs\InputValue typeOf
 * @property \Wish\Filters\Inputs\InputValue state
 * @property \Wish\Filters\Inputs\InputMultiValues place
 * @property \Wish\Filters\Inputs\InputDate startDate
 * @property InputBoolean startASAP
 * @property \Wish\Filters\Inputs\InputValue duration
 * @property Origin origin
 * @property \Wish\Filters\Inputs\InputMultiValues tools
 * @property \Wish\Filters\Inputs\InputMultiValues activityAreas
 * @property \Wish\Filters\Inputs\InputValue expertiseArea
 * @property \Wish\Filters\Inputs\InputValue currency
 * @property \Wish\Filters\Inputs\InputValue currencyAgency
 * @property InputFloat exchangeRate
 * @property \Wish\Filters\Inputs\InputFloat exchangeRateAgency
 * @property \Wish\Filters\Inputs\InputFloat weighting
 * @property InputFloat estimatesExcludingTax
 * @property InputFloat estimatedTurnoverExcludingTax
 * @property \Wish\Filters\Inputs\InputBoolean visibility
 * @property InputRelationship mainManager
 * @property \Wish\Filters\Inputs\InputRelationship agency
 * @property InputRelationship pole
 */
class SaveInformation extends AbstractJsonAPI
{
	const ERROR_RESTRAINED_MODE = 'the mode can\'t be change and the type must be restrained to these modes';
	/**
	 * Profil constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$title = new InputString('title');
		$title->setMinLength(1);
		$title->setRequired(true);
		$this->addInput($title);

		$creationDate=  new InputDateTime('creationDate');
		$updateDate=  new InputDateTime('creationDate');
		$this->addInput( [$creationDate, $updateDate] );

		$reference = new InputString('reference');
		$this->addInput($reference);

		$mode = new InputValue('mode');
		$mode->addFilterInArray([1, 2, 3, 4]);
		$this->addInput($mode);

		$type = new InputValue('typeOf');
		$type->addFilter(FILTER_CALLBACK, function($value){
			$dic = Dictionary::getDict('specific.setting.typeOf.project');
			foreach($dic as $item){
				if($item['id'] == $value){
					$this->mode->setValue($item['mode']);
					break;
				}
			}
			return $this->mode->isValid() ? $value : false;
		});

		$this->addInput($type);

		$state = new InputValue('state');
		$state->addFilterInDict( Dictionary::getDict('specific.setting.state.opportunity') );
		$this->addInput($state);

		$place = new InputMultiValues('place');
		$place->addFilterInDict( Dictionary::getDict('specific.setting.mobilityArea') );
		$this->addInput($place);

		$startDate =new InputDate('startDate');
		$this->addInput($startDate);

		$startasap = new InputBoolean('startASAP');
		$this->addInput($startasap);

		$duration = new InputValue('duration', 0);
		$duration->addFilterInDict( Dictionary::getDict('specific.setting.duration') );
		$this->addInput($duration);

		$origin = new SourceOrOrigin('origin');
		$this->addInput($origin);

		$tools = new InputMultiValues('tools', []);
		$tools->addFilterInDict( Dictionary::getDict('specific.setting.tool') );
		$this->addInput($tools);

		$activityAreas = new InputMultiValues('activityAreas');
		$activityAreas->addFilterInDict( Dictionary::getDict('specific.setting.activityArea') );
		$this->addInput($activityAreas);

		$expertise = new InputValue('expertiseArea', '');
		$expertise->addFilterInDict( Dictionary::getDict('specific.setting.expertiseArea') );
		$this->addInput($expertise);

		$currency = new InputValue('currency');
		$currency->addFilterInDict( Dictionary::getDict('specific.setting.currency'));
		$this->addInput($currency);

		$currency = new InputValue('currencyAgency');
		$currency->addFilterInDict( Dictionary::getDict('specific.setting.currency'));
		$this->addInput($currency);

		$exchangeRate = new InputFloat('exchangeRate');
		$this->addInput($exchangeRate);

		$exchangeRate = new InputFloat('exchangeRateAgency');
		$this->addInput($exchangeRate);

		$visibility = new InputBoolean('visibility');
		$this->addInput($visibility);

		$weighting = new InputFloat('weighting');
		$this->addInput($weighting);

		$estimatesExcludingTax = new InputFloat('estimatesExcludingTax');
		$this->addInput($estimatesExcludingTax);

		$estimatedTurnoverExcludingTax = new InputFloat('estimatedTurnoverExcludingTax');
		$this->addInput($estimatedTurnoverExcludingTax);

		$managerList = Tools::getFieldsToArray( Services\Contacts::getAllGroupManagers(), 'ID_PROFIL') ;

		$mainManager = new InputRelationship('mainManager');
		$mainManager->addFilterInArray( $managerList );
		$this->addInput($mainManager);

		$agency = new InputRelationship('agency');
		$this->addInput($agency);

		$pole = new InputRelationship('pole', 0);
		$this->addInput($pole);

	}

	public function restrictMode($mode){
		$this->mode->addFilterInArray([$mode], self::ERROR_RESTRAINED_MODE);
	}

	/**
	 * @param CurrentUser $currentUser
	 * @return SaveInformation
	 */
	public static function getUserFilter(CurrentUser $currentUser)
	{
		$filter = new self;
		if(!$currentUser->hasRight(BM::RIGHT_ASSIGNMENT, BM::MODULE_OPPORTUNITIES)){
			$filter->mainManager->addFilterInArray([$currentUser->getEmployeeId()]);
			$filter->agency->addFilterInArray( [$currentUser->getAgencyId()] );
			$filter->disableInput('pole');
		}
		if(!$currentUser->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_OPPORTUNITIES)){
			$filter->disableInput('creationDate');
		}
		if(!$currentUser->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)){
			$filter->disableInput('visibility');
		}
		return $filter;
	}
}
