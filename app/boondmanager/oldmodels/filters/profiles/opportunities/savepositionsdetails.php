<?php
/**
 * savepositionsdetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles\Opportunities;

use Wish\Filters\AbstractFilters;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;
use BoondManager\Services\CurrentUser;
use BoondManager\OldModels\Filters\Profiles\PositioningDetail;

/**
 * Class SavePositionsDetails
 * @property \Wish\Filters\Inputs\InputBoolean validateSimulation
 * @property InputMultiObjects additionalTurnoverAndCosts
 * @package BoondManager\Models\Filters\Profiles\Opportunities
 */
class SavePositionsDetails extends AbstractJsonAPI{
	public function __construct()
	{
		parent::__construct();

		$simulation = new InputBoolean('validateSimulation');
		$this->addInput($simulation);

		$details = new InputMultiObjects('additionalTurnoverAndCosts', new PositioningDetail());
		$this->addInput($details);
	}

	/**
	 * @return SavePositionsDetails
	 */
	public static function getUserFilter(CurrentUser $user){
		return new self;
	}
}
