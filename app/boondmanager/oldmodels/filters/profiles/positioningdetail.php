<?php
/**
 * positioningdetail.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputFloat;

/**
 * Class PositioningDetail
 * @package BoondManager\Models\Filters\Profiles
 * @property \Wish\Filters\Inputs\InputInt id
 * @property \Wish\Filters\Inputs\InputString title
 * @property \Wish\Filters\Inputs\InputFloat turnoverExcludingTax
 * @property InputFloat costsExcludingTax
 */
class PositioningDetail extends AbstractFilters{

	public function __construct(){
		parent::__construct();

		$this->addInput([
			(new InputInt('id')) //XXX : Tin : Ne devrait être autorisé que sur un positionnement déjà existant (sinon risque d'erreur BDD duplicate primary key ....)
				->addFilter(FILTER_VALIDATE_INT),
			(new InputString('title'))
				->setMaxLength(150),
			(new InputFloat('turnoverExcludingTax')),
			(new InputFloat('costsExcludingTax')),
		]);
	}
}
