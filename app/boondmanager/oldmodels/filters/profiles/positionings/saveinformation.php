<?php
/**
 * saveinformation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles\Positionings;

use Wish\Filters\AbstractFilters;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputRelationship;
use Wish\MySQL\Where;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\Profiles\PositioningDetail;
use BoondManager\Lib\RequestAccess;
use BoondManager\Services;
use BoondManager\Models;
use BoondManager\Models\Positioning;
use BoondManager\Databases\Local\Employee;
use BoondManager\Databases\Local\Product;
use BoondManager\APIs\Positionings\Specifications\HaveWriteAccessOnField;

class SaveInformation extends AbstractJsonAPI{

	/**
	 * Profil constructor.
	 */
	public function __construct(){
		parent::__construct();

		$this->addInput([
		(new InputDateTime('creationDate')),
		(new InputInt('state'))
			->addFilterInDict( Dictionary::getDict('specific.setting.state.positioning') ),
		(new InputDate('startDate')),
		(new InputDate('endDate')),
		(new InputFloat('averageDailyPriceExcludingTax')),
		(new InputFloat('averageDailyCost')),
		(new InputFloat('numberOfDaysInvoicedOrQuantity')),
		(new InputFloat('numberOfDaysFree')),
		(new InputString('informationComments')),
		(new InputBoolean('includedInSimulation')),
		(new InputMultiObjects('additionalTurnoverAndCosts', new PositioningDetail())),
		(new InputRelationship('project'))
			//~ ->addFilterExistsInDB('TAB_PROJET', new Where('ID_PROJET=?'))
			->addFilterExistingEntity()
			->setMode( InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE ),
		(new InputRelationship('opportunity'))
			//~ ->addFilterExistsInDB('TAB_AO', new Where('ID_AO=?'))
			->addFilterExistingEntity()
			->setMode( InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE ),
		(new InputRelationship('mainManager'))
			->addFilterInArray(Tools::getFieldsToArray( Services\Employees::getAllGroupManagers($only_active = false), 'ID_PROFIL'))
			->setMode( InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE ),
		(new InputRelationship('dependsOn'))
			->addFilterExistingEntity(['candidate', 'resource', 'product'])
			->setMode( InputValue::MODE_ERROR_ON_INCORRECT_VALUE ),
		]);
	}

	/**
	 * @param CurrentUser $currentUser
	 * @return SaveInformation
	 */
	public static function getUserFilter(CurrentUser $user = null, Positioning $positioning = null)
	{
		if(!$user) $user = CurrentUser::instance();
		$filter = new self();

		//~ On ajoute ce filtre que l'on soit en modification ou en création
		$filter->creationDate
			->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
			->addFilter(FILTER_CALLBACK, function ($value) use ($user) {
				return $user->hasRight(BM::RIGHT_EDIT_CREATION_DATE, BM::MODULE_OPPORTUNITIES) ? $value : false;
			}, null, 'User not allowed to modify this');

		$filter->endDate
			->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
			->addFilter(FILTER_CALLBACK, function ($value) use ($filter) {
				$opportunity = $filter->opportunity->filter()->getDbObject(); //~ XXX : Attention on récupère seulement un enregistrement d'une table de cette manière, ce ne sera pas pas forcément suffisant dans tous les cas
				if($opportunity->AO_TYPE == BM::PROJECT_TYPE_RECRUITMENT) return false;
				if($opportunity->AO_TYPE == BM::PROJECT_TYPE_PRODUCT){
					$product = $filter->dependsOn->filter()->getDbObject(); //~ XXX : Tin : Attention il faut être sûr qu'on ait un produit ici, mais cela est vérifié dans le filtre dependsOn
					if($product->PRODUIT_TYPE == BM::SUBSCRIPTION_TYPE_UNITARY) return false;
				}
				return $value;
			}, null, 'Cannot specify end date of a recruitment or unitary product positioning');

		$filter->averageDailyCost
			->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
			->addFilter(FILTER_CALLBACK, function ($value) use ($filter) {
				$opportunity = $filter->opportunity->filter()->getDbObject();
				if(in_array($opportunity->AO_TYPE,[BM::PROJECT_TYPE_RECRUITMENT, BM::PROJECT_TYPE_PRODUCT], true)) return false;
				return $value;
			}, null, 'Cannot specify averageDailyCost of a recruitment or product positioning');

		$cannotModifyIfRecruitment = function ($value) use ($filter) {
			$opportunity = $filter->opportunity->filter()->getDbObject();
			if($opportunity->AO_TYPE == BM::PROJECT_TYPE_RECRUITMENT) return false;
			return $value;
		};
		$filter->numberOfDaysInvoicedOrQuantity
			->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
			->addFilter(FILTER_CALLBACK, $cannotModifyIfRecruitment, null, 'Cannot specify numberOfDaysInvoicedOrQuantity of a recruitment positioning');

		$filter->numberOfDaysFree
			->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
			->addFilter(FILTER_CALLBACK, $cannotModifyIfRecruitment, null, 'Cannot specify numberOfDaysFree of a recruitment positioning');

		$filter->dependsOn
			->addFilter(FILTER_CALLBACK, function ($value) use ($filter) {
				$opportunity = $filter->opportunity->filter()->getDbObject();
				switch($filter->dependsOn->getType()){
					case 'candidate':case 'resource':
						$allowed = [BM::PROJECT_TYPE_RECRUITMENT, BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_TA];
						break;
					case 'product':
						$allowed = [BM::PROJECT_TYPE_PRODUCT];
						break;
					default: return $value;break; //Le filtre dependsOn n'a pas été passé donc il s'agit forcément de celui d'un positionnement existant avec ces 2 filtres compatibles
				}
				return in_array($opportunity->AO_TYPE, $allowed, true) ? $value : false;
			}, null, 'Entity not compatible with opportunity');

		$filter->project
			->addFilter(FILTER_CALLBACK, function ($value) use ($filter) {
				if($filter->project->getDbObject()->ID_AO != $filter->opportunity->filter()->getValue()) return false;
				return $value;
			}, null, 'Project and positioning must have same opportunity');

		if(!$positioning){ //~ We are creating a new positioning
			$filter->state
				->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
				->addFilter(FILTER_CALLBACK, function ($value) use ($filter) {
					return $value == Positioning::STATE_WON_KEY ?
						($filter->project->isDefined() && $filter->project->isValid() ? $value : false) :
						(!$filter->project->isDefined() || !$filter->project->isValid() ? $value : false); //~ cf. REST v6
				}, null, 'Incompatibility between positioning state and project');// Erreur spécifique aux positionnements

			$filter->dependsOn->setRequired(true);

			$filter->mainManager->setRequired(true);

			$filter->opportunity->setRequired(true);
		}
		if($positioning){ //~ We update an existing positioning
			$filter->state
				->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE)
				->addFilter(FILTER_CALLBACK, function ($value) use ($positioning) {
					return $positioning->POS_ETAT != Positioning::STATE_WON_KEY ? $value : $value == Positioning::STATE_WON_KEY; //~ cf. REST v6
				}, null, "Can't change value anymore") // Erreur générique non spécifique aux positionnements
				->addFilter(FILTER_CALLBACK, function ($value) use ($positioning, $filter) {
					return $positioning->POS_ETAT == Positioning::STATE_WON_KEY && $value != Positioning::STATE_WON_KEY ? ($filter->project->isValid() ? false : $value) : $value;  //~ cf. REST v6
				}, null, "Can't set to an other value if associated project exists"); // Erreur générique non spécifique aux positionnements

			$filter->opportunity
				->addFilter(FILTER_CALLBACK, function ($value) use ($positioning) {
					return $positioning->POS_ETAT == Positioning::STATE_WON_KEY && $value != $positioning->opportunity->ID_AO ? false : $value; //~ cf. REST v6
				}, null, "Can't set to an other value if positioning is already won"); // Erreur spécifique aux positionnements
		}
		//~ TODO : Tin : finish rights testing
		return $filter;
	}
}
