<?php
/**
 * project.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputDB;
use Wish\MySQL\Where;
use BoondManager\Models;

/**
 * Class Project
 * @package BoondManager\Models\Filters\Profiles
 * @property \Wish\Filters\Inputs\InputDB id
 */
class Project extends AbstractFilters{

	/**
	 * Project constructor.
	 * @param $name
	 */
	public function __construct($name = 'Project'){
		parent::__construct();

		$this->setName($name);

		$this->addInput([
			(new InputDB('id'))->addFilter(FILTER_CALLBACK, function ($value) {
				if($value < 0) return in_array($value, [Models\WorkUnitType::ACTIVITY_ABSENCE_BDD, Models\WorkUnitType::ACTIVITY_INTERNAL_BDD]) ? $value:false;
				else return $this->id->existsInDB($value, 'TAB_PROJET', new Where('ID_PROJET = ?'));
			}),
		]);
	}
}
