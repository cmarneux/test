<?php
/**
 * rules.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputString;

/**
 * Class Rules
 * @package BoondManager\Models\Filters\Profiles
 * @property InputString reference
 * @property InputString name
 * @property InputFloat priceExcludingTaxOrPriceRate
 * @property InputFloat grossCostOrSalaryRate
 */
class Rules extends AbstractFilters{

	public function __construct($name = null){
		parent::__construct();
		$this->setName($name);

		$reference = new InputString('reference');

		$name = new InputString('name');

		$price = new InputFloat('priceExcludingTaxOrPriceRate');

		$cost = new InputFloat('grossCostOrSalaryRate');

		$this->addInput([$reference, $name, $price, $cost]);
	}

	/**
	 * @return bool
	 */
	protected function postValidation()
	{
		$ok = true;
		if(!$this->priceExcludingTaxOrPriceRate->isDefined() && !$this->grossCostOrSalaryRate->isDefined()){
			$this->invalidate('one of priceExcludingTaxOrPriceRate or grossCostOrSalaryRate must be defined');
			$ok = false;
		}

		return $ok;
	}
}
