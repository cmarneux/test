<?php
/**
 * time.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Profiles;

use BoondManager\Lib\Filters\Inputs\Attributes\WorkUnitTypeOnlyReference;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDateTime;
use Wish\Filters\Inputs\InputDB;
use Wish\Filters\Inputs\InputFloat;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;
use Wish\MySQL\Where;
use BoondManager\Models;

/**
 * Class Time
 * @package BoondManager\Models\Filters\Profiles
 * @property InputDB id
 * @property InputDateTime startDate
 * @property InputDB row integer > 0 if modifying an existing expense else <= 0
 * @property InputInt currency
 * @property \Wish\Filters\Inputs\InputFloat exchangeRate
 * @property InputFloat numberOfKilometers
 * @property \Wish\Filters\Inputs\InputFloat amountInculdingTax,
 * @property InputBoolean reinvoiced
 * @property InputBoolean isKilometricExpense
 * @property ExpenseType expenseType
 * @property Delivery delivery
 * @property Project project
 * @property Batch batch
 */
class Time extends AbstractFilters{

	public function __construct($name, $parentFilter, $category) {

		parent::__construct();

		$this->setName($name);

		$this->addInput([
			(new InputDB('id'))
				->addFilter(FILTER_CALLBACK, function($value) use ($category) {
					if($category === Models\Time::TYPE_TIME_REGULAR){
						return $this->id->existsInDB($value, 'TAB_TEMPS', new Where('ID_TEMPS = ?'));
					}else{
						return $this->id->existsInDB($value, 'TAB_TEMPSEXCEPTION', new Where('ID_TEMPSEXCEPTION = ?'));
					}
				}),
			new InputDateTime('startDate'),
			new InputDateTime('endDate'),
			new InputFloat('duration'),
//			new InputString('category'), déjà défini par
			new InputString('description'),
			new InputBoolean('recovering'),
			(new InputDB('row'))->addFilter(FILTER_CALLBACK, function($value) use($category) {
				return ($category === Models\Time::TYPE_TIME_REGULAR) ? $value : false;
			})->addFilter(FILTER_CALLBACK, function($value){
				if($value <= 0) return $value;
				else return $this->row->existsInDB($value, 'TAB_LIGNETEMPS', new Where('ID_LIGNETEMPS = ?'));
			}),
			new WorkUnitTypeOnlyReference('workUnitType'),
			new Delivery('delivery'),
			new Project('project'),
			new Batch('batch'),
		]);
	}
}
