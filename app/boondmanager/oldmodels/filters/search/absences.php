<?php
/**
 * absences.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;


/**
 * Class Resources
 * @property-read \Wish\Filters\Inputs\InputValue $user
 * @property-read \Wish\Filters\Inputs\InputValue $resourceTypes
 * @property-read InputMultiValues excludeResourceTypes
 * @property-read \Wish\Filters\Inputs\InputValue $sort
 * @property-read \Wish\Filters\Inputs\InputValue $period
 * @property-read \Wish\Filters\Inputs\InputDate $startDate
 * @property-read \Wish\Filters\Inputs\InputDate $endDate
 * @package BoondManager\Models\Filters\Search
 */
class Absences extends AbstractSearch
{
	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_OVERLAPS = 'overlaps';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_RESOURCE_LASTNAME = 'resource.lastName';
	/**#@-*/

	/**
	 * Absences constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		$resourceTypes = new InputMultiValues('resourceTypes');
		$resourceTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.resource'));
		$this->addInput($resourceTypes);

		$excludeResourceTypes = new InputMultiValues('excludeResourceTypes');
		$excludeResourceTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.resource'));
		$this->addInput($excludeResourceTypes);

		$period = new InputValue('period', BM::INDIFFERENT);
		$period->addFilterInArray([self::PERIOD_OVERLAPS]);
		$this->addInput($period);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_RESOURCE_LASTNAME]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_ACTIVITIES_EXPENSES ));

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
