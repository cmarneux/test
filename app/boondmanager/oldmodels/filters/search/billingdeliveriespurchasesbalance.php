<?php
/**
 * billingdeliveriespurchasesbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\FlagsList;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class BillingDeliveriesPurchasesBalance
 */
class BillingDeliveriesPurchasesBalance extends AbstractSearch
{
	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_INPROGRESS = 'inProgress',
		PERIOD_ADDITIONALDATAINPROGRESS = 'additionalDataInProgress';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_ENDDATE = 'endDate',
		ORDERBY_ID = 'id',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_ORDER_REFERENCE = 'order.reference',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingDeliveriesPurchasesBalance constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		$projectStates = new InputMultiValues('projectStates');
		$projectStates->addFilterInDict(Dictionary::getDict('specific.setting.state.project'));
		$this->addInput($projectStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiValues('projectTypes');
		$projectTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
		$this->addInput($projectTypes);

		$period = new InputValue('period', BM::INDIFFERENT);
		$period->addFilterInArray([self::PERIOD_INPROGRESS, self::PERIOD_ADDITIONALDATAINPROGRESS]);
		$this->addInput($period);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_STARTDATE, self::ORDERBY_ENDDATE, self::ORDERBY_ID, self::ORDERBY_MAINMANAGER_LASTNAME,
		self::ORDERBY_PROJECT_COMPANY_NAME, self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_PROJECT_REFERENCE, self::ORDERBY_ORDER_REFERENCE]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_BILLING ));

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
