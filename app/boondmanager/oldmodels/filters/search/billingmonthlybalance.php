<?php
/**
 * billingmonthlybalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\FlagsList;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class BillingMonthlyBalance
 */
class BillingMonthlyBalance extends AbstractSearch
{
	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_NUMBEROFINVOICES = 'numberOfInvoices',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingMonthlyBalance constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		$orderPaymentMethods = new InputMultiValues('orderPaymentMethods');
		$orderPaymentMethods->addFilterInDict(Dictionary::getDict('specific.setting.paymentMethod'));
		$this->addInput($orderPaymentMethods);

		$orderStates = new InputMultiValues('orderStates');
		$orderStates->addFilterInDict(Dictionary::getDict('specific.setting.state.order'));
		$this->addInput($orderStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiValues('projectTypes');
		$projectTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
		$this->addInput($projectTypes);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_CREATIONDATE, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_PROJECT_COMPANY_NAME,
		self::ORDERBY_PROJECT_REFERENCE, self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_NUMBEROFINVOICES]);
		$this->addInput($sort);

		$startMonth = new InputValue('startMonth');
		$startMonth->addFilter(FILTER_CALLBACK, function($value){
			if(preg_match('/^([0-9]{4})-([0-9]{2})$/', $value, $matches))
				return date('Y-m-d', mktime(0,0,0,$matches[2], 1, $matches[1]));
			else
				return false;
		}, null, InputDate::ERROR_DATE_FORMAT);
		$startMonth->setRequired(true);
		$this->addInput($startMonth);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_BILLING ));

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
