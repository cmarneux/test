<?php
/**
 * billingprojectsbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\FlagsList;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

use BoondManager\Lib\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class BillingProjectsBalance
 */
class BillingProjectsBalance extends AbstractSearch
{
	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_CREATEDORWITHADDITIONALDATAINPROGRESS= 'createdOrWithAdditionalDataInProgress';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_NUMBEROFORDERS = 'numberOfOrders',
		ORDERBY_COMPANY_NAME= 'company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingProjectsBalance constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		$projectStates = new InputMultiValues('projectStates');
		$projectStates->addFilterInDict(Dictionary::getDict('specific.setting.state.project'));
		$this->addInput($projectStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiValues('projectTypes');
		$projectTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
		$this->addInput($projectTypes);

		$period = new InputValue('period', BM::INDIFFERENT);
		$period->addFilterInArray([self::PERIOD_CREATED, self::PERIOD_CREATEDORWITHADDITIONALDATAINPROGRESS]);
		$this->addInput($period);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_CREATIONDATE, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_COMPANY_NAME, self::ORDERBY_REFERENCE,
		self::ORDERBY_STATE, self::ORDERBY_NUMBEROFORDERS]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_BILLING ));

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
