<?php
/**
 * billingschedulesbalance.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\FlagsList;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class BillingSchedulesBalance
 */
class BillingSchedulesBalance extends AbstractSearch
{
	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_PAYMENTDATE = 'paymentDate';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_STATE = 'state',
		ORDERBY_NUMBEROFINVOICES = 'numberOfInvoices',
		ORDERBY_PROJECT_REFERENCE= 'project.reference',
		ORDERBY_TURNOVERINVOICEDINCLUDEDTAX = 'turnoverInvoicedExcludedTax',
		ORDERBY_TURNOVERTERMOFPAYMENTEXCLUDEDTAX = 'turnoverTermOfPaymentExcludedTax',
		ORDERBY_DELTAINVOICEDEXCLUDEDTAX = 'deltaOrderdExcludedTax',
		ORDERBY_PROJECT_COMPANY_NAME= 'project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * BillingSchedulesBalance constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		$orderPaymentMethods = new InputMultiValues('orderPaymentMethods');
		$orderPaymentMethods->addFilterInDict(Dictionary::getDict('specific.setting.paymentMethod'));
		$this->addInput($orderPaymentMethods);

		$orderStates = new InputMultiValues('orderStates');
		$orderStates->addFilterInDict(Dictionary::getDict('specific.setting.state.order'));
		$this->addInput($orderStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiValues('projectTypes');
		$projectTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
		$this->addInput($projectTypes);

		$period = new InputValue('period', BM::INDIFFERENT);
		$period->addFilterInArray([self::PERIOD_PAYMENTDATE]);
		$this->addInput($period);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_CREATIONDATE, self::ORDERBY_DELTAINVOICEDEXCLUDEDTAX,
		self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_PROJECT_COMPANY_NAME, self::ORDERBY_PROJECT_REFERENCE,
		self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_NUMBEROFINVOICES, self::ORDERBY_TURNOVERINVOICEDINCLUDEDTAX, self::ORDERBY_TURNOVERTERMOFPAYMENTEXCLUDEDTAX]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_BILLING ));

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
