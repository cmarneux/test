<?php
/**
 * deliveries.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Services\Dictionary;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Deliveries
 * build a filter for lists/deliveries (search & sort)
 * @package BoondManager\Models\Filters\Search
 */
class Deliveries extends AbstractSearch{

	/**#@+
	 * @var int PERIODS
	 */
	const
		PERIOD_STARTED = 'started', //~ Prestation Démarrée
		PERIOD_STOPPED = 'stopped', //~ Prestation Stoppée
		PERIOD_PROJECT_RUNNING_AND_SUM = 'projectRunningAndSum', //~ Prestation dont projet en Cours avec données additionnelles du projet
		PERIOD_PROJECT_RUNNING = 'projectRunning', //~ Prestation dont projet en Cours sans données additionnelles du projet
		PERIOD_RUNNING_AND_SUM = 'runningAndSum', //~ Prestation en Cours avec données additionnelles du projet
		PERIOD_RUNNING = 'running', //~ Prestation en Cours sans données additionnelles du projet
		PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE_AND_SUM = 'hasAdditionalDataOrPurchaseAndSum', //~ ?
		PERIOD_HAS_ADDITIONAL_DATA_OR_PURCHASE = 'hasAdditionalDataOrPurchase'; //~ ?
	/**#@-*/

	/**#@+
	 * @var int Cession type
	 */
	const
		TRANSFER_MASTER = 'master',
		TRANSFER_SLAVE = 'slave',
		TRANSFER_NONE = 'none',
		TRANSFER_NOTSLAVE = 'notSlave',
		TRANSFER_SLAVEINFO = 'slaveInfo',
		TRANSFER_NOTMASTER = 'notMaster';
	/**#@-*/

	/**#@+
	 * @var string order columns
	 */
	const
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_ENDDATE = 'endDate',
		ORDERBY_ID = 'id',
		ORDERBY_PROJECT_REFERENCE = 'project.reference',
		ORDERBY_COMPANY_NAME = 'company.name',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName',
		ORDERBY_GROUPMENT_ID = 'groupment.id'; //FIXME doc groupement -> groupment
	/**#@-*/

	/**
	 * Deliveries constructor.
	 * @param Perimeter|null $perimeter
	 * @param FlagsList|null $flags
	 */
	public function __construct(Perimeter $perimeter = null, FlagsList $flags = null)
	{
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		// Don't show additional data
		$sumAdditionalData = new InputBoolean('sumAdditionalData');
		$this->addInput($sumAdditionalData);

		//project state
		$projectStates = new InputMultiValues('projectStates');
		$projectStates->addFilterInDict(Dictionary::getDict('specific.setting.state.project'));
		$this->addInput($projectStates);

		//project type (opportunity type)
		$projectTypes = new InputMultiValues('projectTypes');
		$projectTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
		$this->addInput($projectTypes);

		//delivery type
		$deliveryStates = new InputMultiValues('deliveryStates');
		$deliveryStates->addFilterInDict(Dictionary::getDict('specific.setting.state.delivery'));
		$this->addInput($deliveryStates);

		//correlated orders
		$returnNbCorrelatedOrders = new InputBoolean('returnNbCorrelatedOrders');
		$this->addInput($returnNbCorrelatedOrders);

		//show deliveries of inactivity
		$showInactivity = new InputBoolean('showInactivity');
		$this->addInput($showInactivity);

		//show groups of deliveries
		$showGroupment = new InputBoolean('showGroupment');
		$this->addInput($showGroupment);

		// period
		$period = new InputValue('period', BM::INDIFFERENT);
		$period->addFilterInArray([
			self::PERIOD_RUNNING_AND_SUM, self::PERIOD_RUNNING, self::PERIOD_STARTED, self::PERIOD_STOPPED
		]);
		$this->addInput($period);

		// Cession
		$transferType = new InputValue('transferType', BM::INDIFFERENT);
		$transferType->addFilterInArray([
			self::TRANSFER_NONE, self::TRANSFER_MASTER, self::TRANSFER_NOTSLAVE, self::TRANSFER_NOTMASTER,
			self::TRANSFER_SLAVE, self::TRANSFER_SLAVEINFO
		]);
		$this->addInput($transferType);

		// Ordering column
		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([
			self::ORDERBY_STARTDATE, self::ORDERBY_ENDDATE, self::ORDERBY_ID, self::ORDERBY_PROJECT_REFERENCE,
			self::ORDERBY_COMPANY_NAME, self::ORDERBY_MAINMANAGER_LASTNAME, self::ORDERBY_GROUPMENT_ID
		]);
		$this->addInput($sort);

		// period start & end dates
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);

	}

	/*
	 * définition de quelques aides pour manipuler ce filtre
	 */

	/**
	 * send the period dates as an array [_startDate_, _endDate_]
	 *
	 * @return array
	 */
	public function getDatesPeriod(){
		return [$this->startDate->getValue(), $this->endDate->getValue()];
	}

	/**
	 * generate a filter adapted for the user
	 * @param CurrentUser $user
	 * @return Deliveries
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_PROJECTS ), $user->getSearchFlags());

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
