<?php
/**
 * documents.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\OldModels\Filters\Search;

use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputValue;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Databases\Local\File;

/**
 * Class Documents
 * @property-read \Wish\Filters\Inputs\InputInt $parent
 * @property-read InputValue $category
 * @property-read \Wish\Filters\Inputs\InputInt $type
 * @package BoondManager\Models\Filters\Search
 */
class Documents extends AbstractSearch{

	public function __construct()
	{
		parent::__construct();

		$parent = new InputInt('parent');
		$type = new InputInt('type');

		$category = new InputValue('category');
		// transformation du CV_CANDIDAT en CV si besoin
		$category->addFilter(FILTER_CALLBACK, function($value){
			if($value == File::TYPE_CANDIDATE_RESUME) return File::TYPE_RESOURCE_RESUME;
			else return $value;
		});
		$category->addFilterInArray([
			File::TYPE_RESOURCE_RESUME, File::TYPE_DOCUMENT, File::TYPE_DOWNLOADCENTER, File::TYPE_OTHER, File::TYPE_JUSTIFICATIVE
		]);
		$category->setRequired(true);

		$this->addInput([$parent, $type, $category]);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter()
    {
		return new self();
	}
}
