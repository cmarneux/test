<?php
/**
 * expenses.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\Expense;
use BoondManager\Models\Perimeter;

/**
 * Class Expenses
 * @property-read \Wish\Filters\Inputs\InputDate endDate
 * @property-read \Wish\Filters\Inputs\InputDate startDate
 * @property-read InputValue category
 * @property-read InputValue activityType
 * @property-read InputMultiValues resourceTypes
 * @property-read InputMultiValues excludeResourceTypes
 * @package BoondManager\Models\Filters\Search
 */
class Expenses extends AbstractSearch{

	/**#@+
	 * @var string TYPE
	 */
	const TYPE_ABSENCE = 'absence',
		  TYPE_INTERNAL = 'internal',
		  TYPE_PRODUCTION = 'production';
	/**#@-*/

	/**#@+
	 * @var string ORDERBY order columns
	 */
	const
		ORDERBY_CATEGORY = 'category',
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_LASTNAME = 'resource.lastName';
	/**#@-*/

	/**
	 * Expenses constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		$startDate = new InputDate('startDate');
		$this->addInput($startDate);

		$startDate = new InputDate('endDate');
		$this->addInput($startDate);

		$resourceType = new InputMultiValues('resourceTypes');
		$resourceType->addFilterInDict( Dictionary::getDict('specific.setting.typeOf.resource') );
		$this->addInput($resourceType);

		$excludeResourceType = new InputMultiValues('excludeResourceTypes');
		$excludeResourceType->addFilterInDict( Dictionary::getDict('specific.setting.typeOf.resource') );
		$this->addInput($excludeResourceType);

		$activityType = new InputValue('activityType');
		$activityType->addFilterInArray([self::TYPE_ABSENCE, self::TYPE_INTERNAL, self::TYPE_PRODUCTION]);
		$this->addInput($activityType);

		$category = new InputValue('category');
		$category->addFilterInArray([Expense::CATEGORY_ACTUAL, Expense::CATEGORY_FIXED]);
		$this->addInput($category);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_CATEGORY, self::ORDERBY_STARTDATE, self::ORDERBY_LASTNAME]);
		$this->addInput($sort);
	}

	/**
	 * @param CurrentUser $user
	 * @return Expenses
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null){
		if(!$user) $user = CurrentUser::instance();
		$filter = new self($user->getSearchPerimeter(BM::MODULE_ACTIVITIES_EXPENSES));
		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );
		return $filter;
	}
}
