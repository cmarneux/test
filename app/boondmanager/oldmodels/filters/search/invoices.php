<?php
/**
 * invoices.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\FlagsList;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;

/**
 * Class Resources
 * @property-read InputValue $user
 * @property-read InputValue $resourceTypes
 * @property-read \Wish\Filters\Inputs\InputMultiValues excludeResourceTypes
 * @property-read InputValue $sort
 * @property-read InputValue $period
 * @property-read InputDate $startDate
 * @property-read InputDate $endDate
 * @package BoondManager\Models\Filters\Search
 */
class Invoices extends AbstractSearch
{
	/**#@+
	 * @var int PERIOD
	 */
	const
		PERIOD_CREATED = 'created',
		PERIOD_EXPECTEDPAYMENT = 'expectedPayment',
		PERIOD_PERFORMEDPAYMENT = 'performedPayment',
		PERIOD_PERIOD = 'period';
	/**#@-*/

	/**#@+
	 * @var string SORT
	 */
	const
		ORDERBY_TURNOVERINCLUDEDTAX = 'turnoverIncludedTax',
		ORDERBY_TURNOVEREXCLUDEDTAX = 'turnoverExcludedTax',
		ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_EXPECTEDPAYMENTDATE= 'expectedPaymentDate',
		ORDERBY_STATE = 'state',
		ORDERBY_REFERENCE= 'reference',
		ORDERBY_ORDER_REFERENCE= 'order.reference',
		ORDERBY_ORDER_PROJECT_REFERENCE= 'order.project.reference',
		ORDERBY_ORDER_PROJECT_COMPANY_NAME= 'order.project.company.name',
		ORDERBY_MAINMANAGER_LASTNAME= 'mainManager.lastName';
	/**#@-*/

	/**
	 * Invoices constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

		$paymentMethods = new InputMultiValues('paymentMethods');
		$paymentMethods->addFilterInDict(Dictionary::getDict('specific.setting.paymentMethod'));
		$this->addInput($paymentMethods);

		$states = new InputMultiValues('states');
		$states->addFilterInDict(Dictionary::getDict('specific.setting.state.invoice'));
		$this->addInput($states);

		$closing = new InputBoolean('closing');
		$this->addInput($closing);

		$creditNote = new InputBoolean('creditNote');
		$this->addInput($creditNote);

		//project type (opportunity type)
		$projectTypes = new InputMultiValues('projectTypes');
		$projectTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
		$this->addInput($projectTypes);

		$period = new InputValue('period', BM::INDIFFERENT);
		$period->addFilterInArray([self::PERIOD_CREATED, self::PERIOD_EXPECTEDPAYMENT, self::PERIOD_PERFORMEDPAYMENT, self::PERIOD_PERIOD]);
		$this->addInput($period);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_CREATIONDATE, self::ORDERBY_EXPECTEDPAYMENTDATE, self::ORDERBY_MAINMANAGER_LASTNAME,
		self::ORDERBY_ORDER_PROJECT_COMPANY_NAME, self::ORDERBY_ORDER_PROJECT_REFERENCE, self::ORDERBY_ORDER_REFERENCE,
		self::ORDERBY_REFERENCE, self::ORDERBY_STATE, self::ORDERBY_TURNOVEREXCLUDEDTAX, self::ORDERBY_TURNOVERINCLUDEDTAX]);
		$this->addInput($sort);

		// Date de début & Fin de periode
		$startDate = new InputDate('startDate');
		$endDate = new InputDate('endDate');
		$this->addInput([$startDate, $endDate]);
	}

	/**
	 * generate a filter adapted for the user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_BILLING ));

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
