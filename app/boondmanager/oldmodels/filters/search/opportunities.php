<?php
/**
 * opportunities.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\OldModels\Filters\Search;

use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use BoondManager\OldModels\Filters\AbstractSearch;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

/**
 * Class Opportunities
 * build a filter for lists/opportunities (search & sort)
 * @package BoondManager\Models\Filters\Search
 */
class Opportunities extends AbstractSearch{

    /**#@+
     * @var int PERIODS
     */
    const
        PERIOD_CREATED = 'created', //~ Opportunities created between start and end date
		PERIOD_UPDATED_POSITIONING = 'updatedPositioning', //~ Opportunities where an associated positioning where updated between start and end date
		PERIOD_STARTED = 'started', //~ Opportunities started between start and end date
		PERIOD_UPDATED = 'updated'; //~ Opportunities updated between start and end date
    /**#@-*/

    /**#@+
     * @var string order columns
     */
    const
        ORDERBY_CREATIONDATE = 'creationDate',
		ORDERBY_TITLE = 'title',
		ORDERBY_COMPANY_NAME = 'company.name', //FIXME : erreur doc company.Name
		ORDERBY_PLACE = 'place',
		ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS = 'numberOfActivePositionings',
		ORDERBY_STARTDATE = 'startDate', //FIXME : erreur doc start
		ORDERBY_DURATION = 'duration',
		ORDERBY_STATE = 'state',
		ORDERBY_TOTAL_WEIGHTED_TURNOVER_EXCLUDED_TAX = 'totalWeightedTurnOverExcludedTax',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
    /**#@-*/

    /**
     * Opportunities constructor.
     * @param Perimeter|null $perimeter
     * @param FlagsList|null $flags
     */
    public function __construct(Perimeter $perimeter = null, FlagsList $flags = null)
    {
        parent::__construct();

        if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

        // AO_VISIBILITE
        $visibleOpportunity = new InputBoolean('visibleOpportunity', 1);
        $this->addInput($visibleOpportunity);

        //AO_ETAT
        $opportunityStates = new InputMultiValues('opportunityStates');
        $opportunityStates->addFilterInDict(Dictionary::getDict('specific.setting.state.opportunity'));
        $this->addInput($opportunityStates);

        //POS_ETAT
        $positioningStates = new InputMultiValues('positioningStates');
        $positioningStates->addFilterInDict(Dictionary::getDict('specific.setting.state.positioning'), ['-2']);
        $this->addInput($positioningStates);

        //AO_TYPEREF
        $opportunityTypes = new InputMultiValues('opportunityTypes');
        $opportunityTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.project'));
        $this->addInput($opportunityTypes);

        //AO_TYPESOURCE
        $origins = new InputMultiValues('origins');
        $origins->addFilterInDict(Dictionary::getDict('specific.setting.origin'));
        $this->addInput($origins);

        //AO_APPLICATIONS
        $activityAreas = new InputMultiValues('activityAreas');
        $activityAreas->addFilterInDict(Dictionary::getDict('specific.setting.activityArea'));
        $this->addInput($activityAreas);

        //AO_INTERVENTION
        $expertiseAreas = new InputMultiValues('expertiseAreas');
        $expertiseAreas->addFilterInDict(Dictionary::getDict('specific.setting.expertiseArea'));
        $this->addInput($expertiseAreas);

        //AO_LIEU
        $place = new InputValue('place');
	    $visibleOpportunity->setMode(InputValue::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
        $place->addFilterInDict(Dictionary::getDict('specific.setting.mobilityArea'));
        $this->addInput($place);

        //AO_DUREE
        $durations = new InputMultiValues('durations');
        $durations->addFilterInDict(Dictionary::getDict('specific.setting.duration'));
        $this->addInput($durations);

        //AO_OUTILS
        $tools = new InputMultiValues('tools');
        $tools->addFilterInDict(Dictionary::getDict('specific.setting.tool'));
        $this->addInput($tools);

        // period
        $period = new InputValue('period', BM::INDIFFERENT);
        $period->addFilterInArray([self::PERIOD_CREATED, self::PERIOD_UPDATED_POSITIONING, self::PERIOD_STARTED, self::PERIOD_UPDATED]);
        $this->addInput($period);

        // ordering column
		$sort = new InputMultiValues('sort');
        $sort->addFilterInArray([
            self::ORDERBY_CREATIONDATE, self::ORDERBY_TITLE, self::ORDERBY_COMPANY_NAME, self::ORDERBY_PLACE, self::ORDERBY_NUMBER_OF_ACTIVE_POSITIONINGS,
			self::ORDERBY_STARTDATE, self::ORDERBY_DURATION, self::ORDERBY_STATE, self::ORDERBY_TOTAL_WEIGHTED_TURNOVER_EXCLUDED_TAX,
			self::ORDERBY_MAINMANAGER_LASTNAME,
        ]);
        $this->addInput($sort);

        // period start & end dates
        $startDate = new InputDate('startDate');
        $endDate = new InputDate('endDate');
        $this->addInput([$startDate, $endDate]);

    }

    /*
     * définition de quelques aides pour manipuler ce filtre
     */

    /**
     * send the period dates as an array [_startDate_, _endDate_]
     *
     * @return array
     */
    public function getDatesPeriod(){
        return [$this->startDate->getValue(), $this->endDate->getValue()];
    }

	/**
	 * generate a filter adapted for the user
	 * @param CurrentUser $user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
	{
		if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_OPPORTUNITIES ), $user->getSearchFlags());

		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		$filter->visibleOpportunity->setDefaultValue(!$user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE))
		                           ->addFilterInArray($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) ? [0,1]:[1]);

		return $filter;
	}
}
