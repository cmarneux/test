<?php
/**
 * products.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */
namespace BoondManager\OldModels\Filters\Search;

use BoondManager\Services\Dictionary;
use Wish\Filters\Inputs\InputBoolean;
use BoondManager\OldModels\Filters\AbstractSearch;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Perimeter;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\BM;

/**
 * Class Products
 * build a filter for lists/products (search & sort)
 * @package BoondManager\Models\Filters\Search
 */
class Products extends AbstractSearch{

    /**#@+
     * @var string order columns
     */
    const
        ORDERBY_UPDATEDATE = 'updateDate',
        ORDERBY_REFERENCE = 'reference',
		ORDERBY_NAME = 'name',
		ORDERBY_TYPE = 'type',
		ORDERBY_PRICEEXCLUDINGTAX = 'priceExcludingTax',
		ORDERBY_MAINMANAGER_LASTNAME = 'mainManager.lastName';
    /**#@-*/

    /**
     * Products constructor.
     * @param Perimeter|null $perimeter
     * @param FlagsList|null $flags
     */
    public function __construct(Perimeter $perimeter = null, FlagsList $flags = null)
    {
        parent::__construct();

        if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		if(!$flags) $flags = new FlagsList();
		$this->setAvailableFlags($flags);

        //PRODUIT_ETAT
        $productStates = new InputMultiValues('productStates');
        $productStates->addFilterInDict(Dictionary::getDict('specific.setting.state.product'));
        $this->addInput($productStates);

        //PRODUIT_TYPE
        $productTypes = new InputMultiValues('productTypes');
        $productTypes->addFilterInDict(Dictionary::getDict('specific.setting.typeOf.subscription'));
        $this->addInput($productTypes);

        // ordering column
        $sort = new InputMultiValues('sort');
        $sort->addFilterInArray([
            self::ORDERBY_REFERENCE, self::ORDERBY_NAME, self::ORDERBY_TYPE, self::ORDERBY_PRICEEXCLUDINGTAX, self::ORDERBY_MAINMANAGER_LASTNAME
        ]);
        $this->addInput($sort);

    }

	/**
	 * generate a filter adapted for the user
	 * @param CurrentUser $user
	 * @return self
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null)
    {
        if(!$user) $user = CurrentUser::instance();

		$filter = new self($user->getSearchPerimeter( BM::MODULE_PRODUCTS ), $user->getSearchFlags());

        $filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );

		return $filter;
	}
}
