<?php
/**
 * times.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use Wish\Filters\Inputs\InputBoolean;
use Wish\Filters\Inputs\InputDate;
use Wish\Filters\Inputs\InputMultiValues;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\Perimeter;
use BoondManager\Models;

/**
 * Class Times
 * @package BoondManager\Models\Filters\Search
 * @property \Wish\Filters\Inputs\InputDate startDate
 * @property InputDate endDate
 * @property \Wish\Filters\Inputs\InputValue activityType
 * @property InputValue category
 * @property \Wish\Filters\Inputs\InputMultiValues resourceTypes
 * @property \Wish\Filters\Inputs\InputMultiValues excludeResourceTypes
 * @property InputBoolean returnDetailedExceptionalTimes
 * @property InputValue sort
 */
class Times extends AbstractSearch{

	/**#@+
	 * @var string TYPE
	 */
	const TYPE_ABSENCE = 'absence',
		  TYPE_INTERNAL = 'internal',
		  TYPE_PRODUCTION = 'production';
	/**#@-*/

	/**#@+
	 * @var string ORDERBY order columns
	 */
	const
		ORDERBY_CATEGORY = 'category',
		ORDERBY_STARTDATE = 'startDate',
		ORDERBY_LASTNAME = 'resource.lastName',
		ORDERBY_REFERENCE = 'workUnitType.reference';
	/**#@-*/

	/**
	 * Times constructor.
	 * @param Perimeter $perimeter
	 */
	public function __construct(Perimeter $perimeter){
		parent::__construct();

		if($perimeter) $this->setAvailablePerimeter($perimeter);
		else $this->setIndifferentPerimeter();

		$startDate = new InputDate('startDate');
		$this->addInput($startDate);

		$endDate = new InputDate('endDate');
		$this->addInput($endDate);

		$activityType = new InputValue('activityType');
		$activityType->addFilterInArray([self::TYPE_ABSENCE, self::TYPE_INTERNAL, self::TYPE_PRODUCTION]);
		$this->addInput($activityType);

		$category = new InputValue('category');
		$category->addFilterInArray([Models\Time::TYPE_TIME_REGULAR, Models\Time::TYPE_TIME_EXCEPTIONAL]);
		$this->addInput($category);

		$resourceType = new InputMultiValues('resourceTypes');
		$resourceType->addFilterInDict( Dictionary::getDict('specific.setting.typeOf.resource') );
		$this->addInput($resourceType);

		$excludeResourceType = new InputMultiValues('excludeResourceTypes');
		$excludeResourceType->addFilterInDict( Dictionary::getDict('specific.setting.typeOf.resource') );
		$this->addInput($excludeResourceType);

		$details = new InputBoolean('returnDetailedExceptionalTimes', 0);
		$this->addInput($details);

		$sort = new InputMultiValues('sort');
		$sort->addFilterInArray([self::ORDERBY_CATEGORY, self::ORDERBY_STARTDATE, self::ORDERBY_LASTNAME, self::ORDERBY_REFERENCE]);
		$this->addInput($sort);
	}

	/**
	 * @param CurrentUser $user
	 * @return Times
	 * @throws \Exception
	 */
	public static function getUserFilter(CurrentUser $user = null){
		if(!$user) $user = CurrentUser::instance();
		$filter = new self($user->getSearchPerimeter(BM::MODULE_ACTIVITIES_EXPENSES));
		$filter->narrowPerimeter->setDefaultValue( $user->getNarrowPerimeter() );
		return $filter;
	}
}
