<?php
/**
 * validations.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Filters\Search;

use Wish\Filters\Inputs\InputString;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\Validation;

/**
 * Class Validations
 * @package BoondManager\Models\Filters\Search
 * @property InputBoolean closed
 * @property InputValue startMonth
 * @property InputValue endMonth
 * @property InputMultiValues sort
 * @property InputMultiValues validationStates
 * @property InputMultiValues resourceTypes
 * @property InputValue documentType
 */
class Validations extends AbstractSearch{

	public function __construct(){
		parent::__construct();

		$this->addInput(
			(new InputString('type'))->addFilterInArray([Validation::TYPE_TIMESREPORT, Validation::TYPE_EXPENSESREPORT, Validation::TYPE_ABSENCESREPORT])
		);

	}

	/**
	 * @param CurrentUser $user
	 * @return Validations
	 */
	public static function getUserFilter()
	{
		return new self();
	}

}
