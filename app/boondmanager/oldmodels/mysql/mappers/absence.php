<?php
/**
 * absence.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class Absence extends Mapper{

	private $workUnitMapper;

	public function __construct()
	{
		$this->workUnitMapper = new WorkUnitType();
	}

	/**
	 * @param $data
	 * @return Models\Absence
	 */
	public function map($data)
	{
		if(!$data) return null;
		$absence =  new Models\Absence(self::extractData($data, [
			'ID_PABSENCE',
			'PABS_DEBUT',
			'PABS_FIN',
			'PABS_DUREE',
			'PABS_INTITULE'
		]));
		$absence->workUnitType = $this->workUnitMapper->map(self::extractData($data, [
			'PABS_TYPEHREF' => 'TYPEH_REF',
			'TYPEH_TYPEACTIVITE',
			'TYPEH_NAME'
		]));

		return $absence;
	}
}
