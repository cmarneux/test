<?php
/**
 * search.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers\Actions;

use Wish\Mapper;
use BoondManager\Lib\Models\Model;
use BoondManager\Services\BM;
use BoondManager\Models\Action;
use BoondManager\Models\App;
use BoondManager\Models\Candidate;
use BoondManager\Models\Company;
use BoondManager\Models\Contact;
use BoondManager\Models\Invoice;
use BoondManager\Models\Account;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Order;
use BoondManager\Models\Product;
use BoondManager\Models\Project;
use BoondManager\Models\Purchase;
use BoondManager\Models\Employee;

class Search extends Mapper{
	public function map($data)
	{
		if(!$data) return null;
		$action = new Action(['ACTION_TYPE'=>$data['ACTION_TYPE']]);
		switch ($action->category){

			case BM::CATEGORY_CRM_CONTACT:
				$action->dependsOn = new Contact(self::extractData($data, [
					'RAPPEL_ID' => 'ID_CRMCONTACT',
					'RAPPEL_NOM' => 'CCON_NOM',
					'RAPPEL_PRENOM' => 'CCON_PRENOM',
					'RAPPEL_TEL1' => 'CCON_TEL1',
					'RAPPEL_TEL2' => 'CCON_TEL2',
					'RAPPEL_TYPE' => 'CCON_TYPE',
					'RAPPEL_EMAIL' => 'CCON_EMAIL',
				]));
				$action->dependsOn->company = new Company(self::extractData($data, [
					'RAPPEL_IDPARENT' => 'ID_CRMSOCIETE',
					'RAPPEL_SOCIETE' => 'CSOC_SOCIETE'
				]));
				break;
			case BM::CATEGORY_CRM_COMPANY:
				$action->dependsOn = new Company(self::extractData($data, [
					'RAPPEL_ID' => 'ID_CRMSOCIETE',
					'RAPPEL_TEL1' => 'CSOC_TEL',
					'RAPPEL_TYPE' => 'CSOC_TYPE',
					'RAPPEL_SOCIETE' => 'CSOC_SOCIETE'
				]));
				break;
			case BM::CATEGORY_CANDIDATE:
				$action->dependsOn = new Candidate(self::extractData($data, [
					'RAPPEL_ID' => 'ID_PROFIL',
					'RAPPEL_NOM' => 'PROFIL_NOM',
					'RAPPEL_PRENOM' => 'PROFIL_PRENOM',
					'RAPPEL_TEL1' => 'PROFIL_TEL1',
					'RAPPEL_TEL2' => 'PROFIL_TEL2',
					'RAPPEL_TYPE' => 'PROFIL_TYPE',
					'RAPPEL_EMAIL' => 'PROFIL_EMAIL',
					'RAPPEL_SOCIETE' => 'PROFIL_STATUT'
				]));
				break;
			case BM::CATEGORY_RESOURCE:
				$action->dependsOn = new Employee(self::extractData($data, [
					'RAPPEL_ID' => 'ID_PROFIL',
					'RAPPEL_NOM' => 'PROFIL_NOM',
					'RAPPEL_PRENOM' => 'PROFIL_PRENOM',
					'RAPPEL_TEL1' => 'PROFIL_TEL1',
					'RAPPEL_TEL2' => 'PROFIL_TEL2',
					'RAPPEL_TYPE' => 'PROFIL_TYPE',
					'RAPPEL_EMAIL' => 'PROFIL_EMAIL',
					'RAPPEL_SOCIETE' => 'PROFIL_STATUT'
				]));
				break;
			case BM::CATEGORY_PRODUCT:
				$action->dependsOn = new Product(self::extractData($data,[
					'RAPPEL_ID' => 'ID_PRODUIT',
					'RAPPEL_NOM' => 'PRODUIT_NOM',
					'RAPPEL_TYPE' => 'PRODUIT_REF',
				]));
				break;
			case BM::CATEGORY_OPPORTUNITY:
				$action->dependsOn = new Opportunity(self::extractData($data, [
					'RAPPEL_ID' => 'ID_AO',
					'RAPPEL_TYPE' => 'AO_TITLE',
				]));
				$this->extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_PROJECT:
				$action->dependsOn = new Project(self::extractData($data, [
					'RAPPEL_ID' => 'ID_PROJET',
					'RAPPEL_TYPE' => 'PRJ_REFERENCE',
				]));
				$this->extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_BILLING:
				$action->dependsOn = new Invoice(self::extractData($data, [
					'RAPPEL_ID' => 'ID_FACTURATION',
					'RAPPEL_TYPE' => 'FACT_REF',
				]));
				$this->extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_ORDER:
				$action->dependsOn = new Order(self::extractData($data, [
					'RAPPEL_ID' => 'ID_BONDECOMMANDE',
					'RAPPEL_TYPE' => 'BDC_REFCLIENT',
				]));
				$this->extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_PURCHASE:
				$action->dependsOn = new Purchase(self::extractData($data, [
					'RAPPEL_ID' => 'ID_ACHAT',
					'RAPPEL_TYPE' => 'ACHAT_TITLE',
				]));
				$this->extractContactAndCompany($action->dependsOn, $data);
				break;
			case BM::CATEGORY_APP:
				$action->dependsOn = new App(self::extractData($data, [
					'RAPPEL_ID' => 'ID_MAINAPI'
				]));
				break;
		}

		$action->mainManager = new Account(self::extractData($data, [
			'RESP_ID' => 'ID_PROFIL',
			'RESP_PRENOM' => 'PROFIL_PRENOM',
			'RESP_NOM' => 'PROFIL_NOM',
		]));

		$action->fromArray($data);

		return $action;
	}

	private function extractContactAndCompany(Model $object, &$data){
		$object->contact = new Contact(self::extractData($data, [
			'RAPPEL_IDPARENT' => 'ID_CRMCONTACT',
			'RAPPEL_NOM' => 'CCON_NOM',
			'RAPPEL_PRENOM' => 'CCON_PRENOM',
			'RAPPEL_TEL1' => 'CCON_TEL1',
			'RAPPEL_TEL2' => 'CCON_TEL2',
			'RAPPEL_EMAIL' => 'CCON_EMAIL',
		]));
		$object->company = new Company(self::extractData($data, [
			'RAPPEL_IDPARENT2' => 'ID_CRMSOCIETE',
			'RAPPEL_SOCIETE' => 'CSOC_SOCIETE'
		]));;
	}
}
