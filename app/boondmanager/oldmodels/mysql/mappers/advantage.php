<?php
/**
 * advantage.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class Advantage extends Mapper{
	public function map($data)
	{
		$advantage = new Models\Advantage( self::extractData($data, [
			'ID_SOCIETE', 'ID_MISSIONPROJET', 'ID_CONTRAT', 'ID_AVANTAGE', 'AVG_COUT', 'AVG_DATE', 'AVG_FIN', 'AVG_MONTANTPARTICIPATION',
			'AVG_MONTANTRESSOURCE', 'AVG_MONTANTSOCIETE', 'AVG_QUANTITE', 'AVG_DEVISE', 'AVG_DEVISEAGENCE', 'AVG_CHANGE', 'AVG_CHANGEAGENCE',
			'AVG_COMMENTAIRES'
		]));

		$advantage->resource = new Models\Employee(self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_VISIBILITE', 'PROFIL_TYPE',
			'ID_RESPMANAGER', 'ID_RESPRH', 'COMP_IDSOCIETE' => 'ID_SOCIETE', 'USER_TYPE'
		]));

		if($data['ID_PROJET']){
			$advantage->project = new Models\Project(self::extractData($data, [
				'ID_PROJET', 'PRJ_REFERENCE'
			]));
		}

		$advantage->advantageType = new Models\AdvantageType(self::extractData($data, [
			'TYPEA_NAME',
			'AVG_TYPEAREF' => 'TYPEA_REF',
			'AVG_TYPE' => 'TYPEA_TYPE',
			'AVG_CATEGORIE' => 'TYPEA_CATEGORIE',
			'AVG_QUOTAPARTICIPATION' => 'TYPEA_QUOTAPARTICIPATION',
			'AVG_QUOTASOCIETE' => 'TYPEA_QUOTASOCIETE',
			'AVG_QUOTARESSOURCE' => 'TYPEA_QUOTARESSOURCE'
		]));

		return $advantage;
	}
}
