<?php
/**
 * contact.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;
use BoondManager\Services\Managers;

class Agency extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->db = new Local\Agency();
	}

	/**
	 * @param $data
	 * @return Models\Agency
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\Agency();
		if(isset($data['GRPCONF_VALIDATIONTEMPS'])) {
			$validators = array_filter(explode('|', self::extractData($data, ['GRPCONF_VALIDATIONTEMPS'])['GRPCONF_VALIDATIONTEMPS']), function ($manager) {
				return $manager != '';
			});
			// TODO : Tin : Attention les validateurs pourront bientôt aussi être des chefs de projets donc on ne pourra plus utiliser getIDProfilFromIDUser
			foreach ($validators as $validator) $entity->groupFieldsAs('timesReportsWorkflow', (new Employee())->map(['ID_PROFIL' => Managers::getEmployeeIdFromUserId($validator)]), $push = true);
		}

		if(isset($data['GRPCONF_VALIDATIONFRAIS'])) {
			$validators = array_filter(explode('|', self::extractData($data, ['GRPCONF_VALIDATIONFRAIS'])['GRPCONF_VALIDATIONFRAIS']), function ($manager) {
				return $manager != '';
			});
			// TODO : Tin : Attention les validateurs pourront bientôt aussi être des chefs de projets donc on ne pourra plus utiliser getIDProfilFromIDUser
			foreach ($validators as $validator) $entity->groupFieldsAs('expensesReportsWorkflow', (new Employee())->map(['ID_PROFIL' => Managers::getEmployeeIdFromUserId($validator)]), $push = true);
		}

		if(isset($data['GRPCONF_VALIDATIONABSENCES'])) {
			$validators = array_filter(explode('|', self::extractData($data, ['GRPCONF_VALIDATIONABSENCES'])['GRPCONF_VALIDATIONABSENCES']), function ($manager) {
				return $manager != '';
			});
			// TODO : Tin : Attention les validateurs pourront bientôt aussi être des chefs de projets donc on ne pourra plus utiliser getIDProfilFromIDUser
			foreach ($validators as $validator) $entity->groupFieldsAs('absencesReportsWorkflow', (new Employee())->map(['ID_PROFIL' => Managers::getEmployeeIdFromUserId($validator)]), $push = true);
		}

		$entity->fromArray($data, $this->retrieveBasic ? $this->db:null);

		return $entity;
	}
}
