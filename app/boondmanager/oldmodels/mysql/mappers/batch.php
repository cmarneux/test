<?php
/**
 * batch.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class Batch extends Mapper{

	/**
	 * @param $data
	 * @return Models\Batch
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\Batch();

		$entity->fromArray($data);

		return $entity;
	}
}
