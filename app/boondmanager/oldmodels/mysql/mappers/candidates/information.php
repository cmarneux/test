<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers\Candidates;

use Wish\Mapper;
use BoondManager\Databases\Local\Agency;
use BoondManager\Databases\Local\Manager;
use BoondManager\Databases\Local\Pole;
use BoondManager\Models;

class Information extends Mapper{

	private $dbAgency;
	private $dbPole;
	private $dbManagers;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->dbAgency = new Agency();
		$this->dbPole = new Pole();
		$this->dbManagers = new Manager();
	}

	public function map($data){
		if(!$data) return null;

		$candidate = new Models\Candidate( );

		$candidate->groupFieldsAs('mainManager', $this->dbManagers->getBasicManager($data['ID_PROFIL_RESPMANAGER']));
		$candidate->groupFieldsAs('hrManager', $this->dbManagers->getBasicManager($data['ID_PROFIL_RESPRH']));
		$candidate->groupFieldsAs('agency', $this->dbAgency->getBasicAgency($data['ID_SOCIETE']) );
		$candidate->groupFieldsAs('pole', $this->dbPole->getBasicPole($data['ID_POLE']) );

		if(isset($data['PARAM_TYPESOURCE'], $data['PARAM_SOURCE'])) {
			$candidate->groupFieldsAs('source', new Models\OriginOrSource(self::extractData($data, [
				'PARAM_TYPESOURCE' => 'TYPE_SOURCE',
				'PARAM_SOURCE'     => 'SOURCE'
			])));
		}

		if(isset($data['ID_CRMSOCIETE'], $data['ID_CRMCONTACT'])){
			$candidate->groupFieldsAs('providerContact', new Models\Contact( self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
			$candidate->groupFieldsAs('providerCompany', new Models\Company( self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));
		}

		if(isset($data['ID_PROFILCONSULTANT'])){
			$candidate->groupFieldsAs('resource', new Models\Employee( self::extractData($data, ['ID_PROFILCONSULTANT' => 'ID_PROFIL']) ));
		}

		$candidate->fromArray($data);

		return $candidate;
	}
}
