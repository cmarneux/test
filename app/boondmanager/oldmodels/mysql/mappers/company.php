<?php
/**
 * company.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;

class Company extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->db = new Local\Company();
	}

	/**
	 * @param $data
	 * @return Models\Company
	 */
	public function map($data)
	{
		if(!$data) return null; // permet d'éviter les boucles infinie car RowObject\Company contient d'autres RowObject\Company ...
		$entity = new Models\Company();

		$entity->groupFieldsAs('mainManager', (new Employee)->map(self::extractData($data, [
			'ID_PROFIL',
		])));
		$entity->groupFieldsAs('agency', (new Agency)->map(self::extractData($data, [
			'ID_SOCIETE',
		])));
		$entity->groupFieldsAs('pole', (new Pole)->map(self::extractData($data, [
			'ID_POLE',
		])));

		$entity->groupFieldsAs('parentCompany', (new Company)->map(self::extractData($data, [
			'ID_RESPSOC'   => 'ID_CRMSOCIETE',
			'RESP_SOCIETE' => 'CSOC_SOCIETE'
		])));

		$entity->groupFieldsAs('origin', (new OriginOrSource)->map( self::extractData($data, [
			'CSOC_TYPESOURCE' => 'TYPE_SOURCE',
			'CSOC_SOURCE' => 'SOURCE'
		])));

		$entity->fromArray($data, $this->retrieveBasic ? $this->db:null);

		return $entity;
	}
}
