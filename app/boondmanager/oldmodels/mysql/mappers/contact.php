<?php
/**
 * contact.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;

class Contact extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->db = new Local\Contact();
	}

	/**
	 * @param $data
	 * @return Models\Contact
	 */
	public function map($data)
	{
		if(!$data) return null;
		$contact = new Models\Contact();

		$contact->groupFieldsAs('mainManager', (new Employee)->map(self::extractData($data, [ 'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM' ])));
		$contact->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE'])));
		$contact->groupFieldsAs('company', new Models\Company( self::extractData($data, [
			'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_TEL', 'CSOC_ADR', 'CSOC_CP', 'CSOC_WEB', 'CSOC_SIREN', 'CSOC_NIC', 'CSOC_FAX',
			'CSOC_VILLE', 'CSOC_PAYS', 'CSOC_TYPE', 'CSOC_EFFECTIF', 'CSOC_METIERS', 'CSOC_SERVICES', 'CSOC_DATEUPDATE',
			'CSOC_INTERVENTION', 'CSOC_ADR'
		])));
		if($contact->company)
			$contact->company->groupFieldsAs('parentCompany', new Models\Company( self::extractData($data, [
				'ID_RESPSOC' => 'ID_CRMSOCIETE',
				'RESP_SOCIETE' => 'CSOC_SOCIETE'
			])));
		$contact->groupFieldsAs('pole', new Models\Pole($data['ID_POLE']));
		$contact->groupFieldsAs('lastAction', new Models\Action(self::extractData($data, [ 'ID_ACTION', 'ACTION_TYPE', 'ACTION_TEXTE', 'ACTION_DATE', 'ID_PARENT' ])));


		if(isset($data['TYPE_SOURCE'])) $contact->groupFieldsAs('origin', new Models\OriginOrSource( self::extractData($data, [
			'CCON_TYPESOURCE' => 'TYPE_SOURCE',
			'CCON_SOURCE' => 'SOURCE'
		])));

		$contact->fromArray($data, $this->retrieveBasic ? $this->db:null);

		return $contact;
	}
}
