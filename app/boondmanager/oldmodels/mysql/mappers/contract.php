<?php
/**
 * contract.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services;

class Contract extends Mapper {
	public function map($data)
	{
		$userData = [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_VISIBILITE', 'ID_RESPMANAGER', 'ID_RESPRH', 'PROFIL_TYPE',
			'USER_TYPE', 'COMP_IDSOCIETE' => 'ID_SOCIETE'
		];

		if(isset($data['ID_PROFIL'], $data['PROFIL_TYPE'])) {
			if($data['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE) {
				$entity = new Models\Candidate(self::extractData($data, $userData));
			}else{
				$entity = new Models\Employee(self::extractData($data, $userData));
			}

			if($entity->ID_PROFIL_RESPMANAGER)
				$entity->mainManager = Services\Managers::getBasic($entity->ID_PROFIL_RESPMANAGER);

			if($entity->ID_PROFIL_RESPRH)
				$entity->hrManager = Services\Managers::getBasic($entity->ID_PROFIL_RESPRH);
		}

		$contract = new Models\Contract( $data );

		if(isset($entity))
			$contract->dependsOn = $entity;

		return $contract;
	}
}
