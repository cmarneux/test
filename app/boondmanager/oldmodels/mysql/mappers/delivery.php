<?php
/**
 * delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;


use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Databases\Local;

class Delivery extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Delivery();
	}

	public function map($data)
	{
		return $data ?
			(new Models\Delivery)
				->groupFieldsAs('dependsOn', (new Employee)->map( self::extractData($data, [
					isset($data['ITEM_TYPE']) && $data['ITEM_TYPE'] == Models\Positioning::ENTITY_PROFILE_KEY ? 'ID_ITEM':'' => 'ID_PROFIL',
					'COMP_IDPROFIL'                                                                                          => 'ID_PROFIL',
					'PROFIL_NOM',
					'COMP_NOM'                                                                                               => 'PROFIL_NOM',
					'PROFIL_PRENOM',
					'COMP_PRENOM'                                                                                            => 'PROFIL_PRENOM',
					'USER_TYPE',
					'PROFIL_TYPE',
					'PROFIL_VISIBILITE',
					'ID_SOCIETE',
					'ID_POLE',
					'ID_RESPMANAGER',
					'ID_RESPRH',
				])))
				->groupFieldsAs('dependsOn', (new Product)->map( self::extractData($data, [
					'ID_PRODUIT',
					isset($data['ITEM_TYPE']) && $data['ITEM_TYPE'] == Models\Positioning::ENTITY_PRODUCT_KEY ? 'ID_ITEM':'' => 'ID_PRODUIT',
					'PRODUIT_NOM',
					'PRODUIT_REF',
					'PRODUIT_TYPE',
				])))
				->groupFieldsAs('project', (new Project)->map( self::extractData($data, [
					'ID_PROJET',
					'PRJ_IDPROFIL' => 'ID_PROFIL',
					'PRJ_POLE' => 'ID_POLE',
					'PRJ_SOCIETE' => 'ID_SOCIETE',
					'PRJ_REFERENCE',
					'PRJ_TYPEREF',
					'PRJ_TYPE',
					'PRJ_DEBUT',
					'PRJ_FIN',
					'PRJ_ADR',
					'PRJ_CP',
					'PRJ_VILLE',
					'PRJ_DEVISE',
					'PRJ_CHANGE',
					'PRJ_DEVISEAGENCE',
					'PRJ_CHANGEAGENCE',
					'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM',// mainManager
					'ID_AO', 'AO_TITLE', 'AO_REF', 'AO_IDPROFIL', 'AO_SOCIETE', 'AO_POLE',// opportunity
					'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM','CCON_TEL1', 'CCON_TEL2', 'CCON_SERVICE',// contact
					'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_BAREMESEXCEPTION', 'CSOC_ADR', 'CSOC_CP', 'CSOC_VILLE', 'CSOC_PAYS',// company
					'ID_CRMTECHNIQUE', 'CCTECH_NOM', 'CCTECH_PRENOM', 'CCTECH_TEL1', 'CCTECH_TEL2', 'CCTECH_SERVICE',// technical (contact)
					'CSTECH_IDCRMSOCIETE', 'CSTECH_SOCIETE', 'CSTECH_ADR', 'CSTECH_CP', 'CSTECH_VILLE', 'CSTECH_PAYS',// technical (company)
				])))
				->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}
}
