<?php
/**
 * deliverydetail.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class DeliveryDetail extends Mapper {
	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data) {
		$deliverydetail = new Models\DeliveryDetail($data);

		if(isset($data['ID_ACHAT']) && $data['ID_ACHAT']) {
			$deliverydetail->groupFieldsAs('purchase', new Models\Id(self::extractData($data, [
				'ID_ACHAT' => 'id'
			])));
		}

		return $deliverydetail;
	}
}
