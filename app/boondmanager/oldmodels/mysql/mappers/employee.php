<?php
/**
 * resource.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;

class Employee extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Employee();
	}

	/**
	 * @param $data
	 * @return Models\Employee
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Employee)
				->groupFieldsAs('account', (new User)->map(self::extractData($data, [
					'ID_USER',
					'USER_TAUXHORAIRE',
					'USER_ALLOWTEMPSEXCEPTION',
					'USER_VALIDATIONTEMPS',
					'USER_VALIDATIONFRAIS',
					'USER_VALIDATIONABSENCES',
				])))
				->groupFieldsAs('mainManager', (new Employee)->map(self::extractData($data, [
					'RESP_ID' => 'ID_PROFIL',
					'RESP_NOM' => 'PROFIL_NOM',
					'RESP_PRENOM' => 'PROFIL_PRENOM',
					'ID_PROFIL_RESPMANAGER' => 'ID_PROFIL', //profiles/resources/Information
				])))
				->groupFieldsAs('hrManager', (new Employee)->map(self::extractData($data, [
					'RESPRH_ID' => 'ID_PROFIL',
					'RESPRH_NOM' => 'PROFIL_NOM',
					'RESPRH_PRENOM' => 'PROFIL_PRENOM',
					'ID_PROFIL_RESPRH' => 'ID_PROFIL', //profiles/resources/Information
				])))
				->groupFieldsAs('agency', (new Agency)->map(self::extractData($data, [
					'ID_SOCIETE',
					'SOCIETE_RAISON',
				])))
				->groupFieldsAs('pole', (new Pole)->map(self::extractData($data, [
					'ID_POLE',
					'POLE_NAME',
				])))
				->groupFieldsAs('candidate', new Models\Candidate(self::extractData($data, [
					'ID_PROFILCANDIDAT'=> 'ID_PROFIL',
					'CAND_NOM' => 'PROFIL_NOM',
					'CAND_PRENOM' => 'PROFIL_PRENOM',
				])))
				->groupFieldsAs('providerContact', (new Contact)->map( self::extractData($data, [
					'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',
				])))
				->groupFieldsAs('providerCompany', (new Company)->map( self::extractData($data, [
					'ID_CRMSOCIETE', 'CSOC_SOCIETE',
				])))
				->groupFieldsAs('resume', (new Resume)->map(self::extractData($data, [
					'ID_CV',
				])))
				->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}
}
