<?php
/**
 * information.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers\Employees;

use Wish\Mapper;
use BoondManager\Databases\Local\Agency;
use BoondManager\Databases\Local\Manager;
use BoondManager\Databases\Local\Pole;
use BoondManager\Models;

class Information extends Mapper{

	private $dbAgency;
	private $dbPole;
	private $dbManagers;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->dbAgency = new Agency();
		$this->dbPole = new Pole();
		$this->dbManagers = new Manager();
	}

	public function map($data){
		if(!$data) return null;

		$resource = new Models\Employee( );

		$resource->groupFieldsAs('mainManager', $this->dbManagers->getBasicManager($data['ID_PROFIL_RESPMANAGER']));
		$resource->groupFieldsAs('hrManager', $this->dbManagers->getBasicManager($data['ID_PROFIL_RESPRH']));
		$resource->groupFieldsAs('agency', $this->dbAgency->getBasicAgency($data['ID_SOCIETE']) );
		$resource->groupFieldsAs('pole', $this->dbPole->getBasicPole($data['ID_POLE']) );
		$resource->groupFieldsAs('candidate', new Models\Candidate(self::extractData($data, [
		'ID_PROFILCANDIDAT'=> 'ID_PROFIL',
		'CAND_NOM' => 'PROFIL_NOM',
		'CAND_PRENOM' => 'PROFIL_PRENOM',
		])) );

		if(isset($data['ID_CRMSOCIETE'], $data['ID_CRMCONTACT'])){
			$resource->groupFieldsAs('providerContact', new Models\Contact( self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
			$resource->groupFieldsAs('providerCompany', new Models\Company( self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));
		}

		$resource->fromArray($data);

		return $resource;
	}
}
