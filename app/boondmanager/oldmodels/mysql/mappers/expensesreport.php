<?php
/**
 * expensesreport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Services\Managers;

class ExpensesReport extends Mapper{

	/**
	 * @param $data
	 * @return  Models\ExpensesReport
	 */
	public function map($data)
	{
		if(!$data) return null;

		$expensesReport = new Models\ExpensesReport();

		$expensesReport->fromArray($data, $this->retrieveBasic ? $this->db:null);

		return $expensesReport;
	}

	/**
	 * @param $array
	 * @return Models\ExpensesReport[]
	 */
	public function mapArray($array)
	{
		$reports = [];
		foreach($array as $data){
			// on extrait d'abord les données relatives au validateur s'il existe
			if(isset($data['RESP_ID_PROFIL'])){
				$expectedValidator = new Models\Employee(self::extractData($data, [
					'RESP_ID_PROFIL' => 'ID_PROFIL',
					'RESP_NOM' => 'PROFIL_NOM',
					'RESP_PRENOM' => 'PROFIL_PRENOM'
				]));

				$validation = new Models\Validation(self::extractData($data, [
					'ID_VALIDATION',
					'VAL_DATE',
					'VAL_STATE' => 'VAL_ETAT'
				]));

				$validation->groupFieldsAs('expectedValidator', $expectedValidator);
			}

			// on s'assure de travailler avec un tableau associatif (cle = chaine) pour maintenir l'ordre du search)
			$id = strval($data['ID_DOCUMENT']);

			if(!array_key_exists($id, $reports)){
				unset($validations);//~ Sinon les validations du dernier document sont ajoutée à tort à celui-ci
				$report = $this->map($data);
				$reports[$id] = $report;
			}else{
				$report = $reports[$id];
			}

			if(isset($validation)){
				if(!isset($validations)) $validations = [$validation];
				else $validations[] = $validation;
				$report->validations = $validations; //~ XXX : Tin : /!\ $report->validations[] = $validation ne fonctionne pas !!!
				unset($validation); //~ Sinon la dernière validation trouvée est ajoutée à tort sur le document suivant ...
			}
		}

		return array_values($reports);
	}
}
