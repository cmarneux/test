<?php
/**
 * getcontact.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local\Agency;
use BoondManager\Databases\Local\Manager;
use BoondManager\Databases\Local\Pole;
use BoondManager\Models;

class GetContact extends Mapper{

	private $dbPole, $dbManager, $dbAgency;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->dbPole = new Pole();
		$this->dbManager = new Manager();
		$this->dbAgency = new Agency();
	}

	public function map($data)
	{
		if(!$data) return null;
		$contact = new Models\Contact();

		$contact->groupFieldsAs('mainManager', $this->dbManager->getBasicManager($data['ID_PROFIL']));
		$contact->groupFieldsAs('agency', $this->dbAgency->getBasicAgency($data['ID_SOCIETE']));
		$contact->groupFieldsAs('pole', $this->dbPole->getBasicPole($data['ID_POLE']));

		$contact->groupFieldsAs('company', new Models\Company( self::extractData($data, [
			'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_TEL', 'CSOC_ADR', 'CSOC_CP', 'CSOC_WEB', 'CSOC_SIREN', 'CSOC_NIC', 'CSOC_FAX',
			'CSOC_VILLE', 'CSOC_PAYS', 'CSOC_TYPE', 'CSOC_EFFECTIF', 'CSOC_METIERS', 'CSOC_SERVICES', 'CSOC_DATEUPDATE',
			'CSOC_INTERVENTION', 'CSOC_ADR'
		])));

		$contact->company->groupFieldsAs('parentCompany', new Models\Company( self::extractData($data, [
			'ID_RESPSOC' => 'ID_CRMSOCIETE',
			'RESP_SOCIETE' => 'CSOC_SOCIETE'
		])));

		$contact->groupFieldsAs('origin', new Models\OriginOrSource( self::extractData($data, [
			'CCON_TYPESOURCE' => 'TYPE_SOURCE',
			'CCON_SOURCE' => 'SOURCE'
		])));

		$contact->fromArray($data);

		return $contact;
	}
}
