<?php
/**
 * getproduct.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Services\CurrentUser;
use BoondManager\Databases\Local\Agency;
use BoondManager\Databases\Local\Manager;
use BoondManager\Databases\Local\Pole;
use BoondManager\Models;

class GetProduct extends Mapper{

	private $dbAgency;
	private $dbPole;
	private $dbManagers;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->dbAgency = new Agency();
		$this->dbPole = new Pole();
		$this->dbManagers = new Manager();
	}

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		/*
		$productsFields = [
			'ID_PRODUIT', 'PRODUIT_REF', 'PRODUIT_TYPE', 'PRODUIT_NOM', 'PRODUIT_TARIFHT', 'PRODUIT_TARIFHT',
			'PRODUIT_CHANGE', 'PRODUIT_DEVISE', 'PRODUIT_CHANGEAGENCE', 'PRODUIT_DEVISEAGENCE'
		];
		*/

		$product = new Models\Product( );

		$product->groupFieldsAs('mainManager', $this->dbManagers->getBasicManager($data['ID_PROFIL']));
		$product->groupFieldsAs('agency', $this->dbAgency->getBasicAgency($data['ID_SOCIETE']) );
		$product->groupFieldsAs('pole', $this->dbPole->getBasicPole($data['ID_POLE']) );

		$product->fromArray($data);

		return $product;
	}
}
