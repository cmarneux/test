<?php
/**
 * opportunity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Databases\Local;

class Opportunity extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Opportunity();
	}

	/**
	 * @param $data
	 * @return Models\Opportunity
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Opportunity())
			->groupFieldsAs('mainManager', (new Employee)->map( self::extractData($data, [
				'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM',
			])))
			->groupFieldsAs('agency', (new Agency)->map( self::extractData($data, [
				'ID_SOCIETE',
			])))
			->groupFieldsAs('pole', (new Pole)->map( self::extractData($data, [
				'ID_POLE',
			])))
			->groupFieldsAs('origin', (new OriginOrSource)->map( self::extractData($data, [
				'AO_TYPESOURCE' => 'TYPE_SOURCE',
				'AO_SOURCE'     => 'SOURCE',
			])))
			->groupFieldsAs('contact', (new Contact)->map( self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',
			])))
			->groupFieldsAs('company', (new Company)->map( self::extractData($data, [
				'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_INTERVENTION', 'CSOC_METIERS',
				'CSOC_ADR', 'CSOC_CP', 'CSOC_TEL', 'CSOC_VILLE', 'CSOC_PAYS', 'CSOC_BAREMESEXCEPTION',
			])))
			->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}
}
