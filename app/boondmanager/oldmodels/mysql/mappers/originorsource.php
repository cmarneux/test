<?php
/**
 * originorsource.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class OriginOrSource extends Mapper{

	/**
	 * @param $data
	 * @return Models\OriginOrSource
	 */
	public function map($data)
	{
		return $data ?
			(new Models\OriginOrSource())
			->fromArray($data)
			:
			null;
	}
}
