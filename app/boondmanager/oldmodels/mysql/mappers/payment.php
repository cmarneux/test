<?php
/**
 * payments.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Databases\Local;

class Payment extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Payment();
	}

	/**
	 * @param $data
	 * @return  Models\Payment
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Payment())
			->groupFieldsAs('purchase', (new Purchase)->map( self::extractData($data, [
			'ID_ACHAT',
			'ACHAT_TITLE',
			'ACHAT_TYPE',
			'ACHAT_CATEGORIE',
			'ACHAT_REF',
			'ACHAT_DEVISE',
			'ACHAT_CHANGE',
			'ACHAT_DEVISEAGENCE',
			'ACHAT_CHANGEAGENCE',
			'ACHAT_DEBUT',
			'ACHAT_FIN',
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM',//mainManager
			'ID_PROJET', 'PRJ_REFERENCE',//project
			'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',//contact
			'ID_CRMSOCIETE', 'CSOC_SOCIETE',//company
			])))
			->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}
}
