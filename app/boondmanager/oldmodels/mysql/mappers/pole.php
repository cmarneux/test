<?php
/**
 * pole.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;

class Pole extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->db = new Local\Pole();
	}

	/**
	 * @param $data
	 * @return Models\Pole
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\Pole();

		$entity->fromArray($data, $this->retrieveBasic ? $this->db:null);

		return $entity;
	}
}
