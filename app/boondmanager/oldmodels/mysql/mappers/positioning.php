<?php
/**
 * positioning.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local\Agency;
use BoondManager\Databases\Local\Manager;
use BoondManager\Databases\Local\Pole;
use BoondManager\Models;

class Positioning extends Mapper{

	private $dbPole, $dbManager, $dbAgency;

	public function __construct($class = null)
	{
		parent::__construct($class);

		$this->dbPole = new Pole();
		$this->dbManager = new Manager();
		$this->dbAgency = new Agency();
	}

	public function map($data)
	{
		if(!$data) return null;
		$positioning = new Models\Positioning();

		$positioning->groupFieldsAs('mainManager', $this->dbManager->getBasicManager($data['ID_RESPPROFIL']));

		$positioning->groupFieldsAs('project', new Models\Project(self::extractData($data, ['ID_PROJET', 'PRJ_REFERENCE'])));

		$opportunity = new Models\Opportunity(self::extractData($data, ['ID_AO', 'AO_TITLE', 'AO_REF', 'AO_TYPE', 'AO_TYPEREF', 'AO_VISIBILITE', 'canReadContact', 'canReadCompany']));

		$opportunity->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, ['AO_IDPROFIL' => 'ID_PROFIL'])));
		$opportunity->groupFieldsAs('contact', new Models\Contact(self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$opportunity->groupFieldsAs('company', new Models\Company(self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));
		$positioning->groupFieldsAs('opportunity', $opportunity);

		if($data['ID_PRODUIT']) $positioning->groupFieldsAs('dependsOn', new Models\Product(self::extractData($data, ['ID_ITEM' => 'ID_PRODUIT', 'PRODUIT_NOM', 'PRODUIT_REF', 'PRODUIT_TYPE'])));
		else {
			$entityData = self::extractData($data, [
				'ID_ITEM' => 'ID_PROFIL',
				'PROFIL_NOM',
				'PROFIL_PRENOM',
			]);
			$positioning->groupFieldsAs('dependsOn',
				$data['PROFIL_TYPE'] == Models\Candidate::TYPE_CANDIDATE ? new Models\Candidate($entityData) : new Models\Employee($entityData));
		}

		$positioning->fromArray($data);

		return $positioning;
	}
}
