<?php
/**
 * product.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Databases\Local;

class Product extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Product();
	}

	/**
	 * @param $data
	 * @return Models\Product
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Product())
			->groupFieldsAs('mainManager', (new Employee)->map( self::extractData($data, [
				'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'ID_USER',
			])))
			->groupFieldsAs('agency', (new Agency)->map( self::extractData($data, [
				'ID_SOCIETE', 'SOCIETE_RAISON',
			])))
			->groupFieldsAs('pole', (new Pole)->map( self::extractData($data, [
				'ID_POLE', 'POLE_NAME',
			])))
			->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}
}
