<?php
/**
 * profiluserconfig.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class ProfilUserConfig extends Mapper{
	public function map($data){
		$resource = new Models\Employee(self::extractData($data, [
			'ID_PROFIL', 'ID_POLE', 'ID_RESPMANAGER', 'ID_RESPRH', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_EMAIL', 'ID_SOCIETE', 'MAX_FIN', 'MIN_DEBUT'
		]));

		if(array_key_exists('ID_USER', $data)) {
			$resource->user = new Models\User(self::extractData($data, [
				'ID_USER', 'USER_TYPE', 'USER_LOGIN', 'USER_LANGUE', 'USER_ABONNEMENT', 'USER_ALLOWTEMPSEXCEPTION',
				'USER_DEFAULTSEARCH', 'USER_JOINCATEGORY', 'USER_HOMEPAGE', 'USER_TPSFRSSTART', 'USER_TAUXHORAIRE',
				'USER_VALIDATIONTEMPS', 'USER_VALIDATIONFRAIS', 'USER_VALIDATIONABSENCES', 'USER_ALLOWABSENCESTYPEHREF',
				'USER_TPSFRSETAT'
			]));
		}

		if(array_key_exists('CONFIG_MENUBAR', $data) && $resource->user){
			$resource->user->config = new Models\UserConfig(self::extractData($data, ['CONFIG_MENUBAR', 'CONFIG_DASHBOARD']));
		}

		return $resource;
	}
}
