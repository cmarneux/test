<?php
/**
 * project.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;

class Project extends Mapper {

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Project();
	}

	/**
	 * @param $data
	 * @return Models\Project
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Project)
			->groupFieldsAs('mainManager', (new Employee)->map( self::extractData($data, [
				'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM',
			])))
			->groupFieldsAs('opportunity', (new Opportunity)->map( self::extractData($data, [
				'ID_AO', 'AO_TITLE',
				'AO_REF',
				'AO_IDPROFIL' => 'ID_PROFIL',
				'AO_SOCIETE'  => 'ID_SOCIETE',
				'AO_POLE'     => 'ID_POLE',
			])))
			->groupFieldsAs('contact', (new Contact)->map( self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',
			])))
			->groupFieldsAs('company', (new Company)->map( self::extractData($data, [
				'ID_CRMSOCIETE', 'CSOC_SOCIETE',
				'CSTECH_IDCRMSOCIETE' => 'ID_CRMSOCIETE', //company
				'CSTECH_SOCIETE'      => 'CSOC_SOCIETE',
				'CSTECH_ADR'          => 'CSOC_ADR',
				'CSTECH_CP'           => 'CSOC_CP',
				'CSTECH_VILLE'        => 'CSOC_VILLE',
				'CSTECH_PAYS'         => 'CSOC_PAYS',
			])))
			->groupFieldsAs('technical', (new Contact)->map( self::extractData($data, [
				'ID_CRMTECHNIQUE' => 'ID_CRMCONTACT',
				'CCTECH_NOM'      => 'CCON_NOM',
				'CCTECH_PRENOM'   => 'CCON_PRENOM',
				'CCTECH_TEL1'     => 'CCON_TEL1',
				'CCTECH_TEL2'     => 'CCON_TEL2',
				'CCTECH_SERVICE'  => 'CCON_SERVICE',
			])))
			->groupFieldsAs('projectManagers', (new Employee)->mapArray(
				array_map(
					function ($id) {return ['ID_PROFIL' => $id];}
					,
					array_filter(explode('|', self::extractData($data, ['ID_PROFILCDP'])['ID_PROFILCDP']), function($manager) {return $manager != '';})
				)
			))
			->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}

	/**
	 * @param $array
	 * @return Models\Project[]
	 */
	public function mapArray($array)
	{
		$projects = [];
		foreach($array as $data){
			if(isset($data['ID_MISSIONPROJET']) && (!$deliveryID || $deliveryID != $data['ID_MISSIONPROJET']))
			{
				$delivery = (new SearchDelivery())->map(self::extractData($data, [
					'ID_MISSIONPROJET',
					'MP_NOM',
					'MP_DEBUT',
					'MP_FIN',
				]));
				$deliveryID = $delivery->ID_MISSIONPROJET;
			}

			if(isset($data['ID_LOT']) && (!$batchID || $batchID != $data['ID_LOT']))
			{
				$batch = (new Batch())->map(self::extractData($data, [
					'ID_LOT',
					'LOT_TITRE',
				]));
				$batchID = $batch->id;
			}

			// on s'assure de travailler avec un tableau associatif (cle = chaine) pour maintenir l'ordre du search)
			$id = strval($data['ID_PROJET']);

			if(!array_key_exists($id, $projects)){
				unset($deliveries);
				unset($batches);
				$project = $this->map($data);
				$projects[$id] = $project;
			}else{
				$project = $projects[$id];
			}

			if(isset($delivery)){
				if(!$deliveries) $deliveries = [$delivery];
				else $deliveries[] = $delivery;
				$project->deliveries = $deliveries;
				unset($delivery); //~ Sinon la dernière mission trouvée est ajoutée à tort sur le projet suivant ...
			}
			if(isset($batch)){
				if(!$batches) $batches = [$batch];
				else $batches[] = $batch;
				$project->batches = $batches;
				unset($batch); //~ Sinon le dernier lot trouvé est ajouté à tort sur le projet suivant ...
			}
		}

		return array_values($projects);
	}
}
