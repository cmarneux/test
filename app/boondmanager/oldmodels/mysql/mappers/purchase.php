<?php
/**
 * purchase.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Databases\Local;

class Purchase extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Purchase();
	}

	/**
	 * @param $data
	 * @return Models\Purchase
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Purchase())
			->groupFieldsAs('mainManager', (new Employee)->map( self::extractData($data, [
				'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM',
			])))
			->groupFieldsAs('agency', (new Agency)->map( self::extractData($data, [
				'ID_SOCIETE', 'SOCIETE_RAISON',
			])))
			->groupFieldsAs('pole', (new Pole)->map( self::extractData($data, [
				'ID_POLE', 'POLE_NAME',
			])))
			->groupFieldsAs('company', (new Company)->map( self::extractData($data, [
				'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_TVA',
			])))
			->groupFieldsAs('contact', (new Contact)->map( self::extractData($data, [
				'ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM',
			])))
			->groupFieldsAs('project', (new Project)->map( self::extractData($data, [
				'ID_PROJET', 'PRJ_REFERENCE', 'PRJ_TYPEREF', 'PRJ_TYPE',
			])))
			->groupFieldsAs('delivery', (new Delivery)->map( self::extractData($data, [
				'ID_MISSIONPROJET', 'ID_MISSIONDETAILS', 'ID_PARENT', 'PARENT_TYPE', 'MP_NOM',
			])))
			->fromArray($data, $this->retrieveBasic ? $this->db:null)
			:
			null;
	}
}
