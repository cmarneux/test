<?php
/**
 * resume.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class Resume extends Mapper{

	/**
	 * @param $data
	 * @return Models\Resume
	 */
	public function map($data)
	{
		return $data ?
			(new Models\Resume())
			->fromArray($data)
			:
			null;
	}
}
