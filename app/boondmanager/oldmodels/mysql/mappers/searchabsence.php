<?php
/**
 * searchabsence.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchAbsence extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$absence = new Models\Absence();

		$absencesReport = new Models\AbsencesReport(self::extractData($data, [
			'ID_LISTEABSENCES',
			'LISTEABSENCES_DATE',
			'LISTEABSENCES_TITRE',
			'LISTEABSENCES_COMMENTAIRES',
			'VAL_STATE' => 'LISTEABSENCES_ETAT',
		]));
		$resource = new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_STATUT']));
		$resource->groupFieldsAs('account', new Models\User(self::extractData($data, ['ID_USER','USER_TAUXHORAIRE'])));
		$absencesReport->groupFieldsAs('resource', $resource);
		$absencesReport->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE', 'SOCIETE_RAISON', 'GRPCONF_TAUXHORAIRE'])));
		$absence->groupFieldsAs('absencesReport', $absencesReport);

		$absence->groupFieldsAs('workUnitType', new Models\WorkUnitType(self::extractData($data, ['PABS_TYPEHREF' => 'TYPEH_REF', 'TYPEH_TYPEACTIVITE', 'TYPEH_NAME'])));

		$absence->fromArray($data);

		return $absence;
	}
}
