<?php
/**
 * searchadvantage.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchAdvantage extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$agency = $data['ID_SOCIETE'];//~ car ce champ est usilisé dans 2 dépendances ...

		$advantage = new Models\Advantage();

		$advantage->groupFieldsAs('resource', new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE'])));

		$advantage->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE', 'SOCIETE_RAISON'])));

		$advantage->groupFieldsAs('typeOf', new Models\AdvantageType(
			self::extractData($data, ['AVG_TYPEAREF' => 'TYPEA_REF', 'TYPEA_NAME'])+
			['ID_SOCIETE' => $agency, 'ID_TYPEAVANTAGE' => 1]));

		$advantage->fromArray($data);

		return $advantage;
	}
}
