<?php
/**
 * searchbillingdeliveriespurchasesbalance.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchBillingDeliveriesPurchasesBalance extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		if($data['DLVPRC_TYPE'] == 0) $entity = new Models\Delivery();
		else if($data['DLVPRC_TYPE'] == 1) $entity = new Models\Purchase();

		$order = new Models\Order(self::extractData($data, [
			'ID_BONDECOMMANDE',
			'BDC_REFCLIENT',
			'BDC_REF',
		]));

		$project =  new Models\Project(self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'PRJ_TYPEREF',
			'PRJ_TYPE',
			'PRJ_DEVISEAGENCE',
			'PRJ_CHANGEAGENCE',
			'PRJ_DEVISE',
			'PRJ_CHANGE',
		]));
		$project->groupFieldsAs('opportunity', new Models\Opportunity(self::extractData($data, ['ID_AO', 'AO_TITLE'])));
		$project->groupFieldsAs('contact', new Models\Contact(self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$project->groupFieldsAs('company', new Models\Company(self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));

		$entity->groupFieldsAs('order', $order);
		$entity->groupFieldsAs('project', $project);

		if($data['DLVPRC_TYPE'] == 0) switch($data['ITEM_TYPE']){
			case 0:
				$entity->groupFieldsAs('resource', new Models\Employee(self::extractData($data, [
					'COMP_IDPROFIL' => 'ID_PROFIL',
					'COMP_NOM' => 'PROFIL_NOM',
					'COMP_PRENOM' => 'PROFIL_PRENOM'])));
				break;
			case 1:
				$entity->groupFieldsAs('product', new Models\Product(self::extractData($data, ['ID_PRODUIT', 'ITEM_TITRE' => 'PRODUIT_NOM'])));
				break;
		}

		$entity->fromArray($data['DLVPRC_TYPE'] == 0 ?
		self::extractData($data, [
		'ID_ITEM' => 'ID_MISSIONPROJET',
		'ITEM_DEBUT' => 'MP_DEBUT',
		'ITEM_FIN' => 'MP_FIN',
		'ITEM_TITRE' => 'MP_NOM',
		'NB_BDC',
		'NB_COR',
		])
		:
		self::extractData($data, [
		'ID_ITEM' => 'ID_ACHAT',
		'ITEM_DEBUT' => 'ACHAT_DEBUT',
		'ITEM_FIN' => 'ACHAT_FIN',
		'ITEM_TITRE' => 'ACHAT_TITLE',
		'NB_BDC',
		'NB_COR',
		])
		);

		return $entity;
	}
}
