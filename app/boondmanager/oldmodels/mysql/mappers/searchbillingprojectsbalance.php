<?php
/**
 * searchbillingprojectsbalance.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchBillingProjectsBalance extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$project = new Models\Project();

		$project->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));
		$project->groupFieldsAs('opportunity', new Models\Opportunity(self::extractData($data, ['ID_AO', 'AO_TITLE'])));
		$project->groupFieldsAs('contact', new Models\Contact(self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$project->groupFieldsAs('company', new Models\Company(self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));

		$project->fromArray($data);

		return $project;
	}
}
