<?php
/**
 * searchcandidate.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchCandidate extends Mapper{
	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$candidate = new Models\Candidate();

		$candidate->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, [
			'ID_RESPMANAGER' => 'ID_PROFIL',
			'RESP_NOM'       => 'PROFIL_NOM',
			'RESP_PRENOM'    => 'PROFIL_PRENOM',
		])));

		$candidate->groupFieldsAs('hrManager', new Models\Employee(self::extractData($data, [
			'RESPRH_ID'     => 'ID_PROFIL',
			'RESPRH_NOM'    => 'PROFIL_NOM',
			'RESPRH_PRENOM' => 'PROFIL_PRENOM',
		])));

		$candidate->groupFieldsAs('resume', new Models\Resume(self::extractData($data, ['ID_CV'])));

		$candidate->fromArray($data);

		return $candidate;
	}
}
