<?php
/**
 * searchcompany.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchCompany extends Mapper{
	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$company = new Models\Company();

		$company->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, [ 'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM' ])));

		$company->fromArray($data);

		return $company;
	}
}
