<?php
/**
 * searchcontact.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchContact extends Mapper{
	/**
	 * @param $data
	 * @return \BoondManager\Lib\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$contact = new Models\Contact();

		$contact->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, [ 'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM' ])));
		$contact->groupFieldsAs('company', new Models\Company(self::extractData($data, [ 'ID_CRMSOCIETE', 'CSOC_SOCIETE', 'CSOC_INTERVENTION' ])));
		$contact->groupFieldsAs('lastAction', new Models\Action(self::extractData($data, [ 'ID_ACTION', 'ACTION_TYPE', 'ACTION_TEXTE', 'ACTION_DATE', 'ID_PARENT' ])));

		$contact->fromArray($data);

		return $contact;
	}
}
