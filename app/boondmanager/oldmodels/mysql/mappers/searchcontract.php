<?php
/**
 * searchcontract.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\Databases\Local;

class SearchContract extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$contract = new Models\Contract();

		$contract->groupFieldsAs('resource', new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));

		if($tab = 'resources/{id}/administrative'){//~ FIXME: ne devrait être fait que dans ce cas ... (==)
			$agenciesDB = new Local\Agency();
			$data['advantageTypes'] = $agenciesDB->getAgency($data['ID_SOCIETE'],6)->toArray()['advantageTypes'];
			//~ TODO: il faudrait filtrer les champs de chaque type d'avantage, qui ne doivent pas être retournés par resources/{id}/administrative
			//~ TODO: peut-être ajouter un paramètre $tab au service de recherche des contrats pour savoir quel champs des contrats retourner en fonction de lónglet d'appel ...
		}

		$contract->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE', 'SOCIETE_RAISON', 'advantageTypes'])));

		$contract->fromArray($data);

		return $contract;
	}
}
