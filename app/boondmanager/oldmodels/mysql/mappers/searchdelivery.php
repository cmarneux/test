<?php
/**
 * searchdelivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchDelivery extends Mapper{
	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\Delivery();

		$entity->groupFieldsAs('dependsOn', new Models\Employee(self::extractData($data, [
			'COMP_IDPROFIL' => 'ID_PROFIL',
			'COMP_NOM' => 'PROFIL_NOM',
			'COMP_PRENOM' => 'PROFIL_PRENOM'])));

		$entity->groupFieldsAs('dependsOn', new Models\Product(self::extractData($data, ['ID_PRODUIT', 'PRODUIT_NOM'])));

		$project = new Models\Project(self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'PRJ_TYPEREF',
			'PRJ_TYPE',
			'PRJ_DEVISE',
			'PRJ_CHANGE',
			'PRJ_DEVISEAGENCE',
			'PRJ_CHANGEAGENCE']));
		$project->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));
		$project->groupFieldsAs('opportunity', new Models\Opportunity(self::extractData($data, ['ID_AO', 'AO_TITLE'])));
		$project->groupFieldsAs('contact', new Models\Contact(self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$project->groupFieldsAs('company', new Models\Company(self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));

		$entity->groupFieldsAs('project', $project);

		$entity->fromArray($data);

		return $entity;
	}
}
