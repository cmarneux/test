<?php
/**
 * searchexpense.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchExpense extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$expense = new Models\Expense();

		$expensesReport = new Models\ExpensesReport(self::extractData($data, [
			'ID_LISTEFRAIS',
			'LISTEFRAIS_DATE',
			'LISTEFRAIS_BKMVALUE',
			'LISTEFRAIS_AVANCE',
			'VAL_STATE' => 'LISTEFRAIS_ETAT',
		]));
		$resource = new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_STATUT']));
		$resource->groupFieldsAs('account', new Models\User(self::extractData($data, ['ID_USER','USER_TAUXHORAIRE'])));
		$expensesReport->groupFieldsAs('resource', $resource);
		$expensesReport->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE', 'SOCIETE_RAISON', 'GRPCONF_TAUXHORAIRE'])));
		$expense->groupFieldsAs('expensesReport', $expensesReport);

		$expense->groupFieldsAs('delivery', new Models\Delivery(self::extractData($data, ['ID_MISSIONPROJET'])));
		$expense->groupFieldsAs('batch', (new Batch())->map(self::extractData($data, ['ID_LOT', 'LOT_TITRE'])));

		$project = (new Project())->map(self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'ID_CRMCONTACT',
			'CCON_NOM',
			'CCON_PRENOM',
			'ID_CRMSOCIETE',
			'CSOC_SOCIETE',
			'ID_PROFILCDP',
		]));
		$expense->groupFieldsAs('project', $project);
		$expense->groupFieldsAs('expenseType', new Models\ExpenseType(self::extractData($data, ['FRAIS_TYPEREF' => 'TYPEFRS_REF', 'TYPEFRS_TVA', 'TYPEFRS_NAME'])));
//var_dump($expense->expenseType);
		$expense->fromArray($data);

		return $expense;
	}
}
