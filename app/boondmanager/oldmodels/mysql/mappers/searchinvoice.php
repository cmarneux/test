<?php
/**
 * searchinvoice.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchInvoice extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$invoice = new Models\Invoice();

		$order = new Models\Order(self::extractData($data, [
			'ID_BONDECOMMANDE',
			'BDC_REFCLIENT',
			'BDC_REF',
		]));
		$order->groupFieldsAs('mainManager', new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));

		$project =  new Models\Project(self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'PRJ_TYPEREF',
			'PRJ_TYPE',
		]));
		$project->groupFieldsAs('opportunity', new Models\Opportunity(self::extractData($data, ['ID_AO', 'AO_TITLE'])));
		$project->groupFieldsAs('contact', new Models\Contact(self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$project->groupFieldsAs('company', new Models\Company(self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));
		$order->groupFieldsAs('project', $project);
		$invoice->groupFieldsAs('order', $order);

		$invoice->groupFieldsAs('termOfPayment', new Models\Schedule(self::extractData($data, [
			'ID_ECHEANCIER',
			'ECH_DATE',
			'ECH_DESCRIPTION',
		])));

		$invoice->fromArray($data);

		return $invoice;
	}
}
