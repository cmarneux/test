<?php
/**
 * searchpayment.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchPayment extends Mapper{
	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\Payment();

		$purchase = new Models\Purchase( self::extractData($data, [
			'ID_ACHAT',
			'ACHAT_TITLE',
			'ACHAT_TYPE',
			'ACHAT_CATEGORIE',
			'ACHAT_REF',
			'ACHAT_DEVISE',
			'ACHAT_CHANGE',
			'ACHAT_DEVISEAGENCE',
			'ACHAT_CHANGEAGENCE',
			'ACHAT_DEBUT',
			'ACHAT_FIN'
		]));
		$purchase->groupFieldsAs('mainManager', new Models\Employee( self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));
		$purchase->groupFieldsAs('project', new Models\Project( self::extractData($data, ['ID_PROJET', 'PRJ_REFERENCE'])));
		$purchase->groupFieldsAs('contact', new Models\Contact( self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$purchase->groupFieldsAs('company', new Models\Company( self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));

		$entity->groupFieldsAs('purchase', $purchase);

		$entity->fromArray($data);

		return $entity;
	}
}
