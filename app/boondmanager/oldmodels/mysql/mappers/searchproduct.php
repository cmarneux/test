<?php
/**
 * searchProduct.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchProduct extends Mapper{

	/**
	 * @param $data
	 * @return \BoondManager\Lib\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		/*
		$productsFields = [
			'ID_PRODUIT', 'PRODUIT_REF', 'PRODUIT_TYPE', 'PRODUIT_NOM', 'PRODUIT_TARIFHT', 'PRODUIT_TARIFHT',
			'PRODUIT_CHANGE', 'PRODUIT_DEVISE', 'PRODUIT_CHANGEAGENCE', 'PRODUIT_DEVISEAGENCE'
		];
		*/

		$product = new Models\Product( );

		$product->groupFieldsAs('mainManager', new Models\Employee( self::extractData($data, ['ID_USER', 'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));

		$product->fromArray($data);

		return $product;
	}
}
