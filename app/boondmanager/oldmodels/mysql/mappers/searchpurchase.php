<?php
/**
 * searchpurchase.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchPurchase extends Mapper{
	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\Purchase();

		$entity->groupFieldsAs('mainManager', new Models\Employee( self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM'])));
		$entity->groupFieldsAs('project', new Models\Project( self::extractData($data, ['ID_PROJET', 'PRJ_REFERENCE'])));
		$entity->groupFieldsAs('contact', new Models\Contact( self::extractData($data, ['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM'])));
		$entity->groupFieldsAs('company', new Models\Company( self::extractData($data, ['ID_CRMSOCIETE', 'CSOC_SOCIETE'])));

		$entity->fromArray($data);

		return $entity;
	}
}
