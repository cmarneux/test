<?php
/**
 * searchtime.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class SearchTime extends Mapper{

	/**
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data)
	{
		if(!$data) return null;
		$time = new Models\Time();

		$timesReport = new Models\TimesReport(self::extractData($data, [
			'ID_LISTETEMPS',
			'LISTETEMPS_DATE',
			'VAL_STATE' => 'LISTETEMPS_ETAT',
		]));
		$resource = new Models\Employee(self::extractData($data, ['ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_TYPE', 'PROFIL_STATUT']));
		$resource->groupFieldsAs('account', new Models\User(self::extractData($data, ['ID_USER','USER_TAUXHORAIRE'])));
		$timesReport->groupFieldsAs('resource', $resource);
		$timesReport->groupFieldsAs('agency', new Models\Agency(self::extractData($data, ['ID_SOCIETE', 'SOCIETE_RAISON', 'GRPCONF_TAUXHORAIRE'])));
		$time->groupFieldsAs('timesReport', $timesReport);

		$time->groupFieldsAs('delivery', new Models\Delivery(self::extractData($data, ['ID_MISSIONPROJET'])));
		$time->groupFieldsAs('batch', (new Batch())->map(self::extractData($data, ['ID_LOT', 'LOT_TITRE'])));

		$project = (new Project())->map(self::extractData($data, [
			'ID_PROJET',
			'PRJ_REFERENCE',
			'ID_CRMCONTACT',
			'CCON_NOM',
			'CCON_PRENOM',
			'ID_CRMSOCIETE',
			'CSOC_SOCIETE',
			'ID_PROFILCDP',
		]));
		$time->groupFieldsAs('project', $project);

		$rules = array_filter(explode('#', self::extractData($data, ['TEMPSEXP_REGLES'])['TEMPSEXP_REGLES']), function($rule) {return $rule != '';});
		foreach($rules as $ruleStr) {
			$ruleFields = explode('|', $ruleStr);
			$rule = new Models\ExceptionalRule([
			'REGLE_REF' => $ruleFields[0], //ruleReference
			'REGLE_BAREMEREF' => $ruleFields[1], //scaleReference
			'REGLE_QUANTITY' => $ruleFields[2], //quantity
			'REGLE_DELIVERYTYPE' => $ruleFields[5], //deliveryType (0|purchasor#1|transferor)
			]);
			//~ 'PARENT_TYPE' => $ruleFields[3], // these 2 fields will be grouped in dependsOn dependancy
			//~ 'ID_PARENT' => $ruleFields[4],
			switch($ruleFields[3]){
				case 0: $rule->groupFieldsAs('dependsOn', new Models\Agency(['ID_SOCIETE' => $ruleFields[4]]));
					break;
				case 1: $rule->groupFieldsAs('dependsOn', new Models\Company(['ID_CRMSOCIETE' => $ruleFields[4]]));
					break;
			}
			$time->groupFieldsAs('rules', $rule, $push = true);
		}

		$time->groupFieldsAs('workUnitType', new Models\WorkUnitType(self::extractData($data, ['TYPEH_REF', 'TYPEH_TYPEACTIVITE', 'TYPEH_NAME'])));

		$time->fromArray($data);

		return $time;
	}
}
