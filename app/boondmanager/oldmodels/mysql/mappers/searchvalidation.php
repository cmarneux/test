<?php
/**
 * searchvalidation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;
use BoondManager\OldModels\Filters;

class SearchValidation extends Mapper{


	/**
	 * @param $data
	 * @return  Models\Validation
	 */
	public function map($data){

		if(!$data) return null;
		$validation = new Models\Validation();

		$validation->fromArray($data);

		return $validation;
	}
}
