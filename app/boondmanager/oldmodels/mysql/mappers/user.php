<?php
/**
 * user.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models;
use BoondManager\Services\Managers;

class User extends Mapper{

	private $db;

	public function __construct($class = null)
	{
		parent::__construct($class);
		$this->db = new Local\Account();
	}

	/**
	 * @param $data
	 * @return Models\User
	 */
	public function map($data)
	{
		if(!$data) return null;
		$entity = new Models\User();

		if(isset($data['USER_VALIDATIONTEMPS'])) {
			$validators = array_filter(explode('|', self::extractData($data, ['USER_VALIDATIONTEMPS'])['USER_VALIDATIONTEMPS']), function ($manager) {
				return $manager != '';
			});
			// TODO : Tin : Attention les validateurs pourront bientôt aussi être des chefs de projets donc on ne pourra plus utiliser getIDProfilFromIDUser
			foreach ($validators as $validator) $entity->groupFieldsAs('timesReportsWorkflow', (new Employee())->map(['ID_PROFIL' => Managers::getEmployeeIdFromUserId($validator)]), $push = true);
		}

		if(isset($data['USER_VALIDATIONFRAIS'])) {
			$validators = array_filter(explode('|', self::extractData($data, ['USER_VALIDATIONFRAIS'])['USER_VALIDATIONFRAIS']), function ($manager) {
				return $manager != '';
			});
			// TODO : Tin : Attention les validateurs pourront bientôt aussi être des chefs de projets donc on ne pourra plus utiliser getIDProfilFromIDUser
			foreach ($validators as $validator) $entity->groupFieldsAs('expensesReportsWorkflow', (new Employee())->map(['ID_PROFIL' => Managers::getEmployeeIdFromUserId($validator)]), $push = true);
		}

		if(isset($data['USER_VALIDATIONABSENCES'])) {
			$validators = array_filter(explode('|', self::extractData($data, ['USER_VALIDATIONABSENCES'])['USER_VALIDATIONABSENCES']), function ($manager) {
				return $manager != '';
			});
			// TODO : Tin : Attention les validateurs pourront bientôt aussi être des chefs de projets donc on ne pourra plus utiliser getIDProfilFromIDUser
			foreach ($validators as $validator) $entity->groupFieldsAs('absencesReportsWorkflow', (new Employee())->map(['ID_PROFIL' => Managers::getEmployeeIdFromUserId($validator)]), $push = true);
		}

		$entity->fromArray($data, $this->retrieveBasic ? $this->db:null);

		return $entity;
	}
}
