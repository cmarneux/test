<?php
/**
 * validation.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class Validation extends Mapper{

	private $managerMapper;

	public function __construct()
	{
		$this->managerMapper = new Employee();
	}

	public function map($data){
		if(!$data) return null;
		// ID_VALIDATION, ID_VALIDATEUR, VAL_DATE, VAL_ETAT, VAL_MOTIF, TAB_VALIDATION.ID_PROFIL, PROFIL_NOM, PROFIL_PRENOM, PROFIL_STATUT
		$validation = new Models\Validation(self::extractData($data, ['ID_VALIDATION', 'VAL_DATE', 'VAL_ETAT', 'VAL_MOTIF']));
		$validation->realValidator = $this->managerMapper->map(self::extractData($data, [
			'ID_PROFIL', 'PROFIL_NOM', 'PROFIL_PRENOM', 'PROFIL_STATUT'
		]));

		return $validation;
	}
}
