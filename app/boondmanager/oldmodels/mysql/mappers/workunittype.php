<?php
/**
 * workunittype.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\MySQL\Mappers;

use Wish\Mapper;
use BoondManager\Models;

class WorkUnitType extends Mapper{
	/**
	 * @param $data array containing the following keys : 'TYPEH_REF', 'TYPEH_TYPEACTIVITE', 'TYPEH_NAME'
	 * @return Models\WorkUnitType
	 */
	public function map($data)
	{
		if(!$data) return null;
		$workUnitType = new Models\WorkUnitType(self::extractData($data, [
			'TYPEH_REF',
			'TYPEH_TYPEACTIVITE',
			'TYPEH_NAME'
		]));

		return $workUnitType;
	}
}
