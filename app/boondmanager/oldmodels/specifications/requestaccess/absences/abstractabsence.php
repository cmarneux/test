<?php
/**
 * abstractabsence.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Absences;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Absence;
use BoondManager\Lib\RequestAccess;

abstract class AbstractAbsence extends AbstractSpecificationItem{
    /**
     * get the absence from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Absence|null
     */
    public function getAbsence($request){
        if($request->data instanceof Absence) return $request->data;
        else return null;
    }
}
