<?php
/**
 * UserHaveSearchAccess.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Absences;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on absences
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Absences
 */
class HaveSearchAccess extends AbstractAbsence{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		//~ On autorise la recherche uniquement pour les administrateurs... (sinon ce serait très compliqué de vérifier les droits pour chaque contrat retourné)
		return $user->isGod() || $user->isManager();
		//~ return true;
	}
}
