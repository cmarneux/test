<?php
/**
 * agencybelongstouser.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\RequestAccess;

class AgencyBelongsToUser extends AbstractSpecificationItem{

	private $fieldName;

	public function __construct($fieldName = 'ID_SOCIETE')
	{
		$this->fieldName = $fieldName;
	}

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{

		$user = $request->user;
		$data = $request->data;
		if (!$data) return false;

		if(! $data instanceof HasAgencyInterface)
			throw new \Exception('class '.get_class($data).' must implement HasManagerInterface');

		return $user->isGod() || in_array($data->getAgencyID(), $user->getAgencyIDs());
	}

}
