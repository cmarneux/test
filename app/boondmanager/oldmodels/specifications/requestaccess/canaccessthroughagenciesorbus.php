<?php
/**
 * CanAccessThroughAgenciesOrBUs.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class CanAccessThroughAgenciesOrBUs extends AbstractSpecificationItem{

    /**
     * check if the user match the specification
     * @param RequestAccess $request
     * @throws \Exception if the resource is a wrong type
     * @return bool
     */
    public function isSatisfiedBy($request){

        $user = $request->user;

        return $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_RESOURCES)
            || $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_RESOURCES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])
                && (new UserBUsContainHR)->or_(new UserBUsContainManager)->isSatisfiedBy($request)
            || $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_RESOURCES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])
                && (new AgencyBelongsToUser)->isSatisfiedBy($request);
    }
}
