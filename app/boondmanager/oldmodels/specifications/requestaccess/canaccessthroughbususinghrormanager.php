<?php
/**
 * canseebususinghrormanager.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class CanAccessThroughBUsUsingHRorManager extends AbstractSpecificationItem{

    /**
     * check if the user match the specification
     * @param RequestAccess $request
     * @throws \Exception if the resource is a wrong type
     * @return bool
     */
    public function isSatisfiedBy($request){

        $user = $request->user;

        return $user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES) && (new UserBUsContainHR)->or_(new UserBUsContainManager)->isSatisfiedBy($request);
    }
}
