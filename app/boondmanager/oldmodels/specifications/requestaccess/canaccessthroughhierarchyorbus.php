<?php
/**
 * userx.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;

class CanAccessThroughHierarchyOrBUs extends AbstractSpecificationItem{

    /**
     * check if the user match the specification
     * @param RequestAccess $request
     * @throws \Exception if the resource is a wrong type
     * @return bool
     */
    public function isSatisfiedBy($request){

        return (new HRinUserHierarchy)->or_(new ManagerInUserHierarchy)->or_(new CanAccessThroughBUsUsingHRorManager)->isSatisfiedBy($request);
    }
}
