<?php
/**
 * canreadthroughgroupagenciesbupoles.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class CanReadThroughGroupAgenciesBUPoles extends AbstractSpecificationItem
{
	private $module;

	/**
	 * CanReadThroughGroupAgenciesBUPoles constructor.
	 * @param string $module
	 */
	public function __construct($module)
	{
		$this->module = $module;
	}

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->user;

		if($user->isGod()) return true;

		return $user->hasRight(BM::RIGHT_SHOWALL, $this->module)
		    || $user->hasRight(BM::RIGHT_SHOWALL, $this->module, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])
		        && (new UserBUsContainHR)->or_(new UserBUsContainManager)->or_( new PoleBelongsToUser )->isSatisfiedBy($request)
		    || $user->hasRight(BM::RIGHT_SHOWALL, $this->module, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])
		        && (new AgencyBelongsToUser)->isSatisfiedBy($request);
	}
}
