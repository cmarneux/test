<?php
/**
 * canreadwritethroughbupoles.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class CanReadWriteThroughBUPoles extends AbstractSpecificationItem{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		$user = $request->getUser();

		if($user->isGod()) return true;

		return $user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES)
			&& (new UserBUsContainHR)->or_(new UserBUsContainManager)->or_( new PoleBelongsToUser )->isSatisfiedBy($request);
	}
}
