<?php
/**
 * canreadwritethroughhierarchybupoles.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */


namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;

class CanReadWriteThroughHierarchyBUPoles extends AbstractSpecificationItem{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request){

		return (new CanReadWriteThroughBUPoles)->or_(new ManagerInUserHierarchy )->isSatisfiedBy($request);
	}
}

