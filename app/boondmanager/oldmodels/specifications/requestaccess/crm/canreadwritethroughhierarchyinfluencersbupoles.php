<?php
/**
 * canreadwritethroughhierarchyinfluencersbupoles.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\CRM;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\CanReadWriteThroughHierarchyBUPoles;

class CanReadWriteThroughHierarchyInfluencersBUPoles extends AbstractSpecificationItem{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;

		return (new CanReadWriteThroughHierarchyBUPoles)->or_(new IsUnderMyInfluence )->isSatisfiedBy($request);
	}
}
