<?php
/**
 * isUnderMyInfluence.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\CRM;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Company;
use BoondManager\Models\Contact;
use BoondManager\Lib\RequestAccess;

class IsUnderMyInfluence extends AbstractSpecificationItem{

	/**
	 * check if the user match the specification
	 * @param RequestAccess $request
	 * @throws \Exception if the resource is a wrong type
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{

		$user = $request->user;
		$data = $request->data;

		if($user->isGod()) return true;

		if($data instanceof Company || $data instanceof Contact){
			if($data->influencers)
				foreach($data->influencers as $inf){
					if($inf->id == $user->getEmployeeId()) return true;
				}
		}

		return false;
	}
}
