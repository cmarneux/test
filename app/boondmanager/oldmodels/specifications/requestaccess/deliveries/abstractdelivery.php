<?php
/**
 * abstractdelivery.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Deliveries;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Models\Delivery;
use BoondManager\Lib\RequestAccess;

abstract class AbstractDelivery extends AbstractSpecificationItem{
	/**
	 * get the delivery from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Delivery|null
	 */
	public function getDelivery($request){
		if($request->data instanceof Delivery) return $request->data;
		else return null;
	}

	/**
	 * @param CurrentUser $user
	 * @param string $module
	 * @return bool
	 */
	private function readBUModule(CurrentUser $user, $module){
		return
			$user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES)
			|| $user->hasRight(BM::RIGHT_WRITEALL, $module) && $user->hasRight(BM::RIGHT_SHOWALL, $module, [BM::RIGHT_SHOW_ALL_BUS, BM::RIGHT_SHOW_ALL_AGENCIES_BUS]);
	}

	/**
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function readBUProject(CurrentUser $user){
		return $this->readBUModule($user, BM::MODULE_PROJECTS);
	}

	/**
	 * @param CurrentUser $user
	 * @param string $module
	 * @param int $idAgency
	 * @return bool
	 */
	private function readAgencyModule(CurrentUser $user, $module, $idAgency){

		return
			$user->hasRight(BM::RIGHT_SHOWALL, $module, BM::RIGHT_SHOW_ALL_GROUP)
			|| (
				$user->hasRight(BM::RIGHT_SHOWALL, $module, [BM::RIGHT_SHOW_ALL_AGENCIES, BM::RIGHT_SHOW_ALL_AGENCIES_BUS])
				&& $user->isMyAgency($idAgency)
			);
	}

	/**
	 * @param CurrentUser $user
	 * @param Delivery $delivery
	 * @return bool
	 */
	protected function readAgencyProject(CurrentUser $user, Delivery $delivery){
		return $this->readAgencyModule($user, BM::MODULE_PROJECTS, $delivery->ID_SOCIETE);
	}
}
