<?php
/**
 * myProject.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Deliveries;

use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class MyProject extends AbstractDelivery{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$delivery = $this->getDelivery($request);

		if($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $user->isGod()) return true;

		// verification manager
		$test =  $user->isMyManager($delivery->ID_USER);
		// verification BUs
		$test |= $user->isMyBusinessUnitManager($delivery->ID_USER) && $this->readBUProject($user);
		// vérification agences
		$test |= $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_PROJECTS) && $this->readAgencyProject($user, $delivery);

		$test |= !$test && $delivery->PRJ_TYPE == BM::PROJECT_TYPE_PACKAGE && isset($delivery->ID_PROFILCDP) && in_array($user->getEmployeeId(), Tools::unserializeArray($delivery->ID_PROFILCDP));

		return $test;
	}
}
