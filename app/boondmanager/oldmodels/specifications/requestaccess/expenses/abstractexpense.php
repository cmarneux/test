<?php
/**
 * abstractexpense.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Expenses;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Expense;
use BoondManager\Lib\RequestAccess;

abstract class AbstractExpense extends AbstractSpecificationItem{
    /**
     * get the expense from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Expense|null
     */
    public function getExpense($request){
        if($request->data instanceof Expense) return $request->data;
        else return null;
    }
}
