<?php
/**
 * hasindifferentperimeter.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class HasIndifferentPerimeter extends AbstractSpecificationItem{
	private $module;
	public function __construct($module)
	{
		$this->module = $module;
	}

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->getUser();

		return $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
		    || $user->hasRight(BM::RIGHT_SHOWALL, $this->module, BM::RIGHT_LEVEL_2)
		    || (new CanReadWriteThroughHierarchyBUPoles)->isSatisfiedBy($object);
	}
}
