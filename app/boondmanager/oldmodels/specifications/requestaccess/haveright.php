<?php
/**
 * userhaveright.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\CurrentUser;

/**
 * Class UserHaveRight
 * @package BoondManager\Models\Specifications\RequestAccess
 */
class HaveRight extends AbstractSpecificationItem{

	/**
	 * @var string right name
	 */
	private $right;
	/**
	 * @var string module name
	 */
	private $module;
	/**
	 * @var int level
	 */
	private $level;

	public function __construct($right, $module = null, $level = null){
		$this->right = $right;
		$this->module = $module;
		$this->level = $level;
	}

	/**
	 * check if the object match the specification
	 * @param mixed $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		/**
		 * @var CurrentUser $user
		 */
		$user = $object->getUser();

		return $user->hasRight($this->right, $this->module, $this->level);
	}
}
