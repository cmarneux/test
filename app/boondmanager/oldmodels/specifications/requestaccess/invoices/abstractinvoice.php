<?php
/**
 * abstractinvoice.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Invoices;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Invoice;
use BoondManager\Lib\RequestAccess;

abstract class AbstractInvoice extends AbstractSpecificationItem{
	/**
	 * get the invoice from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Invoice|null
	 */
	public function getInvoice($request){
		if($request->data instanceof Invoice) return $request->data;
		else return null;
	}
}
