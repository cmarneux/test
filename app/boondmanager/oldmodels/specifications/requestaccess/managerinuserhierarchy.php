<?php
/**
 * managerinuserhierarchy.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Models\JSONApiObjectTrait;
use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\RequestAccess;

class ManagerInUserHierarchy extends AbstractSpecificationItem{

	/**
	* check if the user match the specification
	* @param RequestAccess $request
	* @throws \Exception if the resource is a wrong type
	* @return bool
	*/
	public function isSatisfiedBy($request){

		$user = $request->user;
		$resource = $request->data;

		if($user->isGod()) return true;

		if(!$resource) return false;

		if(! $resource instanceof HasManagerInterface)
			throw new \Exception('class '.get_class($resource).' must implement HasManagerInterface');

		return $user->isMyManager( $resource->getManagerID(), true);
	}
}
