<?php
/**
 * UserMustBeAuthenticated.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Specification that check the user is not a visitor
 * @package BoondManager\Models\Specifications\RequestAccess
 */
class MustBeAuthenticated extends AbstractSpecificationItem{

    /**
     * check if the object match the specification
     * @param RequestAccess $request
     * @return bool
     */
    public function isSatisfiedBy($request)
    {
        return $request->user->getAccount()->level != BM::USER_TYPE_VISITEUR;
    }
}
