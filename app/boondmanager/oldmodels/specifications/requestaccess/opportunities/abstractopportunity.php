<?php
/**
 * abstractopportunity.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Opportunities;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Services\CurrentUser;
use BoondManager\Models\Opportunity;
use BoondManager\Lib\RequestAccess;

abstract class AbstractOpportunity extends AbstractSpecificationItem{
	/**
	 * get the opportunity from the request
	 * @param RequestAccess $request
	 * @return \BoondManager\Models\Opportunity|null
	 */
	public function getOpportunity($request)
	{
		if($request->data instanceof Opportunity) return $request->data;
		else return null;
	}

	/**
	 * @param CurrentUser $user
	 * @param string $module
	 * @return bool
	 */
	private function readBUModule(CurrentUser $user, $module){
		return
			$user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES)
			|| $user->hasRight(BM::RIGHT_WRITEALL, $module) && $user->hasRight(BM::RIGHT_SHOWALL, $module, [BM::RIGHT_SHOW_ALL_BUS, BM::RIGHT_SHOW_ALL_AGENCIES_BUS]);
	}

	/**
	 * @param CurrentUser $user
	 * @return bool
	 */
	protected function readBUOpportunity(CurrentUser $user){
		return $this->readBUModule($user, BM::MODULE_OPPORTUNITIES);
	}

	/**
	 * @param CurrentUser $user
	 * @param string $module
	 * @param int $idAgency
	 * @return bool
	 */
	private function readAgencyModule(CurrentUser $user, $module, $idAgency){

		return
			$user->hasRight(BM::RIGHT_SHOWALL, $module, BM::RIGHT_SHOW_ALL_GROUP)
			|| (
				$user->hasRight(BM::RIGHT_SHOWALL, $module, [BM::RIGHT_SHOW_ALL_AGENCIES, BM::RIGHT_SHOW_ALL_AGENCIES_BUS])
				&& $user->isMyAgency($idAgency)
			);
	}

	/**
	 * @param CurrentUser $user
	 * @param Opportunity $opportunity
	 * @return bool
	 */
	protected function readAgencyOpportunity(CurrentUser $user, Opportunity $opportunity){
		return $this->readAgencyModule($user, BM::MODULE_OPPORTUNITIES, $opportunity->getAgencyID());
	}
}
