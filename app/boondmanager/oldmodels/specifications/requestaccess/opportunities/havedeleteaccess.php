<?php
/**
 * havedeleteaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Opportunities;
use BoondManager\Lib\RequestAccess;

/**
 * Indicate if the user have the right to delete an opportunity
 * @package BoondManager\Models\Specifications\User
 */
class HaveDeleteAccess extends AbstractOpportunity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		$user = $object->user;

		if($user->isGod()) return true;
		if(!$user->isManager()) return false;

		// TODO: Implement isSatisfiedBy() method. from Loic's spec
		return true;
	}
}
