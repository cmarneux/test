<?php
/**
 * UserHaveSearchAccess.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Opportunities;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on positionings
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Opportunities
 */
class HaveSearchAccess extends AbstractOpportunity{

	/**
	* check if the object match the specification
	* @param RequestAccess $request
	* @return bool
	*/
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;

		return $user->isManager() && $user->hasAccess(BM::MODULE_OPPORTUNITIES);
	}
}
