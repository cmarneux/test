<?php
/**
 * mycrm.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Opportunities;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class MyCRM extends AbstractOpportunity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$opportunity = $this->getOpportunity($request);

		if($user->isGod() || $user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)) return true;

		else if( (new MyOpportunity())->isSatisfiedBy($request) && !$user->hasRight(BM::RIGHT_MASKDATA, BM::MODULE_OPPORTUNITIES)) return true;

		return $user->isMyManager($opportunity->CRMRESP_IDPROFIL, $isProfileID = true)
			|| ($user->isMyBusinessUnitManager($opportunity->CRMRESP_IDPROFIL, $isProfileID = true) && $user->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES))
			//|| (on my influence)
			|| $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_CRM) && (
				$user->hasRight(BM::RIGHT_SHOW_ALL_GROUP, BM::MODULE_CRM)
				|| $user->isMyAgency($opportunity->CRMRESP_IDSOCIETE) && $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])
				|| $user->isMyBusinessUnitManager($opportunity->CRMRESP_IDPROFIL, $isProfileID = true) && $user->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])
			);
	}
}
