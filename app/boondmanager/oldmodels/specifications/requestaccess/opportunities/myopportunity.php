<?php
/**
 * myOpportunity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Opportunities;

use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

class MyOpportunity extends AbstractOpportunity{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$opportunity = $this->getOpportunity($request);

		if($user->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $user->isGod()) return true;

		else if(!$opportunity->isVisible()) return false;

		// verification manager
		$test =  $user->isMyManager($opportunity->ID_PROFIL, $isProfileID = true);
		// verification BUs
		$test |= $user->isMyBusinessUnitManager($opportunity->ID_PROFIL, $isProfileID = true) && $this->readBUOpportunity($user);
		// vérification agences
		$test |= $user->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_OPPORTUNITIES) && $this->readAgencyOpportunity($user, $opportunity);

		return $test;
	}
}
