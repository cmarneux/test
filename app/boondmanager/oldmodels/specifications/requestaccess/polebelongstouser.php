<?php
/**
 * polebelongstouser.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\RequestAccess;

class PoleBelongsToUser extends AbstractSpecificationItem{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;
		$data = $request->data;
		if(!$data) return false;

		if(! $data instanceof HasPoleInterface){
			throw new \Exception('class '.get_class($data).' must implement HasHrManagerInterface');
		}

		return $user->isGod() || in_array($data->getPoleID(), $user->getPolesIDs());
	}
}
