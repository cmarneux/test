<?php
/**
 * abstracttime.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Times;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Models\Time;
use BoondManager\Lib\RequestAccess;

abstract class AbstractTime extends AbstractSpecificationItem{
    /**
     * get the time from the request
     * @param RequestAccess $request
     * @return \BoondManager\Models\Time|null
     */
    public function getTime($request){
        if($request->data instanceof Time) return $request->data;
        else return null;
    }
}
