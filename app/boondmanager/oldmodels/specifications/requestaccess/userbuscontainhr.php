<?php
/**
 * userbuscontainhr.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\Models\HasHrManagerInterface;
use BoondManager\Lib\RequestAccess;

class UserBUsContainHR extends AbstractSpecificationItem{

	private $fieldName;
	private $isProfilId;

	public function __construct($fieldName = 'ID_RESPRH', $isProfilId = false)
	{
		$this->fieldName = $fieldName;
		$this->isProfilId = $isProfilId;
	}

	/**
	* check if the user match the specification
	* @param RequestAccess $request
	* @throws \Exception if the resource is a wrong type
	* @return bool
	*/
	public function isSatisfiedBy($request){

		$user = $request->user;
		$resource = $request->data;

		if($user->isGod()) return true;

		if(!$resource) return false;

		if(! $resource instanceof HasHrManagerInterface){
			throw new \Exception('class '.get_class($resource).' must implement HasHrManagerInterface');
		}

		return $user->isMyBusinessUnitManager( $resource->getHrManagerID(), true);
	}
}
