<?php
/**
 * UserHasAccessToModule.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Services\BM;
use BoondManager\Lib\RequestAccess;

/**
 * Specification that check the user is not a visitor
 * @package BoondManager\Models\Specifications\RequestAccess
 */
class UserHasAccessToModule extends AbstractSpecificationItem{

    /**
     * @var string a module key
     */
    private $module;

    /**
     * UserHasAccessToModule constructor.
     * @param string $module
     */
    public function __construct($module){
        $this->module = $module;
    }

    /**
     * check if the object match the specification
     * @param RequestAccess $request
     * @return bool
     */
    public function isSatisfiedBy($request)
    {
        return $request->user->isGod() || $request->user->hasAccess($this->module);
    }
}
