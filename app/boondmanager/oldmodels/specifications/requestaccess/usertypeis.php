<?php
/**
 * UserTypeIs.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess;

use Wish\Specifications\AbstractSpecificationItem;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserTypeIs : check the user type is valid
 * @package BoondManager\Models\Specifications\RequestAccess
 */
class UserTypeIs extends AbstractSpecificationItem{

	/**
	 * @var array user type
	 */
	private $allowedTypes;

	/**
	 * UserTypeIs constructor.
	 * @param int|array $allowedTypes a list of allowed type ( @see BM::USER_TYPE )
	 */
	public function __construct($allowedTypes){
		if(!is_array($allowedTypes)) $allowedTypes = [$allowedTypes];
		$this->allowedTypes = $allowedTypes;
	}

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		return in_array($request->user->getAccount()->level, $this->allowedTypes);
	}
}
