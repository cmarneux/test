<?php
/**
 * havesearchaccess.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\OldModels\Specifications\RequestAccess\Validations;

use BoondManager\APIs\Validations\Specifications\AbstractValidations;
use BoondManager\Lib\RequestAccess;

/**
 * Class UserHaveSearchAccess
 *
 * Indicate if the user can do a search on absences
 *
 * @package BoondManager\Models\Specifications\RequestAccess\Validations
 */
class HaveSearchAccess extends AbstractValidations{

	/**
	 * check if the object match the specification
	 * @param RequestAccess $request
	 * @return bool
	 */
	public function isSatisfiedBy($request)
	{
		$user = $request->user;

		if($user->isGod()) return true;
		if( !$user->isManager() ) return false;

		//TODO faire la verif dès que les specs sont pretes
		return true;
	}
}
