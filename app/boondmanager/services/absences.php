<?php
/**
 * absences.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Models\Absence;
use BoondManager\Models\AbsencesAccount;
use BoondManager\Models\AbsencesQuota;
use BoondManager\Models\AbsencesReport;
use BoondManager\Models\Employee;
use BoondManager\Models\Time;
use BoondManager\Models\WorkUnitType;
use BoondManager\OldModels\Filters;
use Wish\Tools;

/**
 * Class Absences
 * @package BoondManager\Models\Services
 */
class Absences{

	/**
	 * Handle the research MySQL
	 *
	 * @param Filters\Search\Absences $filter
	 * @return SearchResult
	 */
	public static function search(Filters\Search\Absences $filter)
	{
		throw new \Exception('a migrer');
		$db = new \BoondManager\Databases\Local\Absence();
		return $db->search($filter);
	}

	/**
	 * Get all validated at least once absences of a resource on the given period
	 *
	 * @param integer $resource identifiant du profil
	 * @param integer $agency
	 * @param string $startDate Y-M-d
	 * @param string $endDate Y-M-d
	 * @return array of absences or null if none found
	 */
	public static function getListeLignesDemandesAbsences($resource, $agency, $startDate, $endDate) {
		throw new \Exception('a migrer');
		return (new \BoondManager\Databases\Local\Absence())->getListeLignesDemandesAbsences($resource, $agency, $startDate, $endDate);
	}


	//On récupère la période de chaque décompte en fonction de la date de la demande d'absences et des absences posées
	public static function returnPeriodesDecomptesAvailableAndSetQuotasList(AbsencesReport $tabData, $nowDate = '', $withNewAbsencesAsked = false) {
		if(!$nowDate) $nowDate = date('Y-m-d');

		$periodeDecompte = array();
		foreach($tabData->agency->absencesQuotas as $decompte) {
			$absPeriode = self::getPeriodeDecompteFromMonth($decompte->period, $nowDate);
			$periodeDecompte[$decompte->workUnitType->reference.'-'.$absPeriode] = [$decompte->workUnitType->reference, $absPeriode];

			if($withNewAbsencesAsked && $tabData->absencesPeriods) {
				foreach($tabData->absencesPeriods as $demande) {
					if($demande->workUnitType->reference == $decompte->workUnitType->reference) {
						$absPeriode = self::getPeriodeDecompteFromPeriod($decompte->period, $demande->startDate);
						$periodeDecompte[$decompte->workUnitType->reference.'-'.$absPeriode] = [$decompte->workUnitType->reference, $absPeriode];
					}
				}
			}
		}

		//On récupère uniquement les quotas existants sur la période ou les absences posées
		$tabQuotas = [];
		foreach(self::getAllQuotas($tabData->resource->id) as $quota) {
			$pDates = [$quota->getStartDate(), $quota->getEndDate()];

			$addQuota = false;
			if ($nowDate >= $pDates[0] && $nowDate <= $pDates[1])
				$addQuota = true;
			else {
				if ($withNewAbsencesAsked && $tabData->absencesPeriods) {
					foreach ($tabData->absencesPeriods as $demande) {
						if ($demande->workUnitType->reference == $quota->workUnitType->reference && $demande->startDate >= $pDates[0] && $demande->enDate <= $pDates[1]) {
							$addQuota = true;
							break;
						}
					}
				}
			}

			if ($addQuota) {
				$tabQuotas[] = $quota;
				$periodeDecompte[$quota->workUnitType->reference . '-' . $quota->getStartDate()] = [$quota->workUnitType->reference, $quota->getStartDate()];
			}
		}

		return [$periodeDecompte, $tabQuotas];
	}

	/**
	 * @param $profilID
	 * @param array $period
	 * @return AbsencesQuota[]
	 */
	public static function getAllQuotas($profilID, $period = []){
		$db = Local\Absence::instance();
		$data = $db->getAllQuotas($profilID, $period);

		return Mapper\Absence::fromAllQuotas($data);
	}

	/**
	 * @param int $profilID
	 * @param array $period
	 * @return Absence[]
	 */
	public static function getListeAbsencesPoses($profilID, $period = []) {
		$db = Local\Absence::instance();
		$data = $db->getListeAbsencesPoses($profilID, $period);

		return Mapper\Absence::fromListeAbsencesPoses($data);
	}

	/**
	 * @param int $profilID
	 * @param array $period
	 * @return Time[]
	 */
	public static function getListeAbsencesPrises($profilID, $period = []) {
		$db = Local\Absence::instance();
		$data = $db->getListeAbsencesPrises($profilID, $period);

		return Mapper\Absence::fromListeAbsencesPrises($data);
	}


	static public function getPeriodeDecompteFromPeriod($periodStartDecompte = 'JanuaryToDecember', $nowDate = '') {
		$monthStartDecompte = Tools::reverseMapData($periodStartDecompte, AbsencesQuota::ACCOUNTSPERIOD);

		return self::getPeriodeDecompteFromMonth($monthStartDecompte, $nowDate);
	}

	/**
	 * get a start date for a period
	 * @param int $monthStartDecompte
	 * @param string $nowDate
	 * @return string
	 */
	static public function getPeriodeDecompteFromMonth($monthStartDecompte = 1, $nowDate = '') {
		if($nowDate == '') $nowDate = date('Y-m-d');
		list($year, $month, $day) = explode('-', $nowDate);

		if($month >= $monthStartDecompte) $yearDecompte = $year; else $yearDecompte = $year-1;
		if($monthStartDecompte < 10) $monthDecompte = '0'.$monthStartDecompte; else $monthDecompte = $monthStartDecompte;

		return $yearDecompte.'-'.$monthDecompte.'-01';
	}

	static public function getPeriodeDecompteFromDate($dateStartDecompte, $nowDate = '') {
		list($year, $month, $day) = explode('-', $dateStartDecompte);
		return self::getPeriodeDecompteFromMonth(intval($month), $nowDate);
	}

	/**
	 * @param Employee $entity
	 * @param Absence[] $absAsked v6 -> LISTE_ABSENCESPOSEES
	 * @param Time[] $absTaken v6 -> LISTE_ABSENCES
	 * @param AbsencesQuota[] $quotas v6-> LISTE_QUOTAS
	 * @param bool $allActualDecompte
	 * @param array $period
	 * @return AbsencesQuota[]
	 */
	public static function getAllAccounts($entity, $absAsked, $absTaken, $quotas, $allActualDecompte = true, $period = [])
	{
		$aaID = 1;

		//On récupère la période actuelle par défaut des décomptes
		$nowDate = date('Y-m-d');
		$startMonth = date('Y-m-d', mktime(0,0,0,date('n'),1,date('Y')));

		$periodeDecompte = self::getPeriodeDecompteFromPeriod($entity->agency->defaultAbsencesAccountsPeriod, $nowDate);

		//On construit le tableau des périodes actuelles pour chaque type de décompte
		$referencePeriods = [];
		foreach($entity->agency->absencesQuotas as $decompte) {
			$referencePeriods[ $decompte->workUnitType->reference ] = self::getPeriodeDecompteFromMonth(Tools::reverseMapData($decompte->period, AbsencesQuota::ACCOUNTSPERIOD), $nowDate); //TODO: verifier que $decompte->period est bien une chaine de la forme 'January2December'
		}

		//Mode calcul des absences consommées & demandées
		//true = Calcul 1 : Consommées jusque N-1 et Demandées = Consommées jusque N-1 + Demandées à parir de N
		//false = Calcul 2 : Total Consommées et Total Demandées

		//On construit la liste des types d'absences
		$liste_type = [];
		foreach($entity->agency->workUnitTypes as $typeh)
			if($typeh->activityType == WorkUnitType::ACTIVITY_ABSENCE)
				$liste_type[$typeh->reference] = isset($referencePeriods[$typeh->reference])
					? [$typeh->name, $referencePeriods[$typeh->reference] ]
					: [$typeh->name, $periodeDecompte];

		//On ajoute dans la liste des décomptes la somme des absences posées par société, par type et par période
		$liste_decomptes = [];
		foreach($absAsked as $absence) {
			$ref = $absence->workUnitType->reference;

			$abs_periode = isset($referencePeriods[$ref])
				? self::getPeriodeDecompteFromDate($referencePeriods[$ref], $absence->startDate)
				: self::getPeriodeDecompteFromPeriod($entity->agency->defaultAbsencesAccountsPeriod, $absence->startDate);

			/** @var AbsencesAccount $abs */
			$abs = isset($liste_decomptes[$abs_periode][$absence->absencesReport->agency->id][$ref])
				? $liste_decomptes[$abs_periode][$absence->absencesReport->agency->id][$ref]
				: new AbsencesAccount([
					'id' => 'auto'.$aaID++,
					'period' => Mapper\Absence::getPeriodFromINTQUO_PERIODE($abs_periode),
					'year' => Mapper\Absence::getYearFROMINTQUO_PERIODE($abs_periode)
				]);

			$calcul = $absence->absencesReport->agency->absencesCalculationMethod;

			if(!$abs->amountAcquiredAsked) $abs->amountAcquiredAsked = 0;
			if(!$abs->workUnitType) $abs->workUnitType = $absence->workUnitType;

			if(!$abs->agency) $abs->agency = $absence->absencesReport->agency;

			if($calcul == 0) {
				if($absence->startDate >= $startMonth)
					$abs->amountAcquiredAsked += $absence->duration;
			} else
				$abs->amountAcquiredAsked += $absence->duration;

			$liste_decomptes[$abs_periode][$absence->absencesReport->agency->id][$ref] = $abs;
		}

		//On ajoute dans la liste des décomptes la somme des absences prises par société, par type et par période
		foreach($absTaken as $absence) {
			$ref = $absence->workUnitType->reference;
			$abs_periode = isset($referencePeriods[$absence['TYPEH_REF']]) ?
				self::getPeriodeDecompteFromDate($referencePeriods[$ref], $absence->startDate) :
				self::getPeriodeDecompteFromPeriod($entity->agency->defaultAbsencesAccountsPeriod, $absence->startDate);

			/** @var AbsencesAccount $abs */
			$abs = isset($liste_decomptes[$abs_periode][$absence->timesReport->agency->id][$ref])
				? $liste_decomptes[$abs_periode][$absence->timesReport->agency->id][$ref]
				: new AbsencesAccount([
					'id' => 'auto'.$aaID++,
					'period' => Mapper\Absence::getPeriodFromINTQUO_PERIODE($abs_periode),
					'year' => Mapper\Absence::getYearFROMINTQUO_PERIODE($abs_periode)
				]);

			if(!$abs->amountAcquiredAsked) $abs->amountAcquiredAsked = 0;
			if(!$abs->amountAcquiredUsed) $abs->amountAcquiredUsed = 0;
			if(!$abs->workUnitType) $abs->workUnitType = $absence->workUnitType;
			if(!$abs->agency) $abs->agency = $absence->timesReport->agency;

			if($absence->timesReport->agency->absencesCalculationMethod == 0) {
				if($absence->startDate < $startMonth) {
					$abs->amountAcquiredAsked += $absence->duration;
					$abs->amountAcquiredUsed += $absence->duration;
				}
			} else
				$abs->amountAcquiredUsed += $absence->duration;

			$liste_decomptes[$abs_periode][$absence->timesReport->agency->id][$ref] = $abs;
		}


		//On ajoute dans la liste des décomptes les quotas définis pour le consultant par société, par type et par période
		foreach($quotas as $quota) {

			/** @var AbsencesAccount $abs */
			$abs = isset($liste_decomptes[$quota->getStartDate()][$quota->agency->id][$quota->workUnitType->reference])
				? $liste_decomptes[$quota->getStartDate()][$quota->agency->id][$quota->workUnitType->reference]
				: new AbsencesAccount([
					'id' => 'auto'.$aaID++,
					'period' => Mapper\Absence::getPeriodFromINTQUO_PERIODE($quota->getStartDate()),
					'year' => Mapper\Absence::getYearFROMINTQUO_PERIODE($quota->getStartDate())
				]);

			if(!$abs->workUnitType) $abs->workUnitType = $quota->workUnitType;
			if(!$abs->agency) $abs->agency = $quota->agency;

			$abs->amountAcquired = $quota->amountAcquired;
			$abs->amountBeingAcquired = $quota->amountBeingAcquired;
			$abs->comments = $quota->comments;
			$abs->automaticDescription = $quota->automaticDescription;
			$abs['usePose'] = 1;

			$liste_decomptes[$quota->getStartDate()][$quota->agency->id][$quota->workUnitType->reference] = $abs;
		}

		//On ajoute dans la liste des décomptes les quotas par défaut pour la société actuelle du consultant, par type et pour la période actuelle
		if($entity->userId) {
			foreach($entity->agency->absencesQuotas as $decompte) {
				$ref = $decompte->workUnitType->reference;

				if(!$entity->workUnitTypesAllowedForAbsences)
					$absAllow = true;
				else {
					$absAllow = false;
					$tabAllowAbsences = $entity->workUnitTypesAllowedForAbsences;
					foreach($tabAllowAbsences as $allowAbs)
						if($allowAbs[0] == $ref && $allowAbs[1] == $entity->agency->id) {
							$absAllow = true;
							break;
						}
				}

				if($absAllow && $decompte->quotaAcquired != 0) {

					$abs = isset($liste_decomptes[$referencePeriods[$ref]][$entity->agency->id][$ref])
						? $liste_decomptes[$referencePeriods[$ref]][$entity->agency->id][$ref]
						: new AbsencesQuota(['id' => 'auto'.$aaID++]);

					if(!$abs->workUnitType) $abs->workUnitType = $decompte->workUnitType;
					if(!$abs->agency) $abs->agency = $entity->agency;
					if(!$abs->amountAcquired) $abs->amountAcquired = $decompte->quotaAcquired;

					$liste_decomptes[$referencePeriods[$ref]][$entity->agency->id][$ref] = $abs;
				}
			}
		}

		$simpleList = [];

		//On termine tous les calculs de décompte
		// WARNING : attention à la complexité ! O(~n³) -> en fait on scan toutes les cases du tableau liste_decomptes, donc c'est la complexité du code précédent x2
		foreach($liste_decomptes as $periode => $pDecompte) {
			foreach($pDecompte as $idsociete => $sDecompte) {
				foreach($sDecompte as $ref => $decompte) {
					/** @var AbsencesAccount $decompte */
					if(!$decompte->amountBeingAcquiredUsed) $decompte->amountBeingAcquiredUsed = 0;
					if(!$decompte->amountAcquired) $decompte->amountAcquired = 0;
					if(!$decompte->amountBeingAcquired) $decompte->amountBeingAcquired = 0;
					if(!$decompte->deltaAcquiredAsked) $decompte->deltaAcquiredAsked = 0;
					if(!$decompte->deltaBeingAcquiredAsked) $decompte->deltaBeingAcquiredAsked = 0;
					if(!$decompte->usePose) $decompte->usePose = 0;
					if(isset($entity->agency->absencesQuotas)) {
						foreach ($entity->agency->absencesQuotas as $dec) {
							if($dec->workUnitType->reference == $ref && $idsociete == $entity->agency->id) {
								$decompte['TYPED_USEBEINGACQUIRED'] = $dec->useBeingAcquired;
								$decompte['usePose'] = 1;
								break;
							}
						}
					}

					if(!$decompte->amountAcquired) $decompte->amountAcquired = 0;
					if(!$decompte->amountBeingAcquired) $decompte->amountBeingAcquired = 0;
					if(!$decompte->amountAcquiredUsed) $decompte->amountAcquiredUsed = 0;
					if(!$decompte->amountAcquiredAsked) $decompte->amountAcquiredAsked = 0;
					if(!$decompte->deltaAskedAndUsed) $decompte->deltaAskedAndUsed = 0;
					if(!$decompte->comments) $decompte->comments = '';
					if(!$decompte->automaticDescription) $decompte->automaticDescription = [];
					if(!$decompte->workUnitType) $decompte->workUnitType = '';

					if($decompte['TYPED_USEBEINGACQUIRED'] == 1) {
						if($decompte->amountAcquiredUsed > $decompte->amountAcquired) {
							$decompte->amountBeingAcquiredUsed = $decompte->amountAcquiredUsed - $decompte->amountAcquired;
							$decompte->amountAcquiredUsed = $decompte->amountAcquired;
						} else
							$decompte->amountBeingAcquiredUsed = 0;

						if($decompte->amountAcquiredAsked > $decompte->amountAcquired) {
							$decompte->amountBeingAcquiredAsked = $decompte->amountAcquiredAsked - $decompte->amountAcquired;
							$decompte->amountAcquiredAsked = $decompte->amountAcquired;
						} else
							$decompte->amountBeingAcquiredAsked = 0;
					} else {
						$decompte->amountBeingAcquiredUsed = 0;
						$decompte->amountBeingAcquiredAsked = 0;
					}

					$decompte->deltaAcquiredUsed = $decompte->amountAcquired - $decompte->amountAcquiredUsed;
					$decompte->deltaAcquiredAsked = $decompte->amountAcquired - $decompte->amountAcquiredAsked;

					$decompte->deltaBeingAcquiredUsed = $decompte->amountBeingAcquired - $decompte->amountBeingAcquiredUsed;
					$decompte->deltaBeingAcquiredAsked = $decompte->amountBeingAcquired - $decompte->amountBeingAcquiredAsked;

					$decompte->deltaAskedAndUsed = $decompte->amountAcquiredAsked + $decompte->amountBeingAcquiredAsked - $decompte->amountAcquiredUsed - $decompte->amountBeingAcquiredUsed;

					// suppréssion de l'objet si irrélévent
					if($decompte->amountAcquired == 0 &&
						$decompte->amountAcquiredUsed == 0 &&
						$decompte->amountBeingAcquired == 0 &&
						$decompte->amountAcquiredAsked == 0 &&
						$decompte->deltaAcquiredUsed == 0 &&
						$decompte->deltaAcquiredAsked == 0 &&
						$decompte->amountBeingAcquiredUsed == 0 &&
						$decompte->amountBeingAcquiredAsked == 0 &&
						$decompte->deltaBeingAcquiredUsed == 0 &&
						$decompte->deltaBeingAcquiredAsked == 0) {
						unset($decompte);
						if(sizeof($liste_decomptes[$periode][$idsociete]) == 0) unset($liste_decomptes[$periode][$idsociete]);
						if(sizeof($liste_decomptes[$periode]) == 0) unset($liste_decomptes[$periode]);
					}else{
						$simpleList[] = $decompte;
					}
				}
			}
		}

		//var_dump($allActualDecompte, $simpleList);

		/*
		 *  si jamais on veux trier la liste, c'est ici que ça ce passe
		usort($simpleList, function($a, $b){
			/** @var AbsencesAccount $a *
			/** @var AbsencesAccount $b *

		});
		*/

		//Tri les périodes du plus grand au plus petit
		//krsort($liste_decomptes);

		if(!$allActualDecompte) {
			$liste_actualdecomptes = [];
			if(isset($liste_decomptes[$periodeDecompte][$entity->agency->id]))
				$liste_actualdecomptes[] = $liste_decomptes[$periodeDecompte][$entity->agency->id];

			foreach($referencePeriods as $ref => $periode) {
				if(isset($liste_decomptes[$periode][$entity->agency->id]))
					$liste_actualdecomptes[] = $liste_decomptes[$periode][$entity->agency->id];
			}

			return $liste_actualdecomptes;
		} else {
			return $simpleList;
		}
	}

	/**
	 * @param AbsencesReport $entity
	 * @return array
	 */
	public static function getAbsencesWarnings($entity)
	{
		if(!$entity->agency->maskExceedingWarningsWithQuotas) return [];
		$warnings = [];
		foreach ($entity->absencesAccounts as $quota){
			/** @var AbsencesAccount $quota */
			if($quota->deltaAcquiredAsked < 0 || $quota->deltaAcquiredUsed < 0){
				$warnings[] = [
					'code' => BM::WARNING_ABSENCES_MORE_THAN_ACQUIRED,
					'message' => "The number of asked leaves, which type is '{$quota->workUnitType->name}', is higher than the maximum quota allowed !",
					'workUnitType' => [
						'name' => $quota->workUnitType->name
					]
				];
			}
		}

		return $warnings;
	}
}
