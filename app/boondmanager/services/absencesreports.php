<?php
/**
 * absencesreports.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\AbsencesReports\Filters\SaveEntity;
use BoondManager\APIs\AbsencesReports\Filters\SearchAbsencesReports;
use BoondManager\APIs\AbsencesReports\Specifications\HaveReadAccess;
use BoondManager\APIs\AbsencesReports\Specifications\HaveWriteAccess;
use BoondManager\APIs\AbsencesReports\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models;
use BoondManager\Models\AbsencesReport;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use Wish\Models\SearchResult;
use Wish\Tools;

/**
 * Class AbsencesReports
 * @package BoondManager\Models\Services
 */
class AbsencesReports{

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchAbsencesReports $filter
	 * @param bool $includeDetails add list of absences for the each reports
	 * @return SearchResult
	 * @throws \Exception
	 */
	public static function search(SearchAbsencesReports $filter, $includeDetails = false)
	{
		$db = Local\AbsencesReport::instance();
		$result = $db->search($filter);

		$result = Mapper\AbsencesReport::fromSearchResult($result);

		if($includeDetails) {
			foreach ($result->rows as $r) {
				/** @var AbsencesReport $r */
				$data = $db->getAbsenceData($r->id);
				$r->absencesPeriods = $data->absencesPeriods;
				$r->validations= $data->validations;
				$r->files = $data->files; // TODO
			}
		}

		return $result;
	}

	/**
	 * @param $id
	 * @return AbsencesReport
	 */
	public static function get($id)
	{
		$db = Local\AbsencesReport::instance();
		$absence = $db->getAbsenceData($id);

		if(!$absence) return null;
		else $absence = Mapper\AbsencesReport::fromSQL($absence);

		Employees::attachUserConfig($absence->resource);
		$absence->resource->agency = $absence->agency = Agencies::get($absence->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);
		$absence->calculateValidationWorkflow();

		return $absence;
	}

	/**
	 * @param AbsencesReport $entity
	 * @return bool
	 */
	public static function update(AbsencesReport &$entity) {
		$sqlData = Mapper\AbsencesReport::toSQL($entity);

		$db = Local\AbsencesReport::instance();

		if( $db->updateAbsenceData($sqlData, $entity->id)) {
			$entity = self::get($entity->id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param AbsencesReport $entity
	 * @return bool
	 */
	public static function create(AbsencesReport &$entity) {
		$sqlData = Mapper\AbsencesReport::toSQL($entity);

		$db = Local\AbsencesReport::instance();
		if( $id = $db->updateAbsenceData($sqlData)) {
			$entity = self::get($id);
			return true;
		} else {
			return false;
		}
	}

	public static function delete(AbsencesReport $entity)
	{
		$db = Local\AbsencesReport::instance();
		$db->deleteAbsenceData($entity->id);

		//Notification\AbsencesReport::getInstance($entity->resource->id, $entity->toArray(), [])->delete();

		return true;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public static function getApiUri($id)
	{
		return "/absencesreports/$id";
	}

	public static function getRights($timesReport)
	{
		$request = new RequestAccess();
		$request->data = $timesReport;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_ACTIVITIES_EXPENSES, $timesReport);

		foreach(IsActionAllowed::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		$right->addApi('default', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}

	/**
	 * @param SaveEntity $filter
	 * @param AbsencesReport|null $entity
	 * @return AbsencesReport
	 */
	public static function buildFromFilter(SaveEntity $filter, AbsencesReport $entity = null) {
		if(!$entity) $entity = new AbsencesReport();
		$entity->mergeWith($filter->toObject());
		return $entity;
	}

	/**
	 * @param $filter
	 * @return AbsencesReport
	 */
	public static function getNew($filter) {
		$resource = Employees::get($filter->resource->getValue(), Models\Employee::TAB_ABSENCESACCOUNTS);
		Employees::attachUserConfig($resource);

		$agencyID = $filter->agency->getValue();
		if(!$agencyID) $agencyID = $resource->agency->id;

		// overriding employee's agency in the case it concern's an old agency for the user
		$resource->agency = $agency = Agencies::get($agencyID, Models\Agency::TAB_ACTIVITYEXPENSES);

		return new AbsencesReport([
			'id' => 0,
			'creationDate' => date('Y-m-d'),
			'resource' => $resource,
			'agency' => $agency
		]);
	}
}
