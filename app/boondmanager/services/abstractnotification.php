<?php
/**
 * notification.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;
use BoondManager\Lib\Currency;
use Wish\Web;
use Wish\Tools;
use BoondManager\Models\DictionaryEntry;
use BoondManager\Databases\Local;

/**
 * Class Notification
 * @package BoondManager\Lib
 */
class AbstractNotification{

    protected static $typeNotification;
    protected static $id;
    protected static $id_other;
    protected static $web;

    protected static $pushParams;
    /**
     * @deprecated
     */
    protected static $tabSearch;
    /**
     * @deprecated
     */
    protected static $tabReplace;
    protected static $app;
    /** @var array recipients */
    protected static $recipients;
    /**
     * @deprecated
     */
    protected static $tabXMLData;
    /**
     * @deprecated
     */
    protected static $pathXMLFile;

	const
		TYPE_MONEY = 'money',
		TYPE_PERCENTAGE = 'taux',
		TYPE_DATE = 'date',
		TYPE_DATETIME = 'datetime';

    /**
     * get the instance
     * @param int $typeNotification
     * @param int $id
     * @param array $recipients
     * @param int $id_other
     */
    public static function init($typeNotification, $id, $recipients = array(), $id_other = 0) {

        self::$typeNotification = $typeNotification;
        self::$id = $id;
        self::$id_other = $id_other;
        self::$recipients = array();

        if(BM::getCurrentAPI() == BM::API_TYPE_XJWTAPP) {
            die('do migration'); // TODO
            if (Wish_Session::getInstance()->isRegistered('login_apiid')) self::$app = Wish_Session::getInstance()->get('login_apiid'); else ;
        }else self::$app = false;

        $managerBDD = new Local\Manager();
        $userBDD = new Local\Account();
        foreach($recipients as $id_type => $tabManagers) {
            foreach($tabManagers as $idManager) {
                $addManager = false;
                foreach($managerBDD->getSpecificGroupManagers() as $manager)
                    if((!$id_type && $manager['ID_USER'] == $idManager) || ($id_type && $manager['ID_PROFIL'] == $idManager)) {
                        $addManager = true;
                        self::addDestinataire($manager['USER_LANGUE'], $manager['ID_USER']);
                        break;
                    }

                //On n'a pas trouvé le manager on va le chercher dans les USER
                if(!$addManager){
                    $user = ($id_type)? $userBDD->getAccountFromEmployee($idManager) : $userBDD->getAccount($idManager);
                    if($user)
                        self::addDestinataire($userBDD->get('USER_LANGUE'), intval($userBDD->get('ID_USER')));
                }
            }
        }
    }

    /**
     * merge 2 array of recipients.
     * @param array $tabDA
     * @param array $tabDB
     * @return array
     */
    protected static function tabDestinatairesMerge($tabDA = array(), $tabDB = array()) {
        /*
         * Petite note : les clés FALSE & TRUE permettent de savoir si l'ID correspond à un ID_PROFIL ou bien à un ID_USER
         * TODO: nettoyer ça le jour ou l'on se débarassera de ce typage
         */
        $tabDestinataires = [
            false => [], // User ID
            true => [] // Profil ID
        ];
        foreach($tabDA as $type => $dA)
            if(in_array($type, [false,true]))
                foreach($dA as $id)
                    $tabDestinataires[$type][] = $id;
        foreach($tabDB as $type => $dB)
            if(in_array($type, [false,true]))
                foreach($dB as $id)
                    $tabDestinataires[$type][] = $id;

        return $tabDestinataires;
    }

    /**
     * add a recipients
     * @param string $language `fr`,`en`
     * @param int $idManager
     */
    protected static function addDestinataire($language, $idManager) {
        if(!BM::isValidLanguage($language)) $language = BM::DEFAULT_LANGUAGE;
        if(!isset(self::$recipients[$language])) {
            self::$recipients[$language] = [$idManager];
        } else if(!in_array($idManager,self::$recipients[$language])) self::$recipients[$language][] = $idManager;
    }

    /**
     * Check that an element exist in the array
     * @param string|array $key a key or a list of a keys (if array is multi-dim)
     * @param array $tabArray
     * @return array
     * Example:
     *  [`true`, $value] if the value exists
     *  [`false`, ''] if the value does't exists
     */
    protected static function elementExists($key, $tabArray = array()) {
        if(is_array($key)) {
            $curArray = $tabArray;
            $lastKey = array_pop($key);//On dépile le premier élément des index
            foreach($key as $oneKey) {
                $isExist = self::elementExists($oneKey, $curArray);
                if(!$isExist[0]) return [false, ''];
                $curArray = $curArray[$oneKey];
            }
            if(is_array($curArray) && (isset($curArray[$lastKey]) || (is_array($curArray) && array_key_exists($lastKey, $curArray))))
                return [true, $curArray[$lastKey]];
        } else if(isset($tabArray[$key]) || (is_array($tabArray) && array_key_exists($key, $tabArray)))
            return [true, $tabArray[$key]];

        return [false, ''];
    }

    /**
     * add a value that changed to $tabChange
     * @param array $tabChange
     * @param array $tabOldData
     * @param array $tabNewData
     * @param string|array $index a key or a list of key if $tabOldData & $tabNewData are multidims array
     * @param string $dictEntry path to a dict entry
     * @param string $prefix
     * @param array $tabExceptions
     * @param string $valueType
     * @param Currency|null $modDevise
     * @param int $tauxchange
     * @return bool
     */
    protected static function setChangeValue(&$tabChange, $tabOldData, $tabNewData, $index, $dictEntry, $prefix = '', $tabExceptions = array(), $valueType = '', Currency $modDevise = null, $tauxchange = 1) {
        $tabOld = self::elementExists($index, $tabOldData);
        $tabNew = self::elementExists($index, $tabNewData);
        if($tabOld[0] && $tabNew[0] && $tabOld[1] != $tabNew[1]) {
            $oldData = '';
            $newData = '-';
            if(is_array($prefix) && sizeof($prefix) == 2) {
                $oldPrefix = $prefix[0];
                $newPrefix = $prefix[1];
            } else {
                $oldPrefix = $prefix;
                $newPrefix = $prefix;
            }
            switch($valueType) {
                case self::TYPE_PERCENTAGE:
                    $tabOld[1] = round(100*$tabOld[1]-100,2).'%';
                    $tabNew[1] = round(100*$tabNew[1]-100,2).'%';
                    break;
                case self::TYPE_DATE:
                    $tabOld[1] = Tools::convertDateForUI($tabOld[1]);
                    $tabNew[1] = Tools::convertDateForUI($tabNew[1]);
                    break;
                case self::TYPE_DATETIME:
                    $tabOld[1] = Tools::convertDateTimeForUI($tabOld[1]);
                    $tabNew[1] = Tools::convertDateTimeForUI($tabNew[1]);
                    break;
                case self::TYPE_MONEY:
                    $tabOld[1] = $modDevise->getAmountForUI($tabOld[1], $tauxchange);
                    $tabNew[1] = $modDevise->getAmountForUI($tabNew[1], $tauxchange);
                    break;
            }

            if(!in_array($tabOld[1], $tabExceptions)) $oldData = $oldPrefix.$tabOld[1];
            if($oldData != '') {
                if(!in_array($tabNew[1], $tabExceptions)) $newData = $newPrefix.$tabNew[1];
                $tabChange[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
                return true;
            }
        }
        return false;
    }

    protected static function buildDictEntryForField($field, $oldvalue, $newvalue){
        return  Dictionary::prepareEntry('resources.notifications.specials.changeField', [
            'fieldname' => Dictionary::prepareEntry($field),
            'oldvalue' => $oldvalue,
            'newvalue' => $newvalue
        ]);
    }

	/**
	* detect change in 2 array and populate $tabChange
	* @param array $tabChange
	* @param array $tabOldData
	* @param array $tabNewData
	* @param string|array $index
	* @param string $dictEntry a dictionary key
	* @param array $mapping
	* @return bool
	*/
	protected static function setChangeList(&$tabChange, $tabOldData, $tabNewData, $index, $dictEntry, $mapping) {
		$tabOld = self::elementExists($index, $tabOldData);
		$tabNew = self::elementExists($index, $tabNewData);
		if($tabOld[0] && $tabNew[0] && $tabOld[1] != $tabNew[1]) {
			$oldData = Tools::mapData($tabOld[1], $mapping);
			$newData = Tools::mapData($tabNew[1], $mapping);
			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
			return true;
		}
		return false;
	}

	/**
	* detect change in 2 array and populate $tabChange
	* @param array $tabChange
	* @param array $tabOldData
	* @param array $tabNewData
	* @param string|array $index
	* @param string $dictEntry a dictionary key
	* @param boolean $id_type is an id_profil or id_user
	* @return bool
	*/
	protected static function setChangeManager(&$tabChange, $tabOldData, $tabNewData, $index, $dictEntry, $id_type = false) {
		$tabOld = self::elementExists($index, $tabOldData);
		$tabNew = self::elementExists($index, $tabNewData);
		if($tabOld[0] && $tabNew[0] && $tabOld[1] != $tabNew[1]) {
			$managerBDD = new Local\Manager();
			// conversion du type en profil ID
			$oldID = (!$id_type) ? Managers::getEmployeeIdFromUserId($tabOld[1]) : $tabOld[1];
			$newID = (!$id_type) ? Managers::getEmployeeIdFromUserId($tabNew[1]) : $tabNew[1];
			// récupération du manager puis de son nom
			$oldManager = ($m = $managerBDD->getBasicManager($oldID)) ? $m->getManagerName() : '?';
			$newManager = ($m = $managerBDD->getBasicManager($newID)) ? $m->getManagerName() : '?';

			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldManager, $newManager);
			return true;
		}
		return false;
	}

	/**
	* detect change in 2 array and populate $tabChange
	* @param array $tabChange
	* @param array $tabOldData
	* @param array $tabNewData
	* @param string|array $tabNewIndex
	* @param string $dictEntry a dictionary key
	* @return bool
	*/
	protected static function setChangeSociete(&$tabChange, $tabOldData, $tabNewData, $tabNewIndex, $dictEntry) {
		$tabOld = self::elementExists($tabNewIndex, $tabOldData);
		$tabNew = self::elementExists($tabNewIndex, $tabNewData);
		if($tabOld[0] && $tabNew[0] && $tabOld[1] != $tabNew[1]) {
			$agencyDB = new Local\Agency();
			$oldAgency = ($m = $agencyDB->getBasicAgency($tabOld[1] )) ? $m->name : '?';
			$newAgency = ($m = $agencyDB->getBasicAgency( $tabNew[1] )) ? $m->name : '?';
			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldAgency, $newAgency);
			return true;
		}
		return false;
	}

	/**
	 * detect change in 2 array and populate $tabChange
	 * @param array $tabChange
	 * @param array $tabOldData
	 * @param array $tabNewData
	 * @param string|array $index
	 * @param string $dictEntry a dictionary key
	 * @return bool
	 */
	protected static function setChangeInfluencers(&$tabChange, $tabOldData, $tabNewData, $index, $dictEntry)
	{
		$tabOld = self::elementExists($index, $tabOldData);
		$tabNew = self::elementExists($index, $tabNewData);
		if ($tabOld[0] && $tabNew[0] && $tabOld[1]['ID_PROFIL'] != $tabNew[1]['ID_PROFIL']) {
			$tabOldManager = array();
			$tabNewManager = array();
			$managerBDD    = new Local\Manager();
			foreach ($tabOld[1] as $oldI)
				$oldManager[] = ($m = $managerBDD->getBasicManager($oldI['ID_PROFIL'])) ? $m->getManagerName() : '?';
			foreach ($tabNew[1] as $newI)
				$newManager[] = ($m = $managerBDD->getBasicManager($newI['ID_PROFIL'])) ? $m->getManagerName() : '?';

			$tabChange[] = self::buildDictEntryForField($dictEntry, implode(',', $tabOldManager), implode(',', $tabNewManager));
			return true;
		}
		return false;
	}

    /**
     * Create a notification in the Action Table
     * @param string $title Notification title
     * @param DictionaryEntry $message a message to translate
     * @param int $status Notification Status/Type
     * @param int $state Notification state
     * @param int $type Category/Module linked to the notification
     * @param int $id_parent Parent ID
     * @param int $id_other Another ID that might be required
     * @return int action ID created
     * @throws \Exception
     */
    protected static function save($title, DictionaryEntry $message, $status, $state, $type, $id_parent, $id_other) {
        $text = $message->translate(BM::getLanguage());
        if(!$text) throw new \Exception('Unable to translate $message');

        $actionBDD = new Local\Action();

        //Si la notification provient d'une App, on l'ajoute au titre afin de conserver en BDD son origine // TODO: déplacer ça dans un service ?
        $tabTitle = array();
        if(self::$app) $tabTitle[] = '[[__API'.self::$app.'__]]';
        if($title != '') $tabTitle[] = $title;
        $newID = $actionBDD->newActionData([
            'ACTION_TITRE' => implode(' - ', $tabTitle),
            'ACTION_TEXTE' => $text,
            'ACTION_DATE' => date('Y-m-d H:i:s'),
            'ACTION_STATUT' => $status,
            'ACTION_ETAT' => $state,
            'ACTION_TYPE' => $type,
            'ID_PARENT' => $id_parent,
            'ID_OTHER' => $id_other,
            'ID_RESPUSER' => CurrentUser::instance()->getUserId()
        ]);
        return $newID;
    }

    /**
     * Push a notification to a Node server
     * @param string $title Notification title
     * @param array $message array of parameters to send
     * @param int $statut Status/Type
     * `0` = Notice
     * `1` = BoondManager
     * `2` = Help
     * `3` = Step
     * `4` = Warning
     * `5` = Creation
     * `6` = Modification
     * `7` = Deletion
     * @param boolean $sticky Durée d'apparition de la notification via le Serveur de Push
     * `true` = Permanent notification
     * `false` = Temporary notification
     * @param int|null $index index du tableau de destinataire (les langues donc) à utiliser
     * @param boolean $not_this_manager if `true` then the actual manager does not receive a notification
     * @param array $tabDestinataires Array with ID_USER for the push recipients (only used if not using getInstance())
     * @param bool $app App appelante (à utiliser si on ne passe pas par getInstance)
     * @return bool
     */
    protected static function send($title, $message, $statut, $sticky, $index = null, $not_this_manager = false, $tabDestinataires = array(), $app = false) {
        $f3 = \Base::instance();

        if(!CurrentUser::instance()->isLogged() || !$f3->get('BMNODE.ENABLED'))
            return false;

        if(isset(self::$recipients)) $tabDestinataires = self::$recipients;
        if(isset(self::$app)) $app = self::$app;

        if(!isset(self::$web)) self::$web = new Web(BM::isCustomerInterfaceActive() ? BM::getCustomerInterfaceNodeServer() : $f3->get('BMNODE.SERVER'), $f3->get('BMNODE.SECRET'));
        $webURL = (BM::isCustomerInterfaceActive() ? BM::getCustomerInterfaceNodeURL() : $f3->get('BMNODE.URL')) . '/notification/' . BM::getCustomerCode();
        $payload = array('title' => $title, 'message' => $message, 'sticky' => $sticky, 'type' => (intval($statut) == 0 && $app) ? $statut = 'API_'.$app : $statut);

        foreach($tabDestinataires as $i => $tos) {
            if(!isset($index) || $index == $i) {
                if(isset($payload['utilisateurs']))
                	unset($payload['utilisateurs']);

                if(sizeof($tos) > 0) {
                    $payload['utilisateurs'] = array();

                    foreach($tos as $destinataire)
                    	if($destinataire > 0 && (!$not_this_manager || ($not_this_manager && $destinataire != CurrentUser::instance()->getUserId())) && !in_array($destinataire, $payload['utilisateurs']))
                    		$payload['utilisateurs'][] = $destinataire;

                    if(sizeof($payload['utilisateurs']) > 0)
						self::$web->setUrl($webURL)->post($payload);
                } else
					self::$web->setUrl($webURL)->post($payload);
            }
        }
        return true;
    }

    /**
     * send a create notification
     * @param array $notificationMessage
     * @param string $title
     */
    protected function _create(array $notificationMessage, $title = '') {
        $notificationMessage['action'] = 'create';
        self::send($title, $notificationMessage, 5, false, true);
    }


    /**
     * send an update notification
     * @param array $notificationMessage
     * @param DictionaryEntry|null $actionMessage
     * @param $title
     * @throws \Exception
     */
    protected function _update(array $notificationMessage, DictionaryEntry $actionMessage = null, $title = ''){
        $notificationMessage['action'] = 'update';
        if($actionMessage)
            self::save($title, $actionMessage, 6, 0, self::$typeNotification, self::$id, self::$id_other);

        self::send($title, $notificationMessage, 6, false, true);
    }

    /**
     * send a delete notification
     * @param array $notificationMessage
     * @param DictionaryEntry|null $actionMessage
     * @param $title
     * @throws \Exception
     */
    protected function _delete(array $notificationMessage, DictionaryEntry $actionMessage, $title = '') {
        self::save($title, $actionMessage, 7, 0, self::$typeNotification, self::$id, self::$id_other);
        $notificationMessage['action'] = 'delete';
        self::send($title, $notificationMessage, 7, false, true);
    }

    /**
     * send an addDocument notification
     * @param array $notificationMessage
     * @param DictionaryEntry|null $actionMessage
     * @param $title
     * @throws \Exception
     */
    protected function _addDocument(array $notificationMessage, DictionaryEntry $actionMessage, $title = '') {

        self::save($title, $actionMessage, 5, 0, self::$typeNotification, self::$id, self::$id_other);
        $notificationMessage['action'] = 'addDocument';
        self::send($title, $notificationMessage, 5, false, true);
    }

    /**
     * send a deleteDocument notification
     * @param array $notificationMessage
     * @param DictionaryEntry|null $actionMessage
     * @param $title
     * @throws \Exception
     */
    protected function _deleteDocument(array $notificationMessage, DictionaryEntry $actionMessage, $title = '') {
        self::save($title, $actionMessage, 7, 0, self::$typeNotification, self::$id, self::$id_other);
        $notificationMessage['action'] = 'deleteDocument';
        self::send($title, $notificationMessage, 7, false, true);
    }

    /**
     * @param int $type
     * @param string $baction
     * @param DictionaryEntry $actionMessage
     * @param array $notificationMessage
     * @param string $title
     * @param bool $sticky
     * @throws \Exception
     */
    protected function saveANDsend($type, $baction, DictionaryEntry $actionMessage = null, $notificationMessage = null, $title = '', $sticky = false) {

        if($actionMessage) self::save($title, $actionMessage, $type, 0, self::$typeNotification, self::$id, self::$id_other);
        if($notificationMessage){
            $notificationMessage['action'] = $baction;
            self::send($title, $notificationMessage, $type, $sticky, true);
        }
    }
}
