<?php
/**
 * notification.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\Lib\Currency;
use BoondManager\Models\Action;
use BoondManager\Models\Agency;
use BoondManager\Models\Company;
use BoondManager\Models\Employee;
use BoondManager\Models\Notification;
use BoondManager\Services;
use Wish\Models\Model;
use Wish\Models\ModelJSONAPI;
use Wish\Web;
use Wish\Tools;
use BoondManager\Models\DictionaryEntry;
use BoondManager\Databases\Local;

/**
 * Class Notification
 * @package BoondManager\Lib
 */
abstract class AbstractNotification2{

	const ACTION_NOTIFICATION_TYPE = '__UNDEFINED__';
	const ACTION_NOTIFICATION_DICT_PREFIX = 'default.notifications.actions';

	const MODULE = '__UNDEFINED__';

	const STATUS_CREATE = 5;
	const STATUS_UPDATE = 6;
	const STATUS_DELETE = 7;
	const STATUS_SHARE = 8;

	const
		TYPE_MONEY = 'money',
		TYPE_PERCENTAGE = 'taux',
		TYPE_DATE = 'date',
		TYPE_DATETIME = 'datetime';

	private static $web;

	protected static function getAppID(){
		if(BM::getCurrentAPI() == BM::API_TYPE_XJWTAPP) {
			die('do migration'); // TODO
			if (Wish_Session::getInstance()->isRegistered('login_apiid'))
				return Wish_Session::getInstance()->get('login_apiid');
		}
		return false;
	}

	protected static function buildTitle($title){
		if($appID = self::getAppID()) {
			return '[[__API'.$appID.'__]] - '.$title;
		} else {
			return $title;
		}
	}

	/**
	 * add a value that changed to $tabChange
	 * @param array $changesList
	 * @param mixed $old
	 * @param mixed $new
	 * @param string $dictEntry path to a dict entry
	 * @param string $prefix
	 * @param array $tabExceptions
	 * @param string $valueType
	 * @param Currency|null $modDevise
	 * @param int $tauxchange
	 * @return DictionaryEntry
	 */
	private static function setChangeValue(&$changesList, $old, $new, $dictEntry, $prefix = '', $tabExceptions = array(), $valueType = '', Currency $modDevise = null, $tauxchange = 1) {

		if($old != $new) {
			$oldData = '';
			$newData = '-';
			if(is_array($prefix) && sizeof($prefix) == 2) {
				$oldPrefix = $prefix[0];
				$newPrefix = $prefix[1];
			} else {
				$oldPrefix = $prefix;
				$newPrefix = $prefix;
			}
			switch($valueType) {
				case self::TYPE_PERCENTAGE:
					$old = round(100*$old-100,2).'%';
					$new = round(100*$new-100,2).'%';
					break;
				case self::TYPE_DATE:
					$old = Tools::convertDateForUI($old);
					$new = Tools::convertDateForUI($new);
					break;
				case self::TYPE_DATETIME:
					$old = Tools::convertDateTimeForUI($old);
					$new = Tools::convertDateTimeForUI($new);
					break;
				case self::TYPE_MONEY:
					$old = $modDevise->getAmountForUI($old, $tauxchange);
					$new = $modDevise->getAmountForUI($new, $tauxchange);
					break;
			}

			if(!is_null($old) && !in_array($old, $tabExceptions)) $oldData = $oldPrefix.$old;
			if($oldData != '') {
				if(!is_null($new) && !in_array($new, $tabExceptions)) $newData = $newPrefix.$new;
				$changesList[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
			}
		}
		return null;
	}

	protected static function detectChangedValue(&$changesList, Model $entity, $index, $dictEntry, $prefix = '', $tabExceptions = array()){
		if( $change = $entity->getChangedValue($index)) {
			return self::setChangeValue($changesList, $change['old'], $change['new'], $dictEntry, $prefix, $tabExceptions);
		}
		return null;
	}

	protected static function detectChangedRelationship(&$changesList, Model $entity, $index, $dictEntry) {
		if($change = $entity->getChangedValue($index)) {
			if($change['old'] instanceof ModelJSONAPI) $change['old'] = $change['old']->getReference();
			if($change['new'] instanceof ModelJSONAPI) $change['new'] = $change['new']->getReference();
			return self::setChangeValue($changesList, $change['old'], $change['new'], $dictEntry);
		}
		return null;
	}

	protected static function detectChangedDate(&$changesList, Model $entity, $index, $dictEntry, $prefix = '', $tabExceptions = array()){
		if( $change = $entity->getChangedValue($index)) {
			return self::setChangeValue($changesList, $change['old'], $change['new'], $dictEntry, $prefix, $tabExceptions, self::TYPE_DATE);
		}
		return null;
	}

	protected static function detectChangedAmount(&$changesList, Model $entity, $index, $dictEntry, Currency $devise, $change, $prefix = '', $tabExceptions = array()){
		if( $changeData = $entity->getChangedValue($index)) {
			return self::setChangeValue($changesList, $changeData['old'], $changeData['new'], $dictEntry, $prefix, $tabExceptions, self::TYPE_MONEY, $devise, $change);
		}
		return null;
	}

	protected static function buildDictEntryForField($field, $oldvalue, $newvalue){
		return  Dictionary::prepareEntry('resources.notifications.specials.changeField', [
			'fieldname' => Dictionary::prepareEntry($field),
			'oldvalue' => $oldvalue,
			'newvalue' => $newvalue
		]);
	}

	/**
	 * @param $item
	 * @param $priority
	 * @param $title
	 * @param $typeOf
	 * @return Action
	 */
	protected static function buildAction($item, $priority, $title, $typeOf) {

		return new Action([
			'title' => $title,
			'priority' => $priority,
			'typeOf' => $typeOf,
			'state' => 0,
			'dependsOn' => $item,
			'mainManager' => Managers::getBasic(CurrentUser::instance()->getEmployeeId()),
		]);
	}

	/**
	* detect change in 2 array and populate $tabChange
	* @param array $tabChange
	* @param Model $entity
	* @param string|array $index
	* @param string $dictEntry a dictionary key
	* @param array $mapping
	* @return bool
	*/
	protected static function detectChangedList(&$tabChange, Model $entity, $index, $dictEntry, $mapping) {
		if( $changeData = $entity->getChangedValue($index)) {
			$oldData = Tools::mapData($changeData['old'], $mapping);
			$newData = Tools::mapData($changeData['new'], $mapping);
			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
			return true;
		}
		return false;
	}

	protected static function detectChangedManager(&$tabChange, Model $entity, $index = 'mainManager', $dictEntry = 'default.sqlLabels.mainManager') {
		if( $changeData = $entity->getChangedValue($index)) {
			$oldData = $changeData['old'] instanceof Employee ? $changeData['old']->getFullName() : '?';
			$newData = $changeData['new'] instanceof Employee ? $changeData['new']->getFullName() : '?';
			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
			return true;
		}
		return false;
	}

	protected static function detectChangedInfluencers(&$tabChange, Model $entity, $index = 'influencers', $dictEntry = 'default.sqlLabels.influencers') {
		if( $changeData = $entity->getChangedValue($index)) {
			$oldData = $changeData['old'] instanceof Employee ? $changeData['old']->getFullName() : '?';
			$newData = $changeData['new'] instanceof Employee ? $changeData['new']->getFullName() : '?';
			$tabOldManager = [];
			$tabNewManager = [];
			foreach($oldData as $entry) {
				$tabOldManager[] = $entry->getFullName();
			}
			foreach($newData as $entry) {
				$tabNewManager[] = $entry->getFullName();
			}
			$tabChange[] = self::buildDictEntryForField($dictEntry, implode(',', $tabOldManager), implode(',', $tabNewManager));
			return true;
		}
		return false;
	}

	protected static function detectChangedAgency(&$tabChange, Model $entity, $index = 'agency', $dictEntry = 'default.sqlLabels.agency') {
		if( $changeData = $entity->getChangedValue($index)) {
			$oldData = $changeData['old'] instanceof Agency ? $changeData['old']->name : '?';
			$newData = $changeData['new'] instanceof Agency ? $changeData['new']->name : '?';
			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
			return true;
		}
		return false;
	}

	protected static function detectChangedCompany(&$tabChange, Model $entity, $index = 'company', $dictEntry = 'default.sqlLabels.cmopany') {
		if( $changeData = $entity->getChangedValue($index)) {
			$oldData = $changeData['old'] instanceof Company ? $changeData['old']->name : '?';
			$newData = $changeData['new'] instanceof Company ? $changeData['new']->name : '?';
			$tabChange[] = self::buildDictEntryForField($dictEntry, $oldData, $newData);
			return true;
		}
		return false;
	}

	/**
	 * detect change in 2 array and populate $tabChange
	 * @param array $tabChange
	 * @param array $tabOldData
	 * @param array $tabNewData
	 * @param string|array $index
	 * @param string $dictEntry a dictionary key
	 * @return bool
	 */
	protected static function setChangeInfluencers(&$tabChange, $tabOldData, $tabNewData, $index, $dictEntry)
	{
		$tabOld = self::elementExists($index, $tabOldData);
		$tabNew = self::elementExists($index, $tabNewData);
		if ($tabOld[0] && $tabNew[0] && $tabOld[1]['ID_PROFIL'] != $tabNew[1]['ID_PROFIL']) {
			$tabOldManager = array();
			$tabNewManager = array();
			$managerBDD    = new Local\Manager();
			foreach ($tabOld[1] as $oldI)
				$oldManager[] = ($m = $managerBDD->getBasicManager($oldI['ID_PROFIL'])) ? $m->getManagerName() : '?';
			foreach ($tabNew[1] as $newI)
				$newManager[] = ($m = $managerBDD->getBasicManager($newI['ID_PROFIL'])) ? $m->getManagerName() : '?';

			$tabChange[] = self::buildDictEntryForField($dictEntry, implode(',', $tabOldManager), implode(',', $tabNewManager));
			return true;
		}
		return false;
	}

	/**
	 * @param Action $action
	 * @return Action|false|null
	 */
	protected static function saveAction(Action $action) {
		return Actions::create($action);
	}

	/**
	 * @param Model $item
	 * @param $type
	 * @param string $title
	 * @param array $recipientsCC
	 */
	protected static function sendNotification(Model $item, $type, $title = '', array $recipientsCC = []) {
		$notification = static::buildNotification($item, $type, $title, $recipientsCC);

		self::notify($notification, true);
	}

	protected static function buildNotification($item, $type, $title = '', $recipientsCC = []) {

		$notification = new Notification($item, $type);
		$notification->setTitle($title);

		if($recipientsCC) {
			$managers = Managers::getManagers($recipientsCC);
			foreach($managers as $m) {
				$notification->addRecipient($m);
			}
		}

		return $notification;
	}

	/**
	 * Push a notification to a Node server
	 * @param Notification $notification
	 * @param int|null $index index du tableau de destinataire (les langues donc) à utiliser
	 * @param boolean $not_this_manager if `true` then the actual manager does not receive a notification
	 * @param array $tabDestinataires Array with ID_USER for the push recipients (only used if not using getInstance())
	 * @param bool $app App appelante (à utiliser si on ne passe pas par getInstance)
	 * @return bool
	 * @internal param string $title Notification title
	 * @internal param array $message array of parameters to send
	 * @internal param int $statut Status/Type
	 * `0` = Notice
	 * `1` = BoondManager
	 * `2` = Help
	 * `3` = Step
	 * `4` = Warning
	 * `5` = Creation
	 * `6` = Modification
	 * `7` = Deletion
	 * @internal param bool $sticky Durée d'apparition de la notification via le Serveur de Push
	 * `true` = Permanent notification
	 * `false` = Temporary notification
	 */
	protected static function notify(Notification $notification, $index = null, $not_this_manager = false, $tabDestinataires = array(), $app = false) {
		$title = $notification->getTitle();
		$message = $notification->toArray();
		$sticky = $notification->isSticky();
		$statut = Tools::mapData($notification->getAction(), [
			Notification::TYPE_CREATE => self::STATUS_CREATE,
			Notification::TYPE_DELETE => self::STATUS_DELETE,
			Notification::TYPE_UPDATE => self::STATUS_UPDATE,
			Notification::TYPE_ADD_DOCUMENT => self::STATUS_CREATE,
			Notification::TYPE_DELETE_DOCUMENT => self::STATUS_DELETE
		]);
		$f3 = \Base::instance();

		if(!CurrentUser::instance()->isLogged() || !$f3->get('BMNODE.ENABLED'))
			return false;

		$tabDestinataires = $notification->getRecipients();
		$app = self::getAppID();

		if(!isset(self::$web)) self::$web = new Web(BM::isCustomerInterfaceActive() ? BM::getCustomerInterfaceNodeServer() : $f3->get('BMNODE.SERVER'), $f3->get('BMNODE.SECRET'));
		$webURL = (BM::isCustomerInterfaceActive() ? BM::getCustomerInterfaceNodeURL() : $f3->get('BMNODE.URL')) . '/notification/' . BM::getCustomerCode();
		$payload = array('title' => $title, 'message' => $message, 'sticky' => $sticky, 'type' => (intval($statut) == 0 && $app) ? $statut = 'API_'.$app : $statut);

		foreach($tabDestinataires as $i => $tos) {
			// FIXME
			if(!isset($index) || $index == $i) {
				if(isset($payload['utilisateurs']))
					unset($payload['utilisateurs']);

				if(sizeof($tos) > 0) {
					$payload['utilisateurs'] = array();

					foreach($tos as $destinataire)
						if($destinataire > 0 && (!$not_this_manager || ($not_this_manager && $destinataire != CurrentUser::instance()->getUserId())) && !in_array($destinataire, $payload['utilisateurs']))
							$payload['utilisateurs'][] = $destinataire;

					if(sizeof($payload['utilisateurs']) > 0)
						self::$web->setUrl($webURL)->post($payload);
				} else
					self::$web->setUrl($webURL)->post($payload);
			}
		}
		return true;
	}

	/**
	 * send a create notification
	 * @param array $notificationMessage
	 * @param string $title
	 * @deprecated
	 */
	protected function _create(array $notificationMessage, $title = '') {
		$notificationMessage['action'] = 'create';
		self::notify($title, $notificationMessage, 5, false, true);
	}


	/**
	 * send an update notification
	 * @param Notification $notificationMessage
	 * @param DictionaryEntry|null $actionMessage
	 * @param $title
	 * @throws \Exception
	 * @deprecated
	 */
	protected function _update(Notification $notificationMessage, DictionaryEntry $actionMessage = null, $title = ''){
		$notificationMessage['action'] = 'update';
		if($actionMessage) {
			self::save($title, $actionMessage, self::STATUS_UPDATE, 0, self::$typeNotification, self::$id, self::$id_other);
		}

		self::notify($title, $notificationMessage, self::STATUS_UPDATE, false, true);
	}

	/**
	 * send a delete notification
	 * @param array $notificationMessage
	 * @param DictionaryEntry|null $actionMessage
	 * @param $title
	 * @throws \Exception
	 * @deprecated
	 */
	protected function _delete(array $notificationMessage, DictionaryEntry $actionMessage, $title = '') {
		self::save($title, $actionMessage, 7, 0, self::$typeNotification, self::$id, self::$id_other);
		$notificationMessage['action'] = 'delete';
		self::notify($title, $notificationMessage, 7, false, true);
	}

	/**
	 * send an addDocument notification
	 * @param array $notificationMessage
	 * @param DictionaryEntry|null $actionMessage
	 * @param $title
	 * @throws \Exception
	 * @deprecated
	 */
	protected function _addDocument(array $notificationMessage, DictionaryEntry $actionMessage, $title = '') {

		self::save($title, $actionMessage, 5, 0, self::$typeNotification, self::$id, self::$id_other);
		$notificationMessage['action'] = 'addDocument';
		self::notify($title, $notificationMessage, 5, false, true);
	}

	/**
	 * send a deleteDocument notification
	 * @param array $notificationMessage
	 * @param DictionaryEntry|null $actionMessage
	 * @param $title
	 * @throws \Exception
	 * @deprecated
	 */
	protected function _deleteDocument(array $notificationMessage, DictionaryEntry $actionMessage, $title = '') {
		self::save($title, $actionMessage, 7, 0, self::$typeNotification, self::$id, self::$id_other);
		$notificationMessage['action'] = 'deleteDocument';
		self::notify($title, $notificationMessage, 7, false, true);
	}

	/**
	 * @param int $type
	 * @param string $baction
	 * @param DictionaryEntry $actionMessage
	 * @param array $notificationMessage
	 * @param string $title
	 * @param bool $sticky
	 * @throws \Exception
	 * @deprecated
	 */
	protected function saveANDsend($type, $baction, DictionaryEntry $actionMessage = null, $notificationMessage = null, $title = '', $sticky = false) {

		if($actionMessage) self::save($title, $actionMessage, $type, 0, self::$typeNotification, self::$id, self::$id_other);
		if($notificationMessage){
			$notificationMessage['action'] = $baction;
			self::notify($title, $notificationMessage, $type, $sticky, true);
		}
	}

	protected static function getActionParent($item){
		return $item;
	}

	protected static function initRecipients($item, $additionalRecipients = []){
		return $additionalRecipients;
	}

	private function getNotificationID(){
		if( self::ACTION_NOTIFICATION_TYPE != static::ACTION_NOTIFICATION_TYPE) return static::ACTION_NOTIFICATION_TYPE;
		else throw new \Exception('ACTION_NOTIFICATION_TYPE is undefined');
	}

	private function getActionDictPrefix($append = null){
		return $append ? static::ACTION_NOTIFICATION_DICT_PREFIX .'.' .$append : static::ACTION_NOTIFICATION_DICT_PREFIX;
	}

	public static function create(Model $item, $title = ''){
		$recipientsCC = self::initRecipients($item);
		self::sendNotification($item, Notification::TYPE_CREATE, $title, $recipientsCC);
	}

	public static function delete(Model $item, $title = ''){
		$recipientsCC = self::initRecipients($item);
		self::sendNotification($item, Notification::TYPE_DELETE, $title, $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction(self::getActionParent($item), self::STATUS_DELETE, $title, self::getNotificationID());
		$action->text = Dictionary::prepareEntry( self::getActionDictPrefix('delete') )->translate(Services\BM::getLanguage());

		self::saveAction($action);
	}

	public static function addDocument(Model $item, $file, $title = '') {
		$recipientsCC = self::initRecipients($item);
		self::sendNotification($item, Notification::TYPE_ADD_DOCUMENT, $title, $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction(self::getActionParent($item), self::STATUS_CREATE, $title, self::getNotificationID());
		$action->text = Dictionary::prepareEntry( self::getActionDictPrefix('addDocument'), [$file->name])->translate(Services\BM::getLanguage());

		self::saveAction($action);
	}

	public static function deleteDocument(Model $item, $file, $title = ''){
		$recipientsCC = self::initRecipients($item);
		self::sendNotification($item, Notification::TYPE_DELETE_DOCUMENT, $title, $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction(self::getActionParent($item), self::STATUS_DELETE, $title, self::getNotificationID());
		$action->text = Dictionary::prepareEntry( self::getActionDictPrefix('deleteDocument'), [$file->name])->translate(Services\BM::getLanguage());

		self::saveAction($action);
	}

	public static function share(Model $item, $all_recipients = '') {
		$recipientsCC = self::initRecipients($item);
		self::sendNotification($item, Notification::TYPE_DELETE_DOCUMENT, '', $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction(self::getActionParent($item), self::STATUS_SHARE, '', self::getNotificationID());
		$action->text = Dictionary::prepareEntry( self::getActionDictPrefix('share'), [$all_recipients])->translate(Services\BM::getLanguage());

		self::saveAction($action);
	}
}
