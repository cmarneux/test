<?php
/**
 * accounts.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Accounts\Filters\SearchAccounts;
use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;
use BoondManager\Databases\Mapper;
use BoondManager\Models\Account;
use BoondManager\Models\User;
use Wish\Models\SearchResult;

class Accounts {
	/** Search accounts
	 *
	 * @param SearchAccounts $filter
	 * @return SearchResult
	 */
	public static function search(SearchAccounts $filter) {
		$db = new Local\Account();
		$searchResult = $db->searchAccounts($filter);

		return Mapper\Account::fromSearchResult($searchResult);
	}

	/**
	 * @param $id
	 * @return Account|null
	 */
	public static function get($id){
		$db = Local\Account::instance();
		$account = $db->getAccountFromEmployee($id);

		return ($account) ? Mapper\Account::fromSQL($account) : null;
	}

	/**
	 * @param $id
	 * @return Account|null
	 */
	public static function getUser($id){
		$db = Local\Account::instance();
		$account = $db->getAccount($id);

		return ($account) ? Mapper\Account::fromSQL($account) : null;
	}

	/**
	 * Create user
	 * @param Account $account
	 * @return boolean
	 */
	public static function create(Account $account) {
		//transform user to sqlData
		$sqlData = Mapper\Account::toSQL($account);

		$db = new Local\Account();
		$account->id = $db->createAccount($sqlData);
		return $account->id ? true : false;
	}

	/**
	 * @param string $login
	 * @param string $pwd encrypted password
	 * @return \BoondManager\Models\Account|null
	 */
	public static function getFromLoginPwd($login, $pwd){
		if(BM::isCustomerInterfaceActive()){
			$db = new Local\Account();
			$user = $db->getAccountFromLoginAndPwd($login, $pwd);
		}else{
			$db = new BoondManager\Account();
			$user = $db->getAccountFromLoginAndPwd($login, $pwd);
		}

		return ($user) ? Mapper\Account::fromSQL($user) : null;
	}

	/**
	 * @param $id
	 * @return \BoondManager\Models\User|null
	 */
	public static function getLoginAndPwd($id){
		if(BM::isCustomerInterfaceActive()){
			$db = new Local\Account();
			$user = $db->getLoginAndPwd($id);
		}else{
			$db = new BoondManager\Account();
			$user = $db->getLoginAndPwd($id);
		}

		return ($user) ? Mapper\User::fromSQL($user) : null;
	}

	/**
	 * @param int $id user ID
	 * @return bool
	 */
	public static function getIntranetAccess($id){
		$db = new Local\Account();
		return boolval($db->getIntranetAccess($id));
	}

	public static function updateLastConnexionDate(Account &$user, $date = null){
		if(!$date) $date = date('Y-m-d H:i:s');
		$data = [
			'USER' => [
				'USER_LASTCONNEXION' => $date
			]
		];

		if(BM::isCustomerInterfaceActive()) {
			Local\Account::instance()->updateAccount($data, $user->userId, $solR = false);
		} else {
			BoondManager\Account::instance()->updateAccount($data, $user->userId);
		}
	}
}
