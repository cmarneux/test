<?php
/**
 * actions.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\APIs\Actions\Specifications\HaveReadAccess;
use BoondManager\APIs\Actions\Specifications\HaveWriteAccess;
use BoondManager\APIs\Actions\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Action;
use BoondManager\Models\Candidate;
use BoondManager\Models\Contact;
use BoondManager\Models\Employee;
use BoondManager\Models\Invoice;
use BoondManager\Models\Account;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Order;
use BoondManager\Models\Project;
use BoondManager\Models;
use Wish\Tools;
use BoondManager\OldModels\Filters;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Perform all work related to Actions
 * @package BoondManager\Models\Services
 */
class Actions{

	/**
	 * mappert category_id => notification_type
	 * @var array
	 */
	private static $categoriesToNotificationMapper = [
		BM::CATEGORY_OPPORTUNITY => Local\Action::NOTIFICATION_TYPE_OPPORTUNITY,
		BM::CATEGORY_CRM_COMPANY => Local\Action::NOTIFICATION_TYPE_CRM_COMPANY,
		BM::CATEGORY_CRM_CONTACT => Local\Action::NOTIFICATION_TYPE_CRM,
		BM::CATEGORY_CANDIDATE   => Local\Action::NOTIFICATION_TYPE_CANDIDATE,
		BM::CATEGORY_RESOURCE    => Local\Action::NOTIFICATION_TYPE_RESOURCE,
		BM::CATEGORY_PURCHASE    => Local\Action::NOTIFICATION_TYPE_PURCHASE,
		BM::CATEGORY_PROJECT     => Local\Action::NOTIFICATION_TYPE_PROJECT,
		BM::CATEGORY_PRODUCT     => Local\Action::NOTIFICATION_TYPE_PRODUCT,
		BM::CATEGORY_BILLING     => Local\Action::NOTIFICATION_TYPE_BILLING,
		BM::CATEGORY_ORDER       => Local\Action::NOTIFICATION_TYPE_ORDER,
		BM::CATEGORY_APP         => Local\Action::NOTIFICATION_TYPE_APP
	];

	/**
	* get a dictionary for
	* @param int $catID
	* @return array
	*/
	public static function getDictionaryForCategory($catID){
		$f3 = \Base::instance();
		$f3key = 'services.actions.dictionary.'.$catID;

		if(!$f3->get($f3key)) {
			// load default dictionary
			$subDict = self::getCustomTypes($catID);
			$f3->set($f3key, $subDict);
		}
		return $f3->get($f3key);
	}

	/**
	 * get a list of type (without `notification_types`) available for a category. to get a list with notification, @see self::getTypes()
	 * @param int $catID
	 * @param bool $includeNotification
	 * @return array
	 * @throws \Exception
	 */
	public static function getCustomTypes($catID, $includeNotification = true){
		$dictEntry = Dictionary::getActionKeyFromCategoryID($catID);
		if($dictEntry){
			$dict = Dictionary::getDict($dictEntry);
			if(!is_array($dict)) throw new \Exception("no dictionary found for '$dictEntry' entry");

			// removing notification id if asked
			if(!$includeNotification){
				$notificationID = self::getNotificationTypeForCategory($catID);
				$dict = array_filter($dict, function($entry) use ($notificationID){
					return ($entry['id'] != $notificationID);
				});
			}

			return $dict;
		}
		else return [];
	}

	/**
	 * detect a categoryID from a given type
	 * @param int $type
	 * @return int
	 */
	public static function getCategoryFromType($type){
		foreach(array_keys(self::$categoriesToNotificationMapper) as $catID){
			$dict = self::getDictionaryForCategory($catID);
			if(Dictionary::in_dict_array($type, $dict)) return $catID;
		}
	}

	public static function isCollaborative($type){
		$cat = self::getCategoryFromType($type);
		foreach(self::getCustomTypes($cat) as $entry) {
			if($entry['id'] == $type) return isset($entry['collaborative']) ? boolval($entry['collaborative']) : false;
		}
		return false;
	}

	/**
	 * get one or many notification type for one or many categories
	 * @param int|array $categoryID one or many id
	 * @return int|array
	 */
	public static function getNotificationTypeForCategory($categoryID)
	{
		return Tools::mapData($categoryID, self::$categoriesToNotificationMapper);
	}

	/**
	 * get an array with all notification IDS (for all types)
	 * @return array
	 */
	public static function getNotificationsIDs(){
		return array_values(self::$categoriesToNotificationMapper);
	}

	/**
	 * @param int $id
	 * @return Action|false
	 */
	public static function find($id) {
		$filter = new SearchActions();
		$filter->keywords->setValue( Action::buildReference($id) );

		$sql = new Local\Action();
		$result = $sql->searchActions($filter);

		if($result->rows) return Mapper\Action::fromRow($result->rows[0]);
		else return false;
	}

	/**
	* search all actions matching the given filter
	* @param SearchActions $filter
	* @return \Wish\Models\SearchResult|false
	*/
	public static function search(SearchActions $filter)
	{
		$db = Local\Action::instance();
		$actions = $db->searchActions($filter);

		return Mapper\Action::fromSearchResult($actions);
	}

	/**
	 * build canRead & canWrite
	 * @param Action[]|Action $actions
	 */
	public static function attachRights($actions){
		if(!is_array($actions)) $actions = [$actions];

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();

		foreach($actions as $a){
			/** @var Action $a */
			$request->setData($a);

			$a->canReadAction = $readSpec->isSatisfiedBy($request);
			$a->canWriteAction = $writeSpec->isSatisfiedBy($request);
		}
	}

	/**
	 * load an action from its ID
	 * @param $id
	 * @return Action|false
	 */
	public static function get($id)
	{
		$db = Local\Action::instance();
		$action = $db->getObject($id);

		$action = Mapper\Action::fromSQL($action);

		$action->dependsOn = self::getActionParent($action);

		return $action;
	}

	/**
	 * load the parent entity for the action
	 * @param Action$action
	 * @return Candidate|Employee|Contact|Project|Order|Invoice|Opportunity|false
	 * @throws \Exception
	 */
	public static function getActionParent($action){
		switch (get_class($action->dependsOn)){
			case Employee::class: return Employees::get($action->dependsOn->id);
			case Candidate::class: return Candidates::get($action->dependsOn->id);
			case Contact::class: return Contacts::get($action->dependsOn->id, Contact::TAB_ACTIONS);
			case Project::class: return Projects::get($action->dependsOn->id);
			case Order::class: return Orders::get($action->dependsOn->id);
			case Invoice::class: return Invoices::get($action->dependsOn->id);
			case Opportunity::class: return Opportunities::get($action->dependsOn->id);
			default: throw new \Exception('Action entity not handled ('.get_class($action->dependsOn).')');
		}
	}

	/**
	 * get a list of all types associated with the given categories (including notification)
	 * @param array $categories
	 * @return array
	 */
	public static function getTypes(array $categories)
	{
		$types = [];
		foreach($categories as $cKey) {
			$cKey = intval($cKey);
			$dict = self::getDictionaryForCategory($cKey);
			$dict = array_filter($dict, function($value){
				return $value['value'];
			});
			$types = array_merge($types, $dict);
		}

		return $types;
	}

	public static function getTypesIds(array $categories){
		$types = self::getTypes($categories);
		return array_column($types, 'id');
	}

	/**
	 * get a list of all states associated with the given categories
	 * @param array $categories
	 * @return array
	 */
	public static function getStates(array $categories){
		$dict = [];
		foreach($categories as $cKey){
			$cKey = intval($cKey);
			if($dictEntry = Dictionary::getStateKeyFromCategoryID($cKey)) {
				if ($subDict = Dictionary::getDict($dictEntry)) {
					$subDict = array_filter($subDict, function ($value) {
						return $value['value'];
					});
					$dict[ $cKey ] = $subDict;
				}
			}
		}
		return $dict;
	}

	/**
	 * format for the front end the types or the states
	 * @param array $entry
	 * @param array $categories
	 */
	public static function reformatWithCategoriesName(array $entry, array $categories){
		$data = [];
		$catNames = Dictionary::getMapping('main.categories');
		foreach($entry as $catKey => $value){
			foreach($categories as $cKey){
				if($cKey == $catKey){
					$data[] = [$catKey, Tools::mapData($catKey, $catNames), $value];
				}
			}
		}
		return $data;
	}

	public static function getUserCategories(){
		$user = CurrentUser::instance();
		$categories = [];
		if($user->hasAccess(BM::MODULE_OPPORTUNITIES)) $categories[] = BM::CATEGORY_OPPORTUNITY;
		if($user->hasAccess(BM::MODULE_CANDIDATES)) $categories[] = BM::CATEGORY_CANDIDATE;
		if($user->hasAccess(BM::MODULE_PURCHASES)) $categories[] = BM::CATEGORY_PURCHASE;
		if($user->hasAccess(BM::MODULE_RESOURCES)) $categories[] = BM::CATEGORY_RESOURCE;
		if($user->hasAccess(BM::MODULE_PRODUCTS)) $categories[] = BM::CATEGORY_PRODUCT;
		if($user->hasAccess(BM::MODULE_PROJECTS)) $categories[] = BM::CATEGORY_PROJECT;
		if($user->hasAccess(BM::MODULE_MY_APPS)) $categories[] = BM::CATEGORY_APP;
		if($user->hasAccess(BM::MODULE_CRM)) {
			$categories[] = BM::CATEGORY_CRM_CONTACT;
			$categories[] = BM::CATEGORY_CRM_COMPANY;
		}
		if($user->hasAccess(BM::MODULE_BILLING)){
			$categories[] = BM::CATEGORY_BILLING;
			$categories[] = BM::CATEGORY_ORDER;
		}
		return $categories;
	}

	/**
	 * Retrieve all managers
	 *
	 * @param boolean $only_active If `true` it retrieve only managers having an active account
	 * @return Account[]
	 */
	public static function getAllGroupManagers($only_active)
	{
		return Employees::getAllGroupManagers($only_active);
	}

	public static function updateAction(Action &$action, Filters\Profiles\Actions\Save $filter){

		$sqlData = self::transformFilterDataToSQLData($filter);

		$db = Local\Action::instance();
		$db->updateActionData($sqlData, $action->id);

		$action = self::get($action->id);
	}

	/**
	 * @param Filters\Profiles\Actions\Save $filter
	 * @return array
	 */
	private static function transformFilterDataToSQLData(Filters\Profiles\Actions\Save $filter){
		$data = $filter->attributes->getValue();

		$sample = new Action();
		$sqlData = array_merge(
			Tools::reversePublicFieldsToPrivate( $sample->getPublicFieldsMapping(), $data),
			Tools::reversePublicFieldsToPrivate( $sample->getRelationshipMapping(), $filter->relationships->getValue())
		);

		return $sqlData;
	}

	/**
	 * build a new action instance
	 * @param int $category
	 * @return Action
	 */
	public static function buildAction($category)
	{
		$types = self::getCustomTypes($category);
		$type = array_shift($types);
		$action = new Action([
			'id' => 0,
			'typeOf' => $type['id'],
		]);
		$action->mainManager = Managers::getBasic( CurrentUser::instance()->getEmployeeId() );
		return $action;
	}

	public static function createAction(Filters\Profiles\Actions\Save $filter)
	{
		$sqlData = self::transformFilterDataToSQLData($filter);

		if(!isset($sqlData['ACTION_DATE']))
			$sqlData['ACTION_DATE'] = date('Y-m-d H:i:s');

		$db = Local\Action::instance();
		$id = $db->newActionData($sqlData);
		if($id) {
			return self::get($id);
		}else{
			return null;
		}
	}

	public static function create(&$action){

		$sqlData = Mapper\Action::toSQL($action);

		if(!isset($sqlData['ACTION_DATE']))
			$sqlData['ACTION_DATE'] = date('Y-m-d H:i:s');

		$db = Local\Action::instance();
		$id = $db->newActionData($sqlData);
		if($id) {
			return self::get($id);
		}else{
			return null;
		}

	}

	public static function getRights($action) {

		$request = new RequestAccess();
		$request->data = $action;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_ACTIONS, $action);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $a) {
			$spec = new IsActionAllowed($a);
			$right->addAction($a, $spec->isSatisfiedBy($request));
		}

		$right->addApi('attachedFlags', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}
}
