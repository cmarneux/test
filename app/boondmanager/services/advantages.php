<?php
/**
 * advantages.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Advantages\Filters\SaveEntity;
use BoondManager\APIs\Advantages\Filters\SearchAdvantages;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Models\Advantage;
use BoondManager\Models\Agency;
use BoondManager\Models\Employee;
use BoondManager\Models;
use Wish\Cache;
use Wish\Models\SearchResult;
use Wish\Exceptions;

/**
 * Class Advantages
 * @package BoondManager\Models\Services
 */
class Advantages{

	/**
	* Handle the research MySQL
	*
	* @param SearchAdvantages $filter
	* @return SearchResult
	*/
	public static function search(SearchAdvantages $filter)
	{
		$db = Local\Advantage::instance();
		$searchResult = $db->search($filter);

		return Mapper\Advantages::fromSearchResult($searchResult);
	}

	/**
	 * @param int $idagence
	 * @return \BoondManager\Models\AdvantageType[]
	 */
	public static function getAdvantagesTypes($idagence){
		$cache = Cache::instance();
		$key = "services.advantages.getadvantagestypes.$idagence";
		if(!$cache->exists($key)) {
			$db     = Local\Advantage::instance();
			$result = $db->getAdvantagesTypes($idagence);
			$result = Mapper\Advantages::fromAdvantagesTypes($result);
			$cache->set($key, $result, 2);
		}
		return $cache->get($key);
	}

	/**
	 * @param $id
	 * @return Advantage|null
	 */
	public static function get($id)
	{
		$advantagesDB = Local\Advantage::instance();
		$advantage = $advantagesDB->getAdvantageData($id);

		if(!$advantage) return null;

		$advantage = Mapper\Advantages::fromSQL($advantage);

		if($advantage->contract->id){
			$advantage->contract = Contracts::get($advantage->contract->id);
		}

		if($advantage->delivery->id) {
			$advantage->delivery = Deliveries::get($advantage->delivery->id);
		}

		self::attachAgencyConfig($advantage, $advantage->agency->id);

		return $advantage;
	}

	/**
	 * @param Advantage $advantage
	 * @param int $agencyID
	 */
	public static function attachAgencyConfig(Advantage $advantage, $agencyID){
		$advantage->agency = Agencies::get($agencyID, Agency::TAB_RESOURCES);
	}

	/**
	 * @param int $resourceID
	 * @param int $contractID
	 * @param int $projectID
	 * @param int $deliveryID
	 * @return Advantage
	 * @throws Exceptions\Conflict
	 * @throws Exceptions\NotFound
	 */
	public static function newAdvantage($resourceID, $contractID = null, $projectID = null, $deliveryID = null)
	{
		$advantage = new Advantage();
		$advantage->exchangeRate = 1;

		$advantage->resource = Employees::get($resourceID, Employee::TAB_ADMINISTRATIVE);
		if(!$advantage->resource) throw new Exceptions\NotFound('Resource not found');

		$advantage->agency = Agencies::get($advantage->resource->agency->id, Agency::TAB_RESOURCES);
		$advantage->currency = $advantage->agency->currency;
		$advantage->exchangeRateAgency = $advantage->agency->exchangeRate;
		$advantage->currencyAgency = $advantage->agency->currency;

		if( $contractID ){
			$contract = Contracts::getWithAgencyConfig( $contractID );
			if(!$contract) throw new Exceptions\NotFound('Contract not found');
			else $advantage->contract = $contract;

			if($contract->dependsOn->id != $resourceID)
				throw new Exceptions\Conflict('contract not related to the resource');

			/*
			// mapping de la ressource du contrat
			$resource = Employees::get($contract->dependsOn->id, Employee::TAB_ADMINISTRATIVE);

			if(!$resource) throw new Exceptions\NotFound('Resource not found');
			else $advantage->resource = $resource;
			*/

			$advantage->agency = $contract->agency;
			$advantage->currency = $contract->currency;
			$advantage->exchangeRateAgency = $contract->exchangeRateAgency;
			$advantage->currencyAgency = $contract->currencyAgency;
		}else if( $projectID || $deliveryID ){
			if($deliveryID){
				$delivery = Deliveries::get($deliveryID);
				if(!$delivery) throw new Exceptions\NotFound('delivery not found');
				$project = $delivery->project;

				if( !($delivery->dependsOn instanceof Employee) || $advantage->resource->id != $delivery->dependsOn->id)
					throw new Exceptions\Conflict('delivery not related to the resource');

				$advantage->delivery = $delivery;
				$advantage->project = $project;
			}else{
				$project = Projects::get($projectID, Models\Project::TAB_INFORMATION);
				if(!$project) throw new Exceptions\NotFound('Project not found');
				$advantage->project = $project;
			}
			$advantage->currency = $project->currency;
			$advantage->exchangeRateAgency= $project->exchangeRateAgency;
			$advantage->currencyAgency = $project->currencyAgency;
		}

		self::attachAgencyConfig($advantage, $advantage->agency->id);

		return $advantage;
	}

	public static function update(Advantage &$advantage){

		$db = Local\Advantage::instance();

		$sqlData = Mapper\Advantages::toSQL($advantage);

		$db->updateAdvantage($advantage->id, $sqlData);

		$advantage = self::get($advantage->id);

		return true;
	}

	public static function create(Advantage &$advantage){

		$sqlData = Mapper\Advantages::toSQL($advantage);

		$db = Local\Advantage::instance();
		if( $id = $db->create($sqlData) ){
			$advantage = self::get($id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param SaveEntity $filter
	 * @param Advantage $advantage
	 * @return mixed
	 */
	public static function buildFromFilter($filter, $advantage) {
		$advantage->mergeWith( $filter->toObject() );
		$advantage->calculateCostPaid();
		return $advantage;
	}

	/**
	 * @param Advantage $advantage
	 * @return bool
	 */
	public static function delete($advantage)
	{
		if (Local\Advantage::instance()->deleteAdvantage($advantage->id) ) {
			// TODO notification
			return true;
		} else {
			return false;
		}
	}
}
