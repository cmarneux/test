<?php
/**
 * agencies.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Models;
use Wish\Exceptions;
use Wish\Filters\AbstractJsonAPI;
use Wish\Tools;

class Agencies{
	/**#@+
	 * @var string
	 **/
	const
		LOGO_INVOICES = 'img/_clients/invoices/',
		LOGO_ACTIVITYEXPENSES = 'img/_clients/activityExpenses/';
	/**#@- */

	/**
	 * @param int $id
	 * @return string
	 */
	public static function getLogoOnInvoicesPath($id)
	{
		$imgFile = Tools::whichImageWithExtensionExist(self::LOGO_INVOICES . BM::getCustomerCode() . '_' . $id);
		return $imgFile ? self::LOGO_INVOICES . $imgFile : null;
	}

	/**
	 * @param int $id
	 * @return string
	 */
	public static function getLogoOnActivityExpensesPath($id)
	{
		$imgFile = Tools::whichImageWithExtensionExist(self::LOGO_ACTIVITYEXPENSES . BM::getCustomerCode() . '_' . $id);
		return $imgFile ? self::LOGO_ACTIVITYEXPENSES . $imgFile : null;
	}

	/**
	 * @param $id
	 * @param string|int $tab
	 * @return Models\Agency|mixed
	 * @throws Exceptions\DatabaseIntegrity
	 */
	public static function get($id, $tab = Models\Agency::TAB_DEFAULT){
		$db = Local\Agency::instance();
		$agency = $db->getAgency($id, $tab);
		if(!$agency) throw new Exceptions\DatabaseIntegrity('Agency not found '.$id);
		return self::cleanUpAgency(Mapper\Agency::fromSQL($agency, $tab));
	}

	/**
	 * Create an empty entity for creation.
	 *
	 * @return Models\Agency
	 */
	public static function getNew(){
		$user = CurrentUser::instance();
		$object = new Models\Agency([
			'id'                 => 0,
			'state'     		 => true,
			'currency'           => $user->getCurrency(),
			'exchangeRate'       => BM::DEFAULT_EXCHANGERATE,
			'numberOfWorkingDays'=> CurrentUser::instance()->getNumberOfWorkingDays(),
			'chargeFactor'       => CurrentUser::instance()->getChargeFactor(),
			'calendar'            => CurrentUser::instance()->getCalendar(),
			'country'            => CurrentUser::instance()->getCountry()
		]);
		return $object;
	}

	/**
	 * populate an array with default values
	 * @param array $data Tableau à réinitialiser.
	 * @param array $defaultValues the default values
	 */
	private static function resetListeConfiguration(&$data, $defaultValues) {
		foreach($defaultValues as $value) {
			if(isset($data[$value[0]]))
				foreach($data[$value[0]] as $i => $type)
					$data[$value[0]][$i][$value[1]] = 0;
		}
	}

	/**
	 * update agency
	 * @param Models\Agency $agency
	 * @param string|int $tab
	 * @return boolean
	 */
	public static function update(Models\Agency &$agency, $tab = Models\Agency::TAB_DEFAULT) {
		// transform agency to sqlData
		$sqlData = Mapper\Agency::toSQL($agency, $tab);
		$db = Local\Agency::instance();
		return $db->updateAgency($agency->id, $sqlData);
	}

	/**
	 * Create agency
	 * @param Models\Agency $agency
	 * @return boolean
	 */
	public static function create(Models\Agency &$agency) {
		//transform business unit to sqlData
		$sqlData = Mapper\Agency::toSQL($agency);

		$db = Local\Agency::instance();
		$agency->id = $db->createAgency($sqlData);
		if($agency->id) {
			$agency = self::get($agency->id, Models\Agency::TAB_INFORMATION);
			return true;
		} else return false;
	}

	/**
	 * Delete agency
	 * @param int $id
	 * @return boolean
	 */
	public static function delete($id) {
		$db = Local\Agency::instance();
		return $db->isAgencyReducible($id) && $db->deleteAgency($id);
	}

	/**
	 * Build agency from filter
	 * @param AbstractJsonAPI $filter
	 * @param Models\Agency|null $agency
	 * @return Models\Agency
	 */
	public static function buildFromFilter($filter, Models\Agency $agency = null) {
		if(!$agency) $agency = new Models\Agency();
		return self::cleanUpAgency($agency->mergeWith($filter->toObject()));
	}

	/**
	 * Build agency from filter
	 * @param Models\Agency|null $agency
	 * @return Models\Agency
	 */
	public static function cleanUpAgency(Models\Agency $agency) {
		if($agency->advantageTypes) {
			foreach($agency->advantageTypes as $at) {
				switch($at->category) {
					case Models\AdvantageType::CATEGORY_FIXEDAMOUNT:
						$at->participationQuota = null;
						$at->employeeQuota = null;
						break;
					case Models\AdvantageType::CATEGORY_LOAN:
						$at->participationQuota = null;
						$at->agencyQuota = null;
						$at->employeeQuota = null;
						break;
				}
			}
		}
		return $agency;
	}

	/**
	 * @param $id
	 * @return Models\Agency|mixed
	 * @throws \Wish\Exceptions\DatabaseIntegrity
	 */
	public static function getBasic($id){
		$db = Local\Agency::instance();
		$agency = $db->getBasicAgency($id);
		if(!$agency) throw new Exceptions\DatabaseIntegrity('Agency not found '.$id);
		return Mapper\Agency::fromSQL($agency);
	}

	/**
	 * @return Models\Agency[]
	 */
	public static function getAllBasics(){
		$db = Local\Agency::instance();
		$agencies = $db->getAllAgencies();

		return Mapper\Agency::fromDatabaseArray($agencies, Models\Agency::class);
	}

	/**
	 * @param bool $full
	 * @return Models\Agency[]
	 */
	public static function getAllAgencies($full = false){
		if(!$full) return self::getAllBasics();

		$db = Local\Agency::instance();
		$agencies = $db->getAllAgencies(true);

		return Mapper\Agency::fromDatabaseArray($agencies, Models\Agency::class);
	}

	/**
	 * Retrieve all config data from the first agency available
	 *
	 * @see self::exec_config_cmd()
	 * @return Models\Agency|false
	 */
	public static function getFirstAgencyConfig() {
		$db = Local\Agency::instance();
		$agency = $db->getFirstLightAgency();
		return Mapper\Agency::fromSQL($agency);
	}

	/**
	 * @param int $id
	 * @return Models\Agency
	 */
	public static function getAgencyWithConfig($id)
	{
		$db = Local\Agency::instance();
		$agency = $db->getLightAgency($id);

		return Mapper\Agency::fromSQL($agency);
	}

	/**
	 * @param string $customerCode
	 * @return Models\Agency
	 */
	public static function getAgencyWithConfigFromCustomerCode($customerCode)
	{
		$db = Local\Agency::instance();
		$agency = $db->getLightAgencyFromCustomerCode($customerCode);

		return Mapper\Agency::fromSQL($agency);
	}

	/**
	 * @param $value
	 * @return bool|Models\Agency
	 */
	public static function find($value)
	{
		foreach(CurrentUser::instance()->getAgenciesForReassignment() as $agency) {
			if($value == $agency->id) return $agency;
		}
		return false;
	}
}
