<?php
/**
 * apps.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Apps\Filters\SearchApps;
use Wish\Models\SearchResult;
use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;
use BoondManager\Databases\Mapper;
use BoondManager\Models;
use Wish\Tools;
use Wish\Web;

/**
 * Class Apps
 * @package BoondManager\Models\Services
 */
class Apps {
	/** Search apps
	 *
	 * @param SearchApps $filter
	 * @return SearchResult
	 */
	public static function search(SearchApps $filter) {
		$db = new BoondManager\App();
		$searchResult = $db->searchMainApps($filter);
		return Mapper\App::fromSearchResult($searchResult);
	}

	/**
	 * @param bool $fullInfos
	 * @param array $tabIds Références des Apps à filtrer
	 * @return Models\App[]
	 */
	public static function getAllApps($fullInfos = true, $tabIds = []){
		$db = new Local\App();
		$apps = $db->getInstalledApps($fullInfos, $tabIds);

		return \Wish\Mapper::fromDatabaseArray($apps, Models\App::class);
	}

	/**
	 * @param $id
	 * @return Models\App|null
	 */
	public static function get($id){
		$db = new BoondManager\App();
		$app = $db->getMainApp($id);

		return ($app) ? Mapper\App::fromSQL($app) : null;
	}

	/**
	 * @param string $token
	 * @return \BoondManager\Models\App|null
	 */
	public static function getFromToken($token){
		$db = new Local\App();
		$app = $db->getAppFromToken($token);

		return ($app) ? Mapper\App::fromSQL($app) : null;
	}

	/**
	 * @param $token
	 * @return string
	 */
	public static function buildAppToken($token) {
		return Tools::objectID_BM_encode([$token, BM::getCustomerCode()]);
	}

	/**
	 * @return string
	 */
	public static function buildClientToken() {
		return Tools::objectID_BM_encode([BM::getCustomerCode()]);
	}

	/**
	 * @param Models\App $app
	 */
	public static function uninstall($app) {
		$web = new Web();

		//On informe le vendeur de la suppression de son API pour ce client
		$payload = [];
		if($app->version == Models\App::VERSION_NEW) {
			$payload['signedRequest'] = Tools::signedRequest_encode(['clientToken' => self::buildClientToken()], $app->key);
			$web->setUrl($app->url.'/uninstall')->get($payload);
		} else {
			$payload['signed_request'] = Tools::signedRequest_encode(['id_client' => self::buildClientToken()], $app->key);
			$web->setUrl($app->url.'/uninstall')->post($payload);
		}

		//Quelquesoit la réponse, on supprime l'application --> Tant pis si le serveur de l'App ne fonctionnait pas
		$dbApp = new Local\App();
		$dbApp->deleteApp($app->id);
	}
}
