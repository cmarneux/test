<?php
/**
 * architecture.php
 * @author Tanguy Lambert <tanguy.lambert@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\Databases\BoondManager;
use BoondManager\Models;
use Wish\Mapper;

/**
 * Class Architecture
 * @package BoondManager\Services
 */
class Architecture{
	/**
	 * @return array
	 */
	private static function getAllDatabaseServers(){
		$db = new BoondManager\Architecture();
		$architecture = $db->getAllDatabaseServers();

		return Mapper::fromDatabaseArray($architecture, Models\DatabaseServer::class);
	}

	/**
	 * @return array
	 */
	private static function getAllGEDServers(){
		$db = new BoondManager\Architecture();
		$architecture = $db->getAllGEDServers();

		return Mapper::fromDatabaseArray($architecture, Models\GEDServer::class);
	}

	/**
	 * @return array
	 */
	private static function getAllNodeServers(){
		$db = new BoondManager\Architecture();
		$architecture = $db->getAllNodeServers();

		return Mapper::fromDatabaseArray($architecture, Models\NodeServer::class);
	}

	public static function getBasicDatabaseServer($url){
		foreach(self::getAllDatabaseServers() as $server)
			if($server->url == $url) return $server;
		return false;
	}

	public static function getBasicNodeServer($internalURL){
		foreach(self::getAllNodeServers() as $server)
			if($server->internalURL == $internalURL) return $server;
		return false;
	}

	public static function getBasicGEDServer($directory){
		foreach(self::getAllGEDServers() as $server)
			if($server->directory == $directory) return $server;
		return false;
	}
}
