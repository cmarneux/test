<?php
/**
 * attachedflags.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\AttachedFlags\Filters\DeleteAttachedFlag;
use BoondManager\Databases\Local\AttachedFlag as DBAttachedFlag;
use BoondManager\Models;
use BoondManager\Databases\Mapper;
use BoondManager\Models\AttachedFlag;
use Wish\Filters\AbstractJsonAPI;
use Wish\Models\Model;
use Wish\Models\SearchResult;
use Wish\Tools;

class AttachedFlags {
	/**
	 * mappert category_id => notification_type
	 * @var array
	 */
	private static $modelsNameToAttachedFlagType = [
		Models\Company::class => AttachedFlag::TYPE_COMPANY,
		Models\Opportunity::class => AttachedFlag::TYPE_OPPORTUNITY,
		Models\Project::class => AttachedFlag::TYPE_PROJECT,
		Models\Order::class => AttachedFlag::TYPE_ORDER,
		Models\Product::class => AttachedFlag::TYPE_PRODUCT,
		Models\Purchase::class => AttachedFlag::TYPE_PURCHASE,
		Models\Action::class => AttachedFlag::TYPE_ACTION,
		Models\Employee::class => AttachedFlag::TYPE_RESOURCE,
		Models\Candidate::class => AttachedFlag::TYPE_CANDIDATE,
		Models\Positioning::class => AttachedFlag::TYPE_POSITIONING,
		Models\Contact::class => AttachedFlag::TYPE_CONTACT,
		Models\Invoice::class => AttachedFlag::TYPE_INVOICE
	];

	/**
	 * Get flag
	 * @param int $id
	 * @return AttachedFlag
	 */
	public static function get($id) {
		$db = DBAttachedFlag::instance();
		$dbData = $db->getAttachedFlag($id);

		$attachedFlag = false;
		if($dbData) $attachedFlag = Mapper\AttachedFlag::fromSQL($dbData);
		return $attachedFlag;
	}
		/**
	 * Get flag
	 * @param DeleteAttachedFlag $filter
	 * @return AttachedFlag
	 */
	public static function getFromEntity(DeleteAttachedFlag $filter) {
		$db = DBAttachedFlag::instance();

		$idParent = 0;
		$typeParent = Models\AttachedFlag::TYPE_COMPANY;

		if($filter->company->isDefined()) {
			$idParent = $filter->company->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_COMPANY;
		}

		if($filter->opportunity->isDefined()) {
			$idParent = $filter->opportunity->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_OPPORTUNITY;
		}

		if($filter->project->isDefined()) {
			$idParent = $filter->project->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_PROJECT;
		}

		if($filter->order->isDefined()) {
			$idParent = $filter->order->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_ORDER;
		}

		if($filter->product->isDefined()) {
			$idParent = $filter->product->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_PRODUCT;
		}

		if($filter->purchase->isDefined()) {
			$idParent = $filter->purchase->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_COMPANY;
		}

		if($filter->action->isDefined()) {
			$idParent = $filter->action->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_ACTION;
		}

		if($filter->employee->isDefined()) {
			$idParent = $filter->employee->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_RESOURCE;
		}

		if($filter->candidate->isDefined()) {
			$idParent = $filter->candidate->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_CANDIDATE;
		}

		if($filter->positioning->isDefined()) {
			$idParent = $filter->positioning->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_POSITIONING;
		}

		if($filter->contact->isDefined()) {
			$idParent = $filter->contact->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_CONTACT;
		}

		if($filter->invoice->isDefined()) {
			$idParent = $filter->invoice->getValue()->id;
			$typeParent = Models\AttachedFlag::TYPE_INVOICE;
		}

		$dbData = $db->getAttachedFlagFromParentAndFlag($idParent, $typeParent, $filter->flag->getValue()->id);

		$attachedFlag = false;
		if($dbData) $attachedFlag = Mapper\AttachedFlag::fromSQL($dbData);
		return $attachedFlag;
	}

	/**
	 * Create flag
	 * @param AttachedFlag $attachedFlag
	 * @return boolean
	 */
	public static function create(AttachedFlag &$attachedFlag) {
		//transform attached flag to sqlData
		$sqlData = Mapper\AttachedFlag::toSQL($attachedFlag);

		$db = new DBAttachedFlag();

		$dbData = $db->getAttachedFlagFromParentAndFlag($sqlData['ID_PARENT'], $sqlData['FLAG_TYPE'], $sqlData['ID_USERFLAG']);
		if($dbData) {
			$attachedFlag = Mapper\AttachedFlag::fromSQL($dbData);
			return true;
		} else {
			$attachedFlag->id = $db->createAttachedFlag($sqlData['ID_PARENT'], $sqlData['FLAG_TYPE'], $sqlData['ID_USERFLAG'], true);
			if ($attachedFlag->id) {
				$attachedFlag = self::get($attachedFlag->id);
				return true;
			} else return false;
		}
	}

	/**
	 * Delete flag
	 * @param AttachedFlag $attachedFlag
	 * @return boolean
	 */
	public static function delete(AttachedFlag &$attachedFlag) {
		//transform attached flag to sqlData
		$sqlData = Mapper\AttachedFlag::toSQL($attachedFlag);

		$db = DBAttachedFlag::instance();
		if($db->deleteAttachedFlag($sqlData['ID_PARENT'], $sqlData['FLAG_TYPE'], $sqlData['ID_FLAG'], true))
			return true;
		else
			return false;
	}

	/**
	 * Build flag from filter
	 * @param AbstractJsonAPI $filter
	 * @param AttachedFlag|null $attachedFlag
	 * @return AttachedFlag
	 */
	public static function buildFromFilter($filter, AttachedFlag $attachedFlag = null) {
		if(!$attachedFlag) $attachedFlag = new AttachedFlag();

		//filter and calculate all flags data
		$attachedFlag->fromArray($filter->get('attributes')->getValue());
		$attachedFlag->fromArray($filter->get('relationships')->getValue());

		return $attachedFlag;
	}

	/**
	 * @param Model $entity
	 * @return SearchResult
	 * @throws \Exception
	 */
	public static function getAttachedFlagsFromEntity(Model $entity) {
		$id = $entity->id;
		$type = get_class($entity);

		if(!$id) throw new \Exception("no id for entity ('$type')");

		$attachedFlags = [];

		$flagType = Tools::mapData($type, self::$modelsNameToAttachedFlagType);
		if($flagType) {
			$dbFlag = new DBAttachedFlag();
			$tabIDs = [];

			$tabFlags = CurrentUser::instance()->getFlags();
			foreach ($tabFlags as $flag) $tabIDs[] = $flag->id;

			$flags = $dbFlag->getAttachedFlagsFromParent($flagType, $id, $tabIDs);

			foreach($flags as $flag)
				$attachedFlags[] = Mapper\AttachedFlag::fromSQL($flag);
		}

		return new SearchResult($attachedFlags);
	}
}
