<?php
/**
 * billingdeliveriespurchasesbalance.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;
use Wish\Models\SearchResult;
use BoondManager\APIs\BillingDeliveriesPurchasesBalance\Filters\SearchBillingDeliveriesPurchasesBalance;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Class BillingDeliveriesPurchasesBalance
 * @package BoondManager\Models\Services
 */
class BillingDeliveriesPurchasesBalance{

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchBillingDeliveriesPurchasesBalance $filter
	 * @return SearchResult
	 */
	public static function search(SearchBillingDeliveriesPurchasesBalance $filter)
	{
		$db = Local\BillingDeliveriesPurchasesBalance::instance();
		$searchResult = $db->searchBillingDeliveriesPurchasesBalance($filter);

		return Mapper\BillingDeliveriesPurchasesBalance::fromSearchResult($searchResult);
	}
}
