<?php
/**
 * billingmonthlybalance.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;
use Wish\Models\SearchResult;
use BoondManager\APIs\BillingMonthlyBalance\Filters\SearchBillingMonthlyBalance;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Class BillingMonthlyBalance
 * @package BoondManager\Models\Services
 */
class BillingMonthlyBalance{

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchBillingMonthlyBalance $filter
	 * @return SearchResult
	 */
	public static function search(SearchBillingMonthlyBalance $filter)
	{
		$db = new Local\BillingMonthlyBalance();
		$searchResult = $db->searchBillingMonthlyBalance($filter);

		return Mapper\BillingMonthlyBalance::fromSearchResult($searchResult);
	}
}
