<?php
/**
 * billingprojectsbalance.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Services;

use Wish\Models\SearchResult;
use BoondManager\APIs\BillingProjectsBalance\Filters\SearchBillingProjectsBalance;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Class BillingProjectsBalance
 * @package BoondManager\Models\Services
 */
class BillingProjectsBalance{

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchBillingProjectsBalance $filter
	 * @return SearchResult
	 */
	public static function search(SearchBillingProjectsBalance $filter)
	{
		$db = Local\BillingProjectsBalance::instance();
		$searchResult = $db->searchBillingProjectsBalance($filter);

		return Mapper\BillingProjectsBalance::fromSearchResult($searchResult);
	}
}
