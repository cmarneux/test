<?php
/**
 * billingschedulesbalance.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;
use Wish\Models\SearchResult;
use BoondManager\APIs\BillingSchedulesBalance\Filters\SearchBillingSchedulesBalance;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Class BillingSchedulesBalance
 * @package BoondManager\Models\Services
 */
class BillingSchedulesBalance{

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchBillingSchedulesBalance $filter
	 * @return SearchResult
	 */
	public static function search(SearchBillingSchedulesBalance $filter)
	{
		$db = Local\BillingSchedulesBalance::instance();
		$searchResult = $db->searchBillingSchedulesBalance($filter);

		return Mapper\BillingSchedulesBalance::fromSearchResult($searchResult);
	}
}
