<?php
/**
 * bm.php
 * @author  Louis d'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services;

use BoondManager\Databases\BoondManager;
use BoondManager\Lib\Log;
use Wish\Tools;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputValue;
use BoondManager\Lib\AbstractController;

/**
 * BoondManager Global Constants.
 * @namespace \BoondManager
 */
class BM
{
	/**#@+
	 * BoondManager environment execution
	 * @var int
	 **/
	const
		WEBMODE_VM_DEV = 0,
		WEBMODE_PRODUCTION = 1,
		WEBMODE_SANDBOX = 2,
		WEBMODE_MAINTENANCE_VM_DEV = 30,
		WEBMODE_MAINTENANCE_PRODUCTION = 31,
		WEBMODE_MAINTENANCE_SANDBOX = 32;
	/**#@-*/

	/**#@+
	 * User types (admin / manager / guest)
	 * @var int
	 **/
	const
		USER_TYPE_VISITEUR = 'visitor',
		USER_TYPE_MANAGER = 'manager',
		USER_TYPE_ROOT = 'root',
		USER_TYPE_ADMINISTRATOR = 'administrator',
		USER_TYPE_SUPPORT = 'support',
		USER_TYPE_RESOURCE = 'resource';
	/**#@- */

	const
		DB_TYPE_VISITEUR = 0,
		DB_TYPE_MANAGER = 2,
		DB_TYPE_ROOT = 4,
		DB_TYPE_ADMINISTRATOR = 4,
		DB_TYPE_SUPPORT = 5,
		DB_TYPE_RESOURCE = 6;

	const USER_TYPE_MAPPING = [
		self::DB_TYPE_VISITEUR => self::USER_TYPE_VISITEUR,
		self::DB_TYPE_MANAGER => self::USER_TYPE_MANAGER,
		self::DB_TYPE_ADMINISTRATOR => self::USER_TYPE_ADMINISTRATOR,
		self::DB_TYPE_SUPPORT => self::USER_TYPE_SUPPORT,
		self::DB_TYPE_RESOURCE => self::USER_TYPE_RESOURCE,
		/*self::DB_TYPE_ROOT => self::USER_TYPE_ROOT,*/
	];

	/**#@+
	 * User access
	 * @var int
	 **/
	const
		USER_ACCESS_INACTIVE = 'inactive',
		USER_ACCESS_DEMO = 'demonstration',
		USER_ACCESS_ACTIVE = 'active';
	/**#@- */

	const
		DB_ACCESS_INACTIVE = 0,
		DB_ACCESS_DEMO = 1,
		DB_ACCESS_ACTIVE = 3;

	const USER_ACCESS_MAPPING = [
		self::DB_ACCESS_INACTIVE => self::USER_ACCESS_INACTIVE,
		self::DB_ACCESS_DEMO => self::USER_ACCESS_DEMO,
		self::DB_ACCESS_ACTIVE => self::USER_ACCESS_ACTIVE
	];

	/**#@+
	 * Customer interface's regular expression
	 * @var int
	 **/
	const
		CUSTOMER_INTERFACE_REGEX = '/^[a-z0-9_-]{2,40}$/';
	/**#@-*/

	/**#@+
	 * Login's regular expression
	 * @var int
	 **/
	const
		LOGIN_REGEX = '/^.{5,}$/';
	/**#@-*/

	const BU_ASSIGNMENT = 'assignment';
	const POLE_ASSIGNMENT = 'assignment';
	const AGENCY_ASSIGNMENT = 'assignment';

	/**#@+
	 * Rights for showAllManagers
	 * @var int
	 **/
	const
		RIGHT_SHOWALLMANAGERS_PERIMETRE_ET_AFFECTATION = 'assignmentAndSearch',
		RIGHT_SHOWALLMANAGERS_AFFECTATION = 'assignment',
		RIGHT_SHOWALLMANAGERS_PERIMETRE = 'search',
		RIGHT_SHOWALLMANAGERS_NONE = false;
	/**#@- */

	const SHOWALLMANAGER_MAPPING = [
		0 => self::RIGHT_SHOWALLMANAGERS_NONE,
		1 => self::RIGHT_SHOWALLMANAGERS_PERIMETRE_ET_AFFECTATION,
		2 => self::RIGHT_SHOWALLMANAGERS_AFFECTATION,
		3 => self::RIGHT_SHOWALLMANAGERS_PERIMETRE,
	];

	const SHOWBU_MAPPING = [
		0 => false,
		1 => self::BU_ASSIGNMENT,
	];

	const SHOWPOLE_MAPPING = [
		0 => false,
		1 => self::POLE_ASSIGNMENT,
	];

	const SHOWAGENCY_MAPPING = [
		0 => false,
		1 => self::AGENCY_ASSIGNMENT,
	];

	/**#@+
	 * Keys for global rights
	 * @var string
	 */
	const
		RIGHT_GLOBAL_SHOWGROUPE = 'showGroupe',
		RIGHT_GLOBAL_SHOWBUPOLES = 'showBuPole',
		RIGHT_GLOBAL_SHOWALLMANAGERS = 'showAllManagers',
		RIGHT_GLOBAL_SHOWALLAGENCIES = 'showAllAgencies',
		RIGHT_GLOBAL_SHOWALLPOLES = 'showAllPoles',
		RIGHT_GLOBAL_ADMINISTRATOR = 'administrator';
	/**#@-*/

	/**#@+
	 * Keys for module rights
	 * @var string
	 */
	const
		RIGHT_SHOWALL = 'showAll',
		RIGHT_WRITEALL = 'writeAll',
		RIGHT_EXTRACTION = 'extraction',
		RIGHT_EXPORTATION = 'exportation',
		RIGHT_ASSIGNMENT = 'assignment',
		RIGHT_DELETION = 'deletion',
		RIGHT_EDIT_CREATION_DATE = 'editCreationDate',
		RIGHT_EDIT_BDC_REFERENCES = 'editReference',
		RIGHT_CREATION = 'creation',
		RIGHT_MASKDATA = 'maskData',
		RIGHT_ACCESS_ADMINISTRATIVE = 'administrative',
		RIGHT_ACCESS_ACTIONS = 'actions';
	/**#@-*/

	/**#@+
	 * Rights for Administrative access
	 * @var int
	 **/
	const
		RIGHT_ACCESS_ADMINISTRATIVE_READ = 'read',
		RIGHT_ACCESS_ADMINISTRATIVE_READWRITE = 'readAndWrite';
	/**#@- */

	const ACCESSADMINISTRATIVE_MAPPING = [
		0 => false,
		1 => self::RIGHT_ACCESS_ADMINISTRATIVE_READ,
		2 => self::RIGHT_ACCESS_ADMINISTRATIVE_READWRITE
	];

	/**#@+
	 * Rights level used with
	 * SHOWALLRESSOURCES
	 * SHOWALLBESOINS
	 * @var int
	 */
	const
		RIGHT_LEVEL_1 = 1,
		RIGHT_LEVEL_2 = 2,
		RIGHT_LEVEL_3 = 3,
		RIGHT_LEVEL_4 = 4;
	/**#@-*/

	/**#@+
	 * Rights levels used with
	 * CONFIG_SHOWALLRESSOURCES
	 * CONFIG_SHOWALLBESOINS
	 * CONFIG_SHOWALLCANDIDATS,
	 * CONFIG_SHOWALLPRODUITS
	 * CONFIG_SHOWALLCRM
	 * CONFIG_SHOWALLPROJETS
	 * CONFIG_SHOWALLACHATS
	 * CONFIG_SHOWALLBDC
	 * @var int
	 */
	const
		RIGHT_SHOW_PERSONAL = 'personal',
		RIGHT_SHOW_ALL_AGENCIES_BUS = 'agenciesAndBusinessUnitsAndPoles',
		RIGHT_SHOW_ALL_GROUP = 'all',
		RIGHT_SHOW_ALL_AGENCIES = 'agencies',
		RIGHT_SHOW_ALL_BUS = 'businessUnitsAndPoles';
	/**#@-*/

	const SHOWALL_MAPPING = [
		0 => self::RIGHT_SHOW_PERSONAL,
		1 => self::RIGHT_SHOW_ALL_AGENCIES_BUS,
		2 => self::RIGHT_SHOW_ALL_GROUP,
		3 => self::RIGHT_SHOW_ALL_AGENCIES,
		4 => self::RIGHT_SHOW_ALL_BUS
	];


	/**#@+
	* Modules
	* @var string
	 * @TODO verifier que les doublons n'ont pas d'impacts
	**/
	const
		MODULE_FLAGS = 'flags',
		MODULE_RESOURCES = 'resources',
		MODULE_ACCOUNTS = 'accounts',
		MODULE_AGENCIES = 'agencies',
		MODULE_POLES = 'poles',
		MODULE_MARKETPLACE = 'marketplace',
		MODULE_TRIGGER_ACCOUNTS = 'triggerAccounts',
		MODULE_BUSINESS_UNITS = 'businessunits',
		MODULE_APPS = 'apps',
		MODULE_MY_APPS = 'mesapps',
		MODULE_SUBSCRIPTION = 'subscription',
		MODULE_PROJECTS = 'projects',
		MODULE_PROJECTS_ADMINISTRATIVE = 'administrative',
		MODULE_OPPORTUNITIES = 'opportunities',
		MODULE_POSITIONINGS = 'positionings',
		MODULE_CANDIDATES = 'candidates',
		MODULE_CANDIDATES_ACTIONS = 'actions',
		MODULE_CRM = 'crm',
		MODULE_CONTACTS = 'contacts',
		MODULE_COMPANIES = 'companies',
		MODULE_ACTIVITIES_EXPENSES = 'activityExpenses',
		MODULE_BILLING = 'billing',
		MODULE_PRODUCTS = 'products',
		MODULE_PURCHASES = 'purchases',
		MODULE_REPORTING = 'reporting',
		MODULE_ACTIONS = 'actions',
		MODULE_DASHBOARD = 'dashboard',
		MODULE_RESOURCES_ADMINISTRATIVE = 'administrative',
		MODULE_RESOURCES_ACTIONS = 'actions',
		MODULE_ABSENCESACCOUNTS = 'absencesAccounts',
		MODULE_ADMIN = 'administrator',
		MODULE_ADMIN_ACCOUNTS = 'accounts',
		MODULE_ADMIN_ARCHITECTURE = 'architecture',
		MODULE_ADMIN_CUSTOMERS = 'clients',
		MODULE_ADMIN_PAYMENTS = 'reglements',
		MODULE_ADMIN_SUBSCRIPTION = 'abonnement',
		MODULE_DOWNLOAD_CENTER = 'downloadCenter';
	const
		SUBMODULE_DELIVERY = 'deliveries',
		SUBMODULE_GROUPMENT = 'groupments',
		SUBMODULE_INACTIVITIES = 'inactivities';
	/**#@- */

	/**#@+
	 * Indifferent value for filters
	 **/
	const INDIFFERENT = -1;
	/**#@-*/

	/**#@+
	 * Flag categories
	 * @var int
	 */
	const
		CATEGORY_CRM_COMPANY = 0,
		CATEGORY_OPPORTUNITY = 1,
		CATEGORY_PROJECT = 2,
		CATEGORY_ORDER = 3,
		CATEGORY_PRODUCT = 4,
		CATEGORY_PURCHASE = 5,
		CATEGORY_ACTION = 6,
		CATEGORY_RESOURCE = 7,
		CATEGORY_CANDIDATE = 8,
		CATEGORY_POSITIONING = 9,
		CATEGORY_CRM_CONTACT = 10,
		CATEGORY_BILLING = 11,
		CATEGORY_APP = 12;
	/**#@-*/


	/**#@+
	 * Solr modules
	 * @see self::getSolRModule($bmModule)
	 * @var string
	 **/
	//TODO : A mettre dans le Lib\SolR
	const
		SOLR_MODULE_RESOURCES = 'ressources',
		SOLR_MODULE_CANDIDATES = 'candidats',
		SOLR_MODULE_CRM_CONTACTS = 'crmcontact',
		SOLR_MODULE_CRM_COMPANIES = 'crmsociete';
	/**#@- */

	/**#@+
	 * Influencer types
	 * @var int
	 **/
	//TODO : A mettre dans Models\Contact et Models\Company
	const
		INFLUENCER_CRM_CONTACT = 10,
		INFLUENCER_CRM_COMPANY = 0;
	/**#@- */


	/**#@+
	 * Access levels
	 * @var int
	 **/
	const
		ACCESS_RESOURCES_ADMINISTRATIVES_READ_WRITE = 2,
		ACCESS_RESOURCES_ADMINISTRATIVES_READ = 1;
	/**#@-*/

	/**#@+
	 * Profile Accesses
	 * @var int
	 */
	const
		PROFIL_ACCESS_NONE = 0,
		PROFIL_ACCESS_READ_ONLY = 1,
		PROFIL_ACCESS_READ_WRITE = 2,
		PROFIL_ACCESS_READ_WRITE_HIERARCHY = 3;
	/**#@-*/

	/**#@+
	 * Availability types
	 * @var int
	 */
	const
		DISPO_ASAP = 1,
		DISPO_DATE = 2,
		DISPO_FORCEASAP = 3,
		DISPO_FORCEDATE = 4;
	/**#@-*/

	/**#@+
	 * Default tab name
	 * @var string
	 */
	const TAB_DEFAULT = '';

	/**
	 * All default values
	 * @var float
	 */
	const DEFAULT_CHARGEFACTOR = 1.5;
	const DEFAULT_NUMBEROFWORKINGDAYS = 217;
	const DEFAULT_COUNTRY = 'France';
	const DEFAULT_CALENDAR = 'France_Sans_Pentecote';
	const DEFAULT_CURRENCY = 0;
	const DEFAULT_EXCHANGERATE = 1.0;
	const DEFAULT_LANGUAGE = 'en';

	/**#@+
	 * Errors
	 * @var int
	 */
	const
		ERROR_GLOBAL_UPGRADE_REQUIRED = 426,
		ERROR_GLOBAL_INCORRECT_JSON_SCHEMA = 1000,
		ERROR_GLOBAL_WRONG_AUTHENTICATION = 1001,
		ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE = 1002,
		ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST = 1003,
		ERROR_GLOBAL_ATTRIBUTE_CAN_NOT_BE_SET = 1004,
		ERROR_GLOBAL_FAILED_TO_DELETE_ENTITY = 1005,
		ERROR_GLOBAL_FAILED_TO_SAVE_ENTITY = 1006,
		ERROR_GLOBAL_GED_QUOTA_MAX = 1007,
		ERROR_GLOBAL_FILE_CANNOT_BE_DOWNLOADED = 1008,
		ERROR_GLOBAL_NO_VALID_EMAIL = 1009,
		ERROR_GLOBAL_RESOURCE_QUOTA_MAX = 1010,
		ERROR_GLOBAL_WRONG_PASSWORD = 1011,
		ERROR_GLOBAL_RESOURCE_ACCOUNT_QUOTA_MAX = 1012,
		ERROR_GLOBAL_ATTRIBUTE_DUPLICATED = 1013,
		ERROR_GLOBAL_LOGIN_AND_PASSWORD_MUST_BE_SET = 1014,

		ERROR_APPLICATION_WRONGIDS = 1100,
		ERROR_APPLICATION_LOGIN_DOES_NOT_EXIST = 1101,
		ERROR_APPLICATION_NO_LOGIN_ATTEMPT_FOUND_WITH_THIS_DEVICE = 1102,
		ERROR_APPLICATION_WRONG_CODE_TO_AUTHENTICATE_THIS_DEVICE_TO_AUTHENTICATE_THIS_DEVICE = 1103,
		ERROR_APPLICATION_WRONG_LANGUAGE = 1104,
		ERROR_APPLICATION_WRONG_CAPTCHA = 1105,
		ERROR_APPLICATION_FAILED_TO_CREATE_CAPTCHA = 1106;
	/**#@-*/

	/**#@+
	 * Warning
	 * @var int
	 */
	//TODO : A mettre dans Models\Employee
	const
		WARNING_RESOURCE_NO_HR_CONTRACT = 'noContract',
		WARNING_RESOURCE_OVERLAPPING_CONTRACT_DATES = 'overlappingContractDates';
	/**#@-*/

	const WARNING_ABSENCES_MORE_THAN_ACQUIRED = 'moreThanAbsenceAccountAcquired';

	/**#@+
	 * API types
	 * @var string API_TYPE
	 */
	const
		API_TYPE_DEFAULT = 'normal',
		API_TYPE_BASICAUTH = 'basicAuth',
		API_TYPE_XJWTAPP = 'xJwtApp',
		API_TYPE_XJWTCLIENT = 'xJwtClient';
	/**#@-*/

	/**#@+
	 * Project types
	 * @var int
	 */
	const
		PROJECT_TYPE_TA =1,
		PROJECT_TYPE_PACKAGE = 2,
		PROJECT_TYPE_RECRUITMENT = 3,
		PROJECT_TYPE_PRODUCT = 4;
	/**#@-*/

	/**#@+
	 * Product or purchase subscrition types
	 * @var int
	 */
	const SUBSCRIPTION_TYPE_UNITARY = 0;
	const SUBSCRIPTION_TYPE_UNITARYTYPE_MONTHLY = 1;
	const SUBSCRIPTION_TYPE_QUARTERLY = 2;
	const SUBSCRIPTION_TYPE_SEMIANNUAL = 3;
	const SUBSCRIPTION_TYPE_ANNUAL = 4;
	/**#@-*/

	/**#@+
	 * Auth type
	 * @var string
	 */
	const AUTH_BASIC = 'basicAuth';
	const AUTH_HASH = 'session with hash';
	const AUTH_SESSION = 'session';
	const AUTH_JWT = 'jwt';
	const AUTH_GOOGLE = 'google';
	const AUTH_TRUSTELEM = 'trustelem';
	const AUTH_MICROSOFT = 'microsoft';
	/**#@-*/

	private static $authMode;

	/**
	 * init the BM class for the current request (DEMO MODE, LANGUAGE)
	 */
	public static function initForRequest(){
		self::readJSONBody();
		self::setLanguage();
	}

	/**
	 * Get BoondManager execution environment
	 *
	 * @return int
	 *
	 * @see BM::WEBMODE_* constants
	 */
	public static function getEnvironnement()
	{
		return \Base::instance()->get('BOONDMANAGER.MODE');
	}

	/**
	 * is this a production server
	 * @return bool
	 */
	public static function isProductionMode()
	{
		return in_array(self::getEnvironnement(), [self::WEBMODE_PRODUCTION, self::WEBMODE_MAINTENANCE_PRODUCTION]);
	}

	/**
	 * is this a development server
	 * @return bool
	 */
	public static function isDevelopmentMode()
	{
		return in_array(self::getEnvironnement(), [self::WEBMODE_VM_DEV, self::WEBMODE_MAINTENANCE_VM_DEV]);
	}

	/**
	 * is the demo mode enabled
	 * @return bool
	 */
	public static function isModeDemo()
	{
		return \Base::instance()->get('SESSION.BOONDMANAGER.DEMO') == true;
	}

	/**
	 * enable the Demo mode if needed
	 */
	public static function autoSetModeDemo()
	{
		$user = CurrentUser::instance();
		if(!self::isProductionMode() || $user->isLogged() && in_array($user->getAccount()->client, ['demo'])) {
			\Base::instance()->set('SESSION.BOONDMANAGER.DEMO', true);
		}
	}

	/**
	 * @return string
	 */
	public static function getCustomerCode()
	{
		return BM::getCustomerInterfaceDbName();
	}

	/**
	 * is this the customer interface
	 * @return bool
	 */
	public static function isCustomerInterfaceActive()
	{
		return \Base::instance()->exists('SESSION.CLIENT');
	}

	/**
	 * clear the customer interface
	 * @return bool
	 */
	public static function clearCustomerInterface()
	{
		return \Base::instance()->clear('SESSION.CLIENT');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceDbServer()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMMYSQL.SERVER');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceDbPort()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMMYSQL.PORT');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceDbUser()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMMYSQL.USER');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceDbPwd()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMMYSQL.PWD');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceDbName()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMMYSQL.DATABASE');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceGEDDirectory()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMGED.DIRECTORY');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceNodeURL()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMNODE.URL');
	}

	/**
	 * @return string
	 */
	public static function getCustomerInterfaceNodeServer()
	{
		return \Base::instance()->get('SESSION.CLIENT.BMNODE.SERVER');
	}

	/**
	 * build the path to the client configuration's file
	 * @param string $customerCode
	 * @return string|null path to the file
	 */
	public static function buildCustomerInterfaceFilePath($customerCode)
	{
		return 'app/boondmanager/configs/_clients/' . $customerCode . '.ini';
	}

	/**
	 * get the path to the client configuration's file
	 * @param string $customerCode
	 * @return string|null path to the file
	 */
	public static function getCustomerInterfaceFilePath($customerCode)
	{
		$path = null;
		if(preg_match(self::CUSTOMER_INTERFACE_REGEX, $customerCode))
			$path = BM::getRootPath().'/'.self::buildCustomerInterfaceFilePath($customerCode);
		return $path && is_file($path) ? $path : null;
	}

	/**
	 * Load the configuration for a client and return the client code (CLIENT_WEB) if success
	 * @param string $login user login to find the company if none given
	 * @param string|null $customerCode
	 * @return string|null
	 */
	public static function loadCustomerInterfaceFromLogin($login, $customerCode = null)
	{
		$ctxID = Tools::getContext()->startTiming('client loading');

		BM::clearCustomerInterface();

		$clientWeb = $customerCode && BM::getCustomerInterfaceFilePath($customerCode) ? $customerCode : null;
		if(!$clientWeb) {
			$dataBDD = new BoondManager\Login();//Force usage of BoondManager BDD
			if ($login && $data = $dataBDD->getLogin($login))
				$clientWeb = $data['CLIENT_WEB']; //Get customer from this uniq URL
		}

		if($clientWeb) {
			$config = BM::getCustomerInterfaceFilePath($clientWeb);
			if($config) \Base::instance()->config($config);
		}

		Tools::getContext()->endTiming($ctxID);
		return $clientWeb;
	}

	/**
	 * Load the configuration for a client and return the client code (CLIENT_WEB) if success
	 * @param string $customerCode
	 * @return string|null
	 */
	public static function loadCustomerInterface($customerCode)
	{
		return self::loadCustomerInterfaceFromLogin(null, $customerCode);
	}

	/**
	 * tell if the system is in maintenance
	 * @return bool
	 */
	public static function isMaintenanceMode()
	{
		return in_array(self::getEnvironnement(), [self::WEBMODE_MAINTENANCE_PRODUCTION, self::WEBMODE_MAINTENANCE_SANDBOX, self::WEBMODE_MAINTENANCE_VM_DEV]);
	}

	/**
	 * Execute an external script on the server
	 * @param $cmd
	 * @param bool $asynchronous
	 * @param string $logFile
	 */
	public static function execScript($cmd, $asynchronous = false, $logFile = 'asynchronous.script'){
		$logDir = BM::getLogDir() . $logFile;
		if($asynchronous != '') $cmd = "nohup $cmd >> {$logDir}.log 2>&1 &";
		exec($cmd);
	}

	/**
	 * get a message associated with a code..
	 * @param int $errorCode
	 * @return string
	 */
	public static function getErrorMessage($errorCode)
	{
		return Dictionary::getDict('main.error.' . $errorCode);
	}

	/**
	 * set an error handler for f3
	 */
	public static function setErrorHandler()
	{
		self::disableErrorHandler();
		\Base::instance()->set('ONERROR', function ($f3) {

			// prepare error report
			$trace = self::getErrorTrace();
			$error = 'ERROR '.$f3->get('ERROR.code') . ': ' . $f3->get('ERROR.text') . PHP_EOL;
			$error .= implode(PHP_EOL, $trace);

			// should clean the trace
			$data = self::generateError($f3);

			// attach response data
			$error .= 'response: '.PHP_EOL.json_encode($data, JSON_PRETTY_PRINT);

			// log error
			Log::addError($error);

			// send response
			AbstractController::_sendJSONResponse($data);
		});
	}

	/**
	 * config SMTP
	 */
	public static function configSMTP()
	{
		$f3 = \Base::instance();

		$f3->set('MAIN_SMTP.HOST', $f3->get('BMSMTP.HOST'));
		$f3->set('MAIN_SMTP.PORT', $f3->get('BMSMTP.PORT'));
		$f3->set('MAIN_SMTP.SCHEME', $f3->get('BMSMTP.SCHEME'));
		$f3->set('MAIN_SMTP.LOGIN', $f3->get('BMSMTP.LOGIN'));
		$f3->set('MAIN_SMTP.PWD', $f3->get('BMSMTP.PWD'));
	}

	/**
	 *
	 */
	public static function setPageErrorHandler(){
		self::disableErrorHandler();
		\Base::instance()->set('ONERROR', function($f3){
			/** @var \Base $f3 */

			$data = self::generateError($f3);
			if(self::isProductionMode()){
				$title = ($f3->get('ERROR.code')>=1000)?'Error 409':'Error '.$f3->get('ERROR.code');
				$output = '<h1>'.$title.'</h1>';
				$output .= '<h2>'.$data['meta']['error']['text'].'</h2>';
			}else{
				$output = '<h1>Error '.$data['meta']['error']['code'].'</h1>';
				$output .= '<h2>'.$data['meta']['error']['text'].'</h2>';
				$output .= '<pre>'.implode("\n", $data['meta']['error']['trace']).'</pre>';
			}

			$error = 'ERROR '.$f3->get('ERROR.code') . ':' . PHP_EOL;
			$error .= $data['meta']['error']['text'] . PHP_EOL;

			Log::addError($error);

			echo $output;
		});
	}

	public static function error($code,$text='',array $trace=NULL,$level=0){
		return \Base::instance()->error($code, $text, $trace, $level);
	}

	/**
	 * set an error handler adapted to f3 mocking (in order to catch the error)
	 */
	public static function setMockingErrorHandler()
	{
		self::disableErrorHandler();
		\Base::instance()->set('ONERROR', function ($f3) {
			$f3['HALT'] = false;
			$data = self::generateError($f3);
			$f3['RESPONSE'] = json_encode(AbstractController::_sendJSONResponse($data, false));
			http_response_code(200);
			throw new \Exception('MockingError', 0);
		});
	}

	private static function getErrorTrace(){
		return array_filter(explode("\n", strip_tags(\Base::instance()->get('ERROR.trace'))));
	}

	/**
	 * prepare an error message
	 * @param \Base $f3
	 * @return array
	 */
	private static function generateError(\Base $f3)
	{
		$f3->set('ERROR.trace', array_filter(explode("\n", strip_tags($f3->get('ERROR.trace')))));
		if (!self::isDevelopmentMode() || in_array($f3->get('ERROR.code'), [403, 404]))
			$f3->clear('ERROR.trace');

		if($errorMeta = $f3->get('ERRORDATA')) $f3->clear('ERRORDATA');

		$tabData = [];
		// custom errors
		if($f3->get('ERROR.code') >= 1000) {
			header('HTTP/1.1 422');
			$tabData['errors'] = [];
			if($errorMeta && is_array($errorMeta) && isset($errorMeta['invalidData'])){
				foreach($errorMeta['invalidData'] as $field => $value){
					if(in_array($field, ['relationships', 'attributes']) && is_array($value)){
						$errors = self::buildFilterJsonError($value, $field);

						$tabData['errors'] = array_merge($tabData['errors'], $errors);
					} else {
						$tabData['errors'][] = self::buildParameterError($value, $field);
					}
				}
			} else {
				$tabData['errors'][] = self::buildErrorWithCodeAndDetail($f3->get('ERROR.text'), $f3->get('ERROR.code'));

				if($errorMeta) $tabData['meta'] = $errorMeta;
			}
		} else { //http errors like 4xx or 5xx
			header('HTTP/1.1 ' . $f3->get('ERROR.code'));
			if(self::isDevelopmentMode()) $tabData['errors'] = [$f3->get('ERROR')];
			if($errorMeta) $tabData['meta'] = $errorMeta;
		}

		return $tabData;
	}

	private static function buildParameterError($data, $field){
		$error = self::buildErrorWithCodeAndDetail($data);
		$error['source'] = [ 'parameter' => $field ];

		return $error;
	}

	private static function buildFilterJsonError($data, $pointerPrefix) {
		$errors = [];
		foreach($data as $key => $v) {
			if(is_array($v)) {
				$errors = array_merge($errors, self::buildFilterJsonError($v, $pointerPrefix.'/'.$key));
			} else {
				$error = self::buildErrorWithCodeAndDetail($v);
				$error['source'] =[ 'pointer'   => '/data/' . $pointerPrefix . '/' . $key,
					//'parameter' => $parameter
				];
				$errors[] = $error;
			}
		}
		return $errors;
	}

	private static function buildErrorWithCodeAndDetail($detail, $code = null) {
		if(!$code) {
			if (is_int($detail)) {
				$code   = $detail;
				$detail = BM::getErrorMessage($code);
			} else {
				switch ($detail) {
					case InputValue::ERROR_REQUIRED:
					case InputValue::ERROR_INCORRECT_VALUE:
						$code   = BM::ERROR_GLOBAL_WRONG_OR_MISSING_ATTRIBUTE;
						$detail = BM::getErrorMessage($code);
						break;
					case InputValue::ERROR_INCORRECT_SCHEMA:
						$code   = BM::ERROR_GLOBAL_INCORRECT_JSON_SCHEMA;
						$detail = BM::getErrorMessage($code);
						break;
					case InputValue::ERROR_ENTITY_DOES_NOT_EXIST:
						$code   = BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST;
						$detail = BM::getErrorMessage($code);
						break;
					case AbstractFilters::ERROR_NO_VALID_REQUEST:
						$code   = BM::ERROR_GLOBAL_INCORRECT_JSON_SCHEMA;
						$detail = BM::getErrorMessage($code);
						break;
					default:
						$code = \Base::instance()->get('ERROR.code');
						break;
				}
			}
		}
		return [
			'code'   => $code,
			'detail' => $detail
		];
	}

	/**
	 * remove the custom error handler
	 */
	public static function disableErrorHandler()
	{
		\Base::instance()->clear('ONERROR');
	}

	/**
	 * set path for all available dictionaries
	 */
	public static function initDictionaries()
	{
		$namespaces = [
			'main', 'resources', 'candidates', 'products', 'customers', 'contacts', 'specific', 'deliveries',
			'projects', 'opportunities', 'positioning', 'purchases', 'payments', 'timesreports', 'expensesreports', 'absencesreports',
			'default', 'contracts', 'validation'
		];
		Dictionary::setNamespaces($namespaces);
		Dictionary::setDefaultFallback('default');
	}

	/**
	 * load specific dictionary and filter empty string
	 * @param boolean $cleaned return a filtered & sorted dictionary
	 * @return array
	 */
	public static function getDictSpecific($cleaned = true, $language = null)
	{
		$specific = Dictionary::getDict('specific', null, $language);

		if(!$cleaned) return $specific;

		// sort & filter the following parts: action, state
		foreach (['action', 'state'] as $key) {
			$dict =& $specific['setting'];
			foreach ($dict[$key] as &$data) {
				// remove empty input
				if(is_array($data)) {
					$data = array_values(array_filter($data, function ($entry) {
						return trim($entry['value']) != '';
					}));
					// perform alphabetical sort only if needed
					if ($dict[$key]['sort'])
						usort($data, function ($a, $b) {
							return strcmp($a['value'], $b['value']);
						});
				}
			}
		}

		return $specific;
	}

	public static function getRootPath(){
		return \Base::instance()->get('MAIN_ROOTPATH');
	}

	public static function getAppPath(){
		return \Base::instance()->get('MAIN_APPPATH');
	}

	/**
	 * add a custom autoloader to make the loading faster
	 */
	public static function setCustomAutoloader(){
		$base = self::getAppPath();
		spl_autoload_register(function ($class) use ($base){
			$class = strtr(ltrim($class, '\\'), '\\', '/');
			$file = $base . '/' . strtolower(strtr($class, '-', '')) . '.php';
			if(is_file($file)) {
				return require($file);
			} else {
				$apiBasePath = 'boondmanager/apis/';
				if(strncmp($apiBasePath, $class, strlen($apiBasePath))===0) \Base::instance()->error(404);
			}
		}, $throw = true, $prepend = true);
	}

	/**
	 * return the current language
	 * @return string
	 */
	public static function getLanguage(){
		if(\Base::instance()->exists('SESSION.language')) return \Base::instance()->get('SESSION.language');
		else return self::DEFAULT_LANGUAGE;
	}

	//TODO : Utiliser une constante contenant la liste des langues autorisées
	public static function isValidLanguage($language){
		return in_array($language, ['fr','en']);
	}

	/**
	 * set the language
	 * @param string $language if null, try to detect the language
	 */
	public static function setLanguage($language = null){
		if($language){
			$language = self::isValidLanguage($language) ? $language : self::DEFAULT_LANGUAGE ;
			\Base::instance()->set('SESSION.language', $language);
		}

		// sync SESSION.language and LANGUAGE (used for loading dictionaries)
		if(\Base::instance()->exists('SESSION.language'))
			\Base::instance()->set('LANGUAGE', \Base::instance()->get('SESSION.language'));
		else
			\Base::instance()->set('SESSION.language', substr(\Base::instance()->get('LANGUAGE'), 0, 2));
	}

	/**
	 * set the type of API used if none specified
	 * @param string $type
	 * @see self::API_TYPE
	 */
	public static function setCurrentAPI($type){
		if(!self::getCurrentAPI())
			\Base::instance()->set('BOONDMANAGER.TYPE', $type);
	}

	/**
	 * get the current API
	 * @return string
	 */
	public static function getCurrentAPI(){
		return \Base::instance()->instance()->get('BOONDMANAGER.TYPE');
	}

	/**
	 * enable or disable the security based on the user navigator
	 * @param bool $bool
	 */
	public static function enableNavigatorCheck($bool = true){
		\Base::instance()->set('BOONDMANAGER.NAVIGATOR_CHECK', $bool);
	}

	/**
	 * does the current user navigator should be checked
	 * @return bool
	 */
	public static function isNavigatorCheckEnabled(){
		return true==\Base::instance()->get('BOONDMANAGER.NAVIGATOR_CHECK');
	}

	/**
	 * retrieve the JSON body of a request if needed
	 */
	private static function readJSONBody()
	{
		$ctxID = Tools::getContext()->startTiming('JSON body reading');
		if(isset($_SERVER['CONTENT_TYPE']) && preg_match('#application/.*json#',$_SERVER['CONTENT_TYPE']))
			if(in_array(\Base::instance()->VERB, ['POST','PUT','DELETE'])) {
				//FIXME : gerer un mauvais décodage du body
				$data = json_decode(\Base::instance()->BODY, true);
				if(\Base::instance()->BODY != '' && is_null($data)) \Base::instance()->error(BM::ERROR_GLOBAL_INCORRECT_JSON_SCHEMA);
				//copie des données dans la variable REQUEST pour une réutilisation
				if($data) foreach($data as $k=>$v) \Base::instance()->set("REQUEST.$k", $v);
			}
		Tools::getContext()->endTiming($ctxID);
	}

	public static function getAllCategories(){
		return [
			BM::CATEGORY_OPPORTUNITY,
			BM::CATEGORY_CRM_COMPANY,
			BM::CATEGORY_CRM_CONTACT,
			BM::CATEGORY_CANDIDATE,
			BM::CATEGORY_RESOURCE,
			BM::CATEGORY_PURCHASE,
			BM::CATEGORY_PROJECT,
			BM::CATEGORY_PRODUCT,
			BM::CATEGORY_BILLING,
			BM::CATEGORY_ORDER,
			BM::CATEGORY_APP
		];
	}

	private static function getCategoriesNameMapping(){
		return [
			BM::CATEGORY_OPPORTUNITY => 'opportunity',
			BM::CATEGORY_CRM_COMPANY => 'company',
			BM::CATEGORY_CRM_CONTACT => 'contact',
			BM::CATEGORY_CANDIDATE => 'candidate',
			BM::CATEGORY_RESOURCE => 'resource',
			BM::CATEGORY_PURCHASE => 'purchase',
			BM::CATEGORY_PROJECT => 'project',
			BM::CATEGORY_PRODUCT => 'product',
			BM::CATEGORY_BILLING => 'bill',
			BM::CATEGORY_ORDER => 'order',
			BM::CATEGORY_APP => 'app'
		];
	}

	/**
	 * get the module name (singular) from a category ID
	 * @param int $catID
	 * @return string
	 */
	public static function getCategoryName($catID){
		return Tools::mapData($catID, self::getCategoriesNameMapping());
	}

	/**
	 * get a category ID from the module name (singular)
	 * @param string $catName
	 * @return int
	 */
	public static function getCategoryID($catName){
		return Tools::mapData($catName, array_flip(self::getCategoriesNameMapping()));
	}

	public static function getAllModules(){
		return [
			BM::MODULE_CANDIDATES, BM::MODULE_RESOURCES, BM::MODULE_OPPORTUNITIES, BM::MODULE_PROJECTS, BM::MODULE_CRM,
			BM::MODULE_PURCHASES, BM::MODULE_ACTIONS, BM::MODULE_PRODUCTS, BM::MODULE_ACTIVITIES_EXPENSES, BM::MODULE_BILLING,
			BM::MODULE_REPORTING, BM::MODULE_FLAGS, BM::MODULE_POSITIONINGS, BM::MODULE_COMPANIES, BM::MODULE_CONTACTS, BM::MODULE_APPS
		];
	}

	public static function getAllProjectTypes(){
		return [
			BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE, BM::PROJECT_TYPE_RECRUITMENT, BM::PROJECT_TYPE_PRODUCT
		];
	}

	/**
	 * find out if a captcha has been created and waiting for validation
	 * @param int|string a captcha ID
	 * @return bool true if exists
	 */
	public static function hasCaptcha($id = null){
		return true == \Base::instance()->get( self::getCaptchaKey($id) );
	}

	/**
	 * checl a captcha code
	 * @param $code
	 * @param bool $clear
	 * @param null $id
	 * @return bool
	 */
	public static function validateCaptcha($code, $clear = true, $id = null){
		$test =  strtoupper(\Base::instance()->get( self::getCaptchaKey($id) ) ) == strtoupper( $code );
		if($test && $clear) self::clearCaptcha($id);
		return $test;
	}

	public static function clearCaptcha($id = null){
		\Base::instance()->clear( self::getCaptchaKey($id) );
	}

	/**
	 * Generate an image captcha
	 * @param int|string a captcha ID
	 * @return \Image
	 */
	public static function generateCaptcha($id = null){
		$img = new \Image();
		$img->captcha('fonts/captcha.ttf', 16, 5, self::getCaptchaKey($id));
		return $img;
	}

	private static function getCaptchaKey($id = null){
		return 'SESSION.bmdata.captcha.'.($id?$id:'default');
	}

	/**
	 * @return int
	 */
	public static function getNumberMaxOfDatabaseResults(){
		return \Base::instance()->get('BOONDMANAGER.NB_MAX_RESULTATS_MYSQL');
	}

	/**
	 * @return string path to log dir
	 */
	public static function getLogDir(){
		return \Base::instance()->get('MAIN_ROOTPATH') .'/logs/';
	}

	/**
	 *
	 */
	public static function initDebugMode() {
		if(function_exists('getallheaders')) {
			$headers = getallheaders();
			Tools::setDebugMode(array_key_exists('X-Debug-BoondManager', $headers) && $headers['X-Debug-BoondManager'] === 'true' ? Tools::DEBUG_ON : Tools::DEBUG_OFF);
		}
	}

	/**
	 *
	 */
	public static function initCommon() {
		// On ecrase la definition de la locale pour le formatage des nombres (sinon bug de FILTER_VALIDATE_FLOAT)
		setlocale(LC_NUMERIC, ["en_US", "en_US.UTF_8", "en", "en.UTF_8", "C", "C.UTF-8"]);

		//Development mode management
		if(self::isDevelopmentMode()){
			\Base::instance()->set('BOONDMANAGER.DEVELOPER', \Base::instance()->get('SERVER.HTTP_HOST'));
			\Base::instance()->set('DEBUG', 3);
		}

		self::configSMTP();		//Config SMTP for email sending
		self::initDictionaries();
		self::setCustomAutoloader();
	}

	/**
	 *
	 */
	public static function initBackEnd() {
		self::initCommon();

		//Cross-domain authorization
		//$f3->set('CORS.origin', '*');
		//$f3->set('CORS.ttl', '86400');

		self::setErrorHandler();	//Errors management overloading
		self::initDebugMode();
	}

	/**
	 * @param int $mode
	 */
	public static function initScript($mode) {
		\Base::instance()->set('LOCALES', self::getAppPath().'/'.\Base::instance()->get('LOCALES'));
		\Base::instance()->set('TEMP', self::getAppPath().'/'.\Base::instance()->get('TEMP'));
		\Base::instance()->set('LOGS', self::getAppPath().'/'.\Base::instance()->get('LOGS'));

		switch($mode) {
			case BM::WEBMODE_VM_DEV:
			case BM::WEBMODE_MAINTENANCE_VM_DEV://Local
			\Base::instance()->set('SERVER.HTTP_HOST', 'ui.boondmanager');
				break;
			case BM::WEBMODE_SANDBOX:
			case BM::WEBMODE_MAINTENANCE_SANDBOX://SandBox
			\Base::instance()->set('SERVER.HTTP_HOST', 'sandboxui.boondmanager.com');
				break;
			default://Production
				\Base::instance()->set('SERVER.HTTP_HOST', 'ui.boondmanager.com');
				break;
		}

		self::initCommon();
	}

	public static function getAuthType()
	{
		return self::$authMode;
	}

	public static function setAuthMode($mode)
	{
		self::$authMode = $mode;
	}

	public static function getBoondManagerUrl($company = null)
	{
		return \Base::instance()->get('BOONDMANAGER.APP_URL') . (($company != '') ? '/' . $company : '');
	}

	public static function includeEmptyObjects()
	{
		if(function_exists('getallheaders')) {
			$headers = getallheaders();
			return array_key_exists('front', $headers) && $headers['front'] === 'ember';
		}
		return false;
	}

	/**
	 * Envoit un rapport d'erreur a la hotline du site
	 * @param $request
	 * @param string $typeObjet
	 * @param bool $mail_contact
	 * @throws \Exception
	 */
    private function reportErrorEmail($request, $typeObjet = 'erreur', $mail_contact = false)
    {
	    /*
	     * a utiliser dans les cas suivants :
	        ErrorBDD
			Quota-max
			Soap
			500
	        Erreur inconnue
	     */
    	throw new \Exception('todo'); //TODO
        //On envoit un email uniquement si l'erreur vient de l'application et non d'un requete directe sur cette page
        /*if(Wish_Session::getInstance()->isRegistered('erreurexception_msg')){
            //IP de l'internaute
            if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $ip_simple = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }elseif(isset($_SERVER['HTTP_CLIENT_IP'])){
                $ip_simple = $_SERVER['HTTP_CLIENT_IP'];
            }else{
                $ip_simple = $_SERVER['REMOTE_ADDR'];
            }

            //URL requise
            if(isset($_SERVER['REQUEST_URI']))
                $urlrequest = $_SERVER['REQUEST_URI'];
            else
                $urlrequest = '';

            //URL de provenance
            if(isset($_SERVER['HTTP_REFERER']))
                $urlprov = $_SERVER['HTTP_REFERER'];
            else
                $urlprov = '';

            //Date
            $date = date('Y-m-d H:i:s', mktime());

            //Navigateur  Système
            if(isset($_SERVER['HTTP_USER_AGENT']))
                $syst = $_SERVER['HTTP_USER_AGENT'];
            else
                $syst = '';

            $listemembres = $this->xmlLayoutData->getTagListe('/layout/donnees/listemembres');
            $membre = '';
            foreach($listemembres as $mbdata) {
                if(Wish_Session::getInstance()->get('login_usertype') == intval($mbdata->value)) {
                    $membre = $mbdata->item;
                    break;
                }
            }

            $contact = '';
            if($membre != '')
                $contact .= $this->xmlData->getTagString('/page/mail/internaute/type').' : '.$membre."\n";

            if(Wish_Session::getInstance()->get('login_groupe') != '')
                $contact .= $this->xmlData->getTagString('/page/mail/internaute/client').' : '.Wish_Session::getInstance()->get('login_groupe')."\n";

            if(Wish_Session::getInstance()->get('login') != '')
                $contact .= $this->xmlData->getTagString('/page/mail/internaute/login').' : '.Wish_Session::getInstance()->get('login')."\n";

            if(Wish_Session::getInstance()->get('login_nom') != '' || Wish_Session::getInstance()->get('login_prenom') != '')
                $contact .= $this->xmlData->getTagString('/page/mail/internaute/contact').' : '.Wish_Session::getInstance()->get('login_prenom').' '.Wish_Session::getInstance()->get('login_nom')."\n";

            if(Wish_Session::getInstance()->get('login_email') != '')
                $contact .= $this->xmlData->getTagString('/page/mail/internaute/email').' : '.Wish_Session::getInstance()->get('login_email')."\n";

            $msg = $this->xmlData->getTagString('/page/mail/message').' - '.$date."\n\n"
                    .$this->xmlData->getTagString('/page/mail/type').' : '.$request->getParam('param')."\n"
                    .$contact
                    .$this->xmlData->getTagString('/page/mail/ip').' : '.$ip_simple."\n"
                    .$this->xmlData->getTagString('/page/mail/systeme').' : '.$syst."\n"
                    .$this->xmlData->getTagString('/page/mail/url/cause').' : '.Wish_Session::getInstance()->get('erreurexception_uri')."\n"
                    .$this->xmlData->getTagString('/page/mail/requete').' : '.Wish_Session::getInstance()->get('erreurexception_req')."\n"
                    .$this->xmlData->getTagString('/page/mail/url/requise').' : '.$urlrequest."\n"
                    .$this->xmlData->getTagString('/page/mail/url/provenance').' : '.$urlprov."\n"
                    .$this->xmlData->getTagString('/page/mail/message').' : '.Wish_Session::getInstance()->get('erreurexception_msg');

            $emailObj = new Wish_Email();
            $emailObj->Send($mail_contact?MAIL_CONTACT:MAIL_HOTLINE, $this->xmlData->getTagString('/page/mail/objet/'.$typeObjet), $msg, MAIL_NOREPLY);
        }*/
    }
	/**
	 * Construit l'URL cryptée d'une App à appeler dans une iFrame.
	 *
	 * @param string $api_url URL de base de l'App
	 * @param string $api_key Clef utilisée pour crypter l'URL
	 * @param string $api_fn Fonction de l'App à appeler
	 * @return boolean|string
	 * - `false` : Si l'utilisateur n'est pas connecté
	 * - __string__ : URL cryptée_, uniquement si l'utilisateur est connecté_
	 *
	 * @see \BoondManager\Lib\Tools::signedRequest_encode() Appelée pour construire le paramètre **signed_request** de l'URL cryptée
	 * @see \BoondManager\Lib\Tools::getFiltersMoreQueryString() Appelée pour filtrer les paramètres **controller**, **action**, **id** & **function** de l'URL parente de l'iFrame
	 * @todo S'assurer que la variable BMDATA.URL_UI est bien construite correctement
	 */
	public static function getURLApp($api_url, $api_key, $api_fn = '') {
		if(\Base::instance()->exists('SESSION.user')) {
			if($api_fn != '') $api_fn = '/'.$api_fn;
			$payload = [
				'id_user' => (\Base::instance()->get('SESSION.user.type') == 4) ?'' :Tools::objectID_BM_encode([\Base::instance()->get('SESSION.user.id'), \Base::instance()->get('SESSION.user.company.web')]),
				'id_client' => Tools::objectID_BM_encode(array(\Base::instance()->get('SESSION.user.company.web'))),
				'language' => \Base::instance()->get('SESSION.language'),
				'url_callback' => \Base::instance()->get('BMDATA.URL_UI')
			];

			if(\Base::instance()->get('SERVER.REQUEST_METHOD') == 'POST' && is_array(\Base::instance()->get('REQUEST.chk_result'))) {//On traite le cas des résultats retournés sur les listes de recherche
				$tabURL = array();
				foreach(\Base::instance()->get('REQUEST.chk_result') as $value) $tabURL['chk_result'][] = $value;
				$chkResult = Tools::getFiltersMoreQueryString('&', array(), http_build_query($tabURL));
			} else $chkResult = '';

			//On ajoute les paramètres de la requête
			return $api_url.$api_fn
			.'?signed_request='.Tools::signedRequest_encode($payload, $api_key)
			.Tools::getFiltersMoreQueryString('&', array('controller','action','id','function'))
			.$chkResult;
		} else return false;
	}

	public static function isProfitabilityCalculatingBasedOnMarginRate(){
		return Dictionary::getDict('specific.setting.profitabilityMethodOfCalculating') == 'marginRate';
	}

	public static function getDefaultDate() {
		return date('Y-m-d', time());
	}

	public static function getDefaultDateTime() {
		return date('Y-m-d H:i:s', time());
	}

	public static function getEmailNoReply() {
		return \Base::instance()->get('BMEMAIL.NOREPLY');
	}
}
