<?php
/**
 * businessunits.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\Databases\Local;
use BoondManager\Models;
use BoondManager\Databases\Mapper;
use Wish\Filters\AbstractJsonAPI;

class BusinessUnits{

	/**
	 * Get business unit
	 * @param int $id
	 * @return Models\BusinessUnit
	 */
	public static function get($id) {
		$db = Local\BusinessUnit::instance();
		$dbData = $db->getBusinessUnit($id);

		$bu = false;
		if($dbData) $bu = Mapper\BusinessUnit::fromSQL($dbData);
		return $bu;
	}

	public static function getBasic($id) {
		// TODO : faire ça calmement et utiliser un cache
		$bu = self::get($id);
		if($bu) $bu->filterFields(['id','name']);
		return $bu;
	}

	/**
	 * update business unit
	 * @param Models\BusinessUnit $bu
	 * @return boolean
	 */
	public static function update(Models\BusinessUnit &$bu) {
		// transform business unit to sqlData
		$sqlData = Mapper\BusinessUnit::toSQL($bu);

		$db = Local\BusinessUnit::instance();
		return $db->updateBusinessUnit($bu->id, $sqlData);
	}

	/**
	 * Create business unit
	 * @param Models\BusinessUnit $bu
	 * @return boolean
	 */
	public static function create(Models\BusinessUnit &$bu) {
		//transform business unit to sqlData
		$sqlData = Mapper\BusinessUnit::toSQL($bu);

		$db = Local\BusinessUnit::instance();
		$bu->id = $db->createBusinessUnit($sqlData);
		if($bu->id) {
			$bu = self::get($bu->id);
			return true;
		} else return false;
	}

	/**
	 * Delete business unit
	 * @param int $id
	 * @return boolean
	 */
	public static function delete($id) {
		$db = Local\BusinessUnit::instance();
		return $db->deleteBusinessUnit($id);
	}

	/**
	 * Build business unit from filter
	 * @param AbstractJsonAPI $filter
	 * @param Models\BusinessUnit|null $bu
	 * @return Models\BusinessUnit
	 */
	public static function buildFromFilter($filter, Models\BusinessUnit $bu = null) {
		if(!$bu) $bu = new Models\BusinessUnit();

		//filter and calculate all pole data
		$bu->fromArray($filter->get('attributes')->getValue());
		$bu->fromArray($filter->get('relationships')->getValue());

		return $bu;
	}

	/**
	 * @return Models\BusinessUnit[]
	 */
	public static function getAllBusinessUnits(){
		$db = Local\BusinessUnit::instance();
		$units = $db->getAllBusinessUnits();

		return Mapper\BusinessUnit::fromDatabaseArray($units, Models\BusinessUnit::class);
	}

	/**
	 * Get data for all business units for a given user
	 * @param integer $id profil id, cf. [TAB_USER.ID_PROFIL](../../bddclient/classes/TAB_USER.html#property_ID_PROFIL).
	 * @return Models\BusinessUnit[]
	 */
	public static function getAllUserBusinessUnits($id = 0){
		$bus = array();
		foreach(self::getAllBusinessUnits() as $bu) {
			if($bu->areManagersLoaded() && $bu->hasManager($id))
				$bus[] = $bu;
		}
		return $bus;
	}
}
