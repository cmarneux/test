<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Candidates\Filters\SaveInformation;
use BoondManager\APIs\Candidates\Filters\SearchCandidates;
use BoondManager\APIs\Candidates\Specifications\HaveWriteAccessOnField;
use BoondManager\APIs\Candidates\Specifications\IsActionAllowed;
use BoondManager\Models\Candidate;
use BoondManager\Models;
use Wish\Filters\AbstractJsonAPI;
use Wish\Models\Model;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Databases\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Databases\SolR;
use BoondManager\OldModels\Filters;
use BoondManager\OldModels\Filters\Profiles\Candidates\Notation;
use BoondManager\Lib\RequestAccess;
use BoondManager\APIs\Candidates\Specifications\HaveReadAccess;
use BoondManager\APIs\Candidates\Specifications\HaveWriteAccess;

class Candidates{
	const RIGHTS_ACTIONS = ['downloadResumes', 'addAction', 'downloadTD', 'addOpportunity', 'addPositioning', 'share', 'addProject'];
	const RIGHTS_ATTRIBUTES = ['visibility', 'creationDate', 'mainManager', 'agency'];

	/**
	 * @param int $id
	 * @return Candidate|false
	 */
	public static function find($id) {
		$filter = new SearchCandidates();
		$filter->keywords->setValue( Candidate::buildReference($id) );

		$sql = new Local\Candidate();
		$result = $sql->searchCandidates($filter);

		if($result->rows) return Mapper\Candidate::fromRow($result->rows[0]);
		else return false;
	}

	public static function search(SearchCandidates $filter, $solr = true)
	{
		// special cases disabling solr search
		$solr &= ! preg_match('/(^|\s)(AO)([0-9]+)/i', $filter->keywords->getValue());
		$solr &= $filter->getPeriodType() != SearchCandidates::PERIOD_ACTIONS;

		// try a search on solr
		if($solr) {
			$dbSolr = SolR\Candidates::instance();
			$solrResults = $dbSolr->search($filter);
		}

		$sqlCandidate = Local\Candidate::instance();
		if(!isset($solrResults) || !$solrResults->rows) {
			$searchResult = $sqlCandidate->searchCandidates($filter);
			return Mapper\Candidate::fromSearchResult($searchResult);
		}

		// populate Solr Result with MySQL data
		$IDs = [];
		foreach($solrResults->rows as $r)
			$IDs[] = 'CAND'.$r['ID_PROFIL'];

		$newFilter = clone $filter;
		$newFilter->reset( $except = ['order', 'sort', 'returnHRManager']);
		//~ TODO : 18-05-2016 : Tin : le filtre returnHRManager est exclu du reset car il est nécessaire dans la recherche mysql (ajout de colonnes en fonction de ce filtre)
		$newFilter->keywords->setValue(implode(' ', $IDs));

		$mysqlResults = $sqlCandidate->searchCandidates($newFilter);

		$searchResult = self::mergeData($solrResults, $mysqlResults);
		return Mapper\Candidate::fromSearchResult($searchResult);
	}

	/**
	 * merge results from SolR and MySQL
	 *
	 * @param SearchResult $solrResults
	 * @param SearchResult $mysqlResults
	 * @return SearchResult
	 */
	private static function mergeData($solrResults, $mysqlResults){
		$result = new SearchResult();
		$result->total = $solrResults->total;
		$result->solrSearch = true;
		$sqlRows = $mysqlResults->rows;
		foreach($solrResults->rows as $doc) {
			$idState = -1;
			// recuperation de la cle mysql qui match le profil solr
			foreach($sqlRows as $i => $row)
				if($row['ID_PROFIL'] == $doc['ID_PROFIL']) {
					$idState = $i;
					break;
				}
			if($idState >= 0) {
				// test si La ressource a été modifiée sur MySQL et pas encore sur SolR
				if($sqlRows[$idState]['PROFIL_DATEUPDATE'] != str_replace(array('T','Z'),array(' ',''), $doc['PROFIL_DATEUPDATE'])) {
					//On change à minima son nom & prénom
					$sqlRows[$idState]['PROFIL_NOM'] = $doc['PROFIL_NOM'];
					$sqlRows[$idState]['PROFIL_PRENOM'] = $doc['PROFIL_PRENOM'];
					$sqlRows[$idState]['CURRENT_UPDATE'] = true;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}else{
					$sqlRows[$idState]['CURRENT_UPDATE'] = false;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}
				$result->rows[$idState] = $sqlRows[$idState];
			} else {
				$data = new Candidate();
				$data->fromArray([
					'ID_PROFIL' => $doc['ID_PROFIL'],
					'PROFIL_NOM' => $doc['PROFIL_NOM'],
					'PROFIL_PRENOM' => $doc['PROFIL_PRENOM'],
					'CURRENT_UPDATE' => false,
					'CURRENT_DELETE' => true
				]);
				$result->rows[] = $data;
			}
		}
		$result->rows = array_values($result->rows);
		return $result;
	}

	/**
	 * load a candidate from its ID
	 * @param int $id
	 * @param int $tab
	 * @throws \Exception
	 * @return Candidate|false
	 */
	public static function get($id, $tab = Candidate::TAB_INFORMATION)
	{
		$db = new Local\Candidate();
		$entity = $db->getObject($id, $tab);

		if(!$entity) return false;

		$entity = Mapper\Candidate::fromSQL($entity, $tab);

		if($tab == Candidate::TAB_ADMINISTRATIVE){
			if($entity->contract) $entity->contract = Contracts::get($entity->contract->id);
		}

		return $entity;
	}

	/**
	 * create an empty candidate with default values
	 * @return Candidate
	 */
	public static function getNewProfil()
	{
		$user = clone CurrentUser::instance()->getAccount();
		$pole = $user->pole ? clone $user->pole : null;
		$agency = $user->agency ? clone $user->agency : null;
		$candidate = new Candidate([
			'id'               => 0,
			'mainManager'      => $user,
			'hrManager'        => $user,
			'agency'           => $agency,
			'pole'             => $pole,
			'civility'         => 0,
			'typeOf'           => Candidate::TYPE_CANDIDATE,
			'state'            => 0,
			'globalEvaluation' => -1,
			'visibility'       => true,
			'country'          => CurrentUser::instance()->getCountry(),
			'availability'     => -1,
			'source'           => new Models\OriginOrSource(),
			'creationDate'     => date('Y-m-d'),
		]);
		return $candidate;
	}

	/**
	 * get a list of available actions
	 * @param Candidate $entity
	 * @return array
	 */
	public static function getAvailableActions(Candidate $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$actions = [
			'downloadTD' => true,
			'share' => true,
			'DownloadCV' => $entity->NB_CV,
			'createAction' => (new HaveWriteAccess(Candidate::TAB_ACTIONS))->isSatisfiedBy($request),
			//TODO rendre les droits dynamique
			'createOpportunity' => true,
			'createActivity' => true,
			'createPositionning' => true,
		];

		return $actions;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Candidate $entity
	 * @return array
	 */
	public static function getVisibleTabs(Candidate $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Candidate::getAllTabs();
		$readSpec = new HaveReadAccess(Candidate::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Candidate $entity
	 * @return array
	 */
	public static function getWritableTabs(Candidate $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Candidate::getAllTabs();
		$readSpec = new HaveWriteAccess(Candidate::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * Retrieve all managers
	 *
	 * @param boolean $only_actif If `true` it retrieve only managers having an active account
	 * @return \BoondManager\Models\Account[]
	 */
	public static function getAllGroupManagers($active)
	{
		return Employees::getAllGroupManagers($active);
	}

	/**
	 * @param Model[] $managers list of managers
	 * @param integer[] $ids list of user ids'
	 * @param bool $activeOnly
	 * @return Model[]
	 */
	public static function extendManagersWithUserIDs(array $managers, array $ids, $activeOnly = false)
	{
		return Employees::extendManagersWithUserIDs($managers, $ids, $activeOnly);
	}

	/**
	 * Update some data on a candidate
	 * @param Candidate $profil candidate profile
	 * @param $tab
	 * @return Candidate
	 */
	public static function update(Candidate &$profil, $tab)
	{
		$db = Local\Candidate::instance();

		$sqlData = Mapper\Candidate::toSQL($profil, $tab);
		$db->updateObject($sqlData, $profil->id, intval($profil->ID_DT));

		Notification\Candidate::update($profil, $tab);

		$profil = self::get($profil->id, $tab);

		return $profil;
	}

	/**
	 * @param Candidate $candidate
	 * @param bool $importResume
	 */
	public static function hireCandidate($candidate, $importResume = false) {
		$db = Local\Candidate::instance();
		$db->ConvertCandidatToRessource($candidate->id, $importResume);
	}

	/**
	 * get the api uri from a candidate id and a tab id
	 * @param int $id
	 * @param int $tab
	 * @return string
	 */
	public static function getApiUri($id, $tab = Candidate::TAB_INFORMATION)
	{
		$prefix = '/candidates/'.$id;
		return Tools::mapData($tab,[
			Candidate::TAB_INFORMATION    => $prefix.'/information',
			Candidate::TAB_ADMINISTRATIVE => $prefix.'/administrative',
			Candidate::TAB_TD             => $prefix.'/td',
			Candidate::TAB_ACTIONS        => $prefix.'/actions',
			Candidate::TAB_POSITIONINGS   => $prefix.'/positionings',
		]);
	}

	/**
	 * create a new candidate
	 * @param Candidate $candidate
	 * @return bool
	 */
	public static function create(Candidate &$candidate)
	{
		$sqlData = Mapper\Candidate::toSQL($candidate, Candidate::TAB_INFORMATION);

		$db = Local\Candidate::instance();
		$id = $db->setObject($sqlData);

		if($id) {
			$candidate = self::get($id, Candidate::TAB_INFORMATION);
			return true;
		} else {
			return false;
		}
	}

	public static function deleteCandidate(Candidate $profil)
	{
		$db = Local\Candidate::instance();
		if($db->isCandidatReducible($profil->id)) {
			$db->deleteCandidatData($profil->id, $profil->ID_DT);

			Notification\Candidate::delete($profil);
			return true;
		}else{
			return false;
		}
	}

	public static function getDisabledFields(Candidate $entity) {
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$fields = [];
		foreach(self::RIGHTS_ATTRIBUTES as $field){
			$spec = new HaveWriteAccessOnField($field);
			$fields[$field] = ($spec->isSatisfiedBy($request));
		}
		return $fields;
	}

	/**
	 * Get Current User's rights for a given employee
	 * @param Candidate $candidate
	 * @return Models\Rights
	 */
	public static function getRights(Candidate $candidate)
	{
		$request = new RequestAccess();
		$request->data = $candidate;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_CANDIDATES, $candidate);

		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Candidate::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		$fields = self::getDisabledFields($candidate);
		foreach($fields as $field=>$test) {
			$right->addField($field, true, $test);
		}

		return $right;
	}

	/**
	 * @param AbstractJsonAPI $filter
	 * @param Candidate $profil
	 * @return Candidate
	 */
	public static function buildFromFilter(AbstractJsonAPI $filter, Candidate $profil = null)
	{
		if(!$profil) $profil = new Candidate();

		$profil->backupData();

		return $profil->mergeWith($filter->toObject());
	}

}
