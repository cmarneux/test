<?php
/**
 * companies.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Companies\Specifications\IsActionAllowed;
use BoondManager\Models\ContactDetails;
use BoondManager\Models\WebSocial;
use Wish\Filters\AbstractFilters;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Lib\RequestAccess;
use BoondManager\Databases\SolR;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\APIs\Companies\Filters;
use BoondManager\APIs\Companies\Specifications\HaveReadAccess;
use BoondManager\APIs\Companies\Specifications\HaveWriteAccess;
use BoondManager\Models;

class Companies{
	const RIGHTS_ACTIONS = ['share', 'addAction', 'addContact', 'addOpportunity', 'addProject', 'addPurchase'];

	/**
	 * @param int $id
	 * @return Models\Company|false
	 */
	public static function find($id) {
		$filter = new Filters\SearchCompanies();
		$filter->keywords->setValue( Models\Company::buildReference($id) );

		$sql = Local\Company::instance();
		$result = $sql->searchCompanies($filter);

		if($result->rows) return Mapper\Company::fromRow($result->rows[0]);
		else return false;
	}

	public static function search(Filters\SearchCompanies $filter, $solr = true)
	{
		$sqlCompany = Local\Company::instance();

		// try a search on solr
		if($solr) {
			$dbSolr = new SolR\Companies();
			$solrResults = $dbSolr->search($filter);
		}

		if(!isset($solrResults) || !$solrResults->rows) {
			$searchResult = $sqlCompany->searchCompanies($filter);
			return Mapper\Company::fromSearchResult($searchResult);
		}

		// populate Solr Result with MySQL data
		$IDs = [];
		foreach($solrResults->rows as $r)
			$IDs[] = Models\Company::buildReference( $r['ID_CRMSOCIETE'] );

		$newFilter = clone $filter;
		$newFilter->reset( $except = ['order', 'sort']);
		$newFilter->keywords->setValue(implode(' ', $IDs));

		$mysqlResults = $sqlCompany->searchCompanies($newFilter);

		$searchResult = self::mergeData($solrResults, $mysqlResults);
		return Mapper\Company::fromSearchResult($searchResult);
	}

	/**
	 * merge results from SolR and MySQL
	 *
	 * @param \Wish\Models\SearchResult $solrResults
	 * @param \Wish\Models\SearchResult $mysqlResults
	 * @return \Wish\Models\SearchResult
	 */
	private static function mergeData($solrResults, $mysqlResults){
		$result = new SearchResult();
		$result->total = $solrResults->total;
		$sqlRows = $mysqlResults->rows;
		foreach($solrResults->rows as $doc) {
			$idState = -1;
			// recuperation de la cle mysql qui match le profil solr
			foreach($sqlRows as $i => $row)
				if($row['ID_CRMSOCIETE'] == $doc['ID_CRMSOCIETE']) {
					$idState = $i;
					break;
				}
			if($idState >= 0) {
				// test si La ressource a été modifiée sur MySQL et pas encore sur SolR
				if($sqlRows[$idState]['CSOC_DATEUPDATE'] != str_replace( ['T','Z'], [' ',''], $doc['CSOC_DATEUPDATE'])) {
					//On change à minima son nom & prénom
					$sqlRows[$idState]['CSOC_SOCIETE'] = $doc['CSOC_SOCIETE'];
					$sqlRows[$idState]['CURRENT_UPDATE'] = true;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}else{
					$sqlRows[$idState]['CURRENT_UPDATE'] = $sqlRows[$idState]['CURRENT_DELETE'] = false;
				}
				$result->rows[$idState] = $sqlRows[$idState];
			} else {
				$data = new Models\Company();
				$data->fromArray([
					'ID_CRMSOCIETE' => $doc['ID_CRMSOCIETE'],
					'CSOC_SOCIETE' => $doc['CSOC_SOCIETE'],
					'CURRENT_UPDATE' => false,
					'CURRENT_DELETE' => true
				]);
				$result->rows[] = $data;
			}
		}
		$result->rows = array_values($result->rows);
		return $result;
	}

	/**
	 * @param $id
	 * @param int $tab
	 * @return Models\Company
	 * @throws \Exception
	 */
	public static function get($id, $tab = null){
		$db = Local\Company::instance();
		$entity = $db->getCRMData($id, $tab);

		return ($entity) ? Mapper\Company::fromSQL($entity, $tab) : null;
	}

	public static function getNew()
	{
		$user = CurrentUser::instance();
		$company = new Models\Company([
			'id'           => 0,
			'country'      => $user->getCountry(),
			'state'        => 0,
			'mainManager'  => clone $user->getAccount(),
			'agency'       => clone $user->getAccount()->agency,
			'pole'         => $user->getAccount()->pole ? clone $user->getAccount()->pole : null
		]);

		return $company;
	}


	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Models\Company $entity
	 * @return array
	 */
	public static function getVisibleTabs(Models\Company $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Models\Company::getAllTabs();
		$readSpec = new HaveReadAccess(Models\Company::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Models\Company $entity
	 * @return array
	 */
	public static function getWritableTabs(Models\Company $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Models\Company::getAllTabs();
		$readSpec = new HaveWriteAccess(Models\Company::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * update a company
	 * @param Models\Company $entity
	 * @return Models\Company
	 */
	public static function update(Models\Company &$entity){

		$sqlData = Mapper\Company::toSQL($entity);

		$dbCompany = Local\Company::instance();
		$dbCompany->updateCRMData($sqlData, $entity->id);

		Notification\Company::update($entity, Models\Company::TAB_INFORMATION);

		$entity = self::get($entity->id, Models\Company::TAB_INFORMATION);

		return $entity;
	}

	private static function transformFilterDataToSQLData(Filters\Profiles\Companies\SaveInformation $filter){

		$data = $filter->getValue();
		$data = $data['attributes'];

		$data['departments'] = Tools::serializeArray($data['departments']);

		$sampleCompany = new Models\Company();
		$sqlData = [];
		$sqlData['SOCIETE'] = array_merge(
			Tools::reversePublicFieldsToPrivate( $sampleCompany->getPublicFieldsMapping(), $data),
			Tools::reversePublicFieldsToPrivate( $sampleCompany->getRelationshipMapping(), $filter->relationships->getValue())
		);

		$sqlData['FACT_COORDONNEES'] = [];
		$sampleContact = new ContactDetails();
		foreach($filter->billingDetails->getValue() as $subData){
			$sqlData['FACT_COORDONNEES'][] = Tools::reversePublicFieldsToPrivate( $sampleContact->getPublicFieldsMapping() , $subData);
		}

		$sqlData['WEBSOCIALS'] = [];
		$sampleNetwork = new WebSocial();
		foreach($filter->socialNetworks->getValue() as $subData){
			//~ il faut convertir le champ network dans sa valeur BDD
			$subData['network'] = [
				'viadeo' => WebSocial::WEBSOCIAL_VIADEO,
				'linkedin' => WebSocial::WEBSOCIAL_LINKEDIN,
				'facebook' => WebSocial::WEBSOCIAL_FACEBOOK,
				'twitter' => WebSocial::WEBSOCIAL_TWITTER,
			][$subData['network']];
			$sqlData['WEBSOCIALS'][] = Tools::reversePublicFieldsToPrivate( $sampleNetwork->getPublicFieldsMapping() , $subData);
		}

		$sqlData['INFLUENCERS'] = $filter->influencers->getValue();

		return $sqlData;
	}

	/**
	 * Retrieve all managers
	 *
	 * @param boolean $only_active If `true` it retrieve only managers having an active account
	 * @return \BoondManager\Models\Account[]
	 */
	public static function getAllGroupManagers($only_active = true){
		$db = Local\Manager::instance();
		return $db->getSpecificGroupManagers($only_active);
	}

	/**
	 * @param Models\Company $company
	 * @return bool
	 */
	public static function create(Models\Company &$company)
	{
		$sqlData = Mapper\Company::toSQL($company);

		$db = Local\Company::instance();
		$id = $db->newCRMData($sqlData);

		if($id) {
			$company = self::get($id, Models\Company::TAB_INFORMATION);
			return true;
		} else {
			return false;
		}
	}

	public static function delete(Models\Company $entity)
	{
		$db = Local\Company::instance();
		if($db->isCRMReducible($entity->id)){
			$db->deleteCRMData($entity->id);
			Notification\Company::delete($entity);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * get the api uri from a company id and a tab id
	 * @param int $id
	 * @param string $tab
	 * @return string
	 */
	public static function getApiUri($id, $tab = Models\Company::TAB_INFORMATION){
		$tabs = Models\Company::getAllTabs();
		return Tools::mapData($tab,
			array_map(function($value) use($id){
				return "/companies/$id/$value";
			}, $tabs)
		);
	}

	public static function getAvailableActions($entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = $user = CurrentUser::instance();

		$writeInfo = (new HaveWriteAccess(\BoondManager\Models\Contact::TAB_INFORMATION))->isSatisfiedBy($request);

		$project = $writeInfo && $user->hasAccess(BM::MODULE_OPPORTUNITIES) && $user->hasAccess(BM::MODULE_PROJECTS);
		$project |= ($user->hasAccess(BM::MODULE_RESOURCES) || $user->hasAccess(BM::MODULE_CANDIDATES) || $user->hasAccess(BM::MODULE_PRODUCTS));

		$actions = [
			'goToCompany' => true,
			'addAction' => $writeInfo,
			'addContact' => $writeInfo,
			'addPurchase' => $user->hasAccess(BM::MODULE_PURCHASES),
			'addOpportunity' => $user->hasAccess(BM::MODULE_OPPORTUNITIES),
			'addProject' => $project
		];

		return $actions;
	}

	public static function getRights(Models\Company $company)
	{
		$request = new RequestAccess();
		$request->data = $company;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_COMPANIES, $company);

		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Models\Company::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		return $right;
	}

	/**
	 * @param AbstractFilters $filter
	 * @param Models\Company|null $entity
	 * @return Models\Company
	 */
	public static function buildFromFilter(AbstractFilters $filter, Models\Company $entity = null)
	{
		if(!$entity) $entity = new Models\Company();

		$entity->backupData();
		$entity->mergeWith($filter->toObject());

		return $entity;
	}
}
