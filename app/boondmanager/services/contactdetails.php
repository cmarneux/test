<?php
/**
 * contactdetails.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

class ContactDetails {

	public static function get($id) {
		$db = Local\ContactDetails::instance();
		$data = $db->getContactDetails($id);

		return $data ? Mapper\ContactDetails::fromSQL($data) : false;
	}
}
