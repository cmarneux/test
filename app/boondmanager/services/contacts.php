<?php
/**
 * contacts.php
 * @author Tin LE GALL <tin.legall@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Actions\Filters\SearchActions;
use BoondManager\APIs\Contacts\Specifications\IsActionAllowed;
use Wish\Filters\AbstractFilters;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Lib\RequestAccess;
use BoondManager\Databases\SolR;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\APIs\Contacts\Filters;
use BoondManager\APIs\Contacts\Specifications\HaveReadAccess;
use BoondManager\APIs\Contacts\Specifications\HaveWriteAccess;
use BoondManager\Models;

class Contacts{
	const RIGHTS_ACTIONS = ['share', 'addAction', 'seeCompany', 'addOpportunity', 'addProject', 'addPurchase'];

	/**
	 * @param int $id
	 * @return Models\Contact|false
	 */
	public static function find($id) {
		$filter = new Filters\SearchContacts();
		$filter->keywords->setValue( Models\Contact::buildReference($id) );

		$sql = new Local\Contact();
		$result = $sql->searchContacts($filter);

		if($result->rows) return Mapper\Contact::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * @param Filters\SearchContacts $filter
	 * @param bool $solr
	 * @return SearchResult
	 * @throws \Exception
	 */
	public static function search(Filters\SearchContacts $filter, $solr = true)
	{
		// special cases disabling solr search
		$solr &= $filter->period->getValue() != Filters\SearchContacts::PERIOD_ACTIONS;

		$sqlContact = Local\Contact::instance();

		// try a search on solr
		if($solr) {
			$dbSolr = new SolR\Contacts();
			$solrResults = $dbSolr->search($filter);
		}

		if(!isset($solrResults) || !$solrResults->rows) {
			$searchResult = $sqlContact->searchContacts($filter) ;
			$searchResult = Mapper\Contact::fromSearchResult($searchResult);
			$searchResult = self::attachLastAction( $searchResult );
			return $searchResult;
		}

		// populate Solr Result with MySQL data
		$IDs = [];
		foreach($solrResults->rows as $r)
			$IDs[] = Models\Contact::buildReference($r['ID_CRMCONTACT']);

		$newFilter = clone $filter;
		$newFilter->reset( $except = ['order', 'sort', 'returnLastAction']);
		$newFilter->keywords->setValue(implode(' ', $IDs));

		$mysqlResults = $sqlContact->searchContacts($newFilter);

		$searchResult = self::mergeData($solrResults, $mysqlResults);
		$searchResult = Mapper\Contact::fromSearchResult($searchResult);
		$searchResult = self::attachLastAction($searchResult);
		return $searchResult;
	}

	private static function attachLastAction(SearchResult $result){
		$keywords = [];
		foreach($result->rows as $r){
			/** @var Models\Contact $r */
			$keywords[] = $r->getReference();
		}

		$actionFilter = new SearchActions([BM::CATEGORY_CRM_CONTACT]);
		$actionFilter->disableMaxResultLimit();
		$actionFilter->setData([
			'keywords' => implode(' ', $keywords),
			'order' => SearchActions::ORDERBY_DATE,
			'sort' => SearchActions::ORDER_DESC
		]);

		$actions = Actions::search($actionFilter);
		$ID_PARENT = null;
		$sampleActions = [];
		foreach($actions->rows as $a){
			/** @var Models\Action $a */
			if($a->dependsOn->id != $ID_PARENT){
				$ID_PARENT = $a->dependsOn->id;
				$sampleActions[$a->dependsOn->id] = $a;
			}else continue;
		}

		foreach($result->rows as $r){
			/** @var Models\Contact $r */
			if(isset($sampleActions[$r->id]))
				$r->lastAction = $sampleActions[$r->id];
		}
		return $result;
	}

	/**
	 * merge results from SolR and MySQL
	 *
	 * @param SearchResult $solrResults
	 * @param SearchResult $mysqlResults
	 * @return SearchResult
	 */
	private static function mergeData($solrResults, $mysqlResults){
		$result = new SearchResult();
		$result->total = $solrResults->total;
		$sqlRows = $mysqlResults->rows;
		foreach($solrResults->rows as $doc) {
			$idState = -1;
			// recuperation de la cle mysql qui match le profil solr
			foreach($sqlRows as $i => $row)
				if($row['ID_CRMCONTACT'] == $doc['ID_CRMCONTACT']) {
					$idState = $i;
					break;
				}
			if($idState >= 0) {
				// test si La ressource a été modifiée sur MySQL et pas encore sur SolR
				if($sqlRows[$idState]['CCON_DATEUPDATE'] != str_replace(['T','Z'], [' ',''], $doc['CCON_DATEUPDATE'])) {
					//On change à minima son nom & prénom
					$sqlRows[$idState]['CCON_NOM'] = $doc['CCON_NOM'];
					$sqlRows[$idState]['CCON_PRENOM'] = $doc['CCON_PRENOM'];
					$sqlRows[$idState]['CURRENT_UPDATE'] = true;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}else{
					$sqlRows[$idState]['CURRENT_UPDATE'] = false;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}
				$result->rows[$idState] = $sqlRows[$idState];
			} else {
				$data = new Models\Contact();
				$data->fromArray([
					'ID_CRMCONTACT' => $doc['ID_CRMCONTACT'],
					'CCON_NOM' => $doc['CCON_NOM'],
					'CCON_PRENOM' => $doc['CCON_PRENOM'],
					'CURRENT_UPDATE' => false,
					'CURRENT_DELETE' => true
				]);
				$result->rows[] = $data;
			}
		}
		$result->rows = array_values($result->rows);
		return $result;
	}

	/**
	 * @param Models\Contact[]|Models\Contract $data
	 */
	public static function attachRights($data){
		if(!is_array($data)) $data = [$data];
		$requestAccess = new RequestAccess();
		$requestAccess->setUser(CurrentUser::instance());

		$readAccess = new HaveReadAccess(Models\Contact::TAB_INFORMATION);
		$writeAccess = new HaveWriteAccess(Models\Contact::TAB_INFORMATION);

		foreach($data as $contact){
			/** @var Models\Contact $contact */
			$requestAccess->setData($contact);
			$contact->canReadContact = $readAccess->isSatisfiedBy($requestAccess);
			$contact->canWriteContact = $writeAccess->isSatisfiedBy($requestAccess);
		}
	}

	/**
	 * @param int $id
	 * @param mixed $tab
	 * @return Models\Contact
	 */
	public static function get($id, $tab = Models\Contact::TAB_DEFAULT){
		$db = Local\Contact::instance();
		$entity = $db->getCRMData($id, $tab);
		return ($entity) ? Mapper\Contact::fromSQL($entity, $tab) : null;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Models\Contact $entity
	 * @return array
	 */
	public static function getVisibleTabs(Models\Contact $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Models\Contact::getAllTabs();

		$readSpec = new HaveReadAccess(Models\Company::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Models\Contact $entity
	 * @return array
	 */
	public static function getWritableTabs(Models\Contact $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Models\Contact::getAllTabs();
		$readSpec = new HaveWriteAccess(Models\Contact::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	public static function getAllGroupManagers()
	{
		return Employees::getAllGroupManagers();
	}

	public static function update(Models\Contact &$entity, $tab = Models\Contact::TAB_INFORMATION){

		$sqlData = Mapper\Contact::toSQL($entity, $tab);

		$db = Local\Contact::instance();
		$db->updateCRMData($sqlData, $entity->id);

		Notification\Contact::update($entity, $tab);

		$entity = self::get($entity->id, $tab);

		return true;
	}

	/**
	 * @param Models\Contact $profil
	 * @return bool
	 */
	public static function create(Models\Contact &$profil)
	{
		$sqlData = Mapper\Contact::toSQL($profil);

		$db = Local\Contact::instance();
		$id = $db->newCRMData($sqlData);

		if($id) {
			$profil = self::get($id, Models\Contact::TAB_INFORMATION);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * delete a contact
	 * @param Models\Contact $profil
	 * @return bool `true` on success
	 */
	public static function delete(Models\Contact $profil)
	{
		$db = Local\Contact::instance();
		if($db->isCRMReducible($profil->id)){
			$db->deleteCRMData($profil->id);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * @param Models\Company $company
	 * @return Models\Contact
	 */
	public static function getNew(Models\Company $company)
	{
		$user = CurrentUser::instance();
		$country = $company->country ? $company->country : $user->getCountry();

		$contact = new Models\Contact([
			'id'           => 0,
			'state'        => Models\Contact::TYPE_PROSPECT_BDD,
			'country'      => $country,
			'mainManager'  => clone $user->getAccount(),
			'agency'       => clone $user->getAccount()->agency,
			'pole'         => $user->getAccount()->pole ? clone $user->getAccount()->pole : null,
			'company'      => clone $company,
			'civility'     => 0,
			'origin'       => clone $company->origin,
			'creationDate' => date('Y-m-d'),
		]);

		return $contact;
	}


	/**
	 * get a list of available actions
	 * @param \BoondManager\Models\Employee $entity
	 * @return array
	 */
	public static function getAvailableActions(Models\Contact $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = $user = CurrentUser::instance();

		$writeInfo = (new HaveWriteAccess(Models\Contact::TAB_INFORMATION))->isSatisfiedBy($request);

		$project = $writeInfo && $user->hasAccess(BM::MODULE_OPPORTUNITIES) && $user->hasAccess(BM::MODULE_PROJECTS);
		$project |= ($user->hasAccess(BM::MODULE_RESOURCES) || $user->hasAccess(BM::MODULE_CANDIDATES) || $user->hasAccess(BM::MODULE_PRODUCTS));

		$actions = [
			'goToCompany' => true,
			'addAction' => $writeInfo,
			'addPurchase' => $user->hasAccess(BM::MODULE_PURCHASES),
			'addOpportunity' => $user->hasAccess(BM::MODULE_OPPORTUNITIES),
			'addProject' => $project
		];

		return $actions;
	}

	public static function getRights($contact)
	{
		$request = new RequestAccess();
		$request->data = $contact;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_CONTACTS, $contact);

		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Models\Contact::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		return $right;
	}

	/**
	 * @param AbstractFilters $filter
	 * @param Models\Contact|null $entity
	 * @return Models\Contact
	 */
	public static function buildFromFilter(AbstractFilters $filter, Models\Contact $entity = null)
	{
		if(!$entity) $entity = new Models\Contact();

		$entity->backupData();
		$entity->mergeWith( $filter->toObject() );

		return $entity;
	}

	/**
	 * get the api uri from a candidate id and a tab id
	 * @param int $id
	 * @param string $tab
	 * @return string
	 */
	public static function getApiUri($id, $tab = Models\Company::TAB_INFORMATION)
	{
		$prefix = '/companies/'.$id;
		return Tools::mapData($tab,[
			Models\Company::TAB_INFORMATION    => $prefix.'/information',
			Models\Company::TAB_ACTIONS        => $prefix.'/actions',
		]);
	}
}
