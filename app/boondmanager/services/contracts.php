<?php
/**
 * contracts.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Contracts\Filters\SaveEntity;
use BoondManager\APIs\Contracts\Filters\SearchContracts;
use BoondManager\APIs\Contracts\Specifications\HaveReadAccess;
use BoondManager\APIs\Contracts\Specifications\HaveWriteAccess;
use BoondManager\APIs\Contracts\Specifications\IsACtionAllowed;
use BoondManager\Models\AdvantageType;
use BoondManager\Models\Agency;
use BoondManager\Models\Contract;
use BoondManager\Models;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Rights;
use BoondManager\OldModels\Specifications;

/**
 * Class Contracts
 * @package BoondManager\Models\Services
 */
class Contracts{

	/**
	* Handle the research MySQL
	*
	* @param SearchContracts $filter
	* @return SearchResult
	*/
	public static function search(SearchContracts $filter)
	{
		$db = Local\Contract::instance();
		$searchResult = $db->search($filter);

		return Mapper\Contract::fromSearchResult($searchResult);
	}

	/**
	 * @param $id
	 * @return Contract
	 */
	public static function get($id)
	{
		$db = Local\Contract::instance();
		$contrat = $db->getObject($id);

		return ($contrat) ? Mapper\Contract::fromSQL($contrat) : null;
	}

	/**
	 * @param $id
	 * @return Contract
	 */
	public static function getWithAgencyConfig($id)
	{
		$contract = self::get($id);

		if($contract)
			self::attachAgencyConfig($contract, $contract->agency->id);

		return $contract;
	}

	/**
	 * attach name to advantages and filter any element archived
	 * @param Contract $contract
	 * @return Contract
	 */
	public static function cleanUpContract(Contract $contract){

		// on récupère le nom des avantages actifs sur le contrat à partir de la liste des avantages
		/** @var AdvantageType $advantagesMap */
		$advantagesMap = Tools::useColumnAsKey('reference', $contract->agency->advantageTypes);
		foreach($contract->advantages as $advantage){
			$advantage->name = (array_key_exists($advantage->reference, $advantagesMap)) ?
				$advantagesMap[$advantage->reference]->name :
				$advantage->reference;
		}

		// on récupère le nom des dépenses actives sur le contrat à partir de la liste des dépenses
		$expenseMap = Tools::useColumnAsKey('reference', $contract->agency->expenseTypes);
		foreach($contract->expensesDetails as $expense){
			$expense->expenseType->name = (array_key_exists($expense->expenseType->reference, $expenseMap)) ?
				$expenseMap[$expense->expenseType->reference]->name :
				$expense->expenseType->reference;
		}

		// on récupère le nom des baremes activité exceptionnel actifs sur le contrat à partir de la liste de l'agence
		$scalesMap = Tools::useColumnAsKey(
			'x_ref',
			array_map(function($value){
				$value->x_ref = $value->reference.'_'.$value->parentType.'_'.$value->parentId;
			}, $contract->agency->exceptionalScaleTypes)
		);

		foreach($contract->exceptionalScales as $scale){
			$xRef = $scale->reference.'_'.$scale->parentType.'_'.$scale->parentId;
			if(!array_key_exists($xRef, $scalesMap)){
				$scale->name = ($scale->name ? $scale->name : $scale->reference);
				continue;
			};

			$scale->name = $scalesMap[$xRef]->name;

			// idem pour les regles
			$rulesMap = Tools::useColumnAsKey('reference', $scalesMap[$xRef]->rules);
			foreach($scale->exceptionalRules as $rule){
				$rule->name = (array_key_exists($rule->reference, $rulesMap)) ?
					$rulesMap[$rule->reference]->name :
					$rule->reference;
			}
		}

		// on ne garde que les types avantages non-archivés
		$contract->agency->advantageTypes = array_values(array_filter($contract->agency->advantageTypes, function($value){
			/** @var \BoondManager\Models\AdvantageType $value */
			return $value->state == 1 && $value->frequency != 0;
		}));

		// on ne garde que les types dépenses non-archivés
		$contract->agency->expenseTypes = array_values(array_filter($contract->agency->expenseTypes, function($value){
			/** @var \BoondManager\Models\ExpenseType $value */
			return $value->state == 1;
		}));

		// on ne garde que les baremes activité exceptionnel non-archivés
		foreach($contract->agency->exceptionalScaleTypes as $scale){
			$scale->exceptionalRuleTypes = array_values(array_filter($scale->exceptionalRuleTypes, function ($value){
				/** @var \BoondManager\Models\Rules $value */
				return $value->state == 1;
			}));
		}

		return $contract;
	}

	protected static function attachAgencyConfig($contract, $agencyID){
		$contract->agency = Agencies::get($agencyID, Agency::TAB_ACTIVITYEXPENSES);
		$contract->agency->mergeWith(Agencies::get($agencyID, Agency::TAB_RESOURCES));
	}

	/**
	 * @param $resource
	 * @param $candidate
	 * @param $parentContract
	 * @return Contract
	 * @throws \Exception
	 */
	public static function getNew($resource, $candidate, $parentContract)
	{
		$contract = new Contract([
			'id' => 0
		]);

		if($parentContract){
			$parentContract = self::getWithAgencyConfig($contract);
			if(!$parentContract) throw new \Exception('Contract not found', BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);
		}

		if($resource){
			$resource = Employees::get($resource, Models\Employee::TAB_ADMINISTRATIVE);
			if(!$resource) throw new \Exception('Resource not found', BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);

			$contract->dependsOn = $resource;

			if($parentContract && $parentContract->dependsOn->id == $resource->id){
				// recopie des données du parents
				$contract->mergeWith( $parentContract );
				$contract->relatedContract = $parentContract;
				$contract->id        = 0;

				// un peu de nettoyage
				if($parentContract->endDate != Tools::MAX_DATE){
					$contract->startDate = $parentContract->endDate;
				}else{
					$now = new \DateTime();
					$parentDebut = \DateTime::createFromFormat('Y-m-d', $parentContract->startDate);
					if(!$now->diff($parentDebut)->invert){ // if $now < $parentDebut alors la date de debut ne pouvant etre antérieur au contrat parent, on remplace par celle du parent
						$contract->startDate = $parentContract->startDate;
					}else{
						$contract->startDate = $now->format('Y-m-d');
					}
				}
				$contract->endDate = Tools::MAX_DATE;
			}else{ // pas de contrat parent
				$contract->agency = Agencies::getBasic($resource->agency->id);
				$contract->chargeFactor = $contract->agency->GRPCONF_CHARGE;
				$contract->numberOfWorkingDays = $contract->agency->GRPCONF_NBJRSOUVRE;

				self::attachAgencyConfig($contract, $contract->agency->id);

				//avantages par defaut & activité exceptionnel
				$contract->numberOfHoursPerWeek = $contract->agency->GRPCONF_CTRDUREEHEBDOMADAIRE;
				if(!$resource->isExternalConsultant() && $contract->agency->advantagesTypes){
					$contract->advantages = array_filter($contract->agency->advantageTypes, function($value){
						/** @var  \BoondManager\Models\AdvantageType $value */
						return $value->state == 1 && $value->default == true && $value->frequency > 0;
					});
				}

				if($contract->agency->exceptionalScales)
					$contract->exceptionalScales = array_filter($contract->agency->exceptionalScales, function($value){
						/** @var  \BoondManager\Models\ExceptionalScales $value */
						return $value->state == 1;
					});
			}

			$contract->currency           = $contract->agency->currency;
			$contract->exchangeRate       = 1;
			$contract->exchangeRateAgency = $contract->agency->exchangeRate;
			$contract->currencyAgency     = $contract->agency->currency;

		}else if($candidate){
			$candidate = Candidates::get($resource, Models\Candidate::TAB_ADMINISTRATIVE);
			if(!$candidate) throw new \Exception('Candidate not found', BM::ERROR_GLOBAL_ENTITY_DOES_NOT_EXIST);

			$contract->dependsOn = $candidate;
			$contract->typeOf = $candidate->desiredContract;

			$contract->agency = Agencies::getBasic($candidate->agency->id);

			$contract->currency            = $contract->agency->currency;
			$contract->exchangeRate        = 1;
			$contract->exchangeRateAgency  = $contract->agency->exchangeRate;
			$contract->currencyAgency      = $contract->agency->currency;
			$contract->chargeFactor        = $contract->agency->chargeFactor;
			$contract->numberOfWorkingDays = $contract->agency->numberOfWorkingDays;
		}else{
			throw new \Exception('', Contract::ERROR_CONTRACT_NEED_RESOURCEORCANDIDATE);
		}

		return $contract;
	}

	/**
	 * @param Contract $contract
	 * @return Contract
	 */
	public static function update(Contract &$contract)
	{
		$db = Local\Contract::instance();

		$oldCJMContrat = round($contract->contractAverageDailyCost, 4);
		$oldData = $contract->toArray();

		$sqlData = Mapper\Contract::toSQL($contract);

		if($contract->relatedContract && !$contract->childContract) {
			$pContract = self::get($contract->relatedContract->id);
			$oldDataParent = $pContract->toArray();
			$parentData = [];

			$parentStart = \DateTime::createFromFormat('Y-m-d', $pContract->startDate);
			$contractStart = \DateTime::createFromFormat('Y-m-d', $contract->startDate);

			// si la date du contrat est anterieure a celle du parent, alors elle prend la valeur du parent
			if( $contractStart->diff($parentStart)->invert == 0 )
				$sqlData['CONTRAT']['CTR_DEBUT'] = $parentStart->add( new \DateInterval('P1D') )->format('Y-m-d');

			$parentData['CONTRAT']['CTR_FIN'] = $contractStart->format('Y-m-d');

			$db->updateContract($parentData, $pContract->id);

			Notification\Contract::getInstance($pContract->id, $oldDataParent, $pContract->toArray(), [
				true=> array_filter([ $pContract->dependsOn->id, $pContract->dependsOn->mainManager->id, $pContract->dependsOn->getHrManagerID()])
			])->update();
		}
		$db->updateContract($sqlData, $contract->id);

		$contract = self::getWithAgencyConfig($contract->id);
		Notification\Contract::getInstance($contract->id, $oldData, $contract->toArray(), [
			true=> array_filter([ $contract->dependsOn->id, $contract->dependsOn->mainManager->id, $contract->dependsOn->getHrManagerID() ])
		])->update();

		$newCJMContrat = round($contract->contractAverageDailyCost, 4);

		//Si PUT : if(round($newData['CONTRAT']['CTR_CJMCONTRAT'],4) != round($dataBDD->get('CTR_CJMCONTRAT'), 4)) Wish_Registry::getInstance()->set('page_update', true);
		if($oldCJMContrat != $newCJMContrat){
			// TODO mark as update
		}


		return $contract;
	}

	/**
	 * @param Contract $contract
	 * @return bool
	 */
	public static function create(Contract &$contract)
	{
		$db = Local\Contract::instance();

		$sqlData = Mapper\Contract::toSQL($contract);

		if($contract->relatedContract) {
			$pContract = self::get($contract->relatedContract->id);
			$oldDataParent = $pContract->toArray();
			$parentData = [];

			$parentStart = \DateTime::createFromFormat('Y-m-d', $pContract->startDate);
			$contractStart = \DateTime::createFromFormat('Y-m-d', $contract->startDate);

			// si la date du contrat est anterieure a celle du parent, alors elle prend la valeur du parent
			if( $contractStart->diff($parentStart)->invert == 0 )
				$sqlData['CONTRAT']['CTR_DEBUT'] = $parentStart->add( new \DateInterval('P1D') )->format('Y-m-d');

			$parentData['CONTRAT']['CTR_FIN'] = $contractStart->format('Y-m-d');

			$db->updateContract($parentData, $pContract->id);
			Notification\Contract::getInstance($pContract->id, $oldDataParent, $pContract->toArray(), [
				true=> array_filter([ $pContract->dependsOn->id, $pContract->dependsOn->mainManager->id, $pContract->dependsOn->getHrManagerID()])
			])->update();
		}
		if($id = $db->newContratData($sqlData)) {
			$contract = self::getWithAgencyConfig($id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * get the api uri from a contract id
	 * @param int $id
	 * @return string
	 */
	public static function getApiUri($id){
		return "/contracts/$id";
	}

	public static function delete(Contract $contract)
	{
		$db = Local\Contract::instance();
		if($db->isContratReducible($contract->id)){
			$db->deleteContract($contract->id);
			return true;
		}
		return false;
	}

	/**
	 * @param Contract $entity
	 * @return \BoondManager\Models\Rights
	 */
	public static function getRights($entity)
	{
		$right = new Rights(CurrentUser::instance(), BM::MODULE_RESOURCES, $entity);

		$request = new RequestAccess();
		$request->setData($entity);
		$request->setUser(CurrentUser::instance());

		$isReadable = (new HaveReadAccess())->isSatisfiedBy($request);
		$isWritable = (new HaveWriteAccess())->isSatisfiedBy($request);
		//$isDeletable = (new HaveDeleteAccess())->isSatisfiedBy($request);

		//$right->addAction('delete', $isDeletable);
		$right->addAction('share', (new IsACtionAllowed('share'))->isSatisfiedBy($request));

		$right->addApi('profile', $isReadable, $isWritable);

		return $right;
	}

	/**
	 * @param $id
	 * @return Contract|false
	 */
	public static function find($id)
	{
		$filter = new SearchContracts();
		$filter->keywords->setValue( Models\Contract::buildReference($id) );

		$sql = Local\Contract::instance();
		$result = $sql->search($filter);

		if($result->rows) return Mapper\Contract::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * @param SaveEntity $filter
	 * @param Contract|null $contract
	 * @return Contract
	 */
	public static function buildFromFilter(SaveEntity $filter, Contract $contract = null) {
		if(!$contract) $contract = new Contract();
		$contract->mergeWith($filter->toObject());
		return $contract;
	}


	protected static function checkContract($contrat){
		if(!$contrat->agency) {
			Tools::logError("le contrat {$contrat->ID_CONTRAT} n'a pas de config agence associée.");
			return false;
		}
		return true;
	}
}
