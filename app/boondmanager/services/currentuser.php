<?php
/**
 * currentuser.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\Lib\Models\AccountInterface;
use BoondManager\Lib\Models\HasAgencyInterface;
use BoondManager\Lib\Models\HasHrManagerInterface;
use BoondManager\Lib\Models\HasManagerInterface;
use BoondManager\Lib\Models\HasPoleInterface;
use BoondManager\Lib\RequestAccess;
use BoondManager\Lib\Specifications\HasAccess;
use BoondManager\Models\Employee;
use BoondManager\Models\UserAdvancedApps;
use Wish\Cache;
use Wish\Models\ModelJSONAPI;
use Wish\Tools;
use BoondManager\Models\Perimeter;
use BoondManager\Models\FlagsList;
use BoondManager\Models\Account;
use BoondManager\Models\Pole;
use BoondManager\Models\Agency;
use BoondManager\Models\BusinessUnit;
use BoondManager\Databases\Local;
use BoondManager\Lib\Specifications\CanReaffectAllManagers;
use BoondManager\Lib\Specifications\HasAllPerimeterManager;
use BoondManager\Services;

/**
 * Singleton to handle the current user
 *
 * @package BoondManager\Services
 * @inheritdoc Employee
 */
class CurrentUser implements AccountInterface {
	/**
	 * @var Perimeter available perimeter
	 */
	private $perimeter;

	/**
	 * @var int user id
	 * @deprecated
	 */
	public $id;

	/**
	 * @var string user login
	 * @deprecated
	 */
	public $login;

	/**
	 * @var string last name
	 * @deprecated
	 */
	protected $lastname;
	/**
	 * @var string first name
	 * @deprecated
	 */
	protected $firstname;

	/**
	 * @var int profil ID
	 * @deprecated
	 */
	protected $profil_id;

	/**
	 * @var string last connection
	 */
	public $lastConnection;

	/**
	 * @var int user type
	 * @deprecated
	 */
	public $level = BM::USER_TYPE_VISITEUR;

	/**
	 * @var string user email
	 * @deprecated
	 */
	public $email;

	/**
	 * @var CurrentUser current user instance
	 */
	private static $currentUser;

	/**
	 * CurrentUser constructor.
	 * @param array $user `SESSION.user`
	 */
	private function __construct($user){
		$this->setFieldsFromUser($user);
	}

	/**
	 * get the current user
	 * @return CurrentUser|null
	 */
	public static function instance(){
		$f3 = \Base::instance();

		if(!self::$currentUser){
			self::$currentUser = new self($f3->get('SESSION.user'));
		}
		return self::$currentUser;
	}

	/**
	 * destroy the current instance
	 */
	public function destroy(){
		\Base::instance()->clear('SESSION.user');
		self::$currentUser = null;
	}

	/**
	 * return the company ID
	 * @return int
	 */
	public function getAgencyId(){
		return $this->get('company.id');
	}

	public function setAgenciesIDs(array $ids){
		$this->set('agencies', $ids);
	}

	public function getPoleId(){
		return $this->get('pole.id');
	}

	public function setPolesIDs(array $ids){
		return $this->set('poles', $ids);
	}

	/**
	 * populate the session data of the current user
	 * @param string|array $f3Key
	 * @param mixed $value
	 * @return $this
	 */
	public function set($f3Key, $value = null){
		$f3 = \Base::instance();
		if(is_array($f3Key)){
			foreach($f3Key as $key=>$value){
				$this->set($key, $value);
			}
		}else{
			$f3->set('SESSION.user.'.$f3Key, $value);
			$firstLevelKey = explode('.',$f3Key,1);
			if( in_array($firstLevelKey[0], ['id','type','profil'])) $this->reload();
		}
		return $this;
	}

	/**
	 * test if the current user is authenticated
	 * @return bool
	 */
	public function isLogged(){
		return true==$this->getAccount();
	}

	/**
	 * retrieved all user's data saved in session
	 * @return mixed
	 */
	public function getAllData(){
		return \Base::instance()->get('SESSION.user');
	}

	/**
	 * check that a key exists in session for the current user
	 * @param string $f3key
	 * @return bool
	 */
	public function exists($f3key){
		return \Base::instance()->exists('SESSION.user.'.$f3key);
	}

	/**
	 * get a session value for the current user
	 * @param string $f3key
	 * @return mixed
	 */
	public function get($f3key){
		return \Base::instance()->get('SESSION.user.'.$f3key);
	}

	/**
	 * reload data from session (use only for connection)
	 */
	public static function reload(){
		if(self::$currentUser) self::$currentUser->setFieldsFromUser( \Base::instance()->get('SESSION.user') );
	}

	/**
	 * init all fields from the given array
	 * @param $user
	 */
	private function setFieldsFromUser($user){
		if(!is_array($user)) return;

		if(array_key_exists('id',$user)) $this->id = $user['id'];
		if(array_key_exists('type',$user)) $this->level = $user['type'];
		if(array_key_exists('login',$user)) $this->login = $user['login'];
		if(array_key_exists('lastconnexion',$user)) $this->lastConnection = $user['lastconnexion'];

		if(array_key_exists('profil',$user)){
			if(array_key_exists('lastname', $user['profil'])) $this->lastname = $user['profil']['lastname'];
			if(array_key_exists('firstname', $user['profil'])) $this->firstname = $user['profil']['firstname'];
			if(array_key_exists('firstname', $user['profil'])) $this->firstname = $user['profil']['firstname'];
			if(array_key_exists('id', $user['profil'])) $this->profil_id = $user['profil']['id'];
			if(array_key_exists('email', $user['profil'])) $this->email = $user['profil']['email'];
		}
	}

	/**
	 * define a perimeter
	 * @param $perimeter
	 */
	public function setPerimeter($perimeter){
		$this->perimeter = $perimeter;
	}

	/**
	 * Build the perimeter available to the user for a search
	 *
	 * @param string $module la "ressource" bdd auquel l'utilisateur veut accéder
	 * @param boolean $includeMyManagers Indique si on retourne le périmètre "Mon Périmètre" (contient les sous-managers):
	 * - `true` : Le filtre "Mon périmètre" est retourné dans la liste des périmètres
	 * - `false` : Le filtre "Mon périmètre" n'est pas retourné dans la liste des périmètres
	 * @param boolean $allGroup Indique si on simule l'existence de la clef SESSION.user.clef.rights.showgroupe :
	 * - `true` : On force l'existence de la clef
	 * - `false` : On ne force pas l'existence de la clef
	 * @param bool $alwaysIndifferent
	 * @return Perimeter
	 * @throws \Exception
	 */
	public function getSearchPerimeter($module, $includeMyManagers = false, $allGroup = false, $alwaysIndifferent = false){
		//TODO : extraire les ACLs de la classe
		if($module instanceof Local\Rapports){
			//TODO: ajouter une méthode à la liste rapport lors de la migration
			throw new \Exception('TODO: créer une methode getRapportType dans Rapports');
		}
		if($includeMyManagers){
			throw new \Exception('TODO: gerer le cas "mon agence"');
		}

		$perimeter = new Perimeter($this, $module);

		$defaultPerimeter = new Perimeter($this);

		$perimeter->setDefaultMatch( ($this->get('narrowPerimeter')) ? Perimeter::MATCH_ALL : Perimeter::MATCH_ANY );

		$defaultPerimeter->addManager( $this->getAccount() );
		$perimeter->setDefaultPerimeter( $defaultPerimeter );
		$perimeter->setRequiredPerimeter(true);

		// TODO: utiliser des specifications
		// vérification des droits pour voir les societes et les BUs
		if( $this->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $allGroup ||
			(($module == BM::MODULE_RESOURCES || $module == BM::MODULE_ACTIVITIES_EXPENSES || ($module instanceof Local\Rapports && $module->getRapportType() == 'resources' && $this->hasRight(BM::RIGHT_WRITEALL,BM::MODULE_RESOURCES))) && $this->hasRight(BM::RIGHT_SHOWALL,BM::MODULE_RESOURCES, BM::RIGHT_SHOW_ALL_GROUP))
			|| ($module  == BM::MODULE_CANDIDATES &&  $this->hasRight(BM::RIGHT_SHOWALL,BM::MODULE_CANDIDATES, BM::RIGHT_SHOW_ALL_GROUP))
			|| (($module == BM::MODULE_OPPORTUNITIES || $module == BM::MODULE_POSITIONINGS) && $this->hasRight(BM::RIGHT_SHOWALL,BM::MODULE_OPPORTUNITIES, BM::RIGHT_SHOW_ALL_GROUP)) ||
			($module  == BM::MODULE_PRODUCTS &&  $this->hasRight(BM::RIGHT_SHOWALL,BM::MODULE_PRODUCTS, BM::RIGHT_SHOW_ALL_GROUP)) ||
			($module  == BM::MODULE_PURCHASES &&  $this->hasRight(BM::RIGHT_SHOWALL,BM::MODULE_PURCHASES, BM::RIGHT_SHOW_ALL_GROUP)) ||
			(($module == BM::MODULE_CRM || ($module instanceof Local\Rapports && $module->getRapportType() == 'crm' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_CRM))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, BM::RIGHT_SHOW_ALL_GROUP)) ||
			(($module == BM::MODULE_PROJECTS || ($module instanceof Local\Rapports && $module->getRapportType() == 'projects' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_PROJECTS))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PROJECTS, BM::RIGHT_SHOW_ALL_GROUP)) ||
			(($module instanceof Local\Facturation || ($module instanceof Local\Rapports && $module->getRapportType() == 'projects' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_BILLING))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_BILLING, BM::RIGHT_SHOW_ALL_GROUP))) {

			$perimeter->setRequiredPerimeter(false);

			//On récupère la liste de toutes les agences et toutes les BU
			$liste_agences = Services\Agencies::getAllBasics();
			$liste_bu = Services\BusinessUnits::getAllBusinessUnits();
			$liste_poles = Services\Poles::getAllPoles();
		} else {
			if($alwaysIndifferent)
				$perimeter->setRequiredPerimeter(false);

			$liste_agences = [];
			$liste_bu      = [];
			$liste_poles   = [];

			if((($module == BM::MODULE_RESOURCES || $module == BM::MODULE_ACTIVITIES_EXPENSES || ($module instanceof Local\Rapports && $module->getRapportType() == 'resources' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_RESOURCES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				($module  == BM::MODULE_CANDIDATES && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CANDIDATES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				(($module == BM::MODULE_OPPORTUNITIES || $module == BM::MODULE_POSITIONINGS) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_OPPORTUNITIES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				($module  == BM::MODULE_PRODUCTS && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PRODUCTS, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				($module  == BM::MODULE_PURCHASES && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PURCHASES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				(($module == BM::MODULE_CRM || ($module instanceof Local\Rapports && $module->getRapportType() == 'crm' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_CRM))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				(($module == BM::MODULE_PROJECTS || ($module instanceof Local\Rapports && $module->getRapportType() == 'projects' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_PROJECTS))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PROJECTS, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) ||
				(($module instanceof Local\Facturation || ($module instanceof Local\Rapports && $module->getRapportType() == 'projects' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_BILLING))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_BILLING, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES]))) {
				$liste_agences = $this->getAgencies();
			}

			if($this->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES) ||
				(($module == BM::MODULE_RESOURCES || $module == BM::MODULE_ACTIVITIES_EXPENSES || ($module instanceof Local\Rapports && $module->getRapportType() == 'resources' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_RESOURCES))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_RESOURCES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				($module  == BM::MODULE_CANDIDATES && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CANDIDATES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				(($module == BM::MODULE_OPPORTUNITIES || $module == BM::MODULE_POSITIONINGS) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_OPPORTUNITIES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				($module == BM::MODULE_PRODUCTS && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PRODUCTS, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				($module == BM::MODULE_PURCHASES && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PURCHASES, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				(($module == BM::MODULE_CRM || ($module instanceof Local\Rapports && $module->getRapportType() == 'crm' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_CRM))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_CRM, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				(($module == BM::MODULE_PROJECTS || ($module instanceof Local\Rapports && $module->getRapportType() == 'projects' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_PROJECTS))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_PROJECTS, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS])) ||
				(($module instanceof Local\Facturation || ($module instanceof Local\Rapports && $module->getRapportType() == 'projects' && $this->hasRight(BM::RIGHT_WRITEALL, BM::MODULE_BILLING))) && $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_BILLING, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS]))) {
				$liste_bu = $this->getBusinessUnits();
				$liste_poles = $this->getPoles();
			}
		}

		if( (new HasAllPerimeterManager())->isSatisfiedBy($this) )
			$managersList = Services\Managers::getAllGroupManagers();
		else
			$managersList = $this->getManagers();

		//On met des identifiants négatifs pour toutes les sociétés (en négatif pour les distinguer des identifiants de managers)
		foreach($liste_agences as $agence)
			$perimeter->addAgency($agence);

		//On met des identifiants BU_"IDBUSINESSUNIT" pour toutes les BU
		foreach($liste_bu as $bu)
			$perimeter->addBU($bu);

		//On met des identifiants POLE_"IDPOLE" pour tous les poles
		foreach($liste_poles as $pole)
			 $perimeter->addPole($pole);

		foreach($managersList as $manager) {
			$perimeter->addManager($manager);
		}

		return $perimeter;
	}

	/**
	 * Build flags available to the user for a search
	 * @return FlagsList
	 */
	public function getSearchFlags(){
		$tabFlags = $this->getFlags();

		$flags = new FlagsList();
		foreach($tabFlags as $flag)
			$flags->addFlag($flag);
		return $flags;
	}

	/**
	 * Load and store in session all business units for the user
	 *
	 * @return array
	 */
	function getBusinessUnits() {
		$cache = Cache::instance();
		$key = 'bu.list.user.'.$this->getUserId();
		if(!$cache->exists($key)) {
			$bus = $this->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
				? Services\BusinessUnits::getAllBusinessUnits()
				: Services\BusinessUnits::getAllUserBusinessUnits($this->getEmployeeId());
			$cache->set($key, $bus);
		}
		return $cache->get($key);
	}

	/**
	 * Build a list of all accessible flags for the user
	 * @return array
	 */
	public function getFlags(){

		$cache = Cache::instance();
		$key = 'flags.list.user'.$this->getUserId();

		if(!$cache->exists($key)) {
			//On construit la liste des managers appartenant à cet utilisateur
			$managers = $this->getManagers([], $this->hasRight(BM::RIGHT_SHOWALL, BM::MODULE_FLAGS));
			$managersIDs = [$this->getEmployeeId()] + array_unique(Tools::getFieldsToArray($managers, 'id'));

			$usersFlags = Services\Flags::getFlagsFromMainManager($managersIDs);

			$cache->set($key, $usersFlags);
		}

		return $cache->get($key);
	}


	/**
	 * retrieve all user's managers
	 * @param array $addManagers a list of manager IDs to add to the result
	 * @param boolean $forceShowGroup if true, give a virtual "showgroupe" access to the user
	 * @return Account[]
	 */
	public function getManagers($addManagers = [], $forceShowGroup = false){
		return ($this->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $forceShowGroup)
			? Services\Managers::getAllGroupManagers(true, $addManagers)
			: Services\Managers::getAllUserManagers($this->getUserId(), $addManagers);
	}

	/**
	 * Check if the manager is in the hierarchy of the user (sub hierarchy)
	 * @param int $managerId
	 * @return bool
	 */
	public function isMyManager($managerId){
		return Tools::in_2dimArray($managerId, $this->getManagers(), 'id');
	}

	/**
	 * check the agency is in the hierarchy of the user
	 * @param int $agencyId agency ID
	 * @return bool
	 */
	public function isMyAgency($agencyId){
		return in_array($agencyId, $this->getAgencyIDs());
	}

	/**
	 * check if the manager belongs to one of the user business units
	 * @param int $managerId
	 * @return bool
	 * @throws \Exception
	 */
	public function isMyBusinessUnitManager($managerId){
		$bus = Tools::getFieldsToArray($this->getBusinessUnits(), 'id');
		Services\Managers::isManagerInBusinessUnits($managerId, $bus);
		return Tools::in_2dimArray($managerId, $this->getBusinessUnitManager(), 'id');
	}

	/**
	 * check if the user belongs to this pole
	 * @param int $poleID
	 * @return bool
	 */
	public function isMyPole($poleID){
		return in_array($poleID, $this->getPolesIDs());
	}

	/**
	 * get a list of all managers in the current user's business units
	 * @return BusinessUnit[]
	 */
	public function getBusinessUnitManager(){
		$cache = Cache::instance();
		$key = 'managers.bu.list.user.'.$this->getUserId();
		if(!$cache->exists($key)){
			$tabBU = Tools::getFieldsToArray($this->getBusinessUnits(), 'id');

			$managers = Managers::getManagersFromBusinessUnits($tabBU);
			$cache->set($key, $managers);
		}
		return $cache->get($key);
	}

	/**
	 * Build a list of all manager available for reassignment
	 *
	 * @param array $addManagers a list of manager IDs to add to the result
	 * @return Account[]
	 */
	public function getManagerForReassignment($addManagers = []) {
		$cache = Cache::instance();
		$key = 'user.getManagerForReassignments.'.$this->getUserId();
		if(!$cache->exists($key)) {
			$liste_managers =  (new CanReaffectAllManagers)->isSatisfiedBy($this)
				? Managers::getAllGroupManagers(true, $addManagers)
				: $this->getManagers($addManagers);

			$cache->set($key, $liste_managers);
		}
		return $cache->get($key);
	}

	/**
	 * @return Account[]
	 */
	public function getHRForReassignment(){
		return Managers::getAllGroupManagers();
	}

	/**
	 * get a list of agencies
	 * @param array $includeFollowingAgencies a list of agencies ID to add to the result
	 * @return Agency[]
	 */
	public function getAgenciesForReassignment($includeFollowingAgencies = []){
		if($this->hasRight(BM::RIGHT_GLOBAL_SHOWALLAGENCIES)) {
			return Agencies::getAllAgencies();
		}else{
			return $this->getAgencies($includeFollowingAgencies);
		}
	}

	/**
	 * @param array $includeFollowingPoles
	 * @return Pole[]
	 */
	public function getPolesForReassignment($includeFollowingPoles = []){
		if($this->hasRight(BM::RIGHT_GLOBAL_SHOWALLPOLES)) {
			return Poles::getAllPoles();
		}else{
			return $this->getPoles($includeFollowingPoles);
		}
	}

	public function getInfluencersForReassignment(){
		$cache = Cache::instance();
		$key = 'user.getInfluencersForReassignment.'.$this->getUserId();
		if(!$cache->exists($key)) {
			$liste_managers =   Managers::getAllGroupManagers(true);

			$cache->set($key, $liste_managers);
		}
		return $cache->get($key);
	}

	/**
	 * Check if the user has a global right or a right within a module
	 *
	 * @param string $right
	 * @param string $module null means a verification on a global access
	 * @param int|array $level right level
	 * @return bool
	 */
	public function hasRight($right, $module = null, $level = null){
		if($this->isGod()) return true;
		if(!$module) $module = 'main';

		if(ctype_alnum($module) && ctype_alnum($right)) $levelAccess = $this->getAccount()->rights->$module->$right;
		else $levelAccess =  false;

		if(!is_null($level) && $levelAccess !== false){
			return is_array($level) ? in_array($levelAccess, $level) : ($levelAccess == $level);
		}else
			return $levelAccess;
	}

	public function setRight($right, $value) {
		$args = func_get_args();
		if(count($args) == 3) list($right, $module, $value) = $args;
		else $module = 'main';

		$this->getAccount()->rights->$module->$right = $value;
	}

	/**
	 * Check if the user is allowed to access a module, sub module with the good level
	 *
	 * @TODO migrer ça sous la forme d'une spec
	 *
	 * @param string $module module name (ressource, crm, etc ...)
	 * @param string $tab part of the module
	 * @param int $level access level
	 * @return bool
	 */
	public function hasAccess($module, $tab = null, $level = null){

		$access = new RequestAccess();
		$access->setUser($this);
		return (new HasAccess($module, $tab, $level))->isSatisfiedBy($access);
	}

	/**
	 * Give a module access to the user
	 * @param array|string $module
	 * @param int|bool $access
	 * @deprecated
	 */
	public function setAccess($module, $access = true){
		if(!is_array($module)) $module = [$module];
		$modules = $this->getAccount()->modules;
		foreach($module as $m) {
			$this->set("module.$m", $access);
			$modules[$m] = $access;
		}
		$this->getAccount()->modules = $modules;
	}

	public function calculateAccessModules() {
		$account = $this->getAccount();
		switch ($account->level) {
			case BM::USER_TYPE_ROOT:
				if($this->isBoondManagerAdmin()) {
					$account->modules->accounts = true;
					$account->modules->architecture = true;
					$account->modules->dashboard = true;
					$account->modules->subscription = true;
					$account->modules->customers = true;
					$account->modules->reglements = true;
				}
			case BM::USER_TYPE_ADMINISTRATOR:
				if($this->isBoondManagerAdmin()) {
					$account->modules->dashboard = true;
					$account->modules->subscription = true;
					$account->modules->customers = true;
					$account->modules->reglements = true;
				}
				break;
			case BM::USER_TYPE_MANAGER:
				if( $account->agency->downloadCenter ) $account->modules->downloadCenter = true;
				break;
			case BM::USER_TYPE_RESOURCE:
				if($account->agency->showDecompte && $account->typeOf != Employee::TYPE_EXTERNAL_RESOURCE) $account->modules->absencesAccounts;
				break;
			case BM::USER_TYPE_SUPPORT:
				break;
		}
	}

	/**
	 * Check that an object is accessible by checking its owners
	 * @param array|ModelJSONAPI $IDs ids keys to check
	 * this array must contains 2 or 3 items wich are:
	 * - 0 => the agency key. Example : `TAB_PROFIL.ID_SOCETE`
	 * - 1 => the pole key. Example : `TAB_PROFIL.ID_POLE`
	 * - 2 => the responsible key of the object. Example : `TAB_PROFIL.ID_RESPMANAGER`
	 * - 3 => (optional) the key of another responsible. Example : `TAB_PROFIL.ID_RESPRH`
	 * @param string $moduleName the module name (used for checking right access)
	 * @param string $tab tab of the object we want to check
	 * @param array $allowedTabs array of allowed tab if the user has no rights but can access the object
	 * @param array $forceIDs array of allowed responsible key
	 * @param string $readKey reading key (used for checking right access)
	 * @param string $writeKey writing key (used for checking right access)
	 * @return int
	 * @throws \Exception
	 */
	public function checkHierarchyAccess($IDs, $moduleName, $tab = '', $allowedTabs = [], $forceIDs = [], $readKey = BM::RIGHT_SHOWALL, $writeKey = BM::RIGHT_WRITEALL) {
		// possibilité d'utilier IDs comme un objet
		if($IDs instanceof ModelJSONAPI){
			$extractedIDs = [];
			if($IDs instanceof HasAgencyInterface && $IDs instanceof HasManagerInterface && $IDs instanceof HasPoleInterface){
				$extractedIDs[] = $IDs->getAgencyID();
				$extractedIDs[] = $IDs->getPoleID();
				$extractedIDs[] = $IDs->getManagerID();
			}else throw new \Exception('object not implementing Has(Manager/Agency/Pole)Interface');

			if($IDs instanceof HasHrManagerInterface) $extractedIDs[] = $IDs->getHrManagerID();

			$IDs = $extractedIDs;
		}else if(!is_array($IDs)){
			throw new \Exception('First argument is invalid');
		}

		$access = BM::PROFIL_ACCESS_NONE;

		if(is_array($IDs)) {
			switch(sizeof($IDs)) {
				case 4:
					list($idAgency, $idPole, $idManager, $idHR) = $IDs;
					if( $this->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE) || $this->isMyManager($idManager) || $this->isMyManager($idHR)
						|| ($this->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES)
							&& ($this->isMyBusinessUnitManager($idManager)
								|| $this->isMyBusinessUnitManager($idHR)
								|| $this->isMyPole($idPole)
							)
						)
						|| (in_array($this->getEmployeeId(), $forceIDs))
					) {
						$access = BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY;
					}else if( $this->hasRight($readKey, $moduleName, BM::RIGHT_SHOW_ALL_GROUP)
						|| (( $this->hasRight($readKey, $moduleName, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES])) && in_array($idAgency, $this->get('agencies')))
						|| ($this->hasRight($readKey, $moduleName, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS]) && ($this->isMyBusinessUnitManager($idManager) || $this->isMyBusinessUnitManager($idHR) || $this->isMyPole($idPole)))
					) {
						$access = (in_array($tab, $allowedTabs) || $this->hasRight($moduleName, $writeKey))
							? BM::PROFIL_ACCESS_READ_WRITE
							: BM::PROFIL_ACCESS_READ_ONLY;
					}
					break;
				case 3:
					list($idAgency, $idPole, $idManager) = $IDs;
					if($this->hasRight(BM::RIGHT_GLOBAL_SHOWGROUPE)
						|| $this->isMyManager($idManager)
						|| ($this->hasRight(BM::RIGHT_GLOBAL_SHOWBUPOLES) && ($this->isMyBusinessUnitManager($idManager) || $this->isMyPole($idPole)))
						|| (in_array($this->getEmployeeId(), $forceIDs))
					) {
						$access = BM::PROFIL_ACCESS_READ_WRITE_HIERARCHY;
					}else if($this->hasRight($readKey, $moduleName, BM::RIGHT_SHOW_ALL_GROUP)
						|| (($this->hasRight($readKey, $moduleName, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_AGENCIES]) && in_array($idAgency,$this->get('agencies')))  || ($this->hasRight($readKey, $moduleName, [BM::RIGHT_SHOW_ALL_AGENCIES_BUS, BM::RIGHT_SHOW_ALL_BUS]) && ($this->isMyBusinessUnitManager($idManager) || $this->isMyPole($idPole))))
					) {
						$access = (in_array($tab, $allowedTabs) || $this->hasRight($writeKey, $moduleName))
							? BM::PROFIL_ACCESS_READ_WRITE
							: BM::PROFIL_ACCESS_READ_ONLY;
					}
				break;
			}
		}
		return $access;
	}

	/**
	 * @param array $addPoles add poles having the following ids to the results
	 * @return Pole[]
	 */
	public function getPoles(array $addPoles = []){
		$userPoles = $this->getPolesIDs();

		$goodPoles = $userPoles ? array_merge($userPoles, $addPoles) : $addPoles;

		$poles = Services\Poles::getAllPoles($goodPoles);

		return $poles;
	}

	/**
	 * get all user agencies
	 * @param array $addAgencies array with the agencies id
	 * @param bool $fullData if `true` get all agencies data, `false` get only the following fields `ID_SOCIETE` and ``
	 * @return Agency[]
	 */
	public function getAgencies(array $addAgencies = [], $fullData = false){
		$agencies = [];
		$validAgenciesIDs = array_merge($this->getAgencyIDs(), $addAgencies);

		foreach(Services\Agencies::getAllAgencies($fullData) as $agency)
			if(in_array($agency->id, $validAgenciesIDs))
				$agencies[] = $agency;

		return $agencies;
	}

	/**
	 * get a list of agencies id in which the user belongs
	 * @return array
	 */
	public function getAgencyIDs(){
		$mainId = [$this->getAccount()->agency->id];
		$otherIds = Tools::getFieldsToArray($this->getAccount()->otherAgencies, 'id');
		return array_merge($mainId, $otherIds);
	}

	/**
	 * return a list of poles id
	 * @return array
	 */
	public function getPolesIDs(){
		$account = $this->getAccount();
		$mainId = $account->pole ? [$account->pole->id] : [];
		$otherIds = Tools::getFieldsToArray($this->getAccount()->otherPoles, 'id');
		return array_merge($mainId, $otherIds);
	}

	/**
	 * get the full name of the user
	 * @return string
	 */
	public function getFullName(){
		return $this->getFirstName().' '.$this->getLastName();
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->getAccount()->email1;
	}

	/**
	 * get the zenDesk ID
	 * @return string
	 */
	public function getZendeskID(){
		return BM::getCustomerCode().'|'.$this->getUserId();
	}

	/**
	 * set the user apps
	 * @param array $apps
	 */
	public function setApps($apps){
		$this->set('module.'.BM::MODULE_MY_APPS, $apps);
	}

	/**
	 * return an array of user's apps
	 * @return array
	 */
	public function getApps(){
		$data = $this->get('module.'.BM::MODULE_MY_APPS);
		return !$data ? [] : $data;
	}

	/**
	 * get a list of the user's apps id
	 * @return array
	 */
	public function getAppsId(){
		return array_keys($this->getApps());
	}

	/**
	 * check if the intranet is enabled for a user
	 * @return bool
	 */
	public function isIntranetEnabled(){
		$account = $this->getAccount();
		return !in_array($account->level, [BM::USER_TYPE_ROOT, BM::USER_TYPE_ADMINISTRATOR]) && $account->agency->intranet == 1;
	}

	/**
	 * build and store a hash in session link to the navigator
	 */
	public function enableNavigatorCheck(){
		$this->set('navigator.check', $this->calculateHashNavigator());
	}

	/**
	 * check that the navigator is still the one saved in session (in the case it has been saved)
	 * @return bool
	 */
	public function checkNavigator(){
		$hash = $this->get('navigator.check');
		return !$hash || $hash == $this->calculateHashNavigator();
	}

	/**
	 * build a hash to identify the navigator
	 * @return string
	 */
	private function calculateHashNavigator(){
		return $_SERVER['REMOTE_ADDR'] . '|' . $_SERVER['HTTP_USER_AGENT'];
	}

	/**
	 * the user has a super access (can read, right anything)
	 * @return bool
	 */
	public function isGod(){
		return \Base::instance()->exists('SESSION.isGod') ? \Base::instance()->get('SESSION.isGod') && $this->isManager(): false;
	}

	/**
	 * @return string
	 */
	public function getCountry(){
		$agency = $this->getAccount()->agency;
		return ($agency && $agency->country) ? $agency->country: BM::DEFAULT_COUNTRY;
	}

	/**
	 * @return string
	 */
	public function getCalendar(){
		$agency = $this->getAccount()->agency;
		return ($agency && $agency->calendar) ? $agency->calendar : BM::DEFAULT_CALENDAR;
	}

	/**
	 * @return int
	 */
	public function getCurrency(){
		$agency = $this->getAccount()->agency;
		return ($agency && $agency->currency) ? $agency->currency : BM::DEFAULT_CURRENCY;
	}

	/**
	 * @return float
	 */
	public function getExchangeRate(){
		$agency = $this->getAccount()->agency;
		return ($agency && $agency->exchangeRate) ? $agency->exchangeRate : BM::DEFAULT_EXCHANGERATE;
	}

	/**
	 * @return int
	 */
	public function getCurrencyInDatabase(){
		return BM::DEFAULT_CURRENCY;
	}

	/**
	 * is the current user a simple resource
	 * @return bool
	 */
	public function isSimpleResource()
	{
		return $this->getAccount()->level == BM::USER_TYPE_RESOURCE;
	}

	/**
	 * Get the current user id
	 * @return int
	 */
	public function getUserId()
	{
		return $this->getAccount()->userId;
	}

	/**
	 * Get the current user's employee id
	 * @return int
	 */
	public function getEmployeeId()
	{
		return $this->getAccount()->id;
	}

	/**
	 * @return int
	 */
	public function getNumberOfWorkingDays(){
		$agency = $this->getAccount()->agency;
		return $agency && $agency->numberOfWorkingDays ? $agency->numberOfWorkingDays : BM::DEFAULT_NUMBEROFWORKINGDAYS;
	}

	/**
	 * @return float
	 */
	public function getChargeFactor(){
		$agency = $this->getAccount()->agency;
		return $agency && $agency->chargeFactor ? $agency->chargeFactor : BM::DEFAULT_CHARGEFACTOR;
	}

	/**
	 * @return Account
	 */
	public function getAccount(){
		return $this->get('account');
	}

	public function setAdvancedApp($name, $id){
		if($id && array_key_exists($id, $this->getApps())) {
			if(!$this->getAccount()->advancedApps)
				$this->getAccount()->advancedApps = new UserAdvancedApps();
			$this->getAccount()->advancedApps->$name = $id;
		}
	}

	/**
	 * @param Account $level
	 * @return $this
	 */
	public function setAccount(Account $level) {
		$this->calculateAccessModules();
		$this->set('account', $level);
		return $this;
	}

	/**
	 * @param Agency $agency
	 * @return $this
	 */
	public function setAgency(Agency $agency) {
		$this->calculateAccessModules();
		$this->getAccount()->agency = $agency; // TODO : vérifire la persistance en session
		return $this;
	}

	/**
	 * is the current user a manager
	 * @return bool
	 */
	public function isManager()
	{
		// FIXME : ne fonctionne pas pour le moment avec le login via cookie
		return $this->getAccount()->isManager();
	}

	public function isAdministrator()
	{
		// FIXME : il y'a un soucis sur les comptes managers qui ont accès à l'interface d'administration. Le test ne se fait pas correctement (ex vhugo)
		return $this->getAccount()->isAdministrator() || $this->getAccount()->isManager() && $this->getAccount()->config->CONFIG_ADMINISTRATOR;
	}

	public function isRoot(){
		return $this->getAccount()->isRoot();
	}

	public function isSupport(){
		return $this->getAccount()->isSupport();
	}

	public function isEmployee(){
		return $this->getAccount()->isEmployee();
	}

	public function getLastName(){
		return $this->getAccount()->lastName;
	}

	public function getFirstName(){
		return $this->getAccount()->firstName;
	}

	/**
	 * @return boolean
	 */
	public function getNarrowPerimeter(){
		return $this->get('narrowPerimeter');
	}

	public function setBoondManagerAdmin($bool) {
		$this->set('boondmanager', true);
	}

	/**
	 * @return bool
	 */
	public function isBoondManagerAdmin() {
		return boolval($this->get('boondmanager'));
	}
}
