<?php
/**
 * customers.php
 * @author Tanguy Lambert <tanguy.lambert@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\Databases\Mapper;
use BoondManager\Databases\BoondManager;
use BoondManager\Models;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\APIs\Customers\Filters;

/**
 * Class Customers
 * @package BoondManager\Models\Services
 */
class Customers {
	/**#@+
	 * @var string
	 **/
	const
		LOGO = 'www/img/_clients/logo/';
	/**#@- */

	/** Search projects
	 *
	 * @param Filters\SearchCustomers $filter
	 * @return SearchResult
	 */
	public static function search(Filters\SearchCustomers $filter) {
		$db = new BoondManager\Customer();
		$searchResult = $db->searchCustomers($filter);

		return Mapper\Customer::fromSearchResult($searchResult);
	}

	/**
	 * Get customer
	 * @param int $id
	 * @return Models\Customer
	 */
	public static function get($id) {
		$db = new BoondManager\Customer();
		$dbData = $db->getCustomer($id);

		$customer = false;
		if($dbData) $customer = Mapper\Customer::fromSQL($dbData);
		return $customer;
	}

	/**
	 * Delete customer
	 * @param Models\Customer $customer
	 * @return boolean
	 */
	public static function delete(Models\Customer $customer) {
		$db = new BoondManager\Customer();

		if($db->deleteCustomerWithCustomerCode($customer->id, $customer->code)) return true;
		return false;
	}

	/**
	 * Create customer
	 * @param Models\Customer $customer
	 * @return boolean
	 */
	public static function create(Models\Customer $customer) {
		//transform project to sqlData
		$sqlData = Mapper\Customer::toSQL($customer);

		$db = new BoondManager\Customer();
		$customer->id = $db->createCustomer($sqlData);
		return $customer->id ? true : false;
	}

	/**
	 * Create customer with a free trial
	 * @param Models\Customer $customer
	 * @param string $managerLogin
	 * @param bool $managerPasswordIsEqualToLogin
	 * @param bool $sendMail
	 * @return bool
	 * @throws \Exception
	 */
	public static function createFreeTrial(Models\Customer $customer, $managerLogin, $managerPasswordIsEqualToLogin = false, $sendMail = true) {
		if(Login::isLoginExists($customer->administrator->login)) throw new \Exception('Administrator login exists');
		if(Login::isLoginExists($managerLogin)) throw new \Exception('Manager login exists or empty');

		$customer->subscription->state = BM::USER_ACCESS_DEMO;
		if($customer->id = self::create($customer)) {
			BM::loadCustomerInterface($customer->code);

			$idManager = 0;
			$employeeCreated = 0;

			//Buid the employee
			$allAgencies = Agencies::getAllAgencies(true);
			if(isset($allAgencies[0])) {
				$resource = new Models\Employee([
					'firstName' => $customer->firstName,
					'lastName' => $customer->lastName,
					'email1' => $customer->email,
					'typeOf' => Models\Employee::TYPE_INTERNAL_RESOURCE,
				]);

				$resource->agency = new Models\Agency([
					'id' => $allAgencies[0]->id
				]);

				$employeeCreated = Employees::create($resource);
			}

			if($employeeCreated) {
				$password = $managerPasswordIsEqualToLogin ? $managerLogin : substr(uniqid(), 0, 8);

				$manager = new Models\User([
					'login' => $managerLogin,
					'password' => Login::saltPassword($password, $managerLogin),
					'language' => $customer->language,
					'securityAlert' => false,
					'securityCookie' => false,
					'typeOf' => BM::USER_TYPE_MANAGER,
					'state' => BM::USER_ACCESS_ACTIVE,
					'homepage' => 'dashboard',
					'modules' => $customer->administrator->modules
				]);

				$manager->resource = new Models\AccountRights([
					'id' => $resource->id
				]);

				$manager->modules->reporting = true;

				$manager->rights = new Models\AccountRights();
				if ($manager->modules->crm) {
					$manager->rights->crm = new Models\AccountRights\CRM([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_EXPORTATION => true,
						BM::RIGHT_EDIT_CREATION_DATE => true,
						BM::MODULE_ADMIN => false,
						BM::MODULE_SUBSCRIPTION => false
					]);
				}

				if ($manager->modules->opportunities) {
					$manager->rights->opportunities = new Models\AccountRights\Opportunities([
						BM::RIGHT_EDIT_CREATION_DATE => true,
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_EXPORTATION => true,
						BM::RIGHT_MASKDATA => true
					]);
				}

				if ($manager->modules->resources) {
					$manager->rights->resources = new Models\AccountRights\Resources([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_CREATION => true,
						BM::RIGHT_EDIT_CREATION_DATE => true,
						BM::RIGHT_ACCESS_ADMINISTRATIVE => BM::RIGHT_ACCESS_ADMINISTRATIVE_READWRITE,
						BM::RIGHT_ACCESS_ACTIONS => true
					]);
				}

				if ($manager->modules->candidates) {
					$manager->rights->candidates = new Models\AccountRights\Candidates([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_CREATION => true,
						BM::RIGHT_EDIT_CREATION_DATE => true,
						BM::RIGHT_ACCESS_ACTIONS => true
					]);
				}

				if ($manager->modules->products) {
					$manager->rights->products = new Models\AccountRights\Products([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_CREATION => true
					]);
				}

				if ($manager->modules->projects) {
					$manager->rights->projects = new Models\AccountRights\Projects([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_EDIT_CREATION_DATE => true,
						BM::RIGHT_ACCESS_ADMINISTRATIVE => true
					]);
				}

				if ($manager->modules->purchases) {
					$manager->rights->purchases = new Models\AccountRights\Purchases([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_EXPORTATION => true
					]);
				}

				if ($manager->modules->reporting) {
					$manager->rights->reporting = new Models\AccountRights\Reporting([
						BM::RIGHT_EXTRACTION => true
					]);
				}

				if ($manager->modules->activityExpenses) {
					$manager->rights->activityExpenses = new Models\AccountRights\ActivityExpenses([
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_EXPORTATION => true
					]);
				}

				if ($manager->modules->billing) {
					$manager->rights->billing = new Models\AccountRights\Billing([
						BM::RIGHT_SHOWALL => BM::RIGHT_SHOW_ALL_GROUP,
						BM::RIGHT_WRITEALL => true,
						BM::RIGHT_EXTRACTION => true,
						BM::RIGHT_ASSIGNMENT => true,
						BM::RIGHT_DELETION => true,
						BM::RIGHT_EXPORTATION => true,
						BM::RIGHT_EDIT_BDC_REFERENCES => true,
						BM::RIGHT_EDIT_CREATION_DATE => true
					]);
				}

				$manager->rights->actions = new Models\AccountRights\Actions([
					BM::RIGHT_WRITEALL => true,
					BM::RIGHT_EXTRACTION => true,
					BM::RIGHT_ASSIGNMENT => true,
					BM::RIGHT_DELETION => true
				]);

				$manager->rights->main = new Models\AccountRights\Main([
					BM::RIGHT_GLOBAL_SHOWGROUPE => true,
					BM::RIGHT_GLOBAL_SHOWALLMANAGERS => BM::RIGHT_SHOWALLMANAGERS_PERIMETRE_ET_AFFECTATION,
					BM::RIGHT_GLOBAL_SHOWBUPOLES => true,
					BM::RIGHT_GLOBAL_SHOWALLPOLES => BM::POLE_ASSIGNMENT,
					BM::RIGHT_GLOBAL_SHOWALLAGENCIES => BM::AGENCY_ASSIGNMENT
				]);

				//Build the manager
				$idManager = Accounts::create($manager);

				//TODO : Send an email
			}

			BM::clearCustomerInterface();
			return $idManager && $employeeCreated ? true : false;
		}

		return $customer->id ? true : false;
	}

	/**
	 * @param Models\Customer $customer
	 * @param string $typeDatabase
	 * @return bool
	 */
	public static function reset(Models\Customer $customer, $typeDatabase = Filters\Reset::TYPEOF_DEMONSTRATION) {
		//FIXME (Tanguy) : Créer un sémaphore pour empêcher le reset, création, suppression simultanée d'un même client  ==> A METTRE DANS LE CONTROLLEUR ET A UTILISER SUR TOUTES LES APIS DE CREATION/SUPPRESSION/MODIFICATION D'UN CLIENT
		$customer->firstName = self::getResetAdministratorFirstName($customer);
		$customer->lastName = self::getResetAdministratorLastName($customer);
		$customer->administrator->login = self::getResetAdministratorLogin($customer, $typeDatabase);
		$customer->administrator->password = self::getResetAdministratorPassword($customer);
		$customer->language = BM::DEFAULT_LANGUAGE;

		$managerLogin = self::getResetFirstManagerLogin($customer, $typeDatabase);

		if(self::delete($customer) && !self::createFreeTrial($customer, $managerLogin, $managerPasswordIsEqualToLogin = true, $sendMail = false)) {
			return false;
		} else {
			//Build test or demonstration database
			switch($typeDatabase) {
				case Filters\Reset::TYPEOF_DEMONSTRATION:
					die('Demo database to develop');
					break;
				case Filters\Reset::TYPEOF_TEST:
					return self::installTestDatabase($customer, $managerLogin);
					break;
			}
			return false;
		}
	}

	/**
	 * @param Models\Customer $customer
	 * @param string $managerLoginToIgnore
	 * @return bool
	 */
	private static function installTestDatabase(Models\Customer $customer, $managerLoginToIgnore = '') {
		//transform project to sqlData
		$sqlData = Mapper\Customer::toSQL($customer);

		$db = new BoondManager\Customer();
		$customer->id = $db->installTestDatabase($sqlData, $managerLoginToIgnore);
		return $customer->id ? true : false;
	}

	/**
	 * @return string
	 */
	public static function getLogoPath()
	{
		$imgFile = Tools::whichImageWithExtensionExist(self::LOGO . bm::getCustomerCode());
		return $imgFile ? self::LOGO . $imgFile : null;
	}

	/**
	 * Build clientToken
	 * @param string $customerCode
	 * @return string
	 */
	public static function buildToken($customerCode) {
		return Tools::objectID_BM_encode([$customerCode]);
	}

	/**
	 * Build clientKey
	 * @param string $customerCode
	 * @return string
	 */
	public static function buildKey($customerCode)
	{
		return substr(sha1(uniqid() . '*' . $customerCode), 0, 20);
	}

	/**
	 * @param $nbManagers
	 * @return float
	 */
	public static function calculateMaxStorage($nbManagers)
	{
		return $nbManagers * Models\Subscription::DEFAULT_STORAGE_PER_MANAGER;
	}

	/**
	 * @param string $customerCode
	 * @return boolean
	 */
	public static function isCustomerExists($customerCode) {
		return BM::getCustomerInterfaceFilePath($customerCode) ? true : false;
	}

	/**
	 * @param Models\Customer $customer
	 * @return string
	 */
	private static function getResetAdministratorFirstName(Models\Customer $customer) {
		return BM::isDevelopmentMode() ? Models\Customer::DEFAULT_FIRSTNAME : $customer->firstName;
	}

	/**
	 * @param Models\Customer $customer
	 * @return string
	 */
	private static function getResetAdministratorLastName(Models\Customer $customer) {
		return BM::isDevelopmentMode() ? Models\Customer::DEFAULT_LASTNAME : $customer->lastName;
	}

	/**
	 * @param Models\Customer $customer
	 * @return string
	 */
	private static function getResetAdministratorPassword(Models\Customer $customer) {
		return BM::isDevelopmentMode() ? Login::saltPassword($customer->administrator->login, $customer->administrator->login) : $customer->administrator->password;
	}

	/**
	 * @param Models\Customer $customer
	 * @param string $typeDatabase
	 * @return string
	 */
	public static function getResetAdministratorLogin(Models\Customer $customer, $typeDatabase = Filters\Reset::TYPEOF_DEMONSTRATION) {
		if(BM::isDevelopmentMode()) {
			switch($typeDatabase) {
				case Filters\Reset::TYPEOF_DEMONSTRATION:
					$customer->administrator->login = 'adm_demo';
					break;
				case Filters\Reset::TYPEOF_TEST:
					$customer->administrator->login = 'adm_test';
					break;
			}

		}
		return $customer->administrator->login;
	}

	/**
	 * @param Models\Customer $customer
	 * @param string $typeDatabase
	 * @return mixed|string
	 */
	public static function getResetFirstManagerLogin(Models\Customer $customer, $typeDatabase = Filters\Reset::TYPEOF_DEMONSTRATION) {
		$managerLogin = '';
		if(BM::isDevelopmentMode()) {
			$managerLogin = str_replace(' ', '', strtolower(
				substr(Tools::suppr_accents(Models\Customer::DEFAULT_FIRSTNAME),0,1) .
				Tools::suppr_accents(Models\Customer::DEFAULT_LASTNAME)));

			switch($typeDatabase) {
				case Filters\Reset::TYPEOF_DEMONSTRATION:
					$managerLogin = 'demo_'.$managerLogin;
					break;
				case Filters\Reset::TYPEOF_TEST:
					$managerLogin = 'test_'.$managerLogin;
					break;
			}

		}

		return $managerLogin ? $managerLogin : $customer->email;
	}
}
