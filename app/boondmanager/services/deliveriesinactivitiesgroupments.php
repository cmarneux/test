<?php
/**
 * deliveriesinactivitiesgroupments.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\DeliveriesInactivitiesGroupments\Filters;
use BoondManager\APIs\Deliveries\Specifications\HaveReadAccess;
use BoondManager\APIs\Deliveries\Specifications\HaveWriteAccess;
use BoondManager\APIs\Deliveries\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models;
use Wish\Models\SearchResult;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use Wish\Tools;

/**
 * Class Deliveries
 * @package BoondManager\Models\Services
 */
class DeliveriesInactivitiesGroupments{

	/**
	 * @param $id
	 * @return Models\Delivery|Models\Groupment|Models\Inactivity|false
	 */
	public static function find($id)
	{
		$filter = new Filters\SearchDeliveries();
		$filter->keywords->setValue( Models\Delivery::buildReference($id) );

		$sql = Local\Delivery::instance();
		$result = $sql->searchDeliveries($filter);

		if($result->rows) return Mapper\DeliveryInactivityGroupment::fromRow($result->rows[0]);
		else return false;
	}

	/** Search projects
	*
	* @param Filters\SearchDeliveries $filter
	* @return SearchResult
	*/
	public static function search(Filters\SearchDeliveries $filter) {
		$db = Local\Delivery::instance();
		$searchResult = $db->SearchDeliveries($filter);

		return Mapper\DeliveryInactivityGroupment::fromSearchResult($searchResult);
	}

	/**
	 * @param Models\Delivery $delivery
	 * @return bool
	 */
	public static function attachAgenciesConfig(Models\Delivery $delivery) {
		if($delivery->dependsOn instanceof Models\Employee){
			//calendar, workUnitTypes, expensesTypes, name of resource's agency
			$delivery->dependsOn->agency = Agencies::get($delivery->dependsOn->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);

			//Retrieve all agencies on advantages, exceptional scales and expenses details
			$agenciesIDEmployees = [$delivery->dependsOn->agency->id, $delivery->project->agency->id];
			$agenciesIDActivities = [$delivery->dependsOn->agency->id];
			//Retrieve all companies on exceptional scales
			$companiesID = [$delivery->project->company->id];

			foreach($delivery->advantages as $adv) {
				$agenciesIDEmployees[] = $adv->agency->id;
			}

			foreach ($delivery->exceptionalScales as $scale) {
				if($scale->dependsOn instanceof Models\Agency)
					$agenciesIDEmployees[] = $scale->dependsOn->id;

				if($scale->dependsOn instanceof Models\Company)
					$companiesID[] = $scale->dependsOn->id;
			}

			foreach($delivery->expensesDetails as $ed) {
				$agenciesIDActivities[] = $ed->agency->id;
			}

			//All IDs need to be unique
			$agenciesIDEmployees = array_unique($agenciesIDEmployees);
			$agenciesIDActivities = array_unique($agenciesIDActivities);
			$companiesID = array_unique($companiesID);

			//exceptionalScaleTypes, advantageTypes ol all agencies
			$agenciesEmployees = [];
			foreach($agenciesIDEmployees as $aid){
				$agenciesEmployees[$aid] = Agencies::get($aid, Models\Agency::TAB_RESOURCES);
			}

			//calendar, workUnitTypes, expensesTypes, name of all agencies
			array_shift($agenciesIDActivities);
			$agenciesActivities = [
				$delivery->dependsOn->agency->id => $delivery->dependsOn->agency
			];
			foreach($agenciesIDActivities as $aid){
				$agenciesActivities[$aid] = Agencies::get($aid, Models\Agency::TAB_ACTIVITYEXPENSES);
			}

			//exceptionalScales, name of all companies
			$companies = [];
			foreach($companiesID as $cid){
				$companies[$cid] = Companies::get($cid, Models\Company::TAB_SETTING);
			}

			//Merge exceptionalScales, advantageTypes on resource's agency
			$delivery->dependsOn->agency->mergeWith( $agenciesEmployees[$delivery->dependsOn->agency->id] );
			//Merge name, exceptionalScales, advantageTypes on project's agency
			$delivery->project->agency->mergeWith( $agenciesEmployees[$delivery->project->agency->id] );

			//Merge advantageTypes on advantage's agency
			foreach($delivery->advantages as $adv) {
				$adv->agency->mergeWith($agenciesEmployees[$adv->agency->id]);
			}

			//Merge exceptionalScales on each exceptional scale's agency
			//Merge exceptionalScales on each exceptional scale's company
			foreach ($delivery->exceptionalScales as $scale) {
				if($scale->dependsOn instanceof Models\Agency)
					$scale->dependsOn->mergeWith($agenciesEmployees[$scale->dependsOn->id]);

				if($scale->dependsOn instanceof Models\Company)
					$scale->dependsOn->mergeWith($companies[$scale->dependsOn->id]);
			}

			//Merge calendar, workUnitTypes, expensesTypes, name on each expenses details agency
			foreach($delivery->expensesDetails as $ed) {
				$ed->agency->mergeWith($agenciesActivities[$ed->agency->id]);
			}
			return true;
		}

		return false;
	}

	/**
	 * calculate financial data.
	 * It requires the project agency configuration (for the calendar)
	 * @param Models\Delivery $delivery
	 */
	public static function calculateOccupationRate(Models\Delivery $delivery) {
		$days = Tools::getNumberOfWorkingDays($delivery->startDate, $delivery->endDate, ($delivery->dependsOn->agency->calendar) ? $delivery->dependsOn->agency->calendar : CurrentUser::instance()->getCalendar());
		$delivery->occupationRate = $days > 0 ? 100 * ( $delivery->numberOfDaysInvoicedOrQuantity + $delivery->numberOfDaysFree) / $days : 0;
	}

	/**
	 * Build project from filter
	 * @param Filters\Entity $filter
	 * @param Models\Delivery $delivery
	 * @return Models\Delivery
	 */
	public static function buildFromFilter($filter, $delivery = null) {
		if(!$delivery) {
			$delivery = new Models\Delivery();//filter and calculate all projects data
			$delivery->mergeWith( $filter->toObject() );
		}else{
			$delivery->mergeWith($filter->toObject());
		}
		return $delivery;
	}

	/**
	 * Create project
	 * @param Models\Delivery $delivery
	 * @return boolean
	 */
	public static function create(Models\Delivery &$delivery) {
		//transform project to sqlData
		$sqlData = Mapper\Delivery::toSQL($delivery);

		$db = Local\Delivery::instance();
		$delivery->id = $db->createDelivery($sqlData);

		if($delivery->id) {
			$delivery = self::get($delivery->id);
			return true;
		} else return false;
	}

	/**
	 * @param $delivery
	 * @return bool
	 */
	public static function update(&$delivery) {
		//transform project to sqlData
		$sqlData = Mapper\Delivery::toSQL($delivery);

		$db = Local\Delivery::instance();
		$delivery->id = $db->updateDelivery($sqlData, $delivery->id);
		if($delivery->id) {
			$delivery = self::get($delivery->id);
			return true;
		} else return false;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public static function delete($id) {
		$db = Local\Delivery::instance();
		if($db->isDeliveryReducible($id)) {
			$db->deleteDelivery($id);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * @param $delivery
	 * @return Models\Rights
	 */
	public static function getRights($delivery)
	{
		$request = new RequestAccess();
		$request->data = $delivery;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::SUBMODULE_DELIVERY, $delivery);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		$right->addApi('entity', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}
}
