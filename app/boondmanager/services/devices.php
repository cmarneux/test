<?php
/**
 * devices.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace BoondManager\Services;

use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;
use BoondManager\Databases\Mapper;
use BoondManager\Models;
use Wish;

/**
 * Class Apps
 * @package BoondManager\Models\Services
 */
class Devices {
	/**
	 * @return Models\Device[]
	 */
	public static function getAllDevicesFromUser($id){
		$db = new Local\Device();
		$devices = $db->getAllDevicesFromUser($id);
		return Mapper\Device::fromDatabaseArray($devices, Models\Device::class);
	}

	/**
	 * @param integer $id user's id
	 * @param array $matchDeviceList
	 * @param bool $deleteUnused
	 * @return Models\Device|null
	 */
	public static function getFromUserAndCookie($id, $matchDeviceList, $deleteUnused = false)
	{
		$db = new Local\Device();
		$device = $db->getDeviceFromUserAndCookie($id, $matchDeviceList, $deleteUnused);

		return ($device) ? Wish\Mapper::createObject(Models\Device::class, $device) : null;
	}

	/**
	 * @param string $name
	 * @return Models\Device|null
	 */
	public static function createDevice($name)
	{
		$db = new Local\Device();
		$id = $db->createDevice([
			'DALL_NOM' => $name
		]);

		if ($id) {
			return Wish\Mapper::createObject(Models\Device::class, $db->getDevice($id));
		} else return null;
	}
}
