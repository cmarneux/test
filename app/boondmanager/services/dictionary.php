<?php
/**
 * dictionary.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Services;

use Wish\Tools;
use BoondManager\OldModels\DictionaryEntry;

/**
 * Class Dictionary
 * @package BoondManager\Models
 */
class Dictionary {

	private static $defaultFallback;
	private static $mapDictsFromPaths = [];

	/**
	 * @return null|string
	 */
	public static function getSpecificDictPath()
	{
		return \Base::instance()->get('LOCALES') . '_clients/' . BM::getCustomerCode();
	}

	/**
	 * Load a dictionary (language file)
	 *
	 * @param string $key a key from a dictionary
	 * @param string|null $language
	 *
	 * @note
	 * - use the `LANGUAGE` variable from the registyr [\Base](http://fatfreeframework.com/base#Routing) to define wich dictionary to load
	 * - load the dictionary only if not present in the registry
	 * - the directory must be in a subfolder of VHOST_DIRECTORY/F3/boondmanager/dict
	*/
	private static function loadDict($key, $language = null)
	{
		$f3 = \Base::instance();

		list($keyBase, $path) = self::getPath($key);

		$key = self::getFullKey($keyBase, $language);

		if (!$f3->exists($key)) {
			$keyPath = $f3->get('LOCALES').$path;
			$specificDict = self::getSpecificDictPath();
			$lex = $f3->lexicon( ($specificDict ? $specificDict.';' : '') . $keyPath );
			$f3->mset($lex, $key.'.');
		}
	}

	/**
	 * build a F3 Key from an app one
	 * @param string $key
	 * @param string $language
	 * @return string
	 */
	private static function getFullKey($key, $language = null){
		if(!$language) $language = BM::getLanguage();
		$f3 = \Base::instance();
		return "{$f3->get('PREFIX')}$language.$key";
	}

	/**
	 * get a path to a dictionary for a given key
	 * @param string $key
	 * @return array [$prefix, $path]
	 * @throws \Exception
	 */
	private static function getPath($key){
		foreach(self::$mapDictsFromPaths as $prefix => $path){
			if( 0 === strncmp($prefix, $key, strlen($prefix))) return [$prefix, $path];
		}
		throw new \Exception("unable to find a valid dictionary for ".$key);
	}

	/**
	 * load an entry from a dic (and load the dic if needed)
	 * @param string $key
	 * @param string $language
	 * @return mixed
	 */
	private static function extractKeyFromDict($key, $language = null){

		$f3 = \Base::instance();

		$f3Key = self::getFullKey($key, $language);

		if(!$f3->exists( $f3Key )){
			self::loadDict($key, $language);
		}

		return $f3->get($f3Key);
	}

	/**
	 * Retrieve an entry in the dictionary
	 *
	 * @param string $key Key of language data in the F3's hive
	 * @param mixed $param an array of replacement values a single value.
	 * In the case of a single value, it can replace only a `{0}` occurence
	 * @param mixed $language
	 * @return string|array dict entry
	 */
	public static function getDict($key, $param = null, $language = null)
	{
		$entry = self::extractKeyFromDict($key, $language);

		// try a fallback
		if (!$entry && self::$defaultFallback) {
			$path       = explode('.', $key);
			$path[0]    = self::$defaultFallback;
			$defaultKey = implode('.', $path);

			$entry = self::extractKeyFromDict($defaultKey, $language);
		}

		if (!$entry) {
			//TODO: LOG missing entry
			return $language . '.' . $key;
		}

		// replace all variable in the entry
		if ($param) {
			$args = is_array($param) ? $param : [$param];

			// replace all variables matching "{str}"
			$entry = preg_replace_callback('#(\{[\w]+\})#', function ($match) use ($args) {
				$key = trim($match[0], '{}');
				if (is_numeric($key)) $key = intval($key);
				return isset($args[ $key ]) ? $args[ $key ] : $match[0];
			}, $entry);

			// replace all variables matching "{listname|sep}"
			$entry = preg_replace_callback('#(\{[\w]+\|[^\}]+\})#', function ($match) use ($args) {
				list($key, $sep) = explode('|', trim($match[0], '{}'));
				if (is_numeric($key)) $key = intval($key);
				return (isset($args[ $key ]) && is_array($args[ $key ])) ? implode($sep, $args[ $key ]) : $match[0];
			}, $entry);

		}

		return $entry;
	}

	/**
	 * Retrieve values of an entry in the dictionary where label is not empty (label =/= ''), ie a translation of de value exists for the language
	 *
	 * @param string $key Key of language data in the F3's hive
	 * @param string $column = 'id' value column in entry results, knowing that label column is always 'value'
	 * @param mixed $language an array of replacement values a single value.
	 * @return string|array dict entry values
	*/
	public static function getDictTranslatedValues($key, $column = 'id', $language = null)
	{
		//~ TODO : 15-04-2016: Tin : Checker si le nom de cette méthode convient à ce qu'elle fait ...
		$entries = self::getDict($key, null, $language);
		$values = array_column(
			array_filter($entries, function($entry) {
				return $entry['value'] != '';
			})
		, $column);
		return $values;
	}

	/**
	 * test that keys exists in a given dictionary array
	 *
	 * @param mixed $needles single id or array of ids to look for
	 * @param array $dictionary
	 * @return boolean
	 *
	 * @note Example :<br />
	 * <pre><code>
	 * // return true
	 * self::in_dict_array(
	 * 		array(2, 5), array(
	 * 	 		array(0, 'Valeur A', 'Valeur B'),
	 * 			array(2, 'Valeur C', 'Valeur D'),
	 * 	        array(5, 'Valeur E', 'Valeur F')));</code></pre>
	 */
	public static function in_dict_array($needles, $dictionary = array())
	{
		if(!is_array($needles)) $needles = [$needles];
		$etat = false;
		foreach ($needles as $id) {
			$etat = false;
			foreach ($dictionary as $item) if ($item['id'] == $id) {
				$etat = true;
				break;
			}
			if (!$etat) break;
		}
		return $etat;
	}

	/**
	 * get a dictionary key for actions from a category ID
	 * @param int|array $catID a (or an array of) category ID
	 * @return string|array a (or an array of) dictionary key
	 */
	public static function getActionKeyFromCategoryID($catID){
		$prefix = 'specific.setting.action.';
		return Tools::mapData($catID, [
			BM::CATEGORY_OPPORTUNITY => $prefix . 'opportunity',
			BM::CATEGORY_PROJECT     => $prefix . 'project',
			BM::CATEGORY_ORDER       => $prefix . 'order',
			BM::CATEGORY_RESOURCE    => $prefix . 'resource',
			BM::CATEGORY_CANDIDATE   => $prefix . 'candidate',
			BM::CATEGORY_CRM_CONTACT => $prefix . 'contact',
			BM::CATEGORY_CRM_COMPANY => $prefix . 'company',
			BM::CATEGORY_BILLING     => $prefix . 'invoice',
			BM::CATEGORY_APP         => $prefix . 'app',
			BM::CATEGORY_PURCHASE    => $prefix . 'purchase',
			BM::CATEGORY_PRODUCT     => $prefix . 'product',
		]);
	}


	/**
	 * get a dictionary key for states from a category ID
	 * @param int|array $catID a (or an array of) category ID
	 * @return string|array a (or an array of) dictionary key
	 */
	public static function getStateKeyFromCategoryID($catID){
		$prefix = 'specific.setting.state.';
		return Tools::mapData($catID, [
			BM::CATEGORY_CRM_COMPANY => $prefix.'company',
			BM::CATEGORY_OPPORTUNITY => $prefix.'opportunity',
			BM::CATEGORY_PROJECT => $prefix.'project',
			BM::CATEGORY_ORDER => $prefix.'order',
			BM::CATEGORY_PRODUCT => $prefix.'product',
			BM::CATEGORY_PURCHASE => $prefix.'purchase',
			BM::CATEGORY_RESOURCE => $prefix.'resource',
			BM::CATEGORY_CANDIDATE => $prefix.'candidate',
			BM::CATEGORY_POSITIONING => $prefix.'positioning',
			BM::CATEGORY_CRM_CONTACT => $prefix.'contact'
		]);
	}

	/**
	 * define a prefix for a dictionary that should map a directory structure in the dict folder
	 * @param string $namespace
	 *
	 * example: addNamespace('main.application') will add the dictionary in dict/main/application/{en,fr}
	 */
	public static function addNamespace($namespace){
		self::$mapDictsFromPaths[$namespace] = str_replace('.','/',$namespace).'/';
	}

	/**
	 * set an array of namespaces
	 * @param array $namespaces
	 */
	public static function setNamespaces(array $namespaces){
		self::$mapDictsFromPaths = [];
		foreach($namespaces as $n) self::addNamespace($n);
	}

	/**
	 * Prepare a future access from the dictionary. It's used to build a message but delayed the translation
	 * @param string $key
	 * @param array $param
	 * @return \BoondManager\Models\DictionaryEntry
	 */
	public static function prepareEntry($key, $param = null){
		return new \BoondManager\Models\DictionaryEntry($key, $param);
	}

	/**
	 * return an array of key=>values that can be used for a lookup
	 * @param string $entry
	 * @param string $colKey
	 * @param string $colValue
	 * @return array
	 */
	public static function getMapping($entry, $colKey = 'id', $colValue = 'value'){
		return Tools::buildMaping(self::getDict($entry), $colKey, $colValue);
	}

	public static function getActiveStatesIDs($key){
		$entries = self::getDict($key);
		$ids = [];
		foreach($entries as $entry){
			if($entry['active'] && $entry['value']) $ids[] = $entry['id'];
		}
		return $ids;
	}
	/**
	 * lookup for a key in a dict entry and return the value
	 * @param string $entry
	 * @param mixed $key the value to look for
	 * @param mixed $defaultValue the value returned if not found
	 * @param string $colKey
	 * @param string $colValue
	 * @return mixed
	 */
	public static function lookup($entry, $key, $defaultValue = null, $colKey = 'id', $colValue = 'value'){
		$dict = self::getDict($entry);
		foreach($dict as $entry){
			if($key == $entry[ $colKey ]) return $entry[ $colValue ];
		}
		return $defaultValue;
	}

	/**
	 * lookup for a key in a dict entry and return the complete object
	 * @param string $entry
	 * @param mixed $key the value to look for
	 * @param mixed $defaultValue the value returned if not found
	 * @param string $colKey
	 * @return mixed
	 */
	public static function lookupEntry($entry, $key, $defaultValue = null, $colKey = 'id'){
		$dict = self::getDict($entry);
		foreach($dict as $entry){
			if($key == $entry[ $colKey ]) return $entry;
		}
		return $defaultValue;
	}

	public static function setDefaultFallback($string)
	{
		self::$defaultFallback = $string;
	}
}
