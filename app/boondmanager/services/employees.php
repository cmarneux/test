<?php
/**
 * resources.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Services;

use BoondManager\Lib\Specifications\ManagerIsActive;
use BoondManager\Models\Agency;
use BoondManager\Models\Rights;
use Wish\Filters\AbstractJsonAPI;
use Wish\Models\Model;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Models\Employee;
use BoondManager\Models\AbsencesQuota;
use BoondManager\Lib\RequestAccess;
use BoondManager\Databases\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Databases\SolR;
use BoondManager\OldModels\Specifications;
use BoondManager\APIs\Employees\Filters\SearchEmployees;
use BoondManager\APIs\Employees\Specifications\HaveReadAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccess;
use BoondManager\APIs\Employees\Specifications\HaveWriteAccessOnField;
use BoondManager\APIs\Employees\Specifications\IsActionAllowed;

/**
 * Class Resources
 * @package BoondManager\Models\Services
 */
class Employees {
	const RIGHTS_ACTIONS = ['downloadResumes', 'addAction', 'seeProductionForecast', 'downloadTD', 'addOpportunity', 'addPositioning', 'share', 'addProject'];
	const RIGHTS_ATTRIBUTES = ['visibility', 'creationDate'];

	/**
	 * @param int $id
	 * @return Employee|false
	 */
	public static function find($id) {
		$filter = new SearchEmployees();
		$filter->keywords->setValue( Employee::buildReference($id) );

		$sql = new Local\Employee();
		$result = $sql->searchEmployees($filter);

		if($result->rows) return Mapper\Employee::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * Handle the research (using SolR/MySQL)
	 *
	 * @param \BoondManager\APIs\Employees\Filters\SearchEmployees $filter
	 * @param bool $solr Try a research with SolR
	 * @return SearchResult
	 */
	public static function search(SearchEmployees $filter, $solr = true)
	{
		// si la recherche se passe sur un PROJET/CONTACT CRM/SOCIETE CRM ALORS alors on désactive la recherche SolR
		if($solr && preg_match('/(^|\s)(PRJ|CCON|CSOC|AO)[0-9]+/i', $filter->keywords->getValue()))
			$solr = false;

		// si on recherche sur une periode autre que celle par défault => idem
		if($solr && !$filter->isDefaultPeriod() && $filter->period->getValue() != SearchEmployees::PERIOD_AVAILABLE)
			$solr = false;

		// si pas de recherche SolR ou
		// pas de données trouvée dans solr (pas synchro, outdated ?), on cherche dans mysql
		if($solr) $solrResult = self::solrSearch($filter);

		if(!isset($solrResult) || $solrResult === false){
			$searchResult = self::mysqlSearch($filter);
			return Mapper\Employee::fromSearchResult($searchResult);
		}

		// sinon (recherche SolR ET données trouvée)
		// on va récupérer les données sql des données trouvées
		$IDs = array();
		foreach($solrResult->rows as $doc)
			$IDs[] = 'COMP'.$doc['ID_PROFIL'];

		// vide les parametres sauf 'oc', 'ot' et redefinie des mots cles (ids de competences)
		$simpleFilter = clone $filter;
		$simpleFilter->reset( $except=['order', 'sort', 'returnHRManager'] ); //le filtre returnHRManager est exclu du reset car il est nécessaire dans la recherche mysql (ajout de colonnes en fonction de ce filtre)
		$simpleFilter->keywords->setValue(implode(' ', $IDs));
		// reconstruit les valeurs par default pour tout les filtres remits a zero
		$simpleFilter->isValid();

		// on récupère les fiches correspondantes dans mysql
		$mysqlResult = self::mysqlSearch($simpleFilter);

		$searchResult = self::mergeData($solrResult, $mysqlResult);

		return Mapper\Employee::fromSearchResult($searchResult);
	}

	/**
	 * merge results from SolR and MySQL
	 *
	 * @param SearchResult $solrResults
	 * @param SearchResult $mysqlResults
	 * @return SearchResult
	 */
	private static function mergeData($solrResults, $mysqlResults){
		$contextID = Tools::getContext()->startTiming('merge result');
		$result = new SearchResult();
		$result->total = $solrResults->total;
		$result->solrSearch = true;
		//var_dump($solrResults, $mysqlResults);
		$sqlRows = $mysqlResults->rows;
		foreach($solrResults->rows as $doc) {
			$idState = -1;
			// recuperation de la cle mysql qui match le profil solr
			foreach($sqlRows as $i => $row)
				if($row['ID_PROFIL'] == $doc['ID_PROFIL']) {
					$idState = $i;
					break;
				}
			if($idState >= 0) {
				// test si La ressource a été modifiée sur MySQL et pas encore sur SolR
				if($sqlRows[$idState]['PROFIL_DATEUPDATE'] != str_replace(array('T','Z'),array(' ',''), $doc['PROFIL_DATEUPDATE'])) {
					//On change à minima son nom & prénom
					$sqlRows[$idState]['PROFIL_NOM'] = $doc['PROFIL_NOM'];
					$sqlRows[$idState]['PROFIL_PRENOM'] = $doc['PROFIL_PRENOM'];
					$sqlRows[$idState]['CURRENT_UPDATE'] = true;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}else{
					$sqlRows[$idState]['CURRENT_UPDATE'] = false;
					$sqlRows[$idState]['CURRENT_DELETE'] = false;
				}
				$result->rows[$idState] = $sqlRows[$idState];
			} else {
				$data = new Employee();
				$data->fromArray([
					'ID_PROFIL' => $doc['ID_PROFIL'],
					'PROFIL_NOM' => $doc['PROFIL_NOM'],
					'PROFIL_PRENOM' => $doc['PROFIL_PRENOM'],
					'CURRENT_UPDATE' => false,
					'CURRENT_DELETE' => true
				]);
				$result->rows[] = $data;
			}
		}
		$result->rows = array_values($result->rows);
		Tools::getContext()->endTiming($contextID);
		return $result;
	}

	/**
	 * Perform the solr search with the given parameters
	 * @param SearchEmployees $filter
	 * @return SearchResult
	 */
	private static function solrSearch(SearchEmployees $filter)
	{
		$contextID = Tools::getContext()->startTiming('solr search');
		$solr = new SolR\Resources();
		$result = $solr->search($filter);
		Tools::getContext()->endTiming($contextID);
		return $result;
	}

	/**
	 * Perform the mysql search with the given parameters
	 * @param SearchEmployees $filter
	 * @return SearchResult
	 */
	private static function mysqlSearch(SearchEmployees $filter)
	{
		$contextID = Tools::getContext()->startTiming('mysql search');
		$db = new Local\Employee();
		$result = $db->searchEmployees($filter);
		Tools::getContext()->endTiming($contextID);
		return $result;
	}

	/**
	 * Retrieve all managers
	 *
	 * @param boolean $only_active If `true` it retrieve only managers having an active account
	 * @return \BoondManager\Models\Account[]
	 */
	public static function getAllGroupManagers($only_active = true){
		$db = new Local\Manager();
		return $db->getSpecificGroupManagers($only_active);
	}

	/**
	 * @param \Wish\Models\Model[] $managers list of managers
	 * @param integer[] $ids list of ids
	 * @param boolean $isProfil `true` if ids are profil ids' , `false` if they are user ids'
	 * @param boolean $only_active keep active managers only
	 * @return \BoondManager\Models\Account[]
	 * @throws \Exception
	 */
	private static function extendsManagerWithIDs($managers, $ids, $isProfil, $only_active){

		$managerDB = new Local\Manager();

		$item = $managerDB::getRefIdColumn($isProfil);
		$searchManagers = Tools::getFieldsToArray($managers, $item);

		//On vérifie si les managers a ajouter ne sont pas déjà chargé
		$ids = array_filter($ids, function($id) use ($searchManagers){
			return !$id || !in_array($id, $searchManagers);
		});

		// add specifics managers
		$data = $managerDB->getManagers($ids);

		$fullList = array_merge($managers, $data);

		// filters active if required
		if($only_active) {
			$fullList = Tools::filterArrayWithSpec($fullList, new ManagerIsActive() );
		}

		return $fullList;
	}

	/**
	 * @param Model[] $managers list of managers
	 * @param integer[] $ids list of profil ids'
	 * @param bool $activeOnly
	 * @return Model[]
	 */
	public static function extendManagersWithProfilIDs(array $managers, array $ids, $activeOnly = false){
		return self::extendsManagerWithIDs($managers, $ids, true, $activeOnly);
	}

	/**
	 * @param Model[] $managers list of managers
	 * @param integer[] $ids list of user ids'
	 * @param bool $activeOnly
	 * @return \Wish\Models\Model[]
	 */
	public static function extendManagersWithUserIDs(array $managers, array $ids, $activeOnly = false){
		return self::extendsManagerWithIDs($managers, $ids, false, $activeOnly);
	}

	/**
	 * load a profil from an ID
	 * @param int $id profil ID
	 * @param int $tab sub part of the profil
	 * @return \BoondManager\Models\Employee|false
	 */
	public static function get($id, $tab = Employee::TAB_DEFAULT){
		$db = Local\Employee::instance();
		$entity = $db->getObject($id, $tab);

		if(!$entity) return null;
		else $entity = Mapper\Employee::fromSQL($entity, $tab);

		switch ($tab){
			case Employee::TAB_ABSENCESACCOUNTS:
				$agency = Agencies::get($entity->agency->id, Agency::TAB_ACTIVITYEXPENSES);
				if($agency) $entity->agency = $agency;
				$entity->absencesAccounts = Absences::getAllQuotas($entity->id);
				break;
			case Employee::TAB_SETTING_INTRANET:
				$config = $db->getProfilTpsFrsData($entity->id);
				if($config) $entity->mergeWith( Mapper\Employee::fromConfiguration($config) );
				$entity->agency = Agencies::get($entity->agency->id, Agency::TAB_ACTIVITYEXPENSES);
				break;
			case Employee::TAB_SETTING_SECURITY:
				$entity->calculateUserToken();
				$entity->devices = Devices::getAllDevicesFromUser($entity->userId);
				// FIXME (when login apps are ready): see applications lines 396 of v6's file configurationressource.php
				$entity->googleThirdPartyExists = false;
				$entity->microsoftThirdPartyExists = false;
				$entity->trustelemThirdPartyExists = false;
				break;
		}

		return $entity;
	}

	/**
	 * Create an empty profile for creation.
	 *
	 * @return \BoondManager\Models\Employee
	 */
	public static function getNewProfil(){
		$user = CurrentUser::instance();

		$currentAccount = clone $user->getAccount();

		$object = new Employee([
			'id'                             => 0,
			'currencyAgency'                 => $user->getCurrency(),
			'exchangeRateAgency'             => $user->getExchangeRate(),
			'currency'                       => $user->getCurrency(),
			'exchangeRate'                   => 1,
			'state'                          => Employee::STATE_CURRENT,
			'mainManager'                    => $currentAccount,
			'hrManager'                      => $currentAccount,
			'agency'                         => clone $currentAccount->agency,
			'pole'                           => $currentAccount->pole ? clone $currentAccount->pole : null,
			'civility'                       => 0,
			'resourceCanModifyTechnicalData' => false,
			'typeOf'                         => Employee::TYPE_INTERNAL_RESOURCE,
			'visibility'                     => true,
			'availability'                   => true,
			'dateCreation'                   => date('Y-m-d'),
			'country'                        => $user->getCountry()
		]);
		return $object;
	}

	/**
	 * create a new resource
	 * @param Employee $employee
	 * @return int resource ID
	 * @internal param array $data
	 */
	public static function create(&$employee)
	{
		$sqlData = Mapper\Employee::toSQL($employee, Employee::TAB_INFORMATION);

		$db = Local\Employee::instance();
		$id = $db->setObject($sqlData);

		if($id) {
			$employee = self::get($id, Employee::TAB_INFORMATION);
			return true;
		} else {
			return false;
		}
	}

	public static function deleteResource(Employee $profil)
	{
		$db = Local\Employee::instance()
		;
		if($db->isResourceReducible($profil->id)) {
			$db->deleteResourceData($profil->id, $profil->ID_DT);

			Notification\Employee::delete($profil);
			return true;
		}else{
			return false;
		}
	}

	public static function buildFromFilter(AbstractJsonAPI $filter, Employee $employee = null) {
		if(!$employee) $employee = new Employee();

		$employee->backupData();
		$employee->mergeWith($filter->toObject());

		return $employee;
	}

	/**
	 * Update some data on a resource
	 * @param \BoondManager\Models\Employee $profil resource profile
	 * @param string $tab
	 * @return Employee
	 */
	public static function updateProfil(Employee &$profil, $tab = BM::TAB_DEFAULT)
	{
		$dbEmployee = Local\Employee::instance();
		$dbManagers = Local\Manager::instance();

		$sqlData = Mapper\Employee::toSQL($profil, $tab);
		if ($profil->isManager()) {
			$dbManagers->updateManager($sqlData, $profil->id, $profil->ID_DT);
		}
		else {
			$dbEmployee->updateEmployee($sqlData, $profil->id, $profil->ID_DT);
		}

		/*
		if($data instanceof \BoondManager\APIs\Employees\Filters\SaveQuotas){
			$tab = Employee::TAB_SETTING_ABSENCESACCOUNTS;

			self::performQualityCheckOnData($profil, $data);

			$sqlData = [];
			foreach ($data as $absencesQuota) $sqlData[] = self::transformFilterDataToSQLData($absencesQuota);
			(new Local\Absence)->updateQuotas($sqlData, $profil->id);
		}else{
			throw new \Exception('wrong filter (instance of '.get_class($data).' given)');
		}
		*/

		Notification\Employee::update($profil, $tab);

		$profil = self::get($profil->id, $tab);

		return $profil;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param \BoondManager\Models\Employee $entity
	 * @return array
	 */
	public static function getVisibleTabs(Employee $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Employee::getAllTabs();
		$readSpec = new HaveReadAccess(Employee::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param \BoondManager\Models\Employee $entity
	 * @return array
	 */
	public static function getWritableTabs(Employee $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Employee::getAllTabs();
		$readSpec = new HaveWriteAccess(Employee::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of available actions
	 * @param \BoondManager\Models\Employee $entity
	 * @return array
	 */
	public static function getAvailableActions(Employee $entity) {
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$actions = [];
		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$actions[$action] = ($spec->isSatisfiedBy($request));
		}
		return $actions;
	}

	public static function getDisabledFields(Employee $entity) {
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$fields = [];
		foreach(self::RIGHTS_ATTRIBUTES as $field){
			$spec = new HaveWriteAccessOnField($field);
			$fields[$field] = ($spec->isSatisfiedBy($request));
		}
		return $fields;
	}

	/**
	 * get the api uri from a resource id and a tab id
	 * @param int $resourceId
	 * @param string $tabId
	 * @return string
	 */
	public static function getApiUri($resourceId, $tabId = Employee::TAB_INFORMATION){
		$prefix = '/resources/'.$resourceId;
		return Tools::mapData($tabId,[
			Employee::TAB_INFORMATION      => $prefix.'/information',
			Employee::TAB_ADMINISTRATIVE   => $prefix.'/administrative',
			Employee::TAB_TECHNICALDATA               => $prefix.'/td',
			Employee::TAB_ACTIONS          => $prefix.'/actions',
			Employee::TAB_POSITIONINGS     => $prefix.'/positionings',
			Employee::TAB_DELIVERIES       => $prefix.'/deliveries',
			Employee::TAB_TIMESREPORTS       => $prefix.'/timesheets',
			Employee::TAB_EXPENSESREPORTS         => $prefix.'/expenses',
			Employee::TAB_ABSENCESREPORTS         => $prefix.'/absences',
			Employee::TAB_ABSENCESACCOUNTS => $prefix.'/absencesaccounts'
		]);
	}


	/**
	 * Get Current User's rights for a given employee
	 * @param Employee $employee
	 * @return Rights
	 */
	public static function getRights(Employee $employee)
	{
		$request = new RequestAccess();
		$request->data = $employee;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Rights(CurrentUser::instance(), BM::MODULE_RESOURCES, $employee);

		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Employee::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		$fields = self::getDisabledFields($employee);
		foreach($fields as $field=>$test) {
			$right->addField($field, true, $test);
		}

		return $right;
	}

	/**
	 * @param Employee $profil
	 * @param RequestAccess $requestAccess
	 * @return array
	 */
	public static function getWarnings(Employee $profil, RequestAccess $requestAccess)
	{
		$warning = [];
		if( !$profil->hasContract() && (new HaveWriteAccess(Employee::TAB_ADMINISTRATIVE))->isSatisfiedBy($requestAccess) ) {
			$warning[] = [
				'code'=> BM::WARNING_RESOURCE_NO_HR_CONTRACT,
				'detail'=> Dictionary::getDict('main.warning.'.BM::WARNING_RESOURCE_NO_HR_CONTRACT)
			];
		}
		return $warning;
	}

	public static function getWithUserConfig($id){
		$db = Local\TimesExpensesAbsences::instance();//Employee::instance();
		$data = $db->getProfilTpsFrsData($id);

		return $data ? Mapper\Employee::fromConfiguration($data) : null;
	}

	/**
	 * @param Employee $entity
	 */
	public static function attachUserConfig(Employee &$entity)
	{
		$employee = self::getWithUserConfig($entity->id);
		$entity->mergeWith($employee);
	}

	/**
	 * @param $id
	 * @param $startDate
	 * @param null $endDate
	 * @return \BoondManager\Models\Project[]
	 */
	public static function getAllProjects($id, $startDate, $endDate = null){
		if(!$endDate) {
			$date = explode('-',  $startDate);
			$nbJours = date('t', mktime(0,0,0,$date[1], 1, $date[0]));
			$endDate = date('Y-m-d', mktime($date[1], $nbJours, $date[0]));
		}

		$db = Local\Delivery::instance();
		$data = $db->searchDeliveriesAndBatchesForEmployeeOnPeriod($id, $startDate, $endDate);

		return Mapper\Delivery::fromSearchDeliveriesAndBatchesForEmployeeOnPeriod($data);
	}

	public static function buildListAbsences(Employee $entity) {
		$workUnitTypes = [];
		foreach($entity->agency->workUnitTypes as $wut) {
			if($wut->isAbsence()) $workUnitTypes[] = [
				'agency'    => strval($entity->agency->id),
				'reference' => strval($wut->reference),
				'name'      => strval($wut->name)
			];
		}

		$agencyLookup = [];
		foreach($entity->workUnitTypesAllowedForAbsences as $item) {
			list($ref, $agencyId) = $item;
			if($agencyId == $entity->agency->id) continue;
			if(!isset($agencyLookup[$agencyId])) $agencyLookup[$agencyId] = [$ref];
			else $agencyLookup[$agencyId][] = $ref;
		}

		foreach($agencyLookup as $aID => $refs) {
			$agency = Agencies::get($aID, Agency::TAB_ACTIVITYEXPENSES);
			foreach($agency->workUnitTypes as $wut) {
				if($wut->isAbsence() && in_array($wut->reference, $refs)) $workUnitTypes[] = [
					'agency'    => strval($aID),
					'reference' => strval($wut->reference),
					'name'      => strval($wut->name)
				];
			}
		}

		return $workUnitTypes;
	}
}
