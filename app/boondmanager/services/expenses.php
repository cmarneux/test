<?php
/**
 * expenses.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;
use Wish\Models\SearchResult;
use Wish\MySQL\AbstractDb;
use BoondManager\OldModels\Filters;

/**
 * Class Expenses
 * @package BoondManager\Models\Services
 */
class Expenses{

    /**
     * Handle the research MySQL
     *
     * @param Filters\Search\Expenses $filter
     * @return \Wish\Models\SearchResult
     */
    public static function search(Filters\Search\Expenses $filter)
    {
        $db = new \BoondManager\Databases\Local\Expense();
        return $db->search($filter);
    }
}
