<?php
/**
 * expensesreports.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\ExpensesReports\Filters\SearchExpensesReports;
use BoondManager\APIs;
use BoondManager\APIs\ExpensesReports\Specifications\HaveReadAccess;
use BoondManager\APIs\ExpensesReports\Specifications\HaveWriteAccess;
use BoondManager\APIs\ExpensesReports\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\OldModels\Filters;
use BoondManager\Models;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Class ExpensesReports
 * @package BoondManager\Models\Services
 */
class ExpensesReports{

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchExpensesReports $filter
	 * @param bool $includeExpensesDetails include expenses details
	 * @return SearchResult
	 * @throws \Exception
	 */
	public static function search(SearchExpensesReports $filter, $includeExpensesDetails = false)
	{
		$db = Local\ExpensesReport::instance();
		$result = $db->search($filter);

		if($includeExpensesDetails){
			foreach($result->rows as $expensesReport){
				/** @var Models\ExpensesReport $expenses */
				$expenses = self::getExpenses($expensesReport->ID_LISTEFRAIS);
				//~ actual expenses ->
				$expensesReport->actualExpenses = $expenses['actualExpenses'];
				//~ fixed expenses ->
				$expensesReport->fixedExpenses = $expenses['fixedExpenses'];
			}
		}

		return Mapper\ExpensesReport::fromSearchResult($result);
	}

	/**
	 * loads an entity from an ID
	 * @param int $id entity ID
	 * @return Models\ExpensesReport|false
	 */
	public static function get($id){
		$db = Local\ExpensesReport::instance();
		$entity = $db->getObject($id);

		if(!$entity) return false;

		$expenseReport = Mapper\ExpensesReport::fromSQL($entity);

		// config for user & agency
		Employees::attachUserConfig($expenseReport->resource);
		$expenseReport->agency = Agencies::get($expenseReport->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);
		$expenseReport->calculateValidationWorkflow();

		$expenseReport->projects = Employees::getAllProjects($expenseReport->resource->id, $expenseReport->getStartPeriod(), $expenseReport->getEndPeriod());

		$pIds = Tools::getFieldsToArray($entity->projects, 'id');
		$expenseReport->orders = Orders::searchMensualOrdersForProjectsOfAResourceOnPeriod($expenseReport->resource->id, $pIds, $expenseReport->getStartPeriod(), $expenseReport->getEndPeriod());

		// filtrage des bdc selon les droits
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());
		$haveWriteAccess = new APIs\Orders\Specifications\HaveWriteAccess();
		$expenseReport->orders = array_filter($expenseReport->orders, function(Models\Order $val) use ($haveWriteAccess, $request){
			$request->setData($val);
			return $haveWriteAccess->isSatisfiedBy($request);
		});

		return $expenseReport;
	}

	/**
	 * loads expensesreport's expenses from an ID
	 * @param int $id entity ID
	 * @return [ 'actualExpenses' => MySQL\RowObject\Expense[], 'fixedExpenses' => MySQL\RowObject\Expense[] ]
	 */
	public static function getExpenses($id){
		$expenses = [];
		//~ actual expenses ->
		$filter = Filters\Search\Expenses::getUserFilter();
		$filter->disableMaxResultLimit();
		$filter->keywords->setValue('RPT'.$id);
		$filter->category->setValue('actual');
		$expenses['actualExpenses'] = Expenses::search($filter)->rows;
		//~ fixed expenses ->
		$filter->category->setValue('fixed');
		$expenses['fixedExpenses'] = Expenses::search($filter)->rows;

		return $expenses;
	}

	/**
	 * Create an empty expensesreport for creation.
	 *
	 * @param APIs\ExpensesReports\Filters\GetDefault $filter
	 * @return Models\ExpensesReport
	 */
	public static function getNew(APIs\ExpensesReports\Filters\GetDefault $filter){

		$resource = Employees::get($filter->resource->getValue(), Models\Employee::TAB_TIMESREPORTS);
		Employees::attachUserConfig($resource);

		$agencyID = $filter->agency->getValue();
		if(!$agencyID) $agencyID = $resource->agency->id;

		$agency = Agencies::get($agencyID, Models\Agency::TAB_ACTIVITYEXPENSES);

		$entity = new Models\ExpensesReport([
			'id'           => 0,
			'term'         => $filter->term->getValue(),
			'fixedExpenses'	=> [],// TODO : Tin : à déterminer
			'resource'     => $resource,
			'agency'       => $agency,
		]);

		$entity->projects = Employees::getAllProjects($entity->resource->id, $entity->getStartPeriod(), $entity->getEndPeriod());

		$pIds = Tools::getFieldsToArray($entity->projects, 'id');
		$entity->orders = Orders::searchMensualOrdersForProjectsOfAResourceOnPeriod($entity->resource->id, $pIds, $entity->getStartPeriod(), $entity->getEndPeriod());

		// filtrage des bdc selon les droits
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());
		$haveWriteAccess = new APIs\Orders\Specifications\HaveWriteAccess();
		$entity->orders = array_filter($entity->orders, function(Models\Order $val) use ($haveWriteAccess, $request){
			$request->setData($val);
			return $haveWriteAccess->isSatisfiedBy($request);
		});

		return $entity;
	}

	/**
	 * create a new expensesreport
	 * @param Models\ExpensesReport $entity
	 * @return bool
	 */
	public static function create(&$entity) {
		$sqlData = Mapper\ExpensesReport::toSQL($entity);

		$db = Local\ExpensesReport::instance();
		if($id = $db->setObject($sqlData)) {
			$entity = self::get($id);
			return true;
		} else return false;
	}

	/**
	 * @param Models\ExpensesReport $entity
	 * @return bool
	 */
	public static function update(Models\ExpensesReport &$entity) {
		$sqlData = Mapper\ExpensesReport::toSQL($entity);

		$db = Local\ExpensesReport::instance();
		if($db->setObject($sqlData, $entity->id)) {
			$entity = self::get($entity->id);
			return true;
		} else {
			return false;
		}
	}

	public static function delete(Models\ExpensesReport $entity)
	{
		$db = Local\ExpensesReport::instance();

		if($db->canDeleteObject($entity->id)) {
			return $db->deleteObject($entity->id);

			//~ Notification\ExpensesReport::getInstance($entity->ID_LISTEFRAIS, '', [], [])->delete();
			//~ return true;
		}else{
			return false;
		}
	}

	/**
	 * @param Models\ExpensesReport[] $rows
	 * @param string $start Y-m
	 * @param string $end Y-m
	 * @return array
	 */
	public static function getMissingMonths($rows, $start, $end){
		list( $yearStart, $monthStart ) = explode('-', $start);
		list( $yearEnd, $monthEnd ) = explode('-', $end);
		//$missing = [$start];

		$tEnd = mktime(0,0,0, $monthEnd, 1, $yearEnd);
		$m = 0;

		do{
			$cursor = mktime(0, 0, 0, $monthStart + (++$m), 0, $yearStart);
			$missing[] = date('Y-m', $cursor);
		}while($cursor < $tEnd);

		$exists = [];

		foreach($rows as $r){
			$exists[] = $r->term;
		}

		return array_values(array_diff($missing, $exists));
	}

	public static function getRights($timesReport)
	{
		$request = new RequestAccess();
		$request->data = $timesReport;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_ACTIVITIES_EXPENSES, $timesReport);

		foreach(IsActionAllowed::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		$right->addApi('default', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}

	/**
	 * @param APIs\ExpensesReports\Filters\SaveInformation $filter
	 * @param Models\ExpensesReport|null $entity
	 * @return Models\ExpensesReport
	 */
	public static function buildFromFilter(APIs\ExpensesReports\Filters\SaveInformation $filter, Models\ExpensesReport $entity = null) {
		if(!$entity) $entity = new Models\ExpensesReport();
		$entity->mergeWith($filter->toObject());
		return $entity;
	}
}
