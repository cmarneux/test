<?php
/**
 * absencesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */



namespace BoondManager\Services\Extraction;

use Wish\CSV\CSV;
use Wish\MySQL\SearchResult;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\AbsencesReport;
use BoondManager\Services;

class AbsencesReports extends AbstractExtraction{

	private $fullExtract;

	public function __construct($name, $encoding, $full){
		$this->fullExtract = $full;
		parent::__construct($name, $encoding);
	}

	private function isSimpleExtract(){
		return !$this->fullExtract;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		$result = Services\AbsencesReports::search($filter,  $includeDetails = true);

		if ($this->isSimpleExtract()) {
			$result->rows = $this->prepareDataForSimpleExtract($result->rows);
		} else {
			$result->rows = $this->prepareDataForDetailedExtract($result->rows);
		}

		return $result;
	}

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		if($this->isSimpleExtract()){
			$mapping = $this->getColumnsForSimpleExtract();
		}else{
			$mapping = $this->getColumnsForDetailedExtract();
		}

		$columns = [];
		foreach($mapping as $key=>$value) {

			if (is_int($key)){
				$column = new CSV\Column($value, Dictionary::getDict('absencesreports.sqlLabels.' . $value));
			}else{
				$column = new CSV\Column($key, Dictionary::getDict('absencesreports.sqlLabels.' . $key), $value);
			}

			$columns[] = $column;
		}

		return $columns;
	}

	protected function getColumnsForSimpleExtract(){

		$profilesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');

		$mapping = [
			'PROFIL_NOM',
			'PROFIL_PRENOM',
			'PROFIL_REFERENCE',
			'PROFIL_TYPE' => function($value) use($profilesTypes){
				return Tools::mapData($value, $profilesTypes);
			},
			'LISTEABSENCES_DATE' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PABS_DUREE' => function($value){
				return str_replace('.', ',', $value);
			}
		];

		return $mapping;
	}

	protected function getColumnsForDetailedExtract(){

		$profilesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');

		$mapping = [
			'PROFIL_NOM',
			'PROFIL_PRENOM',
			'PROFIL_REFERENCE',
			'PROFIL_TYPE' => function($value) use($profilesTypes){
				return Tools::mapData($value, $profilesTypes);
			},
			'LISTEABSENCES_DATE' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PABS_TYPEHREF',
			'TYPEH_NAME',
			'PABS_DEBUT' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PABS_FIN' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PABS_DUREE' => function($value){
				return str_replace('.', ',', $value);
			},
		];

		return $mapping;
	}

	private function prepareDataForSimpleExtract($data){

		$flattenResult = [];

		foreach($data as $report) {
			/** @var AbsencesReport $report */

			$row = [
				'PROFIL_NOM'         => $report->resource->lastName,
				'PROFIL_PRENOM'      => $report->resource->firstName,
				'PROFIL_REFERENCE'   => $report->resource->reference,
				'PROFIL_TYPE'        => $report->resource->typeOf,
				'LISTEABSENCES_DATE' => $report->creationDate,
				'PABS_DUREE'         => 0
			];

			if(is_array($report->absencesPeriods)) {
				foreach ($report->absencesPeriods as $absence) {
					$row['PABS_DUREE'] += $absence['PABS_DUREE'];
				}
			}

			$flattenResult[] = $row;
		}

		return $flattenResult;
	}

	private function prepareDataForDetailedExtract($data){

		$flattenResult = [];

		foreach($data as $report) {
			/** @var AbsencesReport $report */

			$rowReport = [
				'PROFIL_NOM'         => $report->resource->lastName,
				'PROFIL_PRENOM'      => $report->resource->firstName,
				'PROFIL_REFERENCE'   => $report->resource->reference,
				'PROFIL_TYPE'        => $report->resource->typeOf,
				'LISTEABSENCES_DATE' => $report->creationDate,
			];

			if(is_array($report->absencesPeriods)) {
				foreach ($report->absencesPeriods as $absence) {

					$row = array_filter($absence, function ($k) {
						return in_array($k, [
							'TYPEH_NAME', 'PABS_TYPEHREF', 'PABS_DEBUT', 'PABS_FIN', 'PABS_DUREE'
						]);
					}, ARRAY_FILTER_USE_KEY);

					$row = array_merge($row, $rowReport);

					$flattenResult[] = $row;
				}
			}
		}

		return $flattenResult;
	}
}
