<?php
/**
 * abstractextraction.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use Wish\CSV\CSV;
use Wish\MySQL\SearchResult;
use BoondManager\Services\BM;
use BoondManager\Lib\Filters\AbstractSearch;
use Wish\Tools;

abstract class AbstractExtraction{

	protected $csv;
	protected $timelimit = 3600;
	protected $rows = 500;

	public function __construct($name, $encoding)
	{
		$this->csv = new CSV($name);
		$this->csv->setEncoding($encoding);
		$columns = $this->columnDefinition();
		foreach($columns as $c)
			$this->csv->addColumn($c);
	}

	/**
	 * set a timeout in seconds
	 * @param int $limit
	 */
	public function setTimelimit($limit){
		$this->timelimit = $limit;
	}

	/**
	 * @return CSV\Column[]
	 */
	abstract protected function columnDefinition();

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	abstract protected function loadData($filter);

	/**
	 * send the file throught the output
	 * @param AbstractSearch $filter
	 */
	public function extract($filter){

		set_time_limit($this->timelimit);

		$filter->maxResults->setValue($this->rows);

		$start = 0;

		$this->csv->download();
		$this->csv->addHeader();

		do {
			$filter->page->setValue(++$start);
			$id = Tools::getContext()->startTiming('load & prepare data for extraction');
			$result = $this->loadData($filter);
			Tools::getContext()->endTiming($id);

			$id = Tools::getContext()->startTiming('add rows to CSV');
			foreach ($result->rows as $r) {
				$this->csv->addRow($r);
			}
			Tools::getContext()->endTiming($id);

			// force downloading right away to keep the memory low and don't keep waiting the user
			$this->csv->flush();
		} while ($result->total >= $filter->page->getValue() * $filter->maxResults->getValue());

		$this->csv->close();

	}
}
