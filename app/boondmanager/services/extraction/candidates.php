<?php
/**
 * candidates.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use BoondManager\Models\Candidate;
use BoondManager\Models\Account;
use Wish\CSV;
use BoondManager\Lib\Currency;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

class Candidates extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$states = Dictionary::getMapping('specific.setting.state.candidate');
		$dictDomain = Dictionary::getDict('specific.setting.activityArea');
		$source = Dictionary::getMapping('specific.setting.source');

		$domains = [];
		foreach($dictDomain as $cat=>$values)
			$domains = array_merge($domains, $values['option']);

		$domains = Tools::buildMaping($domains, 'id', 'value');

		$formations = Dictionary::getMapping('specific.setting.training');
		$experiences = Dictionary::getMapping('specific.setting.experience');

		$situation = Dictionary::getMapping('specific.setting.situation');
		$contracts = Dictionary::getMapping('specific.setting.typeOf.contract');

		$devise = new Currency();

		$mapping = [
			'id' => function($value, $row){
				/** @var Candidate $row */
				return $row->getReference();
			},
			'reference',
			'civility'=> function($value){
				return Dictionary::lookup('specific.setting.civility', $value);
			},
			'lastName',
			'firstName',
			'title',
			'skills',
			'activityAreas' => function($value) use ($domains){
				if(!$value) $value = [];
				if(!is_array($value)) throw new \Exception('activityAreas must be an array and not a '.gettype($value));

				$value = array_map(function($value) use ($domains){
					return Tools::mapData($value, $domains);
				}, $value);
				return implode(' ',$value);
			},
			'training' => function($value) use ($formations){
				return Tools::mapData($value, $formations);
			},
			'experience' => function($value) use ($experiences){
				return Tools::mapData($value, $experiences);
			},
			'availability' => function($value){
				if($value==1){
					return Dictionary::lookup('specific.setting.availability', 0); //immédiate
				}else return $value;
			},
			'state' => function($value) use ($states){
				return Tools::mapData($value, $states);
			},
			'PARAM_TYPESOURCE' => function($value, $row) use ($source){
				/** @var Candidate $row */
				return Tools::mapData($row->source->typeOf, $source);
			},
			'PARAM_SOURCE' => function($value, $row) use ($source){
				/** @var Candidate $row */
				return Tools::mapData($row->source->detail, $source);
			},
			'email1','email2','email3','phone1','phone2','phone3',
			'dateOfBirth',
			'nationality',
			'situation' => function($value) use ($situation){
				return Tools::mapData($value, $situation);
			} ,
			'numberOfActivePositionings', 'address', 'postcode', 'town', 'country',
			'currency' => function($value, $row) use($devise){
				/** @var Candidate $row */
				return $devise->getName($row['PARAM_DEVISE']);
			},
			'actualSalary' => function($value, $row) use($devise){
				/** @var Candidate $row */
				$devise->setDefault($row->currencyAgency, $row->exchangeRateAgency);
				return $devise->getAmountForUI($value, $row->exchangeRate);
			},
			'PARAM_TARIF2' => function($value, $row) use($devise){
				/** @var Candidate $row */
				$devise->setDefault($row->currencyAgency, $row->exchangeRateAgency);
				return $devise->getAmountForUI($row->desiredSalary->min, $row->exchangeRate);
			},
			'PARAM_TARIF3' => function($value, $row) use($devise){
				/** @var Candidate $row */
				$devise->setDefault($row->currencyAgency, $row->exchangeRateAgency);
				return $devise->getAmountForUI($row->desiredSalary->max, $row->exchangeRate);
			},

			'desiredContract' => function($value) use ($contracts){
				return Tools::mapData($value, $contracts);
			},

			'mainManager' => function($value){
				/** @var Account $value */
				return $value->getFullName();
			},
			'hrManager' => function($value){
				/** @var Account $value */
				return $value->getFullName();
			}
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('candidates.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('candidates.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param \BoondManager\APIs\Candidates\Filters\SearchCandidates $filter
	 * @return \Wish\Models\SearchResult
	 */
	protected function loadData($filter)
	{
		$filter->returnHRManager->setValue(true);
		return Services\Candidates::search($filter);
	}
}
