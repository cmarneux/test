<?php
/**
 * customers.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use BoondManager\APIs\Companies\Filters\SearchCompanies;
use BoondManager\Models\Company;
use BoondManager\Models\Account;
use Wish\CSV;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

class Companies extends AbstractExtraction{


	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$expertise = Dictionary::getMapping('specific.setting.expertiseArea');
		$states = Dictionary::getMapping('specific.setting.state.crm');


		$mapping = [
			'id' => function($value, $row){
			/** @var Company $row */
				return $row->getReference();
			},
			'name',
			'expertiseArea' => function($value) use ($expertise){
				return Tools::mapData($value, $expertise);
			},
			'informationComments',
			'state' => function($value) use ($states){
				return Tools::mapData($value, $states);
			},
			'website',
			'phone1',
			'address',
			'postcode',
			'town',
			'country',
			'mainManager' => function($value){
				/** @var Account $value */
				return ($value) ? $value->getFullName() : '';
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('customers.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('customers.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param SearchCompanies $filter
	 * @return \Wish\Models\SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Companies::search($filter);
	}
}
