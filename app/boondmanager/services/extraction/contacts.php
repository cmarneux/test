<?php
/**
 * contracts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use Wish\CSV;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

class Contacts extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$expertise = Dictionary::getMapping('specific.setting.expertiseArea');
		$states = Dictionary::getMapping('specific.setting.state.crm');

		$mapping = [
			'ID_CRMCONTACT' => function($value){
				return 'CCON'.$value;
			},
			'CCON_CIVILITE' => function($value){
				return Dictionary::lookup('specific.setting.civility', $value);
			},
			'CCON_NOM', 'CCON_PRENOM', 'CCON_FONCTION', 'CCON_SERVICE',
			'CSOC_SOCIETE' => function($v, $row){
				return $row->company->CSOC_SOCIETE;
			},
			'CSOC_INTERVENTION' => function($value, $row) use ($expertise){
				return Tools::mapData($row->company->CSOC_INTERVENTION, $expertise);
			},
			'CSOC_METIERS' => function($v, $row){
				return $row->company->CSOC_METIERS;
			},
			'CCON_TYPE' => function($value) use ($states){
				return Tools::mapData($value, $states);
			},
			'CCON_EMAIL', 'CCON_EMAIL2', 'CCON_EMAIL3', 'CCON_TEL1', 'CCON_TEL2', 'CCON_ADR', 'CCON_CP', 'CCON_VILLE', 'CCON_PAYS',
			'CSOC_TEL' => function($value, $row){
				return $row->company->CSOC_TEL;
			},
			'CSOC_ADR' => function($value, $row){
				return $row->company->CSOC_ADR;
			},
			'CSOC_CP' => function($value, $row){
				return $row->company->CSOC_CP;
			},
			'CSOC_VILLE' => function($value, $row){
				return $row->company->CSOC_VILLE;
			},
			'CSOC_PAYS' => function($value, $row){
				return $row->company->CSOC_PAYS;
			},
			'CCON_COMMENTAIRE',
			'ID_PROFIL' => function($value, $row){
				return $row->mainManager->PROFIL_NOM.' '.$row->mainManager->PROFIL_PRENOM;
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('contacts.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('contacts.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Contacts::search($filter);
	}
}
