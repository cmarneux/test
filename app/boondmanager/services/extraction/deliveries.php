<?php
/**
 * deliveries.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use BoondManager\Models\Delivery;
use Wish\CSV;
use BoondManager\Lib\Currency;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

class Deliveries extends AbstractExtraction{


	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{

		$deliveriesTypes = Dictionary::getMapping('specific.setting.typeOf.project');
		$deliveriesStates = Dictionary::getMapping('specific.setting.state.delivery');

		$devise = new Currency();

		$mapping = [
			'id' => function($value, $row){
				/** @var Delivery $row */
				return $row->getReference();
			},
			'COMP_NOM' => function($value, $row){
				return ($row['ITEM_TYPE']==1)?$row->product->PRODUIT_NOM:$row->resource->PROFIL_PRENOM;
			},
			'COMP_PRENOM' => function($value, $row){
				return ($row['ITEM_TYPE']==1)?'':$row->resource->PROFIL_PRENOM;
			},
			'MP_DEBUT' => function($value){
				return Tools::convertDateForUI($value);
			},
			'MP_FIN' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PRJ_REFERENCE' => function($value, $row){
				return $row->project->PRJ_REFERENCE;
			},
			'PRJ_TYPEREF' => function($value, $row) use ($deliveriesTypes){
				return Tools::mapData($row->project->PRJ_TYPEREF, $deliveriesTypes);
			},
			'MP_TYPE' => function($value) use ($deliveriesStates){
				return Tools::mapData($value, $deliveriesStates);
			},
			'AO_TITLE' => function($value, $row){
				return $row->project->opportunity->AO_TITLE;
			},
			'CCON_NOM' => function($value, $row){
				return ($row->project->contact)?$row->project->contact->CCON_NOM:'';
			},
			'CCON_PRENOM' => function($value, $row){
				return ($row->project->contact)?$row->project->contact->CCON_PRENOM:'';
			},
			'CSOC_SOCIETE' => function($value, $row){
				return ($row->project->company)?$row->project->company->CSOC_SOCIETE:'';
			},
			'PRJ_DEVISE' => function($value, $row) use($devise){
				return $devise->getName($row->project->PRJ_DEVISE);
			},
			'MP_TARIF' => function($value, $row) use ($devise){
				$devise->setDefault($row->project->PRJ_DEVISEAGENCE,$row->project->PRJ_CHANGEAGENCE);
				$value = $devise->getAmountForUI($value, $row->project->PRJ_CHANGE, 2, '');
				return str_replace('.', ',', $value);
			},
			'MP_CJM' => function($value, $row) use ($devise){
				$devise->setDefault($row->project->PRJ_DEVISEAGENCE,$row->project->PRJ_CHANGEAGENCE);
				$value = $devise->getAmountForUI($value, $row->project->PRJ_CHANGE, 2, '');
				return str_replace('.', ',', $value);
			},
			'TOTAL_CA' => function($value, $row) use($devise){
				$devise->setDefault($row->project->PRJ_DEVISEAGENCE,$row->project->PRJ_CHANGEAGENCE);
				$value = $devise->getAmountForUI($value, $row->project->PRJ_CHANGE, 2, '');
				return str_replace('.', ',', $value);
			},
			'TOTAL_MARGE' => function($value, $row) use($devise){
				$devise->setDefault($row->project->PRJ_DEVISEAGENCE,$row->project->PRJ_CHANGEAGENCE);
				$value = $devise->getAmountForUI($value, $row->project->PRJ_CHANGE, 2, '');
				return str_replace('.', ',', $value);
			},
			'TOTAL_RENTA' => function($value, $row) use($devise){
				$devise->setDefault($row->project->PRJ_DEVISEAGENCE,$row->project->PRJ_CHANGEAGENCE);
				$value = $devise->getAmountForUI(100*$value, $row->project->PRJ_CHANGE, 2, '');
				return str_replace('.', ',', $value).'%';
			},
			'PRJ_CP' => function($value, $row){
				return $row->project->PRJ_CP;
			},
			'PRJ_VILLE' => function($value, $row){
				return $row->project->PRJ_VILLE;
			},
			'ID_PROFIL' => function($value, $row){
				return $row->project->mainManager->PROFIL_NOM.' '.$row->project->mainManager->PROFIL_PRENOM;
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('deliveries.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('deliveries.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\DeliveriesInactivitiesGroupments::search($filter);
	}
}
