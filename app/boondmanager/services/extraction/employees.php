<?php
/**
 * resources.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use BoondManager\APIs\Employees\Filters\SearchEmployees;
use BoondManager\Models\Employee;
use BoondManager\Models\Account;
use Wish\CSV;
use BoondManager\Lib\Currency;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

class Employees extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{

		$types = Dictionary::getMapping('specific.setting.typeOf.resource');
		$states = Dictionary::getMapping('specific.setting.state.resource');

		$dictDomain = Dictionary::getDict('specific.setting.activityArea');

		$domains = [];
		foreach($dictDomain as $cat=>$values)
			$domains = array_merge($domains, $values['option']);

		$domains = Tools::buildMaping($domains, 'id', 'value');

		$formations = Dictionary::getMapping('specific.setting.training');
		$experiences = Dictionary::getMapping('specific.setting.experience');

		$mobilityCategories = Dictionary::getDict('specific.setting.mobilityArea');

		$mobility = [];
		foreach($mobilityCategories as $cat=>$values)
			$mobility = array_merge($mobility, $values['option']);

		$mobility = Tools::buildMaping($mobility, 'id', 'value');

		$situation = Dictionary::getMapping('specific.setting.situation');

		$devise = new Currency();

		$mapping = [
			'id' => function($value, $row){
				/** @var Employee $row */
				return $row->getReference();
			},
			'reference',
			'civility'=> function($value){
				return Dictionary::lookup('specific.setting.civility', $value);
			},
			'lastName',
			'firstName',
			'typeOf' => function($value) use ($types){
				return Tools::mapData($value, $types);
			},
			'state' => function($value) use ($states){
				return Tools::mapData($value, $states);
			},
			'title',
			'skills',
			'activityAreas' => function($value) use ($domains){
				if(!$value) $value = [];
				if(!is_array($value)) throw new \Exception('COMP_APPLICATIONS must be an array and not a '.gettype($value));

				$value = array_map(function($value) use ($domains){
					return Tools::mapData($value, $domains);
				}, $value);
				return implode(' ',$value);
			},
			'training' => function($value) use ($formations){
				return Tools::mapData($value, $formations);
			},
			'experience' => function($value) use ($experiences){
				return Tools::mapData($value, $experiences);
			},
			'availability' => function($value){
				if($value==1){
					return Dictionary::lookup('specific.setting.availability', 0); //immédiate
				}else return $value;
			},
			'currency' => function($value) use($devise){
				return $devise->getName($value);
			},
			'averageDailyPriceExcludingTax' => function($value, $row) use($devise){
				/** @var Employee $row */
				$devise->setDefault($row->currencyAgency,$row->exchangeRateAgency);
				return $devise->getAmountForUI($value, $row->exchangeRate);
			},
			'mobilityAreas' => function($value) use($mobility){
				if(!$value) $value = [];
				if(!is_array($value)) throw new \Exception('PARAM_MOBILITE must be an array and not a '.gettype($value));
				return implode(', ', array_map(function($value) use ($mobility){
					return Tools::mapData($value, $mobility);
				}, $value));
			},
			'email1','email2','email3','phone1','phone2','phone3',
			'dateOfBirth',
			'nationality',
			'situation' => function($value) use ($situation){
				return Tools::mapData($value, $situation);
			} ,
			'numberOfActivePositionings', 'address', 'postcode', 'town', 'country',
			'mainManager' => function($value){
				/** @var Account $value */
				return $value ? $value->getFullName() : '';
			},
			'ID_RESPRH' => function($value){
				/** @var Account $value */
				return $value ? $value->getFullName() : '';
			}
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('resources.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('resources.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param SearchEmployees $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Employees::search($filter);
	}
}
