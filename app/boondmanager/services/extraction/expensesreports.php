<?php
/**
 * expensesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use Wish\CSV\CSV;
use BoondManager\Lib\Currency;
use Wish\MySQL\SearchResult;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\ExpensesReport;
use BoondManager\Services;

class ExpensesReports extends AbstractExtraction{

	private $fullExtract;

	public function __construct($name, $encoding, $full){
		$this->fullExtract = $full;
		parent::__construct($name, $encoding);
	}

	private function isSimpleExtract(){
		return !$this->fullExtract;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		$result = Services\ExpensesReports::search($filter,  $includeDetails = true);

		if ($this->isSimpleExtract()) {
			$result->rows = $this->prepareDataForSimpleExtract($result->rows);
		} else {
			$result->rows = $this->prepareDataForDetailedExtract($result->rows);
		}

		return $result;
	}

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		if($this->isSimpleExtract()){
			$mapping = $this->getColumnsForSimpleExtract();
		}else{
			$mapping = $this->getColumnsForDetailedExtract();
		}

		$columns = [];
		foreach($mapping as $key=>$value) {

			if (is_int($key)){
				$column = new CSV\Column($value, Dictionary::getDict('expensesreports.sqlLabels.' . $value));
			}else{
				$column = new CSV\Column($key, Dictionary::getDict('expensesreports.sqlLabels.' . $key), $value);
			}


			$columns[] = $column;
		}

		return $columns;
	}

	protected function getColumnsForSimpleExtract(){

		$profilesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');

		$devise = new Currency();

		$mapping = [
			'PROFIL_NOM',
			'PROFIL_PRENOM',
			'PROFIL_REFERENCE',
			'PROFIL_TYPE' => function($value) use($profilesTypes){
				return Tools::mapData($value, $profilesTypes);
			},
			'LISTEFRAIS_DATE' => function($value){
				$date = explode('-',$value);
				return implode('/', ['01', $date[1], $date[0]]);
			},
			'LISTEFRAIS_DEVISEAGENCE' => function($value) use ($devise){
				return $devise->getName($value);
			},
			'LISTEFRAIS_BKMVALUE' => function($value){
				return str_replace('.', ',', $value);
			},
			'TOTAL_KM' => function($value){
				return str_replace('.', ',', $value);
			},
			'TOTAL_KM_EXPENSES' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, ' ', ',');
			},
			'TOTAL_REAL' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, ' ', ',');
			},
			'TOTAL_PACKAGED' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, ' ', ',');
			},
			'TOTAL_TVA' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, ' ', ',');
			},
			'TOTAL_EXPENSES' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value + $row['TOTAL_KM_EXPENSES'], 1, 2, ' ', ',');
			},
			'TOTAL_REBILLED' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value + $row['KM_REBILLED'], 1, 2, ' ', ',');
			},
			'LISTEFRAIS_AVANCE' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, '', ',');
			},
			'TOPAYE' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['TOTAL_EXPENSES'] - $row['LISTEFRAIS_AVANCE'], 1, 2, ' ', ',');
			},
			'LISTEFRAIS_REGLE'
		];

		return $mapping;
	}

	protected function getColumnsForDetailedExtract(){

		$hours = Dictionary::getMapping('specific.setting.typeOf.activity');

		$profilesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');

		$defaultLabelKM = Dictionary::getDict('expensesreports.extraction.kmExpenses');

		$devise = new Currency();

		$basedMapping = [
			'PROFIL_NOM',
			'PROFIL_PRENOM',
			'PROFIL_REFERENCE',
			'PROFIL_TYPE' => function($value) use ($profilesTypes){
				return Tools::mapData($value, $profilesTypes);
			},
			'DATE' => function($value){
				return Tools::convertDateForUI($value);
			},
			'ID_PROJET' => function($value, $row) use ($hours){
				if($value <= 0){
					if(array_key_exists(abs($value), $hours)) return $hours[abs($value)];
				}
				if(isset($row['PRJ_REFERENCE']) && $row['PRJ_REFERENCE']){
					$project =  $row['PRJ_REFERENCE'];
				}else{
					$project = 'PRJ'.$value;
				}

				if(isset($row['LOT_TITRE'], $row['ID_LOT'])){
					if($row['LOT_TITRE']) $project = $row['LOT_TITRE'] .' - '. $project;
					else $project = $row['ID_LOT'] .' - '. $project;
				}

				return $project;
			},
			'TYPEFRSREF',
			'TYPEFRS_NAME' => function($value, $row) use ($defaultLabelKM){
				if($row['TYPEFRSREF'] == 0)
					$namehrs = $defaultLabelKM;
				else {
					if (isset($row['TYPEFRS_NAME']))
						$namehrs = $row['TYPEFRS_NAME'] . ' - ' . $row['TVA'] . '%';
					else
						$namehrs = $row['TYPEFRSREF'] . ' - ' . $row['TVA'] . '%';
				}
				return $namehrs;
			},
			'FRAISREEL_INTITULE',
			'FRAISREEL_REFACTURE',
			'LISTEFRAIS_DEVISEAGENCE' => function($value) use ($devise){
				return $devise->getName($value);
			},
			'TOTAL_TTC' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, '', ',');
			},
			'TOTAL_TVA' =>  function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				if(isset($row['FRAISREEL_CHANGE'])) {
					return $devise->getAmountForUI($value, $row['FRAISREEL_CHANGE'], 2, '', ',');
				}else{
					return $devise->getAmountForUI($value, 1, 2, '', ',');
				}
			},
			'TOTAL_HT' => function($value, $row) use ($devise){
				$devise->setDefault($row['LISTEFRAIS_DEVISEAGENCE'], $row['LISTEFRAIS_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, 1, 2, '', ',');
			},
			'LISTEFRAIS_BKMVALUE' => function($value){
				return str_replace('.', ',', $value);
			},
			'TOTAL_KM' => function($value){
				return str_replace('.', ',', $value);
			},
		];

		return $basedMapping;
	}

	private function prepareDataForSimpleExtract($data){

		$flattenResult = [];

		foreach($data as $report) {
			/** @var ExpensesReport $report */

			$row = [
				'PROFIL_NOM'       => $report->resource->PROFIL_NOM,
				'PROFIL_PRENOM'    => $report->resource->PROFIL_PRENOM,
				'PROFIL_REFERENCE' => $report->resource->PROFIL_REFERENCE,
				'PROFIL_TYPE'      => $report->resource->typeOf,
				'LISTEFRAIS_DATE'  => $report->term,
				'LISTEFRAIS_DEVISEAGENCE' => $report->currencyAgency,
				'LISTEFRAIS_CHANGEAGENCE' => $report->exchangeRateAgency,
				'LISTEFRAIS_BKMVALUE'     => $report->ratePerKilometer,
				'KM_SCALE'                => 0, //bareme KM
				'TOTAL_KM'                => 0, //7
				'TOTAL_KM_EXPENSES'      => 0, // 8
				'TOTAL_REAL'              => 0, // 9
				'TOTAL_PACKAGED'          => 0, // 10
				'TOTAL_TVA'               => 0, // 11
				'TOTAL_EXPENSES'          => 0, // 12
				'TOTAL_REBILLED'          => 0, // 13
				'LISTEFRAIS_AVANCE'       => $report->advance,
				'TOPAYE'                  => 0, // 15
				'LISTEFRAIS_REGLE'        => $report->paid,
				'KM_REBILLED'             => 0
			];

			if(is_array($report->actualExpenses)) {
				foreach ($report->actualExpenses as $expense) {

					$row = array_merge($row, array_filter($expense, function ($k) {
						return in_array($k, [
							'ID_PROJET', 'ID_LOT', 'PRJ_REFERENCE', 'LOT_TITRE', 'FRAISREEL_TYPEFRSREF', 'FRAISREEL_REFACTURE',
							'FRAISREEL_CHANGE', 'FRAISREEL_MONTANT', 'FRAISREEL_TVA', 'TYPEFRS_NAME'
						]);
					}, ARRAY_FILTER_USE_KEY));

					if($expense['FRAISREEL_TYPEFRSREF'] == 0){
						$totalTTC = round( $expense['FRAISREEL_MONTANT']*$report->ratePerKilometer , 4);
						$row['TOTAL_KM'] += $expense['FRAISREEL_MONTANT'];
						$row['TOTAL_KM_EXPENSES'] += $totalTTC;
						if($expense['FRAISREEL_REFACTURE'] == 1)
							$row['KM_REBILLED'] += $totalTTC;
					}else{
						$totalTTC = $expense['FRAISREEL_MONTANT'];
						$tva = $expense['FRAISREEL_MONTANT']*(1-1/(1+$expense['FRAISREEL_TVA']/100));
						$row['TOTAL_REAL'] += $totalTTC ;
						$row['TOTAL_TVA'] += $tva;
						$row['TOTAL_EXPENSES'] += $totalTTC;
						if($expense['FRAISREEL_REFACTURE'] == 1)
							$row['TOTAL_REBILLED'] += $totalTTC;
					}
				}
			}

			if(is_array($report->fixedExpenses)) {
				foreach ($report->fixedExpenses as $expense) {

					if( isset($expense['LISTEJOURS']) ) {
						foreach ($expense['LISTEJOURS'] as $day) {

							if($expense['LIGNEFRAIS_TYPEFRSREF'] == 0) {
								$totalTTC = round($day['FRAIS_VALUE']*$report->ratePerKilometer,4);
								$totalKM = $day['FRAIS_VALUE'];

								$row['TOTAL_KM'] += $totalKM;
								$row['TOTAL_KM_EXPENSES'] += $totalTTC;
							} else {
								$totalTTC = $day['FRAIS_VALUE'];
								$tva = $day['FRAIS_VALUE']*(1-1/(1+$expense['LIGNEFRAIS_TVA']/100));

								$row['TOTAL_PACKAGED'] += $totalTTC;
								$row['TOTAL_TVA'] += $tva;
								$row['TOTAL_EXPENSES'] += $totalTTC;
							}

						}
					}
				}
			}

			$flattenResult[] = $row;
		}

		return $flattenResult;
	}

	private function prepareDataForDetailedExtract($data){

		$flattenResult = [];

		foreach($data as $report) {
			/** @var ExpensesReport $report */

			$rowReport = [
				'PROFIL_PRENOM' => $report->resource->PROFIL_PRENOM,
				'PROFIL_NOM' => $report->resource->PROFIL_NOM,
				'PROFIL_TYPE' => $report->resource->typeOf,
				'PROFIL_REFERENCE' => $report->resource->PROFIL_REFERENCE,
				'LISTEFRAIS_DEVISEAGENCE' => $report->currencyAgency,
				'LISTEFRAIS_CHANGEAGENCE' => $report->exchangeRateAgency,
			];

			if(is_array($report->actualExpenses)) {
				foreach ($report->actualExpenses as $expense) {

					$row = array_filter($expense, function ($k) {
						return in_array($k, [
							'ID_PROJET', 'ID_LOT', 'PRJ_REFERENCE', 'LOT_TITRE',
							'FRAISREEL_INTITULE', 'FRAISREEL_REFACTURE', 'TYPEFRS_NAME', 'FRAISREEL_CHANGE'
						]);
					}, ARRAY_FILTER_USE_KEY);

					$row = array_merge($row, $rowReport, [
						'DATE' => $expense->FRAISREEL_DATE,
						'TYPEFRSREF' => $expense->FRAISREEL_TYPEFRSREF,
						'TVA' => $expense->FRAISREEL_TVA,
					]);

					if ($expense['FRAISREEL_TYPEFRSREF'] == 0) {

						$row = array_merge($row, [
							'TOTAL_TTC' => $expense['FRAISREEL_MONTANT']*$report->ratePerKilometer,
							'LISTEFRAIS_BKMVALUE' => $report->ratePerKilometer,
							'TOTAL_KM' => $expense['FRAISREEL_MONTANT']
						]);

					} else {
						$tva = $expense['FRAISREEL_MONTANT']*(1-1/(1+$expense['FRAISREEL_TVA']/100));

						$row = array_merge($row, [
							'TOTAL_TTC' => $expense['FRAISREEL_MONTANT'],
							'TOTAL_TVA' => $tva,
							'TOTAL_HT'  => $expense['FRAISREEL_MONTANT'] - $tva
						]);
					}

					$flattenResult[] = $row;
				}
			}

			if(is_array($report->fixedExpenses)) {
				foreach ($report->fixedExpenses as $expense) {

					if( !isset($expense['LISTEJOURS']) ) continue;

					$rowExpense = array_filter($expense, function ($k) {
						return in_array($k, [
							'ID_PROJET', 'ID_LOT', 'PRJ_REFERENCE', 'LOT_TITRE',
							'LIGNEFRAIS_TYPEFRSREF', 'TYPEFRS_NAME',
						]);
					}, ARRAY_FILTER_USE_KEY);

					foreach ($expense['LISTEJOURS'] as $day) {

						$rowDay = array_merge($rowExpense, $rowReport, [
							'DATE' => $day['FRAIS_DATE'],
							'TYPEFRSREF' => $expense->LIGNEFRAIS_TYPEFRSREF,
							'TVA' => $expense->LIGNEFRAIS_TVA,
						]);

						if($expense['LIGNEFRAIS_TYPEFRSREF'] == 0) {

							$rowDay['TOTAL_TTC'] = $day['FRAIS_VALUE']*$report->ratePerKilometer;
							$rowDay['LISTEFRAIS_BKMVALUE'] = $report->ratePerKilometer;
							$rowDay['TOTAL_KM'] = $day['FRAIS_VALUE'];

						} else {

							$rowDay['TOTAL_TTC'] = $day['FRAIS_VALUE'];
							$rowDay['TOTAL_TVA'] = $day['FRAIS_VALUE']*(1-1/(1+$expense['LIGNEFRAIS_TVA']/100));
							$rowDay['TOTAL_HT'] = $rowDay['TOTAL_TTC'] - $rowDay['TOTAL_TVA'];
						}

						$flattenResult[] = $rowDay;
					}
				}
			}
		}

		return $flattenResult;
	}
}
