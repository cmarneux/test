<?php
/**
 * opportunities.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Extraction;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Models\Account;
use BoondManager\Models\Opportunity;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications\RequestAccess\Opportunities\MyOpportunity;
use BoondManager\Lib\Currency;
use Wish\CSV;
use Wish\Models\SearchResult;
use Wish\Tools;

class Opportunities extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$types = Dictionary::getMapping('specific.setting.typeOf.project');
		$opportunityStates = Dictionary::getMapping('specific.setting.state.opportunity');
		$intervention = Dictionary::getMapping('specific.setting.expertiseArea');
		$duration = Dictionary::getMapping('specific.setting.duration');
		$origins = Dictionary::getMapping('specific.setting.origin');

		// construction de la liste d'application valide
		$dictDomain = Dictionary::getDict('specific.setting.activityArea');

		$domains = [];
		foreach($dictDomain as $cat=>$values)
			$domains = array_merge($domains, $values['option']);

		$domains = Tools::buildMaping($domains, 'id', 'value');

		// construction liste mobilité
		$mobilityCategories = Dictionary::getDict('specific.setting.mobilityArea');

		$mobilities = [];
		foreach($mobilityCategories as $cat=>$values)
			$mobilities = array_merge($mobilities, $values['option']);

		$mobilities = Tools::buildMaping($mobilities, 'id', 'value');

		$devise = new Currency();

		$isMyOpportunity = new MyOpportunity();
		$request = new RequestAccess();
		$request->setUser( CurrentUser::instance() );

		$mapping = [
			'id' => function($value, Opportunity $row){
				return $row->getReference();
			},
			'reference',
			'creationDate' => function($value){
				return Tools::convertDateForUI($value);
			},
			'title',
			'typeOf' => function($value) use ($types){
				return Tools::mapData($value, $types);
			},
			'CCON_NOM' => function($value, Opportunity $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$row->contact->lastName:'';
			},
			'CCON_PRENOM' => function($value, Opportunity $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$row->contact->firstName:'';
			},
			'CSOC_SOCIETE' => function($value, Opportunity $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$row->company->name:'';
			},
			'mode' => function($value) use ($opportunityStates){
				return Tools::mapData($value, $opportunityStates);
			},
			'numberOfActivePositionings',
			'activityAreas' => function($values) use ($domains){
				return implode(',',Tools::mapData($values, $domains));
			},
			'place' => function($values) use ($mobilities){
				return implode(',',Tools::mapData($values, $mobilities));
			},
			'expertiseArea' => function($value) use ($intervention){
				return Tools::mapData($value, $intervention);
			},
			'startDate' => function($value){
				return Tools::convertDateForUI($value);
			},
			'duration' => function($value) use ($duration){
				return Tools::mapData($value, $duration);
			},
			'AO_TYPESOURCE' => function($value, Opportunity $row) use ($origins){
				$origin = Tools::mapData($value, $origins);
				if($row->AO_SOURCE) $origin .= ' - '.$row->AO_ORIGIN;
				return $origin;
			},
			'currency' => function($value) use($devise){
				return $devise->getName($value);
			},
			'previsionnel' => function($value, Opportunity $row) use ($devise){
				$devise->setDefault($row->currencyAgency, $row->exchangeRateAgency);
				$value = $devise->getAmountForUI($row->turnoverEstimatedExcludingTax*$row->weighting/100, $row->exchangeRate, 0, '');
				return str_replace('.', ',', $value);
			},
			'turnoverEstimatedExcludingTax' => function($value, Opportunity $row) use ($devise){
				$devise->setDefault($row->currencyAgency, $row->exchangeRateAgency);
				$value = $devise->getAmountForUI($value, $row->exchangeRate, 0, '');
				return str_replace('.', ',', $value);
			},
			'weighting' => function($value){
				return round($value/100,2);
			},
			'mainManager' => function(Account $value){
				return $value->getFullName();
			}
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('opportunities.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('opportunities.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return \BoondManager\Services\Opportunities::search($filter);
	}
}
