<?php
/**
 * payments.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use Wish\CSV\CSV;
use BoondManager\Lib\Currency;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\OldModels\Filters\AbstractSearch;
use BoondManager\Models\Payment;
use BoondManager\Services;

class Payments extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$purchasesTypes  = Dictionary::getMapping('specific.setting.typeOf.subscription');
		$purchasesCat    = Dictionary::getMapping('specific.setting.typeOf.purchase');
		$paymentsStates  = Dictionary::getMapping('specific.setting.state.payment');

		$devise = new Currency();

		$mapping = [
			'ID_PAIEMENT' => function($value, Payment $row){
				return $row->getRef();
			},
			'PMT_DATE' => function($value){
				return Tools::convertDateForUI($value);
			},
			'ACHAT_DATE' => function($value, Payment $row){
				return Tools::convertDateForUI($row->purchase->date);
			},
			'ACHAT_REF' => function($value, Payment $row){
				return $row->purchase->reference;
			},
			'ACHAT_TITLE' => function($value, Payment $row){
				return $row->purchase->title;
			},
			'CCON_NOM' => function($value, Payment $row){
				return $row->purchase->contact->lastName;
			},
			'CCON_PRENOM' => function($value, Payment $row){
				return $row->purchase->contact->firstName;
			},
			'CSOC_SOCIETE' => function($value, Payment $row){
				return $row->purchase->company->CSOC_SOCIETE;
			},
			'ACHAT_REFOURNISSEUR' => function($value, Payment $row){
				return $row->purchase->number;
			},
			'PRJ_REFERENCE' => function($value, Payment $row){
				if($row->purchase->project){
					return $row->purchase->project->getReference();
				}else return '';
			},
			'ACHAT_TYPE' => function($value, Payment $row) use ($purchasesTypes){
				return Tools::mapData($row->purchase->subscription, $purchasesTypes);
			},
			'ACHAT_CATEGORIE' => function($value, Payment $row) use ($purchasesCat){
				return Tools::mapData($row->purchase->typeOf, $purchasesCat);
			},
			'ACHAT_DEBUT' => function($value, Payment $row){
				return Tools::convertDateForUI($row->purchase->startDate);
			},
			'ACHAT_FIN' => function($value, Payment $row){
				return Tools::convertDateForUI($row->purchase->endDate);
			},
			'PMT_REFPROVIDER',
			'PMT_ETAT' => function($value) use ($paymentsStates){
				return Tools::mapData($value, $paymentsStates);
			},
			'PMT_DATEPAIEMENTATTENDU' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PMT_DATEPAIEMENTEFFECTUE' => function($value){
				return Tools::convertDateForUI($value);
			},
			'ACHAT_DEVISE' => function($value, Payment $row) use($devise){
				return $devise->getName($row->purchase->currency);
			},
			'PMT_MONTANTHT' => function($value, Payment $row) use($devise){
				$devise->setDefault($row->purchase->currencyAgency,$row->purchase->exchangeRateAgency);
				return $devise->getAmountForUI($value, $row->purchase->exchangeRate, 2, '', ',');
			},
			'payeTTC' => function($value, Payment $row) use($devise){
				$devise->setDefault($row->purchase->currencyAgency,$row->purchase->exchangeRateAgency);
				return $devise->getAmountForUI($row->amountExcludingTax*(1+$row->taxRate/100), $row->purchase->exchangeRate, 2, '', ',');
			},
			'ID_PROFIL' => function($value, Payment $row){
				return $row->purchase->mainManager->PROFIL_NOM.' '.$row->purchase->mainManager->PROFIL_PRENOM;
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('payments.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('payments.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Payments::search($filter);
	}
}
