<?php
/**
 * positionings.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Extraction;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Models\Account;
use Wish\CSV;
use BoondManager\Lib\Currency;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Services;
use BoondManager\Models\Positioning;
use BoondManager\Lib\RequestAccess;
use BoondManager\APIs\Positionings\Specifications\MyOpportunity;

class Positionings extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$types = Dictionary::getMapping('specific.setting.typeOf.project');
		$resourcesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');
		$opportunityStates = Dictionary::getMapping('specific.setting.state.opportunity');

		$devise = new Currency();

		$isMyOpportunity = new MyOpportunity();
		$request = new RequestAccess();
		$request->setUser( CurrentUser::instance() );

		$mapping = [
			'id' => function($value, Positioning $row){
				$row->getReference();
			},
			'updateDate' => function($value){
				return Tools::convertDateForUI($value);
			},
			'profilLastName' => function($value, Positioning $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				if($row->dependsOnProfil() && $isMyOpportunity->isSatisfiedBy($request)){
					return $row->dependsOn->lastName;
				}
				return '';
			},
			'profilFirstName' => function($value, Positioning $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				if($row->dependsOnProfil() && $isMyOpportunity->isSatisfiedBy($request)){
					return $row->dependsOn->firstName;
				}
				return '';
			},
			'dependsType' => function($value, Positioning $row) use ($resourcesTypes){
				return Tools::mapData($row->depnedsOn->typeOf, $resourcesTypes);
			},
			'title' => function($value, Positioning $row){
				return $row->opportunity->title;
			},
			'reference' => function($value, Positioning $row){
				return $row->opportunity->reference;
			},
			'typeOf' => function($value, Positioning $row) use ($types){
				return Tools::mapData($row->opportunity->typeOf, $types);
			},
			'state' => function($value) use ($opportunityStates){
				return Tools::mapData($value, $opportunityStates);
			},
			'currency' => function($value, Positioning $row) use($devise){
				return $devise->getName($row->opportunity->currency);
			},
			'ca' => function($value, Positioning $row) use ($devise){
				$devise->setDefault($row->opportunity->currencyAgency, $row->opportunity->exchangeRateAgency);
				$value = $devise->getAmountForUI($row->averageDailyPriceExcludingTax*$row->numberOfDaysInvoicedOrQuantity, $row->opportunity->exchangeRate, 0, '');
				return str_replace('.', ',', $value);
			},
			'marge' => function($value, Positioning $row) use ($devise){
				$devise->setDefault($row->opportunity->currencyAgency, $row->opportunity->exchangeRateAgency);
				$ca = $row->averageDailyPriceExcludingTax*$row->numberOfDaysInvoicedOrQuantity;
				$value = $devise->getAmountForUI($ca-($row->averageDailyCost*($row->numberOfDaysInvoicedOrQuantity+$row->numberOfDaysFree)), $row->opportunity->exchangeRate, 2, '');
				return str_replace('.', ',', $value);
			},
			'renta' => function($value, Positioning $row) use ($devise){
				$devise->setDefault($row->opportunity->currencyAgency, $row->opportunity->exchangeRateAgency);
				$ca = $row->averageDailyPriceExcludingTax*$row->numberOfDaysInvoicedOrQuantity;
				if($ca != 0) {
					$marge = $ca - ($row->averageDailyCost * ($row->numberOfDaysInvoicedOrQuantity + $row->numberOfDaysFree));
					$value = $devise->getAmountForUI($marge*100/$ca, $row->opportunity->exchangeRate, 2, '').'%';
				}else $value = 0;
				return str_replace('.', ',', $value);
			},
			'informationComments' => function($value, Positioning $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$value:'';
			},
			'contactLastName' => function($value, Positioning $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$row->opportunity->contact->lastName:'';
			},
			'contactFirstName' => function($value, Positioning $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$row->opportunity->contact->firstName:'';
			},
			'company' => function($value, Positioning $row) use ($request, $isMyOpportunity){
				$request->setData($row);
				return ($isMyOpportunity->isSatisfiedBy($request))?$row->opportunity->company->name:'';
			},
			'mainManager' => function(Account $value){
				return $value->getFullName();
			},
			'profilHrManager' => function($value, Positioning $row){
				if($row->dependsOnProfil()) return $row->dependsOn->hrManager->getFullName();
			},
			'profilMainManager' => function($value, Positioning $row){
				if($row->dependsOnProfil()) return $row->dependsOn->mainManager->getFullName();
			}
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('positioning.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('positioning.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Positionings::search($filter);
	}
}
