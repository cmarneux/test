<?php
/**
 * products.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use Wish\CSV;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\APIs\Products\Filters\SearchProducts;
use BoondManager\Models\Account;
use BoondManager\Lib\Currency;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Product;
use BoondManager\Services;

class Products extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$types  = Dictionary::getMapping('specific.setting.typeOf.subscription');
		$states = Dictionary::getMapping('specific.setting.state.product');

		$devise = new Currency();

		$mapping = [
			'id' => function($value, $row){
				/** @var Product $row */
				return $row->getReference();
			},
			'name',
			'reference',
			'subscription' => function($value) use($types){
				return Tools::mapData($value, $types);
			},
			'state' => function($value) use($states){
				return Tools::mapData($value, $states);
			},
			'currency' => function($value) use($devise){
				return $devise->getName($value);
			},
			'priceExcludingTax' => function($value, $row) use($devise){
				/** @var Product $row */
				$devise->setDefault($row->currencyAgency,$row->exchangeRateAgency);
				return $devise->getAmountForUI($value, $row->exchangeRate);
			},
			'mainManager' => function($value){
				/** @var Account $value */
				return $value->getFullName();
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('products.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('products.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param SearchProducts $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Products::search($filter);
	}
}
