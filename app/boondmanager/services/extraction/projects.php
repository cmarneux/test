<?php
/**
 * projects.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use Wish\CSV\CSV;
use BoondManager\Lib\Currency;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services;

class Projects extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{

		$deliveriesTypes = Dictionary::getMapping('specific.setting.typeOf.project');
		$deliveriesStates = Dictionary::getMapping('specific.setting.state.delivery');

		$devise = new Currency();

		$mapping = [
			'ID_PROJET' => function($value){
				return 'PRJ'.$value;
			},
			'PRJ_DEBUT' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PRJ_FIN' => function($value){
				return Tools::convertDateForUI($value);
			},
			'PRJ_REFERENCE',
			'PRJ_TYPEREF' => function($value) use ($deliveriesTypes){
				return Tools::mapData($value, $deliveriesTypes);
			},
			'PRJ_ETAT' => function($value) use ($deliveriesStates){
				return Tools::mapData($value, $deliveriesStates);
			},
			'AO_TITLE' => function($value, $row){
				return $row->opportunity->AO_TITLE;
			},
			'CCON_NOM' => function($value, $row){
				return ($row->contact)?$row->contact->CCON_NOM:'';
			},
			'CCON_PRENOM' => function($value, $row){
				return ($row->contact)?$row->contact->CCON_PRENOM:'';
			},
			'CSOC_SOCIETE' => function($value, $row){
				return ($row->company)?$row->company->CSOC_SOCIETE:'';
			},
			'PRJ_DEVISE' => function($value) use($devise){
				return $devise->getName($value);
			},
			'TOTAL_CA' => function($value, $row) use($devise){
				$devise->setDefault($row['PRJ_DEVISEAGENCE'],$row['PRJ_CHANGEAGENCE']);
				$value = $devise->getAmountForUI($value, $row['PRJ_CHANGE'], 2, '');
				return str_replace('.', ',', $value);
			},
			'TOTAL_MARGE' => function($value, $row) use($devise){
				$devise->setDefault($row['PRJ_DEVISEAGENCE'],$row['PRJ_CHANGEAGENCE']);
				$value = $devise->getAmountForUI($value, $row['PRJ_CHANGE'], 2, '');
				return str_replace('.', ',', $value);
			},
			'TOTAL_RENTA' => function($value, $row) use($devise){
				$devise->setDefault($row['PRJ_DEVISEAGENCE'],$row['PRJ_CHANGEAGENCE']);
				$value = $devise->getAmountForUI(100*$value, $row['PRJ_CHANGE'], 2, '');
				return str_replace('.', ',', $value).'%';
			},
			'PRJ_CP', 'PRJ_VILLE',
			'ID_PROFIL' => function($value, $row){
				return $row->mainManager->PROFIL_NOM.' '.$row->mainManager->PROFIL_PRENOM;
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('projects.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('projects.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Projects::search($filter);
	}
}
