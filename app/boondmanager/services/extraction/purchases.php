<?php
/**
 * purchases.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use BoondManager\Lib\Filters\AbstractSearch;
use BoondManager\Models\Company;
use BoondManager\Models\Account;
use Wish\CSV;
use BoondManager\Lib\Currency;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Models\Purchase;
use BoondManager\Services;

class Purchases extends AbstractExtraction{

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		$purchasesTypes  = Dictionary::getMapping('specific.setting.typeOf.subscription');
		$purchasesCat    = Dictionary::getMapping('specific.setting.typeOf.purchase');
		$purchasesStates = Dictionary::getMapping('specific.setting.state.purchase');

		$devise = new Currency();

		$mapping = [
			'id' => function($value, Purchase $row){
				return $row->getReference();
			},
			'date' => function($value){
				return Tools::convertDateForUI($value);
			},
			'reference',
			'title',
			'CCON_NOM' => function($value, Purchase $row){
				return $row->contact->lastName;
			},
			'CCON_PRENOM' => function($value, Purchase $row){
				return $row->contact->firstName;
			},
			'company' => function(Company $value){
				return $value->name;
			},
			'number',
			'PRJ_REFERENCE' => function($value, Purchase $row){
				if($row->project){
					return $row->project->getReference();
				}else return '';
			},
			'subscription' => function($value) use ($purchasesTypes){
				return Tools::mapData($value, $purchasesTypes);
			},
			'typeOf' => function($value) use ($purchasesCat){
				return Tools::mapData($value, $purchasesCat);
			},
			'startDate' => function($value){
				return Tools::convertDateForUI($value);
			},
			'endDate' => function($value){
				return Tools::convertDateForUI($value);
			},
			'state' => function($value) use ($purchasesStates){
				return Tools::mapData($value, $purchasesStates);
			},
			'currency' => function($value) use($devise){
				return $devise->getName($value);
			},
			'quantity',
			'amountExcludingTax' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'totalHT' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['ACHAT_MONTANTHT']*$row['ACHAT_QUANTITE'], $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'totalTTC' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['ACHAT_MONTANTHT']*$row['ACHAT_QUANTITE']*(1+$row['ACHAT_TAUXTVA']/100), $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'engagedPaymentsAmountExcludingTax' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'payeTTC' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['TOTAL_CONSOMMES']*(1+$row['ACHAT_TAUXTVA']/100), $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'deltaHT' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['TOTAL_CONSOMMES'] - $row['TOTAL_MONTANT'], $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'deltaTTC' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI(($row['TOTAL_CONSOMMES'] - $row['TOTAL_MONTANT'])*(1+$row['ACHAT_TAUXTVA']/100), $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'paymentsAmountExcludingTax' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'paymentsTTC' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['TOTAL_PAIEMENTS']*(1+$row['ACHAT_TAUXTVA']/100), $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'reinvoiceAmountExcludingTax' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($value, $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'refactureTTC' => function($value, Purchase $row) use($devise){
				$devise->setDefault($row['ACHAT_DEVISEAGENCE'],$row['ACHAT_CHANGEAGENCE']);
				return $devise->getAmountForUI($row['ACHAT_TARIFFACTURE']*(1+$row['ACHAT_TAUXTVA']/100), $row['ACHAT_CHANGE'], 2, '', ',');
			},
			'mainManager' => function(Account $value){
				return $value->getFullName();
			},
		];

		$columns = [];
		foreach($mapping as $key=>$value) {
			if (is_int($key)) $column = new CSV\Column($value, Dictionary::getDict('purchases.sqlLabels.' . $value));
			else $column = new CSV\Column($key, Dictionary::getDict('purchases.sqlLabels.' . $key), $value);
			$columns[] = $column;
		}

		return $columns;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		return Services\Purchases::search($filter);
	}
}
