<?php
/**
 * timesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Extraction;

use BoondManager\Lib\Filters\AbstractSearch;
use Wish\CSV;
use Wish\Models\SearchResult;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Models\TimesReport;
use BoondManager\Services;

class TimesReports extends AbstractExtraction{

	private $fullExtract;

	public function __construct($name, $encoding, $full){
		$this->fullExtract = $full;
		parent::__construct($name, $encoding);
	}

	private function isSimpleExtract(){
		return !$this->fullExtract;
	}

	/**
	 * @param AbstractSearch $filter
	 * @return SearchResult
	 */
	protected function loadData($filter)
	{
		$result = Services\TimesReports::search($filter,  $includeTimes = true);

		if ($this->isSimpleExtract()) {
			$result->rows = $this->prepareDataForSimpleExtract($result->rows);
		} else {
			$result->rows = $this->prepareDataForDetailedExtract($result->rows);
		}

		return $result;
	}

	/**
	 * @return CSV\Column[]
	 */
	protected function columnDefinition()
	{
		if($this->isSimpleExtract()){
			$mapping = $this->getColumnsForSimpleExtract();
		}else{
			$mapping = $this->getColumnsForDetailedExtract();
		}

		$hoursLabels = [];
		$hours = Dictionary::getMapping('specific.setting.typeOf.activity');
		foreach ($hours as $id => $value)
			$hoursLabels[ 'H'.$id] = $value;

		$columns = [];
		foreach($mapping as $key=>$value) {

			if (is_int($key)){
				$label = array_key_exists($value, $hoursLabels)?$hoursLabels[$value]:Dictionary::getDict('timesreports.sqlLabels.' . $value);
				$column = new CSV\Column($value, $label);
			}else{
				$label = array_key_exists($key, $hoursLabels)?$hoursLabels[$key]:Dictionary::getDict('timesreports.sqlLabels.' . $key);
				$column = new CSV\Column($key, $label, $value);
			}


			$columns[] = $column;
		}

		return $columns;
	}

	protected function getColumnsForSimpleExtract(){

		$profilesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');

		$hours = Dictionary::getMapping('specific.setting.typeOf.activity');

		$mapping = [
			'lastName',
			'firstName',
			'reference',
			'typeOf' => function($value) use($profilesTypes){
				return Tools::mapData($value, $profilesTypes);
			},
			'term' => function($value){
				$date = explode('-',$value);
				return implode('/', ['01', $date[1], $date[0]]);
			}
		];

		foreach ($hours as $id=>$entry)
			$mapping[] =  'H' . $id;

		return $mapping;
	}

	protected function getColumnsForDetailedExtract(){

		$hours = Dictionary::getMapping('specific.setting.typeOf.activity');

		$profilesTypes = Dictionary::getMapping('specific.setting.typeOf.resource');

		$basedMapping = [
			'lastName',
			'firstName',
			'reference',
			'typeOf' => function($value) use ($profilesTypes){
				return Tools::mapData($value, $profilesTypes);
			},
			'TEMPS_DATE' => function($value){
				return Tools::convertDateForUI($value);
			},
			'ID_PROJET' => function($value, $row) use ($hours){
				if($value <= 0){
					if(array_key_exists(abs($value), $hours)) return $hours[abs($value)];
				}
				if(isset($row['PRJ_REFERENCE']) && $row['PRJ_REFERENCE']){
					$project =  $row['PRJ_REFERENCE'];
				}else{
					$project = 'PRJ'.$value;
				}

				if(isset($row['LOT_TITRE'], $row['ID_LOT'])){
					if($row['LOT_TITRE']) $project = $row['LOT_TITRE'] .' - '. $project;
					else $project = $row['ID_LOT'] .' - '. $project;
				}

				return $project;
			},
			'TEMPS_TYPEHREF',
			'TYPEH_NAME',
			'TEMPS_DUREE' => function($value){
				return Tools::convertDateForUI($value);
			},
		];

		return $basedMapping;
	}

	private function prepareDataForSimpleExtract($data){

		$flattenResult = [];

		$hours = Dictionary::getMapping('specific.setting.typeOf.activity');

		foreach($data as $timesReport) {
			/** @var TimesReport $timesReport */

			if (!is_array($timesReport->NORMAL_TIME)) continue;

			$row = [
				'PROFIL_NOM'       => $timesReport->resource->PROFIL_NOM,
				'PROFIL_PRENOM'    => $timesReport->resource->PROFIL_PRENOM,
				'PROFIL_REFERENCE' => $timesReport->resource->PROFIL_REFERENCE,
				'PROFIL_TYPE'      => $timesReport->resource->typeOf,
				'LISTETEMPS_DATE'  => $timesReport->term,
			];
			foreach ($hours as $id => $val) {
				$entry[ 'H' . $id ] = 0;
			}

			foreach ($timesReport->NORMAL_TIME as $lignetps) {
				if (isset($lignetps['LISTEJOURS'])) {

					$row = array_merge($row, array_filter($lignetps, function ($k) {
						return in_array($k, ['ID_PROJET', 'ID_LOT', 'PRJ_REFERENCE', 'LOT_TITRE', 'TYPEH_NAME']);
					}, ARRAY_FILTER_USE_KEY));

					foreach ($lignetps['LISTEJOURS'] as $jour) {
						foreach ($hours as $id => $val) {
							if ($id == $lignetps['TYPEH_TYPEACTIVITE'])
								$row[ 'H' . $id ] += $jour['TEMPS_DUREE'];
						}
					}
				}
			}

			//TODO foreach LISTETEMPSEXCEPTION
			foreach($timesReport->EXCEPTIONAL_TIME as $tpsexp){
				$startDate = explode(' ', $tpsexp['TEMPSEXP_DEBUT']);
				$startDay = explode('-', $startDate[0]);
				$startTime = explode(':', $startDate[1]);
				$endDate = explode(' ', $tpsexp['TEMPSEXP_FIN']);
				$endDay = explode('-', $endDate[0]);
				$endTime = explode(':', $endDate[1]);

				$calendarLength = ceil((mktime($endTime[0], $endTime[1], $endTime[2], $endDay[1], $endDay[2], $endDay[0]) - mktime($startTime[0], $startTime[1], $startTime[2], $startDay[1], $startDay[2], $startDay[0]))/86400+1);
				$hourlyLength = round((mktime($endTime[0], $endTime[1], $endTime[2], $endDay[1], $endDay[2], $endDay[0]) - mktime($startTime[0], $startTime[1], $startTime[2], $startDay[1], $startDay[2], $startDay[0]))/3600, 2);

				foreach ($hours as $id => $entry) {
					if ($id == $tpsexp['TYPEH_TYPEACTIVITE']){
						if($tpsexp['TYPEH_TYPEACTIVITE'] == 4){
							$row[ 'H' . $id ] += $calendarLength;
						}else{
							$row[ 'H' . $id ] += $hourlyLength;
						}
					}
				}
			};

			$flattenResult[] = $row;
		}

		return $flattenResult;
	}

	private function prepareDataForDetailedExtract($data){

		$flattenResult = [];

		foreach($data as $timesReport) {
			/** @var TimesReport $timesReport */

			if (!is_array($timesReport->NORMAL_TIME)) continue;

			foreach($timesReport->NORMAL_TIME as $lignetps){

				if (!isset($lignetps['LISTEJOURS'])) continue;

				$rowTps = array_filter($lignetps, function ($k) {
					return in_array($k, ['ID_PROJET', 'ID_LOT', 'PRJ_REFERENCE', 'LOT_TITRE', 'TYPEH_NAME', 'LIGNETEMPS_TYPEHREF']);
				}, ARRAY_FILTER_USE_KEY);

				foreach ($lignetps['LISTEJOURS'] as $jour) {
					$row = [
						'PROFIL_NOM'       => $timesReport->resource->PROFIL_NOM,
						'PROFIL_PRENOM'    => $timesReport->resource->PROFIL_PRENOM,
						'PROFIL_REFERENCE' => $timesReport->resource->PROFIL_REFERENCE,
						'PROFIL_TYPE'      => $timesReport->resource->typeOf,
						'TEMPS_DATE'       => $jour['TEMPS_DATE'],
						'TEMPS_TYPEHREF'   => $lignetps['LIGNETEMPS_TYPEHREF'],
						'TEMPS_DUREE'      => $jour['TEMPS_DUREE'],
					];

					$flattenResult[] = array_merge($row, $rowTps);
				}
			}

			foreach($timesReport->EXCEPTIONAL_TIME as $tpsexp){

				$rowTps = array_filter($tpsexp, function ($k) {
					return in_array($k, ['ID_PROJET', 'ID_LOT', 'PRJ_REFERENCE', 'LOT_TITRE', 'TYPEH_NAME', 'LIGNETEMPS_TYPEHREF']);
				}, ARRAY_FILTER_USE_KEY);

				$startDate = explode(' ', $tpsexp['TEMPSEXP_DEBUT']);
				$startDay = explode('-', $startDate[0]);
				$startTime = explode(':', $startDate[1]);
				$endDate = explode(' ', $tpsexp['TEMPSEXP_FIN']);
				$endDay = explode('-', $endDate[0]);
				$endTime = explode(':', $endDate[1]);

				$calendarLength = ceil((mktime($endTime[0], $endTime[1], $endTime[2], $endDay[1], $endDay[2], $endDay[0]) - mktime($startTime[0], $startTime[1], $startTime[2], $startDay[1], $startDay[2], $startDay[0]))/86400+1);
				$hourlyLength = round((mktime($endTime[0], $endTime[1], $endTime[2], $endDay[1], $endDay[2], $endDay[0]) - mktime($startTime[0], $startTime[1], $startTime[2], $startDay[1], $startDay[2], $startDay[0]))/3600, 2);

				$row = [
					'PROFIL_NOM'       => $timesReport->resource->PROFIL_NOM,
					'PROFIL_PRENOM'    => $timesReport->resource->PROFIL_PRENOM,
					'PROFIL_REFERENCE' => $timesReport->resource->PROFIL_REFERENCE,
					'PROFIL_TYPE'      => $timesReport->resource->typeOf,
					'TEMPS_DATE'       => $tpsexp['TEMPSEXP_DEBUT'], // != v6 (bug sur v6, variable non initialisée)
					'TEMPS_TYPEHREF'   => $tpsexp['TEMPSEXP_TYPEHREF'],
					'TEMPS_DUREE'      => ($tpsexp['TYPEH_TYPEACTIVITE']==4)?$calendarLength:$hourlyLength,

				];

				$flattenResult[] = array_merge($row, $rowTps);
			}
		}

		return $flattenResult;
	}
}
