<?php
/**
 * flags.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Flags\Filters\SearchFlags;
use BoondManager\Databases\Local\Flag as DBFlag;
use BoondManager\Databases\Mapper;
use BoondManager\Models\Flag;
use Wish\Filters\AbstractJsonAPI;
use Wish\Models\SearchResult;

class Flags {
	/**
	 * @param int id
	 * @return Flag|false
	 */
	public static function find($id) {
		$filter = new SearchFlags();
		$filter->keywords->setValue( Flag::buildReference($id) );

		$sql = new DBFlag();
		$result = $sql->searchFlags($filter);

		if($result->rows) return Mapper\Flag::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * Search flags
	 * @param SearchFlags $filter
	 * @return SearchResult
	 */
	public static function search(SearchFlags $filter) {
		$db = new DBFlag();
		$searchResult = $db->searchFlags($filter);

		return Mapper\Flag::fromSearchResult($searchResult);
	}

	/**
	 * Get flag
	 * @param int $id
	 * @return Flag
	 */
	public static function get($id) {
		$db = DBFlag::instance();
		$dbData = $db->getFlag($id);

		$flag = false;
		if($dbData) $flag = Mapper\Flag::fromSQL($dbData);
		return $flag;
	}

	/**
	 * update flag
	 * @param Flag $flag
	 * @return boolean
	 */
	public static function update(Flag &$flag) {
		// transform flag to sqlData
		$sqlData = Mapper\Flag::toSQL($flag);

		$db = new DBFlag();
		if($db->updateFlag($flag->id, $sqlData))
			return true;
		else
			return false;
	}

	/**
	 * Create flag
	 * @param Flag $flag
	 * @return boolean
	 */
	public static function create(Flag &$flag) {
		//transform flag to sqlData
		$sqlData = Mapper\Flag::toSQL($flag);

		$db = new DBFlag();
		$flag->id = $db->createFlag($sqlData);
		if($flag->id) {
			$flag = self::get($flag->id);
			return true;
		} else return false;
	}

	/**
	 * Delete flag
	 * @param int $id
	 * @return boolean
	 */
	public static function delete($id) {
		$db = new DBFlag();

		if($db->isFlagReducible($id) && $db->deleteFlag($id))
			return true;
		else
			return false;
	}

	/**
	 * Build flag from filter
	 * @param AbstractJsonAPI $filter
	 * @param Flag|null $flag
	 * @return Flag
	 */
	public static function buildFromFilter($filter, Flag $flag = null) {
		if(!$flag) $flag = new Flag();

		//filter and calculate all flags data
		$flag->fromArray($filter->get('attributes')->getValue());
		$flag->fromArray($filter->get('relationships')->getValue());

		return $flag;
	}

	/**
	 * @param $id
	 * @return \BoondManager\Models\Flag[]
	 */
	public static function getFlagsFromMainManager($id){
		$db = new DBFlag();
		$flags = $db->getFlagsFromProfilIds($id);

		$tabFlags = [];
		foreach($flags as $flag)
			$tabFlags[] = Mapper\Flag::fromSQL($flag);

		return $tabFlags;
	}
}
