<?php
/**
 * ged.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\Lib\Models\AbstractFile;
use BoondManager\Models\Document;
use BoondManager\Models\File;
use BoondManager\Models\Proof;
use BoondManager\Models\Resume;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\APIs;
use BoondManager\OldModels\Filters\Search\Documents;
use BoondManager\Models;
use Wish\Models\ModelJSONAPI;
use Wish\Models\SearchResult;
use Wish\Specifications\AbstractSpecificationItem;
use Wish\Tools;

class GED{

	const QUOTA_RESUME = 5;
	const QUOTA_ATTACHEDFILES = 10;
	const QUOTA_JUSTIFICATIFS = 50;
	const MAXSIZE_RESUME = 15728640; // 15Mo
	const MAXSIZE_ATTACHED= 15728640; // 15Mo

	public static function getTypeFromParentType($parentType)
	{
		switch ($parentType) {
			case Document::PARENT_TYPE_ACTION:
			case Document::PARENT_TYPE_COMPANY:
			case Document::PARENT_TYPE_PROJECT:
			case Document::PARENT_TYPE_ORDER:
			case Document::PARENT_TYPE_PRODUCT:
			case Document::PARENT_TYPE_OPPORTUNITY:
			case Document::PARENT_TYPE_PURCHASE:
			case Document::PARENT_TYPE_DELIVERY:
			case Document::PARENT_TYPE_POSITIONING:
				return Local\File::TYPE_DOCUMENT;
			case Resume::PARENT_TYPE_RESOURCE_RESUME:
				return Local\File::TYPE_RESOURCE_RESUME;
			case Resume::PARENT_TYPE_CANDIDATE_RESUME:
				return Local\File::TYPE_CANDIDATE_RESUME;
			case File::PARENT_TYPE_RESOURCE:
			case File::PARENT_TYPE_CANDIDATE:
				return Local\File::TYPE_OTHER;
			case Proof::PARENT_TYPE_EXPENSES_REPORT:
			case Proof::PARENT_TYPE_ABSENCES_REPORT:
			case Proof::PARENT_TYPE_TIMES_REPORT:
			case Proof::PARENT_TYPE_PAYMENT:
				return Local\File::TYPE_JUSTIFICATIVE;
		}
	}

	public static function getDocumentParentType($parentType){
		return Tools::mapData($parentType, Document::MAPPER_DOCUMENT_PARENT_TYPE_ID);
	}

	public static function deleteFile(AbstractFile $file) {
		Local\File::instance()->deleteObject( $file->getSubTypeID(), $file->id);

		switch ($file->getParentType()) {
					/*
			case File::PARENT_TYPE_RESOURCE: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_ADMINISTRATIVE);
			case File::PARENT_TYPE_CANDIDATE: return new APIs\Candidates\Specifications\HaveWriteAccess(Models\Employee::TAB_ADMINISTRATIVE);
					*/
			case Resume::PARENT_TYPE_RESOURCE_RESUME: Notification\Employee::getInstance($file->dependsOn->id, Models\Employee::TAB_INFORMATION, [], [])->deleteCV($file->name);
			case Resume::PARENT_TYPE_CANDIDATE_RESUME: Notification\Candidate::getInstance($file->dependsOn->id, Models\Candidate::TAB_INFORMATION, [], [])->deleteCV($file->name);
			/*
			case Proof::PARENT_TYPE_ABSENCES_REPORT: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_ABSENCES);
			case Proof::PARENT_TYPE_EXPENSES_REPORT: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_EXPENSES);
			case Proof::PARENT_TYPE_TIMES_REPORT: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_TIMESHEETS);
			case Proof::PARENT_TYPE_PAYMENT: return new APIs\Payments\Specifications\HaveWriteAccess();
			case Document::PARENT_TYPE_COMPANY: return new APIs\Companies\Specifications\HaveWriteAccess(Models\Company::TAB_INFORMATION);
			case Document::PARENT_TYPE_OPPORTUNITY: return new APIs\Opportunities\Specifications\HaveWriteAccess(Models\Opportunity::TAB_INFORMATION);
			case Document::PARENT_TYPE_PROJECT: return new APIs\Projects\Specifications\HaveWriteAccess(Models\Company::TAB_INFORMATION);
			case Document::PARENT_TYPE_ORDER: return new APIs\Orders\Specifications\HaveWriteAccess();
			case Document::PARENT_TYPE_PRODUCT: return new APIs\Products\Specifications\HaveWriteAccess(Models\Product::TAB_INFORMATION);
			case Document::PARENT_TYPE_PURCHASE: return new APIs\Purchases\Specifications\HaveWriteAccess();
			case Document::PARENT_TYPE_ACTION: return new APIs\Actions\Specifications\HaveWriteAccess(); //TODO : verifier une fois que la spec est faite
			case Document::PARENT_TYPE_DELIVERY: return new APIs\Deliveries\Specifications\HaveWriteAccess(); //TODO : verifier une fois que la spec est faite
			case Document::PARENT_TYPE_POSITIONING: return new APIs\Positionings\Specifications\HaveWriteAccess(); // TODO: verifier une fois que la spec est faite
			*/
		}

		return true;
	}

	public static function download(AbstractFile $file) {
		\BoondManager\Lib\GED::instance()->downloadFileGED($file->id, $file->name, $file->getSubTypeID());
	}

	/**
	 * @param $entity
	 * @param $file
	 * @param $type
	 * @return AbstractFile|false
	 * @throws \Exception
	 */
	public static function upload($entity, $file, $type)
	{
		switch ($type) {
			case Local\File::TYPE_CANDIDATE_RESUME:
			case Local\File::TYPE_RESOURCE_RESUME:
				return self::uploadResume($entity, $file);
			case Local\File::TYPE_OTHER:
				return self::uploadOtherFile($entity, $file);
			case Local\File::TYPE_DOCUMENT:
				return self::uploadDocument($entity, $file);
			case Local\File::TYPE_JUSTIFICATIVE:
				return self::uploadProof($entity, $file);
			default:
				throw new \Exception("unknown '$type' type ");
		}
	}

	/**
	 * @param $getDocumentType
	 * @param ModelJSONAPI $parent
	 * @return Document|File|Proof|Resume
	 * @throws \Exception
	 */
	public static function create($getDocumentType, $parent)
	{
		switch ($getDocumentType) {
			case Local\File::TYPE_CANDIDATE_RESUME:
			case Local\File::TYPE_RESOURCE_RESUME:
				return new Resume(['dependsOn' => $parent]);
			case Local\File::TYPE_OTHER:
				return new File(['dependsOn' => $parent]);
			case Local\File::TYPE_DOCUMENT:
				return new Document(['dependsOn' => $parent]);
			case Local\File::TYPE_JUSTIFICATIVE:
				return new Proof(['dependsOn' => $parent]);
			default:
				throw new \Exception("unknown '$getDocumentType' type ");
		}
	}

	/**
	 * @param Documents $filter
	 * @return SearchResult
	 */
	public function search(Documents $filter)
	{
		$dbFile = Local\File::instance();
		return $dbFile->search($filter);
	}

	public static function getGEDResumeType($entity){
		if($entity instanceof Models\Employee) return Local\File::TYPE_RESOURCE_RESUME;
		else return Local\File::TYPE_CANDIDATE_RESUME;
	}

	/**
	 * @param int $id
	 * @return Resume
	 */
	public static function getResume($id) {
		$db = Local\File::instance();
		$data = $db->getObject(Local\File::TYPE_RESOURCE_RESUME, $id);

		return $data ? Mapper\Resume::fromSQL($data) : null;
	}

	/**
	 * @param $id
	 * @return Proof
	 */
	public static function getProof($id) {
		$db = Local\File::instance();
		$data = $db->getObject(Local\File::TYPE_JUSTIFICATIVE, $id);

		return $data ? Mapper\Proof::fromSQL($data) : null;
	}

	/**
	 * @param $id
	 * @return File
	 */
	public static function getFile($id) {
		$db = Local\File::instance();
		$data = $db->getObject(Local\File::TYPE_OTHER, $id);

		return $data ? Mapper\File::fromSQL($data) : null;
	}

	/**
	 * @param $id
	 * @return Document
	 */
	public static function getDocument($id) {
		$db = Local\File::instance();
		$data = $db->getObject(Local\File::TYPE_DOCUMENT, $id);

		return $data ? Mapper\Document::fromSQL($data) : null;
	}

	/**
	 * @param $id
	 * @param $type
	 * @return Document|File|Proof|Resume
	 */
	public static function get($id, $type) {
		switch ($type) {
			case Local\File::TYPE_RESOURCE_RESUME:
			case Local\File::TYPE_CANDIDATE_RESUME:
				return self::getResume($id);
			case Local\File::TYPE_DOCUMENT:
				return self::getDocument($id);
			case Local\File::TYPE_JUSTIFICATIVE:
				return self::getProof($id);
			case Local\File::TYPE_OTHER:
				return self::getFile($id);
		}
	}

	/**
	 * Upload a resume
	 * @param Models\Employee|Models\Candidate $resource parent object
	 * @param array $fileInput the $_FILES['file'] array
	 * @return Resume|false the CV ID created or false if something went wrong
	 * @throws \Exception on over quota
	 */
	private static function uploadResume($resource, $fileInput){

		if($resource->numberOfResumes >= self::QUOTA_RESUME)
			throw new \Exception(BM::getErrorMessage(BM::ERROR_GLOBAL_GED_QUOTA_MAX), BM::ERROR_GLOBAL_GED_QUOTA_MAX);

		$ged = \BoondManager\Lib\GED::instance();

		if(Subscriptions::getMaxStorage() <= $ged->getDirectorySize(-1))
			throw new \Exception(BM::getErrorMessage(BM::ERROR_GLOBAL_GED_QUOTA_MAX), BM::ERROR_GLOBAL_GED_QUOTA_MAX);

		$pj = $ged->updateCV($fileInput, self::MAXSIZE_RESUME);

		if(!$pj) return false;

		$dbFile = Local\File::instance();
		$type = self::getGEDResumeType($resource);
		$idCV = $dbFile->setObjectFromParent( $type , $pj['CV_PATH'], $pj['CV_NAME'], $pj['CV_TEXT'], $resource->id, false, $docType = 0);

		if($resource instanceof Models\Employee) {
			$notifService = Notification\Employee::getInstance($resource->id, Models\Employee::TAB_INFORMATION, $resource->toArray(), $newData = []);
			$notifService->addCV($fileInput['name']);
		}else {
			$notifService = Notification\Candidate::getInstance($resource->id, Models\Candidate::TAB_INFORMATION, $resource->toArray(), $newData = []);
			$notifService->addCV($fileInput['name']);
		}

		return self::getResume($idCV);
	}

	/**
	 * upload a file for the administrative tab (resource/candidate)
	 * @param Models\Employee|Models\Candidate $resource
	 * @param array $fileInput
	 * @return false|File
	 * @throws \Exception
	 */
	private static function uploadOtherFile($resource, $fileInput){
		$id = self::uploadFile($fileInput, Local\File::TYPE_OTHER, $resource->id, 0, count($resource->files));
		return self::getFile($id);
	}

	/**
	 * upload a document for an entity
	 * @param \Wish\Models\Model $entity
	 * @param array $fileInput
	 * @return Document|false
	 * @throws \Exception
	 */
	private static function uploadDocument($entity, $fileInput){
		if($entity instanceof Models\Action) {
			$total    = $entity->numberOfFiles;
			$id       = $entity->id;
			$category = BM::CATEGORY_ACTION;
		}else if($entity instanceof Models\Product) {
			$total    = count($entity->files);
			$id       = intval($entity->getID());
			$category = BM::CATEGORY_PRODUCT;
		}else if($entity instanceof Models\Company) {
			$total    = count($entity->files);
			$id       = intval($entity->getID());
			$category = BM::CATEGORY_CRM_COMPANY;
		}else{
			throw new \Exception('Unknown entity');
		}

		$id = self::uploadFile($fileInput, Local\File::TYPE_DOCUMENT, $id, $category, $total);
		return self::getDocument($id);
	}

	private static function uploadProof($entity, $fileInput){
		$id = self::uploadFile($fileInput, Local\File::TYPE_JUSTIFICATIVE, $entity->id);
		return self::getProof($id);
	}

	/**
	 * upload a file
	 * @param array $fileInput
	 * @param int $fileType (TYPE, TYPE_OTHER
	 * @param int $idParent
	 * @param int $parentType see docType
	 * @param int $totalUploadedFiles number of file alreadyUploaded
	 * @return bool|int
	 * @throws \Exception
	 */
	private static function uploadFile($fileInput, $fileType, $idParent, $parentType, $totalUploadedFiles){
		if($totalUploadedFiles >= self::QUOTA_ATTACHEDFILES)
			throw new \Exception(BM::getErrorMessage(BM::ERROR_GLOBAL_GED_QUOTA_MAX), BM::ERROR_GLOBAL_GED_QUOTA_MAX);

		$ged = \BoondManager\Lib\GED::instance();

		if(Subscriptions::getMaxStorage() <= $ged->getDirectorySize(-1))
			throw new \Exception(BM::getErrorMessage(BM::ERROR_GLOBAL_GED_QUOTA_MAX), BM::ERROR_GLOBAL_GED_QUOTA_MAX);

		$pj = $ged->updateFile($fileInput, self::MAXSIZE_ATTACHED);

		if(!$pj) return false;

		$dbFile = Local\File::instance();
		$idFile = $dbFile->setObjectFromParent( $fileType , $pj['FILE_PATH'], $pj['FILE_NAME'], null, $idParent, false, $parentType);

		return $idFile;
	}

	public static function getExtendedEntity($entity){
		if($entity instanceof Models\Employee){
			return Employees::get($entity->id);
		}else if($entity instanceof Models\Candidate){
			return Candidates::get($entity->id);
		}else if($entity instanceof Models\Company){
			return Companies::get($entity->id);
		}else if($entity instanceof Models\Opportunity){
			return Opportunities::get($entity->id);
		}else if($entity instanceof Models\Project){
			return Projects::get($entity->id);
		}else if($entity instanceof Models\Order){
			return Orders::get($entity->id);
		}else if($entity instanceof Models\Product){
			return Products::get($entity->id);
		}else if($entity instanceof Models\Purchase){
			return Purchases::get($entity->id);
		}else if($entity instanceof Models\Payment){
			return Payments::get($entity->id);
		}else if($entity instanceof Models\Action){
			return Actions::get($entity->id);
		}else if($entity instanceof Models\Delivery){
			return Deliveries::get($entity->id);
		}else if($entity instanceof Models\Positioning){
			return Positionings::get($entity->id);
		}else {
			return null;
		}
	}

	/**
	 * @param BoondManager_ObjectBDD $dataBDD
	 * @param string $todo
	 * @param string $index
	 * @param array $tabDocument
	 * @param string $idfile
	 * @param string $typeFiche
	 * @param string $idFiche
	 * @param string $ongletFiche
	 * @param $filetype
	 * @param $quota
	 * @param bool $manageMebuBar
	 * @param string $appviewer
	 * @param string $baction
	 * @param string $htmlFile
	 * @DEPRECATED
	 */
	public function uploadFileGED(BoondManager_ObjectBDD $dataBDD, $todo = '', $index = '', $tabDocument = array(), $idfile = '', $typeFiche = '', $idFiche = '', $ongletFiche = '', $filetype = FILE_CVCANDIDAT, $quota = QUOTA_PIECEJOINTE, $manageMebuBar = false, $appviewer = '', $baction = '', $htmlFile = 'file') {
		die('deprecated'); // ON LE GARDE UNIQUEMENT POUR FAIRE LA MIGRATION
		if(sizeof($tabDocument) == 6) {
			//On configure la réponse XML
			Wish_Registry::getInstance()->set('wish_headers', array("Content-Type" => "text/xml")); //On force le viewer à indiquer qu'il s'agit d'une réponse XML
			Wish_Registry::getInstance()->set('wish_layouttype', 'xml'); //On indique au viewer que le document est de l'XML

			$response = '';
			$result = false;
			switch($todo) {
				case 'delete':
					$fileData = new BoondManager_ObjectBDD_File($dataBDD->getDBinstance(), $this);
					$idfileReturn = 0;
					//On vérifie si le document appartient à la fiche
					if($dataBDD->isRegistered($tabDocument[0])) {foreach($dataBDD->get($tabDocument[0]) as $cv) {if($cv[$tabDocument[1]] == $idfile) {$result = true;$filename = $cv[$tabDocument[2]];break;}}}
					if($result) {
						if(sizeof($dataBDD->get($tabDocument[0])) == $quota) $result = 2;
						$fileData->deleteFileData($filetype, $idfile);
						//Gestion des notifications
						switch($typeFiche) {
							case 'candidat':
								if($manageMebuBar)
									BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteCV($filename);
								else
									BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);
								break;
							case 'ressource':
								if($manageMebuBar)
									BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteCV($filename);
								else
									BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);
								break;
							case 'achat':BoondManager_Notification_Achat::getInstance($dataBDD->get('ID_ACHAT'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'paiement':BoondManager_Notification_Paiement::getInstance($dataBDD->get('ID_PAIEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deletePJ('PMT'.$dataBDD->get('ID_PAIEMENT'), $filename);break;
							case 'action':BoondManager_Notification_Action::getInstance($dataBDD->get('ID_ACTION'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->update();break;
							case 'besoin':BoondManager_Notification_Besoin::getInstance($dataBDD->get('ID_AO'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'positionnement':BoondManager_Notification_Positionnement::getInstance($dataBDD->get('ID_POSITIONNEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'crmsociete':BoondManager_Notification_CRMSociete::getInstance($dataBDD->get('ID_CRMSOCIETE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'commande':BoondManager_Notification_Commande::getInstance($dataBDD->get('ID_BONDECOMMANDE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'produit':BoondManager_Notification_Produit::getInstance($dataBDD->get('ID_PRODUIT'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'projet':BoondManager_Notification_Projet::getInstance($dataBDD->get('ID_PROJET'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'mission':BoondManager_Notification_Mission::getInstance($dataBDD->get('ID_MISSIONPROJET'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->deleteDocument($filename);break;
							case 'temps':break;
							case 'frais':break;
							case 'absence':break;
						}
						if($manageMebuBar) {if(sizeof($dataBDD->get($tabDocument[0])) == 2) {foreach($dataBDD->get($tabDocument[0]) as $cv) {if($cv[$tabDocument[1]] != $idfile) {$idfileReturn = $cv[$tabDocument[1]];break;}}} else if(sizeof($dataBDD->get($tabDocument[0])) == 1) $idfileReturn = -1;}
					}
					$response = '<file todo="delete" value="'.intval($result).'" filename="" idfile="'.$idfileReturn.'" index="'.convertHTMLSpecialChars($index).'" idfiche="'.convertHTMLSpecialChars($idFiche).'" ongletfiche="'.convertHTMLSpecialChars($ongletFiche).'" typefiche="'.convertHTMLSpecialChars($typeFiche).'" appviewer="'.convertHTMLSpecialChars($appviewer).'" urlbase="'.convertHTMLSpecialChars(APP_URL).'" baction="'.convertHTMLSpecialChars($baction).'" vajax="wajax" vtab="wtabFile" language="'.convertHTMLSpecialChars(Wish_Session::getInstance()->get('wish_language')).'" />';
					break;
				case 'join':
					$fileData = new BoondManager_ObjectBDD_File($dataBDD->getDBinstance(), $this);

					$idfile = false;
					$filename = '';

					//On vérifie si la capacité de stockage n'est pas atteinte
					$maxquota = false;
					$todo = 'join';
					$abonnementBDD = new BoondManager_ObjectBDD_Abonnement($dataBDD->getDBinstance());
					if($abonnementBDD->getAbonnementGroupe() && $abonnementBDD->get('AB_MAXSTORAGE') <= $this->getDirectorySize(-1)) {$maxquota = true;$todo = 'quota-max';}

					if(!$maxquota && sizeof($dataBDD->get($tabDocument[0])) < $quota && intval($dataBDD->get($tabDocument[4])) > 0) {
						$pjData = ($manageMebuBar)?$this->updateCV($htmlFile, 8192000):$this->updateFile($htmlFile, 8192000);
						if(sizeof($pjData) > 0) {
							$idfile = $fileData->newFileData($filetype, $pjData[$tabDocument[3]], $pjData[$tabDocument[2]], ($manageMebuBar)?$pjData['CV_TEXT']:'', $dataBDD->get($tabDocument[4]), false, $tabDocument[5]);
							$filename = $pjData[$tabDocument[2]];
							//Gestion des notifications
							switch($typeFiche) {
								case 'candidat':
									if($manageMebuBar)
										BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addCV($filename);//Gestion des notifications
									else
										BoondManager_Notification_Candidat::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);//Gestion des notifications
									break;
								case 'ressource':
									if($manageMebuBar)
										BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addCV($filename);
									else
										BoondManager_Notification_Ressource::getInstance($dataBDD->get('ID_PROFIL'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);
									break;
								case 'achat':BoondManager_Notification_Achat::getInstance($dataBDD->get('ID_ACHAT'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'paiement':BoondManager_Notification_Paiement::getInstance($dataBDD->get('ID_PAIEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addPJ('PMT'.$dataBDD->get('ID_PAIEMENT'), $filename);break;
								case 'action':BoondManager_Notification_Action::getInstance($dataBDD->get('ID_ACTION'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->update();break;
								case 'besoin':BoondManager_Notification_Besoin::getInstance($dataBDD->get('ID_AO'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'positionnement':BoondManager_Notification_Positionnement::getInstance($dataBDD->get('ID_POSITIONNEMENT'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'crmsociete':BoondManager_Notification_CRMSociete::getInstance($dataBDD->get('ID_CRMSOCIETE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'commande':BoondManager_Notification_Commande::getInstance($dataBDD->get('ID_BONDECOMMANDE'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'produit':BoondManager_Notification_Produit::getInstance($dataBDD->get('ID_PRODUIT'), 0, $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'projet':BoondManager_Notification_Projet::getInstance($dataBDD->get('ID_PROJET'), intval($ongletFiche), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'mission':BoondManager_Notification_Mission::getInstance($dataBDD->get('ID_MISSIONPROJET'), $dataBDD->getData(), ($emptyData = array()), $dataBDD->getDBInstance())->addDocument($filename);break;
								case 'temps':break;
								case 'frais':break;
								case 'absence':break;
							}
						}
					}

					if($idfile) {if(sizeof($dataBDD->get($tabDocument[0])) < ($quota-1)) $result = 2; else $result = true;} else $result = false;
					//On renvoit une page simple avec le script ci-dessous avec d'informer l'IFRAME de la page de se rafraîchir
					echo '<script language="javascript" type="text/javascript">window.top.window.Wish_StopUpload(\''.intval($result).'\',\''.convertHTMLSpecialChars($index).'\',\''.$todo.'\',\''.convertHTMLSpecialChars($filename).'\',\''.$idfile.'\',\''.convertHTMLSpecialChars($idFiche).'\',\''.convertHTMLSpecialChars($ongletFiche).'\',\''.convertHTMLSpecialChars($typeFiche).'\',\''.convertHTMLSpecialChars($appviewer).'\',\''.convertHTMLSpecialChars(APP_URL).'\',\''.convertHTMLSpecialChars($baction).'\',\'wajax\',\'wtabFile\',\''.convertHTMLSpecialChars(Wish_Session::getInstance()->get('wish_language')).'\');</script>';
					$dataBDD->dbClose();
					exit;//On quitte car le script ne doit rien renvoyer d'autre au navigateur client
					break;
			}

			//On positionne la réponse XML
			Wish_Registry::getInstance()->set('wish_xmlmessage', $response);
		} else {
			//En cas d'erreur on renvoit le script indiquant de terminer le webservices (et donc de cacher la barre de chargement
			echo '<script language="javascript" type="text/javascript">window.top.window.Wish_StopUpload(\'0\',\''.convertHTMLSpecialChars($index).'\',\''.$todo.'\',\'\',\'0\',\''.convertHTMLSpecialChars($idFiche).'\',\''.convertHTMLSpecialChars($ongletFiche).'\',\''.convertHTMLSpecialChars($typeFiche).'\',\''.convertHTMLSpecialChars($appviewer).'\',\''.convertHTMLSpecialChars(APP_URL).'\',\''.convertHTMLSpecialChars($baction).'\',\'wajax\',\'wtabFile\',\''.convertHTMLSpecialChars(Wish_Session::getInstance()->get('wish_language')).'\');</script>';
			$dataBDD->dbClose();
			exit;//On quitte car le script ne doit rien renvoyer d'autre au navigateur client
		}
	}

	/**
	 * @param AbstractFile $file
	 * @return AbstractSpecificationItem
	 */
	public static function getReadSpec(AbstractFile $file){

		switch ($file->getParentType()) {
			case File::PARENT_TYPE_RESOURCE: return new APIs\Employees\Specifications\HaveReadAccess(Models\Employee::TAB_ADMINISTRATIVE);
			case File::PARENT_TYPE_CANDIDATE: return new APIs\Candidates\Specifications\HaveReadAccess(Models\Employee::TAB_ADMINISTRATIVE);
			case Resume::PARENT_TYPE_RESOURCE_RESUME: return new APIs\Employees\Specifications\HaveReadAccess(Models\Employee::TAB_INFORMATION);
			case Resume::PARENT_TYPE_CANDIDATE_RESUME: return new APIs\Candidates\Specifications\HaveReadAccess(Models\Employee::TAB_INFORMATION);
			case Proof::PARENT_TYPE_ABSENCES_REPORT: return new APIs\Employees\Specifications\HaveReadAccess(Models\Employee::TAB_ABSENCESREPORTS);
			case Proof::PARENT_TYPE_EXPENSES_REPORT: return new APIs\Employees\Specifications\HaveReadAccess(Models\Employee::TAB_EXPENSESREPORTS);
			case Proof::PARENT_TYPE_TIMES_REPORT: return new APIs\Employees\Specifications\HaveReadAccess(Models\Employee::TAB_TIMESREPORTS);
			case Proof::PARENT_TYPE_PAYMENT: return new APIs\Payments\Specifications\HaveReadAccess();
			case Document::PARENT_TYPE_COMPANY: return new APIs\Companies\Specifications\HaveReadAccess(Models\Company::TAB_INFORMATION);
			case Document::PARENT_TYPE_OPPORTUNITY: return new APIs\Opportunities\Specifications\HaveReadAccess(Models\Opportunity::TAB_INFORMATION);
			case Document::PARENT_TYPE_PROJECT: return new APIs\Projects\Specifications\HaveReadAccess(Models\Company::TAB_INFORMATION);
			case Document::PARENT_TYPE_ORDER: return new APIs\Orders\Specifications\HaveReadAccess();
			case Document::PARENT_TYPE_PRODUCT: return new APIs\Products\Specifications\HaveReadAccess(Models\Product::TAB_INFORMATION);
			case Document::PARENT_TYPE_PURCHASE: return new APIs\Purchases\Specifications\HaveReadAccess();
			case Document::PARENT_TYPE_ACTION: return new APIs\Actions\Specifications\HaveReadAccess(); //TODO : verifier une fois que la spec est faite
			case Document::PARENT_TYPE_DELIVERY: return new APIs\Deliveries\Specifications\HaveReadAccess(); //TODO : verifier une fois que la spec est faite
			case Document::PARENT_TYPE_POSITIONING: return new APIs\Positionings\Specifications\HaveReadAccess(); // TODO: verifier une fois que la spec est faite
		}
	}

	/**
	 * @param AbstractFile $file
	 * @return AbstractSpecificationItem
	 */
	public static function geWriteSpec(AbstractFile $file){

		switch ($file->getParentType()) {
			case File::PARENT_TYPE_RESOURCE: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_ADMINISTRATIVE);
			case File::PARENT_TYPE_CANDIDATE: return new APIs\Candidates\Specifications\HaveWriteAccess(Models\Employee::TAB_ADMINISTRATIVE);
			case Resume::PARENT_TYPE_RESOURCE_RESUME: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_INFORMATION);
			case Resume::PARENT_TYPE_CANDIDATE_RESUME: return new APIs\Candidates\Specifications\HaveWriteAccess(Models\Employee::TAB_INFORMATION);
			case Proof::PARENT_TYPE_ABSENCES_REPORT: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_ABSENCESREPORTS);
			case Proof::PARENT_TYPE_EXPENSES_REPORT: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_EXPENSESREPORTS);
			case Proof::PARENT_TYPE_TIMES_REPORT: return new APIs\Employees\Specifications\HaveWriteAccess(Models\Employee::TAB_TIMESREPORTS);
			case Proof::PARENT_TYPE_PAYMENT: return new APIs\Payments\Specifications\HaveWriteAccess();
			case Document::PARENT_TYPE_COMPANY: return new APIs\Companies\Specifications\HaveWriteAccess(Models\Company::TAB_INFORMATION);
			case Document::PARENT_TYPE_OPPORTUNITY: return new APIs\Opportunities\Specifications\HaveWriteAccess(Models\Opportunity::TAB_INFORMATION);
			case Document::PARENT_TYPE_PROJECT: return new APIs\Projects\Specifications\HaveWriteAccess(Models\Company::TAB_INFORMATION);
			case Document::PARENT_TYPE_ORDER: return new APIs\Orders\Specifications\HaveWriteAccess();
			case Document::PARENT_TYPE_PRODUCT: return new APIs\Products\Specifications\HaveWriteAccess(Models\Product::TAB_INFORMATION);
			case Document::PARENT_TYPE_PURCHASE: return new APIs\Purchases\Specifications\HaveWriteAccess();
			case Document::PARENT_TYPE_ACTION: return new APIs\Actions\Specifications\HaveWriteAccess(); //TODO : verifier une fois que la spec est faite
			case Document::PARENT_TYPE_DELIVERY: return new APIs\Deliveries\Specifications\HaveWriteAccess(); //TODO : verifier une fois que la spec est faite
			case Document::PARENT_TYPE_POSITIONING: return new APIs\Positionings\Specifications\HaveWriteAccess(); // TODO: verifier une fois que la spec est faite
		}
	}
}
