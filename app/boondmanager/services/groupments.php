<?php
/**
 * groupments.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Groupments\Filters;
use BoondManager\APIs\Groupments\Specifications\HaveReadAccess;
use BoondManager\APIs\Groupments\Specifications\HaveWriteAccess;
use BoondManager\APIs\Groupments\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

/**
 * Class Deliveries
 * @package BoondManager\Models\Services
 */
class Groupments{

	/**
	 * @param $id
	 * @return Models\Groupment|null
	 */
	public static function get($id)
	{
		$db = Local\Delivery::instance();
		$data = $db->getDelivery($id);

		if(!$data) return null;

		$isGroupment = $data['PRJ_TYPE'] > 0 && !in_array($data['ITEM_TYPE'], [Models\Delivery::TYPE_RESOURCE, Models\Delivery::TYPE_PRODUCT]);
		if(!$isGroupment) return null;

		$groupment = Mapper\Groupment::fromSQL($data);

		return $groupment;
	}

	/**
	 * Build project from filter
	 * @param Filters\Entity $filter
	 * @param Models\Groupment $groupment
	 * @return Models\Groupment
	 */
	public static function buildFromFilter($filter, $groupment = null) {
		if(!$groupment) {
			$groupment = new Models\Groupment();//filter and calculate all projects data
			$groupment->mergeWith( $filter->toObject() );
		}else{
			$groupment->mergeWith($filter->toObject());
		}

		$groupment->calculateData();

		return $groupment;
	}

	/**
	 * notify the groupment that deliveries data have change (and therefore, recalculate datas)
	 * @param int $id Groupment ID
	 * @return Models\Groupment|null
	 */
	public static function deliveriesChangedTrigger($id){
		$groupment = self::get($id);
		if(!$groupment) return null;

		$groupment->calculateDates();
		$groupment->calculateDaysFree();

		self::update($groupment);

		return $groupment;
	}

	/**
	 * Create project
	 * @param Models\Groupment $groupment
	 * @return boolean
	 */
	public static function create(Models\Groupment &$groupment) {
		//transform project to sqlData
		$sqlData = Mapper\Groupment::toSQL($groupment);

		$db = Local\Delivery::instance();
		$groupment->id = $db->createDelivery($sqlData);

		if($groupment->id) {
			$groupment = self::get($groupment->id);
			return true;
		} else return false;
	}

	/**
	 * @param Models\Groupment $groupment
	 * @return bool
	 */
	public static function update(&$groupment) {
		//transform project to sqlData
		$sqlData = Mapper\Groupment::toSQL($groupment);

		$db = Local\Delivery::instance();
		$db->updateDelivery($sqlData, $groupment->id);

		if($groupment->deliveries){
			self::updateNameAndDailyPrice($groupment);
		}

		if($groupment->id) {
			$groupment = self::get($groupment->id);
			return true;
		} else return false;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public static function delete($id) {
		$db = Local\Delivery::instance();
		if($db->isDeliveryReducible($id)) {
			$db->deleteDelivery($id);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * @param Models\Groupment $groupment
	 * @return Models\Rights
	 */
	public static function getRights($groupment)
	{
		$request = new RequestAccess();
		$request->data = $groupment;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::SUBMODULE_GROUPMENT, $groupment);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		$right->addApi('entity', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}

	/**
	 * @param Models\Groupment $groupment
	 * @return bool
	 */
	public static function updateNameAndDailyPrice($groupment) {

		// TODO : utilise le delivery::update() pour déclencher l'envoie de la notification

		foreach ($groupment->deliveries as $gd) {

			$delivery = Deliveries::get($gd->delivery->id);

			$sqlData = [
				'MISSION' => [
					'ID_PARENT'       => $groupment->id,
					'MP_NBJRSFACTURE' => $gd->schedule,
					'MP_NOM'          => $groupment->title,
					'MP_TARIF'        => $groupment->averageDailyPriceExcludingTax
				]
			];

			// todo : notification
			Local\Delivery::instance()->updateDelivery($sqlData, $gd->delivery->id);

			if ($delivery->isMaster()) {
				$sqlData = [
					'MISSION' => [
						'MP_NBJRSFACTURE' => $sqlData['MISSION']['MP_NBJRSFACTURE'] + $delivery->numberOfDaysFree,
						'MP_NBJRSGRATUIT' => 0
					]
				];

				//todo notification
				Local\Delivery::instance()->updateDelivery($sqlData, $delivery->slave->id);
			}
		}
	}
}
