<?php
/**
 * deliveries.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Inactivities\Filters;
use BoondManager\APIs\Inactivities\Specifications\HaveReadAccess;
use BoondManager\APIs\Inactivities\Specifications\HaveWriteAccess;
use BoondManager\APIs\Inactivities\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use Wish\Tools;

/**
 * Class Deliveries
 * @package BoondManager\Models\Services
 */
class Inactivities{

	/**
	 * @param $id
	 * @return Models\Inactivity|null
	 */
	public static function get($id)
	{
		$db = Local\Delivery::instance();
		$data = $db->getDelivery($id);

		if(!$data) return null;

		$isInactivity = $data['PRJ_TYPE'] < 0;
		if(!$isInactivity) return null;

		$delivery = Mapper\Inactivity::fromSQL($data);

		self::attachAgenciesConfig($delivery);
		self::calculateOccupationRate($delivery);

		return $delivery;
	}

	/**
	 * @param Models\Inactivity $inactivity
	 * @return bool
	 */
	public static function attachAgenciesConfig(Models\Inactivity $inactivity) {
		//calendar, workUnitTypes, expensesTypes, name of resource's agency
		$inactivity->resource->agency = Agencies::get($inactivity->resource->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);

		//Retrieve all agencies on advantages, exceptional scales and expenses details
		$agenciesIDEmployees = [$inactivity->resource->agency->id];
		$agenciesIDActivities = [$inactivity->resource->agency->id];
		//Retrieve all companies on exceptional scales
		$companiesID = [];

		foreach($inactivity->expensesDetails as $ed) {
			$agenciesIDActivities[] = $ed->agency->id;
		}

		//All IDs need to be unique
		$agenciesIDEmployees = array_unique($agenciesIDEmployees);
		$agenciesIDActivities = array_unique($agenciesIDActivities);
		$companiesID = array_unique($companiesID);

		//exceptionalScaleTypes, advantageTypes ol all agencies
		$agenciesEmployees = [];
		foreach($agenciesIDEmployees as $aid){
			$agenciesEmployees[$aid] = Agencies::get($aid, Models\Agency::TAB_RESOURCES);
		}

		//calendar, workUnitTypes, expensesTypes, name of all agencies
		array_shift($agenciesIDActivities);
		$agenciesActivities = [
			$inactivity->resource->agency->id => $inactivity->resource->agency
		];
		foreach($agenciesIDActivities as $aid){
			$agenciesActivities[$aid] = Agencies::get($aid, Models\Agency::TAB_ACTIVITYEXPENSES);
		}

		//exceptionalScales, name of all companies
		$companies = [];
		foreach($companiesID as $cid){
			$companies[$cid] = Companies::get($cid, Models\Company::TAB_SETTING);
		}

		//Merge exceptionalScales, advantageTypes on resource's agency
		$inactivity->resource->agency->mergeWith( $agenciesEmployees[$inactivity->resource->agency->id] );

		//Merge calendar, workUnitTypes, expensesTypes, name on each expenses details agency
		foreach($inactivity->expensesDetails as $ed) {
			$ed->agency->mergeWith($agenciesActivities[$ed->agency->id]);
		}
		return true;
	}

	/**
	 * calculate financial data.
	 * It requires the project agency configuration (for the calendar)
	 * @param Models\Inactivity $inactivity
	 */
	public static function calculateOccupationRate(Models\Inactivity $inactivity) {
		$days = Tools::getNumberOfWorkingDays($inactivity->startDate, $inactivity->endDate, ($inactivity->resource->agency->calendar) ? $inactivity->resource->agency->calendar : CurrentUser::instance()->getCalendar());
		$inactivity->occupationRate = $days > 0 ? 100 * ( $inactivity->numberOfDaysInvoicedOrQuantity) / $days : 0;
	}

	/**
	 * Build project from filter
	 * @param Filters\Entity $filter
	 * @param Models\Inactivity $inactivity
	 * @return Models\Inactivity
	 */
	public static function buildFromFilter($filter, $inactivity = null) {
		if(!$inactivity) {
			$inactivity = new Models\Delivery();//filter and calculate all projects data
			$inactivity->mergeWith( $filter->toObject() );
		}else{
			$inactivity->mergeWith($filter->toObject());
		}

		$inactivity->calculateData();

		return $inactivity;
	}

	/**
	 * Create project
	 * @param Models\Inactivity $inactivity
	 * @return boolean
	 */
	public static function create(Models\Inactivity &$inactivity) {

		// creating a related project
		$inactivity->project = new Models\Project([
			'mode'               => Tools::reverseMapData($inactivity->inactivityType, Models\Inactivity::INACTIVITY_MAPPER),
			'currency'           => $inactivity->resource->currency,
			'currencyAgency'     => $inactivity->resource->currencyAgency,
			'exchangeRate'       => $inactivity->resource->exchangeRate,
			'exchangeRateAgency' => $inactivity->resource->exchangeRateAgency,
			'startDate'          => $inactivity->startDate,
			'endDate'            => $inactivity->endDate,
			'agency'             => $inactivity->resource->agency
		]);

		Projects::create($inactivity->project);

		//transform project to sqlData
		$sqlData = Mapper\Inactivity::toSQL($inactivity);

		$db = Local\Delivery::instance();
		$inactivity->id = $db->createDelivery($sqlData);

		if($inactivity->id) {
			$inactivity = self::get($inactivity->id);
			return true;
		} else return false;
	}

	/**
	 * @param Models\Inactivity $inactivity
	 * @return bool
	 */
	public static function update(&$inactivity) {
		// update project
		$inactivity->project->mode = Tools::reverseMapData($inactivity->inactivityType, Models\Inactivity::INACTIVITY_MAPPER);
		$inactivity->project->startDate = $inactivity->startDate;
		$inactivity->project->endDate = $inactivity->endDate;

		Projects::update($inactivity->project);

		//transform inactivity to sqlData
		$sqlData = Mapper\Inactivity::toSQL($inactivity);

		$db = Local\Delivery::instance();

		// reload inactivity
		if($db->updateDelivery($sqlData, $inactivity->id)) {
			Notification\Inactivity::update($inactivity);
			$inactivity = self::get($inactivity->id);
			return true;
		} else return false;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public static function delete($id) {
		$db = Local\Delivery::instance();
		if($db->isDeliveryReducible($id)) {
			$db->deleteDelivery($id);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * @param $delivery
	 * @return Models\Rights
	 */
	public static function getRights($delivery)
	{
		$request = new RequestAccess();
		$request->data = $delivery;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::SUBMODULE_INACTIVITIES, $delivery);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		$right->addApi('entity', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}
}
