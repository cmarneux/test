<?php
/**
 * invoices.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Invoices\Filters\SearchInvoices;
use BoondManager\APIs\Invoices\Specifications\HaveReadAccess;
use BoondManager\APIs\Invoices\Specifications\HaveWriteAccess;
use BoondManager\APIs\Invoices\Specifications\HaveWriteAccessOnField;
use BoondManager\APIs\Invoices\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Agency;
use BoondManager\Models\Delivery;
use BoondManager\Models\Invoice;
use BoondManager\Models\OrderCorrelation;
use BoondManager\Models;
use BoondManager\Models\WorkUnitType;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use Wish\Models\Model;

/**
 * Class Invoices
 * @package BoondManager\Models\Services
 */
class Invoices{

	/**
	 * @param int $id
	 * @return Invoice|false
	 */
	public static function find($id) {
		$filter = new SearchInvoices();
		$filter->keywords->setValue( Invoice::buildReference($id) );

		$sql = Local\Invoice::instance();
		$result = $sql->searchInvoices($filter);

		if($result->rows) return Mapper\Invoice::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * @param $id
	 * @param string $tab
	 * @return Invoice|false
	 */
	public static function get($id, $tab = BM::TAB_DEFAULT) {
		$db = Local\Invoice::instance();
		$data = $db->getFactureData($id, $tab);

		if(!$data) return false;

		$invoice = Mapper\Invoice::fromSQL($data, $tab);

		if($tab == Invoice::TAB_INFORMATION) {
			$invoice->order->project->agency->mergeWith( Agencies::get($invoice->order->project->agency->id, Agency::TAB_BILLING) );
			self::attachActivityDetailsToOrder($invoice);
		}

		return $invoice;
	}

	/**
	 * @param string $id
	 * @param string $lastName
	 * @param string $firstName
	 * @return Model
	 */
	private static function buildActivityEntry($id, $lastName, $firstName) {
		return new Model([
			'id' => strval($id),
			'lastName' => $lastName,
			'firstName' => $firstName,
			'regularTimes' => [],
			'exceptionalTimes' => [],
			'exceptionalCalendars' => [],
			'timesReports' => [],
			'expensesReports' => []
		]);
	}

	/**
	 * Attach more data
	 * @param Invoice $entity
	 * @return Invoice
	 */
	private static function attachActivityDetailsToOrder(Invoice $entity) {
		if(!in_array($entity->order->project->mode, [BM::PROJECT_TYPE_TA, BM::PROJECT_TYPE_PACKAGE])) return false;

		if(!$entity->creditNote) {
			$correls = [];
			foreach($entity->order->deliveriesPurchases as $dp) {
				$correls[] = [
					'CORBDC_TYPE' => ($dp instanceof Delivery) ? OrderCorrelation::DBTYPE_DELIVERY : OrderCorrelation::DBTYPE_PURCHASE,
					'ID_ITEM' => $dp->id
				];
			}
			$timesDetails = self::getIntervenantsDataOnPeriod($entity->order->project->id, $entity->startDate, $entity->endDate, $correls, true);
			$expensesDetails = self::getFraisDataOnPeriod($entity->order->project->id, $entity->startDate, $entity->endDate, $correls, false);

			//On construit le tableau de détail pour les achats de prestation
			$tabDetails = array();
			foreach ($timesDetails['LISTE_TEMPSNORMAUX'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = self::buildActivityEntry($mission['ID_PROFIL'], $mission['PROFIL_NOM'], $mission['PROFIL_PRENOM']);

				// grouped with following fields
				$key = implode('_', [$mission['LIGNETEMPS_TYPEHREF'], $mission['ID_SOCIETE'], $mission['MP_TARIF']]);

				if(!isset($tabDetails[$mission['ID_PROFIL']]['regularTimes'][$key])) {
					$tabDetails[$mission['ID_PROFIL']]['regularTimes'][$key] = $time = new Model([
						'workUnitType' => new WorkUnitType([
							'reference' => $mission['LIGNETEMPS_TYPEHREF'],
							'activityType' => $mission['TYPEH_TYPEACTIVITE'],
							'name' => $mission['TYPEH_NAME']
						]),
						'agency' => Agencies::getBasic($mission['ID_SOCIETE']),
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' => $mission['MP_TARIF']
					]);
				} else {
					$time = $tabDetails[$mission['ID_PROFIL']]['regularTimes'][$key];
				}

				$time['numberOfWorkingDays'] += $mission['NB_JRSOUVRES'];
			}

			if(isset($tabDetails[$mission['ID_PROFIL']]))
				$tabDetails[$mission['ID_PROFIL']]['regularTimes'] = array_values($tabDetails[$mission['ID_PROFIL']]['regularTimes']);

			foreach ($timesDetails['LISTE_TEMPSEXCEPTION'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = self::buildActivityEntry($mission['ID_PROFIL'], $mission['PROFIL_NOM'], $mission['PROFIL_PRENOM']);

				// grouped with following fields
				$key = implode('_', [$mission['TEMPSEXP_TYPEHREF'], $mission['ID_SOCIETE'], $mission['MP_TARIF']]);

				if(!isset($tabDetails[$mission['ID_PROFIL']]['exceptionalTimes'][$key])) {
					$tabDetails[$mission['ID_PROFIL']]['exceptionalTimes'][$key] = $time = new Model([
						'workUnitType' => new WorkUnitType([
							'reference' => $mission['LIGNETEMPS_TYPEHREF'],
							'activityType' => $mission['TYPEH_TYPEACTIVITE'],
							'name' => $mission['TYPEH_NAME']
						]),
						'agency' => Agencies::getBasic($mission['ID_SOCIETE']),
						'priceExcludingTax' =>  $mission['MP_TARIF'],
						'duration' => 0,
					]);
				} else {
					$time = $tabDetails[$mission['ID_PROFIL']]['exceptionalTimes'][$key];
				}

				$time['duration'] += $mission['TEMPS_DUREE'];
			}

			if(isset($tabDetails[$mission['ID_PROFIL']]))
				$tabDetails[$mission['ID_PROFIL']]['exceptionalTimes'] = array_values($tabDetails[$mission['ID_PROFIL']]['exceptionalTimes']);

			/*
			foreach ($expensesDetails['LISTE_FRAISREEL'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = self::buildActivityEntry($mission['ID_PROFIL'], $mission['PROFIL_NOM'], $mission['PROFIL_PRENOM']);


				$expense = null;
				foreach($tabDetails[$mission['ID_PROFIL']]['exceptionalTimes'] as $rt) {
					if($rt->workUnitType->reference == $mission['LIGNETEMPS_TYPEHREF']) {
						$expense = $rt;
						break;
					}
				}
				if(!$expense) {
					$tabDetails[$mission['ID_PROFIL']]['exceptionalTimes'][] = $expense = new Model([
						'workUnitType' => new WorkUnitType([
							'reference' => $mission['LIGNETEMPS_TYPEHREF'],
							'activityType' => $mission['TYPEH_TYPEACTIVITE'],
							'name' => $mission['TYPEH_NAME']
						]),
						'agency' => Agencies::getBasic($mission['ID_SOCIETE']),
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' => $mission['MP_CJM']
					]);
				}

				$time['costsProduction'] += $mission['MP_CJM'];

					$tabDetails[$mission['ID_PROFIL']] = [
						'lastName' => $mission['PROFIL_NOM'],
						'firstName' =>  $mission['PROFIL_PRENOM'],
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' =>  $mission['MP_CJM'],
					];

				if ($mission['FRAISREEL_TYPEFRSREF'] == 0)
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAISREEL_MONTANT'] * $mission['LISTEFRAIS_BKMVALUE'];
				else
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAISREEL_MONTANT'];
			}

			foreach ($expensesDetails['LISTE_FRAISFORFAIT'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = [
						'lastName' => $mission['PROFIL_NOM'],
						'firstName' =>  $mission['PROFIL_PRENOM'],
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' =>  $mission['MP_CJM'],
					];

				if ($mission['LIGNEFRAIS_TYPEFRSREF'] == 0)
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAIS_VALUE'] * $mission['LISTEFRAIS_BKMVALUE'];
				else
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAIS_VALUE'];
			}
			*/

			//On construit le lien vers les PDF projets
			$tpsBDD = Local\TimesReport::instance();
			$frsBDD = Local\ExpensesReport::instance();

			$nbMensualites = Invoices::getNbMensualite($entity->startDate, $entity->endDate);
			$dateMisMensualites = explode('-', $entity->startDate);
			foreach ($tabDetails as $id_ressource => $detail) {
				$tabDetails[$id_ressource]['numberOfWorkingDays'] = round($detail['numberOfWorkingDays'], 2);
				$tabDetails[$id_ressource]['costsProduction'] = floatval($detail['costsProduction']);
				$tabDetails[$id_ressource]['averageDailyCost'] = floatval($detail['averageDailyCost']);

				$timesReports = array();
				$expensesReports = array();
				for ($j = 1; $j <= $nbMensualites; $j++) {
					$dateDocument = Invoices::getNewDateMens($dateMisMensualites[0] . '-' . $dateMisMensualites[1] . '-01', $j);
					if ($timesReportId = $tpsBDD->isTempsExist($dateDocument, $id_ressource)) $timesReports[] = ['id' => $timesReportId, 'term' => (new \DateTime($dateDocument))->format('Y-m')];
					if ($expensesReportId = $frsBDD->isFraisExist($dateDocument, $id_ressource)) $expensesReports[] = ['id' => $expensesReportId, 'term' => (new \DateTime($dateDocument))->format('Y-m')];
				}
				$tabDetails[$id_ressource]['timesReports'] = $timesReports;
				$tabDetails[$id_ressource]['expensesReports'] = $expensesReports;
			}
			$entity->activityDetails = array_values(array_map(function ($resource){
				$resource['timesReports']         = array_values($resource['timesReports']);
				$resource['expensesReports']      = array_values($resource['expensesReports']);
				$resource['regularTimes']         = array_values($resource['regularTimes']);
				$resource['exceptionalTimes']     = array_values($resource['exceptionalTimes']);
				$resource['exceptionalCalendars'] = array_values($resource['exceptionalCalendars']);
				return [
					'resource' => $resource
				];
			},$tabDetails));
		}
	}

	/**
	 * Search projects
	 * @param SearchInvoices $filter
	 * @return \BoondManager\Models\SearchResults\Invoices
	 */
	public static function search(SearchInvoices $filter)
	{
		$db = Local\Invoice::instance();
		$searchResult = $db->searchInvoices($filter);

		return Mapper\Invoice::fromSearchResult($searchResult);
	}

	/**
	 * \brief Récupère la liste des intervenants ayant travaillés sur la période comprenant la date passée en paramètres
	 * @param int $projectId identifiant du projet
	 * @param string $startDate date de début de la période
	 * @param string $endDate date de fin de la période
	 * @param array $correlations tableau contenant la liste des commandes/missions corrélés afin de ne traiter que celles-là
	 * @param boolean $withException
	 * @return array tableau des intervenants de la facture
	 */
	public static function getIntervenantsDataOnPeriod($projectId, $startDate, $endDate, $correlations = array(), $withException = false)
	{
		$db = Local\Invoice::instance();
		return $db->getIntervenantsDataOnPeriod($projectId, $startDate, $endDate, $correlations, $withException);
	}

	/**
	 * \brief Récupère la liste des frais à refacturer au client
	 * @param int $projectId identifiant du projet
	 * @param string $startDate date de début de la période
	 * @param string $endDate date de fin de la période
	 * @param array $correlations tableau contenant la liste des commandes/missions corrélés afin de ne traiter que celles-là
	 * @param boolean $rebilledOnly
	 * @return array tableau des frais à refacturer
	 */
	public static function getFraisDataOnPeriod($projectId, $startDate, $endDate, $correlations = array(), $rebilledOnly = true)
	{
		$db = Local\Invoice::instance();
		return $db->getFraisDataOnPeriod($projectId, $startDate, $endDate, $correlations, $rebilledOnly);
	}

	/**
	 * \brief Construit la référence d'une commande
	 * @param <type> $idclient identifiant du membre
	 * @param <type> $groupe société du membre
	 * @param <type> $date date de la commande
	 * @return <type> renvoit la référence de la commande
	 */
	public static function setReferenceCmd($idclient, $groupe, $date, $increment=0)
	{
		$datetmp = explode('-', $date);
		$ref_date = substr($datetmp[0], 2, 2).$datetmp[2].$datetmp[1];
		$ref_groupe = strtoupper(substr(urlencode($groupe),0,3));

		if($increment > 0)
			$increment = '_'.$increment;
		else
			$increment = '';

		return 'BOONDMANAGER'.$idclient.$ref_groupe.$ref_date.$increment;
	}

	/**
	 * \brief Renvoit la nouvelle date en fonction d'une date de départ Y-m-d et du nombre de mensualité
	 * @param <type> $date date de départ
	 * @param <type> $nb_mensualite nombre de mensualité
	 * @return <type>  renvoit la nouvelle date
	 */
	public static function getNewDateMens($date, $nb_mensualite)
	{
		$date_array = explode('-', $date);
		return date('Y-m-d', mktime(0,0,0, $date_array[1]+$nb_mensualite-1, $date_array[2], $date_array[0]));
	}

	/**
	 * \brief Renvoit le nombre de mensualités entre 2 dates Y-m-d
	 * @param <type> $date_debut date de début
	 * @param <type> $date_fin date de fin
	 * @return <type> renvoit le nombre de mensualités
	 */
	public static function getNbMensualite($date_debut, $date_fin)
	{
		$nDebut = explode('-', $date_debut);
		$nFin = explode('-', $date_fin);
		return $nFin[1] - $nDebut[1] + 1 + ($nFin[0] - $nDebut[0])*12;
	}

	/**
	 * \brief Renvoit la date d'échéance d'une facture en fonction du nombre de mensualités après la date de départ
	 * @param <type> $date_depart date de départ
	 * @param <type> $mensualite nombre de mensualités
	 * @return <type> renvoit la date d'échéance
	 */
	public static function getEcheanceDate($date_depart, $mensualite)
	{
		$date = explode('-', $date_depart);
		$month = date('m', mktime(0,0,0,$date[1]+$mensualite-1,1,$date[0]));
		$year = date('Y', mktime(0,0,0,$date[1]+$mensualite-1,1,$date[0]));

		$month = self::getMonthLabel($month);
		if($month != '')
			return $month.' '.$year;
		else
			return '';
	}

	/**
	 * \brief Renvoit le mois correspondant au numéro demandée dans la langue de ce module
	 * @param <type> $num_month numéro du mois de l'année
	 */
	public static function getMonthLabel($num_month, $language = null)
	{
		if(!$language) $language = BM::getLanguage();
		if($num_month > 0 && $num_month <= 12) {
			switch($language) {
				default:
					switch($num_month) {
						case 1:return 'Janvier';break;case 2:return 'Février';break;case 3:return 'Mars';break;case 4:return 'Avril';break;
						case 5:return 'Mai';break;case 6:return 'Juin';break;case 7:return 'Juillet';break;case 8:return 'Août';break;
						case 9:return 'Septembre';break;case 10:return 'Octobre';break;case 11:return 'Novembre';break;case 12:return 'Décembre';break;
					}
					break;
				case 'en':
					switch($num_month) {
						case 1:return 'January';break;case 2:return 'February';break;case 3:return 'March';break;case 4:return 'April';break;
						case 5:return 'May';break;case 6:return 'June';break;case 7:return 'July';break;case 8:return 'August';break;
						case 9:return 'September';break;case 10:return 'October';break;case 11:return 'November';break;case 12:return 'December';break;
					}
					break;
			}
		}
		return '';
	}

	/**
	 *\brief Construction de la référence de la facture
	 * @param <type> $type $type d'abonnement
	 * @return <type> renvoit la référence de la facture
	 */
	public static function getReferenceFacture($idfacture)
	{
		$id = (16600000+$idfacture);
		return 'WBDM'.$id;
	}

	/**
	 * @param Invoice[]|Invoice $invoices
	 */
	public static function attachRights($invoices)
	{
		if(!is_array($invoices)) $invoices = [$invoices];

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();

		foreach($invoices as $invoice) {
			$request->setData($invoice);
			$invoice->canReadInvoice = $readSpec->isSatisfiedBy($request);
			$invoice->canWriteInvoice = $writeSpec->isSatisfiedBy($request);
		}
	}

	public static function getRights($order) {
		$request = new RequestAccess();
		$request->data = $order;
		$request->user = CurrentUser::instance();

		$right = new Models\Rights(CurrentUser::instance(), BM::SUBMODULE_DELIVERY, $order);

		foreach(IsActionAllowed::ALLOWED_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach (Models\Order::getAllTabs() as $tab) {
			$readSpec = new HaveReadAccess($tab);
			$writeSpec = new HaveWriteAccess($tab);
			$right->addApi($tab, $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		foreach(HaveWriteAccessOnField::FIELDS as $field) {
			$spec = new HaveWriteAccessOnField($field);
			$right->addField($field, $spec->isSatisfiedBy($request), $spec->isSatisfiedBy($request));
		}

		return $right;
	}

	/**
	 *\brief Renvoit les conditions de règlements (Intitulé, X et Y)
	 */
	public function getConditionReglementItem($value, $item, $condArray = array()) {
		$X = $Y = '';

		//On récupère les données X et Y
		$tabCond = explode(' ', $item);
		foreach($tabCond as $item) {if( is_numeric($item) ) {if($X == 0) $X = $item; else $Y = $item;}}
		return array($value % 10, $X, $Y);
	}

	/**
	 * \brief Retourne la date de réglement en fonction de la date de facture et des conditions de réglement
	 * @param <type> $datefacture date de la facture
	 * @param <type> $condition condition de reglement
	 * @param <type> $arrayCond tableau des conditions de reglement
	 */
	public static function getDateReglement($datefacture, $condition, $arrayCond = array())
	{
		//0 = comptant
		//1 = X jours nets
		//2 = X jours fin de mois
		//3 = X jours fin de mois le Y

		//Si le client souhaite créer plus de critères, alors on multiplie par 10 chacune des sous-catégorie
		$typecond = 0;
		$X = 0;
		$Y = 0;
		foreach($arrayCond as $cond) {
			if($cond->value == $condition) {
				$typecond = $cond->value;
				//On récupère les données X et Y
				$tabCond = explode(' ', $cond->item);
				foreach($tabCond as $item) {
					if( is_numeric($item) ) {
						if($X == 0) $X = $item; else $Y = $item;
					}
				}
				break;
			}
		}

		$datereglement = $datefacture;
		$tabDate = explode('-', $datefacture);
		switch($typecond % 10) {
			case 0://Comptant
				break;
			case 1://net
				$datereglement = date('Y-m-d', mktime(0,0,0,$tabDate[1], $tabDate[2]+$X, $tabDate[0]));
				break;
			case 2://fin de mois
				$finMois = date('Y-m-d', mktime(0,0,0,$tabDate[1], $tabDate[2]+$X, $tabDate[0]));
				$finMois = explode('-', $finMois);
				$datereglement = date('Y-m-d', mktime(0,0,0,$finMois[1]+1, 0, $finMois[0]));
				break;
			case 3://fin de mois le
				$finMois = date('Y-m-d', mktime(0,0,0,$tabDate[1]+1, 0, $tabDate[0]));
				$finMois = explode('-', $finMois);
				$datereglement = date('Y-m-d', mktime(0,0,0,$tabDate[1], $finMois[2]+$X+$Y, $tabDate[0]));
				break;
		}
		return $datereglement;
	}

	/**
	 * \brief Retourne la date de réglement en fonction de la date de facture et des conditions de réglement
	 * @param <type> $datefacture date de la facture
	 * @param <type> $condition condition de reglement
	 * @param <type> $arrayCond tableau des conditions de reglement
	 */
	public static function getMontantAbonnement($montant, $quantite, $debut, $fin, $type)
	{
		$total = $montant*$quantite;
		if($type != 0) //L'abonnement n'est pas unitaire
			$total *= self::getPeriodeAbonnement($debut, $fin, $type);
		return $total;
	}

	/**
	 * \brief Retourne la date de réglement en fonction de la date de facture et des conditions de réglement
	 * @param <type> $datefacture date de la facture
	 * @param <type> $condition condition de reglement
	 * @param <type> $arrayCond tableau des conditions de reglement
	 */
	public static function getPeriodeAbonnement($debut, $fin, $type)
	{
		//On calcule le nombre de mensualités entre les 2 dates
		$nbMens = self::getNbMensualite($debut, $fin);
		switch($type) {
			case 1://Mensuel
				return $nbMens;
				break;
			case 2://Trimestriel
				return ceil($nbMens/3);
				break;
			case 3://Semestriel
				return ceil($nbMens/6);
				break;
			case 4://Annuel
				return ceil($nbMens/12);
				break;
		}
		return 0;
	}

	/**
	 * \brief Retourne le nombre de mensualités selon le type
	 * @param <type> nombre entier
	 */
	public static function getNbMensualiteAbonnement($type)
	{
		switch($type) {
			case BM::SUBSCRIPTION_TYPE_QUATERLY:
				return 3;
				break;
			case BM::SUBSCRIPTION_TYPE_SEMIANNUAL:
				return 6;
				break;
			case BM::SUBSCRIPTION_TYPE_ANNUAL:
				return 12;
				break;
			default:
				return 1;
				break;
		}
	}
}
