<?php
/**
 * login.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Services;

use Wish\Email;
use Wish\JWT;
use Wish;
use Wish\Models\Model;
use Wish\Tools;

use Wish\Cache;

use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;
use BoondManager\Databases\Mapper;
use BoondManager\Models;

/**
 * Class Login
 * @package BoondManager\Models\Services
 */
class Login{

	/**
	 * Connect a user and save its data in session `SESSION.user`     *
	 * @param $hash
	 * @return Models\User|false
	 * The object contains the following fields:
	 * - `USER_SECURITYALERT`,
	 * - `USER_SECURITYCOOKIE`
	 * - `USER_LOGIN`
	 * - `PROFIL_EMAIL`
	 * - `ENCRYPTED_PASSWORD`
	 * - `CLIENT_WEB`
	 */
	public static function connectionFromHash($hash){
		$hash = Tools::signedRequest_decode($hash, 'BoondManagerRememberMe');
		if(sizeof($hash) <= 4) return false;

		$company = $hash[0];
		$login = $hash[1];
		$pwdEncrypt = $hash[2];

		\Base::instance()->set('REQUEST.company', $company); // pas tip top de remettre ca dans la request
		\Base::instance()->set('REQUEST.login', $login);

		//Load user's configuration file
		$configFile = BM::loadCustomerInterfaceFromLogin($login, $company);

		$userConnexion = Login::connectionFromLoginPassword($login, $pwdEncrypt, $configFile);

		// la verification doit se faire apres le login pour pouvoir utiliser le CurrentUser
		$user = CurrentUser::instance();
		if($hash[3] != Login::generateSecurityHash(BM::getCustomerCode(), $user->getAccount()->login, $user->getUserId(), $user->lastConnection)) {
			self::deleteLoginData(true);//Log out completely the user
			return false;
		}else{
			return $userConnexion;
		}
	}

	/**
	 * Connect a user and save its data in session `SESSION.user`
	 *
	 * @param  string  $login  User Login
	 * @param  string  $pwd  User Password
	 * @param  string $clientWeb client web (aka company)
	 * @param  boolean  $trustedThirdParty  connection type, cf. [TAB_USER.USER_CONNECTIONTYPE](../../bddclient/classes/TAB_USER.html#property_USER_CONNECTIONTYPE).
	 * @return Models\Account|false
	 */
	public static function connectionFromLoginPassword($login, $pwd, $clientWeb, $trustedThirdParty = false){
		//TODO : Il faut gérer la fonction saveConnectionInformations de la V6 afin de loguer toutes les connexions en BDD

		$ctxID = Tools::getContext()->startTiming('user authentification');
		$f3 = \Base::instance();

		if($account = Accounts::getFromLoginPwd($login, $pwd)) {
			if(!$trustedThirdParty || !$account->connexionType) {
				//Soit l'internaute n'est pas une ressource, Soit c'est une ressource et donc l'intranet de la société doit être activé
				if(!$account->isEmployee() || BM::isCustomerInterfaceActive() && Accounts::getIntranetAccess($account->id)) {//La connexion a réussie
					//On nettoie les données de session
					self::deleteLoginData();

					if(!$f3->exists('SESSION.landingPage') && $account->homePage != '') {//On redirige vers la page d'accueil configuré pour cet utilisateur
						$tabHomePage = explode(' ', $account->homePage);
						$f3->set('SESSION.landingPage', (sizeof($tabHomePage) == 2) ? $tabHomePage[1] : $tabHomePage[0]);
					}

					//On mémorise la date connexion
					Accounts::updateLastConnexionDate($account, date('Y-m-d H:i:s'));

					// enable a security check based on the navigator used for login
					CurrentUser::instance()->enableNavigatorCheck();

					BM::setLanguage($account->language);

					// add the encrypted password for the device part
					$account->password = $pwd;
					$account->client   = $clientWeb;

					//On mémorise en session les données de l'internaute
					CurrentUser::instance()->setAccount($account);

					Tools::getContext()->endTiming($ctxID);
					return $account;
				}
			}
		}
		self::deleteLoginData(true);//Log out completely the user
		Tools::getContext()->endTiming($ctxID);
		return false;
	}

	/**
	 * Connect a user and save its data in session `SESSION.user`
	 *
	 * @param  string  $xJwtToken  X-Jwt-App-BoondManager's Header
	 * @param  string  $appToken  App's Token
	 * @param  string  $userId  User id
	 * @param  string $clientWeb client web (aka company)
	 * @return Models\User|false
	 * The object contains the following fields:
	 * - `USER_SECURITYALERT`,
	 * - `USER_SECURITYCOOKIE`
	 * - `USER_LOGIN`
	 * - `PROFIL_EMAIL`
	 * - `ENCRYPTED_PASSWORD`
	 * - `CLIENT_WEB`
	 */
	public static function connectionFromAppToken($xJwtToken, $appToken, $userId, $clientWeb){
		//TODO : Il faut gérer la fonction saveConnectionInformations de la V6 afin de loguer toutes les connexions en BDD

		$ctxID = Tools::getContext()->startTiming('app authentification');

		if(($localApp = Apps::getFromToken($appToken)) &&
			($boondmanagerApp = Apps::get($localApp->id)) &&
			JWT::decode($xJwtToken, $boondmanagerApp->key, ['HS256']) &&
			(sizeof($boondmanagerApp->hostsAllowed) == 1 && $boondmanagerApp->hostsAllowed[0] == '*' || in_array($_SERVER['REMOTE_ADDR'], $boondmanagerApp->hostsAllowed))) {
			if(($user = Accounts::getLoginAndPwd($userId)) &&
				($userConnexion = Login::connectionFromLoginPassword($user->login, $user->password, $clientWeb))) {
				\Base::instance()->set('SESSION.app.id', $boondmanagerApp->id);
				\Base::instance()->set('SESSION.app.token', $boondmanagerApp->token);
				\Base::instance()->set('SESSION.app.apiAllowed', $boondmanagerApp->apiAllowed);
				return $userConnexion;
			}
		}
		self::deleteLoginData(true);//Log out completely the user
		Tools::getContext()->endTiming($ctxID);
		return false;
	}

	/**
	 * Connect a user and save its data in session `SESSION.user`
	 *
	 * @param  string  $xJwtToken  X-Jwt-Client-BoondManager's Header
	 * @param  string  $customerCode  Customer's Code
	 * @param  string  $userId  User id
	 * @param  string $clientWeb client web (aka company)
	 * @return Models\User|false
	 * The object contains the following fields:
	 * - `USER_SECURITYALERT`,
	 * - `USER_SECURITYCOOKIE`
	 * - `USER_LOGIN`
	 * - `PROFIL_EMAIL`
	 * - `ENCRYPTED_PASSWORD`
	 * - `CLIENT_WEB`
	 */
	public static function connectionFromClientToken($xJwtToken, $customerCode, $userId, $clientWeb){
		//TODO : Il faut gérer la fonction saveConnectionInformations de la V6 afin de loguer toutes les connexions en BDD

		$ctxID = Tools::getContext()->startTiming('client authentification');

		if(($agency = Agencies::getAgencyWithConfigFromCustomerCode($customerCode)) &&
			JWT::decode($xJwtToken, $agency->key, ['HS256'])) {
			if(($user = Accounts::getLoginAndPwd($userId)) &&
				($userConnexion = Login::connectionFromLoginPassword($user->login, $user->password, $clientWeb))) {
				return $userConnexion;
			}
		}
		self::deleteLoginData(true);//Log out completely the user
		Tools::getContext()->endTiming($ctxID);
		return false;
	}

	/**
	 * get an email from the userConnexion
	 * @param Models\User $userConnexion
	 * @return string
	 */
	public static function getValidEmailFromUserConnection($userConnexion){
		$email = $userMail = filter_var($userConnexion->login, FILTER_VALIDATE_EMAIL)
			? $userConnexion->login
			: ($userConnexion->resource && filter_var($userConnexion->resource->email1, FILTER_VALIDATE_EMAIL)
				?$userConnexion->resource->email1
				:''
			  );
		return $email;
	}

	/**
	 * Delete user's session data.
	 *
	 * @param boolean $all Deletion's type :
	 * - If `true` then delete all session's data (Example : when log out)
	 * - If `false` then not delete user's landing page & BDD
	 * @param boolean $backupJWT Saving's type of JWT token (Example : log in to Zendesk) :
	 * - If `true` then save the token
	 * - If `false` then not save the token
	 */
	public static function deleteLoginData($all = false, $backupJWT = true) {
		$contextID = Tools::getContext()->startTiming('deleteLoginData');
		$f3 = \Base::instance();
		$landingPage = (!$all && $f3->exists('SESSION.landingPage'))?$f3->get('SESSION.landingPage'):'';//Is there a specific landing page ?
		$tabClient = (!$all && $f3->exists('SESSION.CLIENT'))?$f3->get('SESSION.CLIENT'):array();//Is a user from a specific BDD ?
		$language = BM::getLanguage();
		if($backupJWT) $jwt = $f3->get('SESSION.jwt');

		//Clean session's data
		Cache::instance()->reset();
		CurrentUser::instance()->destroy();
		$f3->clear('SESSION');

		BM::setLanguage($language); //Backup user's language
		if($backupJWT && $jwt != '') $f3->set('SESSION.jwt', $jwt);//Backup JWT's redirection

		if(sizeof($tabClient) > 0) $f3->set('SESSION.CLIENT', $tabClient);
		if($landingPage != '') $f3->set('SESSION.landingPage', $landingPage);
		Tools::getContext()->endTiming($contextID);
	}

	/**
	 * Build session data from current user
	 *
	 * Always called after a successful connection
	 */
	public static function buildComplementaryUserData() {
		$ctx = Tools::getContext()->startTiming('buildComplementaryUserData');

		$user = CurrentUser::instance();
		if($user->isLogged()) {
			if(BM::isCustomerInterfaceActive()) { //On est sur l'interface cliente
				$userBDD = Local\Account::instance();
				$userConf = null;
				switch($user->getAccount()->level) {
					case BM::USER_TYPE_ADMINISTRATOR://Les administrateurs n'ont pas de profil
						//On récupère la configuration du compte root qui détermine ses droits pour configurer les comptes de sa société
						if($data = $userBDD->getAccount( $user->getUserId()) ) {
							$account = Mapper\Account::fromSQL($data);

							CurrentUser::instance()->getAccount()->mergeWith($account);

							//On récupère toutes les sociétés du groupe
							if($conf = Agencies::getFirstAgencyConfig()) {
								CurrentUser::instance()->setAgency($conf);

								Tools::getContext()->endTiming($ctx);
								return true;
							}
						}
						break;
					case BM::USER_TYPE_MANAGER:
					case BM::USER_TYPE_RESOURCE://Manager & Ressource
						//$user = Users::get($user->id);
						if($data = $userBDD->getAccount( $user->getUserId()) ) {
							$account = Mapper\Account::fromSQL($data);

							CurrentUser::instance()->getAccount()->mergeWith($account);

							if($agency = Agencies::getAgencyWithConfig($data['ID_SOCIETE'])) {

								CurrentUser::instance()->setAgency( $agency );

								Tools::getContext()->endTiming($ctx);
								return true;
							}
						}
						break;
				}
			} else {//On est sur l'interface d'administration de BoondManager
				CurrentUser::instance()->setBoondManagerAdmin(true);
				$account = Accounts::get($user->getUserId());
				if($account) {
					CurrentUser::instance()->getAccount()->mergeWith($account);

					Tools::getContext()->endTiming($ctx);
					return true;
				}
			}
		}
		Tools::getContext()->endTiming($ctx);
		return false;
	}

	/**
	 * Generate a hash based on currentuser's data
	 *
	 * @param string $company company name
	 * @param string $login user login
	 * @param int $id user id
	 * @param string $lastConnection user last connection
	 * @return string
	 */
	public static function generateSecurityHash($company, $login, $id, $lastConnection){
		return sha1(implode('*', [$company, $login, $id, $lastConnection]));
	}

	/**
	 * @param CurrentUser $user
	 * @param string $encryptedPwd the password encrypted
	 * @return string
	 */
	public static function generateHash(CurrentUser $user, $encryptedPwd){
		return Tools::signedRequest_encode(array(
			BM::getCustomerCode(),
			$user->getAccount()->login,
			$encryptedPwd,
			self::generateSecurityHash(BM::getCustomerCode(), $user->getAccount()->login, $user->getUserId(), $user->lastConnection)
		), 'BoondManagerRememberMe');
	}

	/**
	 * hash a given password
	 * @param string $password
	 * @param string $login
	 * @return string
	 */
	public static function saltPassword($password, $login)
	{
		return md5($login . '*' . $password);
	}

	/**
	 * Prepare the zenDesk auth
	 * @param string $returnUrl
	 */
	public static function prepareLoginZenDesk($returnUrl = ''){
		\Base::instance()->set('SESSION.zendesk.jwt', true);
		\Base::instance()->set('SESSION.zendesk.returnURL', $returnUrl);
	}

	/**
	 * build an url to return on zenDesk
	 * @return string
	 */
	public static function getZenDeskURL(){
		$user = CurrentUser::instance();

		$now = time();
		$token = array(
			"jti" => md5($now . rand()),
			"iat" => $now,
			"name" => BM::isModeDemo()?'Compte Demo':$user->getFullName(), //TODO ? faire le test dans currentUser ?
			"email" => BM::isModeDemo()?'contact@boondmanager.com':$user->email, //TODO ? faire le test dans currentUser ? (et utiliser ->getEmail())
			"external_id" => BM::isModeDemo()?'boondmanager_demo|100': $user->getZendeskID() //TODO ? faire le test dans currentUser ?
		);

		$tokenJWT = \Base::instance()->get('ZENDESK.TOKEN');
		$location = 'https://wish.zendesk.com/access/jwt?jwt=' . JWT::encode($token, $tokenJWT);

		if($url = \Base::instance()->get('SESSION.zendesk.returnURL'))
			$location .= "&return_to=".$url;

		\Base::instance()->clear('SESSION.zendesk');

		return $location;
	}

	/**
	 * is the user is logged on zenDesk
	 * @return bool
	 */
	public static function isLoggedToZenDesk(){
		return CurrentUser::instance()->isLogged() && \Base::instance()->get('SESSION.zendesk.jwt');
	}

	/**
	 * get the landing page
	 * @return string
	 */
	public static function getLandingPage(){
		if(Login::isLoggedToZenDesk()) return Login::getZenDeskURL();
		else return \Base::instance()->exists('SESSION.landingPage') ? \Base::instance()->get('SESSION.landingPage') : 'dashboard';
	}

	/**
	 * @deprecated
	 * @return array
	 */
	public static function getUserData()
	{
		$user = CurrentUser::instance();
		if($user->isLogged()) {
			$module =  $user->get('module');
			$module['myApps'] = [];
			if(isset($module['mesapps']) && is_array($module['mesapps'])) {
				foreach ($module['mesapps'] as $key => $array) {
					$module['myApps'][] = [
						'id'          => $array['ID_MAINAPI'],
						'name'        => $array['MAINAPI_NOM'],
						'description' => $array['MAINAPI_DESCRIPTION']
					];
				}
				unset($module['mesapps']);
			}

			$resource = $user->get('profil');
			$resource['agency'] = [
				'id' => $user->get('company.id'),
				'name' => $user->get('company.name'),
				'group' => $user->get('company.group'),
				'url' => $user->get('company.web'),
				'logo' => $user->get('company.logo')
			];
			$data = [
				'id' => $user->getAccount()->id,
				'login' => $user->getAccount()->login,
				'type' => $user->getAccount()->level,
				'resource' => $resource,
				'module' => $module,
				//'navigationBar' => [] // TODO
				//'old' => $user->getAllData()
			];
			return $data;
		}

		return [];
	}

	/**
	 * Connexion with Basic Auth
	 * @return boolean `true` on successfull connexion
	 */
	public static function connexionWithBasicAuth(){
		if(\Base::instance()->get('SERVER.PHP_AUTH_USER') && \Base::instance()->get('SERVER.PHP_AUTH_PW')) {

			// retrieving login/password
			$login    = \Base::instance()->get('SERVER.PHP_AUTH_USER');
			$password = \Base::instance()->get('SERVER.PHP_AUTH_PW');

			// perform connection
			$configFile = BM::loadCustomerInterfaceFromLogin($login);
			$pwdEncrypt = self::saltPassword($password, $login);

			if (self::connectionFromLoginPassword($login, $pwdEncrypt, $configFile)) {
				self::buildComplementaryUserData();
				BM::setCurrentAPI( BM::API_TYPE_BASICAUTH );
				return true;
			}

			// otherwise , clean up
			self::deleteLoginData(true);
		}
		return false;
	}

	/**
	 * Connexion with X-Jwt-App-BoondManager Header
	 * @return boolean `true` on successfull connexion
	 */
	public static function connexionWithXJWTHeader(){
		$headers = \Base::instance()->get('HEADERS');
		$xJwtToken = null;
		$xJwtType = null;
		if(isset($headers['X-Jwt-App-Boondmanager'])) {
			$xJwtToken = $headers['X-Jwt-App-Boondmanager'];
			$xJwtType = 'App';
		} else if(isset($headers['X-Jwt-Client-Boondmanager'])) {
			$xJwtToken = $headers['X-Jwt-Client-Boondmanager'];
			$xJwtType = 'Client';
		}

		if(!is_null($xJwtToken) && !is_null($xJwtType)) {
			$tks = explode('.', $xJwtToken);
			if (count($tks) == 3) {
				list($headb64, $bodyb64, $cryptob64) = $tks;
				if (null !== $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64))) {
					$tabUser = Tools::objectID_BM_decode($payload->userToken);
					if($xJwtType == 'App') {
						$tabToken = Tools::objectID_BM_decode($payload->appToken);
						if (sizeof($tabToken) == 2 && sizeof($tabUser) == 2 && $tabUser[1] == $tabToken[1]) {
							// perform connection
							$configFile = BM::loadCustomerInterface($tabUser[1]);

							if (self::connectionFromAppToken($xJwtToken, $tabToken[0], $tabUser[0], $configFile)) {
								self::buildComplementaryUserData();
								BM::setCurrentAPI(BM::API_TYPE_XJWTAPP);
								\Base::instance()->set('SESSION.isGod', $payload->mode == 'god' ? true : false);
								return true;
							}
						}
					} else {
						$tabToken = Tools::objectID_BM_decode($payload->clientToken);

						if (sizeof($tabToken) == 1 && sizeof($tabUser) == 2 && $tabUser[1] == $tabToken[0]) {
							// perform connection
							$configFile = BM::loadCustomerInterface($tabUser[1]);

							if (self::connectionFromClientToken($xJwtToken, $tabToken[0], $tabUser[0], $configFile)) {
								self::buildComplementaryUserData();
								BM::setCurrentAPI(BM::API_TYPE_XJWTCLIENT );
								\Base::instance()->set('SESSION.isGod', $payload->mode == 'god' ? true : false);
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * @param string $company
	 * @param Models\User $user
	 * @return string
	 */
	public static function getResetPasswordHash($company, Models\User $user){
		return md5($company . '*' . $user->login . '*resetpwd*' . $user->id . '*' . $user->lastConnexion);
	}

	/**
	 * Is login exists ?
	 * @param string $login
	 * @return bool
	 */
	public static function isLoginExists($login){
		$dbLogin = new BoondManager\Login();
		return $dbLogin->getLogin($login);
	}

	/**
	 * Get login
	 * @param string $login
	 * @return Models\Login|false
	 */
	public static function get($login) {
		$db = new BoondManager\Login();
		$dbData = $db->getLogin($login);
		return $dbData ? Wish\Mapper::fromSQL($dbData, Models\Login::class) : false;
	}

	/**
	 * @param string $mail dest
	 * @param string $url
	 */
	public static function sendSecurityAlert($mail, $url)
	{
		$email = new Email();
		$email->Send(
			$mail,
			Dictionary::getDict('main.application.mail.nouveau.objet'),
			Dictionary::getDict('main.application.mail.nouveau.message', $url),
			\Base::instance()->get('BMEMAIL.NOREPLY')
		);
	}

	//A CODER LORS DE LA CONNEXION
	/*
	public function saveConnectionInformations($urlweb = '', $user_login = '')
	{
		$lastDB = null;
		if(isset($this->db)) {$lastDB = $this->db;$this->db->close();}

		$this->db = new Wish_BaseDonnees(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE, DB_PRE);
		$this->db->connect();
		//IP de l'internaute
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip_simple = $_SERVER['HTTP_X_FORWARDED_FOR']; elseif(isset($_SERVER['HTTP_CLIENT_IP'])) $ip_simple = $_SERVER['HTTP_CLIENT_IP']; else $ip_simple = $_SERVER['REMOTE_ADDR'];
		//Navigateur  Système
		if(isset($_SERVER['HTTP_USER_AGENT'])) $syst = $_SERVER['HTTP_USER_AGENT']; else $syst = '';
		$idconnection = $this->db->query_insert('CONNECTIONS', array('CONNECT_IP' => $ip_simple, 'CONNECT_NAVIGATEUR' => $syst, 'CONNECT_DATE' => date('Y-m-d H:i:s', mktime()), 'CLIENT_WEB' => $urlweb, 'USER_LOGIN' => $user_login));
		$this->db->close();

		if(isset($lastDB)) {$this->db = $lastDB;$this->db->connect();}
		return $idconnection;
	}*/
}
