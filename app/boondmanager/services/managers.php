<?php
/**
 * managers.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use Wish\Tools;
use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;
use BoondManager\Models;
use Wish\Exceptions;
use Wish\Mapper;

class Managers{
	private static $managersIDs;

	/**
	 * @param $id
	 * @param bool $fallBackUser load an account if it's not a manager
	 * @return Models\Account|null
	 */
	public static function get($id, $fallBackUser = false){
		if($fallBackUser){
			$active = false;
			$addIDs = [$id];
		}else{
			$active = true;
			$addIDs = [];
		}

		$data = self::getAllGroupManagers($active, $addIDs);
		foreach($data as $manager) {
			if($manager->id == $id) {
				return $manager;
			}
		}

		return null;
	}

	/**
	 * @param array $ids
	 * @param bool $fallBackUser load an account even if it's not a manager
	 * @return Models\Account[]
	 */
	public static function getManagers(array $ids, $fallBackUser = false){
		if($fallBackUser){
			$active = false;
			$addIDs = $ids;
		}else{
			$active = true;
			$addIDs = [];
		}

		$data = self::getAllGroupManagers($active, $addIDs);
		$data = Tools::useColumnAsKey('id', $data);

		return array_filter($data, function($value) use ($ids){
			return in_array($value->id, $ids);
		});
	}

	/**
	 * @return array
	 */
	private static function getAllIds(){
		if(is_null(self::$managersIDs)) {
			$managersDB  = Local\Manager::instance();
			$managers    = $managersDB->getAllGroupManagers();
			self::$managersIDs = Tools::getFieldsToArray($managers, 'ID_PROFIL', 'ID_USER');
		}
		return self::$managersIDs;
	}

	/**
	 * @param int $idUser
	 * @return mixed
	 */
	public static function getEmployeeIdFromUserId($idUser){
		return Tools::mapData($idUser, self::getAllIds());
	}

	/**
	 * @param int $idProfil
	 * @return mixed
	 */
	public static function getUserIdFromEmployeeId($idProfil){
		return Tools::mapData($idProfil, array_flip(self::getAllIds()));
	}

	/**
	 * @param int $id ID_PROFIL
	 * @return Models\Account|mixed
	 * @throws \Wish\Exceptions\DatabaseIntegrity
	 */
	public static function getBasic($id){
		$db = Local\Manager::instance();
		$manager =  $db->getBasicManager($id);
		if(!$manager) return null;
		return Mapper::fromSQL($manager, Models\Account::class);
	}

	/**
	 * @param int $id ID_USER
	 * @return \BoondManager\Models\Account|null
	 * @deprecated pas bien, on devrait travailler avec l'id profil (sauf cas particulier)
	 */
	public static function getBasicFromUser($id){
		return self::getBasic( self::getEmployeeIdFromUserId($id) );
	}

	/**
	 * @param bool $onlyActive
	 * @param array $addManagers
	 * @return array|Models\Account[]
	 */
	public static function getAllGroupManagers($onlyActive = true, $addManagers = []){
		$db = Local\Manager::instance();
		$managers = $db->getSpecificGroupManagers($onlyActive, $addManagers);

		return Mapper::fromDatabaseArray($managers, Models\Account::class);
	}

	/**
	 * @param $id
	 * @param $addManagers
	 * @return Models\Account[]
	 */
	public static function getAllUserManagers($id, $addManagers){
		$db = Local\Manager::instance();
		$managers = $db->getAllUserManagers($id, $addManagers);

		return Mapper::fromDatabaseArray($managers, Models\Account::class);
	}

	/**
	 * @param array $tabBUs business units' id
	 * @return Models\Account[]
	 */
	public static function getManagersFromBusinessUnits($tabBUs){
		$db = Local\Manager::instance();
		$managers = $db->getManagersFromBusinessUnits($tabBUs);

		return Mapper::fromDatabaseArray($managers, Models\Account::class);
	}

	/**
	 * @param $id
	 * @param $bus
	 * @return bool
	 */
	public static function isManagerInBusinessUnits($id, $bus){
		$db = Local\Manager::instance();
		return $db->isManagerInBusinessUnits($id, $bus, true);
	}

	/**
	 * @param int $id ID_PROFIL
	 * @return Models\Account|mixed
	 * @throws \Wish\Exceptions\DatabaseIntegrity
	 */
	public static function getBasicSupport($id){
		$db = new BoondManager\Manager();
		$manager = $db->getBasicSupportManager($id);
		if(!$manager) throw new Exceptions\DatabaseIntegrity('Manager not found '.$id);
		return Mapper::fromSQL($manager, Models\Account::class);
	}

	/**
	 * @return array
	 */
	public static function getAllSupportIds(){
		$db = new BoondManager\Manager();
		return array_keys($db->getAllBasicSupportManagers());
	}
}
