<?php
/**
 * asbencesreport.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Notification;

use BoondManager\Services\CurrentUser;
use BoondManager\Services\Dictionary;
use BoondManager\Databases\Local\Action;
use BoondManager\Services\AbstractNotification;
use BoondManager\Services;

class AbsencesReport extends AbstractNotification{

	protected static $instance;

	private static $oldData;
	private static $newData;
	protected static $id;
	private static $tab;

	/**
	 * @param int $id Profil ID
	 * @param int $tab Tab ID
	 * @param array $oldData
	 * @param array $newData
	 * @param array $to add recipients. the array muste be a 2 dim array, with the first level containing 2 keys `true` or `false`.
	 * Example:
	 * $to = [
	 *  // Profil IDs
	 *  true => [1,3],
	 *  // User IDs
	 *  false => [1, 4]
	 * ]
	 *
	 * @return $this
	 */
	public static function getInstance($id, array $oldData, array $newData, $to = array()) {
		self::$oldData = $oldData;
		self::$newData = $newData;
		self::$id = $id;

		//On construit le tableau des destinataires par langue, clé false car on le charge par ID (et non pas par profil ID)
		$to = parent::tabDestinatairesMerge([true => [ $oldData['ID_PROFIL']]], $to);

		//On appelle le parent
		parent::init(Action::NOTIFICATION_TYPE_RESOURCE, $id, $to);

		self::$pushParams = [
			/*
			'resource' => [
				'name' => (isset($oldData['PROFIL_NOM'], $oldData['PROFIL_PRENOM'])) ? $oldData['PROFIL_PRENOM'].' '.$oldData['PROFIL_NOM'] : '',
			],
			*/
			'absencesreport' => [
				'path'=> Services\AbsencesReports::getApiUri($id),
				'name' => $oldData['LISTEABSENCES_DATE'],
				'id' => $id,
			],
			'author' => [
				'id' => CurrentUser::instance()->getAccount()->id,
				'name' => CurrentUser::instance()->getFullName()
			]
		];

		if(empty(self::$instance)) self::$instance = new self();
		return self::$instance;
	}



	public function validation($type, $baction, $idprofil_ressource, $title = '') {
		throw new \Exception('do migration'); //TODO
		if(in_array($type, array('ressource','manager')) && in_array($baction, array('validation','rejet'))) {
			if($type == 'ressource') {
				//On garde uniquement la ressource en destinataire (Le premier destinataire du tableau)
				foreach(parent::$tabDestinataires as $langue => $listeDest) {foreach($listeDest as $i => $manager) if($i > 0) unset(parent::$tabDestinataires[$langue][$i]);}
				$this->cleanDestinataires();
				parent::saveANDsend(0, 'b'.$baction.$type, $title, array(), array(), false, false);
			} else {
				if($baction == 'validation' && $idprofil_ressource != Wish_Session::getInstance()->get('login_idprofil')) {
					//On avertit également la ressource que son document a été validé par un validateur du circuit
					$tabDestinataires = parent::$tabDestinataires;
					foreach(parent::$tabDestinataires as $langue => $listeDest) {foreach($listeDest as $i => $manager) if($i > 0) unset(parent::$tabDestinataires[$langue][$i]);}
					parent::saveANDsend(0, 'bvalidationressource', '', array(), array(), false, false);
					$this->cleanDestinataires();
					parent::$tabDestinataires = $tabDestinataires;
				}

				//On avertit uniquement les managers en destinataire (On supprime le premier destinataire du tableau)
				foreach(parent::$tabDestinataires as $langue => $listeDest) unset(parent::$tabDestinataires[$langue][0]);
				$this->cleanDestinataires();
				parent::saveANDsend(0, 'b'.$baction.$type, $title, array(), array(), false, false);
			}
		}
	}

	public function deletevalidateur($validateur, $title = '') {//On sauvegarde uniquement
		throw new \Exception('do migration'); //TODO
		parent::saveANDsend(7, 'bdeletevalidateur', $title, array('__notificationvalidateur__'), array($validateur), false, true, false);
	}

	public function delete($title = '') {//On sauvegarde uniquement
		$actionMsg = Dictionary::prepareEntry('absencesreports.notifications.actions.delete',[ \DateTime::createFromFormat('Y-m-d',self::$pushParams['absencesreport']['name'])->format('d/m/Y')]);
		parent::_delete(self::$pushParams, $actionMsg, $title);
	}

	private function cleanDestinataires() {
		throw new \Exception('do migration'); //TODO
		foreach(parent::$tabDestinataires as $langue => $listeDest) if(sizeof($listeDest) == 0) unset(parent::$tabDestinataires[$langue]);
	}

}
