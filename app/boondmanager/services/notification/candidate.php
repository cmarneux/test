<?php
/**
 * candidate.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Notification;

use BoondManager\Models\Notification;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Services\Dictionary;
use BoondManager\Databases\Local\Action;
use BoondManager\Models;
use BoondManager\Services;
use Wish\Tools;

/**
 * Class Candidate
 * @package BoondManager\Models\Services\Notification
 */
class Candidate extends AbstractNotification2{

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_CANDIDATE;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'candidates.notifications.actions';

	/**
	 * @param Models\Candidate $item
	 * @param string $tab
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function update(Models\Candidate $item, $tab, $title = '', $recipientsCC = []) {

		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->setPath(Services\Candidates::getApiUri($item->id, $tab));
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_CANDIDATE);


		$checkKeys = [
			'firstName', 'lastName', 'email1', 'phone1', 'state', 'title',  'skills', 'mainManager', 'hrManager', 'agency'
		];
		foreach($checkKeys as $key){
			parent::detectChangedValue($changedData, $item, $key, 'candidates.sqlLabels.'.$key);
		}

		self::detectChangeDispoDate($changedData, $item);

		if($changedData) {
			$action->text = Dictionary::prepareEntry('candidates.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('candidates.notifications.actions.update', [
					Dictionary::prepareEntry('candidates.tabs.' . $tab)
				]),
				'list' => $changedData
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * check for a change in the dispo date
	 * @param array $tabChange
	 * @param Models\Candidate $item
	 */
	private static function detectChangeDispoDate(&$tabChange, $item){

		$availabilityChanged = $item->getChangedValue('availability');

		if(!$availabilityChanged) return;

		$old = $availabilityChanged['old'];
		$new = $availabilityChanged['new'];

		$mapping = Dictionary::getMapping('specific.setting.availability');

		$old = is_int($old) ? Tools::mapData($old, $mapping) : Tools::convertDateForUI($old);
		$new = is_int($new) ? Tools::mapData($new, $mapping) : Tools::convertDateForUI($new);

		$tabChange[] = self::buildDictEntryForField('candidates.sqlLabels.availability', $old, $new);
	}

	/**
	 * Handle notifications on candidate availability
	 * @param Models\Candidate $item
	 * @param $tab
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function updateDispo(Models\Candidate $item, $tab, $title = '', $recipientsCC = []) {

		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_CANDIDATE);

		self::detectChangeDispoDate($changedData, $item);

		if($changedData) {
			$action->text = Dictionary::prepareEntry('candidates.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('candidates.notifications.actions.update', [
					Dictionary::prepareEntry('candidates.tabs.' . $tab)
				]),
				'list' => $changedData
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * Handle notification for setting a candidate on a project
	 * @param Models\Candidate $item
	 * @param int $idPosition
	 * @param string $needtitle
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public function position($item, $idPosition, $needtitle, $title = '', $recipientsCC = []){


		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->additionalData([
			'position' => [
				'id' => $idPosition,
				'need' => $needtitle
			]
		]);
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_CANDIDATE);
		$action->text = Dictionary::prepareEntry('candidates.notifications.actions.position', $needtitle)->translate(Services\BM::getLanguage());

		self::saveAction($action);
	}

	/**
	 * @param Models\Candidate $item
	 * @param array $recipientsCC
	 * @return array
	 */
	protected static function initRecipients($item, $recipientsCC = [])
	{
		$recipientsCC[] = Services\CurrentUser::instance()->getEmployeeId();

		if($item->mainManager) $recipientsCC[] = $item->mainManager->id;
		if($item->hrManager) $recipientsCC[] = $item->hrManager->id;

		/** @var Models\Candidate $oldItem */
		$oldItem = $item->getBackup();
		if($oldItem->mainManager) $recipientsCC[] = $oldItem->mainManager->id;
		if($oldItem->hrManager) $recipientsCC[] = $oldItem->hrManager->id;

		return $recipientsCC;
	}
}
