<?php
/**
 * company.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Notification;

use BoondManager\Models;
use BoondManager\Models\Notification;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Services\Dictionary;
use BoondManager\Databases\Local\Action;
use BoondManager\Services;

/**
 * Class Resource
 * @package BoondManager\Models\Services\Notification
 */
class Company extends AbstractNotification2{

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_CRM_COMPANY;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'company.notifications.actions';

	/**
	 * Handle notifications on a company
	 * @param Models\Company $item
	 * @param string $tab
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function update($item, $tab, $title = '', $recipientsCC = []) {

		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->setPath(Services\Companies::getApiUri($item->id, $tab));
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_CRM_COMPANY);

		$tabChange = [];

		self::detectChangedRelationship($tabChange, $item, 'agency', 'customers.sqlLabels.agency');
		self::detectChangedList($tabChange, $item, 'state', 'customers.sqlLabels.state', Dictionary::getMapping('specific.setting.state.company'));
		self::detectChangedManager($tabChange, $item, 'mainManager', 'customers.sqlLabels.mainManager');
		self::detectChangedInfluencers($tabChange, $item, 'influencers', 'customers.sqlLabels.influencers');


		if($tabChange) {
			$action->text = Dictionary::prepareEntry('customers.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('customers.notifications.actions.update', [
					Dictionary::prepareEntry('customers.tabs.' . $tab)
				]),
				'list'  => $tabChange
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * @param Models\Company $item
	 * @param array $recipientsCC
	 * @return array
	 */
	protected static function initRecipients($item, $recipientsCC = [])
	{
		$recipientsCC[] = Services\CurrentUser::instance()->getEmployeeId();

		if($item->mainManager) $recipientsCC[] = $item->mainManager->id;

		$oldItem = $item->getBackup();
		if($oldItem->mainManager) $recipientsCC[] = $oldItem->mainManager->id;

		return $recipientsCC;
	}
}
