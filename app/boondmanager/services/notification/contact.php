<?php
/**
 * contact.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Notification;

use BoondManager\Models;
use BoondManager\Models\Notification;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Services\Dictionary;
use BoondManager\Databases\Local\Action;
use BoondManager\Services;
use Wish\Tools;

/**
 * Class Contact
 * @package BoondManager\Models\Services\Notification
 */
class Contact extends AbstractNotification2{

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_CRM;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'contacts.notifications.actions';

	/**
	 * Handle notifications on a contact
	 * @param Models\Contact $item
	 * @param string $tab
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function update($item, $tab, $title = '', $recipientsCC = []) {

		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->setPath(Services\Contacts::getApiUri($item->id, $tab));
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_CRM);

		$tabChange = [];
		$basicFields = ['firstName', 'lastName', 'department', 'email1', 'phone1', 'informationComments'];

		foreach($basicFields as $field) {
			self::detectChangedValue($tabChange, $item, $field, 'contacts.sqlLabels.' . $field);
		}
		self::detectChangedList($tabChange, $item, 'state', 'contacts.sqlLabels.state', Dictionary::getMapping('specific.setting.state.contact'));

		$mapping = Dictionary::getMapping('specific.setting.origin');
		if( $changeData = $item->getChangedValue('origin')) {
			$oldData = Tools::mapData($changeData['old']->typeOf, $mapping);
			$newData = Tools::mapData($changeData['new']->typeOf, $mapping);
			$tabChange[] = self::buildDictEntryForField( Dictionary::prepareEntry('contacts.sqlLabels.origin.typeOf'), $oldData, $newData);
		}
		self::detectChangedCompany($tabChange, $item, 'company', 'contacts.sqlLabels.company.id');
		self::detectChangedManager($tabChange, $item, 'mainManager', 'contacts.sqlLabels.mainManager');
		self::detectChangedInfluencers($tabChange, $item, 'influencers', 'contacts.sqlLabels.influencers');

		if($tabChange) {
			$action->text = Dictionary::prepareEntry('contacts.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('contacts.notifications.actions.update', [
					Dictionary::prepareEntry('contacts.tabs.' . $tab)
				]),
				'list'  => $tabChange
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * @param Models\Contact		 $item
	 * @param array $recipientsCC
	 * @return array
	 */
	protected static function initRecipients($item, $recipientsCC = [])
	{
		$recipientsCC[] = Services\CurrentUser::instance()->getEmployeeId();

		if($item->mainManager) $recipientsCC[] = $item->mainManager->id;

		$oldItem = $item->getBackup();
		if($oldItem->mainManager) $recipientsCC[] = $oldItem->mainManager->id;

		return $recipientsCC;
	}
}
