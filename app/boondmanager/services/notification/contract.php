<?php
/**
 * contracts.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Notification;

use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services\AbstractNotification;
use BoondManager\Services\CurrentUser;
use BoondManager\Databases\Local\Action;
use BoondManager\Models;
use BoondManager\Services;

/**
 * Class Resource
 * @package BoondManager\Models\Services\Notification
 * @TODO: refaire un refactoring complet, on ne peut pas utiliser un getInstance aussi parametrable => il faut soit plusieurs instance soit appeler les sous méthodes avec les parametres de getInstance
 */
class Contract extends AbstractNotification{

	protected static $instance;

	private static $oldData;
	private static $newData;
	protected static $id;
	private static $tab;

	/**
	 * @deprecated
	 */
	protected static $xmlConfiguration;

	/**
	 * @param int $id Profil ID
	 * @param int $tab Tab ID
	 * @param array $oldData
	 * @param array $newData
	 * @param array $to add recipients. the array muste be a 2 dim array, with the first level containing 2 keys `true` or `false`.
	 * Example:
	 * $to = [
	 *  // Profil IDs
	 *  true => [1,3],
	 *  // User IDs
	 *  false => [1, 4]
	 * ]
	 *
	 * @return $this
	 */
	public static function getInstance($id, array $oldData, array $newData, $to = array()) {
		self::$oldData = $oldData;
		self::$newData = $newData;
		self::$id = $id;

		//On appelle le parent
		// TODO handle dependsOn
		parent::init(Action::NOTIFICATION_TYPE_CRM_COMPANY, $id, $to);

		//TODO handle dependsOn
		self::$pushParams = [
			'contract' => [
				'path'=> Services\Contracts::getApiUri($id),
				//'name' => (isset($oldData['CSOC_SOCIETE'])) ? $oldData['CSOC_SOCIETE'] : self::$newData['CSOC_SOCIETE'],
				'id' => $id,
			],
			'author' => [
				'id' => CurrentUser::instance()->getAccount()->id,
				'name' => CurrentUser::instance()->getFullName()
			]
		];

		if(empty(self::$instance)) self::$instance = new self();
		return self::$instance;
	}

	/**
	 * Handle notifications on a company
	 * @param string $title
	 */
	public function update($title = '') {
		$tabChange = [];

		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_DEBUT', 'contracts.sqlLabels.CTR_DEBUT');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_FIN', 'contracts.sqlLabels.CTR_FIN');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_DATEPE1', 'contracts.sqlLabels.CTR_DATEPE1');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_DATEPE2', 'contracts.sqlLabels.CTR_DATEPE2');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_TYPE', 'contracts.sqlLabels.CTR_TYPE');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_CLASSIFICATION', 'contracts.sqlLabels.CTR_CLASSIFICATION');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_CATEGORIE', 'contracts.sqlLabels.CTR_CATEGORIE');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_TPSTRAVAIL', 'contracts.sqlLabels.CTR_TPSTRAVAIL');

		parent::setChangeList($tabChange, self::$oldData, self::$newData, 'CTR_DEVISE', 'contracts.sqlLabels.CTR_DEVISE', Dictionary::getMapping('specific.setting.currency'));
		// TODO faire un changeDevise pour les salaires
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_SALAIREMENSUEL', 'contracts.sqlLabels.CTR_SALAIREMENSUEL');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_SALAIREHEURE', 'contracts.sqlLabels.CTR_SALAIREHEURE');
		// fin du todo
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_NBJRSOUVRE', 'contracts.sqlLabels.CTR_NBJRSOUVRE');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_CHARGE', 'contracts.sqlLabels.CTR_CHARGE');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_FRSJOUR', 'contracts.sqlLabels.CTR_FRSJOUR');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'CTR_FRSMENSUEL', 'contracts.sqlLabels.CTR_FRSMENSUEL');

		parent::setChangeSociete($tabChange, self::$oldData, self::$newData, 'ID_SOCIETE', 'contracts.sqlLabels.ID_SOCIETE');

		$actionMsg = Dictionary::prepareEntry('contracts.notifications.specials.actionMessage', [
			'title' => Dictionary::prepareEntry('contracts.notifications.actions.update'),
			'list' => $tabChange
		]);
		parent::_update(self::$pushParams, $actionMsg, $title);
	}
}
