<?php
/**
 * delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Notification;

use BoondManager\Databases\Local\Action;
use BoondManager\Lib\Currency;
use BoondManager\Models\Notification;
use BoondManager\Models\Product;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Services;
use BoondManager\Services\Dictionary;

class Delivery extends AbstractNotification2 {

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_PROJECT;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'deliveries.notifications.actions';

	/**
	 * @param \BoondManager\Models\Delivery $item
	 * @param string $title
	 * @param array $recipientsCC
	 * @param null $id_other
	 * @throws \Exception
	 */
	public static function update(\BoondManager\Models\Delivery $item, $title = '', array $recipientsCC = [], $id_other = null){
		if(!is_null($id_other)) throw new \Exception('id_other not handled');

		// sending a notification to users
		if(isset($item->project->mainManager)) $recipientsCC[] = $item->project->mainManager->id;

		self::sendNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction($item->project, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_PROJECT);

		if($item->dependsOn instanceof Product) {
			parent::detectChangedDate($tabChange, $item, 'startDate', 'deliveries.sqlLabels.startDateProduct');
		} else {
			parent::detectChangedDate($tabChange, $item, 'startDate', 'deliveries.sqlLabels.startDate');
			parent::detectChangedDate($tabChange, $item, 'endDate', 'deliveries.sqlLabels.endDate');
		}

		if(isset($item->project)) {
			// devise
			$currency = new Currency();
			$change   = 1;
			if (isset($item->project->currencyAgency, $item->project->exchangeRateAgency))
				$currency->setDefault($item->project->currencyAgency, $item->project->exchangeRateAgency);
			if (isset($item->project->exchangeRate))
				$change = $item->project->exchangeRate;

			parent::detectChangedValue($tabChange, $item, 'state', 'deliveries.sqlLabels.state');

			switch ($item->project->mode) {
				case Services\BM::PROJECT_TYPE_PRODUCT:
					parent::detectChangedAmount($tabChange, $item, 'averageDailyCost', 'deliveries.sqlLabels.averageDailyCost', $currency, $change);
					parent::detectChangedAmount($tabChange, $item, 'averageDailyPriceExcludingTax', 'deliveries.sqlLabels.averageDailyPriceExcludingTaxProduct', $currency, $change);
					parent::detectChangedAmount($tabChange, $item, 'additionalTurnoverExcludingTax', 'deliveries.sqlLabels.additionalTurnoverExcludingTax', $currency, $change);
					parent::detectChangedValue($tabChange, $item, 'numberOfDaysInvoicedOrQuantity', 'deliveries.sqlLabels.numberOfDaysInvoicedOrQuantityProduct');
					break;
				case Services\BM::PROJECT_TYPE_TA:
					parent::detectChangedAmount($tabChange, $item, 'averageDailyCost', 'deliveries.sqlLabels.averageDailyCost', $currency, $change);
					parent::detectChangedAmount($tabChange, $item, 'averageDailyPriceExcludingTax', 'deliveries.sqlLabels.averageDailyPriceExcludingTax', $currency, $change);
					parent::detectChangedAmount($tabChange, $item, 'additionalTurnoverExcludingTax', 'deliveries.sqlLabels.additionalTurnoverExcludingTax', $currency, $change);
					parent::detectChangedValue($tabChange, $item, 'numberOfDaysInvoicedOrQuantity', 'deliveries.sqlLabels.numberOfDaysInvoicedOrQuantity');
					parent::detectChangedValue($tabChange, $item, 'numberOfDaysFree', 'deliveries.sqlLabels.numberOfDaysFree');
					break;
				case Services\BM::PROJECT_TYPE_PACKAGE:
					parent::detectChangedAmount($tabChange, $item, 'averageDailyCost', 'deliveries.sqlLabels.averageDailyCostPackage', $currency, $change);
					parent::detectChangedAmount($tabChange, $item, 'averageDailyPriceExcludingTax', 'deliveries.sqlLabels.averageDailyPriceExcludingTaxPackage', $currency, $change);
					parent::detectChangedAmount($tabChange, $item, 'additionalTurnoverExcludingTax', 'deliveries.sqlLabels.additionalTurnoverExcludingTax', $currency, $change);
					parent::detectChangedValue($tabChange, $item, 'numberOfDaysInvoicedOrQuantity', 'deliveries.sqlLabels.numberOfDaysInvoicedOrQuantityPackage');
					break;
			}
			parent::detectChangedAmount($tabChange, $item, 'additionalCostsExcludingTax', 'deliveries.sqlLabels.additionalCostsExcludingTax', $currency, $change);
		}

		if($tabChange) {
			$action->text = Dictionary::prepareEntry('deliveries.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('deliveries.notifications.actions.update', [
					'reference' => $item->getReference(),
					'relation' => $item->dependsOn instanceof Product ? $item->dependsOn->name : $item->dependsOn->getFullName()
				]),
				'list'  => $tabChange
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * @param \BoondManager\Models\Delivery $item
	 * @return mixed
	 */
	protected static function getActionParent($item)
	{
		return $item->project;
	}

	/**
	 * @param \BoondManager\Models\Delivery $item
	 * @param array $additionalRecipients
	 * @return array
	 */
	protected static function initRecipients($item, $additionalRecipients = [])
	{
		$additionalRecipients[] = $item->project->mainManager->id;
		return $additionalRecipients;
	}
}
