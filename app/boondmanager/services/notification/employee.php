<?php
/**
 * resource.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Notification;

use BoondManager\Models\Notification;
use Wish\Tools;
use BoondManager\Services\Dictionary;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Databases\Local\Action;
use BoondManager\Models;
use BoondManager\Services;

/**
 * Class Resource
 * @package BoondManager\Models\Services\Notification
 */
class Employee extends AbstractNotification2{

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_RESOURCE;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'resources.notifications.actions';

	/**
	 * Handle notifications on a profil
	 * @param Models\Employee $item
	 * @param $tab
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function update(Models\Employee $item, $tab, $title = '', $recipientsCC = []) {

		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Models\Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->setPath( Services\Employees::getApiUri($item->id, $tab));
		self::notify($notification, true);

		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_RESOURCE);

		$changedData = array();
		$checkKeys = [
			'lastName', 'firstName', 'reference', 'email1', 'phone1', 'typeOf', 'state', 'title', 'skills', 'mainManager', 'hrManager', 'agency'
		];
		foreach($checkKeys as $key){
			parent::detectChangedValue($changedData, $item, $key, 'resources.sqlLabels.'.$key);
		}

		self::detectChangeDispoDate($changedData, $item);

		if($changedData) {
			$action->text = Dictionary::prepareEntry('resources.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('resources.notifications.actions.update', [
					Dictionary::prepareEntry('resources.tabs.' . $tab)
				]),
				'list'  => $changedData
			])->translate( Services\BM::getLanguage() );
			self::saveAction($action);
		}
	}

	/**
	 * check for a change in the dispo date
	 * @param array $tabChange
	 * @param Models\Employee $item
	 */
	private static function detectChangeDispoDate(&$tabChange, $item){

		$availabilityChanged = $item->getChangedValue('availability');

		if(!$availabilityChanged) return;

		$old = $availabilityChanged['old'];
		$new = $availabilityChanged['new'];

		$old = $old == 'immediate' ? Dictionary::prepareEntry('main.values.immediate') : Tools::convertDateForUI($old);
		$new = $old == 'immediate' ? Dictionary::prepareEntry('main.values.immediate') : Tools::convertDateForUI($new);

		$tabChange[] = self::buildDictEntryForField('candidates.sqlLabels.availability', $old, $new);
	}

	/**
	 * Handle notifications on resource disponibility
	 * @param Models\Employee $item
	 * @param $tab
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function updateDispo(Models\Employee $item, $tab, $title = '', $recipientsCC = []) {

		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->setPath( Services\Employees::getApiUri($item->id, $tab));
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_RESOURCE);

		self::detectChangeDispoDate($changedData, $item);

		if($changedData) {
			$action->text = Dictionary::prepareEntry('resources.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('resources.notifications.actions.update', [
					Dictionary::prepareEntry('candidates.resources.' . $tab)
				]),
				'list'  => $changedData
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * Handle notification for setting a candidate on a project
	 * @param Models\Employee $item
	 * @param int $idPosition
	 * @param string $needtitle
	 * @param string $title
	 * @param array $recipientsCC
	 */
	public static function position($item, $idPosition, $needtitle, $title = '', $recipientsCC = []){


		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		$notification = static::buildNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);
		$notification->additionalData([
			'position' => [
				'id' => $idPosition,
				'need' => $needtitle
			]
		]);
		self::notify($notification, true);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_RESOURCE);
		$action->text = Dictionary::prepareEntry('resources.notifications.actions.position', $needtitle)->translate(Services\BM::getLanguage());

		self::saveAction($action);
	}

	/**
	 * @param Models\Employee $item
	 * @param array $recipientsCC
	 * @return array
	 */
	protected static function initRecipients($item, $recipientsCC = [])
	{
		$recipientsCC[] = Services\CurrentUser::instance()->getEmployeeId();

		if($item->mainManager) $recipientsCC[] = $item->mainManager->id;
		if($item->hrManager) $recipientsCC[] = $item->hrManager->id;

		/** @var Models\Candidate $oldItem */
		$oldItem = $item->getBackup();
		if($oldItem->mainManager) $recipientsCC[] = $oldItem->mainManager->id;
		if($oldItem->hrManager) $recipientsCC[] = $oldItem->hrManager->id;

		return $recipientsCC;
	}
}
