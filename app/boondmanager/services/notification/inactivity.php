<?php
/**
 * delivery.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services\Notification;

use BoondManager\Databases\Local\Action;
use BoondManager\Lib\Currency;
use BoondManager\Models\Notification;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Services;
use BoondManager\Services\Dictionary;

class Inactivity extends AbstractNotification2 {

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_RESOURCE;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'deliveries.notifications.actions';

	/**
	 * @param \BoondManager\Models\Inactivity $item
	 * @param string $title
	 * @param array $recipientsCC
	 * @param null $id_other
	 * @throws \Exception
	 */
	public static function update(\BoondManager\Models\Inactivity $item, $title = '', array $recipientsCC = [], $id_other = null){
		if(!is_null($id_other)) throw new \Exception('id_other not handled');

		// sending a notification to users
		$recipientsCC = self::initRecipients($item, $recipientsCC);

		self::sendNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction($item->project, self::STATUS_UPDATE, $title, Action::NOTIFICATION_TYPE_RESOURCE);

		parent::detectChangedDate($tabChange, $item, 'startDate', 'deliveries.sqlLabels.startDate');
		parent::detectChangedDate($tabChange, $item, 'endDate', 'deliveries.sqlLabels.endDate');

		if(isset($item->project)) {
			// devise
			$currency = new Currency();
			$change   = 1;
			if (isset($item->project->currencyAgency, $item->project->exchangeRateAgency))
				$currency->setDefault($item->project->currencyAgency, $item->project->exchangeRateAgency);
			if (isset($item->project->exchangeRate))
				$change = $item->project->exchangeRate;

			parent::detectChangedAmount($tabChange, $item, 'averageDailyCost', 'deliveries.sqlLabels.averageDailyCost', $currency, $change);
			parent::detectChangedValue($tabChange, $item, 'state', 'deliveries.sqlLabels.state');
			parent::detectChangedList($tabChange, $item, 'inactivityType', 'deliveries.sqlLabels.inactivityTypes', Dictionary::getDict('deliveries.values.inactivityTypes'));
			parent::detectChangedValue($tabChange, $item, 'numberOfDaysInvoicedOrQuantity', 'deliveries.sqlLabels.numberOfDaysInvoicedOrQuantity');
		}

		if($tabChange) {
			$action->text = Dictionary::prepareEntry('deliveries.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('deliveries.notifications.actions.update', [
					'reference' => $item->getReference(),
					'relation' => $item->resource->getFullName()
				]),
				'list'  => $tabChange
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * @param \BoondManager\Models\Inactivity $item
	 * @return mixed
	 */
	protected static function getActionParent($item)
	{
		return $item->project;
	}

	/**
	 * @param \BoondManager\Models\Inactivity $item
	 * @param array $recipientsCC
	 * @return array
	 */
	protected static function initRecipients($item, $recipientsCC = [])
	{
		$recipientsCC[] = Services\CurrentUser::instance()->getEmployeeId();
		if($item->resource->mainManager) $recipientsCC[] = $item->resource->mainManager->id;
		if($item->resource->hrManager) $recipientsCC[] = $item->resource->hrManager->id;
		return $recipientsCC;
	}
}
