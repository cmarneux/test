<?php
/**
 * opportunity.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Notification;

use BoondManager\Lib\Currency;
use Wish\Tools;
use BoondManager\Services\BM;
use BoondManager\Services\Dictionary;
use BoondManager\Services\AbstractNotification;
use BoondManager\Services\CurrentUser;
use BoondManager\Databases\Local\Action;
use BoondManager\Services;

/**
 * Class Resource
 * @package BoondManager\Models\Services\Notification
 * @TODO: refaire un refactoring complet, on ne peut pas utiliser un getInstance aussi parametrable => il faut soit plusieurs instance soit appeler les sous méthodes avec les parametres de getInstance
 */
class Opportunity extends AbstractNotification{

	protected static $instance;

	private static $oldData;
	private static $newData;
	protected static $id;
	private static $tab;

	/**
	 * @deprecated
	 */
	protected static $xmlConfiguration;

	/**
	 * @param int $id Profil ID
	 * @param int $tab Tab ID
	 * @param array $oldData
	 * @param array $newData
	 * @param array $to add recipients. the array muste be a 2 dim array, with the first level containing 2 keys `true` or `false`.
	 * Example:
	 * $to = [
	 *  // Profil IDs
	 *  true => [1,3],
	 *  // User IDs
	 *  false => [1, 4]
	 * ]
	 *
	 * @return $this
	 */
	public static function getInstance($id, $tab, array $oldData, array $newData, $to = array()) {
		self::$oldData = $oldData;
		self::$newData = $newData;
		self::$id = $id;
		self::$tab = $tab;

		//On construit le tableau des destinataires par langue, clé false car on le charge par ID (et non pas par profil ID)
		$to = parent::tabDestinatairesMerge([false => [ CurrentUser::instance()->getUserId()]], $to);
		if(isset(self::$newData['ID_PROFIL']) && self::$newData['ID_PROFIL'] > 0) $to[true][] = self::$newData['ID_PROFIL'];
		if(isset(self::$oldData['ID_PROFIL']) && self::$oldData['ID_PROFIL'] > 0) $to[true][] = self::$oldData['ID_PROFIL'];

		//On appelle le parent
		parent::init(Action::NOTIFICATION_TYPE_OPPORTUNITY, $id, $to);

		self::$pushParams = [
			'opportunity' => [
				'path'=> Services\Opportunities::getApiUri($id, $tab),
				'name' => (isset($oldData['AO_TITLE'])) ? $oldData['AO_TITLE'] : self::$newData['AO_TITLE'],
				'tab' => $tab,
				'id' => $id,
			],
			'author' => [
				'id' => CurrentUser::instance()->getAccount()->id,
				'name' => CurrentUser::instance()->getFullName()
			]
		];

		if(empty(self::$instance)) self::$instance = new self();
		return self::$instance;
	}

	/**
	 * Handle notifications on a company
	 * @param string $title
	 */
	public function update($title = '') {
		$tabChange = [];

		$devise = new Currency();
		if(isset(self::$newData['AO_DEVISEAGENCE'], self::$newData['AO_CHANGEAGENCE']))
			$devise->setDefault(self::$newData['AO_DEVISEAGENCE'], self::$newData['AO_CHANGEAGENCE']);
		else if(isset(self::$oldData['AO_DEVISEAGENCE'], self::$oldData['AO_CHANGEAGENCE']))
			$devise->setDefault(self::$oldData['AO_DEVISEAGENCE'], self::$oldData['AO_CHANGEAGENCE']);

		if(isset(self::$newData['AO_CHANGE'])) $change = self::$newData['AO_CHANGE'];
		else if(isset(self::$oldData['AO_CHANGE'])) $change = self::$oldData['AO_CHANGE'];
		else $change = 1;

		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'AO_TITLE', 'opportunities.sqlLabels.AO_TITLE');
		parent::setChangeList($tabChange, self::$oldData, self::$newData, 'AO_TYPEREF', 'opportunities.sqlLabels.AO_TYPEREF', Dictionary::getMapping('specific.setting.typeOf.project'));
		parent::setChangeList($tabChange, self::$oldData, self::$newData, 'AO_ETAT', 'opportunities.sqlLabels.AO_ETAT', Dictionary::getMapping('specific.setting.state.opportunity'));
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'AO_REF', 'opportunities.sqlLabels.AO_REF');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'ID_CRMCONTACT', 'opportunities.sqlLabels.ID_CRMCONTACT', 'CCON', [-1, 0]);
		parent::setChangeList($tabChange, self::$oldData, self::$newData, 'AO_DUREE', 'opportunities.sqlLabels.AO_DUREE', Dictionary::getMapping('specific.setting.duration'));


		if(isset(self::$oldData['AO_TYPEDEBUT'], self::$newData['AO_TYPEDEBUT'])
			&& (self::$oldData['AO_TYPEDEBUT'] != self::$newData['AO_TYPEDEBUT']
				|| (self::$oldData['AO_TYPEDEBUT'] == BM::DISPO_DATE && isset(self::$oldData['AO_DEBUT'], self::$newData['AO_DEBUT']) && self::$oldData['AO_DEBUT'] != self::$newData['AO_DEBUT']))
		) {
			$oldDebut = (self::$oldData['AO_TYPEDEBUT'] == BM::DISPO_DATE)
				? Tools::convertDateForUI(self::$oldData['AO_DEBUT'])
				: Dictionary::prepareEntry('main.values.immediate');

			$newDebut = (self::$newData['AO_TYPEDEBUT'] == BM::DISPO_DATE)
				? Tools::convertDateForUI(self::$newData['AO_DEBUT'])
				: Dictionary::prepareEntry('main.values.immediate');

			$tabChange[] = self::buildDictEntryForField('opportunities.sqlLabels.AO_DEBUT', $oldDebut, $newDebut);
		}

		parent::setChangeList($tabChange, self::$oldData, self::$newData, 'AO_DEVISE', 'opportunities.sqlLabels.AO_DEVISE', Dictionary::getMapping('specific.setting.currency'));
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'AO_BUDGET', 'opportunities.sqlLabels.AO_BUDGET', '', [], self::TYPE_MONEY, $devise, $change);
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'AO_CA', 'opportunities.sqlLabels.AO_CA', '', [], self::TYPE_MONEY, $devise, $change);
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'AO_PROBA', 'opportunities.sqlLabels.AO_PROBA');
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'AO_CRITERES', 'opportunities.sqlLabels.AO_CRITERES');
		parent::setChangeManager($tabChange, self::$oldData, self::$newData, 'ID_PROFIL', 'opportunities.sqlLabels.ID_PROFIL', true);

		parent::setChangeSociete($tabChange, self::$oldData, self::$newData, 'ID_SOCIETE', 'opportunities.sqlLabels.ID_SOCIETE');

		$actionMsg = Dictionary::prepareEntry('opportunities.notifications.specials.actionMessage', [
			'title' => Dictionary::prepareEntry('opportunities.notifications.actions.update',[
				Dictionary::prepareEntry('opportunities.tabs.'.self::$tab)
			]),
			'list' => $tabChange
		]);
		parent::_update(self::$pushParams, $actionMsg, $title);
	}

	public function delete($title = ''){
		$actionMsg = Dictionary::prepareEntry('opportunities.notifications.actions.delete',[self::$pushParams['opportunity']['name']]);
		parent::_delete(self::$pushParams, $actionMsg, $title);
	}
}
