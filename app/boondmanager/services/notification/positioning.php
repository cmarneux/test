<?php
/**
 * positioning.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Notification;

use BoondManager\Services\Dictionary;
use BoondManager\Models\Candidate;
use BoondManager\Services\AbstractNotification;
use BoondManager\Services\CurrentUser;
use BoondManager\Databases\Local\Action;
use BoondManager\Services;

/**
 * Class Resource
 * @package BoondManager\Models\Services\Notification
 * @TODO: refaire un refactoring complet, on ne peut pas utiliser un getInstance aussi parametrable => il faut soit plusieurs instance soit appeler les sous méthodes avec les parametres de getInstance
 */
class Positioning extends AbstractNotification{

	protected static $instance;

	private static $oldData;
	private static $newData;
	protected static $id;
	private static $tab;

	protected static $profilType;

	/**
	 * @deprecated
	 */
	protected static $xmlConfiguration;

	/**
	 * @param int $id Profil ID
	 * @param int $tab Tab ID
	 * @param array $oldData
	 * @param array $newData
	 * @param array $to add recipients. the array muste be a 2 dim array, with the first level containing 2 keys `true` or `false`.
	 * Example:
	 * $to = [
	 *  // Profil IDs
	 *  true => [1,3],
	 *  // User IDs
	 *  false => [1, 4]
	 * ]
	 *
	 * @return $this
	 *
	 * TODO: test & migrate what's left
	 */
	public static function getInstance($id, $tab, array $oldData, array $newData, $to = array()) {
		self::$oldData = $oldData;
		self::$newData = $newData;
		self::$id = $id;
		self::$tab = $tab;

		//On construit le tableau des destinataires par langue, clé false car on le charge par ID (et non pas par profil ID)
		$to = parent::tabDestinatairesMerge([false => [ CurrentUser::instance()->getUserId()]], $to);
		if(isset(self::$newData['AO_IDPROFIL']) && self::$newData['AO_IDPROFIL'] > 0) $to[true][] = self::$newData['AO_IDPROFIL'];
		if(isset(self::$oldData['ID_RESPPROFIL']) && self::$oldData['ID_RESPPROFIL'] > 0) $to[true][] = self::$oldData['ID_RESPPROFIL'];

		//On appelle le parent
		parent::init(Action::NOTIFICATION_TYPE_OPPORTUNITY, $id, $to);

		if(self::$oldData['ITEM_TYPE'] == 1){
			$path = "products/$id/positionings";
			self::$profilType = 'PROD';
		}else if(self::$oldData['PROFIL_TYPE'] == Candidate::TYPE_CANDIDATE){
			$path = "candidates/$id/positionings";
			self::$profilType = 'CAND';
		}else{
			$path = "resources/$id/positionings";
			self::$profilType = 'COMP';
		}

		self::$pushParams = [
			'positioning' => [
				'path'=> $path,
				'name' => (isset($oldData['AO_TITLE'])) ? $oldData['AO_TITLE'] : self::$newData['AO_TITLE'],
				'tab' => $tab,
				'id' => $id,
			],
			'author' => [
				'id' => CurrentUser::instance()->getAccount()->id,
				'name' => CurrentUser::instance()->getFullName()
			]
		];

		if(empty(self::$instance)) self::$instance = new self();
		return self::$instance;
	}

	/**
	 * Handle notifications on a company
	 * @param string $title
	 * @param string $newPrefixType
	 */
	public function update($title = '', $newPrefixType = 'COMP') {
		$tabChange = [];

		if(self::$profilType == 'PROD')
			parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'ID_ITEM', 'positioning.sqlLabels.POS_ETAT', [self::$profilType, $newPrefixType], [-1, 0]);
		else {
			parent::setChangeList($tabChange, self::$oldData, self::$newData, 'POS_ETAT', 'positioning.sqlLabels.POS_ETAT', Dictionary::getMapping('specific.setting.state.positioning') );
			parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'ID_ITEM', 'positioning.sqlLabels.ID_ITEM', [self::$profilType, $newPrefixType], [-1, 0]);
		}
		parent::setChangeValue($tabChange, self::$oldData, self::$newData, 'POS_COMMENTAIRE', 'positioning.sqlLabels.POS_COMMENTAIRE');

		$actionMsg = Dictionary::prepareEntry('positioning.notifications.specials.actionMessage', [
			'title' => Dictionary::prepareEntry('positioning.notifications.actions.update',[
				Dictionary::prepareEntry('positioning.tabs.'.self::$tab)
			]),
			'list' => $tabChange
		]);
		parent::_update(self::$pushParams, $actionMsg, $title);
	}

	public function delete($title = ''){
		$actionMsg = Dictionary::prepareEntry('positioning.notifications.actions.delete',[self::$pushParams['positioning']['name']]);
		parent::_delete(self::$pushParams, $actionMsg, $title);
	}
}
