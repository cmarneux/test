<?php
/**
 * purchase.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services\Notification;

use BoondManager\Databases\Local\Action;
use BoondManager\Lib\Currency;
use BoondManager\Models\Notification;
use BoondManager\Services\AbstractNotification2;
use BoondManager\Services;
use BoondManager\Services\Dictionary;
use Wish\Tools;

class Purchase extends AbstractNotification2 {

	const ACTION_NOTIFICATION_TYPE = Action::NOTIFICATION_TYPE_PURCHASE;
	const ACTION_NOTIFICATION_DICT_PREFIX = 'purchases.notifications.actions';

	/**
	 * @param \BoondManager\Models\Purchase $item
	 * @param string $tab
	 * @param string $title
	 * @param array $recipientsCC
	 * @param null $id_other
	 * @throws \Exception
	 */
	public static function update(\BoondManager\Models\Purchase $item, $tab, $title = '', array $recipientsCC = [], $id_other = null){
		if(!is_null($id_other)) throw new \Exception('id_other not handled');

		/** @var \BoondManager\Models\Purchase $oldItem */
		$oldItem = $item->getBackup();

		$recipientsCC = self::initRecipients($item, $recipientsCC);
		self::sendNotification($item, Notification::TYPE_UPDATE, $title, $recipientsCC);

		// building an entry in tab action
		$action = self::buildAction($item, self::STATUS_UPDATE, $title, self::ACTION_NOTIFICATION_TYPE);

		// devise
		$currency = new Currency();
		$change   = 1;
		if (isset($item->currencyAgency, $item->exchangeRateAgency)) {
			$currency->setDefault($item->currencyAgency, $item->exchangeRateAgency);
		} else if(isset($oldItem->currencyAgency, $oldItem->exchangeRateAgency)) {
			$currency->setDefault($oldItem->currencyAgency, $oldItem->exchangeRateAgency);
		}
		if (isset($item->exchangeRate)) $change = $item->exchangeRate;
		else if (isset($oldItem->exchangeRate)) $change = $oldItem->exchangeRate;

		$tabChange = [];
		parent::detectChangedValue($tabChange, $item, 'title', 'purchases.sqlLabels.title');
		parent::detectChangedList($tabChange, $item, 'state', 'purchases.sqlLabels.state', Dictionary::getMapping('specific.setting.state.purchase'));
		parent::detectChangedAmount($tabChange, $item, 'amountExcludingTax', 'purchases.sqlLabels.amountExcludingTax', $currency, $change);
		parent::detectChangedValue($tabChange, $item, 'quantity', 'purchases.sqlLabels.quantity');
		parent::detectChangedValue($tabChange, $item, 'reference', 'purchases.sqlLabels.reference');
		parent::detectChangedValue($tabChange, $item, 'number', 'purchases.sqlLabels.number');
		parent::detectChangedValue($tabChange, $item, 'toReinvoice', 'purchases.sqlLabels.toReinvoice');
		parent::detectChangedAmount($tabChange, $item, 'reinvoiceAmountExcludingTax', 'purchases.sqlLabels.reinvoiceAmountExcludingTax', $currency, $change);
		parent::detectChangedRelationship($tabChange, $item, 'project', 'purchases.sqlLabels.project');
		parent::detectChangedRelationship($tabChange, $item, 'contact', 'purchases.sqlLabels.contact');
		parent::detectChangedList($tabChange, $item, 'subscription', 'purchases.sqlLabels.subscription', Dictionary::getMapping('specific.setting.typeOf.subscription'));
		parent::detectChangedManager($tabChange, $item, 'purchases.sqlLabels.subscription');
		parent::detectChangedAgency($tabChange, $item, 'purchases.sqlLabels.agency');

		$tabName = Tools::mapData($tab, Dictionary::getDict('default.tabs'));

		if($tabChange) {
			$action->text = Dictionary::prepareEntry('purchases.notifications.specials.actionMessage', [
				'title' => Dictionary::prepareEntry('purchases.notifications.actions.update', [ $tabName ]),
				'list'  => $tabChange
			])->translate(Services\BM::getLanguage());

			self::saveAction($action);
		}
	}

	/**
	 * @param \BoondManager\Models\Purchase $item
	 * @param array $recipientsCC
	 * @return array
	 */
	protected static function initRecipients($item, $recipientsCC = [])
	{
		/** @var  \BoondManager\Models\Purchase $oldItem */
		$oldItem = $item->getBackup();

		// sending a notification to users
		if(isset($item->mainManager)) $recipientsCC[] = $item->mainManager->id;
		if(isset($oldItem->mainManager)) $recipientsCC[] = $oldItem->mainManager->id;

		return $recipientsCC;
	}
}
