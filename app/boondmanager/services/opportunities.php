<?php
/**
 * opportunities.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Opportunities\Specifications\IsActionAllowed;
use BoondManager\APIs\Positionings\Filters\SearchPositionings;
use Wish\Filters\AbstractJsonAPI;
use Wish\Tools;
use BoondManager\OldModels\Filters;
use BoondManager\Lib\RequestAccess;
use BoondManager\Lib\Specifications\HaveAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveReadAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveWriteAccess;
use BoondManager\APIs\Opportunities\Specifications\HaveWriteAccessOnField;
use BoondManager\OldModels\Specifications\RequestAccess\Opportunities\MyCRM;
use BoondManager\Models;
use BoondManager\APIs\Contacts;
use BoondManager\APIs\Opportunities\Filters\SearchOpportunities;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use Wish\Models\SearchResult;
use BoondManager\OldModels\Specifications;

/**
 * Class Opportunities
 * @package BoondManager\Models\Services
 */
class Opportunities{
	/**
	 * @param int îd
	 * @return Models\Opportunity|false
	 */
	public static function find($id) {
		$filter = new SearchOpportunities();
		$filter->keywords->setValue( Models\Opportunity::buildReference($id) );

		$sql = new Local\Opportunity();
		$result = $sql->searchOpportunities($filter);

		if($result->rows) return Mapper\Opportunity::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Models\Opportunity $entity
	 * @return array
	 */
	public static function getVisibleTabs(Models\Opportunity $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Models\Opportunity::getAllTabs();
		$readSpec = new HaveReadAccess(Models\Opportunity::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of available actions
	 * @param Models\Opportunity $entity
	 * @return array
	 */
	public static function getAvailableActions(Models\Opportunity $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		//~ TODO : Tin : à tester
		$actions = [
			'addAction' => (new HaveWriteAccess(Models\Opportunity::TAB_ACTIONS))->isSatisfiedBy($request),
			'addPositionning' => $entity->AO_TYPE != BM::PROJECT_TYPE_PRODUCT && (new HaveWriteAccess(Models\Opportunity::TAB_POSITIONINGS))->isSatisfiedBy($request),
			'addProject' => $entity->AO_TYPE != BM::PROJECT_TYPE_PRODUCT &&
				(new HaveWriteAccess(Models\Opportunity::TAB_PROJECTS))
				->and_(new HaveAccess(BM::MODULE_PROJECTS))
				->and_(new HaveAccess(BM::MODULE_RESOURCES))
				->and_(new HaveAccess(BM::MODULE_CANDIDATES))
				->isSatisfiedBy($request),
			'sell' =>
				(new HaveWriteAccess(Models\Opportunity::TAB_PROJECTS))
				->and_(new HaveAccess(BM::MODULE_PROJECTS))
				->and_(new HaveAccess(BM::MODULE_PRODUCTS))
				->isSatisfiedBy($request),
			'duplicate' => true,
			'share' => true,
		];

		return $actions;
	}

	/**
	 * get a list of available actions
	 * @param Models\Opportunity $entity
	 * @return array
	 */
	public static function getAvailableFields(Models\Opportunity $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$fields = [];
		foreach(HaveWriteAccessOnField::getFieldsTested() as $f){
			$fields[$f] = (new HaveWriteAccessOnField($f))->isSatisfiedBy($request);
		}

		return $fields;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Models\Opportunity $entity
	 * @return array
	 */
	public static function getWritableTabs(Models\Opportunity $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Models\Opportunity::getAllTabs();
		$readSpec = new HaveWriteAccess(Models\Opportunity::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * Search projects
	 * @param SearchOpportunities $filter
	 * @return SearchResult
	 */
	public static function search(SearchOpportunities $filter)
	{
		$db = new Local\Opportunity();
		$searchResult = $db->searchOpportunities($filter);
		$searchResult = Mapper\Opportunity::fromSearchResult($searchResult);

		/**
		 * creating some variable to filter datas
		 */
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		foreach ($searchResult->rows as $opportunity){
			$request->setData($opportunity);

			//TODO : Migrer MyCRM dans le dossier specifications de l'API
			$opportunity->canReadCompany = $opportunity->canReadContact = (new MyCRM)->isSatisfiedBy($request);
			if(!$opportunity->canReadContact) {
				/**
				 *  @var Models\Opportunity $opportunity
				 */
				$opportunity->removeFields(['contact', 'company']);
			}
		}
		return $searchResult;
	}

	/**
	 * @param $id
	 * @param int $tab
	 * @return Models\Opportunity
	 */
	public static function get($id, $tab = Models\Opportunity::TAB_DEFAULT){
		$db = Local\Opportunity::instance();
		$entity = $db->getOpportunity($id, $tab);

		if(!$entity) return null;

		$entity = Mapper\Opportunity::fromSQL($entity, $tab);

		$requestAccess = new RequestAccess();
		$requestAccess->setUser(CurrentUser::instance());
		$requestAccess->setData($entity->contact);
		$entity->canReadCompany = $entity->canReadContact = (new Contacts\Specifications\HaveReadAccess(Models\Contact::TAB_DEFAULT))->isSatisfiedBy($requestAccess);

		if($tab == Models\Opportunity::TAB_SIMULATION){

			// init filter
			$filter = new SearchPositionings();
			$filter->setIndifferentPerimeter();
			// overwriting keywords & positioningType to restrain the research
			$filter->keywords->setValue( $entity->getReference() );
			if($entity->mode == BM::PROJECT_TYPE_PRODUCT) {
				$filter->positioningType->setValue(Models\Positioning::ENTITY_PRODUCT);
			}else{
				$filter->positioningType->setValue(Models\Positioning::ENTITY_PROFILE);
			}

			$filter->isValid();

			$result = Positionings::search($filter);

			$entity->calculatePositioningsData($result->total_ca, $result->total_costs);
		}

		return $entity;
	}

	/**
	 * @param Models\Opportunity $entity
	 * @param AbstractJsonAPI $filter
	 * @return \Wish\Models\Model|Models\Opportunity|false
	 */
	public static function update(Models\Opportunity &$entity, AbstractJsonAPI $filter){


		$sqlData = self::transformFilterDataToSQLData($filter);
		$oldValues = $entity->toArray();

		$db = new Local\Opportunity();
		$db->updateOpportunity($sqlData, $entity->id);

		if($filter instanceof Filters\Profiles\Opportunities\SaveInformation){
			$tab = Models\Opportunity::TAB_INFORMATION;
		}else {
			$tab = Models\Opportunity::TAB_SIMULATION;
		}
		$entity = self::get($entity->id, $tab);

		$newValues = $entity->toArray();

		Notification\Opportunity::getInstance($entity->id, $tab, $oldValues, $newValues)->update();

		return $entity;
	}

	private static function transformFilterDataToSQLData(AbstractJsonAPI $filter){

		$data = $filter->getValue();
		$data = $data['attributes'];

		$sampleContact = new Models\Opportunity();
		$sqlData = [];

		if($filter instanceof Filters\Profiles\Opportunities\SaveInformation) {
			$sqlData['AO'] = array_merge(
				Tools::reversePublicFieldsToPrivate($sampleContact->getPublicFieldsMapping(), $data),
				Tools::reversePublicFieldsToPrivate($sampleContact->getRelationshipMapping(), $filter->relationships->getValue())
			);

			$sqlData['AO']['AO_OUTILS'] = Tools::serializeArray($filter->tools->getValue());
			$sqlData['AO']['AO_LIEU'] = Tools::serializeArray($filter->place->getValue());
			$sqlData['AO']['AO_APPLICATIONS'] = Tools::serializeArray($filter->activityAreas->getValue());

			if ($filter->origin) {
				$sqlData['AO']['AO_TYPESOURCE'] = $filter->origin->typeOf->getValue();
				$sqlData['AO']['AO_SOURCE']     = $filter->origin->detail->getValue();
			}
		}else if($filter instanceof Filters\Profiles\Opportunities\SavePositionsDetails){
			$sqlData['POSITIONDETAILS'] = [];
			foreach($filter->additionalTurnoverAndCosts->getValue() as $TCdata) {
				$sqlData['POSITIONDETAILS'][] = Tools::reversePublicFieldsToPrivate(\BoondManager\Models\PositioningDetail::getPublicFieldsMapping(), $TCdata);
			}
			$sqlData['AO']['AO_CORRELATIONPOS'] = $filter->validateSimulation->getValue();
		}

		return $sqlData;
	}

	public static function getNew(\BoondManager\Models\Contact $contact)
	{
		$user = CurrentUser::instance();

		$basicDatas = [
			'ID_AO' => 0,
			'AO_ETAT' => 0,
			'AO_DEPOT' => date('Y-m-d'),
			'AO_TYPEREF' => 1,
			'AO_DEVISE' => $user->getCurrency(),
			'AO_CHANGE' => 1,
			'AO_DEVISEAGENCE' => $user->getCurrency(),
			'AO_CHANGEAGENCE' => $user->getExchangeRate(),
			'ID_SOCIETE' => $user->getAgencyId(),
			'ID_PROFIL' => $user->getEmployeeId()
		];

		$opportunity = new Models\Opportunity($basicDatas);

		if($contact){
			$opportunity->contact = clone $contact;
			$opportunity->company = clone $contact->company;
			$opportunity->origin = clone $contact->origin;
			$opportunity->AO_INTERVENTION = $contact->company->expertiseAreas;
		}

		$opportunity->contact->filterFields(['ID_CRMCONTACT', 'CCON_NOM', 'CCON_PRENOM', 'company']);
		$opportunity->company->filterFields(['ID_CRMSOCIETE', 'CSOC_SOCIETE']);

		return $opportunity;
	}

	public static function delete(Models\Opportunity $entity)
	{
		$db = new Local\Opportunity();
		$db->deleteOpportunity($entity->ID_AO);

		Notification\Opportunity::getInstance($entity->ID_AO, '', [], [])->delete();

		return true;
	}


	/**
	 * get the api uri from a company id and a tab id
	 * @param int $id
	 * @param string $tab
	 * @return string
	 */
	public static function getApiUri($id, $tab = Models\Opportunity::TAB_INFORMATION){
		$tabs = Models\Opportunity::getAllTabs();
		return Tools::mapData($tab,
			array_map(function($value) use($id){
				return "/companies/$id/$value";
			}, $tabs)
		);
	}

	public static function create(Filters\Profiles\Opportunities\SaveInformation $filter){
		$sqlData = self::transformFilterDataToSQLData($filter);

		$db = new Local\Opportunity();
		$id = $db->newOpportunity($sqlData);

		if($id) return $db->getOpportunity($id, Models\Opportunity::TAB_INFORMATION);
		else return null;
	}

	/**
	 * @param Models\Opportunity[]|Models\Opportunity $opportunities
	 */
	public static function attachRights($opportunities) {
		if(!is_array($opportunities)) $opportunities = [$opportunities];

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readSpec = new HaveReadAccess(Models\Opportunity::TAB_INFORMATION);
		$writeSpec = new HaveWriteAccess(Models\Opportunity::TAB_INFORMATION);

		foreach($opportunities as $o){
			/** @var Models\Opportunity $o */
			$request->setData($o);

			$o->canReadOpportunity = $readSpec->isSatisfiedBy($request);
			$o->canWriteOpportunity = $writeSpec->isSatisfiedBy($request);
		}
	}

	public static function getRights($opportunities)
	{
		$request = new RequestAccess();
		$request->data = $opportunities;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_OPPORTUNITIES, $opportunities);

		foreach(IsActionAllowed::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Models\Opportunity::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		foreach(HaveWriteAccessOnField::getFieldsTested() as $field){
			$spec = new HaveWriteAccessOnField($field);
			$right->addField($field, $spec->isSatisfiedBy($request), $spec->isSatisfiedBy($request));
		}

		return $right;
	}
}
