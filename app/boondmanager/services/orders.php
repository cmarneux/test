<?php
/**
 * orders.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Orders\Filters\SearchOrders;
use BoondManager\APIs\Orders\Specifications\HaveReadAccess;
use BoondManager\APIs\Orders\Specifications\HaveWriteAccess;
use BoondManager\APIs\Orders\Specifications\HaveWriteAccessOnField;
use BoondManager\APIs\Orders\Specifications\IsActionAllowed;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Order;
use BoondManager\Models;

/**
 * Class Orders
 * @package BoondManager\Models\Services
 */
class Orders{

	/**
	 * @param int $id
	 * @return Order|false
	 */
	public static function find($id) {
		$filter = new SearchOrders();
		$filter->keywords->setValue( Order::buildReference($id) );

		$sql = new Local\Order();
		$result = $sql->searchOrders($filter);

		if($result->rows) return Mapper\Order::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchOrders $filter
	 * @return \BoondManager\Models\SearchResults\Orders
	 */
	public static function search(SearchOrders $filter)
	{
		$db = Local\Order::instance();
		$searchResult = $db->searchOrders($filter);

		/** @var \BoondManager\Models\SearchResults\Orders $result */
		$result = Mapper\Order::fromSearchResult($searchResult);

		return $result;
	}

	/**
	 * Search for all not archived mensual orders:
	 *  | associated with the list of projects available for a resource on period
	 *  | not yet correlated with the list of projects
	 *
	 * v6 > TpsFrsAbs->getBDCMensuelPrj
	 *
	 * @param $resourceId
	 * @param $projectIds
	 * @param $startDate
	 * @param $endDate
	 *
	 * @return \BoondManager\Models\Order[]
	 */
	public static function searchMensualOrdersForProjectsOfAResourceOnPeriod($resourceId, $projectIds, $startDate, $endDate){
		$data = Local\Order::instance()->searchMensualOrdersForProjectsOfAResourceOnPeriod($resourceId, $projectIds, $startDate, $endDate);

		return Mapper\Order::fromSearchMensualOrdersForProjectsOfAReourceOnPeriod($data);
	}

	/**
	 * @param Order[]|Order $orders
	 */
	public static function attachRights($orders) {
		if(!is_array($orders)) $orders =[$orders];

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();

		foreach($orders as $order) {
			$request->setData($order);
			$order->canReadOrder  = $readSpec->isSatisfiedBy($request);
			$order->canWriteOrder = $writeSpec->isSatisfiedBy($request);
		}
	}

	/**
	 * @param $id
	 * @param string $tab
	 * @return Order|false
	 */
	public static function get($id, $tab = BM::TAB_DEFAULT){
		$db = Local\Order::instance();
		$data = $db->getOrder($id, $tab);

		if(!$data) return false;

		$order = Mapper\Order::fromSQL($data, $tab);

		$order->calculateData();

		return $order;
	}

	/**
	 * @param $order
	 * @return Models\Rights
	 */
	public static function getRights($order)
	{
		$request = new RequestAccess();
		$request->data = $order;
		$request->user = CurrentUser::instance();

		$right = new Models\Rights(CurrentUser::instance(), BM::SUBMODULE_DELIVERY, $order);

		foreach(IsActionAllowed::ALLOWED_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach (Models\Order::getAllTabs() as $tab) {
			$readSpec = new HaveReadAccess($tab);
			$writeSpec = new HaveWriteAccess($tab);
			$right->addApi($tab, $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		foreach(HaveWriteAccessOnField::FIELDS as $field) {
			$spec = new HaveWriteAccessOnField($field);
			$right->addField($field, $spec->isSatisfiedBy($request), $spec->isSatisfiedBy($request));
		}

		return $right;
	}

}
