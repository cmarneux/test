<?php
/**
 * payments.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Payments\Filters\SearchPayments;
use BoondManager\APIs\Payments\Specifications\HaveReadAccess;
use BoondManager\APIs\Payments\Specifications\HaveWriteAccess;
use BoondManager\APIs\Payments\Specifications\IsActionAllowed;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models;
use Wish\Models\Model;
use BoondManager\Models\SearchResults;
use Wish\Tools;
use BoondManager\OldModels\Specifications;

/**
 * Class Payments
 * @package BoondManager\Models\Services
 */
class Payments{

	/**
	 * Search projects
	 * @param SearchPayments $filter
	 * @return SearchResults\Payments
	 */
	public static function search(SearchPayments $filter)
	{
		$db = new Local\Payment();
		$searchResult = $db->searchPayments($filter);

		return Mapper\Payment::fromSearchResult($searchResult);
	}

	/**
	 * loads an entity from an ID
	 * @param int $id entity ID
	 * @return \BoondManager\Models\Payment|false
	 */
	public static function get($id){
		$db = Local\Payment::instance();
		$entity = $db->getObject($id);

		if($entity) {
			self::attachMoreData($entity); // On appelle avant de mapper pour conserver les calculs fait dans la v6 avec les noms de champs privés
			$entity = Mapper\Payment::fromSQL($entity);
		} else $entity = false;
		return $entity;
	}

	/**
	 * Attach more data
	 * @param Model $entity
	 * @return Model
	 */
	private static function attachMoreData(Model $entity) {
		//On renseigne activityDetails s'il d'agit d'un achat de prestation
		if($entity['ID_MISSIONPROJET']) {
			$timesDetails = Invoices::getIntervenantsDataOnPeriod(
				$entity['ID_PROJET'],
				$entity['PMT_DEBUT'],
				$entity['PMT_FIN'],
				[
					['CORBDC_TYPE' => 0, 'ID_ITEM' => $entity['ID_MISSIONPROJET']]
				],
				true
			);
			$expensesDetails = Invoices::getFraisDataOnPeriod(
				$entity['ID_PROJET'],
				$entity['PMT_DEBUT'],
				$entity['PMT_FIN'],
				[
					['CORBDC_TYPE' => 0, 'ID_ITEM' => $entity['ID_MISSIONPROJET']]
				],
				false
			);

			//On construit le tableau de détail pour les achats de prestation
			$tabDetails = array();
			foreach ($timesDetails['LISTE_TEMPSNORMAUX'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = [
						'id' => $mission['ID_PROFIL'],
						'lastName' => $mission['PROFIL_NOM'],
						'firstName' =>  $mission['PROFIL_PRENOM'],
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' =>  $mission['MP_CJM'],
					];

				$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['NB_JRSOUVRES'] * $mission['MP_CJM'];
				$tabDetails[$mission['ID_PROFIL']]['numberOfWorkingDays'] += $mission['NB_JRSOUVRES'];
			}

			foreach ($timesDetails['LISTE_TEMPSEXCEPTION'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = [
						'lastName' => $mission['PROFIL_NOM'],
						'firstName' =>  $mission['PROFIL_PRENOM'],
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' =>  $mission['MP_CJM'],
					];

				$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['MP_CJM'];
			}

			foreach ($expensesDetails['LISTE_FRAISREEL'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = [
						'lastName' => $mission['PROFIL_NOM'],
						'firstName' =>  $mission['PROFIL_PRENOM'],
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' =>  $mission['MP_CJM'],
					];

				if ($mission['FRAISREEL_TYPEFRSREF'] == 0)
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAISREEL_MONTANT'] * $mission['LISTEFRAIS_BKMVALUE'];
				else
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAISREEL_MONTANT'];
			}

			foreach ($expensesDetails['LISTE_FRAISFORFAIT'] as $mission) {
				if (!isset($tabDetails[$mission['ID_PROFIL']]))
					$tabDetails[$mission['ID_PROFIL']] = [
						'lastName' => $mission['PROFIL_NOM'],
						'firstName' =>  $mission['PROFIL_PRENOM'],
						'costsProduction' =>  0,
						'numberOfWorkingDays' =>  0,
						'averageDailyCost' =>  $mission['MP_CJM'],
					];

				if ($mission['LIGNEFRAIS_TYPEFRSREF'] == 0)
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAIS_VALUE'] * $mission['LISTEFRAIS_BKMVALUE'];
				else
					$tabDetails[$mission['ID_PROFIL']]['costsProduction'] += $mission['FRAIS_VALUE'];
			}

			//On construit le lien vers les PDF projets
			$tpsBDD = Local\TimesReport::instance();
			$frsBDD = Local\ExpensesReport::instance();

			$nbMensualites = Invoices::getNbMensualite($entity['PMT_DEBUT'], $entity['PMT_FIN']);
			$dateMisMensualites = explode('-', $entity['PMT_DEBUT']);
			foreach ($tabDetails as $id_ressource => $detail) {
				$tabDetails[$id_ressource]['numberOfWorkingDays'] = round($detail['numberOfWorkingDays'], 2);
				$tabDetails[$id_ressource]['costsProduction'] = floatval($detail['costsProduction']);
				$tabDetails[$id_ressource]['averageDailyCost'] = floatval($detail['averageDailyCost']);

				$timesReports = array();
				$expensesReports = array();
				for ($j = 1; $j <= $nbMensualites; $j++) {
					$dateDocument = Invoices::getNewDateMens($dateMisMensualites[0] . '-' . $dateMisMensualites[1] . '-01', $j);
					if ($timesReportId = $tpsBDD->isTempsExist($dateDocument, $id_ressource)) $timesReports[] = ['id' => $timesReportId, 'term' => (new \DateTime($dateDocument))->format('Y-m')];
					if ($expensesReportId = $frsBDD->isFraisExist($dateDocument, $id_ressource)) $expensesReports[] = ['id' => $expensesReportId, 'term' => (new \DateTime($dateDocument))->format('Y-m')];
				}
				$tabDetails[$id_ressource]['timesReports'] = $timesReports;
				$tabDetails[$id_ressource]['expensesReports'] = $expensesReports;
			}
			$entity['activityDetails'] = array_values(array_map(function ($resource){return ['resource' => $resource];},$tabDetails));
		}
	}

	/**
	 * Create an empty payment for creation.
	 *
	 * @return \BoondManager\Models\Payment
	 */
	public static function getNew(\BoondManager\APIs\Payments\Filters\Payment $filter)
	{
		$purchase = (new MySQL\Mappers\Purchase)->map((new \BoondManager\Databases\Local\Purchase)->getPurchase($filter->purchase->getValue(), \BoondManager\Models\Purchase::TAB_INFORMATION));

		if($purchase->ACHAT_TYPE != BM::SUBSCRIPTION_TYPE_UNITARY) {
			$startDate = (new \DateTime($filter->term->isDefined() ? $filter->term->getValue() : $purchase->ACHAT_DATE))
				->modify('first day of this month')->format('Y-m-d');
			$endDate = $startDate
				->modify('+'.(Invoices::getNbMensualiteAbonnement($purchase->ACHAT_TYPE) - 1).' month')
				->modify('last day of this month')->format('Y-m-d');
			$pmtDate = $endDate;
		} else {
			$pmtDate = $startDate = $endDate = $purchase->ACHAT_DATE;
		}

		$entity = new \BoondManager\Models\Payment([
			'ID_PAIEMENT' => 0,
			'PMT_DATE' => $pmtDate,
			'PMT_ETAT' => \BoondManager\Models\Payment::STATE_PLANNED,
			'PMT_TYPEPAYMENT' => $purchase->ACHAT_TYPEPAYMENT,
			'PMT_TAUXTVA' => $purchase->ACHAT_TAUXTVA,
			'PMT_DEBUT' => $startDate,
			'PMT_FIN' => $endDate,
			'purchase' => $purchase,
		]);

		return (new MySQL\Mappers\Payment)->map($entity);
	}

	/**
	 * create a new payment
	 * @param \BoondManager\APIs\Payments\Filters\Payment $filter
	 * @internal param array $data
	 */
	public static function post(\BoondManager\APIs\Payments\Filters\Payment $filter)
	{
		self::performQualityCheckOnData(self::getNew($filter), $filter);

		$sqlData = self::transformFilterDataToSQLData($filter);
		$db = new Local\Payment();
		$id = $db->setObject($sqlData);
		if($id) return $id ? (new MySQL\Mappers\Payment)->map($db->getObject($id)):null;
	}

	public static function put(\BoondManager\Models\Payment &$entity, \BoondManager\APIs\Payments\Filters\Payment $filter)
	{
		self::performQualityCheckOnData($entity, $filter);
		$sqlData = self::transformFilterDataToSQLData($filter);

		$db = new Local\Payment();
		$db->setObject($sqlData, $entity->ID_PAIEMENT);

		$entity = (new MySQL\Mappers\Payment)->map($db->getObject($entity->ID_PAIEMENT));
		return $entity;
	}

	public static function delete(\BoondManager\Models\Payment $entity)
	{
		$db = new Local\Payment();

		if($db->canDeleteObject($entity->ID_PAIEMENT)) {
			return $db->deleteObject($entity->ID_PAIEMENT);

			//~ Notification\Payment::getInstance($entity->ID_PAIEMENT, '', [], [])->delete();
			//~ return true;
		}else{
			return false;
		}
	}

	/**
	 * perform some operations on a new or updated payment
	 * @param \BoondManager\Models\Payment $oldData
	 * @param AbstractJsonAPI $newData
	 * @throws \Exception
	 */
	private static function performQualityCheckOnData(\BoondManager\Models\Payment $oldData, \BoondManager\APIs\Payments\Filters\Payment $newData)
	{
	}

	/**
	 * @param \BoondManager\APIs\Payments\Filters\Payment $filter
	 * @return array
	 */
	private static function transformFilterDataToSQLData(\BoondManager\APIs\Payments\Filters\Payment $filter){

		$data = $filter->getValue();
		$data = $data['attributes'];

		$sampleEntity = new \BoondManager\Models\Payment();

		$sqlData = array_merge(
			Tools::reversePublicFieldsToPrivate( $sampleEntity->getPublicFieldsMapping(), $data),
			Tools::reversePublicFieldsToPrivate( $sampleEntity->getRelationshipMapping(), $filter->relationships->getValue())
		);
		return $sqlData;
	}

	public static function getRights($action) {

		$request = new RequestAccess($action);

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_PURCHASES, $action);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $a) {
			$spec = new IsActionAllowed($a);
			$right->addAction($a, $spec->isSatisfiedBy($request));
		}

		$right->addApi('entity', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}

}
