<?php
/**
 * poles.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Models;
use Wish\Filters\AbstractJsonAPI;

class Poles{

	/**
	 * Get pole
	 * @param int $id
	 * @return Models\Pole
	 */
	public static function get($id) {
		$db = Local\Pole::instance();
		$dbData = $db->getPole($id);

		$pole = false;
		if($dbData) $pole = Mapper\Pole::fromSQL($dbData);
		return $pole;
	}

	/**
	 * update pole
	 * @param Models\Pole $pole
	 * @return boolean
	 */
	public static function update(Models\Pole &$pole) {
		// transform pole to sqlData
		$sqlData = Mapper\Pole::toSQL($pole);

		$db = Local\Pole::instance();
		return $db->updatePole($pole->id, $sqlData);
	}

	/**
	 * Create pole
	 * @param Models\Pole $pole
	 * @return boolean
	 */
	public static function create(Models\Pole &$pole) {
		//transform pole to sqlData
		$sqlData = Mapper\Pole::toSQL($pole);

		$db = Local\Pole::instance();
		$pole->id = $db->createPole($sqlData);
		if($pole->id) {
			$pole = self::get($pole->id);
			return true;
		} else return false;
	}

	/**
	 * Delete pole
	 * @param int $id
	 * @return boolean
	 */
	public static function delete($id) {
		$db = Local\Pole::instance();
		return $db->isPoleReducible($id) && $db->deletePole($id);
	}

	/**
	 * Build pole from filter
	 * @param AbstractJsonAPI $filter
	 * @param Models\Pole|null $pole
	 * @return Models\Pole
	 */
	public static function buildFromFilter($filter, Models\Pole $pole = null) {
		if(!$pole) $pole = new Models\Pole();

		//filter and calculate all pole data
		$pole->fromArray($filter->get('attributes')->getValue());

		return $pole;
	}

	/**
	 * @param $id
	 * @return Models\Pole|null
	 */
	public static function getBasic($id){
		$db = Local\Pole::instance();
		$data = $db->getBasicPole($id);

		return ($data) ? Mapper\Pole::fromSQL($data) : null;
	}

	/**
	 * @return Models\Pole[]
	 */
	public static function getAllBasic(){
		$db = Local\Pole::instance();
		$data = $db->getAllBasicPoles();

		return Mapper\Pole::fromDatabaseArray($data, Models\Pole::class);
	}

	/**
	 * @param int[] $ids get all the following poles. if none given, all poles from the database will be returned
	 * @return Models\Pole[]
	 */
	public static function getAllPoles($ids = []){
		$db = Local\Pole::instance();
		$data = $db->getAllPoles();

		if($ids){
			$data = array_filter($data, function($v) use ($ids){
				return in_array($v['ID_POLE'], $ids);
			});
		}

		return Mapper\Pole::fromDatabaseArray($data, Models\Pole::class);
	}
}
