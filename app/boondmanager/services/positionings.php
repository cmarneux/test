<?php
/**
 * positionings.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Positionings\Filters\SearchPositionings;
use BoondManager\APIs\Positionings\Specifications\HaveReadAccess;
use BoondManager\APIs\Positionings\Specifications\HaveWriteAccess;
use BoondManager\APIs\Positionings\Specifications\IsActionAllowed;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\OldModels\Filters;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models;
use BoondManager\APIs\Positionings\Specifications\MyActor;
use BoondManager\APIs\Positionings\Specifications\MyCRM;
use BoondManager\APIs\Positionings\Specifications\MyOpportunity;
use BoondManager\Models\Positioning;
use Wish\Models\SearchResult;
use Wish\Tools;
use Wish\Filters\AbstractJsonAPI;

/**
 * Class Positionings
 * @package BoondManager\Models\Services
 */
class Positionings{

	/**
	 * @param int $id
	 * @return Positioning|false
	 */
	public static function find($id) {
		$filter = new SearchPositionings();
		$filter->keywords->setValue( Positioning::buildReference($id) );

		$sql = new Local\Positioning();
		$result = $sql->searchPositionings($filter);

		if($result->rows) return Mapper\Positioning::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * Handle the research MySQL
	 *
	 * @param SearchPositionings $filter
	 * @return SearchResult
	 */
	public static function search(SearchPositionings $filter)
	{
		$db = Local\Positioning::instance();
		$result = $db->searchPositionings($filter);

		$result = Mapper\Positioning::fromSearchResult($result);

		/**
		 * creating some variable to filter datas
		 */
		$request = new RequestAccess();
		$request->setUser( CurrentUser::instance() );

		$isMyCRM = new MyCRM();
		$isMyActor = new MyActor();
		$isMyOpportunity = new MyOpportunity();


		foreach($result->rows as $row){
			/** @var Positioning $row */
			$request->setData($row);
			$row->canReadItem = $row->canReadContact = $row->canReadCompany = $row->canReadOpportunity = false;
			if(!$isMyActor->isSatisfiedBy($request))
				unset($row->dependsOn);
			else $row->canReadContact = true;
			if(!$isMyCRM->isSatisfiedBy($request)) {
				unset($row->opportunity->company, $row->opportunity->contact);
			} else $row->canReadCompany = $row->canReadContact = true;
			if(!$isMyOpportunity->isSatisfiedBy($request)) {
				unset($row->opportunity);
			} else $row->canReadOpportunity = true;
		}

		return $result;
	}

	/**
	 * loads an entity from an ID
	 * @param int $id entity ID
	 * @return \BoondManager\Models\Positioning|false
	 */
	public static function get($id){
		$db = new Local\Positioning();
		$data = $db->getObject($id);

		return $data ? Mapper\Positioning::fromSQL($data) : false;
	}

	/**
	 * Create an empty positioning for creation.
	 *
	 * @return \BoondManager\Models\Positioning
	 */
	public static function getNew(){
		$user = CurrentUser::instance();
		$object = new \BoondManager\Models\Positioning([
			//~ 'ID_POSITIONNEMENT'	=> null,
			//~ 'ID_AO'	=> 0,
			//~ 'ID_PROJET'	=> 0,
			'POS_DATE'	=> date('Y-m-d H:i:s', time()),
			//~ 'POS_ETAT'	=> MySQL\RowObject\Positioning::STATE_ACTIVATED,
			//~ 'POS_COMMENTAIRE'	=> '',
			'ID_RESPPROFIL'     => $user->getEmployeeId(),
			//~ 'PARAM_DEVISEAGENCE' => $user->get('devise.value'),
			//~ 'PARAM_CHANGEAGENCE' => $user->get('devise.tauxchange'),
			//~ 'PARAM_DEVISE'       => $user->get('devise.value'),
			//~ 'PARAM_CHANGE'       => 1,
			//~ 'ID_RESPRH'          => $user->id,
			//~ 'ID_SOCIETE'         => $user->get('company.id'),
			//~ 'PROFIL_CIVILITE'    => MySQL\RowObject\Resource::CIV_MR,
			//~ 'PROFIL_ALLOWCHANGE' => false,
			//~ 'PROFIL_TYPE'        => MySQL\RowObject\Resource::TYPE_RESOURCE,
			//~ 'PROFIL_VISIBILITE'  => true,
			//~ 'PROFIL_TYPEDISPO'   => MySQL\RowObject\Resource::DISPO_ASAP,
			//~ 'PROFIL_PAYS'        => $user->getCountry()
		]);
		return $object;
	}

	/**
	 * create a new positioning
	 * @param Filters\Profiles\Positionings\SaveInformation $filter
	 * @return int positioning ID
	 * @internal param array $data
	 */
	public static function post(Filters\Profiles\Positionings\SaveInformation $filter, $solr = false)
	{
		self::performQualityCheckOnData(self::getNew(), $filter);

		$sqlData = self::transformFilterDataToSQLData($filter);
		$db = new Local\Positioning();
		$id = $db->postObject($sqlData);

		if($id) {
			switch($filter->dependsOn->getType()){// cf. ce qui est commenté dans mysql\local\positioning->postObject
				case 'candidate':
					(new Candidate())->updateObject([], $filter->dependsOn->getValue(), 0, $solr);
					break;
				case 'resource':
					(new Resource())->setObject([], $filter->dependsOn->getValue(), 0, $solr);
					break;
				case 'product':
					//~ XXX : Tin : Pas de notification ?
					break;
			}
			return $db->getObject($id);
		}
	}

	public static function put(\BoondManager\Models\Positioning &$entity, AbstractJsonAPI $filter)
	{
		self::performQualityCheckOnData($entity, $filter);
		$sqlData = self::transformFilterDataToSQLData($filter);

		$db = new Local\Positioning();
		$db->putObject($sqlData, $entity->id);

		if($id) {
			//~ dependsOn doit être défini. C'est normalement le cas car sa valeur par défaut prend la valeur en base. (cf. $filter->setDefaultValues( $entity->encode() ); dans le controleur)
			switch($filter->dependsOn->getType()){// cf. ce qui est commenté dans mysql\local\positioning->putObject
				case 'candidate':
					(new Candidate())->updateObject([], $filter->dependsOn->getValue(), 0, $solr);
					break;
				case 'resource':
					(new Resource())->setObject([], $filter->dependsOn->getValue(), 0, $solr);
					break;
				case 'product':
					//~ XXX : Tin : Pas de notification ?
					break;
			}
			return $db->getObject($id);
		}

		$entity = $db->getObject($entity->id);

		return $entity;
	}

	public static function delete(\BoondManager\Models\Positioning $entity)
	{
		$db = new Local\Positioning();
		return $db->deleteObject($entity->id);
	}

	/**
	 * perform some operations on a new or updated positioning
	 * @param \BoondManager\Models\Positioning $oldData
	 * @param AbstractJsonAPI $newData
	 * @throws \Exception
	 */
	private static function performQualityCheckOnData(\BoondManager\Models\Positioning $oldData, AbstractJsonAPI $newData)
	{
		//~ if($oldData->POS_ETAT != 2 && $newData->state->getValue() == 3)
		//~ TODO : Tin : Vérifier les règles des specs ici ? https://wishgroupe.atlassian.net/wiki/pages/viewpage.action?pageId=63307867
	}

	/**
	 * @param Filters\Profiles\Positionings\SaveInformation $filter
	 * @return array
	 */
	private static function transformFilterDataToSQLData(Filters\Profiles\Positionings\SaveInformation $filter){

		$data = $filter->getValue();
		$data = $data['attributes'];

		$sampleEntity = new \BoondManager\Models\Positioning();

		$sqlData = [];
		$sqlData['POSITIONNEMENT'] = array_merge(
			Tools::reversePublicFieldsToPrivate( $sampleEntity->getPublicFieldsMapping(), $data),
			Tools::reversePublicFieldsToPrivate( $sampleEntity->getRelationshipMapping(), $filter->relationships->getValue())
		);
		if($filter->additionalTurnoverAndCosts->isDefined()){
			$sqlData['POSITIONDETAILS'] = [];
			foreach($filter->additionalTurnoverAndCosts->getValue() as $subData){
				$sqlData['POSITIONDETAILS'][] = Tools::reversePublicFieldsToPrivate(\BoondManager\Models\PositioningDetail::getPublicFieldsMapping(), $subData);
			}
		}

		$sqlData['POSITIONNEMENT']['ITEM_TYPE'] = $filter->dependsOn->getType() == 'product' ? Positioning::ENTITY_PRODUCT_KEY : Positioning::ENTITY_PROFILE_KEY;
		if(in_array($filter->opportunity->AO_TYPE, [BM::PROJECT_TYPE_RECRUITMENT, BM::PROJECT_TYPE_PRODUCT]))
			$sqlData['POSITIONNEMENT']['POS_CJM'] = $sqlData['POSITIONNEMENT']['POS_NBJRSGRATUIT'] = 0;
		if($filter->opportunity->AO_TYPE == BM::PROJECT_TYPE_RECRUITMENT) $sqlData['POSITIONNEMENT']['POS_NBJRSFACTURE'] = 1;

		//~ On calcule ici la somme des CA et Coûts additionnels
		$additionalTurnover = $additionalCost = 0;
		foreach($filter->additionalTurnoverAndCosts->getValue() as $additional){
			$additionalTurnover += $additional['turnoverExcludingTax'];// XXX : Tin : dommage qu'on ne puisse pas y accéder de cette manière: $additional->turnoverExcludingTax
			$additionalCost += $additional['costsExcludingTax'];
		}
		$sqlData['POSITIONNEMENT']['POS_TARIFADDITIONNEL'] = $additionalTurnover;
		$sqlData['POSITIONNEMENT']['POS_INVESTISSEMENT'] = $additionalCost;

		return $sqlData;
	}

	public static function getRights($action) {

		$request = new RequestAccess($action);

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_PURCHASES, $action);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $a) {
			$spec = new IsActionAllowed($a);
			$right->addAction($a, $spec->isSatisfiedBy($request));
		}

		$right->addApi('entity', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		$right->addApi('attachedFlags', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
	}

}
