<?php
/**
 * products.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Products\Filters\Information;
use BoondManager\APIs\Products\Filters\SearchProducts;
use BoondManager\APIs\Products\Specifications\IsActionAllowed;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Models\Product;
use BoondManager\Models;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications;
use BoondManager\APIs\Products\Specifications\HaveReadAccess;
use BoondManager\APIs\Products\Specifications\HaveWriteAccess;
use Wish\Models\SearchResult;
use Wish\Tools;

/**
 * Class Products
 * @package BoondManager\Models\Services
 */
class Products{
	const RIGHTS_ACTIONS = ['share', 'addOpportunity', 'addProject'];

	/**
	 * @param int $id
	 * @return Product|false
	 */
	public static function find($id) {
		$filter = new SearchProducts();
		$filter->keywords->setValue( Product::buildReference($id) );

		$sql = new Local\Product();
		$result = $sql->searchProducts($filter);

		if($result->rows) return Mapper\Product::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * loads an entity from an ID
	 * @param int $id entity ID
	 * @param int $tab sub part of the entity
	 * @return Product|false
	 */
	public static function get($id, $tab = Product::TAB_DEFAULT){
		$db = Local\Product::instance();
		$entity = $db->getObject($id, $tab);
		return ($entity) ? Mapper\Product::fromSQL($entity, $tab) : null;
	}

	/**
	 * Create an empty entity for creation.
	 *
	 * @return Product
	 */
	public static function getNew(){
		$user = CurrentUser::instance();
		$object = new Product([
			'id'                 => 0,
			'currencyAgency'     => $user->getCurrency(),
			'exchangeRateAgency' => $user->getExchangeRate(),
			'currency'           => $user->getCurrency(),
			'exchangeRate'       => 1,
			'state'              => Product::STATE_ACTIVATED,
			'subscription'       => BM::SUBSCRIPTION_TYPE_UNITARY,
			'taxRate'            => 20,
			'mainManager'        => Managers::getBasic($user->getEmployeeId()),
			'agency'             => Agencies::getBasic($user->getAgencyId()),
			'pole'               => Poles::getBasic($user->getPoleId()),
		]);
		return $object;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Product $entity
	 * @return array
	 */
	public static function getVisibleTabs(Product $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Product::getAllTabs();
		$readSpec = new HaveReadAccess(Product::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get a list of available actions
	 * @param Product $entity
	 * @return array
	 */
	public static function getAvailableActions(Product $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$actions = [
			'createOpportunity' => (new HaveWriteAccess(Product::TAB_OPPORTUNITIES))->isSatisfiedBy($request),
			'createProject' => (new HaveWriteAccess(Product::TAB_PROJECTS))->isSatisfiedBy($request),
			'share' => true,
		];

		return $actions;
	}

	/**
	 * get a list of tab's id that can be shown for the entity
	 * @param Product $entity
	 * @return array
	 */
	public static function getWritableTabs(Product $entity)
	{
		$request = new RequestAccess();
		$request->data = $entity;
		$request->user = CurrentUser::instance();

		$tabs = Product::getAllTabs();
		$readSpec = new HaveWriteAccess(Product::TAB_INFORMATION);
		$tabs = array_filter($tabs, function ($id) use ($readSpec, $request) {
			$readSpec->setTab($id);
			return $readSpec->isSatisfiedBy($request);
		});

		return $tabs;
	}

	/**
	 * get the api uri from a product id and a tab id
	 * @param int $id
	 * @param int $tabId
	 * @return string
	 */
	public static function getApiUri($id, $tabId = Product::TAB_INFORMATION){
		$prefix = '/products/'.$id;
		return Tools::mapData($tabId,[
			Product::TAB_INFORMATION   => $prefix.'/information',
			Product::TAB_ACTIONS       => $prefix.'/actions',
			Product::TAB_OPPORTUNITIES => $prefix.'/opportunities',
			Product::TAB_PROJECTS      => $prefix.'/projects'
		]);
	}

	/**
	 * Search projects
	 * @param SearchProducts $filter
	 * @return SearchResult
	 */
	public static function search(SearchProducts $filter)
	{
		$db = new Local\Product();
		$searchResult = $db->searchProducts($filter);

		return Mapper\Product::fromSearchResult($searchResult);
	}

	public static function update(Product &$entity, $tab = Product::TAB_DEFAULT)
	{
		$sqlData = Mapper\Product::toSQL($entity, $tab);

		$db = Local\Product::instance();
		if( $db->updateObject($sqlData, $entity->id) ) {
			$entity = self::get($entity->id, Product::TAB_INFORMATION);
			return true;
		}else return false;
	}

	/**
	 * @param Information $filter
	 * @param Product $product
	 * @return Product
	 */
	public static function buildFromFilter(Information $filter, Product $product = null) {
		if(!$product) $product = self::getNew();

		return $product->mergeWith($filter->toObject());
	}

	/**
	 * @param Product $entity
	 * @return bool
	 */
	public static function delete(Product $entity)
	{
		$db = Local\Product::instance();
		if($db->isProductReducible($entity->id)) {
			return $db->deleteObject($entity->id);
		}else return false;
	}

	/**
	 * @param Product $product
	 * @return Models\Rights
	 */
	public static function getRights($product)
	{
		$request = new RequestAccess();
		$request->data = $product;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_PRODUCTS, $product);

		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Product::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		return $right;
	}

	/**
	 * @param $product
	 * @return bool
	 */
	public static function create(&$product){
		$sqlData = Mapper\Product::toSQL($product, Product::TAB_INFORMATION);

		$db = Local\Product::instance();

		if($id = $db->newObject( $sqlData )) {
			$product = self::get($id, Product::TAB_INFORMATION);
			return true;
		} else {
			return false;
		}
	}
}
