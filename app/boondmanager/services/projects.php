<?php
/**
 * projects.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;

use BoondManager\APIs\Projects\Filters\SearchProjects;
use BoondManager\APIs\Projects\Specifications\HaveReadAccess;
use BoondManager\Models\Inactivity;
use BoondManager\Models\SearchResults\Orders;
use BoondManager\Models\TurnoverState;
use Wish\Filters\AbstractJsonAPI;
use Wish\Tools;
use Wish\Models\SearchResult;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\Rights;
use BoondManager\APIs\Projects\Specifications\HaveWriteAccess;
use BoondManager\APIs\Projects\Specifications\IsActionAllowed;
use BoondManager\Databases\Mapper;
use BoondManager\Databases\Local;
use BoondManager\Models\Project;
use BoondManager\Models\Agency;
use BoondManager\Models\Delivery;
use BoondManager\Models\Product;

/**
 * Class Projects
 * @package BoondManager\Models\Services
 */
class Projects {
	const RIGHTS_ACTIONS = ['share', 'addAction', 'addPositioning', 'addPurchase', 'addAdvantage', 'addOrder', 'seeActivityReports', 'showBatchesMarkersTab', 'showAdvantagesTab', 'showPurchasesTab'];

	/**
	 * @param int $id
	 * @return Project|false
	 */
	public static function find($id) {
		$filter = new SearchProjects();
		$filter->keywords->setValue( Project::buildReference($id) );

		$sql = new Local\Project();
		$result = $sql->searchProjects($filter);

		if($result->rows) return Mapper\Project::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * Search projects
	 * @param SearchProjects $filter
	 * @return SearchResult
	 */
	public static function search(SearchProjects $filter) {
		$db = new Local\Project();
		$searchResult = $db->searchProjects($filter);

		return Mapper\Project::fromSearchResult($searchResult);
	}

	/**
	 * Get project
	 * @param int $id
	 * @param string $tab
	 * @return Project
	 */
	public static function get($id, $tab = BM::TAB_DEFAULT) {
		$db = Local\Project::instance();
		$dbData = $db->getProject($id, $tab);

		$project = false;
		if($dbData) $project = Mapper\Project::fromSQL($dbData, $tab);
		return $project;
	}

	/**
	 * update project
	 * @param Project $project
	 * @param string $tab
	 * @return boolean
	 */
	public static function update(Project &$project, $tab = BM::TAB_DEFAULT) {

		// transform project to sqlData
		$sqlData = Mapper\Project::toSQL($project, $tab);

		$db = Local\Project::instance();

		if($db->updateProject($project->id, $sqlData)) {
			//If tab uses array attributes we have to call again GET method in order to get back full object
			if(in_array($tab, [Project::TAB_INFORMATION, Project::TAB_BATCHES_MARKERS, Project::TAB_SIMULATION]))
				$project = self::get($project->id, $tab);

			//TODO : Manage notifications
			//Notification\Project::getInstance($project->id, $tab, $oldValues, $newValues)->update();
			return true;
		} else return false;
	}

	/**
	 * Create project
	 * @param Project $project
	 * @return boolean
	 */
	public static function create(Project &$project) {
		//transform project to sqlData
		$sqlData = Mapper\Project::toSQL($project, Project::TAB_INFORMATION);

		$db = new Local\Project();
		$project->id = $db->createProject($sqlData);
		if($project->id) {
			$project = self::get($project->id, Project::TAB_INFORMATION);
			return true;
		} else return false;
	}

	/**
	 * Delete project
	 * @param int $id
	 * @return boolean
	 */
	public static function delete($id) {
		$db = new Local\Project();

		if($db->isProjectReducible($id) && $db->deleteProject($id)) {
			//TODO : Manage notifications
			//Notification\Project::getInstance($id, '', [], [])->delete();
			return true;
		}
		return false;
	}

	/**
	 * Build project from filter
	 * @param AbstractJsonAPI $filter
	 * @param Project|null $project
	 * @return Project
	 */
	public static function buildFromFilter($filter, Project $project = null) {
		if(!$project) $project = new Project();

		return $project->mergeWith($filter->toObject());
	}

	/**
	 * @param Project $project
	 * @return bool
	 */
	public static function attachAgencyConfig(Project $project){
		$db = new Local\Agency();

		$dbAgency = $db->getAgency($project->agency->getID(), Agency::TAB_PROJECTS);
		if($dbAgency) {
			$project->agency = Mapper\Agency::fromSQL($dbAgency, Agency::TAB_PROJECTS);
			return true;
		}
		return false;
	}

	/**
	 * @param Project $project
	 * @param Delivery[] $deliveries
	 * @return array with the deliveries updated
	 */
	public static function attachDeliveriesProductivity(Project $project, $deliveries) {
		foreach($deliveries as $i => $searchDelivery) {

			$deliveries[$i]->regularTimesSimulated = 0;
			$deliveries[$i]->expensesSimulated = 0;
			$deliveries[$i]->regularTimesProduction = 0;
			$deliveries[$i]->expensesProduction = 0;
			$deliveries[$i]->exceptionalTimesProduction = 0;
			$deliveries[$i]->exceptionalCalendarsProduction = 0;
			$deliveries[$i]->costsProductionExcludingTax = 0;
			$deliveries[$i]->turnoverProductionExcludingTax = 0;

			foreach($project->deliveries as $projectDelivery) {
				/**
				 * @var Delivery $projectDelivery
				 */
				if($searchDelivery->id == $projectDelivery->id) {
					// FIXME
					if(!$searchDelivery->isGroupment && $searchDelivery->state != Delivery::STATE_FORECAST) {
						$nbTerms = Tools::getNumberOfTerms($searchDelivery->startDate, $searchDelivery->endDate);
						$deliveries[$i]->regularTimesSimulated = $searchDelivery->numberOfDaysInvoicedOrQuantity + $searchDelivery->numberOfDaysFree;
						$deliveries[$i]->expensesSimulated = $deliveries[$i]->regularTimesSimulated * $searchDelivery->dailyExpenses +  $searchDelivery->monthlyExpenses * $nbTerms;
					}
					$deliveries[$i]->regularTimesProduction = $projectDelivery->regularTimesProduction;
					$deliveries[$i]->expensesProduction = $projectDelivery->expensesProduction;
					$deliveries[$i]->exceptionalTimesProduction = $projectDelivery->exceptionalTimesProduction;
					$deliveries[$i]->exceptionalCalendarsProduction = $projectDelivery->exceptionalCalendarsProduction;
					$deliveries[$i]->costsProductionExcludingTax = $projectDelivery->costsProductionExcludingTax;
					$deliveries[$i]->turnoverProductionExcludingTax = $projectDelivery->turnoverProductionExcludingTax;
					break;
				}
			}
		}
		return $deliveries;
	}

	/**
	 * @param Delivery[] $deliveries
	 * @return array with the following datas
	 *
	 * 0 => $turnoverSignedExcludingTax,
	 * 1 => $costsSignedExcludingTax,
	 * 2 => $marginSignedExcludingTax, $profitabilitySigned, $turnoverForecastExcludingTax, $costsForecastExcludingTax, $marginForecastExcludingTax, $profitabilityForecast];
	 */
	public static function calculateFinancialResumeForDeliveries($deliveries){
		$turnoverSignedExcludingTax = 0;
		$costsSignedExcludingTax = 0;
		$turnoverForecastExcludingTax = 0;
		$costsForecastExcludingTax = 0;

		foreach($deliveries as $delivery) {
			/**
			 * @var Delivery $delivery
			 */
			if ($delivery->state == Delivery::STATE_FORECAST) {
				$turnoverForecastExcludingTax += $delivery->turnoverSimulatedExcludingTax;
				$costsForecastExcludingTax += $delivery->costsSimulatedExcludingTax;
			} else {
				$turnoverSignedExcludingTax += $delivery->turnoverSimulatedExcludingTax;
				$costsSignedExcludingTax += $delivery->costsSimulatedExcludingTax;
			}
		}

		$marginSignedExcludingTax = $turnoverSignedExcludingTax - $costsSignedExcludingTax;
		if(BM::isProfitabilityCalculatingBasedOnMarginRate())
			$profitabilitySigned = $costsSignedExcludingTax == 0 ? 0 : 100 * $marginSignedExcludingTax / $costsSignedExcludingTax;
		else
			$profitabilitySigned = $turnoverSignedExcludingTax == 0 ? 0 : 100 * $marginSignedExcludingTax / $turnoverSignedExcludingTax;

		$marginForecastExcludingTax = $turnoverForecastExcludingTax - $costsForecastExcludingTax;
		if(BM::isProfitabilityCalculatingBasedOnMarginRate())
			$profitabilityForecast = $costsForecastExcludingTax == 0 ? 0 : 100 * $marginSignedExcludingTax / $costsForecastExcludingTax;
		else
			$profitabilityForecast = $turnoverForecastExcludingTax == 0 ? 0 : 100 * $marginForecastExcludingTax / $turnoverForecastExcludingTax;

		return [$turnoverSignedExcludingTax, $costsSignedExcludingTax, $marginSignedExcludingTax, $profitabilitySigned,
				$turnoverForecastExcludingTax, $costsForecastExcludingTax, $marginForecastExcludingTax, $profitabilityForecast];
	}

	/**
	 * @param Project $project
	 * @param Delivery[] $deliveries
	 * @return array with the following datas
	 *
	 * 0 => $turnoverSignedExcludingTax,
	 * 1 => $costsSignedExcludingTax,
	 * 2 => $marginSignedExcludingTax, $profitabilitySigned, $turnoverForecastExcludingTax, $costsForecastExcludingTax, $marginForecastExcludingTax, $profitabilityForecast];
	 */
	public static function calculateFinancialResumeForProductivity(Project $project, $deliveries){
		$turnoverSignedExcludingTax = $project->additionalTurnoverExcludingTax;
		$costsSignedExcludingTax = $project->additionalCostsExcludingTax;
		$turnoverProductionExcludingTax = $project->turnoverPurchasesAndAdditionnalExcludingTax;
		$costsProductionExcludingTax = $project->costsPurchasesAndAdditionnalExcludingTax;
		$turnoverResourcesExcludingTax = 0;
		$costsResourcesExcludingTax = 0;
		$turnoverPurchasesAndAdditionnalExcludingTax = $project->turnoverPurchasesAndAdditionnalExcludingTax;
		$costsPurchasesAndAdditionnalExcludingTax = $project->costsPurchasesAndAdditionnalExcludingTax;

		foreach($deliveries as $delivery) {
			/**
			 * @var Delivery $delivery
			 */
			$deliveryTurnoverExcludingTax = $delivery->averageDailyPriceExcludingTax * $delivery->numberOfDaysInvoicedOrQuantity + $delivery->additionalTurnoverExcludingTax;
			if($delivery->isGroupment) {
				$turnoverSignedExcludingTax += $deliveryTurnoverExcludingTax;
			} else {
				if ($delivery->state != Delivery::STATE_FORECAST) {
					$costsSignedExcludingTax += $delivery->averageDailyCost * ($delivery->numberOfDaysInvoicedOrQuantity + $delivery->numberOfDaysFree) + $delivery->additionalCostsExcludingTax;

					if(!$delivery->groupment)
						$turnoverSignedExcludingTax += $deliveryTurnoverExcludingTax;

					if($delivery->dependsOn instanceof Product)
						$turnoverProductionExcludingTax += $deliveryTurnoverExcludingTax;
				}
			}

			$turnoverProductionExcludingTax += $delivery->turnoverProductionExcludingTax;
			$costsProductionExcludingTax += $delivery->costsProductionExcludingTax;

			$turnoverResourcesExcludingTax += $delivery->turnoverProductionExcludingTax;
			$costsResourcesExcludingTax += $delivery->costsProductionExcludingTax;
		}

		$marginSignedExcludingTax = $turnoverSignedExcludingTax - $costsSignedExcludingTax;
		if(BM::isProfitabilityCalculatingBasedOnMarginRate())
			$profitabilitySigned = $costsSignedExcludingTax == 0 ? 0 : 100 * $marginSignedExcludingTax / $costsSignedExcludingTax;
		else
			$profitabilitySigned = $turnoverSignedExcludingTax == 0 ? 0 : 100 * $marginSignedExcludingTax / $turnoverSignedExcludingTax;

		$marginProductionExcludingTax = $turnoverProductionExcludingTax - $costsProductionExcludingTax;
		if(BM::isProfitabilityCalculatingBasedOnMarginRate())
			$profitabilityProduction = $costsProductionExcludingTax == 0 ? 0 : 100 * $marginSignedExcludingTax / $costsProductionExcludingTax;
		else
			$profitabilityProduction = $turnoverProductionExcludingTax == 0 ? 0 : 100 * $marginProductionExcludingTax / $turnoverProductionExcludingTax;

		return [$turnoverSignedExcludingTax, $costsSignedExcludingTax, $marginSignedExcludingTax, $profitabilitySigned,
				$turnoverProductionExcludingTax, $costsProductionExcludingTax, $marginProductionExcludingTax, $profitabilityProduction,
				$turnoverResourcesExcludingTax, $costsResourcesExcludingTax,
				$turnoverPurchasesAndAdditionnalExcludingTax, $costsPurchasesAndAdditionnalExcludingTax];
	}

	/**
	 * Get project's rights
	 * @param Project $project
	 * @return Rights
	 */
	public static function getRights(Project $project) {
		$request = new RequestAccess();
		$request->data = $project;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Rights(CurrentUser::instance(), BM::MODULE_PROJECTS, $project);

		foreach(self::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Project::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		return $right;
	}

	/**
	 * Get api uri from project
	 * @param int $id project's id
	 * @param string $tab project's tab
	 * @return string
	 */
	public static function getApiUri($id, $tab = BM::TAB_DEFAULT){
		$tabs = Project::getAllTabs();
		return Tools::mapData($tab,
			array_map(function($value) use($id){
				return "/projects/$id/$value";
			}, $tabs)
		);
	}

	/**
	 * Construct a new reference for a project
	 * @param Project $project
	 * @param boolean $onlymask if true then returns only reference's mask else concatanes reference's mask and start's number
	 * @param string $forceDate if defined then uses this date to build reference's mask else uses now date
	 * @return string
	 */
	public static function buildNewReference(Project $project, $onlymask = false, $forceDate = null) {
		if(preg_match('/([a-zA-Z0-9\[\]_\-\/]{1,})/', $project->agency->projectsReferenceMask, $matchRef)) {
			if($forceDate && preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $forceDate, $match)) {
				$forceDate = explode('-',$forceDate);
				$annee4 = $forceDate[0];
				$annee2 = substr($forceDate[0],2);
				$mois = $forceDate[1];
				$jour = $forceDate[2];
			} else {
				$actualTime = mktime();
				$annee4 = date('Y',$actualTime);
				$annee2 = date('y',$actualTime);
				$mois = date('m',$actualTime);
				$jour = date('d',$actualTime);
			}

			$trigramme_manager = Tools::buildTrigramFromContact($project->mainManager->firstName, $project->mainManager->lastName);

			$tabFind = array('[AAAA]','[AA]','[MM]','[JJ]','[ID_AO]','[ID_PRJ]','[REF_AO]','[CCC]','[RRR]');
			$tabReplace = array($annee4,$annee2,$mois,$jour,$project->opportunity->getID(),$project->getID(),$project->opportunity->reference,strtoupper(substr(Tools::suppr_accents($project->company->name),0,3)),$trigramme_manager);

			$maskSelect = str_replace($tabFind, $tabReplace, $matchRef[1]);

			$tabFind[] = '_';
			$tabFind[] = '/';
			$tabFind[] = '-';

			$tabReplace[] = '\_';
			$tabReplace[] = '\/';
			$tabReplace[] = '\-';

			$maskWhere = str_replace($tabFind, $tabReplace, $matchRef[1]);//Masque de la clause Where

			$maskMax = str_replace(array('/', '-'), array('\/', '\-'), $maskSelect);

			if(strpos($matchRef[1], '[ID_PRJ]') === false) {
				$dbProject = new Local\Project();
				$maxReference = $dbProject->calculateReferenceMax($maskSelect, $maskWhere, $project->getID());
			} else
				$maxReference = $maskSelect;

			if(preg_match('/'.$maskMax.'_([0-9]{1,})/', $maxReference, $matchMax))
				$startNumber = '_'.(intval($matchMax[1])+1); else $startNumber = '';
		} else {
			$maskSelect = $maskMax = $project->getReference();
			$startNumber = '';
		}

		if($onlymask) return $maskSelect; else return $maskSelect.$startNumber;
	}

	/**
	 * @param Project $project
	 * @param Orders $searchResult
	 * @return array [
	 *      TurnoverState[],
	 *      $deltaOrderedExcludingTax,
	 *      $marginOrderedExcludingTax,
	 *      $profitabilityOrdered,
	 *      $marginInvoicedExcludingTax,
	 *      $profitabilityInvoiced,
	 *      $turnoverPaidExcludingTax,
	 *      $marginPaidExcludingTax,
	 *      $profitabilityPaid
	 * ]
	 */
	public static function calculateFinancialResumeForOrders($project, Orders $searchResult)
	{
		$invoiceDict = Dictionary::getDictTranslatedValues('specific.setting.state.invoice');
		/** @var TurnoverState[] $invoiceSummary */
		$invoiceSummary = [10 => new TurnoverState([
			'id' => 10,
			'turnoverExcludingTax' => 0
		])];
		foreach($invoiceDict as $id=>$label){
			$invoiceSummary[$id] = new TurnoverState([
				'id' => $id,
				'turnoverExcludingTax' => 0
			]);
		}

		foreach($searchResult->rows as $order){
			foreach($order->turnoverStates as $state){
				if(isset($invoiceSummary[$state->id]))
					$invoiceSummary[$state->id]->turnoverExcludingTax += $state->turnoverExcludingTax;
			}
		}

		return [
			array_values($invoiceSummary),
			$searchResult->turnoverOrderedExcludingTax - $project->turnoverSignedExcludingTax, // deltaOrderedExcludingTax
			$searchResult->turnoverOrderedExcludingTax - $project->costsProductionExcludingTax, // marginOrderedExcludingTax
			($searchResult->turnoverOrderedExcludingTax) ? 100 * ($searchResult->turnoverOrderedExcludingTax - $project->costsProductionExcludingTax) / $searchResult->turnoverOrderedExcludingTax : 0, //profitabilityOrdered
			$searchResult->turnoverInvoicedExcludingTax - $project->costsProductionExcludingTax, // marginInvoicedExcludingTax
			($searchResult->turnoverInvoicedExcludingTax) ? 100 * ($searchResult->turnoverInvoicedExcludingTax - $project->costsProductionExcludingTax) / $searchResult->turnoverInvoicedExcludingTax : 0, //profitabilityInvoiced
			(isset($invoiceSummary[3])) ? $invoiceSummary[3]->turnoverExcludingTax : 0, // $turnoverPaidExcludingTax
			(isset($invoiceSummary[3])) ? $invoiceSummary[3]->turnoverExcludingTax - $project->costsProductionExcludingTax : 0,// $marginPaidExcludingTax
			(isset($invoiceSummary[3]) && $invoiceSummary[3]->turnoverExcludingTax) ? 100 * ($invoiceSummary[3]->turnoverExcludingTax - $project->costsProductionExcludingTax) / $invoiceSummary[3]->turnoverExcludingTax : 0// $profitabilityPaid
		];
	}

	/**
	 * @param $result Deliveries data
	 */
	public static function rebindProjectDatas($result)
	{
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readAccess = new HaveReadAccess(Project::TAB_INFORMATION);
		$writeAccess = new HaveWriteAccess(Project::TAB_INFORMATION);

		foreach($result->rows as $row){
			/** @var Delivery|Inactivity $row */
			$request->setData($row->project);
			$row->canReadProject = $row->canReadDelivery = $readAccess->isSatisfiedBy($request);
			$row->canWriteProject = $row->canWriteDelivery = $writeAccess->isSatisfiedBy($request);
		}
	}

	/**
	 * @param Project[]|Project $rows
	 */
	public static function attachRights($rows)
	{
		if(!is_array($rows)) $rows = [$rows];

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readAccess = new HaveReadAccess(Project::TAB_INFORMATION);
		$writeAccess = new HaveWriteAccess(Project::TAB_INFORMATION);

		foreach($rows as $row){
			/** @var Delivery $row */
			$request->setData($row->project);
			$row->canReadProject = $readAccess->isSatisfiedBy($request);
			$row->canWriteProject = $writeAccess->isSatisfiedBy($request);
		}
	}
}
