<?php
/**
 * purchases.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\Purchases\Filters\Information;
use BoondManager\APIs\Purchases\Filters\SearchPurchases;
use BoondManager\APIs\Purchases\Specifications\HaveReadAccess;
use BoondManager\APIs\Purchases\Specifications\HaveWriteAccess;
use BoondManager\APIs\Purchases\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use Wish\Tools;
use Wish\Models\SearchResult;
use BoondManager\Models;
use BoondManager\Models\Purchase;
use BoondManager\Databases\Mapper;
use BoondManager\Databases\Local;

/**
 * Class Purchases
 * @package BoondManager\Models\Services
 */
class Purchases{

	/**
	 * @param int $id
	 * @return Purchase|false
	 */
	public static function find($id) {
		$filter = new SearchPurchases();
		$filter->keywords->setValue( Purchase::buildReference($id) );

		$sql = new Local\Purchase();
		$result = $sql->searchPurchases($filter);

		if($result->rows) return Mapper\Purchase::fromRow($result->rows[0]);
		else return false;
	}

	/**
	 * Search projects
	 * @param SearchPurchases $filter
	 * @return SearchResult
	 */
	public static function search(SearchPurchases $filter)
	{
		// On cherche dans mysql
		$db = Local\Purchase::instance();
		$searchResult = $db->searchPurchases($filter);

		return Mapper\Purchase::fromSearchResult($searchResult);
	}

	/**
	 * @param $id
	 * @param string $tab
	 * @return Purchase
	 */
	public static function get($id, $tab = Purchase::TAB_DEFAULT)
	{
		$db = Local\Purchase::instance();
		$entity = $db->getPurchase($id, $tab);

		if($entity) {
			self::attachMoreData($entity); // On appelle avant de mapper pour conserver les calculs fait dans la v6 avec les noms de champs privés
			$entity = Mapper\Purchase::fromSQL($entity, $tab);
		} else $entity = false;
		return $entity;
	}

	public static function getNew($contactID = null, $projectID = null, $deliveryID = null)
	{
		$user = CurrentUser::instance();

		$purchase = new Purchase([
			'ACHAT_DATE' => date('Y-m-d'),
			'ACHAT_ETAT' => 1,
			'ID_PROFIL' => $user->getEmployeeId(),
			'ID_SOCIETE' => $user->getAgencyId(),
			'ACHAT_QUANTITE' => 1,
			'ACHAT_TYPE' => 0,
			'ACHAT_CATEGORIE' => 0,
			'ACHAT_TAUXTVA' => 20,
			'ACHAT_CONDREGLEMENT' => 0,
			'ACHAT_DEBUT' => date('Y-m-d'),
			'ACHAT_FIN' => date('Y-m-d')
		]);

		$testRequest = new \BoondManager\Lib\RequestAccess();
		$testRequest->setUser($user);

		if($contactID){
			$contact = Contacts::get($contactID);
			$testRequest->setData($contact);
			if($contact && (new \BoondManager\APIs\Contacts\Specifications\HaveReadAccess( Models\Contact::TAB_INFORMATION ))->isSatisfiedBy($testRequest)) {
				$purchase->contact = $contact;
			}
		}
		if($projectID){
			throw new \Exception('do a complete review'); //TODO quand les projets seront fait
			$project = Projects::get($projectID);
			$testRequest->setData($project);
			if($project && (new \BoondManager\APIs\Projects\Specifications\HaveReadAccess())->isSatisifedBy($testRequest)) {
				$purchase->project    = $project;
				$purchase->ID_SOCIETE = $project->ID_SOCIETE;
			}
		}
		if($deliveryID){
			throw new \Exception('do a complete review'); //TODO, quand les deliveries seront faites
			$delivery = Deliveries::get($deliveryID);
			$testRequest->setData($delivery);
			if($delivery && (new RequestAccess\Deliveries\HaveReadAccess())->isSatisfiedBy($testRequest)) {
				$purchase->delivery = $delivery;
			}
		}

		$dba = new Local\Agency();
		$agency = $dba->getAgency($purchase->ID_SOCIETE, Models\Agency::TAB_PURCHASES);
		if($agency){
			$purchase->taxRate             = $agency->GRPCONF_ACHATTAUXTVA;
			$purchase->ACHAT_CONDREGLEMENT = $agency->GRPCONF_ACHATCONDREGLEMENT;
			$purchase->ACHAT_TYPEPAYMENT   = $agency->GRPCONF_ACHATTYPEPAYMENT;
		}

		return $purchase;
	}

	/**
	 * @param Information $filter
	 * @param Purchase $entity
	 * @return Purchase
	 */
	public static function buildFromFilter($filter, $entity = null) {
		if(!$entity) $entity = new Purchase();

		$entity->backupData();

		$entity->mergeWith( $filter->toObject() );

		$entity->calculateData();

		return $entity;
	}

	public static function update(Purchase &$entity)
	{
		// transform project to sqlData
		$sqlData = Mapper\Purchase::toSQL($entity);

		$db = Local\Purchase::instance();

		if($db->updatePurchase($sqlData, $entity->id)) {
			Notification\Purchase::update($entity, Purchase::TAB_INFORMATION);
			$entity = self::get($entity->id, Purchase::TAB_INFORMATION);
			return true;
		} else return false;
	}

	private static function transformFilterDataToSQLData(\BoondManager\APIs\Purchases\Filters\Information $filter){

		$data = $filter->getValue();
		$data = $data['attributes'];

		$sampleEntity = new Purchase();

		$sqlData = [];
		$sqlData['ACHAT'] = array_merge(
			Tools::reversePublicFieldsToPrivate( $sampleEntity->getPublicFieldsMapping(), $data),
			Tools::reversePublicFieldsToPrivate( $sampleEntity->getRelationshipMapping(), $filter->relationships->getValue())
		);

		return $sqlData;
	}

	/**
	 * @param Purchase $purchase
	 * @return Purchase
	 * @throws \Exception
	 */
	public static function create(Purchase &$purchase){

		$sqlData = Mapper\Purchase::toSQL($purchase);

		$db = Local\Purchase::instance();

		if( $id = $db->newPurchase( $sqlData ) ) {
			$purchase = self::get($id, Purchase::TAB_INFORMATION);
			Notification\Purchase::create($purchase);
			return $purchase;
		} else {
			return false;
		}

		// cas particulier avec la création d'un achat à partir d'une prestation
		if($deliveryID = $filter->delivery->getValue()){

			throw new \Exception('TODO'); //TODO lorsque project & deliveries seront fait

			$rawDelivery = $filter->delivery->getDbObject();

			$dbProject = new Local\Project();
			$dbProject->getProjetMission($deliveryID);

			$filter->subscription->setValue(1);
			$filter->state->setValue(1);
			$filter->typeOf->setValue(1);
		}

		$db = new Local\Purchase();
		$sqlData = self::transformFilterDataToSQLData($filter);
		$id = $db->newPurchase( $sqlData );

		return $db->getPurchase($id, Purchase::TAB_INFORMATION);
	}

	/**
	 * Attach more data
	 * @return Models\Payment
	 * @param string $tab
	 */
	private static function attachMoreData(Purchase $entity, $tab = Purchase::TAB_DEFAULT)
	{
		switch($tab) {
			case Purchase::TAB_INFORMATION:
				break;
			case Purchase::TAB_ACTIONS:
				break;
			case Purchase::TAB_PAYMENTS:
				break;
			case Purchase::TAB_DEFAULT:
				break;
		}
	}

	public static function delete(Purchase $entity)
	{
		$db = Local\Purchase::instance();

		if($db->canDeleteObject($entity->id)) {
			Notification\Purchase::delete($entity);
			$db->deleteObject($entity->id);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * @param Purchase[]|Purchase $purchases
	 */
	public static function attachRights($purchases)
	{
		if(!is_array($purchases)) $purchases = [$purchases];

		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();

		foreach($purchases as $purchase) {
			$request->setData($purchase);
			$purchase->canReadPurchase = $readSpec->isSatisfiedBy($request);
			$purchase->canWritePurchase = $writeSpec->isSatisfiedBy($request);
		}
	}

	public static function getRights($purchase)
	{
		$request = new RequestAccess();
		$request->data = $purchase;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess(BM::TAB_DEFAULT);
		$writeSpec = new HaveWriteAccess(BM::TAB_DEFAULT);
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_PURCHASES, $purchase);

		foreach(IsActionAllowed::AVAILABLE_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		foreach(Purchase::getAllTabs() as $key) {
			$readSpec->setTab($key);
			$writeSpec->setTab($key);
			$right->addApi(Tools::camelCase($key), $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));
		}

		return $right;
	}
}
