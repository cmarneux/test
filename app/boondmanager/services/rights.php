<?php
/**
 * rights.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace BoondManager\Services;

use BoondManager\Models\Company;
use BoondManager\Models\Opportunity;
use BoondManager\Models\Product;
use BoondManager\Models\Purchase;
use BoondManager\Models\Employee;
use BoondManager\Lib\RequestAccess;
use BoondManager\OldModels\Specifications;

class Rights{
	public static function getModulesRights($module){
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());
		switch ($module){
			case BM::MODULE_RESOURCES:
				$searchAccess = new \BoondManager\APIs\Employees\Specifications\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_RESOURCES) );
				$actions =[
					'create' => ((new \BoondManager\APIs\Employees\Specifications\HaveCreateAccess)->isSatisfiedBy($request)),
					'delete' => ((new \BoondManager\APIs\Employees\Specifications\HaveDeleteAccess)->isSatisfiedBy($request)),
					'duplicate' => false, //TODO
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_CANDIDATES:
				$searchAccess = new \BoondManager\APIs\Candidates\Specifications\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_RESOURCES) );
				$actions =[
					'create' => ((new \BoondManager\APIs\Candidates\Specifications\HaveCreateAccess)->isSatisfiedBy($request)),
					'delete' => ((new \BoondManager\APIs\Candidates\Specifications\HaveDeleteAccess)->isSatisfiedBy($request)),
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_PRODUCTS:
				$searchAccess = new Specifications\RequestAccess\Products\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_PRODUCTS) );
				$actions =[
					'create' => ((new \BoondManager\APIs\Products\Specifications\HaveCreateAccess)->isSatisfiedBy($request)),
					'delete' => ((new \BoondManager\APIs\Products\Specifications\HaveDeleteAccess)->isSatisfiedBy($request)),
					'duplicate' => false, //TODO
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_COMPANIES:
			case BM::MODULE_CONTACTS:
			case BM::MODULE_CRM:
				// the test can be on companies or contact. It should be exactly the same
				$searchAccess = new \BoondManager\APIs\Companies\Specifications\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_CRM) );
				$actions =[
					'create' => ((new \BoondManager\APIs\Companies\Specifications\HaveCreateAccess)->isSatisfiedBy($request)),
					'duplicate' => false, //TODO
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_OPPORTUNITIES:
				//~ TODO : Tin : À tester
				$searchAccess = new Specifications\RequestAccess\Opportunities\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_OPPORTUNITIES) );
				$actions =[
					'create' => ((new \BoondManager\APIs\Opportunities\Specifications\HaveCreateAccess)->isSatisfiedBy($request)),
					'duplicate' => false, //TODO
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_POSITIONINGS:
				//~ TODO : Tin : À tester
				$searchAccess = new \BoondManager\APIs\Positionings\Specifications\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_POSITIONINGS) );
				$actions =[
					'create' => ((new \BoondManager\APIs\Positionings\Specifications\HaveCreateAccess)->isSatisfiedBy($request)),
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_ACTIONS:
				//~ TODO : Tin : À tester
				$searchAccess = new \BoondManager\APIs\Actions\Specifications\HaveSearchAccess();
				$extractAccess = $searchAccess
					->and_( new Specifications\RequestAccess\HaveRight(BM::RIGHT_EXTRACTION, BM::MODULE_ACTIONS) );
				$actions =[
					'extract' => $extractAccess->isSatisfiedBy($request),
					'search' => $searchAccess->isSatisfiedBy($request)
				];
				break;
			case BM::MODULE_PURCHASES:
				$actions = [
					'create' => true,
					'search' => true,
					'extract' => true
				];
				break;
			default :
				throw new \Exception("module '$module' not handled");

		}

		$right = new Rights(CurrentUser::instance(), $module);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		return $right;
	}

	/**
	 * @param string $module
	 * @param int|string $entityID
	 * @return \BoondManager\Models\Rights
	 * @throws \Wish\Exceptions\NotFound
	 * @throws \Exception
	 */
	public static function getEntityRights($module, $entityID){

		switch ($module) {
			case BM::MODULE_CANDIDATES:
				$entity = Candidates::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getCandidatesRight($entity);
			case BM::MODULE_RESOURCES:
				$entity = Employees::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getResourcesRight($entity);
			case BM::MODULE_PRODUCTS:
				$entity = Products::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getProductsRight($entity);
			case BM::MODULE_CONTACTS:
				$entity = Contacts::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getContactRight($entity);
			case BM::MODULE_COMPANIES:
				$entity = Companies::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getCompanyRight($entity);
			case BM::MODULE_OPPORTUNITIES:
				$entity = Opportunities::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getOpportunityRight($entity);
			case BM::MODULE_PURCHASES:
				$entity = Purchases::getObject($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return self::getPurchaseRight($entity);
			case BM::MODULE_APPS:
				return new Rights(CurrentUser::instance(), BM::MODULE_APPS);
				break;
			case 'contracts':
				$entity = Contracts::get($entityID);
				if(!$entity) throw new \Wish\Exceptions\NotFound();
				return Contracts::getRights($entity);
			default:
				throw new \Exception("module '$module' not handled'");
		}
	}

	public static function getResourcesRight($entity){
		$right = new Rights(CurrentUser::instance(), BM::MODULE_RESOURCES, $entity);

		$actions = Employees::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Employees::getVisibleTabs($entity);
		$writableTabs = Employees::getWritableTabs($entity);

		foreach(Employee::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));

		$fields = Employees::getDisabledFields($entity);
		foreach($fields as $field=>$test) {
			$right->addField($field, true, $test);
		}

		return $right;
	}

	public static function getProductsRight($entity){
		$right = new Rights(CurrentUser::instance(), BM::MODULE_RESOURCES, $entity);

		$actions = Products::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Products::getVisibleTabs($entity);
		$writableTabs = Products::getWritableTabs($entity);

		foreach(Product::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));

		//TODO: fields

		return $right;
	}

	/**
	 * @param $entity
	 * @return Rights
	 */
	public static function getCompanyRight($entity){
		$right = new Rights(CurrentUser::instance(), BM::MODULE_CRM, $entity);

		$actions = Companies::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Companies::getVisibleTabs($entity);
		$writableTabs = Companies::getWritableTabs($entity);

		foreach(Company::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));

		//TODO: fields

		return $right;
	}

	public static function getContactRight($entity){
		$right = new Rights(CurrentUser::instance(), BM::MODULE_CRM, $entity);


		$actions = Contacts::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);


		$readableTabs = Contacts::getVisibleTabs($entity);
		$writableTabs = Contacts::getWritableTabs($entity);

		foreach(Company::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));

		//TODO: fields

		return $right;
	}

	private static function getCandidatesRight($entity)
	{
		$right = new Rights(CurrentUser::instance(), BM::MODULE_CANDIDATES, $entity);

		$actions = Candidates::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Candidates::getVisibleTabs($entity);
		$writableTabs = Candidates::getWritableTabs($entity);

		foreach(Employee::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));

		//TODO: fields

		return $right;
	}

	private static function getOpportunityRight($entity)
	{
		$right = new Rights(CurrentUser::instance(), BM::MODULE_OPPORTUNITIES, $entity);

		$actions = Opportunities::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Opportunities::getVisibleTabs($entity);
		$writableTabs = Opportunities::getWritableTabs($entity);

		foreach(Opportunity::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));


		return $right;
	}

	private static function getPurchaseRight($entity)
	{
		$right = new Rights(CurrentUser::instance(), BM::MODULE_OPPORTUNITIES, $entity);
/* TODO
		$actions = Purchases::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Purchases::getVisibleTabs($entity);
		$writableTabs = Purchases::getWritableTabs($entity);
*/
		foreach(Purchase::getAllTabs() as $key)
			//$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));
			$right->addApi($key, true, true);


		return $right;
	}


	private static function getPositioningRight($entity)
	{
		$right = new Rights(CurrentUser::instance(), BM::MODULE_POSITIONINGS, $entity);

		$actions = Positionings::getAvailableActions($entity);
		foreach($actions as $key => $value)
			$right->addAction($key, $value);

		$readableTabs = Positionings::getVisibleTabs($entity);
		$writableTabs = Positionings::getWritableTabs($entity);

		foreach(Employee::getAllTabs() as $key)
			$right->addApi($key, in_array($key, $readableTabs), in_array($key, $writableTabs));

		//TODO: fields

		return $right;
	}
}
