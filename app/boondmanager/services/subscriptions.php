<?php
/**
 * subscriptions.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\OldModels\Filters;
use BoondManager\Lib\MySQL;

/**
 * Class Subscriptions
 * @package BoondManager\Models\Services
 */
class Subscriptions{
    /**
     * get a number of licences available after removing all accounts created. Can be infinite
     * @return int
     */
    public static function getLicencesLeft(){
        $dbSub = new \BoondManager\Databases\Local\Subscription();
        $subscription = $dbSub->getGroupSubscription();
        $licences = INF;
        if($subscription->AB_MAXRESSOURCESPERMANAGER) {
            $dbRes = new \BoondManager\Databases\Local\Employee();

            $filter = new \BoondManager\APIs\Employees\Filters\SearchEmployees();
            $filter->resourceStates->setValue([1]);
            $filter->excludeManager->setValue(true);

            $result = $dbRes->searchEmployees($filter);
            $licencesBought = $subscription->AB_NBMANAGERS * $subscription->AB_MAXRESSOURCESPERMANAGER;
            $licences = max(0, $licencesBought - $result->total);
        }

        return $licences;
    }

    public static function getMaxStorage(){
        $dbSub = new \BoondManager\Databases\Local\Subscription();
        $subscription = $dbSub->getGroupSubscription();
        return $subscription->AB_MAXSTORAGE;
    }
}
