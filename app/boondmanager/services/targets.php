<?php
/**
 * ${FILE_NAME}
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;


use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use BoondManager\Models\Employee;
use Wish\Models\SearchResult;

class Targets
{
	/**
	 * @param Employee $employee
	 * @return SearchResult
	 */
	public static function getAllEmployeeObjectives(Employee $employee) {
		$db = Local\Target::instance();
		$data = $db->getAllObjectifs($employee->userId);

		return new SearchResult( Mapper\Target::getAllObjectifs($data) );
	}
}
