<?php
/**
 * times.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace BoondManager\Services;
use Wish\MySQL\SearchResult;
use Wish\MySQL\AbstractDb;
use BoondManager\OldModels\Filters;

/**
 * Class Times
 * @package BoondManager\Models\Services
 */
class Times{

    /**
     * Handle the research MySQL
     *
     * @param Filters\Search\Times $filter
     * @return SearchResult
     */
    public static function search(Filters\Search\Times $filter)
    {
        $db = new \BoondManager\Databases\Local\Time();
        return $db->search($filter);
    }
}
