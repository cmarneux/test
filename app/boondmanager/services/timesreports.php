<?php
/**
 * timesreports.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs\TimesReports\Filters\SearchTimesReports;
use BoondManager\APIs;
use BoondManager\APIs\TimesReports\Specifications\HaveReadAccess;
use BoondManager\APIs\TimesReports\Specifications\HaveWriteAccess;
use BoondManager\APIs\TimesReports\Specifications\IsActionAllowed;
use BoondManager\Lib\RequestAccess;
use Wish\Filters\AbstractFilters;
use Wish\Tools;
use BoondManager\OldModels\Filters;
use BoondManager\Models;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;

class TimesReports{
	/**
	 * @param SearchTimesReports $filter
	 * @param bool $includeTimes
	 * @return \Wish\Models\SearchResult
	 */
	public static function search(SearchTimesReports $filter, $includeTimes = false){
		$db = Local\TimesReport::instance();
		$result = $db->search($filter);

		if($includeTimes){
			foreach($result->rows as $timeReport){
				$times = self::getTimes($timeReport->ID_LISTETEMPS);
				//~ regular times ->
				$timeReport->regularTimes = $times['regularTimes'];
				//~ exceptional times ->
				$timeReport->exceptionalTimes = $times['exceptionalTimes'];
			}
		}

		return Mapper\TimesReport::fromSearchResult($result);
	}

	/**
	 * loads an entity from an ID
	 * @param int $id entity ID
	 * @return Models\TimesReport|false
	 */
	public static function get($id){
		$db = Local\TimesReport::instance();
		$entity = $db->getObject($id);

		if(!$entity) return false;

		$timesReport = Mapper\TimesReport::fromSQL($entity);
		// config for user & agency
		Employees::attachUserConfig($timesReport->resource);
		$timesReport->agency = Agencies::get($timesReport->agency->id, Models\Agency::TAB_ACTIVITYEXPENSES);
		$timesReport->calculateValidationWorkflow();

		$timesReport->projects = Employees::getAllProjects($timesReport->resource->id, $timesReport->getStartPeriod(), $timesReport->getEndPeriod());

		$pIds = Tools::getFieldsToArray($timesReport->projects, 'id');
		$timesReport->orders = Orders::searchMensualOrdersForProjectsOfAResourceOnPeriod($timesReport->resource->id, $pIds, $timesReport->getStartPeriod(), $timesReport->getEndPeriod());

		// filtrage des bdc selon les droits
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());
		$haveWriteAccess = new APIs\Orders\Specifications\HaveWriteAccess();
		$timesReport->orders = array_filter($timesReport->orders, function(Models\Order $val) use ($haveWriteAccess, $request){
			$request->setData($val);
			return $haveWriteAccess->isSatisfiedBy($request);
		});

		return $timesReport;
	}

	/**
	 * loads timesreport's times from an ID
	 * @param int $id entity ID
	 * @return [ 'regularTimes' => MySQL\RowObject\Time[], 'exceptionalTimes' => MySQL\RowObject\Time[] ]
	 */
	public static function getTimes($id){
		$times = [];
		//~ regular times ->
		$filter = Filters\Search\Times::getUserFilter();
		$filter->disableMaxResultLimit();
		$filter->keywords->setValue('RPT'.$id);
		$filter->category->setValue('regular');
		$times['regularTimes'] = Times::search($filter)->rows;
		//~ exceptional times ->
		$filter->category->setValue('exceptional');
		$filter->returnDetailedExceptionalTimes->setValue(true);
		$times['exceptionalTimes'] = Times::search($filter)->rows;

		return $times;
	}

	/**
	 * Create an empty timesreport for creation.
	 *
	 * @param APIs\TimesReports\Filters\GetDefault $filter
	 * @return Models\TimesReport
	 */
	public static function getNew(APIs\TimesReports\Filters\GetDefault $filter){

		$resource = Employees::get($filter->resource->getValue(), Models\Employee::TAB_TIMESREPORTS);
		Employees::attachUserConfig($resource);

		$agencyID = $filter->agency->getValue();
		if(!$agencyID) $agencyID = $resource->agency->id;

		$agency = Agencies::get($agencyID, Models\Agency::TAB_ACTIVITYEXPENSES);

		$entity = new Models\TimesReport([
			'id'           => 0,
			'term'         => $filter->term->getValue(),
			'regularTimes' => [],// TODO : Tin : à déterminer
			'resource'     => $resource,
			'agency'       => $agency,
		]);

		$entity->projects = Employees::getAllProjects($entity->resource->id, $entity->getStartPeriod(), $entity->getEndPeriod());

		$pIds = Tools::getFieldsToArray($entity->projects, 'id');
		$entity->orders = Orders::searchMensualOrdersForProjectsOfAResourceOnPeriod($entity->resource->id, $pIds, $entity->getStartPeriod(), $entity->getEndPeriod());

		// filtrage des bdc selon les droits
		$request = new RequestAccess();
		$request->setUser(CurrentUser::instance());
		$haveWriteAccess = new APIs\Orders\Specifications\HaveWriteAccess();
		$entity->orders = array_filter($entity->orders, function(Models\Order $val) use ($haveWriteAccess, $request){
			$request->setData($val);
			return $haveWriteAccess->isSatisfiedBy($request);
		});

		return $entity;
	}

	/**
	 * @param AbstractFilters $filter
	 * @param Models\TimesReport|null $entity
	 * @return Models\TimesReport
	 */
	public static function buildFromFilter(AbstractFilters $filter, Models\TimesReport $entity = null) {
		if(!$entity) $entity = new Models\TimesReport();
		$entity->mergeWith($filter->toObject());
		return $entity;
	}

	/**
	 * create a new timesreport
	 * @param Models\TimesReport $entity
	 * @return bool
	 */
	public static function create(Models\TimesReport &$entity)
	{
		$sqlData = Mapper\TimesReport::toSQL($entity);

		$db = Local\TimesReport::instance();

		if($id = $db->setObject($sqlData)) {
			$entity = self::get($id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param Models\TimesReport $entity
	 * @return bool
	 */
	public static function update(Models\TimesReport &$entity) {

		$sqlData = Mapper\TimesReport::toSQL($entity);

		$db = Local\TimesReport::instance();

		if($db->setObject($sqlData, $entity->id)) {
			$entity = self::get($entity->id);
			return true;
		} else {
			return false;
		}
	}

	public static function delete(Models\TimesReport $entity)
	{
		$db = Local\TimesReport::instance();

		if($db->canDeleteObject($entity->id)) {
			return $db->deleteObject($entity->id);

			//~ Notification\TimesReport::getInstance($entity->ID_LISTETEMPS, '', [], [])->delete();
			//~ return true;
		}else{
			return false;
		}
	}

	/**
	 * @param Models\TimesReport[] $rows
	 * @param string $start Y-m
	 * @param string $end Y-m
	 * @return array
	 */
	public static function getMissingMonths($rows, $start, $end){
		list( $yearStart, $monthStart ) = explode('-', $start);
		list( $yearEnd, $monthEnd ) = explode('-', $end);
		//$missing = [$start];

		$tEnd = mktime(0,0,0, $monthEnd, 1, $yearEnd);
		$m = 0;

		do{
			$cursor = mktime(0, 0, 0, $monthStart + (++$m), 0, $yearStart);
			$missing[] = date('Y-m', $cursor);
		}while($cursor < $tEnd);

		$exists = [];

		foreach($rows as $r){
			$exists[] = $r->term;
		}

		return array_values(array_diff($missing, $exists));
	}

	public static function getRights($timesReport)
	{
		$request = new RequestAccess();
		$request->data = $timesReport;
		$request->user = CurrentUser::instance();

		$readSpec = new HaveReadAccess();
		$writeSpec = new HaveWriteAccess();
		$right = new Models\Rights(CurrentUser::instance(), BM::MODULE_ACTIVITIES_EXPENSES, $timesReport);

		foreach(IsActionAllowed::RIGHTS_ACTIONS as $action) {
			$spec = new IsActionAllowed($action);
			$right->addAction($action, $spec->isSatisfiedBy($request));
		}

		$right->addApi('default', $readSpec->isSatisfiedBy($request), $writeSpec->isSatisfiedBy($request));

		return $right;
	}

	/**
	 * @param Models\TimesReport $entity
	 * @return array
	 */
	public static function getWarnings($entity)
	{
		$warnings = [];

		$db = Local\TimesReport::instance();
		$dbWarnings = $db->getTpsPrjWarnings($entity->id);

		foreach($dbWarnings as $warning) {
			if($warning['ID_PROJET'] > 0) {
				if($warning['ID_MISSIONPROJET'] > 0) {
					foreach($entity->projects as $prj) :
						if($prj->id == $warning['ID_PROJET']) :
							$found = false;
							foreach ($prj->deliveries as $del) {
								if ($del->id == $warning['ID_MISSIONPROJET']) {
									$found      = true;
									$warnings[] = [
										'code'     => Models\Time::WARNING_OUTSIDE_DELIVERY_DATES,
										'detail'   => Dictionary::getDict('main.warning.' . Models\Time::WARNING_OUTSIDE_DELIVERY_DATES),
										'project'  => [
											'id'        => $prj->id,
											'reference' => $prj->reference
										],
										'delivery' => [
											'id'        => $del->id,
											'title'     => $del->title,
											'startDate' => $del->startDate,
											'endDate'   => $del->endDate
										]
									];
								}
								break 2;
							}
							// test is not use full but it might be easier for the developer to understand what's going on
							if(!$found) {
								$warnings[] = [
									'code'    => Models\Time::WARNING_WRONG_DELIVERY_MONTH,
									'detail'  => Dictionary::getDict('main.warning.' . Models\Time::WARNING_WRONG_DELIVERY_MONTH),
									'project' => [
										'id'        => $prj->id,
										'reference' => $prj->reference
									]
								];
							}
							break;
						endif;
					endforeach;
				} else {
					$warnings[] = [
						'code'     => Models\Time::WARNING_NO_DELIVERIES_ON_PROJECT,
						'detail'   => Dictionary::getDict('main.warning.' . Models\Time::WARNING_NO_DELIVERIES_ON_PROJECT),
						'project'  => [
							'id'        => $prj->id,
							'reference' => $prj->reference
						]
					];
				}
			} else {
				$warnings[] = [
					'code'     => Models\Time::WARNING_OUTSIDE_CONTRACT_DATES,
					'detail'   => Dictionary::getDict('main.warning.' . Models\Time::WARNING_OUTSIDE_CONTRACT_DATES),
				];
			}
		}

		$workingDays = Tools::getNumberOfWorkingDays($entity->getStartPeriod(), $entity->getEndPeriod(), $entity->agency->calendar);
		$totalDays = $entity->getTotalRegularTime() + $entity->getTotalAbsences() + $entity->getTotalInternal();
		if( $totalDays > $workingDays) {
			$warnings[] = [
				'code' => Models\Time::WARNING_TOO_MUCH_DAYS,
				'detail' => Dictionary::getDict('main.warning.' . Models\Time::WARNING_TOO_MUCH_DAYS),
			];
		}

		return $warnings;
	}
}
