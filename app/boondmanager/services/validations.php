<?php
/**
 * validations.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace BoondManager\Services;

use BoondManager\APIs;
use BoondManager\APIs\Validations\Filters\SaveEntity;
use BoondManager\Lib\RequestAccess;
use BoondManager\Models\AbsencesReport;
use BoondManager\Models\Employee;
use BoondManager\Models\Account;
use BoondManager\Models\ExpensesReport;
use BoondManager\Models\TimesReport;
use BoondManager\Models\Validation;
use BoondManager\OldModels\Filters;
use BoondManager\Databases\Local;
use BoondManager\Databases\Mapper;
use Wish\Email;
use Wish\Tools;

class Validations{
	/**
	 * @param Filters\Search\Validations $filter
	 * @return \Wish\Models\SearchResult
	 * @throws \Exception
	 */
	public static function search(Filters\Search\Validations $filter){
		$db = Local\Validation::instance();
		return $db->search($filter);
	}

	public static function unserializeValidationWorkflow($workflow){
		$valWorkflow = [];
		foreach (Tools::unserializeArray($workflow) as $w){
			if($w == 0) $valWorkflow[] = 'N';
			else if($w < 0) $valWorkflow[] = 'N+'.abs($w);
			else if( $manager = Managers::getBasicFromUser($w)) $valWorkflow[] = $manager;
		}
		return $valWorkflow;
	}

	public static function serializeValidationWorkflow($data){
		$stringVal = [];
		foreach($data as $entry){
			if(is_string($entry)) {
				if ($entry === 'N') $stringVal[] = 0;
				else if (substr($entry, 0, 1) === 'N') $stringVal[] = -intval(substr($entry, 2, 1));
			} else if($entry instanceof Account && $id = Managers::getUserIdFromEmployeeId($entry->id)) $stringVal[] = $id;
		}
		return Tools::serializeArray($stringVal);
	}

	/**
	 * @param Employee $resource
	 * @param $workflow
	 * @return Account[]
	 */
	public static function getValidatorsDataFromWorkflow(Employee $resource, $workflow) {
		//ON récupère la liste des N, N+1, N+2 et N+3
		$highestLvl = -1;
		foreach($workflow as $w) {
			if(!is_string($w)) continue;

			if($w === 'N') $highestLvl = max(0, $highestLvl);
			else if(substr($w, 0, 1) === 'N') $highestLvl = max(intval(substr($w, 2, 1)), $highestLvl);
		}

		$nManagers = [];
		$managerID = $resource->getManagerID();
		for($i = 0; $i <= $highestLvl; $i++) {
			if (!$managerID) break;
			$nManagers[] = $m = Managers::getBasic($managerID);
			$managerID   = $m->getManagerID();
		}

		$result = [ clone $resource ]; // trap, must clone the resouce because it can have a different filterFields in the end. the clone part might move to the filterFields function. EDIT : should now be fixed since the cloning is also done when filtering fields.
		foreach($workflow as $w) {
			if (is_array($w) || $w instanceof Employee) {
				$manager  = Managers::getBasic($w['id']);
				$result[] = $manager;
				continue;
			}
			if ($w === 'N') $n = 0;
			else $n = intval(substr($w, 2, 1));

			if (array_key_exists($n, $nManagers)) $result[] = $nManagers[ $n ];
		}
		return $result;
	}

	public static function buildFromFilter(SaveEntity $filter, Validation $validation = null)
	{
		if(!$validation) $validation = new Validation();
		$validation->mergeWith( $filter->toObject() );
		return $validation;
	}

	public static function get($id) {
		$db = Local\Validation::instance();
		$data = $db->getValidation($id);

		return ($data) ? Mapper\Validation::fromSQL($data) : false;
	}

	public static function getWithFullDependsOn($id) {
		$data = self::get($id);

		switch (get_class($data->dependsOn)) {
			case TimesReport::class: $data->dependsOn = \BoondManager\Services\TimesReports::get($data->dependsOn->id); break;
			case ExpensesReport::class: $data->dependsOn = \BoondManager\Services\ExpensesReports::get($data->dependsOn->id); break;
			case AbsencesReport::class: $data->dependsOn = \BoondManager\Services\AbsencesReports::get($data->dependsOn->id); break;
		}

		return $data;
	}

	/**
	 * @param Validation $validation
	 * @return bool
	 */
	public static function create(Validation &$validation) {
		if($id = self::updateValidationState($validation)) {
			$validation = self::get($id);
			return true;
		} else
			return false;
	}

	/**
	 * @param Validation $validation
	 * @return bool
	 */
	public static function update(Validation &$validation) {
		if( self::updateValidationState($validation) ) {
			$validation = self::get($validation->id);
			return true;
		} else
			return false;
	}

	/**
	 * @param Validation $validation
	 * @return int
	 */
	public static function getDBDependsOnType(Validation $validation) {
		switch (get_class($validation->dependsOn)) {
			case TimesReport::class: return Validation::TYPE_TIMESREPORT_BDD;
			case ExpensesReport::class: return Validation::TYPE_EXPENSESREPORT_BDD;
			case AbsencesReport::class: return Validation::TYPE_ABSENCESREPORT_BDD;
		}
	}

	/**
	 * @param Validation $validation
	 * @return bool
	 */
	private static function updateValidationState(Validation $validation) {
		$db = Local\Validation::instance();
		$email = new Email();

		$report = $validation->dependsOn;

		$list = $report->validationWorkflow;
		$validatorID = $validation->expectedValidator->id;

		$validatorPosition = 0;
		$valExists = false;
		foreach($list as $i => $employee) {
			if($employee->id == $validatorID) {
				$validatorPosition = $i;
				$valExists = true;
				break;
			}
		}

		$dbParentType = self::getDBDependsOnType($validation);

		$report = self::getDictReportMapper($validation);

		$CIRCUIT_VAL_IDPROFIL = $validatorID;

		$request = new RequestAccess($validation->dependsOn);

		switch ($validation->state) {
			case Validation::VALIDATION_VALIDATED:
				if( self::getSpec($validation, 'validate')->isSatisfiedBy($request) ) { // tests droits
					$return = $db->updateValidationData([
						'VAL_ETAT' => 1,
						'VAL_MOTIF' => '',
						'ID_VALIDATEUR' => $validation->realValidator->id
					], $validatorID, $validation->dependsOn->id, $dbParentType);

					if($valExists) {
						//On met en attente le prochain validateur et on lui envoit un mail (Uniquement, si le prochain n'est pas le validateur actuel)
						$j = $validatorPosition + 1;
						while(isset($list[$j]) && $list[$j]->id == $validatorID) $j++;

						if(isset($list[$j])) {
							$db->updateValidationData([
								'VAL_ETAT' => 2,
								'ID_VALIDATEUR' => $list[$j]->id
							],$list[$j]->id, $validation->dependsOn->id, $dbParentType);

							$NOTIFICATION_VAL_IDPROFIL = $list[$j]->id;

							// FIXME : it needs 2 other fields from a manager
							if($list[$j]->subscription == BM::USER_ACCESS_ACTIVE && $list[$j]->email1) {
								$email->Send(
									$list[$j]->email1,
									Dictionary::getDict("validation.$report.mail.waitingForValidation.object"),
									Dictionary::getDict("validation.$report.mail.waitingForValidation.message", [
										'firstNameValidator' => $list[$j]->firstName,
										'lastNameValidator' => $list[$j]->lastName,
										'firstNameEmployee' => $validation->dependsOn->resource->firstName,
										'lastNameEmployee' => $validation->dependsOn->resource->lastName,
										'url' => self::getUrlActivity($validation)
									]),
									BM::getEmailNoReply(),
									CurrentUser::instance()->getEmail()
								);
							}
						}

						//On envoit un mail au validateur précédent
						$j = $validatorPosition-1;
						if(isset($list[$j]) && $list[$j]->subscription == BM::USER_ACCESS_ACTIVE && $list[$j]->email1) {
							$email->reset();
							$email->Send(
								$list[$j]->email1,
								Dictionary::getDict("validation.$report.mail.validated.object"),
								Dictionary::getDict("validation.$report.mail.validated.message", [
									'firstNameValidator' => $list[$j]->firstName,
									'lastNameValidator' => $list[$j]->lastName,
									'firstNameEmployee' => $validation->dependsOn->resource->firstName,
									'lastNameEmployee' => $validation->dependsOn->resource->lastName,
									'firstNameNextValidator' => $list[$validatorPosition]->firstName,
									'lastNameNextValidator' => $list[$validatorPosition]->lastName,
									'url' => self::getUrlActivity($validation),
								]),
								BM::getEmailNoReply(),
								CurrentUser::instance()->getEmail()
							);
						}
					}
					return $return;
				}
				break;
			case Validation::VALIDATION_REJECTED:
				if( self::getSpec($validation, 'reject')->isSatisfiedBy($request) ) { // tests droits
					$return = $db->updateValidationData([
						'VAL_ETAT' => 3,
						'VAL_MOTIF' => $validation->reason,
						'ID_VALIDATEUR' => $validation->realValidator->id
					], $validatorID, $validation->dependsOn->id, $dbParentType);

					//On dévalide le précédent validateur et on lui envoit un mail si il existe (Uniquement, si le précédent n'est pas le validateur actuel)
					if($valExists) {
						$j = $validatorPosition-1;
						if($validation->reasonTypeOf != Validation::REASON_TYPE_CORRECTION_FOR_ALL_VALIDATOR) while(isset($list[$j]) && $list[$j]->id == $validatorID) $j--;

						for($k = $j; $k >= 0; $k--) {
							if(isset($list[$k])) {
								$NOTIFICATION_VAL_IDPROFIL = $list[$j]->id;
								switch($validation->reasonTypeOf) {
									case Validation::REASON_TYPE_REFUSED:
										$objectPathDict = "validation.$report.mail.refused.object";
										$messagePathDict = "validation.$report.mail.refused.messageRefused";
										break;
									default:
										$db->updateValidationData([
											'VAL_ETAT' => 2,
											'ID_VALIDATEUR' => $list[$k]->id
										], $list[$k]->id, $validation->dependsOn->id, $dbParentType);
										$objectPathDict = "validation.$report.mail.rejected.object";
										$messagePathDict = "validation.$report.mail.rejected.message";
										break;
								}
								if(($validation->reasonTypeOf != Validation::REASON_TYPE_CORRECTION_FOR_ALL_VALIDATOR || $k == 0)
									&& $list[$k]->subscription == BM::USER_ACCESS_ACTIVE && $list[$k]->email1)
								{
									$email->Send(
										$list[ $k ]->email1,
										Dictionary::getDict($objectPathDict),
										Dictionary::getDict($messagePathDict, [
											'firstNameValidator' => $list[$j]->firstName,
											'lastNameValidator' => $list[$j]->lastName,
											'firstNameEmployee' => $validation->dependsOn->resource->firstName,
											'lastNameEmployee' => $validation->dependsOn->resource->lastName,
											'firstNamePreviousValidator' => $list[$validatorPosition]->firstName,
											'lastNamePreviousValidator' => $list[$validatorPosition]->lastName,
											'reason' => $validation->reason,
											'url' => self::getUrlActivity($validation),
										]),
										BM::getEmailNoReply(),
										CurrentUser::instance()->getEmail()
									);
								}
							}
							if($validation->reasonTypeOf != Validation::REASON_TYPE_CORRECTION_FOR_ALL_VALIDATOR) $k = -1;
						}
					}
					return $return;
				}
				break;
			case Validation::VALIDATION_WAITING:
				if( self::getSpec($validation, 'unvalidate')->isSatisfiedBy($request) ) {
					return $db->updateValidationData([
						'VAL_ETAT' => 2,
						'VAL_MOTIF' => '',
						'ID_VALIDATEUR' => $validation->realValidator->id
					], $validatorID, $validation->dependsOn->id, $dbParentType);
				}
				break;
		}

		return false;
	}

	private static function getUrlActivity(Validation $validation) {

		$path = '';
		switch (get_class($validation->dependsOn)) {
			case TimesReport::class: $path = 'times-report'; break;
			case ExpensesReport::class: $path = 'expenses-report'; break;
			case AbsencesReport::class: $path = 'absences-report'; break;
		}

		return BM::getBoondManagerUrl() . "/$path/" . $validation->dependsOn->id .'?company=' . BM::getCustomerCode();
	}

	private static function getDictReportMapper(Validation $validation) {

		switch (get_class($validation->dependsOn)) {
			case TimesReport::class: return 'timesReport';
			case ExpensesReport::class: return 'expensesReport';
			case AbsencesReport::class: return 'absencesReport';
		}
	}

	private static function getSpec(Validation $validation, $right) {
		switch (get_class($validation->dependsOn)) {
			case TimesReport::class: return new APIs\TimesReports\Specifications\IsActionAllowed($right);
			case ExpensesReport::class:return new APIs\ExpensesReports\Specifications\IsActionAllowed($right);
			case AbsencesReport::class: return new APIs\AbsencesReports\Specifications\IsActionAllowed($right);
		}
	}

	/**
	 * @param Validation $entity
	 * @return bool
	 */
	public static function delete(Validation $entity) {
		$db = Local\Validation::instance();
		return $db->deleteValidationData($entity->id);
	}
}
