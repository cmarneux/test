<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" exclude-result-prefixes="w">
    <xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="no" />
        <xsl:template match="w:t">
            <xsl:value-of select="text()"/>
            <xsl:text> </xsl:text>
        </xsl:template>
</xsl:stylesheet>
