<?php
/**
 * cache.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish;

/**
 * Handle all the caching needed in Boondmanager
 *
 * the cached data are available only for the current user
 *
 * @namespace Wish
 */
class Cache extends \Prefab{
    /**
     * a prefix used in the session
     * @var string
     */
    private $prefix = 'SESSION.CACHE.';

    /**
     * is the cache disabled
     * @var bool
     */
    private $isEnabled = true;

    /**
     * build the full f3 path
     * @param string $key
     * @return string
     */
    private function buildKey($key){
        return $this->prefix.$key;
    }

    /**
     * save some data in the cache
     * @param string $key
     * @param mixed $val
     * @param int $ttl
     * @return mixed
     */
    public function set($key, $val, $ttl=0){
        return \Base::instance()->set( $this->buildKey($key) , serialize($val), $ttl);
    }

    /**
     * find out if a key exists in the Cache
     * @param string $key
     * @param mixed $val
     * @return bool
     */
    public function exists($key, &$val = NULL){
        return $this->isEnabled && \Base::instance()->exists( $this->buildKey($key), $val);
    }

    /**
     * get a value stored in the Cache
     * @param string $key
     * @return mixed
     */
    public function get($key){
        return unserialize(\Base::instance()->get( $this->buildKey($key) ));
    }

    /**
     * remove a value from the cache
     * @param $key
     * @return NULL
     */
    public function clear($key){
    	$cache = \Base::instance();
    	if ($cache->exists($this->buildKey($key))) {
        return $cache->clear( $this->buildKey($key) );
    	}
    	else {
    		return NULL;
    	}
    }

    /**
     * reset the cache
     * @return NULL
     */
    public function reset(){
        return \Base::instance()->clear( trim($this->buildKey(''), '.' ));
    }

    /**
     * enable or disable the cache
     * @param $bool
     */
    public function setEnabled($bool){
        $this->isEnabled = $bool==true;
    }
}
