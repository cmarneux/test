<?php
/**
 * context.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\ClockWork;

/**
 * Class Context
 * Gather data for benchmarking the application
 * @package Wish\ClockWork
 */
class Context{

	private $index = 0;

	/**
	 * @var array
	 */
	private $queries = [];

	/**
	 * @var array
	 */
	private $timings = [];

	/**
	 * return current time in ms
	 * @return float
	 */
	public function getTime(){
		return microtime(true);
	}

	/**
	 * prepare a benchmark for a query
	 * @param string $query
	 * @param array $params
	 * @return int a context ID (for ending the query)
	 */
	public function startQuery($query, $dbName, $params = null){
		$this->queries[] = ['query'=>$dbName.':'.PHP_EOL.$query, 'params'=>$params, 'start'=> $this->getTime()];
		return count($this->queries)-1;
	}

	/**
	 * end a query benchmark
	 * @param int $id the context ID
	 */
	public function endQuery($id){
		$endTime = $this->getTime();
		$this->queries[$id]['end'] = $endTime;
		$this->setDuration($this->queries[$id]);
	}

	/**
	 * prepare a benchmark for a custom label
	 * @param string $label a description for the benchmark
	 * @return int a context ID
	 */
	public function startTiming($label, $grouped = false){
		$key = strval($this->index++);
		$this->timings[$key] = [
			'description'=>$label,
			'start'=> $this->getTime(),
			// following fields are added for the grouped option
			'grouped' => $grouped,
			'name'=>$label,
			'count' => 1
		];
		return $key;
	}

	/**
	 * add a duration value to the entry based on the start & end values
	 * @param array $entry
	 * @return float
	 */
	private function setDuration(&$entry){
		if(isset($entry['start'], $entry['end'])) return $entry['duration'] = ($entry['end'] - $entry['start'])*1000;
	}

	/**
	 * add a timing entry
	 * @param string $label a custom description
	 * @param float $start start time (in seconds)
	 * @param float $end end time (in seconds)
	 */
	public function setTiming($label, $start, $end){
		$entry = [
			'description' => $label,
			'start' => $start,
			'end' => $end
		];
		$this->setDuration($entry);
		$this->timings[] = $entry;
	}

	/**
	 * stop a benchmark for an id
	 * @param int $id
	 * @return float
	 */
	public function endTiming($id){
		$endTime = $this->getTime();
		$this->timings[$id]['end'] = $endTime;
		$duration = $this->setDuration($this->timings[$id]);

		if($this->timings[$id]['grouped']){
			$this->merge($this->timings[$id]['name']);
		}

		return $duration;
	}

	private function merge($name){
		$ids = [];

		foreach($this->timings as $key => $entry){
			if($entry['name'] === $name && isset($entry['end'])){
				$ids[] = $key;
			}
		}

		if(count($ids) > 1){
			$ref = array_shift($ids);
			foreach($ids as $id){
				$this->timings[$ref]['end'] = max($this->timings[$id]['end'], $this->timings[$ref]['end']);
				$this->timings[$ref]['duration'] = $this->timings[$id]['duration'] + $this->timings[$ref]['duration'];
				$this->timings[$ref]['count']++;
				$this->timings[$ref]['description'] = $this->timings[$ref]['name'].':'.$this->timings[$ref]['count'].'x';
				unset($this->timings[$id]);
			}
		}
	}

	/**
	 * get the list of all benchmark on custom descriptions
	 * @return array
	 */
	public function getTimings(){
		return array_values($this->timings);
	}

	/**
	 * get the list of all queries benchmarked
	 * @return array
	 */
	public function getQueries(){
		return $this->queries;
	}
}
