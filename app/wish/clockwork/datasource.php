<?php
/**
 * DataSource.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\ClockWork;

use Clockwork\Request\Request;

/**
 * Class DataSource
 * @package Wish\ClockWork
 */
class DataSource extends \Clockwork\DataSource\DataSource{

    /**
     * @var Context app context
     */
    protected $context;

    /**
     * DataSource constructor.
     * @param Context $context
     */
    function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * retrieve data from the context and populate the request
     * @param Request $request
     * @return Request
     */
    public function resolve(Request $request)
    {
        $timeline = $this->context->getTimings();

        // optionally: pre-sort the timeline
        uasort($timeline, function($a, $b) {
            if($a['start'] > $b['start'])
                return 1;

            if($a['start'] == $b['start']) {
                if($a['end'] > $b['end'])
                    return 1;
                elseif ($a['end'] < $b['end'])
                    return -1;

                return 0;
            }

            return -1;
        });

        $queries = $this->context->getQueries();

        $request->timelineData = $timeline;
        $request->databaseQueries = $queries;

        return $request;
    }
}
