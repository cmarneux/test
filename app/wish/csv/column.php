<?php
/**
 * column.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\CSV;

class Column{
	/**
	 * @var string column key name
	 */
	private $key;
	/**
	 * @var string column label
	 */
	private $name;
	/**
	 * @var callable a callback to transform an input value
	 */
	private $callback;

	/**
	 * Column constructor.
	 * @param string $key
	 * @param string $name
	 * @param callable $callback
	 */
	public function __construct($key, $name=null, $callback = null){
		$this->key = $key;
		$this->name = $name;
		$this->setCallback($callback);
	}

	/**
	 * set the column's label
	 * @param string $name a label
	 * @return $this
	 */
	public function setName($name){
		$this->name = $name;
		return $this;
	}

	/**
	 * get the column's label
	 * @return null|string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * define a transformer method
	 * @param callable $callback a function to transform an input value
	 * @return $this
	 * @throws \Exception
	 */
	public function setCallback($callback){
		if($callback && is_callable($callback)){
			$this->callback = $callback;
		}
		return $this;
	}

	/**
	 * transform a value before inserting into the CSV
	 * @param mixed $input a value to transform
	 * @return mixed
	 */
	public function transform($input, $otherCols=null){
		if($this->callback != null) return call_user_func($this->callback, $input, $otherCols);
		else return $input;
	}

	/**
	 * get the column key name
	 * @return string
	 */
	public function getKey(){
		return $this->key;
	}
}
