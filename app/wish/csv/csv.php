<?php
/**
 * csv.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\CSV;

/**
 * Class CSV
 * @package BoondManager\Lib
 */
class CSV{
	/**
	 * @var resource
	 */
	private $handler;

	/**
	 * @var string;
	 */
	private $name;

	/**
	 * @var Column[]
	 */
	private $columns;

	/**
	 * @var array
	 */
	private $rows;

	/**
	 * @var bool
	 */
	private $downloadStarted = false;

	/**
	 * @var string
	 */
	private $filePath;

	/**
	 * @var
	 */
	private $encoding;

	/**
	 * CSV constructor.
	 * @param string $name filename
	 * @param string $filePath
	 */
	public function __construct($name, $filePath = 'php://output')
	{
		$this->filePath = $filePath;
		$this->name = $name;

		if($filePath != 'php://output') $filePath .= $name;
		$this->handler = fopen($filePath, 'w+');

		$this->columns = [];
		$this->rows = [];
	}

	/**
	 * @param $encoding
	 */
	public function setEncoding($encoding){
		$this->encoding = $encoding;
	}

	/**
	 *
	 */
	public function download(){
		$this->downloadStarted = true;
		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="'.$this->name.'"');
		header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		header('Expires: 0');

		foreach($this->rows as $r){
			$this->_addRow($r);
		}
	}

	/**
	 *
	 */
	public function flush(){
		ob_flush();
		flush();
	}

	/**
	 * @param $array
	 */
	private function _addRow($array){
		foreach($array as $k=>$v)
			$array[$k] = $this->convert($v);

		if($this->downloadStarted) fputcsv($this->handler, $array);
		else $this->rows[] = $array;
	}

	/**
	 * @param $value
	 * @return string
	 */
	private function convert($value){
		return mb_convert_encoding($value, $this->encoding, 'UTF-8');
	}

	/**
	 *
	 */
	public function addHeader(){
		$row = [];
		foreach($this->columns as $col){
			$row[] = $col->getName();
		}
		$this->_addRow($row);
	}

	/**
	 * @param Column $col
	 */
	public function addColumn(Column $col){
		$this->columns[] = $col;
	}

	/**
	 * @param $item
	 */
	public function addRow($item){
		$row = [];
		if($this->columns){
			foreach($this->columns as $col){
				$value = isset($item[$col->getKey()])? $col->transform($item[$col->getKey()], $item) : $col->transform(null, $item);
				$row[] = $value;
			}
		}else{
			$row = $item;
		}
		$this->_addRow($row);
	}

	/**
	 * @param $key
	 */
	public function removeColumn($key){
		foreach($this->columns as $k => $col){
			if($col->getKey == $key) {
				unset($this->columns[$k]);
				break;
			}
		}
	}

	/**
	 *
	 */
	public function close(){
		fclose($this->handler);
	}

}
