<?php
/**
 * email.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish;

/**
 * Librairie de gestion des emails.
 * @namespace \BoondManager\Lib
 */
class Email {
	/**
	 * Instance de la variable de gestion des emails.
	 * @var \SMTP [\SMTP](http://fatfreeframework.com/smtp).
	 */
	protected $smtp = null;

	/**
	 * Constructeur.
	 */
	public function __construct() {
		$this->reset();
	}

	/**
	* Initialise les variables de la classe.
	*/
	public function reset() {
		try {
			if(isset($this->smtp)) unset($this->smtp);
			$f3 = \Base::instance();
			$this->smtp = new \SMTP($f3->get('MAIN_SMTP.HOST'), $f3->get('MAIN_SMTP.PORT'), $f3->get('MAIN_SMTP.SCHEME'), $f3->get('MAIN_SMTP.LOGIN'), $f3->get('MAIN_SMTP.PWD'));
		} catch(\Exception $e) {
			Tools::logError($e->getMessage());
		}
	}

	/**
	* Envoi un email.
	* @param string $to      Email du destinataire.
	* @param string $subject Objet de l'email.
	* @param string $message Corps de l'email.
	* @param string $from    Nom & Email de l'expéditeur
	* 2 formats sont possibles, exemple :
	* - `BoondManager <contact@bondmanager.com>`
	* - `contact@bondmanager.com`
	* @param string $replyto Email de réponse à cette email_, toutes les messageries ne gèrent pas forcément le tag `Reply-To` dans l'en-tête de l'email_.
	* @return  boolean `true` si l'envoi du mail a réussi, `false` sinon.
	*/
	public function Send($to, $subject, $message, $from, $replyto = '') {
		$contextId = Tools::getContext()->startTiming('sending email');
		$mailSent = false;
		try {
			list($FromMail, $FromName, $full) = $this->extractNameAndMail($from);

			if(!$FromMail) throw new \Exception('no mail found ('. $from.')');

			$this->smtp->set('From', $full);
			$this->smtp->set('X-Sender', '<'.$FromMail.'>');
			$this->smtp->set('X-auth-smtp-user', '<'.$FromMail.'>');
			$this->smtp->set('X-abuse-contact', '<'.$FromMail.'>');
			$this->smtp->set('Return-Path', '<'.$FromMail.'>');
			$this->smtp->set('X-Priority', 3);

			list($mail, $name, $full) = $this->extractNameAndMail($to);
			$this->smtp->set('To', $full);
			if($replyto != '') {
				list($mail, $name, $replyto) = $this->extractNameAndMail($replyto);
				$this->smtp->set('Reply-To', $replyto);
			}

			$this->smtp->set('Subject', $subject);
			$mailSent = $this->smtp->send($message);
		} catch(\Exception $e) {
			// mail can be use to log errors, if it can\'t be sent, it must be logged using another tool
			// to avoid an inifinite loop of sendMail|fail|repportError
			syslog(LOG_ERR, $e->getMessage());
		}
		Tools::getContext()->endTiming($contextId);
		return $mailSent;
	}

	private function extractNameAndMail($string){
		if(preg_match("/(.*) <(.*)>/", $string, $matchexp)) {
			$mail = trim($matchexp[2]);
			$from = trim($matchexp[1]);
		} else {
			$mail = trim($string);
			$from = $mail;
		}
		return [$mail, $from, '"'.$from.'" <'.$mail.'>'];
	}

	/**
	 * Attache un fichier à un email.
	 *
	 * Doit être appelé avant la fonction {@see \Wish\Email::Send() Send}.
	 * @param string $filename Chemin du fichier à attacher.
	 * @param string $alias Nom du fichier à attacher.
	 */
	public function addFile($filename, $alias = null)
	{
		try {
			$this->smtp->attach($filename, $alias);
		} catch (\Exception $e) {
			Tools::logError($e->getMessage());
		}
	}

	public function getLog(){
		return $this->smtp->log();
	}
}
