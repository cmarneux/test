<?php
/**
 * encoderobject.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Encoders;

/**
 * Interface EncoderInterface
 * @package Wish\Encoders
 */
interface EncoderInterface{
    /**
     * @param object $data
     * @return array
     */
    public function serialize($data);
}
