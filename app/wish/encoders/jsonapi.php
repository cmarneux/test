<?php
/**
 * jsonapi.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Encoders;

use BoondManager\APIs\Employees\Filters\Tool;
use BoondManager\Services\BM;
use Wish\Models\PublicFieldsInterface;
use Wish\Tools;
use Wish\Models\JSONApiObjectInterface;

/**
 * Class JsonAPI
 * @package BoondManager\Lib\Encoders
 */
class JsonAPI extends \Prefab implements EncoderInterface{

	/**
	 * an array to keep trace of each object encoded so we don't encode it twice
	 * @var array
	 */
	private $objectsDefinition = [];

	private $includes = [];

	private $includeEmptyObjects = false;

	public function setIncludeEmptyObjects($bool){
		$this->includeEmptyObjects = $bool;
		return $this;
	}

	/**
	 * prepare the object to fit the JsonAPI
	 * @param object $object
	 * @return array
	 */
	public function serialize($object){
		$this->reset();

		$data = $this->_serialize($object);
		$this->reduceIncludes();

		$root = [];

		if($data) $root['data'] = $data;
		if($this->includes) $root['included'] = $this->includes;

		return $root;
	}

	private function reset(){
		$this->includes = [];
		$this->objectsDefinition = [];
	}

	/**
	 * map the object to JSONApi, all relationships are put in the $includes
	 * @param mixed $data
	 * @param bool $simplify
	 * @return array
	 * @throws \Exception
	 */
	private function _serialize($data, $simplify = false){
		if($data instanceof JSONApiObjectInterface){
			$relationships = $data->getRelationships();
			$linksRelationships = [];
			foreach($relationships as $key=>$object){
				if($object) {
					$linksRelationships[ $key ] = [];
					// louis : FIXME : me crée un pb si l'on instancie directement notre objet. Dans quel cas cet unset resolvait-il un pb ?
					// unset($data->$key);// On supprime les éventuels doublons de attributs (une dépendance est prioritaire)
					if(is_array($object)) {
						$linksRelationships[ $key ]['data'] = [];
						foreach($object as $o) {
							$linksRelationships[ $key ]['data'][] = $this->encodeRelationship($o);
						}
					}else{
						$linksRelationships[ $key ]['data'] = $this->encodeRelationship($object);
					}
				}
			}

			$attributes = $this->translatePublicAttributes($data);
			if($simplify) {
				$jsonData = $attributes;
				foreach ($linksRelationships as $key => $relationship)
					$jsonData[$key] = $relationship['data'];
			} else {
				unset($attributes['id']);// On supprime l'id car il apparait déjà un cran au dessus
				$jsonData = [
					'id' => $data->getID(),
					'type' => $data->getType(),
				];

				$this->objectsDefinition[spl_object_hash($data)] = $jsonData;

				if ($attributes) $jsonData['attributes'] = $attributes;
				if ($linksRelationships) $jsonData['relationships'] = $linksRelationships;
			}
			return $jsonData;
		}else if(is_array($data)){
			foreach($data as &$item){
				$item = $this->_serialize($item, $simplify);
			}
			return $data;
		}else {
			return $data;
		}
	}

	private function encodeRelationship($object){
		if(!$object instanceof JSONApiObjectInterface) {
			throw new \Exception('you should check that '.get_class($object).' is a RowObject');
		}

		$rel = [
			'id'   => $object->getID(),
			'type' => $object->getType()
		];

		$hash = spl_object_hash($object);

		if(!isset($this->objectsDefinition[$hash])){
			$this->objectsDefinition[$hash] = true; // !important : it reserve the space before serializing it
			$serializedObject = $this->_serialize($object);
			$this->objectsDefinition[$hash] = $serializedObject;
			$this->includes[] = $serializedObject;
		}

		return $rel;
	}

	/**
	 * TODO : publicMapping recursif
	 * TODO 2 : (louis) doit s'appeler encodeAttributes et ne plus prendre en compte la conversion dbKey -> fieldName
	 * @param $data
	 * @param $includes
	 * @return array
	 */
	private function translatePublicAttributes($data){
		$id = Tools::getContext()->startTiming('translatePublicAttributes', true);
		if($data instanceof PublicFieldsInterface) {
			$attributes = [];

			// récursion
			foreach ($data->getAttributes() as $key => $value) {
				$publicName = ($data->isDBName($key)) ? $data->getPublicName($key) : $key;
				if(!$publicName) {
					if($key && $value instanceof JSONApiObjectInterface)
						$attributes[$key] = $this->_serialize($value, $simplify = true);
					else continue;
				}else if ($value instanceof PublicFieldsInterface) {
					$attributes[ $publicName ] = $this->translatePublicAttributes($value);
				}else if (Tools::isArray($value)) {
					// on s'assure d'avoir à minima un tableau
					if( $data->shouldEncodeField($key) ) {
						$attributes[ $publicName ] = [];

						$translatedItem = false;
						foreach ($value as $k2 => $item) {
							if ($item instanceof PublicFieldsInterface) {
								$attributes[ $publicName ][ $k2 ] = $this->translatePublicAttributes($item);
								$translatedItem                   = true;
							}else if(is_scalar($item)){
								$attributes[ $publicName ][ $k2 ] = $item;
							}
						}
						if (!$translatedItem && $value) $attributes[ $publicName ] = $value;
					}
				} else {
					if($data->shouldEncodeField($key)) {
						$attributes[ $publicName ] = $data->getPublicValue($key);
					}
				}
			}

			Tools::getContext()->endTiming($id);
			return $attributes;
		}else if($data instanceof JSONApiObjectInterface){
			Tools::getContext()->endTiming($id);
			return $data->getAttributes();
		}else{
			Tools::getContext()->endTiming($id);
			return $data;
		}
	}

	/**
	 * merge all identical object (based on ID and Type)
	 * @return array
	 */
	private function reduceIncludes(){
		$ctxID = Tools::getContext()->startTiming('reduceIncludes');
		$reducedIncludes = [];
		foreach($this->includes as $object){
			$key = $object['id'].'-'.$object['type'];
			if($this->includeEmptyObjects || $object['attributes'] || $object['relationships']) {
				if (!array_key_exists($key, $reducedIncludes))
					$reducedIncludes[ $key ] = $object;
				else {
					if($reducedIncludes[ $key ]['attributes'] || $object['attributes']) {
						$reducedIncludes[ $key ]['attributes'] = array_merge($reducedIncludes[ $key ]['attributes'] ? $reducedIncludes[ $key ]['attributes'] : [], $object['attributes'] ? $object['attributes'] : []);
					}
					if (array_key_exists('relationships', $object)) {
						if (array_key_exists('relationships', $reducedIncludes[ $key ])) {
							$reducedIncludes[ $key ]['relationships'] = array_merge($reducedIncludes[ $key ]['relationships'], $object['relationships']);
						} else {
							$reducedIncludes[ $key ]['relationships'] = $object['relationships'];
						}
					}
				}
			}
		}
		$this->includes = array_values($reducedIncludes);
		Tools::getContext()->endTiming($ctxID);
	}
}
