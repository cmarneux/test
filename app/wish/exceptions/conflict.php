<?php
/**
 * conflict.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Exceptions;

/**
 * Class Conflict
 * @package Wish\Exceptions
 */
class Conflict extends Custom{
	/**
	 * Conflict constructor.
	 * @param string $message
	 * @param int $code
	 * @param \Exception|null $previous
	 */
	public function __construct($message = 'Conflict. Please check your parameters and resubmit', $code = 409, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}
