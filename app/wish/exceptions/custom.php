<?php
/**
 * custom.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Exceptions;

/**
 * Class Custom
 * @package Wish\Exceptions
 */
abstract class Custom extends \Exception{}
