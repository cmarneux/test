<?php
/**
 * databaseintegrity.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish\Exceptions;

/**
 * Class DatabaseIntegrity
 * @package Wish\Exceptions
 */
class DatabaseIntegrity extends Custom {
	/**
	 * DatabaseIntegrity constructor.
	 * @param string $message
	 * @param int $code
	 * @param \Exception|null $previous
	 */
	public function __construct($message = 'Database integrity', $code = 500, \Exception $previous = null) {
		parent::__construct($message, $code, $previous);
	}
}
