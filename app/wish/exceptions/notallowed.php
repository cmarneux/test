<?php
/**
 * notallowed.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Exceptions;

/**
 * Class NotAllowed
 * @package Wish\Exceptions
 */
class NotAllowed extends Custom{
	/**
	 * NotAllowed constructor.
	 * @param string $message
	 * @param int $code
	 * @param \Exception|null $previous
	 */
	public function __construct($message = 'Not Allowed', $code = 403, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}
