<?php
/**
 * notfound.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Exceptions;

/**
 * Class NotFound
 * @package Wish\Exceptions
 */
class NotFound extends Custom{
	/**
	 * NotFound constructor.
	 * @param string $message
	 * @param int $code
	 * @param \Exception|null $previous
	 */
	public function __construct($message = 'Object not found', $code = 404, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}
