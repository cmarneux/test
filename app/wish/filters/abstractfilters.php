<?php
/**
 * abstractfilter.php
 * @author louis.darras@wishgroupe.com
 */
namespace Wish\Filters;

use Wish\Filters\Inputs\InputInterface;
use Wish\Filters\Inputs\InputValue;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class Filters
 *
 * Use for building a filter to check all parameters sent by user
 *
 * @package BoondManager\Models
 */
abstract class AbstractFilters implements InputInterface, ToObjectInterface{
	/**#@+
	 * BoondManager environment execution
	 * @var int
	 **/
	const ERROR_NO_VALID_REQUEST = 'Incorrect request';
	/**#@-*/

	/**
	 * map filter to an object
	 * @var string
	 */
	protected $_objectClass = null;

	/**
	 * list of InputValue ( or available parameters)
	 * @var InputInterface[]
	 */
	private $_inputValues = [];


	/**
	 * @var array
	 */
	private $_rawValues = [];

	/**
	 * @var bool
	 */
	private $_isDefined = false;

	/**
	 * @var bool
	 * @deprecated
	 */
	private $_retrieveDefaultValueIfNone = false;

	/**
	 * @var bool
	 */
	private $_isRequired = false;

	/**
	 * @var null
	 */
	private $_isValid = null;

	/**
	 * @var string $name filter name
	 */
	private $_name;

	/**
	 * @var
	 */
	private static $ctxID;

	/**
	 * @var bool
	 */
	private $_allowEmptyValue = true;

	/**
	 * @var array
	 */
	private $errors = [];

	/**
	 * @var InputInterface
	 */
	private $_parent;

	/**
	 * AbstractFilters constructor.
	 * @param null $name
	 */
	public function __construct($name = null){
		$this->_name = $name ? $name : static::class;
	}

	/**
	 * @param InputInterface $parent
	 * @return $this
	 */
	public function setParent(InputInterface $parent){
		$this->_parent = $parent;
		return $this;
	}

	/**
	 * @param bool $bool
	 * @return $this
	 */
	public function setAllowEmptyValue($bool = true)
	{
		$this->_allowEmptyValue = boolval($bool);
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isAllowingEmptyValue()
	{
		return $this->_allowEmptyValue;
	}

	/**
	* set values to the parameters and check them
	*
	* @param array $data
	* @return $this
	*/
	public function setData($data){
		if(!is_array($data)) {
			$data = [$data];
		}
		$this->_isDefined = true;
		$this->_rawValues  = [];
		foreach($data as $name => $value){
			if(array_key_exists($name, $this->_inputValues)){
				$this->_inputValues[$name]->setValue($value);
				$this->_rawValues[$name] = $value;
			}
		}
		return $this;
	}

	/**
	 * @param bool $bool
	 * @deprecated
	 */
	public function replaceMissingValuesWithDefault( $bool = true){
		$this->_retrieveDefaultValueIfNone = $bool;
	}

	/**
	 * set values to the parameters and check them
	 * by calling {@see self::isValid() isValid()}
	 *
	 * @param array $data
	 * @return $this
	 */
	public function setAndFilterData(array $data){
		$this->setData($data);
		$this->isValid();
		return $this;
	}

	/**
	 * Add a parameter to the filter
	 * @param \Wish\Filters\Inputs\InputInterface|\Wish\Filters\Inputs\InputInterface[] $input
	 * @throws \Exception if the parameter is not an instance of InputValue
	 * @return $this
	 */
	public function addInput($input){
		if(!is_array($input)) $input = [$input];
		foreach($input as $i){
			if($i instanceof InputInterface) {
				$this->_inputValues[ $i->getName() ] = $i;
				$i->setParent($this);
			}
			else throw new \Exception('The Input must be an instance of Filters\AbstractInput');

		}
		return $this;
	}


	/**
	 * Get a list of all parameters
	 * @return InputInterface[]
	 */
	public function getDataFilters(){
		return $this->_inputValues;
	}

	/**
	 * disable a parameter from the filter
	 * @TODO pour le moment, c'est un alias de removeInput. A terme, il faudrait empecher l'input en écriture et lever une erreur en cas de modification par l'utilisateur
	 * @param string $name the parameter's name
	 * @return $this
	 */
	public function disableInput($name){
		$this->removeInput($name);
		return $this;
	}

	/**
	 * Remove a parameter from the filter
	 * @param string $name the parameter's name
	 * @return $this
	 */
	public function removeInput($name) {
		if(array_key_exists($name, $this->_inputValues)){
			unset($this->_inputValues[$name]);
		}
		return $this;
	}

	/**
	 * delete all fields' value.
	 * May require a call to {@see self::isValid() isValid()} in order to reload default values before usage
	 *
	 * @param array $except fields' name wich will not be reseted
	 * @return $this
	 */
	public function reset(array $except = []){
		$this->_isDefined = false;
		foreach($this->_inputValues as $input){
			if(in_array($input->getName(), $except)) continue;
			$input->reset();
		}
		return $this;
	}

	/**
	 * reset validation
	 * @return $this
	 */
	public function resetValidation() {
		$this->_isValid = null;
		$this->resetParentValidation();
		return $this;
	}

	/**
	 * Check all filters
	 *
	 * @return bool
	 */
	public function isValid(){
		if(!self::$ctxID){
			// variable static car on ne s'interresse pas au sous-validation du formulaire
			self::$ctxID = $ctxID = Tools::getContext()->startTiming('filter validation');
		}
		// the validation might have been forced at false by invalidate
		if(!is_null($this->_isValid))
			return $this->_isValid;

		$ok = $this->validateInputs();

		if($ok){
			$this->postValidation();
			$ok = $this->validateInputs();
		}

		if(!$this->isAllowingEmptyValue() && !$this->getValue()){
			$this->invalidate(self::ERROR_NO_VALID_REQUEST);
		}

		if(isset($ctxID)){
			// utilisation de la variable locale car on ne veut pas qu'un sous formulaire mette fin au benchmark
			Tools::getContext()->endTiming($ctxID);
		}

		return ($ok && !is_null($this->_isValid)) ? $this->_isValid : $ok;
	}

	/**
	 * @return bool
	 */
	protected function validateInputs(){
		$ok = true;

		foreach($this->_inputValues as $inputValue){
			$ok &= $inputValue->isValid();
		}

		return boolval($ok);
	}

	/**
	 * Check all filters (alias of isValid)
	 *
	 * @return $this
	 */
	public function filter(){
		$this->isValid();
		return $this;
	}

	/**
	 * get a list of fields name that failed the {@see self::isValid() isValid()} test
	 * @return array
	 */
	public function getInvalidDatas(){
		$nonValidDatas = array();
		foreach($this->_inputValues as $input){
			if(!$input->isValid()){
				if($input instanceof AbstractFilters){
					$nonValidDatas[$input->getName()] = $input->getInvalidDatas();
				}else
					$nonValidDatas[$input->getName()] = $input->getError();
			}
		}

		if($this->errors) $nonValidDatas['_data'] = $this->errors;

		return $nonValidDatas;
	}

	/**
	 * retrieve a parameter from its name
	 * @param string $name filter's name
	 * @return InputValue|null
	 */
	public function get($name){
		return array_key_exists($name, $this->_inputValues)?$this->_inputValues[$name]:null;
	}

	/**
	 * retrieve a parameter from its name
	 * @param $name
	 * @return \Wish\Filters\Inputs\InputValue
	 */
	public function __get($name){
		return $this->get($name);
	}

	/**
	* return a filter list as an array
	* ex : array(
	*      'pr' => [0, 1],
	*      'kw' => 'keywords to search ...',
	*      'ot' => 'asc'
	* );
	*
	* @return array
	*/
	public function toArray(){
		$data = array();
		foreach($this->getDataFilters() as $filter){
			if(!$filter->isDisabled() && $filter->isDefined()) {
				$data[ $filter->getName() ] = $filter->getValue();
			}
		}

		return $data;
	}

	/**
	 * build a url query with all filters and their values
	 * @return string
	 */
	public function toUrl(){
		$params = $this->toArray();
		return http_build_query($params);
	}

	/**
	 * clone the filter and all its InputValue
	 */
	public function __clone(){
		foreach($this->_inputValues as $key => $input){
			$this->_inputValues[$key] = clone $input;
		}
	}

	/**
	* set the input value
	* @param mixed $value
	* @return $this
	*/
	public function setValue($value)
	{
		return $this->setData($value);
	}

	/**
	 * set multiple default values
	 * @param array $data
	 * @return $this
	 * @throws \Exception
	 */
	public function setDefaultValue($data){
		if(!is_array($data))
			throw new \Exception('data must be an array');

		foreach($data as $inputName=>$value){
			if(array_key_exists($inputName, $this->_inputValues)){
				$input = $this->_inputValues[$inputName];
				$input->setDefaultValue($value);
			}
		}
		return $this;
	}

	/**
	* get the input value
	* @return mixed
	*/
	public function getValue()
	{
		return $this->toArray();
	}

	/**
	* get the input name
	* @return string
	*/
	public function getName()
	{
		return $this->_name;
	}

	/**
	* set the input name
	* @param string $name
	* @return $this
	*/
	public function setName($name)
	{
		$this->_name = $name;
		return $this;
	}

	public function isDefined(){
		return $this->_isDefined;
	}

	/**
	 * @return bool
	 */
	public function isRequired(){
		return $this->_isRequired;
	}

	/**
	 * @param bool $value
	 * @return $this
	 */
	public function setRequired($value){
		$this->_isRequired = boolval($value);
		return $this;
	}

	/**
	 * get the raw value
	 * @return mixed
	 */
	public function getRawValue()
	{
		return $this->_rawValues;
	}

	/**
	 * @return mixed
	 */
	public function getDefaultValue()
	{
		$defaultValues = [];
		foreach($this->_inputValues as $inputName=> $input){
			$defaultValues[$inputName] = $input->getDefaultValue();
		}
		return $defaultValues;
	}

	private $disabled = false;
	/**
	 * @param bool $bool
	 * @return $this
	 */
	public function setDisabled($bool = true)
	{
		$bool = boolval($bool);
		$this->disabled = $bool;

		foreach($this->_inputValues as $inputName=> $input){
			$input->setDisabled($bool);
		}
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isDisabled()
	{
		return $this->disabled;
	}

	/**
	 * perform a cross validation between inputs (last part of the validation)
	 * @return boolean
	 */
	protected function postValidation() { return true; }

	/**
	 * flag the input as invalid
	 * @param string|int $error


	 * @return $this
	 */
	public function invalidate($error = null){
		$this->_isValid = false;
		$this->errors   = $error;
		$this->resetParentValidation();
		return $this;
	}

	/**
	 * @return $this
	 */
	protected function resetParentValidation() {
		if($this->_parent) $this->_parent->resetValidation();
		return $this;
	}


	public function toObject(){
		if($this->_objectClass)
			$object = new $this->_objectClass;
		else
			$object = new Model();

		foreach($this->getDataFilters() as $filter){
			if(!$filter->isDisabled() && $filter->isDefined()) {
				$object->{$filter->getName()} = ($filter instanceof ToObjectInterface) ? $filter->toObject() : $filter->getValue();
			}
		}
		return $object;
	}
}
