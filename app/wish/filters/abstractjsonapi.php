<?php
/**
 * Created by PhpStorm.
 * User: Wish
 * Date: 26/11/2016
 * Time: 16:06
 */
namespace Wish\Filters;

use Wish\Filters\Inputs\InputMultiRelationships;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Models\Model;

/**
 * Class AbstractJsonAPI
 * @package BoondManager\Lib\Filters
 * @property Relationships relationships
 * @property Attributes attributes
 */
abstract class AbstractJsonAPI extends AbstractFilters
{
	/**
	 * AbstractJsonAPI constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$relationships = new Relationships;
		$relationships->setName('relationships');
		$this->addInput($relationships);

		$attributes = new Attributes();
		$attributes->setName('attributes');
		$this->addInput($attributes);
	}

	/**
	 * @param Inputs\InputInterface|Inputs\InputInterface[] $input
	 * @return $this
	 */
	public function addInput($input)
	{
		if (!is_array($input)) $input = [$input];
		foreach ($input as $i) {
			if ($i instanceof Relationships || $i instanceof Attributes) {
				parent::addInput($i);
			} else if ($i instanceof InputRelationship || $i instanceof InputMultiRelationships) {
				$this->relationships->addInput($i);
			} else
				$this->attributes->addInput($i);
		}
		return $this;
	}

	/**
	 * Remove a parameter from the filter
	 * @param string $name the parameter's name
	 * @return $this
	 */
	public function disableInput($name)
	{
		$this->relationships->disableInput($name);
		$this->attributes->disableInput($name);
		return $this;
	}

	/**
	 * retrieve a parameter from its name
	 * @param string $name filter's name
	 * @return \Wish\Filters\Inputs\InputValue|null
	 */
	public function get($name)
	{
		if (in_array($name, ['attributes', 'relationships'])) return parent::get($name);
		else if ($attr = $this->attributes->get($name)) return $attr;
		else if ($relation = $this->relationships->get($name)) return $relation;
		else return null;
	}

	public function toObject()
	{
		if($this->_objectClass)
			$object = new $this->_objectClass;
		else
			$object = new Model();

		foreach($this->attributes->getDataFilters() as $filter){
			if(!$filter->isDisabled() && $filter->isDefined()) {
				$object->{$filter->getName()} = (method_exists($filter, 'toObject')) ? $filter->toObject() : $filter->getValue();
			}
		}

		foreach($this->relationships->getDataFilters() as $filter){
			if(!$filter->isDisabled() && $filter->isDefined()) {
				$object->{$filter->getName()} = (method_exists($filter, 'toObject')) ? $filter->toObject() : $filter->getValue();
			}
		}
		return $object;
	}
}

/**
 * Class Relationships
 * @package Wish\Filters
 */
class Relationships extends AbstractFilters{}

/**
 * Class Attributes
 * @package Wish\Filters
 */
class Attributes extends AbstractFilters{}
