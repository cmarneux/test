<?php
/**
 * abstractinput.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

/**
 * Interface AbstractInput
 * @package Wish\Filters\Inputs
 */
interface AbstractInput{
	/**
	 * Check the input value match all filters
	 * @return bool
	 */
	public function isValid();

	/**
	 * set the input value
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value);

	/**
	 * get the input value
	 * @return mixed
	 */
	public function getValue();

	/**
	 * get the input name
	 * @return string
	 */
	public function getName();

	/**
	 * set the input name
	 * @param string $name
	 * @return $this
	 */
	public function setName($name);

	/**
	 * reset the input
	 * @return mixed
	 */
	public function reset();

	/**
	 * @return boolean
	 */
	public function isRequired();

	/**
	 * @param boolean $value
	 * @return mixed
	 */
	public function setRequired($value);

	/**
	 * @return boolean mixed
	 */
	public function isDefined();
}
