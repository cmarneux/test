<?php
/**
 * inputboolean.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Filters\Inputs;

/**
* Class InputBoolean
* @package Wish\Filters\Inputs
*/
class InputBoolean extends InputValue{
	/**
	 * InputBoolean constructor.
	 * @param string $name
	 * @param bool $defaultValue
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name, $defaultValue = false, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE)
	{
		parent::__construct($name, $defaultValue, $required, $mode);
		$this->addFilter(FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
	}
}
