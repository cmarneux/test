<?php
/**
 * inputdate.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Filters\Inputs;

use Wish\Tools;

/**
 * Class InputDate
 * @package Wish\Filters\Inputs
 */
class InputDate extends InputValue{
	/**
	* InputDate constructor.
	* @param string $name
	* @param null $defaultValue if `null`, the default value is the current date
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name, $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		if(!$defaultValue) $defaultValue = date('Y-m-d', time());
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->addFilterCallback(function($value){
			if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value)) {
				return $value;
			}else if ($date = \DateTime::createFromFormat(\DateTime::ISO8601, $value)){
				return $date->format('Y-m-d');
			}else {
				return false;
			}
		});
	}

	/**
	 * convert an empty date to 3000-01-01 (max date)
	 * @return $this
	 */
	public function convertEmptyDate(){
		$this->addFilterCallback(FILTER_CALLBACK, function($value){
			if($value === ''){
				return Tools::MAX_DATE;
			}else return $value;
		});
		return $this;
	}

	/**
	* @return \DateTime
	*/
	public function getDateTime(){
		return \DateTime::createFromFormat('Y-m-d', $this->value);
	}

	/**
	 * @return int
	 */
	public function getYear(){
		return intval( $this->getDateTime()->format('Y') );
	}

	/**
	 * @return int
	 */
	public function getMonth(){
		return intval( $this->getDateTime()->format('m'));
	}

	/**
	 * @return int
	 */
	public function getDay(){
		return intval( $this->getDateTime()->format('d'));
	}

	/**
	 * @return int
	 */
	public function getTimestamp() {
		return intval( $this->getDateTime()->getTimestamp() );
	}
}
