<?php
/**
 * inputdatetime.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Filters\Inputs;

/**
 * Class InputDate
 * @package Wish\Filters\Inputs
 */
class InputDateTime extends InputValue{
    /**
     * InputDateTime constructor.
     * @param string $name
     * @param null $defaultValue if `null`, the default value is the current dateTime
     * @param bool $required
     * @param int $mode
     */
    public function __construct($name, $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
        if(!$defaultValue) $defaultValue = date('Y-m-d H:i:s', time());
        parent::__construct($name, $defaultValue, $required, $mode);

        $this->addFilter(FILTER_CALLBACK, function($value){
            if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $value)) return $value;
			else if ($date = \DateTime::createFromFormat(\DateTime::ISO8601, $value)){
				return $date->format('Y-m-d H:i:s');
			}
            else return false;
        });
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(){
        return \DateTime::createFromFormat('Y-m-d H:i:s', $this->value);
    }
}
