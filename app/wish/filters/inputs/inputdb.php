<?php
/**
 * inputdb.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Filters\Inputs;

use Wish\MySQL\Query;
use Wish\MySQL\Where;
use Wish\Models\Model;
use BoondManager\Databases\Local\AbstractObject;

/**
 * Class InputBoolean
 * @package Wish\Filters\Inputs
 */
class InputDB extends InputValue
{
	/**
	 * @var false|Model
	 */
	private $dbEntry = false;

	/**
	 * get a dbEntry
	 * @return Model|false
	 * @deprecated might cause a bug if used within a multi-input
	 */
	public function getDbObject(){
		return $this->dbEntry;
	}

	/**
	 * check the value match a row in the database
	 * @param string $table
	 * @param Where|string $where
	 * @param string $db `local` or `boondmanager`
	 * @return $this
	 * @throws \Exception
	 */
	public function addFilterExistsInDB($table, $where, $db = 'local'){
		if(is_string($where)) $where = new Where($where);
		if( ! $where instanceof Where) throw new \Exception('second param must be a Where or a String');
		$this->addFilterCallback(function($value) use ($table, $where, $db){
			return $this->existsInDB($value, $table, $where, $db);
		}, self::ERROR_ENTITY_DOES_NOT_EXIST);
		return $this;
	}

	/**
	 * check the value match a row in the database
	 * @param mixed @value
	 * @param string $table
	 * @param Where $where
	 * @param string $db `local` or `boondmanager`
	 * @return bool
	 *
	 * @TODO devrait passer static
	 */
	public function existsInDB($value, $table, Where $where, $db = 'local'){
		if($db == 'boondmanager') $dbObject = new \BoondManager\Databases\BoondManager\AbstractObject();
		else $dbObject = new AbstractObject();
		$query = new Query();
		$query->select()->from($table)->addWhere($where->setArgs($value));
		$this->dbEntry = $dbObject->singleExec($query);
		return ($this->dbEntry) ? $value: false;
	}


}
