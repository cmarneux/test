<?php
/**
 * inputfile.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

class InputFile extends InputFiles{

	protected function importData() {
		$data = \Base::instance()->get('FILES');
		$this->rawValue = $data;
	}
}
