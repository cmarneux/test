<?php
/**
 * inputfile.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Wish\Filters\Inputs;

class InputFiles implements InputInterface{

	private $_name;

	private $isRequired = false;

	protected $rawValue = [];

	private $isDefined = false;

	private $isValid = null;

	private $error = null;

	private $maxSizePerFile = 0;
	private $maxTotalSize = 0;
	private $maxFile = 0;

	private $fileTypeAllowed = [];

	const ERROR_INI_SIZE = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
	const ERROR_FORM_SIZE = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
	const ERROR_PARTIAL = 'The uploaded file was only partially uploaded';
	const ERROR_NOFILE = 'No file was uploaded';
	const ERROR_TMP_DIR = 'Missing a temporary folder';
	const ERROR_CANT_WRITE = 'Failed to write file to disk';
	const ERROR_EXTENSION = 'File upload stopped by extension';
	const ERROR_MAX_SIZE = 'The uploaded file "%s" exceeds %d Mo';
	const ERROR_TOTAL_MAX_SIZE = 'The sum of uploaded files exceeds %d Mo';
	const ERROR_TOO_MUCH_FILES = 'too much files uploaded';
	const ERROR_FILE_EXTENSION = 'File type not allowed';

	/**
	 * @var InputInterface
	 */
	private $_parent;

	public function __construct($name)
	{
		$this->setName($name);
	}

	/**
	 * Check the input value match all filters
	 * @return bool
	 */
	public function isValid() {
		if(!is_null($this->isValid))
			return $this->isValid;

		$this->importData();

		$this->isValid = true;
		$totalSize = 0;

		if($this->isRequired && !$this->getTotalFile()) {
			$this->error = InputValue::ERROR_REQUIRED;
			return $this->isValid = false;
		}

		if($this->maxFile && $this->getTotalFile() > $this->maxFile) {
			$this->error = self::ERROR_TOO_MUCH_FILES;
			return $this->isValid = false;
		}

		// TODO: ameliorer le isValid pour prendre en compte des regles plus poussée (type de fichier, taille max, etc)
		foreach($this->rawValue as $k=>$data){
			if($data['error']) {
				switch ($data['error']){
					case UPLOAD_ERR_INI_SIZE: $this->error = self::ERROR_INI_SIZE; break;
					case UPLOAD_ERR_FORM_SIZE: $this->error = self::ERROR_FORM_SIZE; break;
					case UPLOAD_ERR_PARTIAL: $this->error = self::ERROR_PARTIAL; break;
					case UPLOAD_ERR_NO_FILE: $this->error = self::ERROR_NOFILE; break;
					case UPLOAD_ERR_NO_TMP_DIR: $this->error = self::ERROR_TMP_DIR; break;
					case UPLOAD_ERR_CANT_WRITE: $this->error = self::ERROR_CANT_WRITE; break;
					case UPLOAD_ERR_EXTENSION: $this->error = self::ERROR_EXTENSION; break;
					default: $this->error = 'unknown error';
				}
				return $this->isValid = false;
			}
			if($this->maxSizePerFile && $data['size'] > $this->maxSizePerFile) {
				$this->error = sprintf(self::ERROR_MAX_SIZE, $data['name'], $this->maxSizePerFile/(1024*1024));
				return $this->isValid = false;
			}
			if($this->fileTypeAllowed && !$this->checkFileType($data['tmp_name'])) {
				$this->error = self::ERROR_FILE_EXTENSION;
				return $this->isValid = false;
			}
			$totalSize += $data['size'];
		}

		if($this->maxTotalSize && $totalSize > $this->maxTotalSize){
			$this->error = sprintf(self::ERROR_TOTAL_MAX_SIZE, $this->maxTotalSize / (1024*1024));
			return $this->isValid = false;
		}


		return $this->isValid;
	}

	public function checkFileType($file){
		if(!$this->fileTypeAllowed) return true;
		$mime = mime_content_type($file);

		if($extStart = strrpos($file, '.') )
			$ext = substr($file, $extStart);

		foreach ($this->fileTypeAllowed as $entry){
			if(substr($entry, 0, 1) == '.') {
				if(isset($ext) && $ext == $entry) return true;
			} else {
				if($entry == $mime) return true;
			}
		}
		return false;
	}

	/**
	 * validate the input
	 * @return $this
	 */
	public function filter() {
		$this->isValid();
		return $this;
	}

	/**
	 * @param $int
	 * @return $this
	 */
	public function setMaxFile($int){
		$this->maxFile = $int;
		return $this;
	}

	public function setMaxTotalSize($size, $unit = 'Mo') {
		$this->maxTotalSize = $this->convertSize($size, $unit);
		return $this;
	}

	/**
	 * @param int $size
	 * @param string $unit o|Ko|Mo|Go
	 * @return $this
	 */
	public function setMaxFileSize($size, $unit = 'Mo'){
		$this->maxSizePerFile = $this->convertSize($size, $unit);;
		return $this;
	}

	/**
	 * @param $size
	 * @param string $unit  o|Ko|Mo|Go
	 * @return mixed
	 * @throws \Exception
	 */
	protected function convertSize($size, $unit = 'Mo'){
		$coef = 1;
		switch($unit){
			case 'Go':
				$coef *= 1024;
			case 'Mo':
				$coef *= 1024;
			case 'Ko':
				$coef *= 1024;
			case 'o':
				break;
			default:
				throw new \Exception('unknow unit');
		}
		return $size * $coef;
	}

	/**
	 * set the input value
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value) {
		$this->importData();
		return $this;
	}

	protected function importData(){
		$data = \Base::instance()->get('FILES');
		if(is_array($data) && isset($data[$this->getName()])){
			$rawData = [];
			$file = $data[$this->getName()];
			foreach($file['tmp_name'] as $k => $value){
				$rawData[$k] = [
					'name'=>$file['name'][$k],
					'type'=>$file['type'][$k],
					'tmp_name'=>$file['tmp_name'][$k],
					'error'=>$file['error'][$k],
					'size'=>$file['size'][$k],
				];
			}
			$this->rawValue = $rawData;
		}
	}

	/**
	 * get the input value
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->getRawValue();
	}

	/**
	 * get the raw value
	 * @return mixed
	 */
	public function getRawValue()
	{
		return $this->rawValue;
	}

	/**
	 * @return int
	 */
	public function getTotalFile(){
		return count($this->rawValue);
	}

	/**
	 * @param mixed $value
	 * @return mixed
	 * @throws \Exception
	 */
	public function setDefaultValue($value) {
		throw new \Exception('no default value for this kind of input');
	}

	/**
	 * @return mixed
	 * @throws \Exception
	 */
	public function getDefaultValue() {
		throw new \Exception('no default value for this kind of input');
	}

	/**
	 * get the input name
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * set the input name
	 * @param string $_name
	 * @return $this
	 */
	public function setName($_name) {
		$this->_name = $_name;
		return $this;
	}

	/**
	 * reset the input
	 * @return $this
	 */
	public function reset()
	{
		$this->rawValue = [];
		$this->isDefined = false;
		$this->isValid = null;
		return $this;
	}

	public function resetValidation() {
		$this->isValid = null;
		$this->resetParentValidation();
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isRequired()
	{
		return $this->isRequired;
	}

	/**
	 * @param boolean $value
	 * @return $this
	 */
	public function setRequired($value)
	{
		$this->isRequired = boolval( $value );
		return $this;
	}

	/**
	 * @return boolean mixed
	 */
	public function isDefined()
	{
		return $this->isDefined;
	}

	/**
	 * @param bool $bool
	 * @return $this
	 * @deprecated (ignored)
	 */
	public function setDisabled($bool = true)
	{
		return $this;
	}

	/**
	 * @return bool
	 * @deprecated (ignored)
	 */
	public function isDisabled()
	{
		return false;
	}

	/**
	 * @return $this
	 */
	public function invalidate() {
		$this->isValid = false;
		return $this;
	}

	/**
	 * @param bool $bool
	 * @return $this
	 * @deprecated (ignored)
	 */
	public function setAllowEmptyValue($bool = true)
	{
		return $this;
	}

	/**
	 * @return boolean
	 * @deprecated (ignored)
	 */
	public function isAllowingEmptyValue()
	{
		return true;
	}

	public function getError(){
		return $this->error;
	}

	private function resetParentValidation(){
		if($this->_parent) $this->_parent->resetValidation();
		return $this;
	}

	/**
	 * @param InputInterface $input
	 * @return $this;
	 */
	public function setParent(InputInterface $input)
	{
		$this->_parent = $input;
		return $this;
	}
}
