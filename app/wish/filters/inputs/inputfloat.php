<?php
/**
 * inputfloat.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

/**
 * Class InputFloat
 * @package Wish\Filters\Inputs
 */
class InputFloat extends InputValue{
	/**
	 * InputFloat constructor.
	 * @param string $name
	 * @param bool $defaultValue
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name, $defaultValue = false, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE)
	{
		parent::__construct($name, $defaultValue, $required, $mode);
		$this->addFilter(FILTER_VALIDATE_FLOAT);
	}

	/**
	 * @param $number
	 * @param bool $exclude
	 * @return $this
	 */
	public function setMin($number, $exclude = false){
		$this->addFilterCallback(function($value) use ($number, $exclude){
			if($exclude){
				return ($value <= $number) ? false : $value;
			}else{
				return ($value < $number) ? false : $value;
			}
		});

		return $this;
	}

	/**
	 * @param $number
	 * @param bool $exclude
	 * @return $this
	 */
	public function setMax($number, $exclude = false){
		$this->addFilterCallback(function($value) use ($number, $exclude){
			if($exclude){
				return ($value >= $number) ? false : $value;
			}else{
				return ($value > $number) ? false : $value;
			}
		});

		return $this;
	}

	/**
	 * @param $number
	 * @return InputFloat
	 */
	public function setMinExcluded($number){
		return $this->setMin($number, $excluded = true);
	}

	/**
	 * @param $number
	 * @return InputFloat
	 */
	public function setMaxExcluded($number){
		return $this->setMax($number, $excluded = true);
	}

	/**
	 * round at $x digits
	 * @param $x
	 * @return $this
	 */
	public function setPrecision($x){
		$this->addFilterCallback(function($v) use ($x){
			return round($v, $x);
		});
		return $this;
	}
}
