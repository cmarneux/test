<?php
/**
 * inputid.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

use Wish\Tools;

/**
 * Class InputId
 * @package Wish\Filters\Inputs
 */
class InputId extends InputString {
	/**
	 * Id constructor.
	 * @param string $name
	 * @param bool $defaultValue
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'id', $defaultValue = false, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE) {
		parent::__construct($name, $defaultValue, $required, $mode);
	}

	/**
	 * @param array $allowedIds
	 * @return $this
	 */
	public function addFilterIdExists($allowedIds = []) {
		$this->addFilterCallback(function($value) use($allowedIds) {
			$allowedIds = Tools::useColumnAsKey('id', $allowedIds);
			return array_key_exists($value, $allowedIds) ? $value : false;
		}, self::ERROR_ENTITY_DOES_NOT_EXIST);
		return $this;
	}
}
