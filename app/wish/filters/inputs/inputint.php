<?php
/**
 * InputInt.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

/**
 * Class InputInt
 * @package Wish\Filters\Inputs
 * @TODO utiliser une classe parent InputNumber pour InputInt & InputFloat
 */
class InputInt extends InputValue{
	/**
	 * InputInt constructor.
	 * @param string $name
	 * @param bool $defaultValue
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name, $defaultValue = false, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE)
	{
		parent::__construct($name, $defaultValue, $required, $mode);
		$this->addFilter(FILTER_VALIDATE_INT);
	}

	/**
	 * @param $number
	 * @param bool $excluded
	 * @return $this
	 */
	public function setMin($number, $excluded = false){
		$this->addFilterCallback(function($value) use ($number, $excluded){
			if($excluded && $value == $number) return false;
			if($value < $number) return false;
			else return $value;
		});

		return $this;
	}

	/**
	 * @param $number
	 * @param bool $excluded
	 * @return $this
	 */
	public function setMax($number, $excluded = false){
		$this->addFilterCallback(function($value) use ($number, $excluded){
			if($excluded && $value == $number) return false;
			if($value > $number) return false;
			else return $value;
		});

		return $this;
	}

	/**
	 * @param $number
	 * @return InputInt
	 */
	public function setMinExcluded($number){
		return $this->setMin($number, true);
	}

	/**
	 * @param $number
	 * @return InputInt
	 */
	public function setMaxExcluded($number){
		return $this->setMax($number, true);
	}
}
