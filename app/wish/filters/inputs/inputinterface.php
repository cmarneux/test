<?php
/**
 * abstractinput.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

/**
 * Interface InputInterface
 * @package Wish\Filters\Inputs
 */
interface InputInterface{
	/**
	 * Check the input value match all filters
	 * @return bool
	 */
	public function isValid();

	/**
	 * validate the input
	 * @return $this
	 */
	public function filter();

	/**
	 * set the input value
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value);


	/**
	 * get the input value
	 * @return mixed
	 */
	public function getValue();

	/**
	 * get the raw value
	 * @return mixed
	 */
	public function getRawValue();

	/**
	 * @param mixed $value
	 * @return mixed
	 */
	public function setDefaultValue($value);

	/**
	 * @return mixed
	 */
	public function getDefaultValue();

	/**
	 * get the input name
	 * @return string
	 */
	public function getName();

	/**
	 * set the input name
	 * @param string $name
	 * @return $this
	 */
	public function setName($name);

	/**
	 * reset the input
	 * @return $this
	 */
	public function reset();

	/**
	 * @return boolean
	 */
	public function isRequired();

	/**
	 * @param boolean $value
	 * @return $this
	 */
	public function setRequired($value);

	/**
	 * @return boolean mixed
	 */
	public function isDefined();

	/**
	 * @param bool $bool
	 * @return $this
	 */
	public function setDisabled($bool = true);

	/**
	 * @return bool
	 */
	public function isDisabled();

	/**
	 * @return $this
	 */
	public function invalidate();

	/**
	 * @param bool $bool
	 * @return $this
	 */
	public function setAllowEmptyValue($bool = true);

	/**
	 * @return boolean
	 */
	public function isAllowingEmptyValue();

	/**
	 * @return $this
	 */
	public function resetValidation();

	/**
	 * @param InputInterface $input
	 * @return $this;
	 */
	public function setParent(InputInterface $input);
}
