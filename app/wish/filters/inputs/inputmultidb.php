<?php
/**
 * inputmultidb.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Filters\Inputs;

use Wish\MySQL\Query;
use Wish\MySQL\Where;
use BoondManager\Databases\Local\AbstractObject;

/**
 * Class InputMultiDB
 * @package Wish\Filters\Inputs
 */
class InputMultiDB extends InputMultiValues{
    /**
     * @var \Wish\Models\Model[]
     */
    private $dbEntry = [];

    /**
     * @var bool has the db already be requested
     */
    private $dbRequested = false;

    /**
     * @var Query the prepared Query
     */
    private $dbRequest = null;

    /**
     * @var string 'local' or 'boondmanager'
     */
    private $db;

    /**
     * get a dbEntry
     * @return \Wish\Models\Model|null
     */
    public function getDbObject($id){
        $this->requestDB();
        return array_key_exists($id, $this->dbEntry)?$this->dbEntry[$id]:null;
    }

	/**
	 * check the value match a row in the database
	 * @param $table
	 * @param Where $where
	 * @param string $db
	 * @return $this
	 */
    public function addFilterExistsInDB($table, Where $where, $db = 'local'){

        // prepare a query
        $query = new Query();
        $query->select()->from($table)->addWhere($where);
        $this->dbRequest = $query;
        $this->db = $db;

        $this->addFilter(FILTER_CALLBACK, function($value){
            return ($this->getDbObject($value)) ? $value: false;
        });
        return $this;
    }

    /**
     * make a request to the Database and populate self::$dbEntry with the result.
     * @throws \Exception
     */
    private function requestDB(){
        if($this->dbRequested) return;

        if($this->db == 'boondmanager') $dbObject = new \BoondManager\Databases\BoondManager\AbstractObject();
        else $dbObject = new AbstractObject();
        $this->dbRequest->getWhere()->setArgs($this->value);
        $rows = $dbObject->exec($this->dbRequest);
        if(!$rows) $rows = [];
        $this->dbEntry = $rows;
    }
}
