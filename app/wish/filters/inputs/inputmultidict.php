<?php
/**
 * inputmultidict.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

use BoondManager\Services\Dictionary;

class InputMultiDict extends InputMultiValues {

	public function __construct($name, $dictEntry = null, $defaultValue = [], $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE)
	{
		parent::__construct($name, $defaultValue, $required, $mode);

		if(!is_null($dictEntry)) $this->setDict(Dictionary::getDict($dictEntry));
	}

	/**
	 * flatten a dict entry like mobility :
	 * [
	 *   'label'=> [
	 *     ['key1', 'value', ...],
	 *     ['key2', 'value', ...],
	 *   'label2'=> [
	 *     ['key3', 'value', ...],
	 *     ['key4', 'value', ...],
	 * ]
	 *
	 * to
	 * [
	 *     ['key1', 'value', ...],
	 *     ['key2', 'value', ...],
	 *     ['key3', 'value', ...],
	 *     ['key4', 'value', ...],
	 * ]
	 * @param array $dict
	 * @param string $subDictKey
	 * @return array
	 */
	private function flattenDict(array $dict, $subDictKey = 'option'){

		$newDict = [];
		foreach($dict as $subDict){
			if(isset($subDict[ $subDictKey ]) && is_array($subDict[ $subDictKey ])) {
				$newDict = array_merge($newDict, $this->flattenDict($subDict[ $subDictKey ]));
			}
			else return $dict;
		}
		return $newDict;
	}
	/**
	 * Add a filter to check that the value belongs to a dictionary {@see self::addFilterInArray() addFilterInArray}
	 *
	 * A dictionary must be an array with 2 dimensions, the verification will be made on with the [X][0] value
	 * @param array $dict
	 * @param array $moreValues
	 * @param string $column
	 * @param string $assertText
	 * @return $this
	 */
	public function setDict(array $dict, array $moreValues = [], $column = 'id', $assertText = self::ERROR_INCORRECT_VALUE){
		$dict = $this->flattenDict($dict);

		$dictValue = array_merge(array_column($dict, $column),$moreValues);
		$this->validDataList = $dictValue;
		$this->addFilter(FILTER_CALLBACK, function($value) use ($dictValue){
			$sValue = strval($value);
			foreach($dictValue as $dv) {
				if(strval($dv) === $sValue ) return $sValue;
			}
			return false;
		}, null, $assertText);
		return $this;
	}

	/**
	 * add a filter that check the value belongs to a list of dictionary's entries
	 * @param array $dictArray a 3 dim array
	 * the first level key will be used as the `topKey` @see self::getTopKey()
	 * and the fird level, first value will be used as the value (in the example, `10`, `11` and `12`)
	 * example:
	 * [
	 *      0=>[
	 *          [0=>10, 1=>'dictEntry1']
	 *          [0=>12, 1=>'dictEntry2']
	 *      ],
	 *      1=>[
	 *          [0=>11, 1=>'dictEntry3']
	 *          [0=>12, 1=>'dictEntry4']
	 *      ],
	 *
	 * ]
	 * @param string $assertText an error message
	 * @return $this
	 */
	public function setMultiDict(array $dictArray, $assertText = self::ERROR_INCORRECT_VALUE){
		$this->topKey = true;
		$validData = [];
		foreach($dictArray as $k=>$dict){
			foreach($dict as $entry){
				$validData[] = $k.'_'.$entry['id'];
			}
		}
		$this->validDataList = $validData;
		$this->addFilter(FILTER_CALLBACK, function($value) use ($validData){
			return in_array($value, $validData, true)?$value:false;
		}, null, $assertText);
		return $this;
	}

	/**
	 * get a list of accepted values
	 * @return array
	 */
	public function getListOfValidValues(){
		return $this->validDataList;
	}
}
