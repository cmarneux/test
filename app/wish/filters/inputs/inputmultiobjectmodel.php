<?php
/**
 * InputMultiObjectModel.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Wish\Filters\Inputs;

use Wish\Filters\AbstractFilters;

class InputMultiObjectModel{

	/**
	 * @var InputMultiObjects
	 */
	private $_inputMultiObject;
	/**
	 * @var AbstractFilters
	 */
	private $_model;

	/**
	 * InputMutiObjectModel constructor.
	 * @param InputMultiObjects $parent
	 * @param AbstractFilters $model
	 */
	public function __construct(InputMultiObjects $parent, AbstractFilters $model)
	{
		$this->_inputMultiObject = $parent;
		$model->setParent($parent);
		$this->_model = $model;
	}

	public function getModel() {
		return $this->_model;
	}

	// proxy to model methods, apply call to each cloned model
	public function __call($name, $arguments) {
		$forbiddenCalls = [
			'getDataFilters', 'isValid', 'filter', 'getInvalidDatas', '__clone', 'getValue', 'getDefaultValue',
			'getRawValue', 'isDefined', 'invalidate', 'postValidation', 'toObject'
		];
		if(in_array($name, $forbiddenCalls)) {
			throw new \Exception("method '$name' is not allowed on InputMultiObject model");
		}

		$proxy = $this->getArrayProxy();
		return call_user_func_array([$proxy, $name], $arguments);
	}

	private function getArrayProxy(){
		$proxy = new InputMultiObjectArrayProxy();
		$proxy->addItemToArrayProxy($this->_model);
		if($this->_inputMultiObject->getItems()) {
			foreach($this->_inputMultiObject->getItems() as $item) {
				$proxy->addItemToArrayProxy($item);
			}
		}
		return $proxy;
	}

	// proxy to fields
	public function __get($name) {
		return $this->getArrayProxy()->$name;
	}

	public function __set($name, $value){
		$this->getArrayProxy()->$name = $value;
	}
}

class InputMultiObjectArrayProxy {

	private $_items = [];

	// stupid name to avoid a collusion with proxyfied object
	public function addItemToArrayProxy($item){
		$this->_items[] = $item;
	}

	public function getSizeOfArrayProxy(){
		return count($this->_items);
	}

	// proxy to model methods, apply call to each cloned model
	public function __call($name, $arguments) {
		$proxy = new InputMultiObjectArrayProxy();
		foreach($this->_items as $item) {
			$proxy->addItemToArrayProxy(call_user_func_array([$item, $name], $arguments));
		}
		return $proxy;
	}

	// proxy to fields
	public function __get($name) {
		$proxy = new InputMultiObjectArrayProxy();
		foreach($this->_items as $item) {
			$proxy->addItemToArrayProxy($item->$name);
		}
		return $proxy;
	}

	public function __set($name, $value){
		foreach($this->_items as $item){
			$item->$name = $value;
		}
	}
}
