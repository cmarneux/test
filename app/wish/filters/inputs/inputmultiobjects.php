<?php
/**
 * inputmultiobjects.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

use Wish\Filters\AbstractFilters;
use Wish\Filters\ToObjectInterface;
use Wish\Tools;

/**
 * Class InputMultiObjects
 * Accept an array and check each entry with a given filter
 * @package Wish\Filters\Inputs
 */
class InputMultiObjects extends InputMultiValues implements ToObjectInterface{
	/**
	 * a filter to apply on each entry
	 * @var AbstractFilters
	 */
	private $modelFilter;

	/**
	 * list of values (filtered if isValid() called)
	 * @var AbstractFilters[]
	 */
	private $filtersValues;

	/**
	 * list of filters unvalid
	 * @var AbstractFilters[]
	 */
	private $unvalidFilters;

	/**
	 * @var string use a column as an index
	 */
	private $columnAsIndex = null;

	/**
	 * InputMultiObjects constructor.
	 * @param string $name
	 * @param AbstractFilters $model
	 * @param bool $required
	 * @param int $mode
	 * @throws \Exception if mode is incorret
	 */
	public function __construct($name, AbstractFilters $model, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, [], $required, $mode);

		$this->modelFilter = new InputMultiObjectModel($this, $model);

		$this->reset();
	}

	/**
	 * reset the input
	 * @return InputMultiValues
	 */
	public function reset()
	{
		$this->filtersValues = [];
		$this->unvalidFilters = [];
		return parent::reset();
	}

	/**
	 * @return bool
	 */
	public function isValid(){
		// on verifie que le test n'a pas eu lieu (sinon renvoi de la valeur du dernier test)
		if(is_null($this->isValid)){
			if($this->isDefined()){
				$validValues = [];
				$this->unvalidFilters = [];
				foreach($this->filtersValues as $val){
					if($val->isValid()) {
						$validValues[] = $val;
					}else if( $this->mode !== self::MODE_IGNORE_WRONG_ENTRY) {
						$this->unvalidFilters[] = $val;
					}
				}

				if($this->unvalidFilters)
					$this->invalidate();
				else {
					$this->validate();
					$this->filtersValues = $validValues;
				}

				if($this->isValid){
					$this->checkGroupFilters();
				}

				// succès de la validation, on sort de la
				if($this->isValid){
					if($this->columnAsIndex) $this->filtersValues = array_column($this->filtersValues, null, $this->columnAsIndex);
					return true;
				}

				//FIXME : tout les tests sur $this->value ne sont pas cohérents avec le $this->filtersValues : à revoir !

				if( (!$this->isValid || !$this->value) && $this->isRequired()){
					$this->invalidate(self::ERROR_REQUIRED);
					return false;
				}

				// on est autorisé à supprimer la valeur (utile pour les relationships)
				if($this->isAllowingEmptyValue() && !$this->getRawValue()) {
					$this->value = $this->getEmptyValue();
					$this->validate();
					return true;
				}

				if(Tools::getDebugMode() == Tools::DEBUG_OFF) {
					if ($this->mode == self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) {
						// FIXME : l'utilisation d'un defaultValue casse un appel possible à $this->getItems()
						$this->value = $this->getDefaultValue();
						if (!is_null($this->defaultIsDefined)) $this->isDefined = $this->defaultIsDefined;
						$this->validate();
						return true;
					} // champs non requis, suite des tests
					else if ($this->mode == self::MODE_IGNORE_INCORRECT_VALUE ) {
						$this->reset();
						$this->setDisabled(true);
						$this->validate();
						return true;
					}
				}

				$this->invalidate(self::ERROR_INCORRECT_VALUE);
				return false;
			}else{
				// not defined

				// cas ou l'on est non-définie OU echec de la validation
				if($this->isRequired()){
					$this->invalidate(self::ERROR_REQUIRED);
					return false;
				}
				if ($this->mode == self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) {
					$this->value = $this->getDefaultValue(); // FIXME : idem que le cas ou c'est definie
					if (!is_null($this->defaultIsDefined)) $this->isDefined = $this->defaultIsDefined;
				}

				$this->validate();
				return true;
			}
		}
		return $this->isValid;
	}

	public function setMode($mode)
	{
		if($mode == InputValue::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT){
			throw new \Exception('mode replace incorrect value with default disabled on InputMultiObjects');
		}
		return parent::setMode($mode);
	}

	protected function checkGroupFilters(){
		$values = $this->filtersValues;
		foreach($this->groupedFilter as $filtersARGS){
			array_unshift($filtersARGS, $values);
			if($filtersARGS[1] == FILTER_CALLBACK){
				$values = $this->applyCallback($filtersARGS[0], $filtersARGS[2]['options']);
			}else{
				$values = call_user_func_array('filter_var', $filtersARGS);
			}
			if($values === false){
				$errorMsg = (isset($filtersARGS[2]['assertText'])) ? $filtersARGS[2]['assertText'] : '';
				$this->invalidate($errorMsg);
				$this->unvalidFilters = array_merge($this->filtersValues, $this->unvalidFilters);
				$this->filtersValues = [];
				return;
			}
		}
		$this->filtersValues = $values;
	}

	/**
	 * @param $filterID
	 * @param null $options
	 * @param null $flags
	 * @param null $assertText
	 * @return $this|void
	 * @throws \Exception
	 * @deprecated
	 */
	public function addFilter($filterID, $options = null, $flags = null, $assertText = null)
	{
		throw new \Exception('method disabled on inputMultiObjects');
	}

	/**
	 * @param $callback
	 * @param null $errorMsg
	 * @disabled
	 * @return void|InputMultiObjects|InputValue
	 */
	public function addFilterCallback($callback, $errorMsg = null)
	{
		return $this->addFilter($callback);
	}

	/**
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value){
		parent::setValue($value);
		if(is_array($value) || $value instanceof \Iterator)
			foreach($value as $v){
				$filter = clone $this->modelFilter->getModel();
				$filter->setValue($v);
				$filter->setParent($this);
				$this->filtersValues[] = $filter;
			}
		return $this;
	}

	/**
	 * get the value
	 * @return array
	 */
	public function getValue()
	{
		$value = [];
		foreach($this->filtersValues as $filter){
			$value[] = $filter->getValue();
		}
		return $value;
	}

	/**
	 * @return \Wish\Filters\AbstractFilters[]
	 */
	public function getItems(){
		return $this->filtersValues;
	}

	/**
	 * @return AbstractFilters
	 */
	public function getModel(){
		return $this->modelFilter;
	}

	/**
	 * use a specific column as index
	 * @param $key
	 */
	public function setColumnAsIndex($key){
		$this->columnAsIndex = $key;
	}

	public function toObject(){
		$value = [];
		foreach($this->filtersValues as $filter){
			$value[] = ($filter instanceof ToObjectInterface) ? $filter->toObject() : $filter->getValue();
		}
		return $value;
	}

	/**
	 * get a list of fields name that failed the {@see self::isValid() isValid()} test
	 * @return array
	 */
	public function getError(){
		$nonValidDatas = array();
		foreach($this->filtersValues as $k=>$input){
			if(!$input->isValid()){
				if($input instanceof AbstractFilters){
					$nonValidDatas[$k] = $input->getInvalidDatas();
				}else
					$nonValidDatas[$k] = $input->getError();
			}
		}

		if($this->error) $nonValidDatas['_data'] = $this->error;

		return $nonValidDatas;
	}
}
