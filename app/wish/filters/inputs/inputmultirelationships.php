<?php
/**
 * inputmultirelationships.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

/**
 * Class InputMultiRelationships
 * @package Wish\Filters\Inputs
 */
class InputMultiRelationships extends InputMultiValues {
	/**
	 * set the input value (reset the valid state)
	 * @param mixed $value
	 * @return InputMultiValues
	 */
	public function setValue($value){

		$value = $this->parseInputValue($value);

		return parent::setValue($value);
	}

	/**
	 * @param $value
	 * @return array
	 */
	private function parseInputValue($value){

		if (is_array($value) && isset($value['data'])) $value = array_column($value['data'], 'id');

		return $value;
	}

	/**
	 * @param mixed $value
	 * @return InputMultiValues
	 */
	public function setDefaultValue($value)
	{
		$value = $this->parseInputValue($value);
		return parent::setDefaultValue($value);
	}

}

