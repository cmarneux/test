<?php
/**
 * inputmultivalues.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Filters\Inputs;

use BoondManager\Services\BM;
use Wish\Tools;

/**
 * Class InputMultiValues
 *
 * Allow to handle a paramater with multi values (array)
 *
 * @package Wish\Filters\Inputs
 */
class InputMultiValues extends InputValue implements \Countable {
	/**
	 * @var array
	 */
	protected $groupedFilter = [];

	const MODE_IGNORE_WRONG_ENTRY = 8;

	/**
	* InputMultiValues constructor.
	* @param string $name
	* @param array $defaultValue
	* @param bool $required
	* @param int $mode
	*/
	public function __construct($name, array $defaultValue = [], $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);
		$this->setAllowEmptyValue(true);
		$this->value = [];
	}

	/**
	* @inheritdoc
	* @return bool
	*/
	public function isValid(){
		// on verifie que le test n'a pas eu lieu (sinon renvoi de la valeur du dernier test)
		if(is_null($this->isValid)){

			if($this->isDefined()){

				$validValues = [];
				foreach($this->value as $val){
					$cleanData = $this->checkMultiFilters($val);
					if($cleanData !== false) {
						$validValues[] = $cleanData;
					}else if($this->mode !== self::MODE_IGNORE_WRONG_ENTRY){
						$this->invalidate();
					}
				}
				if(is_null($this->isValid )) $this->validate();

				// succès de la validation, on sort de la
				if($this->isValid){
					$this->value = $validValues;
					$this->checkGroupFilters(); // can modify isValid
					if($this->isValid) return true;
				}

				if( (!$this->isValid || !$this->value) && $this->isRequired()){
					$this->invalidate(self::ERROR_REQUIRED);
					return false;
				}

				// on est autorisé à supprimer la valeur (utile pour les relationships)
				if($this->isAllowingEmptyValue() && !$this->getRawValue()) {
					$this->value = $this->getEmptyValue();
					$this->validate();
					return true;
				}

				if(Tools::getDebugMode() == Tools::DEBUG_OFF) {
					if ($this->mode == self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) {
						$this->value = $this->getDefaultValue();
						if (!is_null($this->defaultIsDefined)) $this->isDefined = $this->defaultIsDefined;
						$this->validate();
						return true;
					} // champs non requis, suite des tests
					else if ($this->mode == self::MODE_IGNORE_INCORRECT_VALUE) {
						$this->reset();
						$this->setDisabled(true);
						$this->validate();
						return true;
					}
				}

				$this->invalidate(self::ERROR_INCORRECT_VALUE);
				return false;
			}else{
				// cas ou l'on est non-définie OU echec de la validation
				if($this->isRequired()){
					$this->invalidate(self::ERROR_REQUIRED);
					return false;
				}

				if ($this->mode == self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) {
					$this->value = $this->getDefaultValue();
					if (!is_null($this->defaultIsDefined)) $this->isDefined = $this->defaultIsDefined;
				}

				$this->validate();
				return true;

			}
		}
		return $this->isValid;
	}

	/**
	* apply all filters on an array value
	* @param mixed $val
	* @return mixed|false the cleaned data or false if a filter failed
	*/
	private function checkMultiFilters($val){
		foreach($this->filters as $filtersARGS){

			array_unshift($filtersARGS, $val);

			if($filtersARGS[1] == FILTER_CALLBACK){
				$val = $this->applyCallback($filtersARGS[0], $filtersARGS[2]['options']);
			}else {
				$val = call_user_func_array('filter_var', $filtersARGS);
			}

			if($val === false){
				$this->error = (!$this->error && isset($filtersARGS[2]['assertText'])) ? $filtersARGS[2]['assertText'] : '';
				break;
			}
		}

		return $val;
	}

	/**
	 * get a list of value, can be filtered with a top key
	 * @deprecated si utilisé avec $topKey (devrait juste servir à un alias à getValue)
	 * @param string $topKey
	 * @return array
	 * @deprecated
	 */
	public function getValues($topKey = null){
		if(!$topKey) return $this->value;

		$values = [];
		foreach($this->value as $value){
			$top = self::getTopKey($value);
			$val = self::getRealKey($value);
			if($top == $topKey) $values[] = $val;
		}

		return $values;
	}

	/**
	 * set value
	 * @param string|array $value the value can be a string with values separated by comas or an array
	 * @return InputValue
	 * @throws \Exception
	 */
	public function setValue($value)
	{
		if(is_string($value)) $value = explode(',', trim($value, '[]'));
		if(!is_array($value)) throw new \Exception('InputMultiValues->setValue() is expecting an array. '.gettype($value).' given');
		return parent::setValue($value);
	}

	/**
	 * @param mixed $value
	 * @return InputValue
	 */
	public function setDefaultValue($value)
	{
		if(!is_array($value)) $value = [$value];
		return parent::setDefaultValue($value);
	}

	/**
	 * @see parent::reset()
	 * @return $this
	 */
	public function reset(){
		$this->setValue([]);
		$this->rawValue = null;
		$this->isDefined = false;
		return $this;
	}

	/**
	 * return a readable value of the parameter
	 * @return string
	 */
	public function __toString(){
		return '['.implode(',', $this->getValue()).']';
	}

	/**
	 *
	 */
	protected function checkGroupFilters(){
		foreach($this->groupedFilter as $filtersARGS){
			array_unshift($filtersARGS, $this->value);
			if($filtersARGS[1] == FILTER_CALLBACK){
				$result = $this->applyCallback($filtersARGS[0], $filtersARGS[2]['options']);
			}else{
				$result = call_user_func_array('filter_var', $filtersARGS);
			}
			if($result === false){
				$errorMsg = (isset($filtersARGS[2]['assertText'])) ? $filtersARGS[2]['assertText'] : '';
				$this->invalidate($errorMsg);
				return;
			}else{
				$this->value = $result;
			}
		}
		$this->validate();
	}

	/**
	 * Add a filter on all value at once
	 * @param $filterID {@see http://php.net/manual/fr/function.filter-var.php filter_var}
	 * @param mixed $options
	 * @param mixed $flags
	 * @param string $assertText an error message
	 * @return $this
	 */
	public function addGroupFilter($filterID, $options = null, $flags = null, $assertText = null){
		$params = [$filterID];
		if($flags || $options || $assertText){
			$params[1] = array();
			if($options) $params[1]['options'] = $options;
			if($flags) $params[1]['flags'] = $flags;
			if($assertText) $params[1]['assertText'] = $assertText;
		}
		$this->groupedFilter[] = $params;
		$this->resetValidation();
		return $this;
	}

	/**
	 * @param $callback
	 * @param int|string $errorMsg
	 * @return InputMultiValues
	 */
	public function addGroupFilterCallback($callback, $errorMsg = null){
		return $this->addGroupFilter(FILTER_CALLBACK, $callback, null, $errorMsg);
	}

	/**
	 * sanitize the multi input to keep only one item of each
	 * @return $this
	 */
	public function addUnicityFilter(){
		$this->setMode(self::MODE_ERROR_ON_INCORRECT_VALUE);
		$this->addGroupFilterCallback(function($array){
			$uniq = array_unique($array);
			return (count($uniq) < count($array)) ? false : $array;
		}, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
		return $this;
	}

	/**
	 * sanitize the multi input to keep only one item with the same attribute
	 * @param string $attr
	 * @return $this
	 */
	public function addGroupFilterOnAttribute($attr){
		$this->addGroupFilterCallback(function($array) use($attr) {
			$newArray = [];
			foreach($array as $entry) {
				$key = $entry->$attr->getValue();
				if(isset($newArray[$key]))
					return false;
				else $newArray[$key] = $key;
			}
			return $array;
		}, BM::ERROR_GLOBAL_ATTRIBUTE_DUPLICATED);
	}

	/**
	 * @param null $error
	 * @return $this
	 */
	public function invalidate($error = null)
	{
		parent::invalidate($error);
		$this->value = [];
		return $this;
	}

	/**
	 * @param int $mode
	 * @return $this
	 */
	public function setMode($mode)
	{
		if($mode == self::MODE_IGNORE_WRONG_ENTRY){
			$this->mode = $mode;
			$this->resetValidation();
		}else{
			parent::setMode($mode);
		}
		return $this;
	}

	/**
	 * Count elements of an object
	 * @link http://php.net/manual/en/countable.count.php
	 * @return int The custom count as an integer.
	 * </p>
	 * <p>
	 * The return value is cast to an integer.
	 * @since 5.1.0
	 */
	public function count()
	{
		return count($this->getValue());
	}
}
