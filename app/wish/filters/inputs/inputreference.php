<?php
/**
 * inputreference.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

use Wish\Tools;

/**
 * Class InputReference
 * @package Wish\Filters\Inputs
 */
class InputReference extends InputInt {

	private $_objects = [];

	/**
	 * Id constructor.
	 * @param string $name
	 * @param bool $defaultValue
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name = 'reference', $defaultValue = false, $required = false, $mode = self::MODE_ERROR_ON_INCORRECT_VALUE) {
		parent::__construct($name, $defaultValue, $required, $mode);
		$this->setMin(1);
	}

	/**
	 * @param array $allowedReferences
	 * @return $this
	 */
	public function addFilterReferenceExists($allowedReferences = []) {
		$this->_objects = Tools::useColumnAsKey('reference', $allowedReferences);
		$this->addFilterCallback(function($value){
			return array_key_exists($value, $this->_objects) ? $value : false;
		}, self::ERROR_ENTITY_DOES_NOT_EXIST);
		return $this;
	}

	/**
	 * @return mixed|null
	 */
	public function getObject() {
		if($this->isValid())
			return $this->_objects[$this->getValue()];
		else return null;
	}
}
