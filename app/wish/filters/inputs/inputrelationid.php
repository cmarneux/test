<?php
/**
 * inputrelationid.php
 * @author Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

use Wish\Filters\ToObjectInterface;
use Wish\Models\Model;

/**
 * Class InputRelationId
 * @package Wish\Filters\Inputs
 */
class InputRelationId extends InputValue implements ToObjectInterface{
	/**
	 * @var bool
	 */
	protected $_allowEmptyValue = false;

	/**
	 * @var null
	 */
	protected $emptyValue = null;

	/**
	 * set the input value (reset the valid state)
	 * @param mixed $value
	 * @return InputValue
	 */
	public function setValue($value){

		$value = $this->parseInputValue($value);
		return parent::setValue($value);
	}

	/**
	 * @param $value
	 * @return mixed|null
	 */
	private function parseInputValue($value){

		if(is_array($value)) {
			if(isset($value['id']))
				$value = $value['id'];
			else
				$value = null;
		}

		return $value;
	}

	/**
	 * set the default value
	 * @param mixed $value
	 * @return InputValue
	 */
	public function setDefaultValue($value)
	{
		$value = $this->parseInputValue($value);
		return parent::setDefaultValue($value);
	}

	/**
	 * @param array $allowedIds
	 * @return $this
	 */
	public function addFilterInIds($allowedIds = []){
		$this->addFilter(FILTER_CALLBACK, function($value) use($allowedIds) {
			if(array_key_exists($value, $allowedIds)) {
				return ['id' => intval($value)];
			} else
				return false;
		});
		return $this;
	}

	public function toObject(){
		$object = new Model($this->getValue());
		return $object;
	}
}
