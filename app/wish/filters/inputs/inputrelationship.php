<?php
/**
 * inputrelationship.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Filters\Inputs;

use Wish\MySQL\Where;

/**
 * Class InputRelationship
 * @package Wish\Filters\Inputs
 */
class InputRelationship extends InputDB{
	/**
	 * @var bool
	 */
	protected $_allowEmptyValue = false;

	/**
	 * @var null
	 */
	protected $emptyValue = null;

	/**
	 * @var string parameter's type
	 */
	private $type;

	/**
	 * relationship type
	 * @return string
	 */
	public function getType(){
		return $this->type;
	}

	/**
	 * set the input value (reset the valid state)
	 * @param mixed $value
	 * @return InputDB
	 */
	public function setValue($value){

		$value = $this->parseInputValue($value);
		return parent::setValue($value);
	}

	public function reset()
	{
		$this->type = null;
		return parent::reset();
	}

	public function invalidate($error = null)
	{
		$this->type = null;
		return parent::invalidate($error);
	}

	/**
	 * @param $value
	 * @return mixed|null
	 */
	private function parseInputValue($value){

		if(is_array($value)) {
			if (isset($value['id'], $value['type'])){
				$this->type = $value['type'];
				$value = $value['id'];
			} else if (isset($value['data'], $value['data']['id'], $value['data']['type'])) {
				$this->type = $value['data']['type'];
				$value = $value['data']['id'];
			} else $value = null;
		}

		return $value;
	}

	/**
	 * set the default value
	 * @param mixed $value
	 * @return InputDB
	 */
	public function setDefaultValue($value)
	{
		$value = $this->parseInputValue($value);
		return parent::setDefaultValue($value);
	}

	/**
	 * check the relationship involves an existing entity
	 * @param array $allowedEntityTypes
	 * @param string $db
	 * @return $this
	 * @deprecated
	 */
	public function addFilterExistingEntity($allowedEntityTypes = [], $db = 'local'){
		$this->addFilter(FILTER_CALLBACK, function($value) use ($allowedEntityTypes, $db){
			if(! $this->type) $this->type = $this->name;
			//On déduit le RowObject correspondant pour aller y chercher la table et la colonne d'identifiant
			$rowobjectClass = "Boondmanager\\Models\\MySQL\\RowObject\\".ucfirst($this->type);
			if(! class_exists($rowobjectClass)) return $value;
			if(! $allowedEntityTypes || in_array($this->type, $allowedEntityTypes))
			return $this->existsInDB($value, $rowobjectClass::getJSONTable(), new Where($rowobjectClass::getJSONIDAttribute().'=?'), $db);
			return false;
		}, null, self::ERROR_ENTITY_DOES_NOT_EXIST);
		return $this;
	}

	/**
	 * @param $type
	 * @return $this
	 */
	public function addFilterType($type){
		if(!is_array($type)) $type = [$type];
		$this->addFilterCallback(function($value) use ($type){
			if(in_array($this->getType() , $type)) return $value;
			else return false;
		}, self::ERROR_INCORRECT_SCHEMA);
		return $this;
	}
}
