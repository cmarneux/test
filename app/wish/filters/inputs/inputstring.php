<?php
/**
 * inputstring.php
 * @author louis D'Arras louis.darras@wishgroupe.com
 */
namespace Wish\Filters\Inputs;

/**
 * Class InputString
 * @package Wish\Filters\Inputs
 */
class InputString extends InputValue{
	/**
	 * InputString constructor.
	 * @param string $name
	 * @param string $defaultValue
	 * @param bool $required
	 * @param int $mode
	 */
	public function __construct($name, $defaultValue = '',  $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		parent::__construct($name, $defaultValue, $required, $mode);

		$this->setEmptyValue('');

		$this->addFilterCallback(function($value){
			$value = trim($value);
			if( ($this->isRequired() || !$this->isAllowingEmptyValue()) && $value == '') return false;
			else return $value;
		});
	}

	/**
	 * @param $number
	 * @return $this
	 */
	public function setMinLength($number){
		$this->addFilterCallback(function($value) use ($number){
			$length = strlen($value);
			if($length < $number) return false;
			else return $value;
		});

		return $this;
	}

	/**
	 * @param $number
	 * @return $this
	 */
	public function setMaxLength($number){
		$this->addFilterCallback(function($value) use ($number){
			$length = strlen($value);
			if($length > $number) return false;
			else return $value;
		});

		return $this;
	}
}
