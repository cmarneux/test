<?php
/**
 * inputvalue.php
 * @author louis D'Arras louis.darras@wishgroupe.com
 */
namespace Wish\Filters\Inputs;

use Wish\Filters\AbstractFilters;
use Wish\Tools;

/**
 * Class InputValue
 *
 * used for parsing one parameter
 *
 * @property-read string $name
 * @property-read mixed $value
 * @property-read mixed $defaultValue
 *
 * @package Wish\Filters\Inputs
 */
class InputValue implements InputInterface{
	/**#@+
	 * Error's type
	 * @var string
	 **/
	const ERROR_REQUIRED = 'The value is required and can not be empty';
	const ERROR_INCORRECT_VALUE = 'Incorrect value';
	const ERROR_INCORRECT_SCHEMA = 'Schema JSON Incorrect';
	const ERROR_ENTITY_DOES_NOT_EXIST = 'Entity does not exist';
	/**#@-*/

	/**#@+
	 * @var int MODE test on the mode
	 */
	/**
	 * the input will be marked as incorret if the value doesn't match
	 * @var int
	 */
	const MODE_ERROR_ON_INCORRECT_VALUE = 1;
	/**
	 * if the value is wrong, it will be completely ignored
	 */
	const MODE_IGNORE_INCORRECT_VALUE = 2;
	/**
	 * if the value doesn't match, it will be replaced by a default value and the input will be valid
	 * @var int
	 */
	const MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT = 4;
	/**#@-*/

	/**
	 * @var null
	 */
	protected $defaultIsDefined = null;

	/**
	 * @var bool is the value disabled
	 */
	private $disabled = false;

	/**
	 * @var bool is the value required
	 */
	private $required = false;

	/**
	 * @var string parameter's name
	 */
	private $name;

	/**
	 * @var mixed default value
	 */
	private $defaultValue = null;

	/**
	 * @var mixed user input
	 */
	protected $value;
	/**
	 * @var mixed user input
	 */
	protected $rawValue;

	/**
	 * @var bool did the value passed the test
	 */
	protected $isValid = null;

	/**
	 * @var int how to handle the value if a filter fail
	 */
	protected $mode = self::MODE_ERROR_ON_INCORRECT_VALUE;

	/**
	 * @var array list of filter to apply to the parameter
	 */
	protected $filters = [];

	/**
	 * @var array list of formater to apply to the parameter
	 */
	protected $preFormatters = [];

	/**
	 * @var array list of formater to apply to the parameter
	 */
	protected $postFormatters = [];

	/**
	 * can the value be a mixed of a top key and a value (separated by an _)
	 * @var bool
	 */
	protected $topKey = false;

	/**
	 * a list of valid data
	 * @var array
	 */
	protected $validDataList = [];

	/**
	 * an error
	 * @var string
	 */
	protected $error;

	/**
	 * @var bool
	 */
	protected $isDefined = false;

	/**
	 * @var bool
	 */
	protected $_allowEmptyValue = true;

	/**
	 * @var null
	 */
	protected $emptyValue = null;

	/**
	 * @var
	 */
	protected $filter;

	/**
	 * @var InputInterface
	 */
	private $_parent;

	/**
	 * InputValue constructor.
	 * @param string $name
	 * @param mixed $defaultValue
	 * @param bool $required
	 * @param int $mode
	 * @TODO supprimer le default value du constructeur ainsi que le mode
	 */
	public function __construct($name, $defaultValue = null, $required = false, $mode = self::MODE_IGNORE_INCORRECT_VALUE){
		$this->setName($name);
		if(!is_null($defaultValue)) $this->setDefaultValue($defaultValue);
		$this->setRequired($required);
		$this->setMode($mode);
	}

	/**
	 * @param InputInterface $input
	 * @return $this
	 */
	public function setParent(InputInterface $input)
	{
		$this->_parent = $input;
		return $this;
	}

	/**
	 * set the way to handle incorrect values
	 * @param int $mode
	 * @return $this
	 * @throws \Exception
	 */
	public function setMode($mode){
		if(!in_array($mode,[
			self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT,
			self::MODE_ERROR_ON_INCORRECT_VALUE,
			self::MODE_IGNORE_INCORRECT_VALUE
		])) throw new \Exception('Incorrect mode');
		$this->mode = $mode;
		$this->resetValidation();
		return $this;
	}

	/**
	 * is the parameter required
	 * @return bool
	 */
	public function isRequired(){
		return $this->required;
	}

	/**
	 * mark the field as required
	 * @param bool $boolean
	 * @return $this
	 */
	public function setRequired($boolean){
		$this->resetValidation();
		$this->required = boolval($boolean);
		if($this->required) $this->setDisabled(false); // Un champ requis est forcément autorisé
		return $this;
	}

	/**
	 * is the parameter forbidden
	 * @return bool
	 */
	public function isDisabled(){
		return $this->disabled;
	}

	/**
	 * mark the field as forbidden
	 * @param bool $boolean
	 * @return $this
	 */
	public function setDisabled($boolean = true){
		$this->resetValidation();
		$this->disabled = boolval($boolean);
		if($this->disabled) $this->isDefined = false; // pour corriger mon bug de désactivation des relations mainManager & agencies
		if($this->disabled) $this->setRequired(false); // Un champ interdit est forcément facultatif
		return $this;
	}

	/**
	 * input value's name
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * set a default value (used if no value is given or if a wrong value has to be replaced)
	 * @param mixed $value
	 * @return $this
	 */
	public function setDefaultValue($value){
		$this->defaultValue = $value;
		return $this;
	}

	/**
	 * @param $value
	 * @return $this
	 */
	public function setModeDefaultValue($value, $defined = null){
		$this->setDefaultValue($value);
		$this->setMode(self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$this->markDefaultValueAsDefined($defined);
		return $this;
	}

	/**
	 * get the default value
	 * @return mixed
	 */
	public function getDefaultValue(){
		return $this->defaultValue;
	}

	/**
	 * @param bool|null $bool if the value is null, then it depends of user input
	 * @return $this
	 */
	public function markDefaultValueAsDefined($bool = true){
		$this->defaultIsDefined = is_null($bool) ? null : boolval($bool);
		return $this;
	}

	/**
	 * set the input value (reset the valid state)
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value){
		$this->value = $value;
		$this->rawValue = $value;
		$this->resetValidation();
		$this->isDefined = true;
		return $this;
	}

	/**
	 * Input value (filtered if called $this->filter() or $this->isValid())
	 * @return mixed
	 */
	public function getValue(){
		$this->filter();
		return $this->value;
	}

	public function hasBeenValidated(){
		return !is_null($this->isValid);
	}

	/**
	 * Raw input value (not filtered)
	 * @return mixed
	 */
	public function getRawValue(){
		return $this->rawValue;
	}

	/**
	 * return true if a rawValue was set
	 * @return bool
	 */
	public function isDefined(){
		return $this->isDefined;
	}

	/**
	 * Add a filter
	 * @param $filterID {@see http://php.net/manual/fr/function.filter-var.php filter_var}
	 * @param mixed $options
	 * @param mixed $flags
	 * @param string $assertText an error message
	 * @return $this
	 */
	public function addFilter($filterID, $options = null, $flags = null, $assertText = null){
		$params = [$filterID];
		if($flags || $options || $assertText){
			$params[1] = array();
			if($options) $params[1]['options'] = $options;
			if($flags) $params[1]['flags'] = $flags;
			if($assertText) $params[1]['assertText'] = $assertText;
		}
		$this->filters[] = $params;
		$this->resetValidation();
		return $this;
	}

	/**
	 * Add a Formater
	 * @param $filterID {@see http://php.net/manual/fr/function.filter-var.php filter_var}
	 * @param mixed $options
	 * @param mixed $flags
	 * @param string $assertText an error message
	 * @return $this
	 * @deprecated
	 */
	public function addPostFormatter($filterID, $options = null, $flags = null, $assertText = null){
		$params = [$filterID];
		if($flags || $options || $assertText){
			$params[1] = array();
			if($options) $params[1]['options'] = $options;
			if($flags) $params[1]['flags'] = $flags;
			if($assertText) $params[1]['assertText'] = $assertText;
		}
		$this->postFormatters[] = $params;
		$this->resetValidation();
		return $this;
	}

	/**
	 * Add a Formater
	 * @param $filterID {@see http://php.net/manual/fr/function.filter-var.php filter_var}
	 * @param mixed $options
	 * @param mixed $flags
	 * @param string $assertText an error message
	 * @return $this
	 * @deprecated
	 */
	public function addPreFormatter($filterID, $options = null, $flags = null, $assertText = null){
		$params = [$filterID];
		if($flags || $options || $assertText){
			$params[1] = array();
			if($options) $params[1]['options'] = $options;
			if($flags) $params[1]['flags'] = $flags;
			if($assertText) $params[1]['assertText'] = $assertText;
		}
		$this->preFormatters[] = $params;
		$this->resetValidation();
		return $this;
	}

	/**
	 * Add a filter to check the value belongs to an array of allowed values
	 * @param array $validDatas allowed values
	 * @param string $assertText an error message
     * @param bool $strict Third parameter of in_array function
	 * @deprecated use InputEnum instead
	 * @return $this
	 */
	public function addFilterInArray(array $validDatas, $assertText = self::ERROR_INCORRECT_VALUE, $strict = false){
        $this->setAllowEmptyValue(false);
        $this->validDataList = $validDatas;
        $this->addFilterCallback(function ($value) use($strict) {
            return in_array($value, $this->validDataList, $strict) ? $value : false;
        }, $assertText);
		return $this;
	}

    /**
     * Add a filter to check the value belongs to an array of allowed values
     * @param array $validDatas allowed values
     * @param string $assertText an error message
     * @return $this
     */
    public function addFilterInArrayStrict(array $validDatas, $assertText = self::ERROR_INCORRECT_VALUE){
        return $this->addFilterInArray($validDatas, $assertText, true);
    }

	/**
	 * return the top key value (or null if it doesn't exists)
	 * @param string $value
	 * @return string|null
	 */
	public static function getTopKey($value){
		$parts = explode('_', $value);
		if(count($parts) != 2) return null;
		else return $parts[0];
	}

	/**
	 * get the value from a composed one ( {top}_{real} )
	 * @param $value
	 * @return string|null
	 */
	public static function getRealKey($value){
		$parts = explode('_', $value);
		if(count($parts) != 2) return null;
		else return $parts[1];
	}

	/**
	 * flatten a dict entry like mobility :
	 * [
	 *   'label'=> [
	 *     ['key1', 'value', ...],
	 *     ['key2', 'value', ...],
	 *   'label2'=> [
	 *     ['key3', 'value', ...],
	 *     ['key4', 'value', ...],
	 * ]
	 *
	 * to
	 * [
	 *     ['key1', 'value', ...],
	 *     ['key2', 'value', ...],
	 *     ['key3', 'value', ...],
	 *     ['key4', 'value', ...],
	 * ]
	 * @param array $dict
	 * @return array
	 */
	private function flattenDict(array $dict, $subDictKey = 'option'){

		$newDict = [];
		foreach($dict as $subDict){
			if(isset($subDict[ $subDictKey ]) && is_array($subDict[ $subDictKey ])) {
				$newDict = array_merge($newDict, $this->flattenDict($subDict[ $subDictKey ]));
			}
			else return $dict;
		}
		return $newDict;
	}

	/**
	 * Add a filter to check that the value belongs to a dictionary {@see self::addFilterInArray() addFilterInArray}
	 *
	 * A dictionary must be an array with 2 dimensions, the verification will be made on with the [X][0] value
	 * @param array $dict
	 * @param array $moreValues
	 * @param string $column
	 * @param string $assertText
	 * @deprecated use InputDict
	 * @return $this
	 * @TODO deplacer ça dans un InputDict et le mettre en deprecated
	 */
	public function addFilterInDict(array $dict, array $moreValues = [], $column = 'id', $assertText = self::ERROR_INCORRECT_VALUE){
		$dict = $this->flattenDict($dict);

		$dictValue = array_merge(array_column($dict, $column),$moreValues);
		$this->validDataList = $dictValue;
		$this->addFilter(FILTER_CALLBACK, function($value) use ($dictValue){
			if(in_array($value, $dictValue, true)) return $value; //comparaison stricte d'abord
			if($value !== '' && in_array($value, $dictValue)) return $value; //comparaison non stricte uniquement si value != chaine vide (car "" = 0 (ce qu'on ne veut pas) mais on peut accepter "0" = 0)
			else return false;
		}, null, $assertText);
		return $this;
	}

	/**
	 * add a filter that check the value belongs to a list of dictionary's entries
	 * @param array $dictArray a 3 dim array
	 * the first level key will be used as the `topKey` @see self::getTopKey()
	 * and the fird level, first value will be used as the value (in the example, `10`, `11` and `12`)
	 * example:
	 * [
	 *      0=>[
	 *          [0=>10, 1=>'dictEntry1']
	 *          [0=>12, 1=>'dictEntry2']
	 *      ],
	 *      1=>[
	 *          [0=>11, 1=>'dictEntry3']
	 *          [0=>12, 1=>'dictEntry4']
	 *      ],
	 *
	 * ]
	 * @param string $assertText an error message
	 * @return $this
	 * @deprecated
	 */
	public function addFilterInMultiDict(array $dictArray, $assertText = self::ERROR_INCORRECT_VALUE){
		$this->topKey = true;
		$validData = [];
		foreach($dictArray as $k=>$dict){
			foreach($dict as $entry){
				$validData[] = $k.'_'.$entry['id'];
			}
		}
		$this->validDataList = $validData;
		$this->addFilter(FILTER_CALLBACK, function($value) use ($validData){
			return in_array($value, $validData)?$value:false;
		}, null, $assertText);
		return $this;
	}

	/**
	 * add a filter to force the value to be set
	 * @return $this
	 * @deprecated
	 */
	public function notEmpty(){
		$this->addFilter(FILTER_CALLBACK, function($value){
			return boolval($value)?$value:false;
		});
		return $this;
	}

	/**
	 * retrieve all filters
	 * @return array
	 */
	public function getFilters(){
		return $this->filters;
	}

	/**
	 * delete all filters
	 * @return $this
	 */
	public function resetFilters(){
		$this->filters = array();
		return $this;
	}

	/**
	 * the next isValid() or filter() will force a revalidation process
	 * @return $this
	 */
	public function resetValidation(){
		$this->isValid = null;
		$this->resetParentValidation();
		return $this;
	}

	/**
	 * @return $this
	 */
	private function resetParentValidation(){
		if($this->_parent) $this->_parent->resetValidation();
		return $this;
	}

	/**
	 * delete the current value
	 * @return self
	 */
	public function reset(){
		if($this->name == 'reference') var_dump('reset !!!1');
		$this->setValue(null);
		$this->isDefined = false;
		return $this;
	}

	/**
	 * validate input (call isValid());
	 * @return $this
	 */
	public function filter(){
		$this->isValid();
		return $this;
	}

	public function dump(){
		return [
			'isValid' => $this->isValid,
			'isDefined' => $this->isDefined,
			'rawValue' => $this->rawValue,
			'value' => $this->value
		];
	}

	public function detachParent() {
		$this->_parent = null;
	}

	/**
	 * Check the parameter value match all filters
	 *
	 * if the parameter is rejected, the output will depend on the given mode:
	 *  - {@see self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT}
	 *  - {@see self::MODE_INVALID_ON_INCORRECT_VALUE}
	 *
	 * @return bool
	 */
	public function isValid(){
		// on verifie que le test n'a pas eu lieu (sinon renvoi de la valeur du dernier test)
		if(is_null($this->isValid)){

			if($this->isDefined()){
				// reset data
				$this->value = $this->rawValue;


				// si pas de valeur, on assigne la valeur par défault
				$this->value = $this->applyPreFormaters($this->value);

				// vérification que la valeur passe les filtres
				$this->checkFilters();

				// succès de la validation, on sort de la
				if($this->isValid){
					$this->value = $this->applyPostFormaters($this->value);
					return true;
				}

				if( (!$this->isValid || !$this->value) && $this->isRequired()){
					$this->invalidate(self::ERROR_REQUIRED);
					return false;
				}

				// on est autorisé à supprimer la valeur (utile pour les relationships)
				if($this->isAllowingEmptyValue() && !$this->getRawValue()) {
					$this->value = $this->getEmptyValue();
					$this->validate();
					return true;
				}

				if(Tools::getDebugMode() == Tools::DEBUG_OFF) {
					if($this->mode == self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT) {
						$this->value = $this->getDefaultValue();
						if (!is_null($this->defaultIsDefined)) $this->isDefined = $this->defaultIsDefined;
						$this->validate();
						return true;
					} else if ($this->mode == self::MODE_IGNORE_INCORRECT_VALUE) {
						// champs non requis, suite des tests
						$this->reset();
						$this->setDisabled(true);
						$this->validate();
						return true;
					}
				}

				$this->invalidate(self::ERROR_INCORRECT_VALUE);
			}else{
				// cas ou l'on est non-définie
				if($this->isRequired()){
					$this->invalidate(self::ERROR_REQUIRED);
					return false;
				}

				if($this->mode == self::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT){
					$this->value = $this->getDefaultValue();
					if (!is_null($this->defaultIsDefined)) $this->isDefined = $this->defaultIsDefined;
				}

				$this->validate();
				return true;
			}
		}
		return $this->isValid;
	}

	/**
	 * @param $emptyValue
	 * @return $this
	 */
	public function setEmptyValue($emptyValue){
		$this->emptyValue = $emptyValue;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getEmptyValue(){
		return $this->emptyValue;
	}

	/**
	 * @param string $error
	 * @return $this
	 */
	public function invalidate($error = null){
		if(!$this->error) $this->error = $error;
		$this->value = null;
		$this->isValid = false;
		$this->resetParentValidation();
		return $this;
	}

	/**
	 * disable input unless it's a debug request. In that case, it will be invalidated
	 * @param int|string $error
	 * @return $this
	 */
	public function invalidateIfDebug($error = null){
		Tools::getDebugMode() == Tools::DEBUG_OFF ? $this->setDisabled(true) : $this->invalidate($error);
		return $this;
	}

	/**
	 * @return $this
	 */
	protected function validate(){
		$this->isValid = true;
		$this->resetParentValidation();
		return $this;
	}

	/**
	 * Apply all filters on the value  (can alter the value)
	 */
	private function checkFilters(){
		$ok = true;
		$errorMsg = '';
		foreach($this->filters as $filtersARGS){
			array_unshift($filtersARGS, $this->value);
			if($filtersARGS[1] == FILTER_CALLBACK){
				$result = $this->applyCallback($filtersARGS[0], $filtersARGS[2]['options']);
			}else {
				$result = call_user_func_array('filter_var', $filtersARGS);
			}
			$isBoolean = $this instanceof InputBoolean;
			if( ($isBoolean && $result === NULL) || ( !$isBoolean && $result === false)){
				$ok = false;
				$errorMsg = (isset($filtersARGS[2]['assertText'])) ? $filtersARGS[2]['assertText'] : '';
				break;
			}else{
				if($result === '') $result = $this->getEmptyValue();
				$this->value = $result;
			}
		}

		if($ok) $this->validate();
		else $this->invalidate($errorMsg);
	}

	protected function applyCallback($value, \Closure $callback){
		$callback = $callback->bindTo($this);
		return $callback($value);
		// return $callback->call($this, $value); pour PHP 7 :D
	}

	/**
	 * add a callback
	 * @param $callback
	 * @param int|string $errorMsg
	 * @return InputValue
	 */
	public function addFilterCallback($callback, $errorMsg = null){
		return $this->addFilter(FILTER_CALLBACK, $callback, null, $errorMsg);
	}

	/**
	 * Apply all filters on the value  (can alter the value)
	 * @deprecated
	 * @return bool
	 */
	private function applyPostFormaters($value){
		foreach($this->postFormatters as $filtersARGS){
			//if($value === false) return $value;
			array_unshift($filtersARGS, $value);
			$result = call_user_func_array('filter_var', $filtersARGS);

			$value = $result;
		}
		return $value;
	}
	/**
	 * Apply all filters on the value  (can alter the value)
	 * @deprecated
	 * @return bool
	 */
	private function applyPreFormaters($value){
		foreach($this->preFormatters as $filtersARGS){
			//if($value === false) return $value;
			array_unshift($filtersARGS, $value);
			$result = call_user_func_array('filter_var', $filtersARGS);

			$value = $result;
		}
		return $value;
	}

	/**
	 * give a read-only access on the following fields : value, name, defaultValue
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name){
		// read-only properties
		$prop = ['value', 'name', 'defaultValue'];
		if(in_array($name, $prop)){
			return $this->{$name};
		}
	}

	/**
	 * return the value of the parameter
	 * @return mixed
	 */
	public function __toString(){
		return $this->getValue();
	}

	/**
	 * check if the value is the default value
	 * @return bool
	 */
	public function isDefaultValue(){
		return $this->value == $this->defaultValue;
	}

	/**
	 * get a list of accepted values
	 * @return array
	 * @tODO deplacer ça dans un composant InputArray (dont doit etendre InputDict) et le mettre en deprecated ici
	 */
	public function getListOfValidValues(){
		return $this->validDataList;
	}

	/**
	 * set the input name
	 * @param string $name
	 * @return $this
	 */
	public function setName($name){
		$this->name = $name;
		return $this;
	}

	/**
	 * get an error message if available
	 * @return string
	 */
	public function getError(){
		return $this->error;
	}

	/**
	 * @param bool $bool
	 * @return $this
	 */
	public function setAllowEmptyValue($bool = true)
	{
		$this->_allowEmptyValue = boolval($bool);
	}

	/**
	 * @return boolean
	 */
	public function isAllowingEmptyValue()
	{
		return $this->_allowEmptyValue;
	}

	/**
	 * @param AbstractFilters $filter
	 * @return $this
	 */
	public function attachToFilter(AbstractFilters $filter){
		$filter->addInput($this);
		return $this;
	}
}
