<?php
/**
 * toobjectinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Wish\Filters;

interface ToObjectInterface{
	public function toObject();
}
