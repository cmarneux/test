<?php
/**
 * ftp.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

namespace Wish;

/**
 * Librairie de gestion de la connexion FTP.
 * @namespace Wish
 */
class FTP {
	/**
	 * Instance de la connexion au serveur FTP.
	 *
	 * _Utilisé uniquement si `$this->mode_local = false`_.
	 * @var resource
	 */
	protected $hFTP;

	/**
	 * Adresse du serveur FTP.
	 *
	 * _Utilisé uniquement si `$this->mode_local = false`_.
	 * @var string
	 */
	protected $serveur;

	/**
	 * Port du serveur FTP.
	 *
	 * _Utilisé uniquement si `$this->mode_local = false`_.
	 * @var string
	 */
	protected $port;

	/**
	 * Identifiant de connexion au serveur FTP.
	 *
	 * _Utilisé uniquement si `$this->mode_local = false`_.
	 * @var string
	 */
	protected $username;

	/**
	 * Mot de passe de connexion au serveur FTP.
	 *
	 * _Utilisé uniquement si `$this->mode_local = false`_.
	 * @var string
	 */
	protected $password;

	/**
	 * Répertoire par défaut à la connexion au serveur FTP ou en local.
	 *
	 * _Utilisé uniquement si `$this->mode_local = false`_.
	 * @var string
	 */
	protected $root;

	/** @var  boolean Indique si la librairie gère une connexion en locale, `true`, ou sur un serveur FTP distant, `false`. */
	protected $mode_local;

	/**
	 * Constructeur.
	 * @param string $serveur Adresse du serveur FTP.
	 * @param string $username Identifiant de connexion au serveur FTP.
	 * @param string $password Mot de passe de connexion au serveur FTP.
	 * @param string $root Répertoire par défaut à la connexion au serveur FTP ou en local.
	 * @param int $port Port du serveur FTP.
	 */
	public function __construct($serveur = '', $username = '', $password = '', $root = '/', $port = 21) {
		if($serveur == '' && $username == '' && $password == '') {
			$this->mode_local = true;
			$this->root = \Base::instance()->get('MAIN_ROOTPATH').'/';
		} else {
			$this->mode_local = false;
			$this->serveur = $serveur;
			$this->port = $port;
			$this->username = $username;
			$this->password = $password;
			$this->root = $root;
		}
	}

	/**
	 * Ouvre la connexion FTP.
	 * @return boolean `true` si succès, `false` sinon.
	 */
	public function openFTPSocket() {
		if(!$this->mode_local) {
			$this->hFTP = @ftp_connect($this->serveur, $this->port);
			if(!$this->hFTP || !ftp_login($this->hFTP, $this->username, $this->password) || !ftp_chdir($this->hFTP, $this->root))
				return false;
		}
		return true;
	}

	/**
	 * Ferme la connexion FTP.
	 * @return boolean `true` si succès, `false` sinon
	 */
	public function closeFTPSocket() {
		return ($this->hFTP && !$this->mode_local)?ftp_close($this->hFTP):true;
	}

	/**
	 * Retourne le mode de la connexion FTP.
	 * @see FTP::$mode_local() $mode_local
	 * @return boolean
	 */
	public function getLocalMode() {
		return $this->mode_local;
	}

	/**
	 * Retourne le répertoire par défaut de la connexion FTP.
	 * @see FTP::$root() $root
	 * @return string
	 */
	public function getRootPath() {
		return $this->root;
	}

	/**
	 * Récupère un fichier local/téléchargé et le copie/déplace localement ou sur un serveur distant.
	 * @param string $filename Chemin du fichier à copier.
	 * @param string $target Chemin du fichier de destination.
	 * @param  boolean $copy  Indique si on copie le fichier ou si on déplace celui téléchargé.
	 * @param boolean $delete `true` si on suppprime le fichier original après l'avoir copié_, utilisé uniquement si `$copie = true`.
	 * @return boolean `true` si succès, `false` sinon.
	 */
	public function uploadFile($filename, $target, $copy = false, $delete = true) {
		$result = false;
		if($this->mode_local) {//Mode Local
			if($copy) {
				$result = copy($filename, $target);
				if($delete) $result = unlink($filename);
			} else
				$result = move_uploaded_file($filename, $target);
		} else if($this->hFTP) {
			if($copy) {
				$result = ftp_put($this->hFTP, $target, $filename, FTP_BINARY);
				if($delete) $result = unlink($filename);
			} else {
				$filetemp = tempnam(\Base::instance()->get('MAIN_ROOTPATH').'/tmp', 'FTP');
				if(move_uploaded_file($filename, $filetemp))
					$result = ftp_put($this->hFTP, $target, $filetemp, FTP_BINARY) && unlink($filetemp);
			}
		}
		return $result;
	}
	/**
	 * Indique si un fichier existe.
	 * @param string $filename Chemin du fichier.
	 * @return boolean `true` si le fichier existe, `false` sinon.
	 */
	public function file_exists($filename) {
		return $this->mode_local ? file_exists($filename) : ( ftp_size($this->hFTP, $filename) != -1 ? true : false );
	}

	/**
	 * Retourne le fichier demandée.
	 * @param string $filename Chemin du fichier.
	 * @param string $tempPath Chemin du répertoire temporaire.
	 *
	 * Répertoire où l'on copie `$filename` que l'on vient de récupérer via FTP_, uniquement si `$this->mode_local == false`_.
	 * @return array Tableau_, vide si erreur,_ contenant 2 éléments :
	 *
	 * Si succès et mode_local = true, le résultat est un tableau contenant le nom du fichier $filename et sa taille
	 * Si succès et mode_local = false, le résultat est un tableau contenant le nom du fichier temporaire téléchargé localement et sa taille
	 *
	 * A la fin de l'usage
	 */
	public function getFile($filename, $tempPath = '') {
		if($this->mode_local)
			return file_exists($filename)?array($filename, filesize($filename), false):array();
		else {
			if($tempPath == '') $tempPath = \Base::instance()->get('MAIN_ROOTPATH').'/tmp';
			$filetemp = tempnam($tempPath, 'FTP');
			return ftp_get($this->hFTP, $filetemp, $filename, FTP_BINARY)?array($filetemp, ftp_size($this->hFTP, $filename), true):array();
		}
	}
	/**
	 * Supprime un fichier.
	 * @param string $filename Chemin du fichier à supprimer.
	 * @return boolean `true` si succès, `false` sinon.
	 */
	public function deleteFile($filename) {
		if(!$this->mode_local && $this->file_exists($filename))
			return ftp_delete($this->hFTP, $filename);
		else if($this->mode_local && file_exists($filename))
			return unlink($filename);
		return false;
	}
	/**
	 * Liste les fichiers d'un répertoire.
	 * @param string $dirname Chemin du répertoire.
	 * @return array Tableau contenant les fichiers du répertoire.
	 */
	public function readdir($dirname) {return !$this->mode_local?ftp_nlist($this->hFTP, $dirname):scandir($dirname);}
	/**
	 * Crée un répertoire.
	 * @param string $dirname Chemin du répertoire à créer.
	 * @return boolean `true` si succès, `false` sinon.
	 */
	public function createDirectory($dirname) {return !$this->mode_local?ftp_mkdir($this->hFTP, $dirname):mkdir($dirname);}
	/**
	 * Supprime un répertoire et son contenu.
	 * @param string $dirname Chemin du répertoire à supprimer.
	 * @return boolean `true` si succès, `false` sinon.
	 */
	public function deleteDirectory($dirname) {
		if(!$this->mode_local) {
			$ar_files = ftp_rawlist($this->hFTP, $dirname);
			if(is_array($ar_files)) {
				foreach($ar_files as $st_file) {
					if(preg_match("([-d][rwxst-]+).* ([0-9]) ([a-zA-Z0-9]+).* ([a-zA-Z0-9]+).* ([0-9]*) ([a-zA-Z]+[0-9: ]*[0-9]) (.+)", $st_file, $matches)) {
						if($matches[7] == "." || $matches[7] == "..") continue;//Ignorer le répertoire courant & parent
						if(substr($matches[1],0,1)=="d")
							$this->deleteDirectory($dirname."/".$matches[7]);
						else
							$this->deleteFile($dirname."/".$matches[7]);
					}
				}
			}
			return ftp_rmdir($this->hFTP, $dirname);// delete empty directories
		} else if(is_dir($dirname)) {
			foreach(scandir($dirname) as $object) {
				if($object == "." || $object == "..") continue;//Ignorer le répertoire courant & parent
				if(filetype($dirname."/".$object) == "dir")
					$this->deleteDirectory($dirname."/".$object);
				else
					$this->deleteFile($dirname."/".$object);
			}
			return rmdir($dirname);// delete empty directories
		}
		return false;
	}
	/**
	 * Retourne la date de dernière modification d'un fichier
	 * @param string $filepath Chemin du fichier.
	 * @return string Date de dernière modification au format Y-m-d H:i:s
	 */
	public function filemtime($filepath) {
		return filemtime(!$this->mode_local?'ftp://'.urlencode($this->username).':'.urlencode($this->password).'@'.$this->serveur.':'.$this->port.$filepath:$filepath);
	}
	/**
	 * Modifie le mode d'un fichier.
	 * @param string $filename Chemin du fichier.
	 * @param string $mode Mode à appliquer au fichier.
	 * @return boolean `true` si succès, `false` sinon.
	 */
	public function updateChmod($filename, $mode = '0775') {return !$this->mode_local?ftp_chmod($this->hFTP, octdec($mode), $filename):chmod($filename, octdec($mode));}
	/**
	 * Retourne l'instance de la connexion au serveur FTP
	 * @return resource
	 */
	public function getInstance() {return $this->hFTP;}
	/**
	 * Retourne la taille d'un répertoire.
	 *
	 * ATTENTION, cette fonction ne marche qu'en mode local !
	 * @param string $dirname Chemin du répertoire.
	 * @param string $unite Unité souhaitée pour exprimer la taille du répertoire :
	 * - `K` : Kilo-octet
	 * - `M` : Méga-octet
	 * - `G` : Giga-octet
	 * @return integer Taille du répertoire dans l'unité souhaitée arrondit à 2 chiffres après la virgule.
	 */
	public function sizeDirectory($dirname, $unite = 'G') {
		$size = 0;
		if($this->mode_local) {
			if(preg_match("/([0-9]{1,}).?([0-9]{0,})(K|M|G)/", exec("du -sh '".$dirname."' | awk '{print $1}'"), $match)) {
				$sizeTemp = floatval(str_replace($match[3], '', $match[0]));
				//On étudie la sortie obtenue
				switch($match[3]) {
					case 'K':
						switch($unite) {
							case 'K':$size = $sizeTemp;break;
							case 'M':$size = $sizeTemp/1024;break;
							case 'G':$size = $sizeTemp/1048576;break;
						}
						break;
					case 'M':
						switch($unite) {
							case 'K':$size = $sizeTemp*1024;break;
							case 'M':$size = $sizeTemp;break;
							case 'G':$size = $sizeTemp/1024;break;
						}
						break;
					case 'G':
						switch($unite) {
							case 'K':$size = $sizeTemp*1024*1024;break;
							case 'M':$size = $sizeTemp*1024;break;
							case 'G':$size = $sizeTemp;break;
						}
						break;
				}
			}
		}
		return round($size,2);
	}
}

