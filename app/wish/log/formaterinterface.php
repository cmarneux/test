<?php
/**
 * formaterinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Wish\Log;

interface FormaterInterface{
	public function format($message);
}
