<?php
/**
 * loggerinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Wish\Log;

interface LoggerInterface{
	public function write($message);
}
