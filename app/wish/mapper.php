<?php
/**
 * mapper.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish;

use Wish\Models\JSONApiObjectInterface;
use Wish\Models\Model;
use Wish\Models\ModelJSON;
use Wish\Models\PublicFieldsInterface;
use Wish\Models\SearchResult;

/**
 * Class Mapper
 * @package BoondManager\Lib
 */
class Mapper {
	/**
	 * @var \Wish\Models\Model
	 */
	protected $className = Model::class;

	/**
	 * @var bool indicates if basic entity data has to be retrieved from bdd if only it's id is given
	 */
	protected $retrieveBasic = true;

	/**
	 * Mapper constructor.
	 * @param string $class
	 */
	public function __construct($class = null) {
		if($class) $this->className = $class;
	}

	/**
	 * instantiate an object from an array
	 * @param $data
	 * @return \Wish\Models\Model
	 */
	public function map($data) {
		/** @var Model $object */
		$object = new $this->className;
		$object->fromArray($data);
		return $object;
	}

	/**
	 * build a single RowObject from the array
	 * @param $array
	 * @return Model[]
	 */
	public function mapArray($array) {
		$list = [];
		foreach($array as $row){
			$list[] = $this->map($row);
		}
		return $list;
	}

	/**
	 * create a new array using another one. All key/values extracted are removed from the original array
	 * @param Model|array $data the original array
	 * @param array $keyToExtract the key can be map to new fields
	 * @return array
	 *
	 * example for $keyToExtract:
	 *  $keyToExtract = ['ID_PROFIL', 'PROFIL_NOM']; // will extract the 2 keys `ID_PROFIL` and `PROFIL_NOM`
	 *  $keyToExtract = ['ID_AUTHOR'=>'ID_PROFIL', 'PROFIL_NOM']; will extract the keys `ID_AUTHOR` and `PROFIL_NOM` and rename the key `ID_AUTHOR` to `ID_PROFIL`
	 */
	public static function extractData($data, array $keyToExtract) {
		$extractedData = [];
		if($data instanceof Model) $data = $data->toArray();
		foreach($keyToExtract as $key=>$value){
			if(is_int($key) && array_key_exists($value, $data)){
				$extractedData[$value] = $data[$value];
			}
			else if(array_key_exists($key, $data)){
				$extractedData[$value] = $data[$key];
			}
		}
		return $extractedData;
	}

	/**
	 * create a new array using another one. All key/values extracted are removed from the original array
	 * @param array $data the original array
	 * @param array $keysToRename the key can be map to new fields
	 * @return array
	 *
	 * example for $keyToExtract:
	 *  $keyToExtract = ['ID_PROFIL', 'PROFIL_NOM']; // will extract the 2 keys `ID_PROFIL` and `PROFIL_NOM`
	 *  $keyToExtract = ['ID_AUTHOR'=>'ID_PROFIL', 'PROFIL_NOM']; will extract the keys `ID_AUTHOR` and `PROFIL_NOM` and rename the key `ID_AUTHOR` to `ID_PROFIL`
	 */
	public static function adaptData($data, array $keysToRename) {
		$extractedData = [];
		foreach($data as $key=>$value){
			if(array_key_exists($key, $keysToRename)){
				$extractedData[$keysToRename[$key]] = $value;
			}else
				$extractedData[$key] = $value;
		}
		return $extractedData;
	}

	/**
	 * @return boolean
	 */
	public function isRetrieveBasic() {
		return $this->retrieveBasic;
	}

	/**
	 * @param bool $retrieveBasic
	 * @return $this
	 */
	public function retrieveBasic($retrieveBasic = true) {
		$this->retrieveBasic = $retrieveBasic;
		return $this;
	}

	/**
	 * Convert a database's data to camelCase
	 * @param \Wish\Models\Model $object
	 * @param string [$class] simple mapping to one class
	 * @return \Wish\Models\Model|mixed
	 */
	public static function fromSQL($object) {
		$class = (func_num_args() == 2) ? func_get_arg(1) : null;
		if($class) return self::createObject($class, $object);
		else return new Model($object);
	}

	/**
	 * Convert an array of database's data to camelCase
	 * @param array $data
	 * @param string [$class] simple mapping to one class
	 * @return array
	 */
	public static function fromDatabaseArray(array $data) {
		$class = (func_num_args() == 2) ? func_get_arg(1) : null;
		foreach($data as $k=>$v){
			$data[$k] = static::fromSQL($v, $class);
		}

		return $data;
	}

	/**
	 * Convert a SearchResult's rows to camelCase
	 * @param \Wish\Models\Model $object
	 * @return \Wish\Models\Model|mixed
	 */
	public static function fromRow($object) {
		return self::fromSQL($object);
	}

	/**
	 * Convert a database's SearchResult to camelCase
	 * @param SearchResult $data
	 * @return SearchResult
	 */
	public static function fromSearchResult($data){
		foreach($data->rows as $k=>$row){
			$data->rows[$k] = static::fromRow($row);
		}
		return $data;
	}

	/**
	 * @param $class
	 * @param $data
	 * @param bool $allowNoId
	 * @return mixed
	 * @throws \Exception
	 */
	public static function createObject($class, $data, $allowNoId = false){
		$instance = new $class([]);

		if( ! $instance instanceof PublicFieldsInterface) throw new \Exception('la classe doit etre une instance de \'PublicFieldsInterface\'');
		if( ! $instance instanceof Model) throw new \Exception('la classe doit etre une instance de \'Model\'');

		if($data instanceof Model) $data = $data->toArray();

		/**
		 * @var ModelJSON $instance
		 */
		self::databaseArrayToModel($data, $instance);

		//Not doing this test here would mean to do it in many other places
		if(!$allowNoId) {
			if ($instance instanceof JSONApiObjectInterface && !$instance->getID()) {
				/*
				if ($instance->toArray())
					throw new \Exception('try to create an object without id');
				*/
				return null;
			}
		}

		return $instance;
	}

	/**
	 * @param $data
	 * @param ModelJSON $instance
	 * @return ModelJSON
	 */
	public static function databaseArrayToModel($data, ModelJSON $instance){
		$data = self::extractData($data, $instance::getPublicFieldsMapping());

		// champs mappés
		$mappers = array_column($instance::getPublicFieldsDefinition(), 'mapper', 'name');
		foreach($mappers as $name=>$mapper){
			if(array_key_exists($name, $data)) $data[$name] = Tools::mapData($data[$name], $mapper);
		}
		// champs sérializé
		$serializedFields = array_column($instance::getPublicFieldsDefinition(), 'serializedArray', 'name');
		foreach($serializedFields as $name=>$isSerialized){
			if($isSerialized && array_key_exists($name, $data)) $data[$name] = array_filter(Tools::unserializeArray($data[$name]));
		}
		// champs sérializé
		$serializedFields = array_column($instance::getPublicFieldsDefinition(), 'doubleSerializedArray', 'name');
		foreach($serializedFields as $name=>$isSerialized){
			if($isSerialized && array_key_exists($name, $data)) $data[$name] = array_filter(Tools::unserializeDoubleArray($data[$name]));
		}

		// serializerFourni
		$unserializedCallbacks = array_column($instance::getPublicFieldsDefinition(), 'unserializeCallback', 'name');
		foreach($unserializedCallbacks as $name=>$callback){
			if(array_key_exists($name, $data)) $data[$name] = forward_static_call([get_class($instance), $callback], $data[$name]);
		}

		$instance->fromArray( $data );

		return $instance;
	}

	/**
	 * @param ModelJSON $instance
	 * @return array
	 */
	public static function modelToDatabaseArray(ModelJSON $instance){
		$mappers = array_column($instance::getPublicFieldsDefinition(), 'mapper', 'name');
		$objectData = $instance->toArray();
		foreach($mappers as $name => $mapper){
			if(array_key_exists($name, $objectData))
				$objectData[$name] = Tools::reverseMapData($objectData[$name], $mapper);
		}

		$serializedFields = array_column($instance::getPublicFieldsDefinition(), 'serializedArray', 'name');
		foreach($serializedFields as $name=>$isSerialized) {
			if($isSerialized && array_key_exists($name, $objectData)) $objectData[$name] = Tools::serializeArray($objectData[$name]);
		}

		$serializedFields = array_column($instance::getPublicFieldsDefinition(), 'doubleSerializedArray', 'name');
		foreach($serializedFields as $name=>$isSerialized) {
			if($isSerialized && array_key_exists($name, $objectData)) $objectData[$name] = Tools::serializeDoubleArray($objectData[$name]);
		}

		// serializerFourni
		$serializedCallbacks = array_column($instance::getPublicFieldsDefinition(), 'serializeCallback', 'name');
		foreach($serializedCallbacks as $name=>$callback){
			if(array_key_exists($name, $objectData)) $objectData[$name] = forward_static_call([get_class($instance), $callback], $objectData[$name]);
		}

		return Tools::reversePublicFieldsToPrivate($instance->getPublicFieldsMapping(), $objectData);
	}

	/**
	 * @param $class
	 * @param $data
	 * @return array
	 */
	public static function createObjectArray($class, $data){
		$list = [];
		if($data) foreach($data as $row){
			$list[] = self::createObject($class, $row);
		}
		return $list;
	}
}
