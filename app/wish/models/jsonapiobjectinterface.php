<?php
/**
 * jsonapiobjectinterface.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Models;

/**
 * Interface JSONApiObject so the object should respect the following format http://jsonapi.org/format/
 * @link http://jsonapi.org/format/
 * @package Wish\Models
 */
interface JSONApiObjectInterface{
	/**
	 * return the object ID
	 * @return string
	 */
	public function getID();

	/**
	 * set the object ID
	 * @param string $id
	 * @return mixed
	 */
	public function setID($id);

	/**
	 * return the object type
	 * @return string
	 */
	public function getType();

	/**
	 * return the object attributes
	 * @return array
	 */
	public function getAttributes();

	/**
	 * return the object relationships
	 * @return JSONApiObjectInterface[]
	 */
	public function getRelationships();
}
