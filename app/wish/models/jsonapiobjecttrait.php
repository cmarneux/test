<?php
/**
 * jsonapiobject.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Models;

use Wish\Encoders\JsonAPI;
use Wish\Tools;

/**
 * Interface JSONApiObject so the object should respect the following format http://jsonapi.org/format/
 *
 * class using this trait should have the followings attributes :
	protected static $_jsonIDAttribute;
	protected static $_jsonType;
 *
 *
 * @link http://jsonapi.org/format/
 * @package Wish\Models
 *
 */
trait JSONApiObjectTrait{

	/**
	 * @var array
	 */
	private $_relationships = [];

	private $_relationshipMapping = [];

	private $_relCacheDef = null;

	private $_relCache = null;

	/**
	 * JSONApiObjectTrait constructor.
	 */
	public function __construct()
	{
		call_user_func_array('parent::__construct', func_get_args());

		$this->initRelationships();
	}

	/**
	 *
	 */
	protected function initRelationships(){}

	public function resetRelationships(){
		$this->_relationships = [];
		$this->_relationshipMapping = [];
		$this->_relCache = null;
		$this->_relCacheDef = null;
	}

	/**
	 * @param $fieldForID
	 * @param $toField
	 * @param $className
	 * @return $this
	 */
	protected function setRelationships($fieldForID, $toField, $className){
		$this->_relationships[$fieldForID] = ['id' => $fieldForID, 'name'=>$toField, 'class'=>$className];
		$this->_relationshipMapping[$fieldForID] = $toField;
		return $this;
	}

	/**
	 * @param $fieldForID
	 * @param $toField
	 * @return $this
	 */
	protected function setGroupedRelationships($fieldForID, $toField){
		$this->_relationships[$fieldForID] = ['id' => $fieldForID, 'name'=>$toField, 'array'=>true];
		$this->_relationshipMapping[$fieldForID] = $toField;
		return $this;
	}

	/**
	 * @param $fieldForID
	 * @param $callback
	 * @return $this
	 */
	protected function setDynamicTypedRelationship($fieldForID, $toField, $callback){
		$this->_relationships[$fieldForID] = ['id' => $fieldForID, 'name' => $toField, 'callback' => $callback];
		$this->_relationshipMapping[$fieldForID] = $toField;
		return $this;
	}

	/**
	 * return the object ID
	 * @return string
	 * @throws \Exception
	 */
	public function getID(){
		$fieldID = $this->getJSONIDAttribute();
		return strval($this->$fieldID);
	}

	/**
	 * @param $id
	 */
	public function setID($id){
		$fieldID = $this->getJSONIDAttribute();
		$this->$fieldID = $id;
	}

	/**
	 * @return string
	 */
	public static function getJSONIDAttribute(){
		return isset(static::$_jsonIDAttribute) ? static::$_jsonIDAttribute : 'id';
	}

	/**
	 * @deprecated
	 * @fixme pas de documentation, doute sur l'utilité de la méthode pour le moment
	 * @return mixed
	 */
	public static function getJSONTable(){
		return str_replace('ID_', 'TAB_', self::getJSONIDAttribute());
	}

	/**
	 * return the object type
	 * @return string
	 * @throws \Exception
	 */
	public function getType(){
		$type = static::$_jsonType;
		if(!isset($type)) return get_called_class();
		else return $type;
	}

	public static function getJsonType() {
		return static::$_jsonType;
	}

	/**
	 * return the object attributes
	 * @return array
	 */
	public function getAttributes(){
		// la premiere vérification est pour BoondManager car normalement toutes les classes qui utiliseront ce trait hériteront de RowObject
		// on utilise cette connaissance pour eviter de faire un trait qui melangerait rowObject & jsonapiobject
		// ou alors rowobject doit etre la classe qui utilise jsonapiobject mais dans ce cas la il faudra faire attention
		// aux rowobjects qui ne sont pas destiné au front
		if($this instanceof Model) $attributes = $this->toArray();
		elseif ($this instanceof \JsonSerializable) $attributes = $this->jsonSerialize();
		else $attributes = get_class_vars(static::class);

		// une relationship n'est pas un attribut (et la relationship prime)
		foreach($this->_relationshipMapping as $id => $name){
			unset($attributes[$id]);
			unset($attributes[$name]);
		}

		return $attributes;
	}

	/**
	 * retrieve the list of relationship definitions including a resolution of dynamic relationships based on the current state
	 * @return array
	 */
	protected function getRelationshipDefinitions(){
		if(is_null($this->_relCacheDef)) {
			$relationships = [];
			foreach ($this->_relationships as $fieldID => $link) {
				if (array_key_exists('callback', $link)) {
					$newLink = [
						'id' => $link['id'],
						'name' => $link['name'],
						'class' => call_user_func($link['callback'], $this)
					];
					$relationships[ $fieldID ] = $newLink;
				} else {
					$relationships[ $fieldID ] = $link;
				}
			}
			$this->_relCacheDef = $relationships;
		}
		return $this->_relCacheDef;
	}

	/**
	 * return a mapping for relation with localName => publicName
	 * @return array example:
	 * [
	 *   'ID_PROFIL' => 'resource',
	 *   'ID_SOCIETE' => 'agency'
	 * ]
	 */
	public function getRelationshipMapping(){
		return $this->_relationshipMapping;
	}

	/**
	 * return a list of relationships
	 * @return JSONApiObjectTrait[]
	 */
	public function getRelationships(){
		$id = Tools::getContext()->startTiming('getRelationships', true);
		$relationships = [];
		foreach ($this->getRelationshipDefinitions() as $fieldID => $link) {
			$relationships[ $link['name'] ] = $this->getRelationship($fieldID, $link);
		}
		Tools::getContext()->endTiming($id);
		return $relationships;
	}

	/**
	 * @return array
	 */
	public function getRelationshipsValue(){
		$ids = [];
		$rels = $this->getRelationships();
		foreach($rels as $name => $obj){
			if(!$obj) continue;
			$ids = $obj->getID();
		}
		return $ids;
	}

	/**
	 * @param $fieldID
	 * @param null $definition
	 * @return JSONApiObjectTrait|array|null
	 */
	public function getRelationship($fieldID, $definition = null){

		if(!$definition) $definition = $this->getRelationshipDefinition($fieldID);

		$name = $definition['name'];

		if(isset($definition['array'])){
			$array = ($this->$name) ? $this->$name : [];
			return $array;
		}
		if ($this->$name) {
			return $this->$name;
		}else
			return null;
	}

	/**
	 * @param $name
	 * @return bool
	 */
	protected function isRelationshipName($name){
		$relationships = $this->getRelationshipDefinitions();
		return in_array($name, array_column($relationships, 'name'));
	}

	/**
	 * @param $id
	 * @return null
	 */
	public function getRelationshipName($id){
		$relationships = $this->getRelationshipMapping();
		return isset($relationships[$id])?$relationships[$id]:null;
	}

	/**
	 * @param $name
	 * @return null
	 */
	public function getRelationshipID($name){
		$relationships = array_flip($this->getRelationshipMapping());
		return isset($relationships[$name])?$relationships[$name]:null;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	protected function isRelationshipID($id){
		return array_key_exists($id, $this->_relationships);
	}

	/**
	 * @param $fieldID
	 * @return mixed
	 * @throws \Exception
	 */
	protected function getRelationshipDefinition($fieldID){
		$mapping = $this->getRelationshipDefinitions();
		if(!array_key_exists($fieldID, $mapping)) throw new \Exception("there is no relationship with the field '$fieldID'");
		return $mapping[$fieldID];
	}

	/**
	 * @param $fieldID
	 * @param null $definition
	 * @return JSONApiObjectInterface
	 */
	protected function initRelationship($fieldID, $definition = null){
		if(!$definition) $definition = $this->getRelationshipDefinition($fieldID);

		$name      = $definition['name'];
		$className = $definition['class'];

		/** @var JSONApiObjectInterface $object */
		$object = new $className;
		$object->setID($this->$fieldID);
		$this->$name = $object;

		return $object;
	}

	/**
	 * @deprecated use JsonAPI::instance()->serialize($object) instead
	 * @return mixed
	 */
	public function encode(){
		$encoder = new JsonAPI();
		$dataEncoded = $encoder->serialize($this);
		return $dataEncoded['data'];
	}

	/**
	 * @param $name
	 * @return array|null|string|JSONApiObjectTrait
	 * @throws \Exception
	 */
	public function &get($name){
		// vu que l'on est dans un trait, l'appel à la classe parente doit etre vérifiée, cela correcspond normalement à un get du RowObject
		if(method_exists(get_parent_class($this), 'get')){
			$value = parent::get($name);
			return $value;
		}else
			throw new \Exception('no implementation found for this method');
	}
}
