<?php
/**
 * rowobject.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Models;
use BoondManager\Databases\Local\AbstractObject;
use Wish\Tools;

/**
 * Class RowObject
 *
 * represent a row result from mysql
 *
 * @package Wish\Models
 */
class Model extends \Magic implements \JsonSerializable, \Iterator{
	/**#@+
	 * Default value
	 * @var int
	 **/
	const TAB_DEFAULT = -1;

	/**
	 * @var
	 */
	private $_currentKey;

	/**
	 * @var array
	 */
	private $_oldValues = [];

	/**
	 * contains all fields/keys
	 * @var array
	 */
	public $_data = array();

	private $_backupArray;
	private $_backup;


	/**
	 * callback function after a key/field is updated
	 * @param $updateKey
	 * @param mixed $value
	 * @param mixed $oldValue
	 * @deprecated
	 */
	protected function onUpdatedData($updateKey, $value = null, $oldValue = null){}

	/**
	 * RowObject constructor.
	 * @param array $data data to populate the object
	 */
	public function __construct($data = []){
		if($data) $this->fromArray($data);
		$this->init();
	}

	/**
	 *
	 */
	protected function init(){}

	/**
	 * return an array of all fields.
	 * ex:
	 *  ['id'=>1, 'name'=>'john']
	 * @return array
	 */
	public function toArray($recursive = false) {
		if($recursive)
			return $this->_itemToArray($this->_data);
		else
			return $this->_data;
	}

	/**
	 * recursive transformation to array
	 * @param $item
	 * @return array
	 */
	private function _itemToArray($item) {
		if(Tools::isArray($item)){
			$array = [];
			foreach($item as $key => $value) {
				$array[$key] = $this->_itemToArray($value);
			}
			return $array;
		}else if(is_object($item) && method_exists($item, 'toArray')) {
			return $item->toArray();
		}else{
			return $item;
		}
	}

	/**
	 * populate the object from an array
	 * @param $data
	 * @param AbstractObject|null $db
	 * @return $this|null
	 */
	public function fromArray($data, AbstractObject $db = null){
		if($data) {
			if ($data instanceof Model) $data = $data->toArray(); // ça ne devrait pas etre la
			if ($db) {
				// FIXME: enlever cette partie, utiliser à la rigueur (mais a discuter avant) une methode fromDB et surcharcher les champs nécéssaire.
				// FIXME (suite) : dans tout les cas, cette méthode ne correspond pas à l'approche générale et le code ne correspond pas au nom de la fonction
				$basicFields = self::getBasicFields();
				if (count($basicFields) != count(array_intersect($basicFields, array_keys($data)))) {
					$class = get_called_class();
					$basicData = $db->singleExec('
						SELECT ' . implode(',', $basicFields) . '
						FROM ' . $class::getJSONTable(). '
						WHERE ' . $class::getJSONIDAttribute() . '=:id', ['id' => $data[$class::getJSONIDAttribute()]]);
					$data = array_merge($data, $basicData ? $basicData->toArray():[]);
				}
			}
			foreach ($data as $key => $value) $this->set($key, $value);
			return $this;
		} else return null;
	}

	/**
	 * Get basic fields of an entity
	 * @param bool $public
	 * @return array
	 */
	public static function getBasicFields($public = false){
		$class = get_called_class();
		return $public ?
			array_map(function($value){return $value['name'];}, array_values(array_filter($class::getPublicFieldsDefinition(), function ($field) {return $field['basic'];})))
			:
			array_keys(array_filter($class::getPublicFieldsDefinition(), function ($field) {return $field['basic'];}))
			;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition()
	{
		return [];
	}

	/**
	 * merge the object with other data
	 * @param Model|array
	 * @return $this
	 */
	public function mergeWith($object){
		if($object instanceof Model)
			$data =  $object->toArray();
		if(is_array($data)){
			$this->fromArray($data);
		}
		return $this;
	}

	/**
	 * Return TRUE if key is not empty
	 * @return bool
	 * @param $key string
	 **/
	function exists($key)
	{
		return array_key_exists($key, $this->_data);
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function __isset($key){
		return $this->exists($key) && !is_null($this->get($key));
	}

	/**
	 * Bind value to key
	 * @return mixed
	 * @param $key string
	 * @param $val mixed
	 **/
	function set($key, $val)
	{
		// si on met à jour une valeur et qu'on a pas de copie de la valeur d'origine, on crée cette copie
		if(array_key_exists($key, $this->_data) && $this->_data[$key] != $val && !array_key_exists($key, $this->_oldValues)){
			$this->_oldValues[$key] = $this->_data[$key];
		}

		// TODO à supprimer
		$oldValue = isset($this->_data[$key])?$this->_data[$key]:null;
		$this->_data[$key] = $val;
		if($val !== $oldValue) {
			$this->onUpdatedData($key, $val, $oldValue);
		}
	}

	/**
	 * Retrieve contents of key
	 * @return mixed
	 * @param $key string
	 **/
	function &get($key)
	{
		if($this->exists($key, $this->_data))
			return $this->_data[$key];
		else return null;
	}

	/**
	 * Unset key
	 * @return NULL
	 * @param $key string
	 **/
	function clear($key)
	{
		if(array_key_exists($key, $this->_data)) {
			$oldValue = $this->_data[$key];
			unset($this->_data[$key]);
			if($oldValue !== null) {
				$this->onUpdatedData($key);
			}
		}
	}

	/**
	 * give the data to serialize
	 * @implements \JsonSerializable
	 * @return array
	 */
	public function jsonSerialize()
	{
		return $this->_data;
	}

	/**
	 * remove some fields from the object (reduct)
	 * @param array $fields
	 * @return $this
	 */
	public function removeFields($fields){
		foreach($fields as $f){
			$this->clear($f);
		}
		return $this;
	}

	/**
	 * remove all fields except the given one. This is a reccursive method
	 *
	 * Comportements importants:
	 * [
	 *    fieldName(instance of RowObject) => [] // Laisse passer toutes les propriétés du RowObject concerné
	 * ]
	 * est équivalent à
	 * [
	 *    fieldName(instance of RowObject)
	 * ]
	 *
	 * @param array $fields
	 * @return $this
	 */
	public function filterFields($fields){
		if(!$fields) return $this; //~ On ne filtre rien si le tableau est vide (ce ne serait pas logique et cela permet d'autoriser tout de manière simple)

		$newData = [];
		foreach($fields as $key => $field){
			if(is_array($field)){
				// there is a subfilter, so let's apply it. It can be done on an object, or a simple array, or an array of objects
				if(isset($this->_data[$key])){
					if($this->_data[$key] instanceof Model) $newData[$key] = $this->_data[$key]->filterFields($field);
					else if(is_array($this->_data[$key])) {
						$newData[$key] = [];
						foreach($this->_data[$key] as $included) {
							if(is_array($included) && !($included instanceof Model)) $included = new Model($included);
							if($included instanceof Model) $newData[$key][] = $included->filterFields($field);
						}
					}
				}
			} else if( isset($this->_data[$field])) {
				// there is no subfields specified, so it should take all properties
				$newData[$field] = $this->_data[$field];
			}
		}

		$className = get_class($this);
		$clone = new $className();
		$clone->fromArray($newData);
		return $clone;
	}

	/**
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 * @since 5.0.0
	 */
	public function current()
	{
		return $this->_data[$this->key()];
	}

	/**
	 * Move forward to next element
	 * @link http://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function next()
	{
		$this->_currentKey++;
	}

	/**
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 * @since 5.0.0
	 */
	public function key()
	{
		$keys = array_keys($this->_data);
		return $keys[$this->_currentKey];
	}

	/**
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 * @since 5.0.0
	 */
	public function valid()
	{
		return array_key_exists($this->_currentKey, array_keys($this->_data));
	}

	/**
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function rewind()
	{
		$this->_currentKey = 0;
	}

	/**
	 * Group fields in an other object with given key (or including it in an array of objects with given key)
	 * - can not override an existing key
	 * - object with no id is ignored
	 * @param $key
	 * @param $object
	 * @param bool $push
	 * @return $this
	 * @throws \Exception
	 */
	public function groupFieldsAs($key, $object, $push = false){
		if(is_array($object)) {
			if(!$this->exists($key)) $this->set($key, $object);
			else $this->_data[$key] = array_merge($this->_data[$key], $object);
		} else if(!method_exists($object, 'getID') || $object->getID()){
			if($this->exists($key) && ! $push) throw new \Exception('Can not redefine include '.$key, 666);
			if($push){
				if(!$this->exists($key)) $this->set($key, [$object]);
				else array_push($this->_data[$key], $object);
			} else $this->set($key, $object);
		}
		return $this;
	}

	/**
	 * Get bdd value of a constant assuming the public constant respects the following format:
	 * const {$prefix}_{strtoupper($value)} = $value;
	 * and the corresponding bdd constant respects the following format:
	 * const {$prefix}_{strtoupper($value)}_BDD = {bdd value};
	 * @param $prefix
	 * @param $value
	 * @return mixed
	 */
	public static function bddFromPublic($prefix, $value){
		//Use late static binding to get constant from child class
		return constant(get_called_class().'::'.$prefix.'_'.strtoupper($value).'_BDD');
	}

	/**
	 * return a list of modified fields
	 * @param bool $all if false (default) will return changes only for scalar attributes
	 * @return array with each item corresponding of a modified field. Ex:
	 * [
	 * 'lastName' => [ 'old' => 'Dupond', 'new' => 'Dupont']
	 * 'title' => ['old' => null, 'new' => 'Ingénieur']
	 * ]
	 */
	public function getChangedValues( $all = false ) {
		if($all) return $this->_oldValues;
		$values = [];
		foreach($this->_oldValues as $field => $val) {
			if(is_scalar($val) || is_null($val) && is_scalar($this->get($field))) {
				$values[ $field ] = ['old' => $val, 'new' => $this->get($field)];
			}
		}
		return $values;
	}

	public function getOldValue($field){
		return isset($this->_oldValues[$field]) ? $this->_oldValues[$field] : null;
	}

	public function getChangedValue($field){
		if($this->hasChanged($field)) {
			$change = $this->getChangedValues()[$field];
			if($change['old'] != $change['new']) return $change;
		}
		return null;
	}

	public function hasChanged($field){
		return isset($this->_oldValues[$field]);
	}

	/**
	 * @return $this
	 */
	public function backupData() {
		$this->_backup = clone $this;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function getBackup(){
		return $this->_backup;
	}

	/**
	 * clone all sub objects
	 */
	public function __clone() {
		foreach($this->_data as $field => $value) {
			if($value instanceof Model) $this->_data[$field] = clone $value;
			if(is_array($value)) {
				foreach($value as $i=> $subValue){
					if($subValue instanceof Model) $this->_data[$field][$i] = clone $subValue;
				}
			}
		}
	}

	/**
	 * @deprecated
	 */
	public function backupArrayData() {
		$this->_backupArray = $this->toArray(true);
	}

	/**
	 * @deprecated
	 */
	public function compareWithBackup() {
		$array = $this->toArray();
		$diff1 = Tools::arrayRecursiveDiff($this->_backupArray, $array);
		$diff2 = Tools::arrayRecursiveDiff($array, $this->_backupArray);

		return $this->mergeChangedDiff($diff1, $diff2);
	}

	/**
	 * @deprecated
	 */
	private function mergeChangedDiff($item1, $item2){
		if(is_array($item1) && is_array($item2)) {
			$result = [];
			foreach ($item1 as $key => $value) {
				$value2 = array_key_exists($key, $item2) ? $item2[$key] : null;
				$result[$key] = $this->mergeChangedDiff($value, $value2);
			}
			foreach($item2 as $key => $value2) {
				if(array_key_exists($key, $item1)) continue;
				$result[$key] = $this->mergeChangedDiff(null, $value2);
			}
			return $result;
		} else {
			return ['old' => $item1, 'new' => $item2];
		}
	}
}
