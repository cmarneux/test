<?php
/**
 * ModelJSON.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Models;

/**
 * Class ModelJSON
 * @package Wish\Models
 */
abstract class ModelJSON extends Model implements PublicFieldsInterface{
	use PublicFieldsTrait;

	public function getAttributes()
	{
		return $this->toArray();
	}
}
