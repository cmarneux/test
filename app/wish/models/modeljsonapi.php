<?php
/**
 * bmpublicobject.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Models;

/**
 * Class ModelJSONAPI
 * @package Wish\Models
 */
abstract class ModelJSONAPI extends ModelJSON  implements JSONApiObjectInterface{
	use JSONApiObjectTrait;

	const REF_PREFIX = '__UNDEFINED__';

	/**
	 * @param string $field
	 * @return null
	 */
	public function getPublicName($field){
		$def = static::getPublicFieldsDefinition();
		if (array_key_exists($field, $def)) return $def[ $field ]['name'];
		$relationships = $this->getRelationshipDefinitions();
		if(array_key_exists($field, $relationships)) return $relationships[ $field ]['name'];
		else return null;
	}

	/**
	 * @param int $id
	 * @return string
	 */
	public static function buildReference($id){
		return static::REF_PREFIX.$id;
	}

	/**
	 * @return string
	 */
	public function getReference(){
		return self::buildReference( $this->getID() );
	}

	public function getChangedRelationship($field)
	{
		if ($this->hasChanged($field)) {
			$change = $this->_oldValues[ $field ];
			if ($change['old'] || $change['new']) {
				if($change['old'] instanceof ModelJSONAPI && $change['new'] instanceof ModelJSONAPI
					&& get_class($change) == get_class($change)
					&& $change['old']->getID() == $change['new']->getID()
				) {
					return null;
				}
				return $change;
			}
		}
		return null;
	}
}
