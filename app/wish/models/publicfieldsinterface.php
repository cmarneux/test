<?php
/**
 * remapobject.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Models;

/**
 * Interface PublicFieldsInterface
 * @package Wish\Models
 */
interface PublicFieldsInterface{
	/**#@+
	 * Input's type
	 * @var string
	 **/
	const TYPE_INT = 'int';
	const TYPE_FLOAT = 'float';
	const TYPE_BOOLEAN = 'bool';
	const TYPE_STRING = 'string';
	const TYPE_DATE = 'date';
	const TYPE_DATETIME = 'datetime';
	const TYPE_TERM = 'term';
	const TYPE_ARRAY = 'array';
	const TYPE_OBJECT = 'object';
	const TYPE_IGNORE = 'ignore';

	/**
	 * @return array
	 */
	public static function getPublicFieldsDefinition();

	/**
	 * @return array
	 */
	public static function getPublicFieldsMapping();

	/**
	 * @param string $field
	 * @return string
	 */
	public function getPublicName($field);

	/**
	 * @param string $field
	 * @return mixed
	 */
	public function getPublicValue($field);

	/**
	 * @param string $field
	 * @return string
	 */
	public function getPrivateName($field);

	/**
	 * @return array
	 */
	public function getAttributes();

	/**
	 * @param $field
	 * @return bool
	 */
	public function shouldEncodeField($field);
}
