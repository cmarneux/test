<?php
/**
 * publicfields.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Wish\Models;

/**
 * Class PublicFields (must be used on a class that implement publicFieldInterface)
 * @package Wish\Models
 */
trait PublicFieldsTrait{

	/**
	 * @return array
	 */
	public static function getPublicFieldsMapping(){
		$def = static::getPublicFieldsDefinition();
		return array_combine(array_keys($def), array_column($def, 'name'));
	}

	/**
	 * @return array
	 */
	public static function getPrivateFieldsMapping(){
		return array_flip(static::getPublicFieldsMapping());
	}

	/**
	 * @param string $field
	 * @return string
	 */
	public function getPublicName($field){
		$def = static::getPublicFieldsDefinition();
		if(array_key_exists($field, $def)) return $def[$field]['name'];
		else return null;
	}

	/**
	 * @param $field
	 * @return null
	 */
	public function getPrivateName($field){
		$def = self::getPrivateFieldsMapping();
		if(array_key_exists($field, $def)) return $def[$field];
		else return null;
	}

	/**
	 * @param $key
	 * @return bool
	 * @deprecated
	 */
	public function isDBName($key){
		return array_key_exists($key, static::getPublicFieldsDefinition());
	}

	/**
	 * @param string $field
	 * @return mixed
	 */
	public function getPublicValue($field){
		$def = static::getPublicFieldsDefinition();
			$name = $this->getPrivateName($field);
			if (array_key_exists($name, $def))
				return $this->_cast($this->$field, $def[ $name ]['type']);

		return null;
	}

	/**
	 * @param $value
	 * @param $type
	 * @return bool|float|int|\stdClass|string
	 * @throws \Exception
	 */
	private function _cast($value, $type){
		switch ($type){
			case PublicFieldsInterface::TYPE_BOOLEAN: return boolval($value);
			case PublicFieldsInterface::TYPE_FLOAT: return floatval($value);
			case PublicFieldsInterface::TYPE_INT: return intval($value);
			case PublicFieldsInterface::TYPE_DATETIME:
				$datetime = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
				if($datetime)
					return $datetime->format(\DateTime::ISO8601);
				else
					return '';
			case PublicFieldsInterface::TYPE_DATE:
				$maxDate = \DateTime::createFromFormat('Y-m-d', '3000-01-01');
				$datetime = \DateTime::createFromFormat('Y-m-d', $value);
				if($datetime)
					return ($datetime->diff($maxDate)->days === 0) ? '' : $datetime->format('Y-m-d');
				else
					return '';
			case PublicFieldsInterface::TYPE_TERM:
				$datetime = \DateTime::createFromFormat('Y-m-d', $value);
				if($datetime)
					return $datetime->format('Y-m');
				else
					return '';
			case PublicFieldsInterface::TYPE_ARRAY:
				if(!$value) return [];
				else if(is_array($value)) return array_values($value);
				else throw new \Exception('wrong data type for array'); //ne devrait pas se produire
			case PublicFieldsInterface::TYPE_OBJECT:
				if(!$value) return new \stdClass();
				else return $value;
			case PublicFieldsInterface::TYPE_STRING:
				return strval($value);
			default: return $value;
		}
	}

	/**
	 * @param $field
	 * @return bool
	 */
	public function shouldEncodeField($field)
	{
		$dbKey = $this->getPrivateName($field);
		$def   = static::getPublicFieldsDefinition();
		if (array_key_exists($dbKey, $def)) {
			if (isset($def[ $dbKey ]['removeIfEmpty']) && $def[ $dbKey ]['removeIfEmpty'] && !$this->$field) return false;
			if (isset($def[ $dbKey ]['removeIfEqual']) && $this->$field == $def[$dbKey]['removeIfEqual']) return false;
			if (isset($def[ $dbKey ]['removeIfNull']) && $this->$field === null) return false;
		}
		return true;
	}

	/**
	 * @return array
	 */
	public static function getPublicFieldsTyping(){
		return array_column(static::getPublicFieldsDefinition(), 'type', 'name');
	}
}
