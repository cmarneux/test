<?php
/**
 * searchresult.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Models;
use Wish\MySQL\AbstractDb;
use Wish\MySQL\Query;

/**
 * Class SearchResult
 * @package Wish\MySQL
 */
class SearchResult{
	/**
	 * total rows matching the query
	 * @var int
	 */
	public $total;

	/**
	 * @var array result rows
	 */
	public $rows = [];

	/**
	 * @var
	 */
	public $solrSearch;

	/**
	 * SearchResult constructor.
	 * @param array $rows
	 * @param int $total
	 */
	public function __construct($rows = [], $total=null)
	{
		$this->rows = $rows;
		$this->total = is_null($total)?count($rows):$total;
	}

	/**
	 * searchResult factory. Build a result from a given query and database
	 * @param Query $query
	 * @param AbstractDb $db
	 * @return SearchResult
	 * @throws \Exception
	 */
	public static function performSearch(Query $query, AbstractDb $db){
		$count = $db->exec($query->getCountQuery());
		$rows = $db->exec($query);
		return new self($rows, $count);
	}

	/**
	 * @param $name
	 * @return mixed
	 * @throws \Exception
	 */
	public function __get($name) {
		if(isset($this->$name)) return $this->$name;
		throw new \Exception('Undefined property '.$name.' !');
	}

	/**
	 * @param $name
	 * @param $value
	 */
	public function __set($name, $value) {
		$this->$name = $value;
	}

	/*
	 *  methodes proxies
	 */
	/**
	 * call filterFields on each rows
	 * @see Model::filterFields()
	 * @param array $fields
	 */
	public function filterFields($fields){
		foreach($this->rows as $row){
			/** @var Model $row */
			$row->filterFields($fields);
		}
	}

	/**
	 * call removeFields on each Rows
	 * @see Model::removeFields()
	 * @param array $fields
	 */
	public function removeFields($fields){
		foreach($this->rows as $row){
			/** @var Model $row */
			$row->removeFields($fields);
		}
	}
}
