<?php
/**
 * request.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish\MySQL;

use Registry;
use Wish\Mapper;
use Wish\Models\Model;
use Wish\Tools;

/**
 * Class MySQL
 * @package Wish\MySQL
 */
abstract class AbstractDb extends \Magic {
	/**
	 * F3 Instance
	 * @var \Base
	 */
	protected $f3 = null;

	/**
	 * Db Instance
	 * @var DbLink
	 */
	protected $db = null;

	/**
	 * internal class name
	 * @var string
	 */
	protected $classKeyName;

	/**
	 * Indique si toutes les données de l'objet sont accessibles ou dépendent des droits de l'utilisateur.
	 *
	 * - `true` : Toutes les données sont toujours accessibles
	 * - `false` : L'accès aux données dépend des droits de l'utilisateur
	 * @deprecated L'acces de ne devrait pas etre verifié dans les objets sql mais au niveaux du controlleur. Si les données doivent etre "reduites" avec l'acces, on recupere les donnees normalement et on les filtres dans le controlleur
	 * @var boolean
	 */
	public $always_access = false;

	/**
	 * Constructeur.
	 * @param DbLink $db Variable [\DB\SQL](http://fatfreeframework.com/sql)
	 */
	public function __construct($db) {
		$this->f3 = \Base::instance();
		$this->classKeyName = str_replace(array('boondmanager\\models','\\'), array('','.'), strtolower(get_class($this)));
		$this->db = $db;
	}

	/**
	 *	Return class instance
	 *	@return static
	 **/
	static function instance() {
		if (!Registry::exists($class=get_called_class())) {
			$ref=new \Reflectionclass($class);
			$args=func_get_args();
			Registry::set($class,
				$args?$ref->newinstanceargs($args):new $class);
		}
		return Registry::get($class);
	}

	/**
	 * @param $cmds
	 * @param null $args
	 * @param int $ttl
	 * @param bool $log
	 * @return array|FALSE|int
	 */
	private function _exec($cmds,$args=NULL,$ttl=0,$log=TRUE) {
		return $this->db->exec($cmds, $args, $ttl, $log);
	}

	/**
	 * escape a variable to be integrated in a mysql query
	 * @param mixed $var if an array, it will be escaped recursively
	 * @return mixed the escaped variable
	 */
	public function escape($var) {
		return is_array($var) ? array_map([$this, 'escape'], $var): $this->db->quote($var);
	}

	/**
	 * execute a SQL query
	 * @param string|array|Query $request Query(s) to execute
	 * @param string|array $args Arguments for the query
	 * @param string $modelClass class' name if the rows should be instantiate to an object (must inherit of RowObject)
	 * @param Mapper $mapper
	 * @return \Wish\Models\Model[]|false|int query result
	 * @throws \Exception
	 * @link http://fatfreeframework.com/sql#exec
	 */
	public function exec($request, $args = NULL, $modelClass = null, Mapper $mapper = null) {
		if(!$this->db){
			throw new \Exception('pas de base disponible');
		}
		if($request instanceof Query){
			$result = $this->_exec($request->getQuery(), $request->getArgs());
			if($request->getMapper() && !$mapper) {
				$mapper = $request->getMapper();
			}else if($request->getModelClass() && !$modelClass){
				$modelClass = $request->getModelClass();
			}
		}else {
			$result = $this->_exec($request, $args);
		}

		if(!$mapper){
			if(!$modelClass) $modelClass = Model::class;
			$mapper = new Mapper($modelClass);
		}

		$result = $mapper->mapArray($result);

		return $result;
	}

	/**
	 * Execute a query to retrieve a single row
	 * @param string|Query $request
	 * @param string|array|null $args Arguments for the query
	 * @param string|null $modelClass a class name to instanciate the model
	 * @param Mapper|NULL $mapper
	 * @link http://fatfreeframework.com/sql#exec
	 * @return bool|\Wish\Models\Model
	 */
	public function singleExec($request, $args = NULL, $modelClass = NULL, Mapper $mapper = NULL) {
		$result = $this->exec($request, $args, $modelClass, $mapper);
		if($result) return $result[0];
		else return false;
	}

	/**
	 * Update an object in the database
	 *
	 * @param string $table Table name to update
	 * Example : `$table = 'TAB_PROFIL';`
	 *
	 * @param array $data array of data to update:
	 *
	 * - Keys should match the column name
	 * - Values are the new values
	 *
	 * Example : `$data = ['PROFIL_NOM' => 'Hugo', 'PROFIL_PRENOM' => 'Victor'];`
	 *
	 * @param string $where add a where clause to the query
	 * @param array $args Arguments for the query
	 *
	 * The array should have all keys prefixed by ':' and match a string used in the where clause
	 * Example :
	 *
	 *     $where = 'ID_PROFIL=:idprofil';
	 *     $args = [':idprofil' => 100];
	 *
	 * @return int Number of rows updated
	 * @throws \Exception
	 * @link http://fatfreeframework.com/sql#exec
	 */
	public function update($table, $data, $where = '', $args = NULL) {
		if(is_array($data) && sizeof($data) > 0) {
			$data = $this->filterFields($table, $data);
			$tabKey = array();
			$tabValue = array();
			foreach($data as $key => $value) {
				$var = ':'.strtolower($key);
				$tabKey[] = $key.'='.$var;
				$tabValue[$var] = $this->prepareValue($value);
			}
			if($args){
				if(!is_array($args)) $args = [1=>$args];
				$tabValue += $args;
			}

			if($tabKey)
				return $this->_exec('UPDATE '.$table.' SET '.implode(',',$tabKey).(($where != '')?' WHERE '.$where:''), $tabValue);
		}
		return 0;
	}

	/**
	 * Insert a single row in a table
	 * @param string $table table's name
	 *
	 * Example : `$table = 'TAB_PROFIL';`
	 *
	 * @param array $data data to insert :
	 *
	 * - Keys should match the column name
	 * - Values are the new values
	 *
	 * Example : `$data = ['PROFIL_NOM' => 'Hugo', 'PROFIL_PRENOM' => 'Victor'];`
	 *
	 * @link http://fatfreeframework.com/sql#exec
	 * @return int the new ID
	 */
	public function insert($table, array $data) {
		if(!$data) return null;

		$data = $this->filterFields($table, $data);

		$defaultData = $this->guessDefaultValue($table);
		foreach($defaultData as $field => $default)
			if(!isset($data[$field])) $data[$field] = $default;

		$tabKey = array();
		$tabValue = array();
		$nbValue = 1;
		foreach($data as $key => $value) {
			$tabKey[] = $key;
			$tabValue[$nbValue++] = $this->prepareValue($value);
		}

		$this->_exec('INSERT INTO '.$table.' ('.implode(',',$tabKey).') VALUES ('. Where::prepareWhereIN($tabValue) .')', $tabValue);
		return $this->getLastInsertId();
	}

	/**
	 * prepare a value for update / insert
	 * @param mixed $value
	 * @return string|int|boolean
	 */
	private function prepareValue($value){
		if(is_array($value)) $value = Tools::serializeArray($value);
		if(in_array(strtolower($value), array('null', 'now()'))) $value = strtoupper($value);
		return $value;
	}

	/**
	 * Return the last id build from an insert
	 * @link http://php.net/manual/fr/pdo.lastinsertid.php
	 * @return int
	 */
	public function getLastInsertId(){
		return $this->db->lastInsertId();
	}

	/**
	 * Delete a row in a table
	 *
	 * @param string $table table's name
	 *
	 * Example : `$table = 'TAB_PROFIL';`
	 *
	 * @param string $where add a where clause
	 * @param array $args Arguments for the query
	 *
	 * The array should have all keys prefixed by ':' and match a string used in the where clause
	 * Example :
	 *
	 *     $where = 'ID_PROFIL=:idprofil';
	 *     $args = [':idprofil' => 100];
	 *
	 * @link http://fatfreeframework.com/sql#exec
	 * @return integer number of rows deleted
	 */
	public function delete($table, $where = '', $args = NULL) {
		return $this->_exec('DELETE FROM '.$table.(($where != '')?' WHERE '.$where:''), $args);
	}

	/**
	 * Create/Update a value from a db-row
	 * @param string $key path to save the data
	 * @param mixed $value the value to save
	 * @link http://fatfreeframework.com/base#set
	 * @return void
	 */
	public function set($key, $value) {
		\Base::instance()->set('BMDATA'.$this->classKeyName.(($key != '')?'.':'').$key, $value);
	}

	/**
	 * Load a saved data from a given path
	 * @param string $key
	 * @link http://fatfreeframework.com/base#get
	 * @return mixed
	 */
	public function &get($key) {
		return \Base::instance()->get('BMDATA'.$this->classKeyName.(($key != '')?'.':'').$key);
	}

	/**
	 * Check if a data exists for a given key
	 * @param string $key
	 * @return boolean
	 * @link http://fatfreeframework.com/base#exists
	 */
	public function exists($key) {
		return \Base::instance()->exists('BMDATA'.$this->classKeyName.(($key != '')?'.':'').$key);
	}

	/**
	 * remove a data saved with a given key
	 * @param string $key
	 * @link http://fatfreeframework.com/base#clear
	 * @return NULL|void
	 */
	public function clear($key) {
		\Base::instance()->clear('BMDATA'.$this->classKeyName.(($key != '')?'.':'').$key);
	}

	/**
	 * @param $table
	 * @return array
	 */
	public function guessDefaultValue($table){
		$schema = $this->getSchema($table);
		$default = [];
		foreach($schema as $field=>$data){
			// creation du tableau uniquement pour les types "string" (inclus les dates) et qui n'ont pas de valeurs par default
			if( $data['pdo_type'] === \PDO::PARAM_STR && !$data['nullable']){
				if($data['type'] == 'date') $value = date('Y-m-d');
				elseif($data['type'] == 'datetime') $value = date('Y-m-d H:i:s');
				else if($data['default'] !== null) $value = $data['default'];
				else $value = '';
				$default[$field] = $value;
			}
		}
		return $default;
	}

	/**
	 * @param $table
	 * @return array|FALSE
	 */
	public function getSchema($table){
		return $this->db->schema($table, NULL, 3600);
	}

	/**
	 * @param $table
	 * @param $data
	 * @return array
	 */
	public function filterFields($table, $data){
		$schema = $this->getSchema($table);
		return array_intersect_key($data, $schema);
	}

	/**
	 * perform an implode on the array and wrap it with parenthesis if there is more than 1 element
	 * @param array  $tabArray
	 * @param string $operator Glue field
	 * @return  string
	 *
	 * @deprecated
	 *
	 * Example :
	 *
	 *     echo addOperatorValues(array(3,4,8));
	 *     (3 OR 4 OR 8)
	 */
	public function addOperatorValues($tabArray = array(), $operator = ' OR ') {return (sizeof($tabArray) > 1)?'('.implode($operator, $tabArray).')':$tabArray[0];}

	/**
	 * parse the keywords and add a where clauses if a keyword has a known prefix
	 *
	 * @param array|string $keywords if it is a string, it will be transform to an array by performing an explode on spaces
	 *
	 * @param  array $liste_ref array containing the mapping PREFIX=>DATABASE_FIELD
	 *
	 * Example : `$liste_ref = ["COMP" => "TAB_PROFIL.ID_PROFIL"];`
	 * @return Where
	 */
	public function getListIdSearch($keywords, $liste_ref = array()) {
		$where = new Where();
		if($keywords) {
			$items = (is_array($keywords))?$keywords:explode(' ', $keywords);//On sépare la variable mot clefs de chaque espace
			$result = array();
			foreach($items as $item) {
				foreach($liste_ref as $ref => $id_table) {
					if(preg_match("#^".$ref."([0-9]{1,})$#",strtoupper($item), $id_reference)) {
						$result[$ref]['tab'] = $id_table;
						$result[$ref]['key'][] = '?';
						$result[$ref]['id'][] = $id_reference[1];
						break;
					}
				}
			}

			if(sizeof($result) > 0) {
				foreach($result as $ref) $where->and_(new Where($ref['tab'].' IN (?)', $ref['id']));
			}
		}
		return $where;
	}

	/**
	 * Build a where clause in the query for the given values and filters
	 *
	 * @param  mixed $data (string, array, int, ...) field value
	 * @param  string $colBDD field name in the database
	 * @param  array $exceptionArray ignore some values
	 * @param  boolean $regexp false if the value should be an exact match, otherwise, the field should start with the value
	 * @param int|null $indifferent
	 * @return Where
	 */
	public function getFilterSearch($data, $colBDD, $exceptionArray = array(), $regexp = false, $indifferent = null){
		if($data && (is_array($data) && !in_array($indifferent, $data) || $data != $indifferent && !in_array($data, $exceptionArray))){
			if (is_array($data)) {
				$data = array_diff($data,$exceptionArray);
				if ($regexp) {
					$tabRegExp = array();
					foreach ($data as $mcItem)
						$tabRegExp[] = '[[:<:]]' . $mcItem . '[[:>:]]';
					return new Where($colBDD . ' REGEXP ?', implode('|', $tabRegExp));
				} else return new Where($colBDD . ' IN (?)', $data);
			} else {
				return ($regexp)
					? new Where($colBDD . ' LIKE ?', $data . '%')
					: new Where($colBDD . ' = ?', $data);
			}
		}
		return new Where();
	}
}
