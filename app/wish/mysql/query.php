<?php
/**
 * query.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\MySQL;

use Wish\Mapper;

/**
 * Class Query
 * @package Wish\MySQL
 */
class Query{
	/**
	 * where clauses and their args (each entry is an array with 2 items : [0=>WHERE_CLAUSE, 1=>ARGS])
	 * @var Where
	 */
	private $where;

	/**
	 * @var string columns used to group by
	 */
	private $groupBy;

	/**
	 * @var string from main table used for the query
	 */
	private $from;
	/**
	 * @var array contains all join to perform ex: ["INNER JOIN table2 t USING (id)"]
	 */
	private $joins = [];

	/**
	 * @var array list of sorting columns
	 */
	private $order = [];

	/**
	 * @var array columns in the select
	 */
	private $columns = [];

	/**
	 * @var string the limit clause
	 */
	private $limit;

	/**
	 * @var int start offset
	 */
	private $offsetStart = 0;

	/**
	 * @var int rows to retrieve
	 * @TODO recuperer la valeur de conf de BM
	 */
	private $rows = 30;

	/**
	 * a class name for result mapping
	 * @var string
	 */
	private $modelClass = null;

	/**
	 * a mapper to create each rows
	 * @var Mapper
	 */
	private $mapper = null;

	/**
	 * set a field for a count
	 * @var string
	 */
	private $countColumn = null;

	/**
	 * @var array of additional columns for count
	 */
	private $countColumns = [];

	/**
	 * Set the column(s) used to build the group by
	 * @param array|string $cols
	 * @return $this
	 */
	public function groupBy($cols){
		if(is_array($cols)) $cols = implode(', ',$cols);
		$this->groupBy = $cols;
		return $this;
	}

	/**
	 * @param string $column
	 * @return $this
	 */
	public function setCountColumn($column){
		$this->countColumn = $column;
		return $this;
	}

	/**
	 * Set the column(s) used to build the group by
	 * @param array|string $cols
	 * @return $this
	 */
	public function resetGroupBy(){
		$this->groupBy = null;
		return $this;
	}

	/**
	 * FROM part of the request
	 * @param string $from
	 * @return $this
	 */
	public function from($from){
		$this->from = $from;
		return $this;
	}

	/**
	 * add a join
	 * @param string $join
	 * @return $this
	 */
	public function addJoin($join){
		$this->joins[] = $join;
		return $this;
	}

	/**
	 * add a field used to order by
	 * @param string $field column name for the sort (can include the suffix ASC|DESC)
	 * example: $query->addOrderBy('date DESC');
	 * @return $this
	 */
	public function addOrderBy($field){
		$this->order[] = $field;
		return $this;
	}

	/**
	 * add a column to the select
	 * @param array $data
	 * @return $this
	 */
	public function setColumns(array $data){
		$this->columns = $data;
		return $this;
	}

	/**
	 * Delete columns
	 * @return $this
	 */
	public function resetColumns(){
		$this->columns = [];
		return $this;
	}

	/**
	 * replace columns of the select
	 *
	 * @param string|array $cols if the input is an array and has a string key, it will be used as an alias
	 * @return $this
	 */
	public function replaceColumns($cols){
		$this->resetColumns();
		$this->addColumns($cols);
		return $this;
	}

	/**
	 * add columns to the select
	 * ex:
	 *  addColumns(['colA', 'colB']);
	 *  addColumns('colA');
	 *  addColumns(['aliasA'=>'colA', 'colB']);
	 *
	 * @param string|array $cols if the input is an array and has a string key, it will be used as an alias
	 * @return $this
	 */
	public function addColumns($cols){
		if(!is_array($cols)) $cols = [$cols];
		foreach($cols as $key=>$v){
			if(is_string($key))
				$this->columns[$key] = $v;
			else
				$this->columns[] = $v;
		}
		return $this;
	}

	/**
	 * add columns to the count
	 * ex:
	 *  addColumns(['colA', 'colB']);
	 *  addColumns('colA');
	 *  addColumns(['aliasA'=>'colA', 'colB']);
	 * @param $cols
	 * @return $this
	 * @throws \Exception
	 */
	public function addCountColumns($cols) {
		if(!is_array($cols)) $cols = [$cols];
		foreach($cols as $key=>$v){
			if(is_string($key))
				$this->countColumns[$key] = $v;
			else
				$this->countColumns[] = $v;
		}
		return $this;
	}

	/**
	 * Delete columns to the count
	 * @return $this
	 */
	public function resetCountColumns(){
		$this->countColumns = [];
		return $this;
	}

	/**
	 * alias of addColumns
	 * @see self::addColumns()
	 * @param mixed $cols columns to add
	 * @return $this
	 */
	public function select($cols = '*'){
		return $this->addColumns($cols);
	}

	/**
	 * add a where filter
	 * ex:
	 *  addWhere('fieldA = ?', 'test');
	 *  addWhere('fieldA = ?', ['test']);
	 *  addWhere('fieldA = ? AND fieldB = ?', ['testA', 'testB]);
	 *  addWhere('field IN (?)', array $args); <=> field IN (?, ?, ?, ..), $args)
	 * @param Where|string $where filtre where
	 * @param mixed $args a single value or an array
	 * @return $this
	 */
	public function addWhere($where, $args=[]){
		if(is_string($where)){
			$where = new Where($where, $args);
		}

		if($where && !$where->isEmpty()) {
			$where = clone $where;
			if (!$this->where) $this->where = $where;
			else $this->where->and_($where);
		}

		return $this;
	}

	/**
	* Build the query to count all matching results
	* @param string $fieldName
	* @return string
	*/
	public function getCountQuery($fieldName = 'NB_ROWS'){
		$count = $this->countColumn?
			$this->countColumn:
			($this->groupBy?'DISTINCT '.$this->groupBy:'*');

		$countFields = $this->getCountFields();
		return 'SELECT COUNT('. $count . ') AS '.$fieldName.($countFields != '' ? ', '.$countFields : '').' FROM '.$this->from.' '.$this->buildJoins().' '.$this->buildWhere();
	}

	/**
	 * build the query string
	 * @return string
	 */
	public function __toString(){
	   return $this->getQuery();
	}

	/**
	 * Build the query string
	 * @return string
	 */
	public function getQuery(){
		return 'SELECT '. $this->getSelectFields().' FROM '.$this->from.' '. $this->buildJoins().' '.$this->buildWhere(). $this->buildGroupBy() . $this->buildOrderBy() . $this->limit;
	}

	/**
	 * Build the join string
	 * @return string
	 */
	public function buildJoins(){
		return implode(' ',$this->joins);
	}

	/**
	 * build the select string (all columns)
	 * @return string
	 */
	private function getSelectFields(){
		$fields = [];
		foreach($this->columns as $alias=>$column){
			if(is_string($alias)) $fields[] = $column.' AS '.$alias;
			else $fields[] = $column;
		}
		return implode(', ',$fields);
	}

	/**
	 * build the count string (all additional columns)
	 * @return string
	 */
	private function getCountFields(){
		$fields = [];
		foreach($this->countColumns as $alias=>$column)
			if(is_string($alias)) $fields[] = $column.' AS '.$alias;
			else $fields[] = $column;
		return implode(', ',$fields);
	}

	/**
	 * build the where string (and prepare all args)
	 * @return string
	 */
	public function buildWhere(){
		return ($this->where && !$this->where->isEmpty())?' WHERE '.$this->where->getWhere():'';
	}

	/**
	 * build the group by string
	 * @return string
	 */
	public function buildGroupBy(){
		return ($this->groupBy)?' GROUP BY '.$this->groupBy:'';
	}

	/**
	 * build the order by string
	 * @return string
	 */
	public function buildOrderBy(){
		return $this->order?' ORDER BY '.implode(', ', $this->order):'';
	}

	/**
	 * add a LIMIT to the query
	 * @param int $rows numbers of rows
	 * @param int $start start offset
	 * @return $this
	 */
	public function setLimit($rows, $start = 0){
		$this->offsetStart = $start;
		$this->rows = $rows;
		$this->limit = ($start)?" LIMIT $start,$rows":" LIMIT $rows";
		return $this;
	}

	/**
	 * get all query args (for the where clauses)
	 * @return array|null
	 */
	public function getArgs(){
		return ($this->where)?$this->where->getArgs():[];
	}

	/**
	 * a class name to map each row of the query result
	 * @param string $class
	 * @param \Wish\Mapper $mapper
	 * @return $this
	 */
	public function setModelClass($class, Mapper $mapper = null){
		$this->modelClass = $class;
		if($mapper) $this->setMapper($mapper);
		return $this;
	}

	/**
	 * return a class name used for mapping each results
	 * @return string
	 */
	public function getModelClass(){
		return $this->modelClass;
	}

	/**
	 * @return \Wish\Mapper
	 */
	public function getMapper(){
		return $this->mapper;
	}

	/**
	 * @param \Wish\Mapper $mapper
	 * @return $this
	 */
	public function setMapper(Mapper $mapper){
		$this->mapper = $mapper;
		return $this;
	}

	/**
	 * clear the where part
	 * @return $this
	 */
	public function resetWhere(){
		$this->where = null;
		return $this;
	}

	/**
	 * get the where data
	 * @return Where
	 */
	public function getWhere(){
		return $this->where;
	}

	/**
	 * return an array with all orderBy clauses
	 * @return array
	 */
	public function getOrder(){
		return $this->order;
	}

	/**
	 * supprime le order by
	 * @return $this
	 */
	public function resetOrder(){
		$this->order = [];
		return $this;
	}

	/**
	 * return an array with all columns
	 * @return array
	 */
	public function getColumns(){
		return $this->columns;
	}

	/**
	 * return a string with the table used in the from clause
	 * @return string
	 */
	public function getFrom(){
		return $this->from;
	}

	/**
	 * return an array with all tables used with joins
	 * @return array
	 */
	public function getJoins(){
		return $this->joins;
	}

	/**
	 * start offset
	 * @return int
	 */
	public function getOffsetStart(){
		return $this->offsetStart;
	}

	/**
	 * row's limit
	 * @return int
	 */
	public function getRowsLimit(){
		return $this->rows;
	}

	/**
	 * column used for the group by
	 * @return mixed
	 */
	public function getPrimaryKey(){
		return $this->groupBy;
	}

	/**
	 * calculate the offset given the page and the rows per pages
	 * @param int $page
	 * @param int $rows
	 * @return int
	 */
	public static function getOffset($page, $rows){
		return ($page-1)*$rows;
	}
}
