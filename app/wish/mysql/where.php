<?php
/**
 * where.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\MySQL;

/**
 * Class Where
 * @package Wish\MySQL
 */
class Where{
    /**
     * @var string a where clause
     */
    private $where;

    /**
     * @var array args for the where clause (if needed)
     */
    private $args;

    /**
     * Where constructor.
     * @param string $where
     * @param array $args
     */
    public function __construct($where = '', $args = []){

        $this->setArgs($args);

        $where = strval($where);
        // petite aide si la clause est IN(?)
        if($where && count($this->args)>0 && preg_match_all('/\?/',$where)!=count($this->args)) {
            if(!$this->args) $where = '';
            else $where = preg_replace('/\s+IN\s*\(\s*\?\s*\)/im', ' IN (' . self::prepareWhereIN($this->args) . ')', $where);
        }

        $this->where = $where;
    }

    /**
     * set the arg(s) to the where clause
     * @param mixed $args string, int if a single arg or an array of args
     * @return $this
     */
    public function setArgs($args){
        if(!is_array($args)) $args = [1=>$args];
        if(isset($args[0])) $args = self::reindexArgs($args);
        $this->args = $args;
        return $this;
    }

    /**
     * add more args to the current Where clause
     * @param array $args
     * @return Where
     */
    private function addArgs($args){
        $currentArgs = ($this->args)?$this->args:[];
        return $this->setArgs( array_merge($currentArgs, $args));
    }

    /**
     * combine the where clause with another one using an `OR`
     * @param Where|string $where
     * @param array $args
     * @return $this
     */
    public function or_($where, $args = []){
        if(! ($where instanceof Where)) $where = new Where($where, $args);
        if($where->isEmpty()) return $this;
        if($this->where){
            $this->where = "{$this->where} OR ({$where->getWhere()})";
        }else{
            $this->where = $where->getWhere();
        }
        $this->addArgs($where->getArgs());
        return $this;
    }

    /**
     * combine the where clause with another one using an `AND`
     * @param Where|string $where
     * @param array $args
     * @return $this
     */
    public function and_ ($where, $args = []){
        if(! ($where instanceof Where)) $where = new Where($where, $args);
        if($where->isEmpty()) return $this;
        if ($this->where) {
            $this->where = "{$this->where} AND ({$where->getWhere()})";
        } else {
            $this->where = $where->getWhere();
        }
        $this->addArgs($where->getArgs());
        return $this;
    }

    /**
     * get the where clause
     * @return string|null
     */
    public function getWhere(){
        return $this->where;
    }

    /**
     * get all args of the where clause
     * @return array|null
     */
    public function getArgs(){
        return $this->args;
    }

    /**
     * check if the where is empty or not
     * @return bool
     */
    public function isEmpty(){
        return !$this->where;
    }

    /**
     * build a string of ?,?,?,... depending of the number of args given
     * @param array $args
     * @return string
     */
    public static function prepareWhereIN($args)
    {
        return implode(',', array_fill(0, count($args), '?'));
    }

    /**
     * build an arguments array for PDO [$argNamePrefix<1> => $args[0], ..., $argNamePrefix<n> => $args[n]] depending of the number of args given
     * @param string $argNamePrefix
     * @param array $args
	 * @param bool $colon
     * @return array
     */
    public static function prepareNamedWhereINArgs($argNamePrefix, $args, $colon = false)
    {
		$prefixed = array_fill(0, count($args), ($colon ? ':' : '') .$argNamePrefix);
		return array_combine(array_map(function ($value, $key){return $value.($key + 1);}, $prefixed, array_keys($prefixed)),$args);
    }

    /**
     * build a string of $argNamePrefix<1>, ..., $argNamePrefix<n> depending of the number of args given
     * @param string $argNamePrefix
     * @param array $args
     * @return string
     */
    public static function prepareNamedWhereIN($argNamePrefix, $args)
    {
		return implode(',', array_keys(self::prepareNamedWhereINArgs($argNamePrefix, $args, $colon = true)));
    }

    /**
     * reindex the given array at 1
     * @param array $args
     * @return array
     */
    public static function reindexArgs($args)
    {
        if ($args) return array_combine(range(1, count($args)), array_values($args));
        else return [];
    }
}
