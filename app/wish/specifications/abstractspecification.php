<?php
/**
 * abstractspecification.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Specifications;

/**
 * Class AbstractSpecification
 * @package Wish\Specifications
 */
abstract class AbstractSpecification implements InterfaceSpecification{
    /**
     * add another specification that can match
     * @param InterfaceSpecification $specification
     * @return OrSpecification
     */
    public function or_(InterfaceSpecification $specification){
        return new OrSpecification($this, $specification);
    }

    /**
     * add a specification that must to match
     * @param InterfaceSpecification $specification
     * @return InterfaceSpecification
     */
    public function and_(InterfaceSpecification $specification)
    {
        return new AndSpecification($this, $specification);
    }

    /**
     * add a specification that must not match
     * @param InterfaceSpecification $specification
     * @return InterfaceSpecification
     */
    public function not(InterfaceSpecification $specification)
    {
        return new NotSpecification($specification);
    }
}
