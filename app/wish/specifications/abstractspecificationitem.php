<?php
/**
 * abstractspecificationitem.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Specifications;

/**
 * Class AbstractSpecificationItem
 * @package Wish\Specifications
 */
abstract class AbstractSpecificationItem extends AbstractSpecification{
    /**
     * check if the object match the specification
     * @param mixed $object
     * @return bool
     */
    public abstract function isSatisfiedBy($object);
}
