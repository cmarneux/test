<?php
/**
 * interfacespecification.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Specifications;

/**
 * Interface InterfaceSpecification
 * @package Wish\Specifications
 */
interface InterfaceSpecification{
    /**
     * check if the object match the specification
     * @param mixed $object
     * @return bool
     */
    public function isSatisfiedBy($object);

    /**
     * add another specification that can match
     * @param InterfaceSpecification $specification
     * @return InterfaceSpecification
     */
    public function or_(InterfaceSpecification $specification);

    /**
     * add a specification that must to match
     * @param InterfaceSpecification $specification
     * @return InterfaceSpecification
     */
    public function and_(InterfaceSpecification $specification);

    /**
     * add a specification that must not match
     * @param InterfaceSpecification $specification
     * @return InterfaceSpecification
     */
    public function not(InterfaceSpecification $specification);
}
