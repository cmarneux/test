<?php
/**
 * orspecification.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Specifications;

/**
 * Class NotSpecification
 * Create a Specification by inverting the value of a specification
 * @package Wish\Specifications
 */
class NotSpecification extends AbstractSpecification{
    /**
     * @var AbstractSpecification the specification used
     */
    private $spec;

    /**
     * NotSpecification constructor.
     * @param InterfaceSpecification $spec
     */
    public function __construct(InterfaceSpecification $spec){
        $this->spec = $spec;
    }

    /**
     * check if the object match the specification
     * @param mixed $object
     * @return bool
     */
    public function isSatisfiedBy($object){
        return !$this->spec->isSatisfiedBy($object);
    }
}
