<?php
/**
 * orspecification.php
 * @author Louis D'Arras <darras.louis@gmail.com>
 */
namespace Wish\Specifications;

/**
 * Class OrSpecification
 * Create a Specification by combining two specifications with an OR
 * @package Wish\Specifications
 */
class OrSpecification extends AbstractSpecification{
    /**
     * @var AbstractSpecification first specification
     */
    private $spec1;

    /**
     * @var AbstractSpecification second specification
     */
    private $spec2;

    /**
     * OrSpecification constructor.
     * @param InterfaceSpecification $spec1
     * @param InterfaceSpecification $spec2
     */
    public function __construct(InterfaceSpecification $spec1, InterfaceSpecification $spec2){
        $this->spec1 = $spec1;
        $this->spec2 = $spec2;
    }

    /**
     * check if the object match the specification
     * @param mixed $object
     * @return bool
     */
    public function isSatisfiedBy($object){
        return $this->spec1->isSatisfiedBy($object) || $this->spec2->isSatisfiedBy($object);
    }
}
