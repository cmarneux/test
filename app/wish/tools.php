<?php
/**
 * tools.php
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
namespace Wish;

use Wish\Specifications\AbstractSpecificationItem;
use Wish\ClockWork\Context;
use Wish\ClockWork\DataSource;
use Clockwork\Clockwork;
use Clockwork\DataSource\PhpDataSource;
use Clockwork\Storage\FileStorage;

/**
 * Singleton contenant les méthodes globales de BoondManager.
 * @namespace \Wish
 */
class Tools {
	/**#@+
	 * Value used for defined a maximum date
	 * @var string
	 */
	const MAX_DATE = '3000-01-01';

	/**
	 * @var Clockwork current clowkwork instance
	 */
	private static $clockworkInstance;

	/**
	 * @var Context current context instance
	 */
	private static $contextInstance;

	/**
	 * @var \Log
	 */
	private static $loggerInstance = null;

	/**#@+
	 * Debug Mode
	 * @var int
	 */
	const
		DEBUG_OFF = 0,
		DEBUG_ON = 1;
	/**#@-*/

	/**
	 * @var int
	 */
	private static $debugMode = self::DEBUG_OFF;

	/**
	 * get the current clockwork instance
	 * @return Clockwork|null
	 */
	public static function getClockwork()
	{
		return self::$clockworkInstance;
	}

	/**
	 * get the current context instance
	 * @return Context|null
	 */
	public static function getContext()
	{
		if (!self::$contextInstance) self::$contextInstance = new Context();
		return self::$contextInstance;
	}

	/**
	 * init Clockwork to store send data to the client
	 */
	public static function initClockwork()
	{
		$f3 = \Base::instance();
		$CW = new Clockwork();
		self::$clockworkInstance = $CW;

		header('X-Clockwork-Id: '. $CW->getRequest()->id);
		header('X-Clockwork-Version: '. Clockwork::VERSION);
		header('X-Clockwork-Path: /ui.boondmanager/api/clockwork/');

		$CW->addDataSource( new DataSource( self::getContext() ));
		$CW->addDataSource( new PhpDataSource() );
		$CW->setStorage( new FileStorage($f3->get('TEMP')) );

		register_shutdown_function(function() use ($CW){
			$CW->resolveRequest();
			$CW->storeRequest();
		});
	}

	/**
	 * @param int $mode
	 */
	public static function setDebugMode($mode) {
		self::$debugMode = in_array($mode, [self::DEBUG_OFF, self::DEBUG_ON]) ? $mode : self::DEBUG_OFF;
	}

	/**
	 * @return int
	 */
	public static function getDebugMode() {
		return self::$debugMode;
	}

	/**
	 * @param $msg
	 */
	public static function logError($msg)
	{
		if (!self::$loggerInstance) self::$loggerInstance = new \Log('f3.error.log');
		$logger = self::$loggerInstance;

		$backtrace = debug_backtrace();
		$lastTrace = $backtrace[count($backtrace) - 2];

		$date = date('d/m/y - H:i:s');

		$loggedMsg = "$date - {$_SERVER['REMOTE_ADDR']} - {$_SERVER['REQUEST_URI']}\n";
		$loggedMsg .= "$date - in {$lastTrace['function']} at {$lastTrace['file']}:{$lastTrace['line']}\n";
		$loggedMsg .= "$date - $msg\n";

		$logger->write($loggedMsg);
	}

	/**
	 * transform an array of object to a simple array by extracting a given field's value from each object
	 * @param array $array an array of object
	 * @param string $field the field to extract from each object
	 * @param mixed|null $keyField
	 * @return array
	 */
	public static function getFieldsToArray($array, $field, $keyField = null){
		$output = [];
		if(self::isIterable($array)) foreach($array as $item) {
			if($keyField) $output[$item->$keyField] = $item->$field;
			else $output[] = $item->$field;
		}
		return $output;
	}

	/**
	 * test if value can be used in foreach
	 * @param $value
	 * @return boolean
	 */
	public static function isIterable($value){
		return is_array($value) || $value instanceof \Iterator;
	}

	/**
	 * test if $value act like an array
	 * @param $value
	 * @return bool
	 */
	public static function isArray($value){
		return is_array($value) || $value instanceof \Iterator && $value instanceof \ArrayAccess;
	}

	/**
	 * search a value in a 2 dimensionnal array
	 * @param mixed $needle searched value
	 * @param array $array 2 dim array
	 * @param mixed $subKey if a key is given, the value's key must match
	 * @return bool
	 */
	public static function in_2dimArray($needle, $array, $subKey = null){
		foreach($array as $a){
			if(isset($subKey)){
				if($a[$subKey] == $needle) return true;
			}else if(in_array($needle, $a)) return true;
		}
		return false;
	}

	/**
	 * Teste si 2 tableaux sont identiques.
	 *
	 * @param array $array1 Tableau 1.
	 * @param array $array2 Tableau 2.
	 * @return boolean `true` si égal, `false` sinon.
	 */
	public static function is_equal_array($array1 = array(), $array2 = array()) {
		if(sizeof($array1) != sizeof($array2)) return false;

		sort($array1);
		sort($array2);

		while($array1){
			if(array_shift($array1) != array_shift($array2)) return false;
		}

		return true;
	}

	/**
	 * Filtre la requête d'une URL en supprimant les paramètres interdits.
	 *
	 * @param string $symFirstParam Premier caractère de la requête (? ou &)
	 * @param array $tabExceptions Tableau des paramètres interdits
	 * @param string $url URL à filtrer
	 * @return string Requête de l'URL filtrée
	 *
	 * @note Si `$url` == '' alors l'URL filtrée est $_SERVER['QUERY_STRING']
	 */
	public static function getFiltersMoreQueryString($symFirstParam = '?', $tabExceptions = array(), $url = '') {
		$f3 = \Base::instance();
	    $tabOut = null;
		parse_str( $url? $url : $f3->get('SERVER.QUERY_STRING'), $tabOut);
	    $filtersMore = array();
	    if(is_array($tabOut))
	    	foreach($tabOut as $param => $value)
	    		if(!in_array($param, $tabExceptions)) {
	    			if(is_array($value)) {
	    				foreach($value as $pValue) $filtersMore[] = $param.'%5B%5D='.$pValue;
	    			} else
	    				$filtersMore[] = $param.'='.$value;
	    		}
	    if(sizeof($filtersMore) > 0) return $symFirstParam.implode('&',$filtersMore); else return '';
	}

	/**
	 * Convert a date from (Y-m-d or d/m/Y) to d/m/Y
	 * @param string $date
	 * @return string
	 */
	public static function convertDateForUI($date){
		if(preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/', $date, $match)) {
			$newdate = explode('-', $date);
			return date('d/m/Y',mktime(0,0,0,$newdate[1],$newdate[2],$newdate[0]));
		}
		return $date;
	}

	/**
	 * Transforme une date/heure.
	 *
	 * @param string $date Date à transformer
	 * @return string Date transformée YYYY-MM-DD HH:MM:SS --> DD/MM/YYYY HH:MM
	 */
	public static function convertDateTimeForUI($date) {
	    if(preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/', $date, $match)) {
	        $datetime = explode(' ', $date);
	        $newdate = explode('-', $datetime[0]);
	        $newtime = explode(':', $datetime[1]);
			return date('d/m/Y H:i',mktime($newtime[0],$newtime[1],0,$newdate[1],$newdate[2],$newdate[0]));
	    } else return $date;
	}

	/**
	 * Transforme une chaîne de caractères en tableau de valeurs & inversement.
	 *
	 * @param string|array $items Chaîne de caractères ou Tableau à transformer
	 * @param boolean $etat Sens de la transformation :
	 * - `true` : Chaîne de caractères --> Tableau
	 * - `false` : Tableau --> Chaîne de caractères
	 * @param string $chaine chaîne de caractères préfixant & postfixant chaque valeur du tableau _, uniquement si *$etat* = **true**_
	 * @return string|array Chaîne de caractères ou Tableau transformé
	 */
	private static function convertArray($items, $etat = true, $chaine = '') {
	    if($etat) {
	    	$tabitem = ($items != '')?explode('|', $items):array();
	        if($chaine != '') {
	            $newitems = array();
	            foreach($tabitem as $item) $newitems[] = $chaine.$item.$chaine;
	        } else
	            $newitems = $tabitem;
	    } else {
	        $newitems = array();
	        foreach($items as $item) $newitems[] = strval($item);
	        $newitems = implode('|', $newitems);
	    }
	    return $newitems;
	}

	/**
	 * Serialize an array to a string with value separated by a |
	 * @param array $items
	 * @return string
	 */
	public static function serializeArray($items){
		return self::convertArray($items, false);
	}

	/**
	 * Unserialize a string serialized with `self::serializeArray()`
	 * @param string $string
	 * @param string $prefix add a prefix for each value
	 * @return array
	 */
	public static function unserializeArray($string, $prefix = ''){
		return self::convertArray($string, true, $prefix);
	}

	/**
	 * Transforme une chaîne de caractères en tableau contenant des tableaux de valeurs & inversement.
	 *
	 * @param string|array[] $dbitems Chaîne de caractères ou Tableau à transformer
	 * @param boolean $etat Sens de la transformation :
	 * - `true` : Chaîne de caractères --> Tableau
	 * - `false` : Tableau --> Chaîne de caractères
	 * @param boolean $index Type de clefs dans le tableau_, uniquement si *$etat* = **true**_ :
	 * - `true` : La clef des tableaux de valeur est présente dans la chaîne de caractères
	 * - `false` : La clef des tableaux de valeur est un entier correspondant à la position du tableau dans la chaîne de caractères
	 * @return string|array Chaîne de caractères ou Tableau transformé
	 */
	public static function convertDoubleArray($dbitems, $etat = true, $index = true) {
	    if($etat) {
	        $tabdbitem = ($dbitems != '')?explode('#', $dbitems):array();
	        $newtab = array();
	        foreach($tabdbitem as $i => $items) {
	            $items = explode('|', $items);
				if($index) $newtab[$items[0]] = isset($items[1]) ? (array_shift($items) && count($items) > 1 ? $items : $items[0]) : ''; else $newtab[$i] = $items;
	        }
	    } else {
	        $tab = array();
	        if($index)
	        	foreach($dbitems as $key => $value) $tab[] = strval($key).((isset($value))?'|'.(is_array($value)?implode('|',$value):strval($value)):'');
			else
				foreach($dbitems as $tabdbitem) $tab[] = implode('|', $tabdbitem);
	        $newtab = implode('#', $tab);
	    }
	    return $newtab;
	}

	/**
	 * Serialize an array to a string with value separated by | and  #
	 * @param array $items
	 * @return string
	 */
	public static function serializeDoubleArray($items){
		return self::convertDoubleArray($items, false, false);
	}

	/**
	 * Unserialize a string serialized with `self::unserializeDoubleArray()`
	 * @param string $string
	 * @return array
	 */
	public static function unserializeDoubleArray($string){
		return self::convertDoubleArray($string, true, false);
	}


	/**
	 * Serialize an array to a string with value separated by | and  #
	 * La clef des tableaux de valeur est présente dans la chaîne de caractères
	 * @param array $items
	 * @return string
	 */
	public static function serializeDoubleArrayWithIndex($items){
		return self::convertDoubleArray($items, false, true);
	}


	/**
	 * Unserialize a string serialized with `self::unserializeDoubleArray()`
	 * La clef des tableaux de valeur est présente dans la chaîne de caractères
	 * @param array $string
	 * @return string
	 */
	public static function unserializeDoubleArrayWithIndex($string){
		return self::convertDoubleArray($string, true, true);
	}

	/**
	 * remove all accents from a string
	 * @param $str string to clean up
	 * @param string $encoding
	 * @return mixed|string
	 */
	public static function suppr_accents($str, $encoding='utf-8'){
		// transformer les caractères accentués en entités HTML
		$str = htmlentities($str, ENT_NOQUOTES, $encoding);

		// remplacer les entités HTML pour avoir juste le premier caractères non accentués
		// Exemple : "&ecute;" => "e", "&Ecute;" => "E", "Ã " => "a" ...
		$str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);

		// Remplacer les ligatures tel que : Œ, Æ ...
		// Exemple "Å“" => "oe"
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);

		// Supprimer tout le reste
		$str = preg_replace('#&[^;]+;#', '', $str);

		return $str;
	}

	/**
	 * Encode une URL en base64.
	 *
	 * @param string $input URL à encoder
	 * @return string Chaîne de caractères encodée
	 */
	public static function base64_url_encode($input) {return strtr(base64_encode($input), '+/', '-_');}

	/**
	 * Décode une chaîne de caractères en base64 en URL.
	 *
	 * @param string $input Chaîne à décoder
	 * @return string URL décodée
	 */
	public static function base64_url_decode($input) {return base64_decode(strtr($input, '-_', '+/'));}

	/**
	 * Encode & crypte un tableau en base64.
	 *
	 * @param array $payload Tableau à encoder & crypter
	 * @param string $key Clef utilisée pour crypter la chaîne de caractères
	 * @param boolean $newAPI
	 * @return string Chaîne de caractères encodée & cryptée
	 */
	public static function signedRequest_encode($payload, $key, $newAPI = true) {
		if(!$newAPI) $payload['issued_at'] = time(); else $payload['issuedAt'] = time();
		$payload = self::base64_url_encode(json_encode($payload));
		return self::base64_url_encode(hash_hmac('SHA256', $payload, $key)).'.'.$payload;
	}

	/**
	 * Décrypte & décode une chaîne de caractères en base64 en URL.
	 *
	 * @param array $signed_request Chaîne de caractères à décrypter & décoder
	 * @param string $key Clef utilisée pour décrypter la chaîne de caractères
	 * @return boolean|array
	 * - `false` : Si le décodage est incorrect
	 * - __array__ : Tableau décrypté & décodé_, uniquement si le décodage est correct_
	 */
	public static function signedRequest_decode($signed_request, $key) {
		$tabRequest = explode('.', $signed_request, 2); //0 = encoded_signature AND 1 = payload
		return (sizeof($tabRequest) == 2 && self::base64_url_decode($tabRequest[0]) == hash_hmac('sha256', $tabRequest[1], $key))
					?json_decode(self::base64_url_decode($tabRequest[1]), true)
					:false;
	}
	/**
	 * Encode un tableau en chaîne de caractères.
	 *
	 * @param array $tabParam Tableau à encoder
	 * @return string Chaîne de caractères encodée
	 */
	public static function objectID_BM_encode($tabParam = array()) {
	    $retval = '';$object = implode('.',$tabParam);$length = strlen($object);
	    for($idx = 0; $idx < $length; $idx++) $retval .= str_pad(base_convert(ord($object[$idx]), 10, 16), 2, '0', STR_PAD_LEFT);
	    return $retval;
	}

	/**
	 * Décode une chaîne de caractères en tableau.
	 *
	 * @param string $stringParam Chaîne de caractères à décoder
	 * @return array Tableau décodé
	 */
	public static function objectID_BM_decode($stringParam = '') {
	    $retval = '';
	    if(strlen($stringParam) % 2 == 1) $stringParam = '0'.$stringParam;
	    $length = strlen($stringParam);
	    for($idx = 0; $idx < $length; $idx += 2) $retval .= chr(base_convert(substr($stringParam, $idx, 2), 16, 10));
	    return explode('.',$retval);
	}

	/**
	 * Indique le nombre de mensualités entre 2 dates.
	 *
	 * @param string $start Date de début (incluse) au format YYYY-MM-DD
	 * @param string $stop Date de fin (incluse) au format YYYY-MM-DD
	 * @return integer Nombre de mensualités
	 */
	public static function getNumberOfTerms($start, $stop) {
		$nDebut = explode('-', $start);
		$nFin = explode('-', $stop);
		return $nFin[1] - $nDebut[1] + 1 + ($nFin[0] - $nDebut[0])*12;
	}

	/**
	 * Indique le nombre de jours ouvrés entre 2 dates.
	 *
	 * @param string $start Date de début (incluse) au format YYYY-MM-DD
	 * @param string $stop Date de fin (incluse) au format YYYY-MM-DD
	 * @param string $calendrier Calendrier à utiliser
	 * @return integer Nombre de jours ouvrés
	 */
	public static function getNumberOfWorkingDays($start, $stop, $calendrier = 'France') {
	    //On construit le TIMESTAMP de chaque date
	    $date_start = strtotime($start);
	    $date_stop = strtotime($stop);

		$arr_bank_holidays = array(); // Tableau des jours feriés

		// On boucle dans le cas où l'année de départ serait différente de l'année d'arrivée
		$diff_year = date('Y', $date_stop) - date('Y', $date_start);
		for($i = 0; $i <= $diff_year; $i++) {
			$year = (int)date('Y', $date_start) + $i;
	        // Liste des jours feriés
	        $arr_bank_holidays = array_merge($arr_bank_holidays, self::getBankHolidays($calendrier, $year));
		}
		$nb_days_open = 0;
		while($date_start <= $date_stop) {
			// Si le jour suivant n'est ni un dimanche (0) ou un samedi (6), ni un jour férié, on incrémente les jours ouvrés
			if(!in_array(date('w', $date_start), array(0, 6)) && !in_array(date('j_n_'.date('Y', $date_start), $date_start), $arr_bank_holidays))
				$nb_days_open++;
			$date_start = mktime(date('H', $date_start),
								 date('i', $date_start),
								 date('s', $date_start),
								 date('m', $date_start),
								 date('d', $date_start) + 1,
								 date('Y', $date_start));
		}
		return $nb_days_open;
	}

	/**
	 * Récupère la liste des jours fériés d'une année donnée.
	 *
	 * @param string $calendrier Calendrier à utiliser
	 * @param string $year Année dont il faut récupérer tous les jours fériés
	 * @return array Tableau des jours fériés
	 */
	public static function getBankHolidays($calendrier, $year) {
		$easter = easter_date($year);//Calcul du dimanche de pâques
	    switch($calendrier) {
			default://Par défaut, on retourne les jours fériés français
	            //Renvoit les jours de : Nouvelle Année, Fete du Travail, Victoire 1945, Fete Nationale, Assomption, Toussaint, Armistice 1918, Noël, Paques, Ascension et Pentecote
	            return array('1_1_'.$year, //Nouvel An
	            			 '1_5_'.$year, //Fête du travail
	            			 '8_5_'.$year, //Victoire 1945
	            			 '14_7_'.$year, //Fête nationale
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '11_11_'.$year, //Armistice 1918
	            			 '25_12_'.$year, //Noël
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 date('j_n_'.$year, $easter + (86400*39)), //Ascension
	            			 date('j_n_'.$year, $easter + (86400*50))); //Lundi de Pentecôte
	            break;
			case 'France_Sans_Pentecote':
	            //Renvoit les jours de : Nouvelle Année, Fete du Travail, Victoire 1945, Fete Nationale, Assomption, Toussaint, Armistice 1918, Noël, Paques, Ascension
	            return array('1_1_'.$year, //Nouvel An
	            			 '1_5_'.$year, //Fête du travail
	            			 '8_5_'.$year, //Victoire 1945
	            			 '14_7_'.$year, //Fête nationale
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '11_11_'.$year, //Armistice 1918
	            			 '25_12_'.$year, //Noël
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 date('j_n_'.$year, $easter + (86400*39))); //Ascension
	            break;
	        case 'Polynesie':
	            //Renvoit les jours de : Nouvelle Année, Fermeture des administrations, Lundi de pentecôte, Fête de l'autonomie, Fête nationale, Assomption, Toussaint, Armistice, Noël
	            return array('1_1_'.$year, //Nouvel An
	            			 '2_1_'.$year,  //Fermeture des administrations
	            			 date('j_n_'.$year, $easter + (86400*50)), //Lundi de Pentecôte
	            			 '29_6_'.$year, //Fête de l'autonomie
	            			 '14_7_'.$year, //Fête nationale
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '11_11_'.$year, //Armistice 1918
	            			 '25_12_'.$year);//Noël
	            break;
	        case 'Royaume_Uni':
	            return array('1_1_'.$year, //Nouvel An
	            			 '25_12_'.$year, //Noël
	            			 '26_12_'.$year, //Boxing Day
	            			 date('j_n_'.$year, $easter - 86400*2), //Vendredi Saint
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 self::getDateFromSpecification(1, 5, $year, true), //1° Lundi de Mai
	            			 self::getDateFromSpecification(1, 5, $year, false), //Dernier Lundi de Mai
	            			 self::getDateFromSpecification(1, 8, $year, false)); //Dernier Lundi d'Août
	            break;
	        case 'Belgique':
	            //Renvoit les jours de : Nouvelle Année, Fete du Travail, Fete Nationale, Assomption, Toussaint, Armistice 1918, Noël, Paques, Ascension et Pentecote
	            return array('1_1_'.$year, //Nouvel An
	            			 '1_5_'.$year, //Fête du travail
	            			 '21_7_'.$year, //Fête nationale
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '11_11_'.$year, //Armistice 1918
	            			 '25_12_'.$year, //Noël
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 date('j_n_'.$year, $easter + (86400*39)), //Ascension
	            			 date('j_n_'.$year, $easter + (86400*50))); //Lundi de Pentecôte
	            break;
	        case 'Allemagne':
	            return array('1_1_'.$year, //Nouvel An
	            			 '6_1_'.$year, //Epiphanie
	            			 date('j_n_'.$year, $easter - 86400*2), //Vendredi Saint
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 '1_5_'.$year, //Fête du travail
	            			 date('j_n_'.$year, $easter + (86400*39)), //Ascension
	            			 date('j_n_'.$year, $easter + (86400*50)), //Lundi de Pentecôte
	            			 date('j_n_'.$year, $easter + 86400*(7*8 + 4)), //Fête Dieu
	            			 '3_10_'.$year, //Jour de l'unité Allemande
	            			 '1_11_'.$year, //Toussaint
	            			 '25_12_'.$year, //Noël
	            			 '26_12_'.$year); //Lendemain de Noël
	            break;
	        case 'Suisse':
	            return array('1_1_'.$year, //Nouvel An
	            			 '2_1_'.$year, //Saint Berchtold
	            			 date('j_n_'.$year, $easter - 86400*2), ///Vendredi Saint
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 date('j_n_'.$year, $easter + (86400*39)), //Ascension
	            			 date('j_n_'.$year, $easter + (86400*50)), //Lundi de Pentecôte
	            			 '1_8_'.$year, //Fête nationale
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '25_12_'.$year, //Noël
	            			 '26_12_'.$year); //Saint Etienne
	            break;
	        case 'Suisse_Geneve':
	            $FirstSundaySeptember = explode('_', self::getDateFromSpecification(0, 9, $year, true, 1)); //1° dimanche de Septembre
	            return array('1_1_'.$year, //Nouvel An
	            			 date('j_n_'.$year, $easter - 86400*2), //Vendredi Saint
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 date('j_n_'.$year, $easter + (86400*39)), //Ascension
	            			 date('j_n_'.$year, $easter + (86400*50)), //Lundi de Pentecôte
	            			 '1_8_'.$year, //Fête nationale
	            			 date('j_n_Y', mktime(0,0,0,9,$FirstSundaySeptember[0]+4,$year)), //Jeune Gênevois (1° dimanche de Septembre + 4 jours)
	            			 '25_12_'.$year, //Noël
	            			 '31_12_'.$year); //Restauration de la République
	            break;
			case 'Luxembourg':
	            return array('1_1_'.$year, //Nouvel An
	            			 '1_5_'.$year,  //Fête du travail
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '25_12_'.$year, //Noël
	            			 '26_12_'.$year, //Lendemain de Noël
	            			 '23_06_'.$year, //Fête nationale
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 date('j_n_'.$year, $easter + (86400*39)), //Ascension
	            			 date('j_n_'.$year, $easter + (86400*50))); //Lundi de Pentecôte
	            break;
			case 'Pays_Bas':
				return array(
					'1_1_'.$year, // nouvel an
					'27_4_'.$year, // jour du roi
					'25_12_'.$year, //Noël
					'26_12_'.$year, //Lendemain de Noël
					date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
					date('j_n_'.$year, $easter + (86400*39)), //Ascension
					date('j_n_'.$year, $easter + (86400*50)) //Lundi de Pentecôte
				);
		    case 'Malaisie':
				$tabMuslim = array('Intitulé' => array('Nouvel An','Anniversaire du prophète','Thaipusam','Journée du Territoire Fédérale','Nouvel An Chinois 1° Jour','Nouvel An Chinois 2° Jour','Fête du travail','Journée de Wesak', 'Anniversaire du Roi','Hari Raya Puasa 1° Jour (Aïd el-Fitr)','Hari Raya Puasa 2° Jour', 'Fête Nationale','Journée de la Malaisie','Hari Raya Haji (Aïd el-Kebir)','Deepavali', 'Ramadan','Noël'),
					2013 => array('1_1','24_1','27_1','1_2','10_2','11_2','1_5','24_5','1_6','8_8','9_8','31_8','16_9','15_10','3_11','5_11','25_12'),
					2014 => array('1_1','14_1','16_1','1_2','31_1','1_2','1_5','13_5','7_6','29_7','30_7','1_9','16_9','5_10','23_10','25_10','25_12'),
					2015 => array('1_1','3_1','3_2','2_2','19_2','20_2','1_5','1_6','6_6','18_7','19_7','31_8','16_9','24_9','11_11','15_10','25_12'),
					2016 => array('1_1','12_12','24_1','1_2','8_2','9_2','2_5','21_5','4_6','7_7','8_7','31_8','16_9','13_9','30_10','3_10','26_12'),
					2017 => array('2_1','1_12','10_2','1_2','28_1','29_1','1_5','10_5','3_6','26_6','27_6','31_8','16_9','2_9','19_10','22_9','25_12'),
					2018 => array('1_1','21_11','31_1','1_2','16_2','17_2','1_5','29_5','2_6','15_6','16_6','31_8','16_9','22_8','7_11','12_9','25_12'),
					2019 => array('1_1','10_11','21_1','1_2','5_2','6_2','1_5','19_5','1_6','5_6','6_6','31_8','16_9','12_8','27_10','1_9','25_12'),
					2020 => array('1_1','29_10','8_2','1_2','25_1','26_1','1_5','7_5','6_6','24_5','25_5','31_8','16_9','31_7','14_11','20_8','25_12'));

				return self::addMuslimBankHolidays($tabMuslim, $year);
	            break;
			case 'Singapour':
				$tabMuslim = array('Intitulé' => array('Nouvel An','Nouvel An Chinois 1° Jour','Nouvel An Chinois 2° Jour','Vendredi Saint','Fête du travail','Journée de Wesak','Hari Raya Puasa 1° Jour (Aïd el-Fitr)','Fête Nationale','Hari Raya Haji (Aïd el-Kebir)','Deepavali','Noël'),
					2013 => array('1_1','10_2','11_2','29_3','1_5','24_5','8_8','9_8','15_10','4_11','25_12'),
					2014 => array('1_1','31_1','1_2','18_4','1_5','13_5','9_8','24_9','5_10','25_12'),
					2015 => array('1_1','19_2','20_2','3_4','1_5','1_6','18_7','10_8','24_9','11_11','25_12'),
					2016 => array('1_1','8_2','9_2','25_3','2_5','21_5','7_7','9_8','13_9','30_10','25_12'),
					2017 => array('1_1','28_1','30_1','14_4','1_5','10_5','26_6','9_8','2_9','19_10','25_12'),
					2018 => array('1_1','16_2','17_2','30_3','1_5','29_5','15_6','9_8','22_8','7_11','25_12'),
					2019 => array('1_1','5_2','6_2','19_4','1_5','19_5','5_6','9_8','12_8','27_10','25_12'),
					2020 => array('1_1','25_1','27_1','10_4','1_5','7_5','24_5','10_8','31_7','14_11','25_12'));

				return self::addMuslimBankHolidays($tabMuslim, $year);
	            break;
			case 'Etats_Unis':
	            //Renvoit les jours de : Nouvelle Année, Martin Luther King Jr. Birthday, President's Day, Memorial Day, Independance Day, Labor Day, Colombus Day, Thanksgiving, Christmas Day
	            return array('1_1_'.$year,
	            			 self::getDateFromSpecification(1, 1, $year, true, 3),
	            			 self::getDateFromSpecification(1, 2, $year, true, 3),
	            			 self::getDateFromSpecification(1, 5, $year, false),
	            			 '4_7_'.$year,
	            			 self::getDateFromSpecification(1, 9, $year, true),
	            			 self::getDateFromSpecification(1, 10, $year, true, 2),
	            			 self::getDateFromSpecification(4, 11, $year, false),
	            			 '25_12_'.$year);
	            break;
	        case 'Tunisie':
	            $tabGregorian = array('1_1_'.$year, //Nouvel an
	            					  '14_1_'.$year, //Fête de la révolution & jeunesse
	            					  '20_3_'.$year, //Fête de l'indépendance
	            					  '9_4_'.$year, //Journée des martyrs
	            					  '1_5_'.$year, //Fête du travail
	            					  '25_7_'.$year, //Fête de la république
	            					  '13_8_'.$year, //Fête de la femme/famille
	            					  '15_10_'.$year); //Fête de l'évacuation

	            $tabMuslim = array('Intitulé' => array('Nouvel An','Anniversaire du prophète','Aïd el-Fitr','Aïd el-Kebir'),
									2014 => array('25_10','14_1','29_7','5_10'),
									2015 => array('15_10','3_1','18_7','24_9'),
									2016 => array('3_10','12_12','7_7','13_9'),
									2017 => array('22_9','1_12','26_6','2_9'),
									2018 => array('12_9','21_11','15_6','22_8'),
									2019 => array('1_9','10_11','5_6','12_8'),
									2020 => array('20_8','29_10','24_5','31_7'));

				return self::addMuslimBankHolidays($tabMuslim, $year, $tabGregorian);
	            break;
	        case 'Australie':
	            return array('1_1_'.$year, //Nouvelle Année
	            		     '26_1_'.$year,
	            		     '25_4_'.$year,
	            		     '25_12_'.$year, //Noël
	            		     '26_12_'.$year, //Lendemain de Noël
	            			 date('j_n_'.$year, $easter - 86400*2), //Vendredi Saint
	            			 date('j_n_'.$year, $easter + 86400)); //Lundi de Pâques
	            break;
	        case 'Monaco':
	            return array('1_1_'.$year, //Nouvel An
	            			 '27_1_'.$year,
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 '1_5_'.$year, //Fête du travail
	            			 date('j_n_'.$year, $easter + 86400*39), //Ascension
	            			 date('j_n_'.$year, $easter + 86400*50), //Lundi de Pentecôte
	            			 date('j_n_'.$year, $easter + 86400*(7*8 + 4)), //Fête Dieu
	            			 '15_8_'.$year, //Assomption
	            			 '1_11_'.$year, //Toussaint
	            			 '19_11_'.$year, //Fête du Prince
	            			 '8_12_'.$year, //Immaculée Conception
	            			 '25_12_'.$year); //Noël
	            break;
	        case 'Canada_Quebec':
	            return array('1_1_'.$year, //Nouvelle Année
	            			 date('j_n_'.$year, $easter - 86400*2), //Vendredi Saint
	            			 date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
	            			 self::getDateFromSpecification(1, 5, $year, false, 2), //Journée nationale des patriotes
	            			 '24_6_'.$year, //Fête nationale
	            			 '1_7_'.$year, //Fête du Canada
	            			 self::getDateFromSpecification(1, 9, $year, true, 1), //Fête du travail
	            			 self::getDateFromSpecification(1, 10, $year, true, 2), //Action de grâce
	            			 '25_12_'.$year); //Noël
	            break;
			case "Slovaquie":
				return array(
					'1_1_'.$year, // Nouvelle Année
					'6_1_'.$year, // Epiphanie
					date('j_n_'.$year, $easter - 86400*2), // Vendredit Saint
					date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
					'1_5_'.$year, //Fête du travail
					'8_5_'.$year, //Jour de la victoire sur le nazisme
					'5_7_'.$year, //Fête de saints Cyrille et Méthode
					'29_8_'.$year, //Jour d'anniversaire du soulèvement national
					'1_9_'.$year, //Jour de la constitution de la république
					'15_10_'.$year, //Fête de notre dame des septs douleurs
					'1_11_'.$year, //Toussaint
					'17_11_'.$year, //Journée de la lutte pour la liberté & démocratie
					'24_12_'.$year, //Veille de noël
					'25_12_'.$year, //Noël
					'26_12_'.$year); //Saint Etienne
            	break;
			case "Republique_Tcheque":
				return array(
					'1_1_'.$year, //Nouvelle Année
					date('j_n_'.$year, $easter + 86400), //Lundi de Pâques
					'1_5_'.$year, //Fete du Travail
					'8_5_'.$year, //Fëte de la libération
					'5_7_'.$year, //Fête de saints Cyrille et Méthode
					'6_7_'.$year, //Fête de Jan Hus
					'28_9_'.$year, //Fête de Saint Wanceslas
					'28_10_'.$year, //Fête de la fondation du pays
					'17_11_'.$year, //Journée de la lutte pour la liberté & démocratie
					'24_12_'.$year, //Veille de noël
					'25_12_'.$year, //Noël
					'26_12_'.$year //Saint Etienne
				);
				break;
			case "Emirats_Arabes_Unis":
				/* source :
					http://www.timeanddate.com/holidays/united-arab-emirates/2015#!hol=9
					http://www.timeanddate.com/holidays/united-arab-emirates/2016#!hol=9
				*/
				$tabMuslim = array(
					2015 => array('1_1', '3_1', '16_5', '16_7', '17_7', '18_7', '22_9', '23_9', '24_9', '25_9', '15_10', '30_11', '2_12' , '3_12' , '24_12'),
					2016 => array('1_1', '5_5', '6_7', '7_7', '10_9', '11_9', '12_9', '13_9', '2_10', '30_11', '2_12', '3_12', '11_12')
				);

				return self::addMuslimBankHolidays($tabMuslim, $year);
				break;
			case 'Maroc':
				//Mixe du calendrier grégorien et calendrier musulman
				$tabGregorian = array('1_1_'.$year, //Nouvel an
									'11_1_'.$year, //Manifeste de l'indépendance
									'1_5_'.$year, //Fête du travail
									'30_7_'.$year, //Fête du Trône
									'14_8_'.$year, //Allégeance Oued Eddahab
									'20_8_'.$year, //Révolution du roi et du peuple
									'21_8_'.$year, //Fête de la jeunesse
									'6_11_'.$year, //La marche verte
									'18_11_'.$year); //Fête de l'indépendance

				$tabMuslim = array('Intitulé' => array('Nouvel An','Anniversaire du prophète','Aïd el-Fitr','Aïd el-Kebir'),
					2014 => array('25_10','14_1','29_7','5_10'),
					2015 => array('15_10','3_1','18_7','24_9'),
					2016 => array('3_10','12_12','7_7','13_9'),
					2017 => array('22_9','1_12','26_6','2_9'),
					2018 => array('12_9','21_11','15_6','22_8'),
					2019 => array('1_9','10_11','5_6','12_8'),
					2020 => array('20_8','29_10','24_5','31_7'));

				return self::addMuslimBankHolidays($tabMuslim, $year, $tabGregorian);
				break;
			case 'Cambodge':
				//http://www.mef.gov.kh/public-holiday.html
				$tabGregorian = array('1_1_'.$year,
									'7_1_'.$year,
									'8_3_'.$year,
									'1_5_'.$year,
									'13_5_'.$year,
									'14_5_'.$year,
									'15_5_'.$year,
									'1_6_'.$year,
									'18_6_'.$year,
									'24_9_'.$year,
									'23_10_'.$year,
									'29_10_'.$year,
									'9_11_'.$year,
									'10_12_'.$year);

				$tabMuslim = array('Intitulé' => array('Meak Bockea Day', 'Khmer New Year Day', 'Visak Bockea Day', 'Royal Plowing Ceremony', 'Pchum Ben Day', ' Commemoration Day of King\'s Father', 'King\'s commemoration day', 'Water Festival'),
					2016 => array('22_2','13_4','14_4','15_4','16_4','2_5','16_5','20_5','24_5','30_9','1_10','2_10','3_10','15_10','24_10','13_11','14_11','15_11','16_11'),
					2017 => array('2_1','9_1','11_2','13_2','14_4','15_4','16_4','17_4','10_5','16_5','19_6','19_9','20_9','21_10','25_9','15_10','16_10','30_10','2_11','3_11','4_11','6_11','11_12'));

				return self::addMuslimBankHolidays($tabMuslim, $year, $tabGregorian);
				break;
	    }
	}

	/**
	 * Extract bank holliday for a specific year
	 * @param array $tabMuslimCalendar Muslim calendar
	 * @param int $year Year to extract
	 * @param array $tabOtherCalendar Other calendar to merge with $tabDates
	 * @return array
	 */
	public static function addMuslimBankHolidays($tabMuslimCalendar = array(), $year = 0, $tabOtherCalendar = array()) {
		if(isset($tabMuslimCalendar[$year])) {
			foreach($tabMuslimCalendar[$year] as $i => $date)
				$tabMuslimCalendar[$year][$i] = $date.'_'.$year;

			return array_merge($tabMuslimCalendar[$year], $tabOtherCalendar);
		} else return $tabOtherCalendar;
	}
	/**
	 * Retourne un jour particulier de l'année.
	 *
	 * @param integer $dayweek Jour de la semaine recherché (0 = dimanche, 6 = samedi)
	 * @param integer $month Mois considéré du départ de la recherche
	 * @param integer $year Année considérée du départ de la recherche
	 * @param bool $begin Sens du départ de la recherche :
	 *  - `true : Début du mois
	 *  - `false` : Fin du mois
	 * @param integer $nb Nombre de jours après le départ de la recherche
	 * @return false|string
	 * - `false` : Jour non trouvé
	 * - __string__ : Jour trouvé au format 'jour_mois_annee' avec jour, mois & annee = *integer*
	 */
	public static function getDateFromSpecification($dayweek, $month, $year, $begin = true, $nb = 1) {
	    $nbJour = date('t', mktime(0,0,0,$month,1,$year));
		$j = 1;
	    if($begin) {
	        for($i = 1; $i <= $nbJour; $i++) {
	            $timeDay = mktime(0,0,0,$month, $i, $year);
	            if(date('w', mktime(0,0,0,$month, $i, $year)) == $dayweek) {//Si ce jour est celui de dayweek (0=dimanche, 6=samedi)
					if($nb == $j) return date('j_n_'.$year, $timeDay); else $j++;
				}
	        }
	    } else {
	        for($i = $nbJour; $i >= 1; $i--) {
	            $timeDay = mktime(0,0,0,$month, $i, $year);
	            if(date('w', mktime(0,0,0,$month, $i, $year)) == $dayweek) {//Si ce jour est celui de dayweek (0=dimanche, 6=samedi)
					if($nb == $j) return date('j_n_'.$year, $timeDay); else $j++;
				}
	        }
	    }
	    return false;
	}

	/**
     * Retourne le fichier dont l'extension existe
     *
     * @param string $filewithoutextension Nom du fichier à récupérer sans son extension
	 * @param array $tabExtensions Tableau des extensions à tester (sans le .)
	 * @return false|string
	 * - `false` : Fichier non trouvé
	 * - __string__ : Nom complet du fichier trouvé
	 */
	public static function whichImageWithExtensionExist($filewithoutextension, $tabExtensions = array('gif','png','jpg','jpeg')) {
		foreach($tabExtensions as $extension) if(file_exists($filewithoutextension.'.'.$extension)) return pathinfo($filewithoutextension.'.'.$extension, PATHINFO_BASENAME);
        return false;
	}

	/**
	 * return a mapped value for the input
	 * @param string|array $input a single value or an array of value to map
	 * @param array $mapping
	 * @return mixed the mapped value or an array if the input was an array
	 */
	public static function mapData($input, array $mapping)
	{
		if(!is_array($input)){
			if(array_key_exists($input, $mapping)) return $mapping[$input];
			else return null;
		}else{
			$data = [];
			foreach($input as $k=>$i){
				$mappedValue = self::mapData($i, $mapping);
				// on filtre a la main pour supprimer uniquement les valeurs NULL
				if(!is_null($mappedValue)) $data[$k] = $mappedValue;
			}
			return $data;
		}
	}

	public static function reverseMapData($input, $mapping){
		return self::mapData($input, array_flip($mapping));
	}

	/**
	 * @param $inputData
	 * @param $colAsKey
	 * @param $colAsValue
	 * @return array
	 */
	public static function buildMaping($inputData, $colAsKey, $colAsValue){
		$map = [];
		foreach($inputData as $data){
			if(isset($data[$colAsKey])) $map[$data[$colAsKey]] = isset($data[$colAsValue])?$data[$colAsValue]:null;
		}
		return $map;
	}

	/**
	 * filter an array to keep only items matching a given spec
	 * @param array $array
	 * @param AbstractSpecificationItem $spec
	 * @return array
	 */
	public static function filterArrayWithSpec(array $array, AbstractSpecificationItem $spec){
		return array_filter($array, function($val) use ($spec){
			return $spec->isSatisfiedBy($val);
		});
	}

	/**
	 * use a column as a key to reindex the given array
	 * @param string $column column to map
	 * @param array $data
	 * @param bool $castString
	 * @return array
	 */
	public static function useColumnAsKey($column, $data, $castString = false){
		$newData = [];
		foreach($data as $row){
			$key = ($castString) ? strval($row[$column]) : $row[$column];
			$newData[$key] = $row;
		}
		return $newData;
	}

	/**
	 * @param $array
	 * @param $keysMapping
	 * @return array
	 */
	public static function rename_array_keys($array, $keysMapping){
		$newArray = [];
		foreach($array as $key=>$value){
			if(array_key_exists($key, $keysMapping)) $newArray[ $keysMapping[$key] ] = $value;
			else $newArray[ $key ] = $value;
		}
		return $newArray;
	}

	/**
	 * @param $publicMaping
	 * @param $data
	 * @return array
	 */
	public static function reversePublicFieldsToPrivate($publicMaping, $data)
	{
		// build array with key matching sql fields
		$privateFieldsMapping = array_flip($publicMaping);
		$newData = [];

		foreach($privateFieldsMapping as $publicField => $privateField){
			if(array_key_exists($publicField, $data)){
				$value = $data[$publicField];
				if(!is_array($value)) $newData[$privateField] = $data[$publicField];
			}
		}

		return $newData;
	}

	/**
	 * @param string $firstName
	 * @param string $lastName
	 * @return string
	 */
	public static function buildTrigramFromContact($firstName, $lastName) {
		return strtoupper(substr(Tools::suppr_accents($firstName),0,1).substr(Tools::suppr_accents($lastName),0,2));
	}

	/**
	 * @param $str
	 * @param array $noStrip
	 * @return mixed|string
	 */
	public static function camelCase($str, array $noStrip = [])
	{
		// non-alpha and non-numeric characters become spaces
		$str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
		$str = trim($str);
		// uppercase the first character of each word
		$str = ucwords($str);
		$str = str_replace(" ", "", $str);
		$str = lcfirst($str);

		return $str;
	}

	/**
	 * extract all data from array 1 than are different from array 2
	 * @param array $array1
	 * @param array $array2
	 * @return array
	 */
	public static function arrayRecursiveDiff(array $array1, array $array2)
	{
		$result = [];
		foreach($array1 as $key1 => $value1){
			if(array_key_exists($key1, $array2)) {
				$value2 = $array2[$key1];
				if(is_array($value1) && is_array($value2)) {
					$diff = self::arrayRecursiveDiff($value1, $value2);
					if($diff) $result[$key1] = $diff;
				}else if($value1 != $value2){
					$result[$key1] = $value1;
				}
			}else{
				$result[$key1] = $value1;
			}
		}

		return $result;
	}

	/**
	 * @param $object
	 * @return string
	 */
	public static function getInstanceID($object) {
		ob_start();
		var_dump($object);
		$content = ob_get_contents();
		ob_end_clean();
		$data = explode("\n", $content, 1);
		return trim(preg_replace('|^.*\((.*)\)#([0-9]+).*$|m', '$1#$2', $data[0]));
	}
}
