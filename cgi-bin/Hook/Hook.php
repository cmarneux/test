<?php
/**
 * ATTENTION : Il ne peut y avoir qu'un script tournant simultanément pour gérer les Hooks
 *
 * - $_SERVER['argv'][1] = GRPCONF_WEB
 * - $_SERVER['argv'][2] = Type de Hook (0, 1, 2)
 * - $_SERVER['argv'][3] = Paramètre Ret (Module_idFiche_tabFiche)
 * - $_SERVER['argv'][4] = Url de l'App, encodée en base64, à appeler pour le Hook
 * - $_SERVER['argv'][5] = Mode de BoondManager
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

use BoondManager\Services\BM;
use Wish\Web;

define('WISH_HOOKQUEUE', 1000);
if(isset($_SERVER['argv'][1], $_SERVER['argv'][2], $_SERVER['argv'][3], $_SERVER['argv'][4], $_SERVER['argv'][5])) {
	list($script, $customerCode, $hookType, $ret, $appUrl, $mode) = $_SERVER['argv'];

	set_time_limit(0); //Ce script n'a pas de durée limite d'exécution

	$scriptPath = realpath(dirname(__FILE__));

	/** @var \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base).en base de données. */
	//Autoloader composer
	require($scriptPath.'/../../vendor/autoload.php');
	$f3 = \Base::instance();

	//Main application folder
	$f3->set('MAIN_ROOTPATH', $scriptPath.'/../..');

	//Répertoire racine de l'application
	$f3->set('MAIN_APPPATH', $scriptPath.'/../../app');

	// Chargement des fichiers de configuration globaux ?
	$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/boondmanager.ini');
	\Base::instance()->set('PLUGINS', \Base::instance()->get('MAIN_APPPATH').'/');

	BM::initScript($mode);

	$hookDirectory = $f3->get('MAIN_ROOTPATH').'/cgi-bin/Hook';
	$hookLock = $hookDirectory.'/Hook.pid';

	$queue = msg_get_queue(WISH_HOOKQUEUE);
	msg_send($queue, 1, $customerCode.' '.$hookType.' '.$ret.' '.$appUrl);

	//Si le script ne tourne pas pour ce serveur alors on l'exécute, sinon on termine le script afin de ne pas en lancer plusieurs en parallèle
	if(!file_exists($hookLock)) {
		file_put_contents($hookLock, getmypid());//On construit le verrou

		try {
			$web = new Web();
			$msg_type = NULL;
			$msg = NULL;
			while(msg_receive($queue, 1, $msg_type, 16384, $msg, true, MSG_IPC_NOWAIT)) {
				list($msgCustomerCode, $msgHookType, $msgRet, $msgAppURL) = explode(' ', $msg);

				$tabURL = explode('?', base64_decode($msgAppURL));
				$tabParams = array();
				parse_str($tabURL[1], $tabParams);

				$web->setUrl($tabURL[0], 'application/json')->get($tabParams);
				$tabStat = msg_stat_queue($queue);

				$logger = new \Log('hook.script.log');
				$logger->write($msgCustomerCode.' - '.$msgHookType.' - '.$msgRet.' - '.$tabURL[0].' - '.$tabStat['msg_qnum'].PHP_EOL);
			}
		} catch(Exception $e) {}
		unlink($hookLock);//On supprime le verrou
	}
}
