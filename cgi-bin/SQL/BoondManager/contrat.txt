/**
* Contract.
*
* @class  TAB_CONTRAT
*/


/**
* Unique identifier.
* @property ID_CONTRAT
* @type int(11)
*/

/**
* Type.
*
* _cf. key **id** for JSON response **data.settings.typeOf.contract** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CTR_TYPE
* @type tinyint(2)
* @default 0
*/

/**
* Start date.
* @property CTR_DEBUT
* @type date
*/

/**
* End date.
* @property CTR_FIN
* @type date
*/

/**
* End date of pratice period.
*
* This property can be `null`.
* @property CTR_DATEPE1
* @type date
*/

/**
* End date of renewal's pratice period.
*
* This property can be `null`.
* @property CTR_DATEPE2
* @type date
*/

/**
* Average daily cost.
* @property CTR_CJMCONTRAT
* @type double
* @default 0
*/

/**
* Average daily cost used in production financial data.
* @property CTR_CJMPRODUCTION
* @type double
* @default 0
*/

/**
* Enable/disable to manually define **CTR_CJMPRODUCTION** :
* - `1` : Enable
* - `0` : Disable_, **CTR_CJMPRODUCTION** = **CTR_CJMCONTRAT**_
* @property CTR_FORCECJMPRODUCTION
* @type tinyint(1)
* @default 0
*/

/**
* Hourly gross salary.
* @property CTR_SALAIREHEURE
* @type double
* @default 0
*/

/**
* Monthly gross salary.
* @property CTR_SALAIREMENSUEL
* @type double
* @default 0
*/

/**
* Charge factor.
* @property CTR_CHARGE
* @type float(4)
* @default 0
*/

/**
* Number of working days.
* @property CTR_NBJRSOUVRE
* @type float(4)
* @default 0
*/

/**
* Daily net expenses.
* @property CTR_FRSJOUR
* @type double
* @default 0
*/

/**
* Monthly net expenses.
* @property CTR_FRSMENSUEL
* @type double
* @default 0
*/

/**
* Currency.
*
* _cf. key **id** for JSON response **data.settings.currency** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CTR_DEVISE
* @type tinyint(2)
* @default 0
*/

/**
* Exchange rate.
* @property CTR_CHANGE
* @type float(4)
* @default 1
*/

/**
* Agency's currency.
*
* _cf. key **id** for JSON response **data.settings.currency** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CTR_DEVISEAGENCE
* @type tinyint(2)
* @default 0
*/

/**
* Agency's exchange rate.
* @property CTR_CHANGEAGENCE
* @type float(4)
* @default 1
*/

/**
* Information comments.
* @property CTR_COMMENTAIRES
* @type text
*/

/**
* List of advantages.
*
* Each advantage is described by :
* - Reference : _cf. [TAB_TYPEAVANTAGE.TYPEA_REF](TAB_TYPEAVANTAGE.html#property_TYPEA_REF)_
* - Frequency : _cf. [TAB_TYPEAVANTAGE.TYPEA_TYPE](TAB_TYPEAVANTAGE.html#property_TYPEA_TYPE)_
* - Category : _cf. [TAB_TYPEAVANTAGE.TYPEA_CATEGORIE](TAB_TYPEAVANTAGE.html#property_TYPEA_CATEGORIE)_
* - Participation amount : _cf. [TAB_TYPEAVANTAGE.TYPEA_QUOTAPARTICIPATION](TAB_TYPEAVANTAGE.html#property_TYPEA_QUOTAPARTICIPATION)_
* - Employee amount : _cf. [TAB_TYPEAVANTAGE.TYPEA_QUOTARESSOURCE](TAB_TYPEAVANTAGE.html#property_TYPEA_QUOTARESSOURCE)_
* - Agency amount : _cf. [TAB_TYPEAVANTAGE.TYPEA_QUOTASOCIETE](TAB_TYPEAVANTAGE.html#property_TYPEA_QUOTASOCIETE)_
* - Reference, frequency, category, participation amount, employee amount, agency amount are listed in this order and separated by `|` character
*
* Advantages are separated by `#` character.
* @property CTR_AVANTAGES
* @type text
*/

/**
* Classification.
*
* _cf. key **id** for JSON response **data.settings.classification** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CTR_CLASSIFICATION
* @type varchar(250)
* @default ''
*/

/**
* Employee's type.
*
* _cf. key **id** for JSON response **data.settings.typeOf.employee** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CTR_CATEGORIE
* @type tinyint(2)
* @default -1
*/

/**
* Working time's type.
*
* _cf. key **id** for JSON response **data.settings.typeOf.workingTime** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CTR_TPSTRAVAIL
* @type tinyint(2)
* @default -1
*/

/**
* Weekly duration of work.
* @property CTR_DUREEHEBDOMADAIRE
* @type float(4)
* @default 0
*/

/**
* Enable/disable to manually define **CTR_SALAIREHEURE** :
* - `1` : Enable
* - `0` : Disable
* @property CTR_FORCESALAIREHEURE
* @type tinyint(1)
* @default 0
*/

/**
* List of exceptional scales.
*
* Each scale is described by :
* - Scale information :
* 	- Unique identifier : _cf. [TAB_GROUPECONFIG.GRPCONF_BAREMESEXCEPTION](TAB_GROUPECONFIG.html#property_GRPCONF_BAREMESEXCEPTION)_
* 	- `0`
* 	- Agency's identifier : _cf. [TAB_GROUPECONFIG.ID_SOCIETE](TAB_GROUPECONFIG.html#property_ID_SOCIETE)_
* 	- Unique identifier, `0` & agency's identifier are listed in this order and separated by `_`character
* - List of rules :
* 	- Each rule is described by :
* 		- Reference : [TAB_REGLEEXCEPTION.REGLE_REF](TAB_REGLEEXCEPTION.html#property_REGLE_REF), *with [TAB_REGLEEXCEPTION.REGLE_BAREMEREF](TAB_REGLEEXCEPTION.html#property_REGLE_BAREMEREF) = `Scale's unique identifier` & [TAB_REGLEEXCEPTION.PARENT_TYPE](TAB_REGLEEXCEPTION.html#property_PARENT_TYPE) = `Entity's type` & [TAB_REGLEEXCEPTION.ID_PARENT](TAB_REGLEEXCEPTION.html#property_ID_PARENT) = `Entity's identifier`*
*  		- Price excluding tax or price rate : If `not empty` then replace [TAB_REGLEEXCEPTION.REGLE_TARIF](TAB_REGLEEXCEPTION.html#property_REGLE_TARIF)
*    	- Gross cost or salary rate : If `not empty` then replace [TAB_REGLEEXCEPTION.REGLE_COUT](TAB_REGLEEXCEPTION.html#property_REGLE_COUT)
*   -  Rules are separated by `|` character
* - Scale information & Liste of rules are listed in this order and separated by `|` character
* - Price excluding tax or price rate & Gross cost or salary rate can not be both `empty`
*
* Scales are separated by `#` character.
* @example `"1_0_1#2_0_1|2|100|"`
* @property CTR_REGLESTEMPSEXCEPTION
* @type text
*/

/**
* [TAB_CONTRAT.ID_PARENT](TAB_CONTRAT.html#property_ID_PARENT).
*
* Previous contract.
* @property   ID_PARENT
* @type int(11)
* @default 0
*/

/**
* [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL).
*
* Main manager.
* @property ID_PROFIL
* @type int(11)
* @default 0
*/

/**
* [TAB_SOCIETE.ID_SOCIETE](TAB_SOCIETE.html#property_ID_SOCIETE).
*
* Agency.
* @property ID_SOCIETE
* @type int(11)
* @default 0
*/
