/**
* Relationship between order & delivery or purchase.
*
* @class  TAB_CORRELATIONBONDECOMMANDE
*/


/**
* Unique identifier.
* @property ID_CORRELATIONBONDECOMMANDE
* @type int(11)
*/

/**
* [TAB_BONDECOMMANDE.ID_BONDECOMMANDE](TAB_BONDECOMMANDE.html#property_ID_BONDECOMMANDE).
*
* Order.
* @property ID_BONDECOMMANDE
* @type int(11)
* @default 0
*/

/**
* Entity's identifier :
* - Company : if [TAB_CORRELATIONBONDECOMMANDE.CORBDC_TYPE](TAB_CORRELATIONBONDECOMMANDE.html#property_CORBDC_TYPE) is `0`*, cf. [TAB_MISSIONPROJET.ID_MISSIONPROJET](TAB_MISSIONPROJET.html#property_ID_MISSIONPROJET)*
* - Opportunity : if [TAB_CORRELATIONBONDECOMMANDE.CORBDC_TYPE](TAB_CORRELATIONBONDECOMMANDE.html#property_CORBDC_TYPE) is `1`*, cf. [TAB_ACHAT.ID_ACHAT](TAB_ACHAT.html#property_ID_ACHAT)*
* @property ID_ITEM
* @type int(11)
* @default 0
*/

/**
* Entity's type :
* - `0` : Delivery
* - `1` : Purchase
* @property CORBDC_TYPE
* @type tinyint(1)
* @default 0
*/
