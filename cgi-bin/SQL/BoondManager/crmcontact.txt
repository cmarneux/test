/**
* Contact.
*
* @class  TAB_CRMCONTACT
*/


/**
* Unique identifier.
* @property ID_CRMCONTACT
* @type int(11)
*/

/**
* Civility.
*
* _cf. key **id** for JSON response **data.settings.civility** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property CCON_CIVILITE
* @type tinyint(1)
* @default 0
*/

/**
* Last name.
* @property CCON_NOM
* @type varchar(100)
* @default ''
*/

/**
* First name.
* @property CCON_PRENOM
* @type varchar(100)
* @default ''
*/

/**
* Type.
*
* _cf. key **id** for JSON response **data.settings.state.contact** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property  CCON_TYPE
* @type tinyint(2)
* @default 0
*/

/**
* Department.
*
* Could be a customized string or one of [TAB_CRMSOCIETE.CSOC_SERVICES](TAB_CRMSOCIETE.html#property_CSOC_SERVICES).
* @property CCON_SERVICE
* @type varchar(100)
* @default ''
*/

/**
* Function.
* @property CCON_FONCTION
* @type varchar(100)
* @default ''
*/

/**
* Phone 1.
* @property CCON_TEL1
* @type char(20)
* @default ''
*/

/**
* Phone 2.
* @property CCON_TEL2
* @type char(20)
* @default ''
*/

/**
* Fax.
* @property CCON_FAX
* @type char(20)
* @default ''
*/

/**
* Email 1.
* @property CCON_EMAIL
* @type varchar(100)
* @default ''
*/

/**
* Email 2.
* @property CCON_EMAIL2
* @type varchar(100)
* @default ''
*/

/**
* Email 3.
* @property CCON_EMAIL3
* @type varchar(100)
* @default ''
*/

/**
* Address.
* @property CCON_ADR
* @type varchar(250)
* @default ''
*/

/**
* Postcode.
* @property CCON_CP
* @type char(10)
* @default ''
*/

/**
* Town.
* @property CCON_VILLE
* @type varchar(100)
* @default ''
*/

/**
* Country.
* @property CCON_PAYS
* @type varchar(100)
* @default 'France'
*/

/**
* Information comments.
* @property CCON_COMMENTAIRE
* @type varchar(250)
* @default ''
*/

/**
* Origin's type.
*
* _cf. key **id** for JSON response **data.settings.origin** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property CCON_TYPESOURCE
* @type tinyint(2)
* @default -1
*/

/**
* Origin's detail.
* @property CCON_SOURCE
* @type varchar(100)
* @default ''
*/

/**
* List of activity areas.
*
* Each activity area is described by key **option.id** for JSON response **data.settings.activityArea** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction).
*
* Activity areas are separated by `|` character.
* @example `"category1domain1|category2domain1"`
* @property CCON_APPLICATIONS
* @type text
*/

/**
* List of tools.
*
* Each tool is described by key **id** for JSON response **data.settings.tool** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction).
*
* Tools are separated by `|` character.
* @example `"tool1|tool2"`
* @property CCON_OUTILS
* @type text
*/

/**
* Update date.
* @property CCON_DATEUPDATE
* @type datetime
*/

/**
* Creation date.
* @property CCON_DATE
* @type datetime
*/

/**
* [TAB_CRMSOCIETE.ID_CRMSOCIETE](TAB_CRMSOCIETE.html#property_ID_CRMSOCIETE).
*
* Company.
* @property  ID_CRMSOCIETE
* @type int(11)
* @default 0
*/

/**
* [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL).
*
* Main manager.
* @property  ID_PROFIL
* @type int(11)
* @default 0
*/

/**
* [TAB_SOCIETE.ID_SOCIETE](TAB_SOCIETE.html#property_ID_SOCIETE).
*
* Agency.
* @property  ID_SOCIETE
* @type int(11)
* @default 0
*/

/**
* [TAB_POLE.ID_POLE](TAB_POLE.html#property_ID_POLE).
*
* Pole.
* @property  ID_POLE
* @type int(11)
* @default 0
*/
