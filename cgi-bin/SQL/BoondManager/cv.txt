/**
* Resume.
*
* @class  TAB_CV
*/


/**
* Unique identifier.
* @property ID_CV
* @type int(11)
*/

/**
* Name.
* @property CV_NAME
* @type varchar(150)
* @default ''
*/

/**
* Content.
* @property CV_TEXT
* @type mediumtext
*/

/**
* [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL).
*
* Resource.
* @property ID_PROFIL
* @type int(11)
* @default 0
*/
