/**
* Technical document.
*
* @class  TAB_DT
*/


/**
* Unique identifier.
* @property ID_DT
* @type int(11)
*/

/**
* Title.
* @property DT_TITRE
* @type varchar(150)
* @default ''
*/

/**
* List of languages.
*
* Each language is described by :
* - language spoken : _cf. key **id** for JSON response **data.settings.languageSpoken** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* - language level : _cf. key **id** for JSON response **data.settings.languageLevel** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
*
* Languages are separated by `#` character.
* @example `"french|mothertongue#english|fluent"`
* @property DT_LANGUES
* @type text
*/

/**
* List of diplomas.
*
* Diplomas are separated by `|` character.
* @example `"Bachelor|Master"`
* @property DT_DIPLOMES
* @type text
*/

/**
* Experience.
*
* _cf. key **id** for JSON response **data.settings.experience** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property DT_EXPERIENCE
* @type tinyint(3)
* @default 0
*/

/**
* Training.
*
* _cf. key **id** for JSON response **data.settings.training** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property DT_FORMATION
* @type varchar(30)
* @default ''
*/

/**
* List of tools.
*
* Each tool is described by :
* - Tool's type : _cf. key **id** for JSON response **data.settings.tool** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* - Tool's level : An integer between `1` and `5`
* - Tool's type & level are listed in this order and separated by '|' character
*
* Tools are separated by `#` character.
* @example `"tool1|2#tool2|4"`
* @property DT_OUTILS
* @type text
*/

/**
* List of activity areas.
*
* Each activity area is described by key **option.id** for JSON response **data.settings.activityArea** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction).
*
* Activity areas are separated by `|` character.
* @example `"category1domain1|category2domain1"`
* @property COMP_APPLICATIONS
* @type text
*/

/**
* List of expertise areas.
*
* Each expertise area is described by key **option.id** for JSON response **data.settings.expertiseArea** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction).
*
* Expertise areas are separated by `|` character.
* @example `"aeronautics|aerospace"`
* @property COMP_INTERVENTIONS
* @type text
*/

/**
* Skills.
* @property COMP_COMPETENCE
* @type text
*/
