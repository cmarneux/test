/**
* Schedule.
*
* @class  TAB_ECHEANCIER
*/


/**
* Unique identifier.
* @property ID_ECHEANCIER
* @type int(11)
*/

/**
* Creation date.
* @property ECH_DATE
* @type date
*/

/**
* Amount excluding tax.
* @property ECH_MONTANTHT
* @type double
* @default 0
*/

/**
* Description.
* @property ECH_DESCRIPTION
* @type varchar(250)
* @default ''
*/

/**
* Rate of turnover to invoice excluding tax*, cf. [TAB_BONDECOMMANDE.BDC_MONTANTHT](TAB_BONDECOMMANDE.html#property_BDC_MONTANTHT)*.
*
* This property can be `null`.
* @property ECH_QUOTA
* @type float(4)
*/

/**
* [TAB_BONDECOMMANDE.ID_BONDECOMMANDE](TAB_BONDECOMMANDE.html#property_ID_BONDECOMMANDE).
*
* Order.
* @property ID_BONDECOMMANDE
* @type int(11)
* @default 0
*/
