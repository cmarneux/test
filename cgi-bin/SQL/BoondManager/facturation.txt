/**
* Invoice.
*
* @class  TAB_FACTURATION
*/


/**
* Unique identifier.
* @property ID_FACTURATION
* @type int(11)
*/

/**
* Reference.
* @property FACT_REF
* @type varchar(250)
* @default ''
*/

/**
* Creation date.
* @property FACT_DATE
* @type date
*/

/**
* Start date.
* @property FACT_DEBUT
* @type date
*/

/**
* End date.
* @property FACT_FIN
* @type date
*/

/**
* Expected payment date.
* @property FACT_DATEREGLEMENTATTENDUE
* @type date
*/

/**
* Performed payment date.
*
* This property can be `null`.
* @property FACT_DATEREGLEMENTRECUE
* @type date
*/

/**
* Discount rate.
* @property FACT_TAUXREMISE
* @type float(4)
* @default 0
*/

/**
* Method of payment.
*
* _cf. key **id** for JSON response **data.settings.paymentMethod** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property FACT_TYPEPAYMENT
* @type tinyint(2)
* @default 0
*/

/**
* Information comments.
* @property FACT_COMMENTAIRE
* @type text
*/

/**
* State.
*
* _cf. key **id** for JSON response **data.settings.state.invoice** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property FACT_ETAT
* @type tinyint(1)
* @default 0
*/

/**
* Currency.
*
* _cf. key **id** for JSON response **data.settings.currency** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property FACT_DEVISE
* @type tinyint(2)
* @default 0
*/

/**
* Exchange rate.
* @property FACT_CHANGE
* @type float(4)
* @default 1
*/

/**
* Agency's currency.
*
* _cf. key **id** for JSON response **data.settings.currency** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property FACT_DEVISEAGENCE
* @type tinyint(2)
* @default 0
*/

/**
* Agency's exchange rate.
* @property FACT_CHANGEAGENCE
* @type float(4)
* @default 1
*/

/**
* Enable/disable to show information comments on PDF :
* - `1` : Enable
* - `0` : Disable
* @property FACT_SHOWCOMMENTAIRE
* @type tinyint(1)
* @default 0
*/

/**
* Enable/disable this invoice to be a credit note :
* - `1` : Enable
* - `0` : Disable
* @property FACT_AVOIR
* @type tinyint(1)
* @default 0
*/

/**
* Enable/disable to close this invoice :
* - `1` : Enable
* - `0` : Disable
* @property FACT_CLOTURE
* @type tinyint(1)
* @default 0
*/

/**
* [TAB_BONDECOMMANDE.ID_BONDECOMMANDE](TAB_BONDECOMMANDE.html#property_ID_BONDECOMMANDE).
*
* Order.
* @property ID_BONDECOMMANDE
* @type int(11)
* @default 0
*/

/**
* [TAB_ECHEANCIER.ID_ECHEANCIER](TAB_ECHEANCIER.html#property_ID_ECHEANCIER).
*
* Schedule.
* @property ID_ECHEANCIER
* @type int(11)
* @default 0
*/
