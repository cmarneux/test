/**
* Resource/Candidate's administrative file.
*
* @class  TAB_FILE
*/


/**
* Unique identifier.
* @property ID_FILE
* @type int(11)
*/

/**
* Name.
* @property FILE_NAME
* @type varchar(150)
* @default ''
*/

/**
* [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL).
*
* Resource.
* @property ID_PROFIL
* @type int(11)
* @default 0
*/
