/**
* Influencers.
*
* @class  TAB_INFLUENCER
*/


/**
* [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL).
*
* Main manager.
* @property ID_PROFIL
* @type int(11)
* @default 0
*/

/**
* Entity's identifier :
* - Company : if [TAB_INFLUENCER.ITEM_TYPE](TAB_INFLUENCER.html#property_ITEM_TYPE) is `0`*, cf. [TAB_CRMSOCIETE.ID_CRMSOCIETE](TAB_CRMSOCIETE.html#property_ID_CRMSOCIETE)*
* - Contact : if [TAB_INFLUENCER.ITEM_TYPE](TAB_INFLUENCER.html#property_ITEM_TYPE) is `10`*, cf. [TAB_CRMCONTACT.ID_CRMCONTACT](TAB_CRMCONTACT.html#property_ID_CRMCONTACT)*
* @property ID_ITEM
* @type int(11)
* @default 0
*/

/**
* Entity's type :
* - `0` : Company
* - `10` : Contact
* @property ITEM_TYPE
* @type tinyint(1)
* @default 0
*/
