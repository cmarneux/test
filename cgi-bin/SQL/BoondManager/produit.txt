/**
* Product.
*
* @class  TAB_PRODUIT
*/


/**
* Unique identifier.
* @property ID_PRODUIT
* @type int(11)
*/

/**
* Name.
* @property PRODUIT_NOM
* @type varchar(150)
* @default ''
*/

/**
* Reference.
* @property PRODUIT_REF
* @type varchar(250)
* @default ''
*/

/**
* Subscription's type.
*
* _cf. key **id** for JSON response **data.settings.typeOf.subscription** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property PRODUIT_TYPE
* @type tinyint(1)
* @default 0
*/

/**
* State.
*
* _cf. key **id** for JSON response **data.settings.state.product** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property PRODUIT_ETAT
* @type tinyint(2)
* @default 1
*/

/**
* Description.
* @property PRODUIT_DESCRIPTION
* @type text
*/

/**
* Price excluding tax.
* @property PRODUIT_TARIFHT
* @type double
* @default 0
*/

/**
* Tax rate.
*
* _cf. JSON response **data.settings.taxRate** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_.
* @property PRODUIT_TAUXTVA
* @type float(4)
* @default 0
*/

/**
* Currency.
*
* _cf. key **id** for JSON response **data.settings.currency** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property PRODUIT_DEVISE
* @type tinyint(2)
* @default 0
*/

/**
* Exchange rate.
* @property PRODUIT_CHANGE
* @type float(4)
* @default 1
*/

/**
* Agency's currency.
*
* _cf. key **id** for JSON response **data.settings.currency** of [API /application/dictionary](https://support.boondmanager.com/hc/fr/articles/209465606-Introduction)_
* @property PRODUIT_DEVISEAGENCE
* @type tinyint(2)
* @default 0
*/

/**
* Agency's exchange rate.
* @property PRODUIT_CHANGEAGENCE
* @type float(4)
* @default 1
*/

/**
* Update date.
* @property PRODUIT_DATEUPDATE
* @type datetime
*/

/**
* [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL).
*
* Main manager.
* @property ID_PROFIL
* @type int(11)
* @default 0
*/

/**
* [TAB_SOCIETE.ID_SOCIETE](TAB_SOCIETE.html#property_ID_SOCIETE).
*
* Agency.
* @property ID_SOCIETE
* @type int(11)
* @default 0
*/

/**
* [TAB_POLE.ID_POLE](TAB_POLE.html#property_ID_POLE).
*
* Pole.
* @property ID_POLE
* @type int(11)
* @default 0
*/
