/**
* Entity's bank details (agency, resource, company).
*
* @class  TAB_RIB
*/


/**
* Unique identifier.
* @property   ID_RIB
* @type int(11)
*/

/**
* Description.
* @property   RIB_DESCRIPTION
* @type text
*/

/**
* IBAN.
* @property   RIB_IBAN
* @type char(34)
* @default ''
*/

/**
* SWIFT/BIC.
* @property   RIB_BIC
* @type char(11)
* @default ''
*/

/**
* Entity's type :
* - `0` : Agency
* - `1` : Resource
* - `2` : Company
* @property   RIB_TYPE
* @type tinyint(1)
* @default 0
*/

/**
* Entity's identifier :
* - Agency : if [TAB_RIB.RIB_TYPE](TAB_RIB.html#property_RIB_TYPE) is `0`*, cf. [TAB_SOCIETE.ID_SOCIETE](TAB_SOCIETE.html#property_ID_LISTEFRAIS)*
* - Resource : if [TAB_RIB.RIB_TYPE](TAB_RIB.html#property_RIB_TYPE) is `1`*, cf. [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL)*
* - Company : if [TAB_RIB.RIB_TYPE](TAB_RIB.html#property_RIB_TYPE) is `2`*, cf. [TAB_CRMSOCIETE.ID_CRMSOCIETE](TAB_CRMSOCIETE.html#property_ID_CRMSOCIETE)*
* @property   ID_PARENT
* @type int(11)
* @default 0
*/
