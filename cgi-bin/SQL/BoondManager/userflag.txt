/**
* User's flag.
*
* @class  TAB_USERFLAG
*/


/**
* Unique identifier.
* @property   ID_USERFLAG
* @type int(11)
*/

/**
* Title.
* @property   FLAG_LIBELLE
* @type varchar(100)
* @default ''
*/

/**
* [TAB_USER.ID_USER](TAB_USER.html#property_ID_USER).
*
* User.
* @property   ID_USER
* @type int(11)
* @default 0
*/
