/**
* Entity's social network.
*
* @class  TAB_WEBSOCIAL
*/


/**
* Unique identifier.
* @property ID_WEBSOCIAL
* @type int(11)
*/

/**
* Type :
* - `0` : Viadeo
* - `1` : Linkedin
* - `2` : Facebook
* - `3` : Twitter
* @property   WEBSOCIAL_NETWORK
* @type tinyint(2)
* @default 0
*/

/**
* Social network's url.
* @property   WEBSOCIAL_URL
* @type varchar(250)
* @default ''
*/

/**
* Entity's type :
* - `0` : Candidate
* - `1` : Resource
* - `2` : Contact
* - `3` : Company
* @property   WEBSOCIAL_TYPE
* @type tinyint(1)
* @default 0
*/

/**
* Entity's identifier :
* - Candidate : if [TAB_WEBSOCIAL.WEBSOCIAL_TYPE](TAB_WEBSOCIAL.html#property_WEBSOCIAL_TYPE) is `0`*, cf. [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL)*
* - Resource : if [TAB_WEBSOCIAL.WEBSOCIAL_TYPE](TAB_WEBSOCIAL.html#property_WEBSOCIAL_TYPE) is `1`*, cf. [TAB_PROFIL.ID_PROFIL](TAB_PROFIL.html#property_ID_PROFIL)*
* - Contact : if [TAB_WEBSOCIAL.WEBSOCIAL_TYPE](TAB_WEBSOCIAL.html#property_WEBSOCIAL_TYPE) is `2`*, cf. [TAB_CRMCONTACT.ID_CRMCONTACT](TAB_CRMCONTACT.html#property_ID_CRMCONTACT)*
* - Company : if [TAB_WEBSOCIAL.WEBSOCIAL_TYPE](TAB_WEBSOCIAL.html#property_WEBSOCIAL_TYPE) is `3`*, cf. [TAB_CRMSOCIETE.ID_CRMSOCIETE](TAB_CRMSOCIETE.html#property_ID_CRMSOCIETE)*
* @property   ID_PARENT
* @type int(11)
* @default 0
*/
