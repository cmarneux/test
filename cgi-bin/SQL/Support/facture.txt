/**
* Invoice.
*
* @class  TAB_FACTURE
*/


/**
* Unique identifier.
* @property ID_FACTURE
* @type int(11)
*/

/**
* Creation date.
* @property   FACT_DATE
* @type date
*/

/**
* Amount excluding tax.
* @property   FACT_MONTANTHT
* @type float(4)
* @default 0
*/

/**
* Tax rate.
* @property   FACT_TAUXTVA
* @type float(4)150)
* @default 19.6
*/

/**
* Tax amount.
* @property   FACT_MONTANTTVA
* @type float(4)
* @default 0
*/

/**
* Reference.
* @property   FACT_REFERENCE
* @type varchar(150)
* @default ''
*/

/**
* Company Name.
* @property   FACT_SOCIETE
* @type varchar(100)
* @default ''
*/

/**
* V.A.T number.
* @property   FACT_TVA
* @type varchar(13)
* @default ''
*/

/**
* Address.
* @property   FACT_ADR
* @type varchar(250)
* @default ''
*/

/**
* Postcode.
* @property   FACT_CP
* @type char(10)
* @default ''
*/

/**
* Town.
* @property   FACT_VILLE
* @type varchar(100)
* @default ''
*/

/**
* Country.
* @property   FACT_PAYS
* @type varchar(100)
* @default 'France'
*/

/**
* [TAB_COMMANDE.ID_COMMANDE](TAB_COMMANDE.html#property_ID_COMMANDE).
*
* Order.
* @property ID_COMMANDE
* @type int(11)
* @default 0
*/
