/**
* Node server.
*
* @class  TAB_SERVEURNODE
*/


/**
* Unique identifier.
* @property ID_SERVEURNODE
* @type int(11)
*/

/**
* Absolute URL.
* @property   NODE_URL
* @type varchar(100)
* @default ''
*/

/**
* Relative URL.
* @property   NODE_SERVER
* @type varchar(100)
* @default ''
*/

/**
* Do BoondManager use this server by default on a new customer ?
* - `1` : Yes
* - `0` : No
* @property   NODE_DEFAULT
* @type tinyint(1)
* @default ''
*/
