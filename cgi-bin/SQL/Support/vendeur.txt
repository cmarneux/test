/**
* Apps vendor.
*
* @class  TAB_VENDEUR
*/

/**
* Unique identifier.
* @property ID_VENDEUR
* @type int(11)
*/

/**
* Name.
* @property   VENDEUR_NOM
* @type varchar(250)
* @default ''
*/

/**
* Description.
* @property   VENDEUR_DESCRIPTION
* @type text
*/

/**
* Phone.
* @property   VENDEUR_TEL
* @type char(20)
* @default ''
*/

/**
* Address.
* @property   VENDEUR_ADR
* @type varchar(250)
* @default ''
*/

/**
* Postcode.
* @property   VENDEUR_CP
* @type char(10)
* @default ''
*/

/**
* Town.
* @property   VENDEUR_VILLE
* @type varchar(100)
* @default ''
*/

/**
* Country.
* @property   VENDEUR_PAYS
* @type varchar(100)
* @default 'France'
*/

/**
* Website.
* @property   VENDEUR_WEB
* @type varchar(100)
* @default ''
*/

/**
* Email.
* @property   VENDEUR_EMAIL
* @type varchar(100)
* @default ''
*/

/**
* Subscription's date & time.
*
* This property can be `null`.
* @property   VENDEUR_INSCRIPTION
* @type datetime
*/


/**
* [TAB_CLIENT.ID_CLIENT](TAB_CLIENT.html#property_ID_CLIENT).
*
* Customer.
* @property ID_CLIENT
* @type int(11)
* @default 0
*/
