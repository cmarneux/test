<?php
/**
 * SolR.php
 *
 * Script exécuté lors d'une demande de synchronisation à la base de données SolR.
 *
 * ATTENTION : Il ne peut y avoir qu'un script tournant simultanément par serveur SolR et par core/module !
 *
 * - $_SERVER['argv'][1] = Ip du serveur SolR
 * - $_SERVER['argv'][2] = BDD du client
 * - $_SERVER['argv'][3] = Module concerné
 * - $_SERVER['argv'][4] = Date dernier import
 * - $_SERVER['argv'][5] = Heure dernier import
 * - $_SERVER['argv'][6] = Mode de BoondManager
 * - $_SERVER['argv'][7] = Id du module concerné
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

use BoondManager\Services\BM;
use BoondManager\Lib\SolR;

define('WISH_SOLRQUEUE', 3000);
if(isset($_SERVER['argv'][1], $_SERVER['argv'][2], $_SERVER['argv'][3], $_SERVER['argv'][4], $_SERVER['argv'][5], $_SERVER['argv'][6], $_SERVER['argv'][7])) {
	list($script, $solrIp, $customerCode, $moduleName, $lastImportDate, $lastImportTime, $mode, $moduleId) = $_SERVER['argv'];

	set_time_limit(0); //Ce script n'a pas de durée limite d'exécution

	$scriptPath = realpath(dirname(__FILE__));

	/** @var \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base).en base de données. */
	//Autoloader composer
	require($scriptPath.'/../../vendor/autoload.php');
	$f3 = \Base::instance();

	//Main application folder
	$f3->set('MAIN_ROOTPATH', $scriptPath.'/../..');

	//Répertoire racine de l'application
	$f3->set('MAIN_APPPATH', $scriptPath.'/../../app');

	// Chargement des fichiers de configuration globaux ?
	$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/boondmanager.ini');
	\Base::instance()->set('PLUGINS', \Base::instance()->get('MAIN_APPPATH').'/');

	BM::initScript($mode);

	//On exécute ce script uniquement si le mode SolR est activé
	if($f3->get('BMSOLR.ENABLED')) {
		$solrDirectory = $f3->get('MAIN_ROOTPATH').'/cgi-bin/SolR/'.$solrIp;
		$solrLock = $solrDirectory.'/'.$moduleName.'.pid';
		if(!file_exists($solrDirectory)) {
			exec('mkdir -p '.$solrDirectory);
			exec('chmod -R 777 '.$solrDirectory);
		}

		$queue = msg_get_queue(WISH_SOLRQUEUE + ord(substr($solrIp, -1)) + $moduleId);
 		msg_send($queue, 1, $customerCode.' '.$lastImportDate.' '.$lastImportTime);
		//Si le script ne tourne pas pour ce serveur SolR alors on l'exécute, sinon on termine le script afin de ne pas en lancer plusieurs en parallèle
		if(!file_exists($solrLock)) {
			file_put_contents($solrLock, getmypid());//On construit le verrou
			try {
				$msg_type = NULL;
				$msg = NULL;
				while(msg_receive($queue, $desiredMsgType, $msg_type, 16384, $msg, true, MSG_IPC_NOWAIT)) {
					list($msgCustomerCode, $msgImportDate, $msgImportTime) = explode(' ', $msg);//Client Date Heure
					while(!SolR::instance()->status($solrIp, $moduleName, $msgCustomerCode)) sleep(2);	//Tant que le serveur SolR est occupé, le script se rendort 2 secondes.

					$tabStat = msg_stat_queue($queue);
					$logger = new \Log('solr.script.log');
					$logger->write($msgCustomerCode.' - '.$solrIp.' - '.$moduleName.' - '.$msgImportDate.' '.$msgImportTime.' - '.$tabStat['msg_qnum'].PHP_EOL);

					SolR::instance()->dataimport($solrIp, $msgCustomerCode, $moduleName, $msgImportDate.' '.$msgImportTime);//On lance la synchronisation
				}
			} catch(Exception $e) {}
			unlink($solrLock);//On supprime le verrou
		}
	}
}
