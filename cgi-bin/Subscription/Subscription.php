<?php
/**
 * ATTENTION : Il ne peut y avoir qu'un script tournant simultanément pour mettre à jour les données de l'abonnement
 *
 * - $_SERVER['argv'][1] = GRPCONF_WEB
 * - $_SERVER['argv'][2] = Mode de BoondManager
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */

use BoondManager\Services\BM;
use BoondManager\Lib\GED;
use BoondManager\Databases\Local;
use BoondManager\Databases\BoondManager;

define('WISH_SUBSCRIPTIONQUEUE', 2000);
if(isset($_SERVER['argv'][1], $_SERVER['argv'][2])) {
	list($script, $customerCode, $mode) = $_SERVER['argv'];

	set_time_limit(0); //Ce script n'a pas de durée limite d'exécution

	$scriptPath = realpath(dirname(__FILE__));

	/** @var \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base).en base de données. */
	//Autoloader composer
	require($scriptPath.'/../../vendor/autoload.php');
	$f3 = \Base::instance();

	//Main application folder
	$f3->set('MAIN_ROOTPATH', $scriptPath.'/../..');

	//Répertoire racine de l'application
	$f3->set('MAIN_APPPATH', $scriptPath.'/../../app');

	// Chargement des fichiers de configuration globaux ?
	$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/boondmanager.ini');
	\Base::instance()->set('PLUGINS', \Base::instance()->get('MAIN_APPPATH').'/');

	BM::initScript($mode);

	$subscriptionDirectory = $f3->get('MAIN_ROOTPATH').'/cgi-bin/Subscription';
	$subscriptionLock = $subscriptionDirectory.'/Subscription.pid';

	$queue = msg_get_queue(WISH_SUBSCRIPTIONQUEUE);
	msg_send($queue, 1, $customerCode);
	//Si le script ne tourne pas pour ce serveur alors on l'exécute, sinon on termine le script afin de ne pas en lancer plusieurs en parallèle
	if(!file_exists($subscriptionLock)) {
		file_put_contents($subscriptionLock, getmypid());//On construit le verrou
		try {
			$ged = new GED();
			$msg_type = NULL;
			$msgCustomerCode = NULL;
			while(msg_receive($queue, 1, $msg_type, 16384, $msgCustomerCode, true, MSG_IPC_NOWAIT)) {
				BM::loadCustomerInterface($msgCustomerCode);
				if(BM::isCustomerInterfaceActive()) {
					$dbLocalSub = new Local\Subscription();
        			$localSubscription = $dbLocalSub->getGroupSubscription();

        			$dbBoondSub = new BoondManager\Subscription();
        			$boondSubscription = $dbBoondSub->getSubscriptionFromCustomerCode($msgCustomerCode);

			        if($boondSubscription && $localSubscription) {
						$size = $ged->getDirectorySize(-1);

						$dbLocalSub->updateGroupSubscription(array('AB_STORAGEUSE' => $size));
						$dbBoondSub->updateSubscriptionFromClient(array('AB_STORAGEUSE' => $size), $boondSubscription['ID_CLIENT']);

						$tabStat = msg_stat_queue($queue);

						$logger = new \Log('subscription.script.log');
						$logger->write($msgCustomerCode.' - '.$tabStat['msg_qnum'].PHP_EOL);
				    }
				}
				BM::clearCustomerInterface();
			}
		} catch(Exception $e) {}
		unlink($subscriptionLock);//On supprime le verrou
	}
}
