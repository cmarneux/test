#!/bin/bash

#-----------------------------------------------------------
#----    Script de création du Dossier Web du projet    ----
#-----------------------------------------------------------
dir_prj=`echo "$0" | sed -e "s/[^\/]*$//"`../../..
dir_web="/home/wishuser"
dir_data="/home/wishuser/Web/BoondManager_Data"
group="wishuser"
user="www-data"
hardlinks=0
reset=""
sedtype=0
ged="GEDA"
WISH_WEBMODE="1"
WISH_SOLRMODE="false"
WISH_SOLRUSER=""
WISH_SOLRPWD=""
WISH_SOLRPORT=""
WISH_DBSERVER=""
WISH_DBPORT=""
WISH_DBUSER=""
WISH_DBPWD=""
WISH_DBDATABASE=""
WISH_CLUSTERUSER=""
WISH_CLUSTERPWD=""
WISH_CLUSTERSERVERS=""
WISH_CLUSTERPROTOCOL=""
WISH_CLUSTERROOTDIRECTORY=""
WISH_NODEMODE=""
WISH_NODEURL=""
WISH_NODESERVER=""
WISH_NODESECRET=""
WISH_SITEURL=""
WISH_BASEURL=""
WISH_DBEXECPATH=""
WISH_DBROOTUSER=""
WISH_DBROOTPWD=""
WISH_APPURL=""
WISH_EMAILCONTACT=""
WISH_EMAILHOTLINE=""
WISH_ZENDESKUSER=""
WISH_ZENDESKPASS=""
WISH_ZENDESKJWT=""
WISH_SMTPHOST=""
WISH_SMTPPORT=""
WISH_SMTPSCHEME=""
WISH_SMTPLOGIN=""
WISH_SMTPPWD=""

#On charge le fichier de librairie Bash
source $dir_prj/Wish/FrameWork/cgi-bin/wish_library.sh
set_parameters $*	#On configure les variables par défaut

#On construit les dossiers du projet
mkdir -p $dir_web/logs
mkdir -p $dir_web/tmp
mkdir -p $dir_web/cgi-bin
mkdir -p $dir_web/app/boondmanager/configs
mkdir -p $dir_web/app/boondmanager/dict

#On installe les modules standards
install_mvc $dir_prj/BoondManager/Back_End $dir_web $hardlinks

if [ "$reset" = "1" ]; then
	rm -Rf $dir_data/vendor/*
fi

#On construit tous les liens symboliques
ln -nFs $dir_data/cgi-bin/share $dir_web/cgi-bin/share
ln -nFs $dir_data/cgi-bin/catdoc $dir_web/cgi-bin/catdoc
ln -nFs $dir_data/cgi-bin/pdftotext $dir_web/cgi-bin/pdftotext
ln -nFs $dir_data/app/boondmanager/configs/_clients $dir_web/app/boondmanager/configs/_clients
ln -nFs $dir_data/app/boondmanager/dict/_clients $dir_web/app/boondmanager/dict/_clients
ln -nFs $dir_data/UI_Sessions $dir_web/sessions
ln -nFs $dir_data/vendor $dir_web/vendor

#On installe les plugins via composer
composer install -d $dir_web

#On modifie boondmanager.conf.ini
config_Web=$dir_web/app/boondmanager/configs
mv $config_Web/boondmanager.conf.ini $config_Web/boondmanager.ini

if [ "$sedtype" = "1" ]; then backup=" .back"; else backup=""; fi
sed -i$backup "s|WISH_WEBMODE|$WISH_WEBMODE|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SITEURL|$WISH_SITEURL|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SOLRMODE|$WISH_SOLRMODE|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SOLRUSER|$WISH_SOLRUSER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SOLRPWD|$WISH_SOLRPWD|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SOLRPORT|$WISH_SOLRPORT|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBSERVER|$WISH_DBSERVER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBPORT|$WISH_DBPORT|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBUSER|$WISH_DBUSER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBPWD|$WISH_DBPWD|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBDATABASE|$WISH_DBDATABASE|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_CLUSTERUSER|$WISH_CLUSTERUSER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_CLUSTERPWD|$WISH_CLUSTERPWD|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_CLUSTERSERVERS|$WISH_CLUSTERSERVERS|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_CLUSTERPROTOCOL|$WISH_CLUSTERPROTOCOL|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_CLUSTERROOTDIRECTORY|$WISH_CLUSTERROOTDIRECTORY|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_NODEMODE|$WISH_NODEMODE|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_NODEURL|$WISH_NODEURL|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_NODESERVER|$WISH_NODESERVER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_NODESECRET|$WISH_NODESECRET|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_EMAILCONTACT|$WISH_EMAILCONTACT|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_EMAILHOTLINE|$WISH_EMAILHOTLINE|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_ZENDESKUSER|$WISH_ZENDESKUSER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_ZENDESKPASS|$WISH_ZENDESKPASS|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_ZENDESKJWT|$WISH_ZENDESKJWT|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SMTPHOST|$WISH_SMTPHOST|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SMTPPORT|$WISH_SMTPPORT|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SMTPSCHEME|$WISH_SMTPSCHEME|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SMTPLOGIN|$WISH_SMTPLOGIN|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_SMTPPWD|$WISH_SMTPPWD|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBROOTUSER|$WISH_DBROOTUSER|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBROOTPWD|$WISH_DBROOTPWD|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_DBEXECPATH|$WISH_DBEXECPATH|g" $config_Web/boondmanager.ini
sed -i$backup "s|WISH_APPURL|$WISH_APPURL|g" $config_Web/boondmanager.ini

if [ "$sedtype" = "1" ]; then
	rm -f $config_Web/boondmanager.ini.back
fi

old="$IFS"	#Sauvegarde de la valeur du IFS et modification du IFS
IFS=":"	#Définition du caractère de séparation
tab_ged=($ged) #On créé un tableau contenant le paramètre et sa valeur
IFS="$old"	#Restauration ud IFS
for (( j=0 ; j<${#tab_ged[*]} ; j++)); do
	ln -nFs $dir_data/${tab_ged[$j]}/PAS $dir_web/${tab_ged[$j]}
done

#On met à jour les droits des dossiers du projet
touch $dir_web/www/token
chmod -R 775 $dir_web
chown -R $user:$group $dir_web
chmod -R 775 $dir_data
chown -R $user:$group $dir_data
exit 0
