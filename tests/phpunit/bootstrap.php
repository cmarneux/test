<?php
/**
 * autoloader.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

ini_set("session.save_path", realpath(dirname(__FILE__).'/../../sessions')); //Sessions folder storage
ini_set("auto_detect_line_endings", 1); //Automatic end of lines detection in text files (Necessary for CSV reading)
ini_set("expose_php", "off");
ini_set("session.gc_probability", 1);   //Deletion of old sessions files
ini_set("soap.wsdl_cache_enabled", 1);  //WSDL file definition cache activation
ini_set('default_socket_timeout', 25);  //Socket timeout : 25 seconds

// autoloader composer
require(__DIR__.'/../../vendor/autoload.php');

$f3 = \Base::instance();

if($f3->exists('SERVER.HTTP_USER_AGENT')) ini_set('user_agent', $f3->get('SERVER.HTTP_USER_AGENT')); //User agent definition (Necessary to avoid FOPEN errors)

//Main application folder
$f3->set('MAIN_ROOTPATH', realpath(dirname(__FILE__).'/../..'));
//F3 folder
$f3->set('MAIN_APPPATH', realpath(dirname(__FILE__).'/../../app'));

//Main configuration files loading
$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/boondmanager.ini');
$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/routes.ini');

$f3->PLUGINS = $f3->get('MAIN_APPPATH').'/';

// On ecrase la definition de la locale pour le formatage des nombres (sinon bug de FILTER_VALIDATE_FLOAT)
setlocale(LC_NUMERIC, ["en_US", "en_US.UTF_8", "en", "en.UTF_8", "C", "C.UTF-8"]);

use BoondManager\Services\BM;

//Development mode management
if(BM::isDevelopmentMode()){
	$f3->set('BOONDMANAGER.DEVELOPER', $f3->get('SERVER.HTTP_HOST'));
	$tabURI = explode('/', $f3->get('SERVER.REQUEST_URI'));
	$f3->set('DEBUG', 3);
}

BM::initDictionaries();
BM::setCustomAutoloader();
