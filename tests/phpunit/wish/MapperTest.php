<?php
/**
 * MapperTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish;

use PHPUnit\Framework\TestCase;
use Tests\PhpUnit\Wish\TestData\SimpleJSONApiModel;
use Tests\PhpUnit\Wish\TestData\SimpleJSONModel;
use Wish\Mapper;

class MapperTest extends TestCase{

	/** @test */
	public function check_database_array_to_model()
	{
		$model = new SimpleJSONModel();
		Mapper::databaseArrayToModel([
			'PROFIL_NOM' => 'Bob',
			'PROFIL_PRENOM' => 'Babette',
			'PROFIL_TYPE' => 1
		], $model);

		$this->assertEquals('Bob', $model->lastName);
		$this->assertEquals('Babette', $model->firstName);
		$this->assertEquals('modo', $model->typeOf);
	}

	/** @test */
	public function check_model_to_database_array()
	{
		$model = new SimpleJSONModel();
		$model->fromArray([
			'lastName' => 'Bob',
			'firstName' => 'Babette',
			'typeOf' => 'admin'
		]);

		$data = Mapper::modelToDatabaseArray($model);

		$this->assertEquals([
			'PROFIL_NOM' => 'Bob',
			'PROFIL_PRENOM' => 'Babette',
			'PROFIL_TYPE' => 0
		], $data);
	}

	/** @test */
	public function check_serialized_array()
	{
		$model = new SimpleJSONApiModel();
		$data = [
			'CLOTHES' => 'jean|shirt|shoes'
		];

		Mapper::databaseArrayToModel($data, $model);

		$this->assertEquals(['jean', 'shirt', 'shoes'], $model->clothes);

		$data2 = Mapper::modelToDatabaseArray($model);

		$this->assertEquals($data, $data2);
	}

	/** @test */
	public function check_serialize_callback()
	{
		$model = new SimpleJSONApiModel();
		$data = [
			'SA' => 'test|1#test2|23'
		];

		Mapper::databaseArrayToModel($data, $model);

		$this->assertEquals([['test',1], ['test2', 23]], $model->sa);

		$data2 = Mapper::modelToDatabaseArray($model);

		$this->assertEquals($data, $data2);
	}
}
