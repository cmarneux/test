<?php
/**
 * ToolsTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */
namespace Tests\PhpUnit\Wish;

use Wish\Models\Model;
use PHPUnit\Framework\TestCase;
use Wish\Tools;

class ToolsTest extends TestCase {

	/** @test */
	public function is_equal_array(){
		$this->assertEquals(true, Tools::is_equal_array([1, 2, 3], [1, 2, 3]), 'test same array');
		$this->assertEquals(true, Tools::is_equal_array([1, 2, 3], [2, 1, 3]), 'test same array (shuffled');
		$this->assertEquals(true, Tools::is_equal_array([], []), 'test empty arrays');
		$this->assertEquals(true, ! Tools::is_equal_array([1, 2, 3], [2, 1, 4]), 'test different array');
		$this->assertEquals(true, ! Tools::is_equal_array([1, 2, 3, 4], [1, 2, 3]), 'test array different sizes ');
		$this->assertEquals(true, ! Tools::is_equal_array([1, 2, 3], [1, 2, 3, 4]), 'test array different sizes');
	}

	/** @test */
	public function fields_to_array()
	{

		$a     = new \stdClass();
		$b     = new \stdClass();
		$a->a  = 1;
		$a->b  = 2;
		$b->a  = 3;
		$b->b  = 2;
		$b->c  = 3;
		$array = [$a, $b];
		$this->assertEquals(true, Tools::is_equal_array(Tools::getFieldsToArray($array, 'a'), [1, 3]), 'test transformation object array to simple array');
		$this->assertEquals(true, Tools::is_equal_array(Tools::getFieldsToArray($array, 'b'), [2, 2]), 'test transformation object array to simple array');

		$array = [
			new Model(['a' => 1, 'b' => 2]),
			new Model(['a' => 3, 'b' => 2, 'c' => 3]),
		];
		$this->assertEquals(true, Tools::is_equal_array(Tools::getFieldsToArray($array, 'a'), [1, 3]), 'test transformation object array to simple array on RowObject');
		$this->assertEquals(true, Tools::is_equal_array(Tools::getFieldsToArray($array, 'b'), [2, 2]), 'test transformation object array to simple array  on RowObject');
	}
}
