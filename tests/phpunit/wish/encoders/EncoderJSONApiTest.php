<?php
/**
 * EncoderJSONApiTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Encoders;

use PHPUnit\Framework\TestCase;
use Tests\PhpUnit\Wish\TestData\Address;
use Tests\PhpUnit\Wish\TestData\SimpleJSONApiModel;
use Wish\Encoders\JsonAPI;

class EncoderJSONApiTest extends TestCase{

	private function buildModel(){
		$model = new SimpleJSONApiModel([
			'id' => '1',
			'lastName' => 'Bob',
			'firstName' => 'Bobette',
			'age' => 21,
			'money' => 1000.5,
			'coins' => [5, 5, 100, 100, 100, 0.5, 500, 200, 50, 20, 10, 10],
			'vip' => true,
			'typeOf' => 'admin'
		]);

		$model->address = new Address([
			'city' => 'Paris',
			'country' => 'France',
			'street' => '1 rue de la paix'
		]);

		$model->friend = new SimpleJSONApiModel([
			'id' => '2',
			'lastName' => 'Alice',
			'firstName' => 'Aleyde',
		]);
		return $model;
	}

	/** @test */
	public function check_encoder()
	{
		$model = $this->buildModel();
		$data = JsonAPI::instance()->serialize($model);
		$this->assertEquals([
			'data' => [
			'id' => '1',
			'type' => 'user',
			'attributes' => [
				'lastName' => 'Bob',
				'firstName' => 'Bobette',
				'age' => 21,
				'money' => 1000.5,
				'coins' => [5, 5, 100, 100, 100, 0.5, 500, 200, 50, 20, 10, 10],
				'vip' => true,
				'typeOf' => 'admin',
				'address' => [
					'city' => 'Paris',
					'country' => 'France',
					'street' => '1 rue de la paix',
				]
			],
			'relationships' => [
				'friend' => [
					'data' => [
						'id' => '2',
						'type' => 'user'
					]
				]
			]
			],
			'included' => [
				[
					'id' => '2',
					'type' => 'user',
					'attributes' => [
						'lastName' => 'Alice',
						'firstName' => 'Aleyde',
					]
				]
			]
		], $data);
	}

	/** @test */
	public function check_encoder_types()
	{
		$model = $this->buildModel();
		$data = JsonAPI::instance()->serialize($model);

		$this->assertInternalType('string', $data['data']['id']);
		$this->assertInternalType('string', $data['data']['type']);
		$this->assertInternalType('int', $data['data']['attributes']['age']);
		$this->assertInternalType('string', $data['data']['attributes']['lastName']);
		$this->assertInternalType('float', $data['data']['attributes']['money']);
		$this->assertInternalType('array', $data['data']['attributes']['coins']);
		$this->assertInternalType('boolean', $data['data']['attributes']['vip']);
	}

	/** @test */
	public function check_encoder_type_cast()
	{
		$model = $this->buildModel();

		$model->id = 1;
		$model->money = '1000.5';
		$model->vip = 'yes';
		$model->age = '23';
		$model->lastName = 12;

		$data = JsonAPI::instance()->serialize($model);

		$this->assertInternalType('string', $data['data']['id']);
		$this->assertInternalType('string', $data['data']['type']);
		$this->assertInternalType('int', $data['data']['attributes']['age']);
		$this->assertInternalType('string', $data['data']['attributes']['lastName']);
		$this->assertInternalType('float', $data['data']['attributes']['money']);
		$this->assertInternalType('boolean', $data['data']['attributes']['vip']);

		$this->assertEquals(23, $data['data']['attributes']['age']);
		$this->assertEquals('12', $data['data']['attributes']['lastName']);
		$this->assertEquals(true, $data['data']['attributes']['vip']);
	}

	/** @test */
	public function check_encoder_include_reduce()
	{

		$model = $this->buildModel();
		$thirdFriend = new SimpleJSONApiModel([
			'id' => '3',
			'lastName' => 'Eve',
			'firstName' => 'Eeeeve'
		]);
		// 2 and 3 are mutual friends (cyclic relationship)
		$model->friend->friend = $thirdFriend;
		$thirdFriend->friend = $model->friend;

		$data = JsonAPI::instance()->serialize($model);

		$this->assertEquals(2, count($data['included']));
	}


	/** test // TODO */
	public function check_remove_null_value()
	{
		$model = $this->buildModel();
		$model->lastName = null;
		$model->coins = [];

		$data = JsonAPI::instance()->serialize($model);

		$this->assertArrayNotHasKey('lastName', $data['data']['attributes']);
		$this->assertArrayHasKey('coins', $data['data']['attributes']);
		$this->assertEmpty($data['data']['attributes']['coins']);
		$this->assertInternalType('array', $data['data']['attributes']['coins']);
	}
}
