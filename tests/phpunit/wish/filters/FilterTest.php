<?php
/**
 * FilterTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters;

use PHPUnit\Framework\TestCase;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputString;
use Wish\Filters\Inputs\InputValue;

class FilterTest extends TestCase {
	/**
	 * @return AbstractFilters
	 */
	private function createEmptyFilter(){
		return $this->getMockForAbstractClass(AbstractFilters::class);
	}

	/** @test */
	public function check_filter_validation()
	{
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputInt('test'))->setMin(0)
		]);

		$filter->setData([
			'test' => 1,
			'autre' => 10
		]);

		$filter->filter();

		$this->assertTrue($filter->isValid());
		$this->assertArrayHasKey('test', $filter->getValue());
		$this->assertArrayNotHasKey('autre', $filter->getValue());
	}

	/** @test */
	public function check_filter_wrong_input(){
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputInt('test'))->setMin(0)->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE)
		]);

		$filter->setData([
			'test' => -1,
			'autre' => 10
		]);


		$this->assertFalse($filter->isValid());
		$this->assertArrayHasKey('test', $filter->getValue());
		$this->assertEquals(['test'=>null], $filter->getValue());
	}

	/** @test */
	public function check_filter_ignored_wrong_input(){
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputInt('test'))->setMin(0)
		]);

		$filter->setData([
			'test' => -1,
			'autre' => 10
		]);


		$this->assertTrue($filter->isValid());
		$this->assertArrayNotHasKey('test', $filter->getValue());
	}

	/** @test */
	public function check_filter_get_value_intersect_valid_and_sent_data(){
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputInt('myInteger'))->setMin(0),
			(new InputString('name')),
			(new InputString('title'))
		]);

		$filter->setData([
			'myInteger' => -1,
			'name' => 'mrX',
			'format' => 'none'
		]);


		$this->assertTrue($filter->isValid());
		$this->assertEquals(['name' => 'mrX'], $filter->getValue());

		$this->assertEquals(null, $filter->title->getValue());
	}

	/**
	 * Création d'un objet avec un manager
	 * Mais, l'utilisateur n'a pas le droit de modifier le manager et on doit prendre le manager par defaut
	 * @test
	 */
	public function check_filter_with_default_value_and_missing_required_field()
	{
		// creation d'un manager
		// l'utilisateur n'a pas le droit
		//

		//$filter = $this->createEmptyFilter();
		$filter = $this->createEmptyFilter();
		$manager = new InputValue('manager');
		$manager->addFilterCallback(function($value){
			// user cant create the manager
			return false;
		});
		$manager->setModeDefaultValue(1)->setRequired(true);
		$filter->addInput($manager);

		$filter->setData(['manager' => 2]);

		$manager->filter()->setDisabled(true);

		$this->assertTrue($filter->isValid());
		$this->assertEquals([], $filter->getValue());
	}

	/** @test */
	public function check_filter_missing_required(){
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputInt('myInteger'))->setMin(0),
			(new InputString('name'))->setRequired(true),
		]);

		$filter->setData([
			'myInteger' => -1,
			'format' => 'none'
		]);

		$this->assertFalse($filter->isValid());
	}
}
