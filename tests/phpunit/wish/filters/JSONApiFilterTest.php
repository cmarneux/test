<?php
/**
 * JSONApiFilterTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\AbstractJsonAPI;
use Wish\Filters\Inputs\InputRelationship;
use Wish\Filters\Inputs\InputValue;

class JSONApiFilterTest extends TestCase {
	/**
	 * @return AbstractJsonAPI
	 */
	private function createEmptyFilter(){
		return $this->getMockForAbstractClass(AbstractJsonAPI::class);
	}

	/** @test */
	public function check_filter_fill()
	{
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputValue('test')),
			(new InputRelationship('rel'))->addFilterType('banana')
		]);

		$filter->setData([
			'attributes' => [
				'test' => 'abc'
			],
			'relationships' => [
				'rel' => [
					'data' => [
						'id' => 1,
						'type' => 'banana'
					]
				]
			]
		]);

		$filter->filter();

		$this->assertEquals([
			'attributes' => [
				'test' => 'abc',
			],
			'relationships' => [
				'rel' => 1
			]
		], $filter->getValue());

		$this->assertEquals('abc', $filter->test->getValue());
		$this->assertEquals('1', $filter->rel->getValue());
		$this->assertEquals('banana', $filter->rel->getType());
	}
}
