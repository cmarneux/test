<?php
/**
 * InputBooleanTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputBoolean;

class InputBooleanTest extends TestCase {
	/** @test */
	public function check_cast_to_boolean()
	{
		$input = new InputBoolean('test');

		$input->setValue('false');
		$this->assertEquals(false, $input->getValue());

		$input->setValue('true');
		$this->assertEquals(true, $input->getValue());

		$input->setValue(0);
		$this->assertEquals(false, $input->getValue());

		$input->setValue(1);
		$this->assertEquals(true, $input->getValue());
	}
}
