<?php
/**
 * InputDateTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputDate;

class InputDateTest extends TestCase {
	/** @test */
	public function check_date_input()
	{
		$input = new InputDate('test');
		$input->setMode(InputDate::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setValue('2012-12-31');

		$this->assertTrue($input->isValid());
		$this->assertEquals('2012-12-31', $input->getValue());

		$input->setValue('2012-12-31T01:01:00Z');

		$this->assertTrue($input->isValid());
		$this->assertEquals('2012-12-31', $input->getValue());

		$input->setValue('2012-12-31 01:01:00');

		$this->assertFalse($input->isValid());

		$input->setValue('test');

		$this->assertFalse($input->isValid());
		$this->assertEquals(null, $input->getValue());

		$input->setMode(InputDate::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);
		$input->setValue('test');

		$this->assertTrue($input->isValid());
		$this->assertEquals(date('Y-m-d'), $input->getValue());
	}
}
