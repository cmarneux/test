<?php
/**
 * InputDateTimeTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputDateTime;

class InputDateTimeTest extends TestCase {
	/** @test */
	public function check_date_input()
	{
		$input = new InputDateTime('test');
		$input->setMode(InputDateTime::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue('2012-12-31 01:01:00');

		$this->assertTrue($input->isValid());
		$this->assertEquals('2012-12-31 01:01:00', $input->getValue());

		$input->setValue('2012-12-31T02:01:00Z');

		$this->assertTrue($input->isValid());
		$this->assertEquals('2012-12-31 02:01:00', $input->getValue());

		$input->setValue('2012-12-31');

		$this->assertFalse($input->isValid());

		$input->setValue('test');

		$this->assertFalse($input->isValid());
		$this->assertEquals(null, $input->getValue());
	}
}

