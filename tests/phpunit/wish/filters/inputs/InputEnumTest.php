<?php
/**
 * InputBooleanTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputEnum;

class InputEnumTest extends TestCase {
	/** @test */
	public function check_basic_input()
	{
		$input = new InputEnum('test');
		$input->setMode(InputEnum::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowedValues(["abc", "def"]);

		$input->setValue('a');
		$this->assertEquals(false, $input->isValid());

		$input->setValue('def');
		$this->assertEquals(true, $input->isValid());
	}

	/** @test */
	public function check_add_allowed_value()
	{
		$input = new InputEnum('test');
		$input->setMode(InputEnum::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowedValues(["abc", "def"]);
		$input->addAllowedValue('hello');

		$input->setValue('a');
		$this->assertEquals(false, $input->isValid());

		$input->setValue('def');
		$this->assertEquals(true, $input->isValid());

		$input->setValue('hello');
		$this->assertEquals(true, $input->isValid());
	}

	/** @test */
	public function check_strict_mode()
	{
		$input = new InputEnum('test');
		$input->setMode(InputEnum::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setStrict(true);
		$input->setAllowedValues(["1", 2]);

		$input->setValue(1);
		$this->assertEquals(false, $input->isValid());

		$input->setValue('1');
		$this->assertEquals(true, $input->isValid());
	}

	/** @test */
	public function check_empty_values()
	{
		$input = new InputEnum('test');
		$input->setMode(InputEnum::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowedValues([1, 2]);

		$input->setValue(false);
		$this->assertEquals(false, $input->isValid());

		$input->setValue(null);
		$this->assertEquals(false, $input->isValid());

		$input->setValue(0);
		$this->assertEquals(false, $input->isValid());

		$input->setValue(1);
		$this->assertEquals(true, $input->isValid());
	}

	/** @test */
	public function check_default_value()
	{
		$input = new InputEnum('test', 0);
		$input->setMode(InputEnum::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowedValues([1, 2]);

		$input->setValue(3);
		$this->assertEquals(false, $input->isValid());

		$input->setValue(1);
		$this->assertEquals(true, $input->isValid());

		$input->setValue(0);
		$this->assertEquals(true, $input->isValid());
	}
}
