<?php
/**
 * InputFloatTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputFloat;

class InputFloatTest extends TestCase {
	/** @test */
	public function check_cast_to_int()
	{
		$input = new InputFloat('test');
		$input->setValue('1.1');
		$input->filter();

		$this->assertTrue( is_float($input->getValue()) );
		$this->assertEquals(1.1, $input->getValue() );
	}

	/** @test */
	public function check_min()
	{
		$input = new InputFloat('test');
		$input->setMin(3);
		$input->setMode(InputFloat::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setValue(2.9)->filter();

		$this->assertFalse( $input->isValid() );

		$input->setValue(3.0)->filter();
		$this->assertTrue( $input->isValid() );

		$input->setValue(3.1)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_min_excluded()
	{
		$input = new InputFloat('test');
		$input->setMinExcluded(3);
		$input->setMode(InputFloat::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(2.9999)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3.0)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3.0001)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_max()
	{
		$input = new InputFloat('test');
		$input->setMax(3);
		$input->setMode(InputFloat::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(3.00001)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3.00000)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_max_excluded()
	{
		$input = new InputFloat('test');
		$input->setMaxExcluded(3);
		$input->setMode(InputFloat::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(3.001)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3.000000)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(2.99999)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_negative_number()
	{
		$input = new InputFloat('test');
		$input->setMode(InputFloat::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(-1)->filter();

		$this->assertTrue( $input->isValid() );
		$this->assertEquals( -1,  $input->getValue());
	}

	/** @test */
	public function check_min_max()
	{
		$input = new InputFloat('test');
		$input->setMin(0)->setMax(10);
		$input->setMode(InputFloat::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(-1)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(11)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(0)->filter();
		$this->assertTrue( $input->isValid() );

		$input->setValue(10)->filter();
		$this->assertTrue( $input->isValid() );
	}
}
