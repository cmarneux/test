<?php
/**
 * InputIntTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputInt;

class InputIntTest extends TestCase {
	/** @test */
	public function check_cast_to_int()
	{
		$input = new InputInt('test');
		$input->setValue('1');
		$input->filter();

		$this->assertTrue( is_int($input->getValue()) );
	}

	/** @test */
	public function check_min()
	{
		$input = new InputInt('test');
		$input->setMin(3);
		$input->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setValue(2)->filter();

		$this->assertFalse( $input->isValid() );

		$input->setValue(3)->filter();

		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_min_excluded()
	{
		$input = new InputInt('test');
		$input->setMinExcluded(3);
		$input->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(2)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(4)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_max()
	{
		$input = new InputInt('test');
		$input->setMax(3);
		$input->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(4)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_max_excluded()
	{
		$input = new InputInt('test');
		$input->setMaxExcluded(3);
		$input->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(4)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(3)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(2)->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_negative_integer()
	{
		$input = new InputInt('test');
		$input->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(-1)->filter();

		$this->assertTrue( $input->isValid() );
		$this->assertEquals( -1,  $input->getValue());
	}

	/** @test */
	public function check_min_max()
	{
		$input = new InputInt('test');
		$input->setMin(0)->setMax(10);
		$input->setMode(InputInt::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue(-1)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(11)->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue(0)->filter();
		$this->assertTrue( $input->isValid() );

		$input->setValue(10)->filter();
		$this->assertTrue( $input->isValid() );
	}
}
