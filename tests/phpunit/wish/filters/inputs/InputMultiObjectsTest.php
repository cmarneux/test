<?php
/**
 * InputMultiObjectsTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\AbstractFilters;
use Wish\Filters\Inputs\InputInt;
use Wish\Filters\Inputs\InputMultiObjects;
use Wish\Filters\Inputs\InputString;

class InputMultiObjectsTest extends TestCase {
	/**
	 * @return AbstractFilters
	 */
	private function createEmptyFilter(){
		return $this->getMockForAbstractClass(AbstractFilters::class);
	}

	/** @test */
	public function check_input()
	{
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputString('name'))->setRequired(true),
			(new InputInt('age'))->setMin(0)->setMax(150)
		]);

		$input = new InputMultiObjects('test', $filter);

		$input->setValue([
			['name' => 'Bob ', 'age' => 22],
			['name' => 'John', 'age' => '25'],
		]);

		$this->assertTrue( $input->isValid() );
		$this->assertEquals([
			['name' => 'Bob', 'age' => 22],
			['name' => 'John', 'age' => 25]
		], $input->getValue());
	}

	/** @test */
	public function check_mode_ignore()
	{
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputString('name'))->setRequired(true),
			(new InputInt('age'))->setMin(0)->setMax(150)
		]);

		$input = new InputMultiObjects('test', $filter);

		$input->setValue([
			['name' => '', 'age' => 22],
			['name' => 'John', 'age' => '25'],
		]);

		$this->assertTrue( $input->isValid() );
		$this->assertEquals([], $input->getValue());
	}

	/** @test */
	public function check_mode_error()
	{
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputString('name'))->setRequired(true),
			(new InputInt('age'))->setMin(0)->setMax(150)
		]);

		$input = new InputMultiObjects('test', $filter);
		$input->setMode(InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue([
			['name' => '', 'age' => 22],
			['name' => 'John', 'age' => '25'],
		]);

		$this->assertFalse( $input->isValid() );
	}

	/** @test */
	public function check_mode_ignore_item()
	{
		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputString('name'))->setRequired(true),
			(new InputInt('age'))->setMin(0)->setMax(150)
		]);

		$input = new InputMultiObjects('test', $filter);
		$input->setMode(InputMultiObjects::MODE_IGNORE_WRONG_ENTRY);

		$input->setValue([
			['name' => '', 'age' => 22],
			['name' => 'John', 'age' => '25'],
		]);

		$this->assertTrue( $input->isValid() );
		$this->assertEquals([
			['name' => 'John', 'age' => 25]
		], $input->getValue());
	}

	/** @test */
	public function check_invalide_on_post_validation() {

		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputString('name'))->setRequired(true),
			(new InputInt('age'))->setMin(0)->setMax(150)->setRequired(true)
		]);

		$input = new InputMultiObjects('test', $filter);
		$input->setMode(InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue([
			['name' => 'Hum hum', 'age' => 22],
			['name' => 'John', 'age' => '25'],
		]);

		$this->assertTrue( $input->isValid() );

		$input->getModel()->age->setMax(24);

		$this->assertFalse( $input->isValid() );
	}

	/** @test */
	public function check_invalide_with_manual_sub_invalidation() {

		$filter = $this->createEmptyFilter();
		$filter->addInput([
			(new InputString('name'))->setRequired(true),
			(new InputInt('age'))->setMin(0)->setMax(150)->setRequired(true)
		]);

		$input = new InputMultiObjects('test', $filter);
		$input->setMode(InputMultiObjects::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue([
			['name' => 'Hum hum', 'age' => 22],
			['name' => 'John', 'age' => '25'],
		]);

		$this->assertTrue( $input->isValid() );

		$input->getItems()[0]->age->invalidate('PAS CONTENT');

		$this->assertFalse( $input->isValid() );
	}
}
