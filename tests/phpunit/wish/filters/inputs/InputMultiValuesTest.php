<?php
/**
 * InputMultiValues.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputMultiValues;

class InputMultiValuesTest extends TestCase {
	/** @test */
	public function check_basic_input()
	{
		$input = new InputMultiValues('test');
		$input->setMode(InputMultiValues::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue([0, 2, 'test']);
		$input->filter();

		$this->assertEquals([0, 2, 'test'], $input->getValue());

		$this->assertNotEquals([1, 2, 'test'], $input->getValue());
	}

	/** @test */
	public function check_mode(){

		$input = new InputMultiValues('testMode');
		$input->addFilterCallback(function($value){
			return false;
		});
		$input->setDefaultValue(['test']);

		$input->setValue([1, 2]);

		$this->assertTrue( $input->isValid() );
		$this->assertEquals([], $input->getValue());

		$input->setMode(InputMultiValues::MODE_REPLACE_INCORRECT_VALUE_WITH_DEFAULT);

		$this->assertTrue( $input->isValid() );
		$this->assertEquals(['test'], $input->getValue());

		$input->setMode(InputMultiValues::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setValue([1, 2]);

		$this->assertFalse($input->isValid());
		$this->assertEquals([], $input->getValue());

	}

	/** @test */
	public function check_ignore_wrong_entries()
	{
		$input = new InputMultiValues('test');
		$input->addFilterCallback(function($value){
			return is_numeric($value)?$value:false;
		});
		$input->setMode(InputMultiValues::MODE_IGNORE_WRONG_ENTRY);

		$input->setValue([0, 2, 'test', '1']);
		$input->filter();

		$this->assertTrue($input->isValid());
		$this->assertEquals([0, 2, '1'], $input->getValue());
	}

	/** @test */
	public function check_group_filter()
	{
		$input = new InputMultiValues('test');
		$input->addGroupFilterCallback(function($values){
			return (count($values) == 2) ? $values : false;
		});
		$input->setMode(InputMultiValues::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowEmptyValue(false);

		$input->setValue([0, 1, 2]);

		$this->assertFalse( $input->isValid() );

		$input->setValue([0, 1]);

		$this->assertTrue( $input->isValid() );

		$input->setValue([]);

		$this->assertFalse( $input->isValid() );
	}

	/** @test */
	public function check_unicity_filter()
	{
		$input = new InputMultiValues('test');
		$input->addUnicityFilter();
		$input->setValue([0, 1, 2, 1]);

		$this->assertFalse( $input->isValid() );
	}
}
