<?php
/**
 * InputRelationship.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputRelationship;

class InputRelationshipTest extends TestCase {
	/** @test */
	public function check_set_data()
	{
		$input = new InputRelationship('test');
		$input->setValue([
			'id' => 1,
			'type' => 'bananas'
		]);

		$input->filter();

		$this->assertEquals(1, $input->getValue());
		$this->assertEquals('bananas', $input->getType());
	}

	/** @test */
	public function check_set_empty_data_with_error()
	{
		$input = new InputRelationship('testa');
		$input->addFilterCallback(function($value){
			//if($value === null) return 10;
			return ($value > 1) ? $value : false;
		});
		$input->setMode(InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setValue([]);

		$input->isValid();
		$this->assertFalse($input->isValid());
	}

	/** @test */
	public function check_set_empty_data_with_empty()
	{
		$input = new InputRelationship('testa');
		$input->addFilterCallback(function($value){
			//if($value === null) return 10;
			return ($value > 1) ? $value : false;
		});
		$input->setMode(InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowEmptyValue(true);
		$input->setValue([]);

		$input->isValid();
		$this->assertTrue($input->isValid());
	}

	/** @test */
	public function check_type_filter()
	{
		$input = new InputRelationship('test');
		$input->setMode(InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->addFilterType('orange');

		$input->setValue([
			'id' => 1,
			'type' => 'banana'
		]);

		$this->assertFalse($input->isValid());
		$this->assertNull($input->getValue());
		$this->assertNull($input->getType());

		$input->setValue([
			'id' => 1,
			'type' => 'orange'
		]);

		$this->assertTrue($input->isValid());
		$this->assertEquals(1, $input->getValue());
		$this->assertEquals('orange', $input->getType());
	}

	/** @test */
	public function check_multy_type_filter()
	{
		$input = new InputRelationship('test');
		$input->setMode(InputRelationship::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->addFilterType(['orange', 'kiwi']);

		$input->setValue([
			'id' => 1,
			'type' => 'banana'
		]);

		$this->assertFalse($input->isValid());
		$this->assertNull($input->getValue());
		$this->assertNull($input->getType());

		$input->setValue([
			'id' => 1,
			'type' => 'kiwi'
		]);

		$this->assertTrue($input->isValid());
		$this->assertEquals(1, $input->getValue());
		$this->assertEquals('kiwi', $input->getType());
	}
}
