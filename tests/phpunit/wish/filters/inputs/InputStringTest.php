<?php
/**
 * InputStringTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use PHPUnit\Framework\TestCase;
use Wish\Filters\Inputs\InputString;

class InputStringTest extends TestCase {
	/** @test */
	public function check_cast_to_string()
	{
		$input = new InputString('test');
		$input->setValue(1);
		$input->filter();

		$this->assertTrue( is_string($input->getValue()) );
		$this->assertEquals( "1", $input->getValue() );
	}

	/** @test */
	public function check_min()
	{
		$input = new InputString('test');
		$input->setMinLength(3);
		$input->setMode(InputString::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue('ab')->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue('abc')->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_max()
	{
		$input = new InputString('test');
		$input->setMaxLength(3);
		$input->setMode(InputString::MODE_ERROR_ON_INCORRECT_VALUE);

		$input->setValue('abcd')->filter();
		$this->assertFalse( $input->isValid() );

		$input->setValue('abc')->filter();
		$this->assertTrue( $input->isValid() );
	}

	/** @test */
	public function check_trim()
	{
		$input = new InputString('test');
		$input->setMode(InputString::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setAllowEmptyValue(false);

		$input->setValue(' test ')->filter();
		$this->assertEquals( 'test', $input->getValue() );

		$input->setValue(' ')->filter();
		$this->assertFalse($input->isValid());
	}

	/** @test */
	public function check_trim_required()
	{
		$input = new InputString('test');
		$input->setMode(InputString::MODE_ERROR_ON_INCORRECT_VALUE);
		$input->setRequired(true);

		$input->setValue(' ')->filter();
		$this->assertFalse($input->isValid());
	}
}

