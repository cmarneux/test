<?php
/**
 * InputValueTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Filters\Inputs;

use Wish\Filters\Inputs\InputValue;
use PHPUnit\Framework\TestCase;

class InputValueTest extends TestCase{
	/** @test */
	public function check_validation_is_required(){

		$inputValue = new InputValue('test');
		$inputValue->setRequired(true);

		$this->assertEquals($inputValue->isValid(), false);

		$inputValue->setValue('notEmpty');
		$this->assertEquals($inputValue->isValid(), true);
	}

	/** @test */
	public function check_validation_not_required(){
		$inputValue = new InputValue('test');
		$this->assertEquals( $inputValue->isValid(), true);
	}

	/** @test */
	public function check_get_value_validate_input(){
		$inputValue = new InputValue('test');
		$inputValue->addFilter(FILTER_CALLBACK, function($value){
			return intval($value)+1;
		});
		$inputValue->setValue(1);

		$this->assertEquals( 2, $inputValue->getValue());
	}

	/** @test */
	public function check_callback_filter(){
		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			return $value == 1;
		});
		$inputValue->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);

		$inputValue->setValue(1);
		$this->assertEquals( true, $inputValue->isValid());

		$inputValue->setValue(2);
		$this->assertEquals( false, $inputValue->isValid());
	}

	/** @test */
	public function check_callback_context(){
		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			/** @var InputValue $this */
			return $this->name;
		});
		$inputValue->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);

		$inputValue->setValue(1);
		$this->assertEquals( 'test', $inputValue->getValue());
	}

	/** @test */
	public function check_callback_modification()
	{
		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			if($value == '1') return ['data'=>$value];
			else return $value;
		});

		$inputValue->setValue('2')->filter();
		$this->assertTrue( is_string($inputValue->getValue()));

		$inputValue->setValue('1')->filter();
		$this->assertTrue( is_array($inputValue->getValue()));
	}

	/** @test  */
	public function check_is_defined(){
		$inputValue = new InputValue('test');
		$this->assertEquals(false, $inputValue->isDefined());

		$inputValue->setValue(1);
		$this->assertEquals(true, $inputValue->isDefined());
	}

	/** @test */
	public function check_reset(){
		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			return $value == 1;
		});
		$inputValue->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);
		$inputValue->setValue(1)->filter();

		$this->assertEquals(true, $inputValue->isDefined());
		$this->assertEquals(true, $inputValue->isValid());
		$this->assertEquals(1, $inputValue->getValue());
		$this->assertEquals(1, $inputValue->getRawValue());

		$inputValue->reset();

		$this->assertEquals(false, $inputValue->isDefined());
		$this->assertEquals(null, $inputValue->getValue());
		$this->assertEquals(null, $inputValue->getRawValue());

		$inputValue->setValue(2);

		$this->assertEquals(false, $inputValue->isValid());
		$this->assertEquals(2, $inputValue->getRawValue());
		$this->assertEquals(null, $inputValue->getValue());
	}

	/** @test */
	public function check_input_mode_replace_default(){

		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value) {
			return $value == 1;
		});
		$inputValue->setModeDefaultValue(12);
		$inputValue->setValue(2);

		$this->assertEquals(true, $inputValue->isValid());
		$this->assertEquals(true, $inputValue->isDefined());
		$this->assertEquals(12, $inputValue->getValue());
		$this->assertEquals(2, $inputValue->getRawValue());
	}

	/** @test */
	public function chech_input_replace_with_defaut_and_field_required()
	{
		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			return false;
		});
		$inputValue->setModeDefaultValue(12)->markDefaultValueAsDefined(true);

		$this->assertEquals(true, $inputValue->isValid());
		$this->assertEquals(true, $inputValue->isDefined());

		$this->assertEquals(12, $inputValue->getValue());
		$this->assertEquals(null, $inputValue->getRawValue());
	}

	/** @test */
	public function check_input_mode_error(){

		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			return $value == 1;
		});
		$inputValue->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);
		$inputValue->setValue(2);

		$this->assertEquals(false, $inputValue->isValid());
		$this->assertEquals(true, $inputValue->isDefined());
		$this->assertEquals(null, $inputValue->getValue());
		$this->assertEquals(2, $inputValue->getRawValue());
	}

	/** @test */
	public function check_input_mode_ignore(){

		$inputValue = new InputValue('test');
		$inputValue->addFilterCallback(function($value){
			return $value == 1;
		});
		$inputValue->setMode(InputValue::MODE_IGNORE_INCORRECT_VALUE);
		$inputValue->setValue(2);

		$this->assertEquals(true, $inputValue->isValid());
		$this->assertEquals(false, $inputValue->isDefined());
		$this->assertEquals(null, $inputValue->getValue());
		$this->assertEquals(null, $inputValue->getRawValue());
	}

	/** @test */
	public function check_array_filter(){
		$inputValue = new InputValue('test');
		$inputValue->addFilterInArray([0, 1, 2]);
		$inputValue->setMode(InputValue::MODE_ERROR_ON_INCORRECT_VALUE);

		$inputValue->setValue(2);
		$this->assertEquals(true, $inputValue->isValid());

		$inputValue->setValue(0);
		$this->assertEquals(true, $inputValue->isValid());

		$inputValue->setValue(3);
		$this->assertEquals(false, $inputValue->isValid());
	}
}
