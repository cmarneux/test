<?php
/**
 * ModelTest.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\Models;

use PHPUnit\Framework\TestCase;
use Wish\Models\Model;

class ModelTest extends TestCase {
	/** @test */
	public function check_model_construct()
	{
		$model = new Model([
			'age' => 11,
			'name' => 'Bob'
		]);

		$this->assertEquals(11, $model->age);
		$this->assertEquals('Bob', $model->name);
	}

	/** @test */
	public function check_set_from_array()
	{
		$model = new Model(['title' =>'director']);
		$model->fromArray([
			'age' => 11,
			'name' => 'Bob'
		]);

		$this->assertEquals(11, $model->age);
		$this->assertEquals('Bob', $model->name);
		$this->assertEquals('director', $model->title);
	}

	/** @test */
	public function check_model_arrayAccess()
	{
		$model = new Model([
			'age' => 11,
			'name' => 'Bob'
		]);

		$this->assertEquals(11, $model['age']);
	}

	/** @test */
	public function check_model_toArray()
	{
		$model = new Model([
			'age' => 11,
			'name' => 'Bob'
		]);

		$this->assertEquals([
			'age' => 11,
			'name' => 'Bob'
		], $model->toArray());
	}

	/** @test */
	public function check_model_removeFields()
	{
		$model = new Model([
			'age' => 11,
			'name' => 'Bob'
		]);

		$model->removeFields(['age']);

		$this->assertEquals([
			'name' => 'Bob'
		], $model->toArray());
	}
}
