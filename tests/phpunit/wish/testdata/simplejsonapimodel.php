<?php
/**
 * simplejsonapimodel.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\TestData;

use Wish\Models\ModelJSON;
use Wish\Models\ModelJSONAPI;

/**
 * Class SimpleJSONApiModel
 * @package Tests\PhpUnit\Wish\TestData
 * @property string id
 * @property string lastName
 * @property string firstName
 * @property string typeOf
 * @property int age
 * @property float money
 * @property string lastConnexion
 * @property Address address
 * @property SimpleJSONApiModel friend
 */
class SimpleJSONApiModel extends ModelJSONAPI {

	protected static $_jsonIDAttribute = 'id';
	protected static $_jsonType = 'user';

	public static function getPublicFieldsDefinition()
	{
		return [
			'ID'                  => ['name' => 'id', 'type' => self::TYPE_STRING],
			'PROFIL_NOM'          => ['name' => 'lastName', 'type' => self::TYPE_STRING],
			'PROFIL_PRENOM'       => ['name' => 'firstName', 'type' => self::TYPE_STRING],
			'PROFIL_AGE'          => ['name' => 'age', 'type' => self::TYPE_INT],
			'PROFIL_TOTAL_WALLET' => ['name' => 'money', 'type' => self::TYPE_FLOAT],
			'PROFIL_CONNECTION'   => ['name' => 'lastConnexion', 'type' => self::TYPE_DATETIME],
			'PROFIL_VIP'          => ['name' => 'vip', 'type' => self::TYPE_BOOLEAN],
			'address'             => ['name' => 'address', 'type' => self::TYPE_OBJECT],
			'PROFIL_WALLET'       => ['name' => 'wallet', 'type' => self::TYPE_ARRAY],
			'PROFIL_TYPE'         => ['name' => 'typeOf', 'type' => self::TYPE_STRING, 'mapper' => [
				0 => 'admin',
				1 => 'modo',
				2 => 'manager'
			]],
			'EMPTY_VALUE' => ['name' => 'canBeEmpty', 'type' => self::TYPE_STRING, 'removeIfEmpty' => true, 'removeIf' => 'test'],
			'CLOTHES'     => ['name' => 'clothes', 'type' => self::TYPE_ARRAY, 'serializedArray' => true],
			'SA'          => ['name' => 'sa', 'type' => self::TYPE_ARRAY, 'serializeCallback' => 'serializer', 'unserializeCallback' => 'unserializer'],
		];
	}

	protected function initRelationships()
	{
		$this->setRelationships('FRIEND', 'friend', self::class);
	}

	public static function serializer($value){
		return implode('#', array_map(function($value){
			return implode('|', $value);
		}, $value));
	}

	public static function unserializer($value){
		return array_map(function($v){
			return explode('|', $v);
		}, explode('#', $value));
	}
}

class Address extends ModelJSON {
	public static function getPublicFieldsDefinition()
	{
		return [
			'STREET'  => ['name' => 'street', 'type' => self::TYPE_STRING],
			'CITY'    => ['name' => 'city', 'type' => self::TYPE_STRING],
			'COUNTRY' => ['name' => 'country', 'type' => self::TYPE_STRING]
		];
	}
}
