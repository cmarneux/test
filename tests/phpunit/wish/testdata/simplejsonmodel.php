<?php
/**
 * MyModel.php
 * @author Louis D'Arras <louis.darras@wishgroupe.com>
 */

namespace Tests\PhpUnit\Wish\TestData;

use Wish\Models\ModelJSON;

class SimpleJSONModel extends ModelJSON{

	public static function getPublicFieldsDefinition()
	{
		return [
			'PROFIL_NOM'    => ['name' => 'lastName', 'type' => self::TYPE_STRING],
			'PROFIL_PRENOM' => ['name' => 'firstName', 'type' => self::TYPE_STRING],
			'PROFIL_TYPE'   => ['name' => 'typeOf', 'type' => self::TYPE_STRING, 'mapper' => [
				0 => 'admin',
				1 => 'modo',
				2 => 'manager'
			]],
		];
	}
}
