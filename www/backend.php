<?php
$initStart = microtime(true);
/**
 * index.php
 *
 * This file is executed for each request
 *
 * @author  Tanguy Lambert <tanguy.lambert@wishgroupe.com>
 */
define('BOONDMANAGER_SESSIONS_PATH', '../sessions');

ini_set("session.save_path", realpath(dirname(__FILE__).'/'.BOONDMANAGER_SESSIONS_PATH)); //Sessions folder storage
ini_set("auto_detect_line_endings", 1); //Automatic end of lines detection in text files (Necessary for CSV reading)
ini_set("expose_php", "off");
ini_set("session.gc_probability", 1);   //Deletion of old sessions files
ini_set("soap.wsdl_cache_enabled", 1);  //WSDL file definition cache activation
ini_set('default_socket_timeout', 25);  //Socket timeout : 25 seconds

ignore_user_abort(true);
$initEnd = microtime(true);

$initF3 = microtime(true);

/** @var \Base $f3 Global F3 variable [\Base](http://fatfreeframework.com/base).en base de données. */
//Autoloader composer
require('../vendor/autoload.php');
$f3 = \Base::instance();

if($f3->exists('SERVER.HTTP_USER_AGENT')) ini_set('user_agent', $f3->get('SERVER.HTTP_USER_AGENT')); //User agent definition (Necessary to avoid FOPEN errors)

//Main application folder
$f3->set('MAIN_ROOTPATH', realpath(dirname(__FILE__).'/..'));
//F3 folder
$f3->set('MAIN_APPPATH', realpath(dirname(__FILE__).'/../app'));

//Main configuration files loading
$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/boondmanager.ini');
$f3->config($f3->get('MAIN_APPPATH').'/boondmanager/configs/routes.ini');
\Base::instance()->set('PLUGINS', \Base::instance()->get('MAIN_APPPATH').'/');

$endF3 = microtime(true);

$initBM = microtime(true);
use BoondManager\Services\BM;
use Wish\Tools;

BM::initBackEnd();

if(BM::isDevelopmentMode()) Tools::initClockwork();
$endBM = microtime(true);

Tools::getContext()->setTiming('Script init', $initStart, $initEnd);
Tools::getContext()->setTiming('F3 init', $initF3, $endF3);
Tools::getContext()->setTiming('BM init', $initBM, $endBM);

$id = Tools::getContext()->startTiming('run');
$f3->run();
$duration = Tools::getContext()->endTiming($id);
\BoondManager\Lib\Log::addInfo( \BoondManager\Lib\Log\Message::execTime($duration) );
